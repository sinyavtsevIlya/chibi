// Simplified Skybox shader. Differences from regular Skybox one:
// - no tint color

Shader "Mobile/Skybox Blend" {
Properties {

	_Blend("Blend", Range(0.0,1.0)) = 0.0

	_FrontTex ("Front (+Z)", 2D) = "white" {}
	_BackTex ("Back (-Z)", 2D) = "white" {}
	_LeftTex ("Left (+X)", 2D) = "white" {}
	_RightTex ("Right (-X)", 2D) = "white" {}
	_UpTex ("Up (+Y)", 2D) = "white" {}
	_DownTex ("Down (-Y)", 2D) = "white" {}

	_FrontTex1 ("Front1 (+Z)", 2D) = "white" {}
	_BackTex1 ("Back1 (-Z)", 2D) = "white" {}
	_LeftTex1 ("Left1 (+X)", 2D) = "white" {}
	_RightTex1 ("Right1 (-X)", 2D) = "white" {}
	_UpTex1 ("Up1 (+Y)", 2D) = "white" {}
	_DownTex1 ("Down1 (-Y)", 2D) = "white" {}
}

SubShader {
	Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox" }

	//Cull Off ZWrite Off Fog { Mode Off }
	Pass {

		SetTexture [_FrontTex] { combine texture }
		SetTexture[_FrontTex1]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
	}
	Pass {
		SetTexture [_BackTex]  { combine texture }
			SetTexture[_BackTex1]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
	}
	Pass {
		SetTexture [_LeftTex]  { combine texture }
		SetTexture[_LeftTex1]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
	}
	Pass {
		SetTexture [_RightTex] { combine texture }
		SetTexture[_RightTex1]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
	}
	Pass {
		SetTexture [_UpTex]    { combine texture }
		SetTexture[_UpTex1]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
	}
	Pass {
		SetTexture [_DownTex]  { combine texture }
		SetTexture[_DownTex1]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
	}
}



}
