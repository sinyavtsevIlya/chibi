
Shader "Mobile/Terrain/Diffuse 3 RGB" {
Properties {
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125

	_Control ("Control (RGB)", 2D) = "white" {}

	_Splat2 ("Layer 2 (B)", 2D) = "white" {}
	_Splat1 ("Layer 1 (G)", 2D) = "white" {}
	_Splat0 ("Layer 0 (R)", 2D) = "white" {}
	_DopSplat("Dop", 2D) = "white" {}
	_Alpha("Alpha", Range(0,1)) = 0.5
}
                
SubShader {
	Tags {
   		"SplatCount" = "3"
   		"RenderType" = "Opaque"
	}
	LOD 0
		Cull off

	
CGPROGRAM
#pragma surface surf MobileBlinnPhong
#pragma target 3.0

inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
{
	fixed diff = max (0, dot (s.Normal, lightDir));
	fixed nh = max (0, dot (s.Normal, halfDir));
	fixed spec = pow (nh, s.Specular*128) * s.Gloss;
	
	fixed4 c;
	c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
	UNITY_OPAQUE_ALPHA(c.a);
	return c;
}

struct Input {
	float2 uv_Control : TEXCOORD0;
	float2 uv_Splat0 : TEXCOORD1;
	float2 uv_Splat1 : TEXCOORD2;
//	float2 uv_Splat2 : TEXCOORD3;
	float2 uv_DopSplat : TEXCOORD4;
};
 
sampler2D _Control;
sampler2D _Splat0, _Splat1, _Splat2, _DopSplat;
float _Alpha;

half _Shininess;

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 splat_control = tex2D (_Control, IN.uv_Control);
	fixed4 splat_0 = tex2D(_Splat0, IN.uv_Splat0);
	fixed4 splat_dop = tex2D(_DopSplat, IN.uv_DopSplat);
	fixed4 col;

	fixed4 splat_r = (splat_0 * _Alpha + splat_dop * (1 - _Alpha))/1.1f;

	col = splat_control.r * splat_r;
	col += splat_control.g * tex2D (_Splat1, IN.uv_Splat1);
	col += splat_control.b * tex2D (_Splat2, IN.uv_Splat0);
	
	o.Albedo = col.rgb;
	//o.Gloss = col.a;
//	o.Alpha = col.a;
	
	//o.Specular = _Shininess;
}
ENDCG 
}

Fallback "Mobile/VertexLit"
}
