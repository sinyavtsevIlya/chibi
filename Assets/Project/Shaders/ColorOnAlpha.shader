﻿Shader "Custom/DoubleSided/ColorOnAlpha" {
        Properties {
				_Color ("Main Color", Color) = (1,1,1,1)
                _MainTex ("Base (RGB)", 2D) = "white" {}
                _MaskTex ("Mask (RGB) Trans (A)", 2D) = "white" {}
        }
        SubShader {
                Tags {"Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True"}
                LOD 200
                Cull Off

                CGPROGRAM
                #pragma surface surf Lambert

                sampler2D _MainTex;
                sampler2D _MaskTex;
				fixed4 _Color;

                struct Input {
                        float2 uv_MainTex;
                        float2 uv_MaskTex;
                };

                void surf (Input IN, inout SurfaceOutput o) {
                        half4 tex = tex2D (_MainTex, IN.uv_MainTex);
                        half4 mask = tex2D (_MaskTex, IN.uv_MaskTex);
						
                        o.Albedo = tex.rgb + (mask.rgb/3 * _Color);	
											
                        o.Alpha = 1;
                }
                ENDCG
        } 
         Fallback "Transparent/Cutout/VertexLit"
}