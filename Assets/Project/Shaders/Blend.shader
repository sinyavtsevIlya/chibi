﻿Shader "Custom/Blend" {
	Properties{
		_Blend("Blend", Range(0.0,1.0)) = 0.0

		_Tex1("Textture 1", 2D) = "white" {}
	_Tex2("Texture 2", 2D) = "white" {}
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			Pass{

			SetTexture[_Tex1]{ combine texture }
			SetTexture[_Tex2]{ constantColor(0,0,0,[_Blend]) combine texture lerp(constant) previous }
		}
	}
}
