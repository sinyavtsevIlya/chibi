﻿Shader "Transparent/Diffuse ZWrite" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	}

		SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		//Tags { "Queue"="AlphaTest" "IgnoreProjector"="true" "RenderType"="TransparentCutout" }
		LOD 200
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha

		// extra pass that renders to depth buffer only
		Pass{
		ZWrite On
		ColorMask 0
		Cull Off
	}

		ZWrite Off
		Cull Off
		CGPROGRAM
#pragma surface surf Standard fullforwardshadows  alpha:blend
		//#pragma surface surf Standard fullforwardshadows alphatest:_Cutout
		//#pragma surface surf Standard fullforwardshadows alpha:fade

		sampler2D _MainTex;
	fixed4 _Color;

	struct Input {
		float2 uv_MainTex;
	};

	//void surf (Input IN, inout SurfaceOutput o) {
	void surf(Input IN, inout SurfaceOutputStandard o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
	}
	ENDCG
		// paste in forward rendering passes from Transparent/Diffuse
		//UsePass "Transparent/Diffuse/FORWARD"
	}

		Fallback "Legacy Shaders/Transparent/Diffuse"
}
