﻿Shader "Custom/AlphaTest Diffuse 2 TEX" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	_SecondTex("Base (RGB) Trans (A)", 2D) = "white" {}
	_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		_Alpha("Alpha", Range(0,1)) = 0.5
	}
		SubShader{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 200
		Cull Off

		CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff addshadow
		sampler2D _MainTex;
	sampler2D _SecondTex;
	float _Alpha;
	float4 _Color;

	struct Input {
		float2 uv_MainTex;
		float2 uv_SecondTex;
	};
	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 f = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		fixed4 s = tex2D(_SecondTex, IN.uv_SecondTex);
	//	s.a = _Alpha;
		o.Albedo = (f.rgb + s.rgb * _Alpha);
		o.Alpha = f.a ;
	}
	ENDCG
	}
		Fallback "Legacy Shaders/Transparent/Cutout/VertexLit"
}