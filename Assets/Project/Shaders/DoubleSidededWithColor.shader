﻿Shader "Custom/DoubkeSided With Color" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_Alpha("Alpha", Range(0,1)) = 0.5
	}
		SubShader{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 200
		Cull Off

		CGPROGRAM
#pragma surface surf Lambert
		sampler2D _MainTex;
	float4 _Color;

	struct Input {
		float2 uv_MainTex;
	};
	void surf(Input IN, inout SurfaceOutput o) {
		o.Albedo = tex2D(_MainTex, IN.uv_MainTex)  *_Color;
	}
	ENDCG
	}
		Fallback "Legacy Shaders/Transparent/Cutout/VertexLit"
}