﻿Shader "Custom/DoubleSidedWithColorAlpha" {
        Properties {
				_Color ("Main Color", Color) = (1,1,1,1)
                _MainTex ("Base (RGB)", 2D) = "white" {}
                _MaskTex ("Mask (RGB) Trans (A)", 2D) = "white" {}
                _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
				_A("Alpha", Range(0,1)) = 0.5
        }
        SubShader {
                Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
                LOD 200
                Cull Off

                CGPROGRAM
                #pragma surface surf Lambert alphatest:_Cutoff

                sampler2D _MainTex;
                sampler2D _MaskTex;
				fixed4 _Color;
				fixed _A;

                struct Input {
                        float2 uv_MainTex;
                        float2 uv_MaskTex;
                };

                void surf (Input IN, inout SurfaceOutput o) {
                        half4 tex = tex2D (_MainTex, IN.uv_MainTex) * _Color;
                        half4 mask = tex2D (_MaskTex, IN.uv_MaskTex)* _Color;

                        o.Albedo = tex.rgb;
                        o.Alpha = _A ;
                }
                ENDCG
        } 
         Fallback "Transparent/Cutout/VertexLit"
}