﻿Shader "Custom/RGBWater" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
	_Blend("Blend", Range(0,1)) = 0.5
	_DopTex("Dop", 2D) = "white" {}
	_WiggleTex("Base (RGB)", 2D) = "white" {}
	_WiggleStrength("Wiggle Strength", Range(0.01, 0.1)) = 0.01
	}
		SubShader{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 200
		Cull Off

		CGPROGRAM
#pragma surface surf Lambert alpha:fade

		sampler2D _MainTex;
	sampler2D _DopTex;
	float _Blend;
	sampler2D _WiggleTex;
	fixed4 _Color;
	float _WiggleStrength;

	struct Input
	{
		float2 uv_MainTex;
		float2 uv_DopTex;
		float2 uv_WiggleTex;
		float _Blend;
	};

	void surf(Input IN, inout SurfaceOutput o)
	{
		//	tc2.x += (sin(_Time+tc2.x*10)* 0.01f * ((fmod(_Time,2.0f)) - 1.0f));
		//	tc2.y -= (cos(_Time+tc2.y*10)* 0.01f  * ((fmod(_Time,2.0f)) - 1.0f)); 	

		float2 tc2 = IN.uv_WiggleTex;
		tc2.x -= _SinTime;
		tc2.y += _CosTime;
		float4 wiggle = tex2D(_WiggleTex, tc2);

		IN.uv_MainTex.x -= wiggle.r * _WiggleStrength;
		IN.uv_MainTex.y += wiggle.b * _WiggleStrength*1.5f;

		fixed4 main = tex2D(_MainTex, IN.uv_MainTex);
		fixed4 dop = tex2D(_DopTex, IN.uv_DopTex);
		fixed4 c = ((main * _Blend + dop * (1 - _Blend)) / 1.1f)* _Color;

	
	//	fixed4 c = tex2D(newmain, IN.uv_MainTex) * _Color;
		//fixed4 c = newmain * _Color;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
	}
	ENDCG
	}
	FallBack "Diffuse"
}
