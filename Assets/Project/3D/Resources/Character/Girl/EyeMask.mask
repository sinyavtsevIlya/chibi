%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: EyeMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: asuna
    m_Weight: 1
  - m_Path: Base Human
    m_Weight: 1
  - m_Path: Base HumanAss for elements
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanLThigh
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanLThigh/Base HumanLCalf
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanLThigh/Base HumanLCalf/Base HumanLFoot
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanRThigh
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanRThigh/Base HumanRCalf
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanRThigh/Base HumanRCalf/Base HumanRFoot
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanLCollarbone
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanLCollarbone/Base HumanLUpperarm
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base HumanLPalm
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/crown
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/ears
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/EYE Right Bone
    m_Weight: 0
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/Eyebow bone
    m_Weight: 0
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/Eyelid Left Bone
    m_Weight: 0
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/Eyelid Left Bone 1
    m_Weight: 0
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/Eyelid Right Bone
    m_Weight: 0
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/hat
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/Mouth bottom bone
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanNeck/Base HumanHead/Mouth top bone
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanRCollarbone
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanRCollarbone/Base HumanRUpperarm
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm
    m_Weight: 1
  - m_Path: Base HumanAss for elements/Base HumanSpine1/Base HumanSpine2/Base HumanRibcage/Base
      HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base HumanRPalm
    m_Weight: 1
  - m_Path: Base HumanLPlatform
    m_Weight: 1
  - m_Path: Base HumanRPlatform
    m_Weight: 1
  - m_Path: girl_low
    m_Weight: 1
  - m_Path: harley
    m_Weight: 1
  - m_Path: kunoichi
    m_Weight: 1
  - m_Path: lolita
    m_Weight: 1
  - m_Path: queen
    m_Weight: 1
  - m_Path: vampire
    m_Weight: 1
