﻿using UnityEngine;
using System.Collections;

public class RotateRes : MonoBehaviour {

    public float rotationSpeedX=90;
    public float rotationSpeedY=0;
    public float rotationSpeedZ=0;
    
	
	void Update ()
    {
        transform.Rotate(new Vector3(rotationSpeedX, rotationSpeedY, rotationSpeedZ) * Time.deltaTime);
    }
}
