﻿using UnityEngine;
using System.Collections.Generic;


public class Localization{

    public string CurrentLanguage = "";
    private Localization()
    {

    }

    private static Localization _instance;

    public static Localization instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new Localization();
            }
            return _instance;
        }
    }

    public enum LocalizationLanguage
    {
        RU,
        EN,
        JP,
        TH
    }

    public LocalizationLanguage Lang;

    Dictionary<string, string> _dictionary = null;
    Dictionary<string, string> _englishdictionary = null;

    public void SetLanguage(string inLang, bool saveLang = true)
    {     
        //Задаем язык интерфейся
        if (inLang == "")
            inLang = GetSystemLanguage();
        Lang = StrToLang(inLang);
        CurrentLanguage = inLang;
        //Парсим файл
        //ParseXmlFile();
        ParseCSVFile();

        // Созраняем выбранный язык
       // if (saveLang)
          //  MApplication.instance.gameManager.language = inLang;
        
        // Генерируем событие о смене языка
       // Events.instance.Raise(new EventSetLang());
    }

    public static string GetSystemLanguage()
    {
        SystemLanguage lang = Application.systemLanguage;
        switch (lang)
        {
            case SystemLanguage.Russian: return "RU";
            case SystemLanguage.Japanese: return "JP";
            case SystemLanguage.Thai: return "TH";
            default: return "EN";
        }
    }

    LocalizationLanguage StrToLang(string inLan)
    {
        switch (inLan)
        {
            case "RU": return LocalizationLanguage.RU;
            case "JP": return LocalizationLanguage.JP;
            case "TH": return LocalizationLanguage.TH;
            default: return LocalizationLanguage.EN;
        }
    }
   
    /*
    private void ParseXmlFile()
    {
        if (_dictionary != null)
            _dictionary.Clear();
        else
            _dictionary = new Dictionary<string, string>();

        XmlDocument xmlDoc = new XmlDocument();
        TextAsset xmlAsset = Resources.Load<TextAsset>("xml/Localisation");

        if (xmlAsset)
            xmlDoc.LoadXml(xmlAsset.text);

        XmlNodeList dataList = xmlDoc.GetElementsByTagName("words");

        foreach (XmlNode item in dataList)
        {
            XmlNodeList itemContent = item.ChildNodes;

            string wordCode = item.Attributes["code"].Value;
            foreach (XmlNode itemItens in itemContent)
            {
                if (itemItens.Name == Lang.ToString())
                {
                    string wordValue = itemItens.InnerText;
                    DictionaryAdd(wordCode, wordValue);
                }
            }
        }

        Resources.UnloadUnusedAssets();
    }
*/
    
    private void ParseCSVFile()
    {
        //Очищаем словарь
        if (_dictionary != null)
            _dictionary.Clear();
        else
            _dictionary = new Dictionary<string, string>();

        // Загружаем и парсим файл
        TextAsset loadtext = Resources.Load<TextAsset>("xml/Localization");
        string[,] grid = CSVReader.SplitCsvGrid(loadtext.text);

        AddEnglishDictionary(grid);
        // находим столбец с выбранным языком
        int langRow = 1;
        for (int x = 1; x < grid.GetUpperBound(0); x++)
            if (grid[x, 0] == Lang.ToString())
            {
                langRow = x;
                break;
            }

        // Заносим этот столбец в словарь
        for (int y = 1; y < grid.GetUpperBound(1); y++)
        {
            string wordCode = grid[0, y];
            string wordValue = grid[langRow, y];

            if (wordValue == "")
            {
                wordValue = grid[1, y];
                if (wordValue == "")               
                    wordValue = wordCode;   
            }
            
            DictionaryAdd(wordCode, wordValue);
        }

        Resources.UnloadUnusedAssets();
    }

    void DictionaryAdd(string wordCode, string wordValue)
    {
        if (!_dictionary.ContainsKey(wordCode))
            _dictionary.Add(wordCode, wordValue);
       // else MyDebug.LogError("Dictionary ContainsKey " + wordCode + " " + wordValue);
    }

    void AddEnglishDictionary(string[,] grid)
    {
        if (_englishdictionary != null)
            _englishdictionary.Clear();
        else
            _englishdictionary = new Dictionary<string, string>();
        // находим столбец с выбранным языком
        int langRow = 1;
        for (int x = 1; x < grid.GetUpperBound(0); x++)
            if (grid[x, 0] == "EN")
            {
                langRow = x;
                break;
            }

        // Заносим этот столбец в словарь
        for (int y = 1; y < grid.GetUpperBound(1); y++)
        {
            string wordCode = grid[0, y];
            string wordValue = grid[langRow, y];

            if (wordValue == "")
            {
                wordValue = grid[1, y];
                if (wordValue == "")
                    wordValue = wordCode;
            }

            if (!_englishdictionary.ContainsKey(wordCode))
                _englishdictionary.Add(wordCode, wordValue);
        }
    }
    #region GetText
    public static string GetText(string key)
    {
      return instance.getTextByKey(key);
    }

    public string getTextByKey(string key)
    {
        if ((_dictionary != null) && (_dictionary.ContainsKey(key)))
        {
            if(!_dictionary[key].Contains("#"))
                return _dictionary[key];
        }
        if(_englishdictionary!=null && _englishdictionary.ContainsKey(key))
            return _englishdictionary[key];

        return key;
    }
    #endregion

    public void PrintDict()
    {
        foreach (string key in _dictionary.Keys)
        {
            Debug.Log(key + "  " + _dictionary[key]);
        }
    }

}
