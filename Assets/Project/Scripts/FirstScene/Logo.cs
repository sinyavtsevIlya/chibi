using UnityEngine;
using UnityEngine.SceneManagement;

public class Logo : MonoBehaviour
{
    const float SHOW_DELAY = 3;

    public GameObject logoAndorid;
    public string NextScene = "Login";

    void Start()
    {
        //GFM.GDPR.GDPRCanvas.instance.Init();
        Invoke("LoadScene", SHOW_DELAY);
    }

    private void LoadScene()
    {
        //GFM.GDPR.GDPRCanvas.instance.Show(false, () =>
        //{
            SceneManager.LoadScene("Login");
        //});
    }

    
}
