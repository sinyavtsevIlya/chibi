﻿using UnityEngine;

public class Constants
{
    public static Vector3 VectorZero => Vector3.zero;
    public static Vector3 VectorHalf => Vector3.one * 0.5f;
    public static Vector3 VectorOne => Vector3.one;

    public static Vector3 Recipe => Vector3.one * 0.7f;



    public static Vector3 CraftScale => Vector3.one * 0.9f;
    public static Vector3 AdminWindow => Vector3.one;
    public static Vector3 NoMoneyWindow => Vector3.one;
    public static Vector3 OffersWindow => Vector3.one * 0.9f;
    public static Vector3 Inventory => Vector3.one * 0.95f;
    public static Vector3 ChestWindow => Vector3.one;
    public static Vector3 BridgeCraftWindow => Vector3.one;
    public static Vector3 BuySlotWindow => Vector3.one;
    public static Vector3 CantOpen => Vector3.one;
    public static Vector3 FirecampWindow => Vector3.one;
    public static Vector3 ChangeWeatherWindow => Vector3.one * 0.95f;
    public static Vector3 BagAfterDead => Vector3.one;
    public static Vector3 QuestWindow => Vector3.one * 0.9f;
    public static Vector3 SettingsWindow => Vector3.one;
    public static Vector3 NoConnection => Vector3.one;
    public static Vector3 Shop => Vector3.one * 0.875f;
    public static Vector3 GemWindow => Vector3.one;

}