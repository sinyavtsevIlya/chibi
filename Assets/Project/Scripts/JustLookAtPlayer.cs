﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustLookAtPlayer : MonoBehaviour {     //поворачивает стрелки в туторе

    public GameManager GOD;

    private void Start()
    {
        GameObject g = GameObject.Find("GameManager");
        if (g)
            GOD = g.GetComponent<GameManager>();

    }
    private void Update()
    {
        if(GOD)
        transform.LookAt(GOD.PlayerCam.transform);
    }

}
