﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

    public GameManager GOD;
    public MenuScript Menu;
    public AudioSource Sound;
    public AudioSource PlayerSound;
    public AudioSource FreezeSound;

    //МУЗЫКА__________________________________________
    [Header("Музыка:")]
    public AudioSource Music;
    AudioClip NextMusic;
    public AudioClip Green, Snow, Sun, Water;

    //ИНТЕРФЕЙС_______________________________________
    [Header("Интерфейс:")]
    public AudioClip Btn;            //нажатие на кнопку
    public AudioClip Slot;            //перетягивание
    public AudioClip Buy;            //покупка
    public AudioClip BuyGem;            //покупка
    public AudioClip OpenChest;      //открываем сундук в инвентаре
    public AudioClip GetItem, GetWeapon;       //надеваем одежду и оружие
    public AudioClip BreakItem;     //предмет сломался
    public AudioClip BadFood;      //испорченная еда
    public AudioClip AfterCraft;    //предмет добавлен после крафта
    public AudioClip Eat;       //съели
    public AudioClip Get;       //взяли
    public AudioClip Delete;       //удалили
    public AudioClip GetLut;   //подобрали лут

    //ИГРОК____________________________________________
    [Header("Игрок:")]
    public AudioClip GreenStep;
    public AudioClip WinterStep;
    public AudioClip WaterStep;
    public AudioClip SandStep;
    public AudioClip WoodStep;
    public AudioClip RockStep;
    AudioClip CurrentStep;
    public AudioClip Hand; //удар рукой
    public AudioClip Cut1, Cut2; //удар топором
    public AudioClip Pick; //удар киркой
    public AudioClip Shovel; //лопата
    public AudioClip GetWater; // набираем воду
    public AudioClip Attack1, Attack2, Attack3, Attack4; //крики
    public AudioClip Climping; //лазанье
    public AudioClip Die; //смерть
    public AudioClip Pain; //боль


    //ВРАГИ___________________________________________________
    [Header("Враги:")]
    public AudioClip WolfAttack1;
    public AudioClip WolfAttack2, WolfDead, WolfAgressive, WolfIdle;
    public AudioClip BeeAttack1, BeeAttack2, BeeDead, BeeAgressive, BeeIdle;
    public AudioClip BearAttack1, BearAttack2, BearDead, BearAgressive, BearIdle;
    public AudioClip PigAttack1, PigAttack2, PigDead, PigAgressive, PigIdle;
    public AudioClip CrabAttack1, CrabAttack2, CrabDead, CrabAgressive;
    public AudioClip ElefantAttack1, ElefantAttack2, ElefantDead, ElefantAgressive, ElefantIdle;
    public AudioClip SpiderAttack1, SpiderAttack2, SpiderDead, SpiderAgressive;
    public AudioClip ScorpionAttack1, ScorpionAttack2, ScorpionDead, ScorpionAgressive;
    public List<AudioSource> AllEnemies = new List<AudioSource>(); //все враги


    //ОКРУЖЕНИЕ____________________________________________
    [Header("Окружение:")]
    public AudioSource Rain;  //дождь
    public AudioSource Freeze;  //замерзание
    public AudioSource[] Rivers; //все реки
    public AudioSource[] Waterfalls; //все водопады
    public AudioClip Build; //звук при установке здания

    public List<AudioSource> AllFirecamps = new List<AudioSource>(); //все костры
    public AudioSource BrokenTree1, BrokenTree2; //звук падения дерева
    public AudioSource BrokenRock1, BrokenRock2; //звук падения скалы


    public List<AudioSource[]> AllBirds = new List<AudioSource[]>(); //все птицы

    public AudioClip Portal; //звук портала

    //РЫБАЛКА
    [Header("Рыбалка:")]
    public GameObject FishingSound;
    AudioSource FishAudio;
    public AudioClip FishingStart;
    public AudioClip FishingProcess;
    public AudioClip FishingFail;
    public AudioClip FishingSucsess;

    //ПОГОДА
    [Header("Погода:")]
    public AudioSource WeatherSound;
    public AudioClip StartWeather;
    public AudioClip ProcessWeather;
    public AudioClip StopWeather;

    //ЗДАНИЯ
    [Header("Здания:")]
    public AudioClip Windmill;
    public AudioClip Stove;
    public AudioClip Smithy;
    public AudioClip Mannequie;
    public AudioClip Chest;
    public AudioClip IceChest;

    //КВЕСТЫ_______________________________________
    [Header("Квесты:")]
    public AudioClip NewQuest;            //новый квест
    public AudioClip StageComplete;            //этап завершен
    public AudioClip QuestComplete;            //квест завершен

    public void InitIsland(Transform Island)  //инициализируем птиц для острова
    {
        Transform t = Island.Find("Birds");
        if(t!=null)
        {
            AudioSource[] newBirds = new AudioSource[t.childCount];
            for(int i=0; i<newBirds.Length;i++)
            {
                newBirds[i] = t.GetChild(i).GetComponent<AudioSource>();
            }
            AllBirds.Add(newBirds);
            ChangeSoundVolume();
        }
    }

    public void Init()
    {
        ChangeMale(); //устанавливаем крики
        FishAudio = FishingSound.GetComponent<AudioSource>();
        if(!GOD.IsTutorial)
            StartCoroutine(BirdController());
    }

    public void ChangeMale()
    {
        if(GOD.OurPlayer == "Girl")
        {
            Attack1 = Resources.Load<AudioClip>("Player/AttackGirl1");
            Attack2 = Resources.Load<AudioClip>("Player/AttackGirl2");
            Attack3 = Resources.Load<AudioClip>("Player/AttackGirl3");
            Attack4 = Resources.Load<AudioClip>("Player/AttackGirl4");
            Pain = Resources.Load<AudioClip>("Player/PainGirl");
        }
        else
        {
            Attack1 = Resources.Load<AudioClip>("Player/AttackBoy1");
            Attack2 = Resources.Load<AudioClip>("Player/AttackBoy2");
            Attack3 = Resources.Load<AudioClip>("Player/AttackBoy3");
            Attack4 = Resources.Load<AudioClip>("Player/AttackBoy4");
            Pain = Resources.Load<AudioClip>("Player/PainBoy");
        }
        Resources.UnloadUnusedAssets();
    }

    public void ChangeMusicVolume()  //меняем громкость музыки в настройках
    {
        GOD.Settings.MusicSlider.value = GOD.Settings.MusicVolume;
        Music.volume = GOD.Settings.MusicVolume;
    }
    public void ChangeSoundVolume()  //меняем громкость звуков в настройках
    {
        float volume = GOD.Settings.SoundVolume;
        GOD.Settings.SoundSlider.value = volume;
        if(Sound)
            Sound.volume = volume;
        if(PlayerSound)
            PlayerSound.volume = volume;
        if(Rain)
            Rain.volume = volume;
        if(FishAudio)
            FishAudio.volume = volume;
        if(WeatherSound)
            WeatherSound.volume = volume;
        if(BrokenTree1)
            BrokenTree1.volume = volume;
        if (BrokenTree2)
            BrokenTree2.volume = volume;
        if (BrokenRock1)
            BrokenRock1.volume = volume;
        if (BrokenRock2)
            BrokenRock2.volume = volume;
        
        for (int i = 0; i > AllFirecamps.Count; i++)  //костры
            AllFirecamps[i].volume = volume;
        
        for (int i = 0; i < AllEnemies.Count; i++)  //враги
            AllEnemies[i].volume = volume;
        
        for (int i = 0; i < AllBirds.Count; i++)  //птицы
        {
            for (int x = 0; x < AllBirds[i].Length; x++)
                AllBirds[i][x].volume = volume;
        }

        for (int i = 0; i < Rivers.Length; i++)  //реки
        {
            if(Rivers[i].volume>volume)
                Rivers[i].volume = volume;
            if (volume > 0 && Rivers[i].enabled)
                Rivers[i].volume = volume;
        }

        for (int i = 0; i < Waterfalls.Length; i++)  //водопады
            Waterfalls[i].volume = volume;

        if (Freeze)
            Freeze.volume = volume;
    }

    public void SetMusic()  // устанавливаем музыку при старте
    {
        if (GOD.EnviromentContr.CurrentIsland.Contains("Winter"))
            Music.clip = Snow;
        else if (GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
            Music.clip = Sun;
        else if (GOD.EnviromentContr.CurrentIsland.Contains("Water"))
            Music.clip = Water;
        else
            Music.clip = Green;
        Music.Play();
        SetPlayerStep();
    }

    public void ChangeMusic()  // смена музыкой
    {
        if (NextMusic != null)
        {
            StopAllCoroutines();
            StartCoroutine(BirdController());
        }

            if (GOD.EnviromentContr.CurrentIsland.Contains("Winter"))
                NextMusic = Snow;
            else if (GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
                NextMusic = Sun;
            else if (GOD.EnviromentContr.CurrentIsland.Contains("Water"))
                NextMusic = Water;
            else
                NextMusic = Green;

            if (Music.clip != NextMusic)
                StartCoroutine(ChangeMusicForTime());
            else
                NextMusic = null;


    }

    IEnumerator ChangeMusicForTime()
    {
       float speed = Music.volume / 10; 
       while(Music.volume> 0)
        {
            Music.volume -= speed;
            yield return new WaitForSeconds(1);
        }
        Music.volume = 0;
        Music.clip = NextMusic;
        Music.Play();
        while (Music.volume < GOD.Settings.MusicVolume)
        {
            Music.volume += speed;
            yield return new WaitForSeconds(1);
        }
        NextMusic = null;
    }


    public void SetPlayer(int x)
    {
        AudioClip c;
        if (GOD.IsTutorial)
        {
            if (x == 3)
                c = WaterStep;
            else if (x == 4)
                c = Climping;
            else
                c = CurrentStep;
        }
        else
        {
            if (x == 1)
                c = CurrentStep;
            else if (x == 2)
                c = WoodStep;
            else if (x == 3)
                c = WaterStep;
            else if (x == 4)
                c = Climping;
            else
                c = RockStep;
        }

        if (c != PlayerSound.clip)
        {
            PlayerSound.clip = c;
            PlayerSound.Play();
        }
    }

    public void SetPlayerStep()  //шаги
    {
        if (GOD.IsTutorial)
        {
            if(GOD.EnviromentContr.CTut.CurrentState == "Snow")
                CurrentStep = WinterStep;
            else
                CurrentStep = GreenStep;
        }
        else
        {
            if (GOD.EnviromentContr.CurrentIsland.Contains("Winter"))
            {
                if (GOD.EnviromentContr.CSnow.CurrentState == "Sun")
                    CurrentStep = WaterStep;
                else
                    CurrentStep = WinterStep;
            }
            else if (GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
            {
                if (GOD.EnviromentContr.CSand.CurrentState == "Snow")
                    CurrentStep = WinterStep;
                else if (GOD.EnviromentContr.CSand.CurrentState == "Water")
                    CurrentStep = WaterStep;
                else
                    CurrentStep = SandStep;
            }
            else if (GOD.EnviromentContr.CurrentIsland.Contains("Water"))
            {
                if (GOD.EnviromentContr.CWater.CurrentState == "Snow")
                    CurrentStep = WinterStep;
                else if (GOD.EnviromentContr.CWater.CurrentState == "Sun")
                    CurrentStep = GreenStep;
                else
                    CurrentStep = WaterStep;
            }
            else
                CurrentStep = GreenStep;
        }
    }

    public void Button() //нажатие на кнопку
    {
       Sound.PlayOneShot(Btn);
    }

    public void BuyGemSound() //нажатие на кнопку
    {
        Sound.PlayOneShot(BuyGem);
    }

    IEnumerator BirdController()  //проигрываем крики птиц
    {
        AudioSource[] a = AllBirds[GOD.EnviromentContr.CurrentIslandNumber];
        if (a.Length>0)
        {
            int x = Random.Range(0, a.Length);
            a[x].Play();
        }
        yield return new WaitForSeconds(20);
        StartCoroutine(BirdController());
    }

    public void SetEnemiesSounds(Enemy e)
    {
        AllEnemies.Add(e.EnemySound);
        if(e.EnemyWalk)
            AllEnemies.Add(e.EnemyWalk);
        ChangeSoundVolume();

        switch (e.gameObject.name)
        {
            case "Wolf":
                e.Attack1 = WolfAttack1;
                e.Attack2 = WolfAttack2;
                e.Attack3 = WolfAttack1;
                e.Death = WolfDead;
                e.Angry = WolfAgressive;
                e.Idle = WolfIdle;
                break;
            case "Ozz":
                e.Attack1 = BeeAttack1;
                e.Attack2 = BeeAttack2;
                e.Attack3 = BeeAttack1;
                e.Death = BeeDead;
                e.Angry = BeeAgressive;
                e.Idle = BeeIdle;
                break;
            case "Bear":
                e.Attack1 = BearAttack1;
                e.Attack2 = BearAttack2;
                e.Attack3 = BearAttack1;
                e.Death = BearDead;
                e.Angry = BearAgressive;
                e.Idle = BearIdle;
                break;
            case "Pig":
                e.Attack1 = PigAttack1;
                e.Attack2 = PigAttack2;
                e.Attack3 = PigAttack1;
                e.Death = PigDead;
                e.Angry = PigAgressive;
                e.Idle = PigIdle;
                break;
            case "Crab":
                e.Attack1 = CrabAttack1;
                e.Attack2 = CrabAttack1;
                e.Attack3 = CrabAttack2;
                e.Death = CrabDead;
                e.Angry = CrabAgressive;
                break;
            case "Elefant":
                e.Attack1 = ElefantAttack1;
                e.Attack2 = ElefantAttack1;
                e.Attack3 = ElefantAttack2;
                e.Death = ElefantDead;
                e.Angry = ElefantAgressive;
                e.Idle = ElefantIdle;
                break;
            case "Spider":
                e.Attack1 = SpiderAttack1;
                e.Attack2 = SpiderAttack1;
                e.Attack3 = SpiderAttack2;
                e.Death = SpiderDead;
                e.Angry = SpiderAgressive;
                break;
            case "Scorpion":
                e.Attack1 = ScorpionAttack1;
                e.Attack2 = ScorpionAttack1;
                e.Attack3 = ScorpionAttack2;
                e.Death = ScorpionDead;
                e.Angry = ScorpionAgressive;
                break;
        }
    }

    //ЗВУКИ РЕК
    public void GetRiver()
    {
        if (GOD.EnviromentContr.CurrentIsland.Contains("Water"))
        {
            if (Rivers[0].clip == null)
            {
                AudioClip a = Resources.Load<AudioClip>("Enviroment/River");
                for (int i = 0; i < Rivers.Length; i++)
                {
                    Rivers[i].clip = a;
                    Rivers[i].enabled = false;
                    Rivers[i].enabled = true;
                }
            }
        }
        else
        {
            if(Rivers[0].clip != null)
            {
                for (int i = 0; i < Rivers.Length; i++)
                {
                    Rivers[i].clip = null;
                }
                //Resources.UnloadUnusedAssets();
            }
        }

    }

    //ЗВУКИ ВОДОПАДОВ
    public void GetWaterFall()
    {
        if (GOD.EnviromentContr.CurrentIsland.Contains("Water") || GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
        {
            if (Waterfalls[0].clip == null)
            {
                AudioClip a = Resources.Load<AudioClip>("Enviroment/Waterfall");
                for (int i = 0; i < Waterfalls.Length; i++)
                {
                    Waterfalls[i].clip = a;
                    Waterfalls[i].enabled = false;
                    Waterfalls[i].enabled = true;
                }
            }
        }
        else
        {
            if (Waterfalls[0].clip != null)
            {
                for (int i = 0; i < Waterfalls.Length; i++)
                {
                    Waterfalls[i].clip = null;
                }
               // Resources.UnloadUnusedAssets();
            }
        }

    }
}
