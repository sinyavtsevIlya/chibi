﻿//using System;
//using UnityEngine;
//#if !NO_GPGS
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;
//using GooglePlayGames.BasicApi.SavedGame;

//public class GPGSSave
//{
//    static GPGSSave _instance;
//    public static GPGSSave instance
//    {
//        get
//        {
//            if (_instance == null)
//            {
//                _instance = new GPGSSave();
//            }
//            return _instance;
//        }
//    }

//    public byte[] saveData;
//    public byte[] saveIcon;
//    public TimeSpan playedTime;

//    public Action<SaveLoadClass> callbackData;
//    public Action<string> callbackURL;
//    public Action<string> callbackFileName;

//    public static void SaveToCloud(string filename, byte[] saveData, byte[] saveIcon, TimeSpan playedTime)
//    {
//        instance.saveData = saveData;
//        instance.saveIcon = saveIcon;
//        instance.playedTime = playedTime;

//        Debug.Log("Saving progress to the cloud... " + filename);
//        ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(filename,
//            DataSource.ReadCacheOrNetwork,
//            ConflictResolutionStrategy.UseLongestPlaytime,
//            SavedGameOpened);
//    }

//    public static void LoadFromCloud(string saveName, Action<SaveLoadClass> callbackData, Action<string> callbackURL)
//    {
//        instance.callbackData = callbackData;
//        instance.callbackURL = callbackURL;

//        ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(saveName,
//            DataSource.ReadCacheOrNetwork,
//            ConflictResolutionStrategy.UseLongestPlaytime,
//            LoadGameOpened);
//    }

//    public static void SelectSlotToSave(Action<string> callbackFileName)
//    {
//        instance.callbackURL = callbackFileName;

//        ((PlayGamesPlatform)Social.Active).SavedGame.ShowSelectSavedGameUI("Select slot to save",
//                        4, true, true, SavedGameSelected);
//    }

//    public static void SelectSlotToLoad(Action<string> callbackFileName)
//    {
//        instance.callbackURL = callbackFileName;

//        ((PlayGamesPlatform)Social.Active).SavedGame.ShowSelectSavedGameUI("Select slot to save",
//                        4, false, true, LoadGameSelected);
//    }

//    public static void SavedGameSelected(SelectUIStatus status, ISavedGameMetadata game)
//    {
//        if (status == SelectUIStatus.SavedGameSelected)
//        {
//            string filename = game.Filename;

//            if (filename == null || filename.Length == 0)
//                filename = SaveManager.GetSlotName(SaveManager.CurrentSlot) + "_" + SaveManager.GetDataTimeToString();

//            instance.callbackURL(filename);
//        }
//        else
//        {
//            Debug.LogWarning("Error selecting save game: " + status);
//            instance.callbackURL(null);
//        }
//    }

//    private static void SavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
//    {
//        if (status == SavedGameRequestStatus.Success)
//        {
//            Debug.Log("Saving to " + game);

//            SavedGameMetadataUpdate.Builder builder = new
//            SavedGameMetadataUpdate.Builder()
//                .WithUpdatedPlayedTime(instance.playedTime)
//                .WithUpdatedDescription(game.Filename + " at " + DateTime.Now);

//            if (instance.saveIcon != null)
//            {
//                Debug.Log("Save image of len " + instance.saveIcon.Length);
//                builder = builder.WithUpdatedPngCoverImage(instance.saveIcon);
//            }
//            else
//            {
//                Debug.Log("No image avail");
//            }

//            SavedGameMetadataUpdate updatedMetadata = builder.Build();
//            ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(game, updatedMetadata, instance.saveData, SavedGameWritten);
//        }
//        else
//        {
//            Debug.LogWarning("Error opening game: " + status);
//        }
//    }

//    private static void SavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
//    {
//        if (status == SavedGameRequestStatus.Success)
//        {
//            Debug.Log("Game " + game.Description + " written");
//        }
//        else
//        {
//            Debug.LogWarning("Error saving game: " + status);
//        }
//    }

//    public static void LoadGameSelected(SelectUIStatus status, ISavedGameMetadata game)
//    {
//        if (status == SelectUIStatus.SavedGameSelected)
//        {
//            instance.callbackURL(game.Filename);
//        }
//        else
//        {
//            Debug.LogWarning("Error selecting save game: " + status);
//            instance.callbackURL(null);
//        }
//    }


//    private static void LoadGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
//    {
//        if (status == SavedGameRequestStatus.Success)
//        {
//            instance.callbackURL(game.CoverImageURL);
//            ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(game, LoadGameLoaded);
//        }
//        else
//        {
//            instance.callbackData(null);
//            instance.callbackURL(null);
//            Debug.LogWarning("Error opening game: " + status);
//        }
//    }

//    private static void LoadGameLoaded(SavedGameRequestStatus status, byte[] data)
//    {
//        if (status == SavedGameRequestStatus.Success)
//        {
//            instance.callbackData(SaveManager.ByteArrayObjectTo(data));
//        }
//        else
//        {            
//            Debug.LogWarning("Error reading game: " + status);
//            instance.callbackData(null);
//        }

//    }
//}
//#endif