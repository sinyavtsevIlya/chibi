﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class IndexNomination : MonoBehaviour {


    enum IndexType
    {
        Resources = 1,
        Animals = 2,
    }
    enum ResourceType
    {
        Trees =1,
        Stones=2,
        Bush =3,
        Flowers =4,
        Ore =5,
        SnowTrees = 6,
        IceRocks = 7,
        Snow = 8,
        Under = 888,
        NonType =999
    }
    enum EnemyType
    {
        Wolf = 1,
        Ozz = 2,
        Bear = 3,
        Pig = 4,
        Elefant = 5,
        Crab = 6,
        Spider = 7,
        Scorpion = 8,
        NonType = 9
    }

    [ContextMenu("IndexNomaination")]
    public void SetIndex()//выставляем индекс
    {
        for(int i=0; i<transform.childCount;i++)//перебираем острова
        {
            int islandNumber = transform.GetChild(i).GetComponent<IslandScript>().IslandNumber +1; //номер острова
            Transform world = transform.GetChild(i).Find("World");
            if(world) //ресурсы
            {
                for(int x=0; x<world.childCount; x++)
                {
                    Transform resourceParent = world.GetChild(x);
                    ResourceType type;          //определяем тип ресурсов
                    if (resourceParent.name.Contains("Under"))
                        type = ResourceType.Under;
                    else
                    {
                        try
                        {
                            type = (ResourceType)Enum.Parse(typeof(ResourceType), resourceParent.name);
                            if (!Enum.IsDefined(typeof(ResourceType), type))
                                type = ResourceType.NonType;
                        }
                        catch (ArgumentException)
                        {
                            type = ResourceType.NonType;
                        }
                    }

                    for(int y=0; y<resourceParent.childCount; y++)//считаем сами ресурсы
                    {
                        Index index = resourceParent.GetChild(y).GetComponent<Index>();
                        if (islandNumber == 2)
                            Debug.Log((islandNumber * 1000000000) + " "+ (1 * 10000000) + " " + (int)type * 10000+" "+(y+1));
                        if (index)
                            index.MyIndex = islandNumber * 1000000000 + 1 * 10000000 + (int)type * 10000 + (y+1); //номер острова + номер типа + номер типа ресурса + номер ресурса
                    }
                }
            }

            Transform enemy = transform.GetChild(i).Find("Enemies");
            if (enemy) //враги
            {
                for (int x = 0; x < enemy.childCount; x++)
                {
                    Transform enemyParent = enemy.GetChild(x);
                    EnemyType type;          //определяем тип врагов
                    try
                    {
                        type = (EnemyType)Enum.Parse(typeof(EnemyType), enemyParent.name);
                        if (!Enum.IsDefined(typeof(EnemyType), type))
                            type = EnemyType.NonType;
                    }
                    catch (ArgumentException)
                    {
                        type = EnemyType.NonType;
                    }

                    for (int y = 0; y < enemyParent.childCount; y++)//считаем сами ресурсы
                    {
                        Index index = enemyParent.GetChild(y).GetComponent<Index>();
                        if (index)
                            index.MyIndex = islandNumber * 1000000000 + 2 * 10000000 + (int)type * 10000 + (y + 1); //номер острова + номер типа + номер типа ресурса + номер ресурса
                    }
                }
            }
        }
    }
}
