﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveSlotScript : MonoBehaviour {
    public int id;
    public Image MyIcon;
    public Text Load; //текст загрузить или перезаписать
    public Text Time; //время создания игры
    public Text CurrentDay; //день в игре
    public GameObject FromCloud;//кнопка загрузить с облака
    public Text LoadFromCloud;//загрузить с облака текст
    public Text NewGame; //надпись - новая игра
    public GameObject LoadCircle; //вращающийся кружок
    public MenuScript Menu;


    public void Init()
    {
        Load.text = Localization.instance.getTextByKey("#Menu4");
        LoadFromCloud.text = Localization.instance.getTextByKey("#Menu5");
    }

    public void SetMyIcon(Sprite s)//задаем иконку сейва
    {
        MyIcon.sprite = s;
    }


//    public void LoadfromCloud()//загружаем с облака
//    {
//#if !NO_GPGS
//        LoadCircle.SetActive(true);
//        SaveManager.SelectSlotToLoad(SelectSlotToLoadCallBack);
//#endif
//    }

//    void SelectSlotToLoadCallBack(string fileName)
//    {
//        if ((fileName != null) && (fileName.Length > 0))
//        {
//            Debug.Log("SelectSlotToLoadCallBack " + fileName);
//            SaveManager.SetCloudFileName(id, fileName);
//            SaveManager.LoadFromCloud(id, LoadFromCloudDataCallBack, LoadFromCloudDataURLCallBack);
//        }
//        else
//            LoadCircle.SetActive(false);
//    }

    //void LoadFromCloudDataCallBack(SaveLoadClass loadData)
    //{
    //    SaveManager.AllSaveData[id] = loadData;
    //}

    //void LoadFromCloudDataURLCallBack(string loadDataURL)
    //{
    //    if ((loadDataURL == null) || (loadDataURL == ""))
    //    {
    //        LoadCircle.SetActive(false);
    //        return;
    //    }
    //    StartCoroutine(LoadIcon(loadDataURL));
    //}
    IEnumerator LoadIcon(string url)
    {
        LoadCircle.SetActive(false);
        Debug.Log("LoadIcon " + url);
        WWW www = new WWW(url);
        yield return www;
        Sprite icon = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
        SetMyIcon(icon);
        Time.text = SaveManager.AllSaveData[id].SaveDate;
        MyIcon.transform.parent.gameObject.SetActive(true);
        Menu.AllSaves[id] = true;
        PlayerPrefsX.SetBoolArray("ChibiAllSaves", Menu.AllSaves);
    }
}
