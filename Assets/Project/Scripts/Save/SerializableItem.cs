﻿using UnityEngine;
using System;

[Serializable]
public struct SerializableItem
{
    public string Name;
    public int Count;
    public int Durability;
    public int Validity;
    public int WaterCount;

    public int OnBag;
    public string OnPlayer;
    public int OnGame;

    public int ChestPlace;

    public SerializableItem(string n, int c, int d, int v, int w, int onbag, string onplayer, int ongame, int chestplace)
    {
        Name = n;
        Count = c;
        Durability = d;
        Validity = v;
        WaterCount = w;
        OnBag = onbag;
        OnPlayer = onplayer;
        OnGame = ongame;
        ChestPlace = chestplace;
    }

}

