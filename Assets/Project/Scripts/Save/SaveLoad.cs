﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class SaveLoad : MonoBehaviour {

    public GameManager GOD;
    public int CurrentSlot = 0;
    public bool IsNewGame = false;
    public Texture2D CurentScreen;

    int SaveTimer = -1;
    int SaveTime = 60;

    public bool IsMenu = false; //выходим ли сейчас в меню

    bool needToActivate = false; //нужно ли после скрина включать окно

    IEnumerator WaitSave()
    {
        while (SaveTimer < SaveTime)
        {
            SaveTimer++;
            yield return new WaitForSeconds(1);
        }
        SaveGame();
        SaveTimer = 0;
        StartCoroutine(WaitSave());
    }

    public void SaveGame(bool IsWait=true)
    {
        if (!GOD.IsTutorial)
        {
            if (!IsWait)
                SaveTimer = 30;
            if (SaveTimer >= 30)
            {
                //print("ALL SAVE ");
                bool[] AllSaves = PlayerPrefsX.GetBoolArray("ChibiAllSaves");
                if (AllSaves.Length > 0)
                {
                    if (GOD.Settings.SettingsWindow.activeSelf)
                    {
                        needToActivate = true;
                        GOD.Settings.SettingsWindow.SetActive(false);
                    }
                    else
                        needToActivate = false;
                    StartCoroutine(TakeScreanShot(MakeScreenShot, 2)); //Делаес скриншот
                    AllSaves[CurrentSlot] = true;
                    PlayerPrefsX.SetBoolArray("ChibiAllSaves", AllSaves);
                   
                }
                SaveTimer = 0;
                SaveTime = 60;
            }
            else
                SaveTime = 30;
        }
    }

    public void SaveOnDisk()
    {
        SaveLoadClass newSave = new SaveLoadClass();
        newSave.SaveDate = DateTime.Now.ToString("dd/MM/yyyy   HH:mm");
        // ВРЕМЯ_____________________________________________
        newSave.Year = GOD.TG.IYear;
        newSave.Month = GOD.TG.IMonth;
        newSave.Day = GOD.TG.IDay;
        newSave.Hour = GOD.TG.IHour;
        newSave.Minute = GOD.TG.IMinute;
        newSave.AllDays = GOD.TG.AllDays;
        // ПОЛОЖЕНИЕ персонажа_______________________________
        newSave.PlayerPosition = GOD.Player.transform.position;
        newSave.PlayerRotation = GOD.Player.transform.rotation;
        //ПАРАМЕТРЫ ПЕРСОНАЖА________________________________
        newSave.playerHP = GOD.PlayerContr.HP;
        newSave.playerRealMaxHP = GOD.PlayerContr.realmaxHP;
        newSave.playerEAT = GOD.PlayerContr.EAT;
        newSave.Male = GOD.OurPlayer;
        //БАФЫ_______________________________________________
        if (GOD.BafContr.IsSpeed)  //баф скорости
        {
            newSave.IsSpeed = true;
            newSave.SpeedTimer = GOD.BafContr.SpeedTimer;
        }
        if (GOD.BafContr.IsRegen)  //баф регенерации здоровья
        {
            newSave.IsRegen = true;
            newSave.RegenTimer = GOD.BafContr.RegenTimer;
        }
        if (GOD.BafContr.IsSatiety)  //баф сытости
        {
            newSave.IsSatiety = true;
            newSave.SatietyTimer = GOD.BafContr.SatietyTimer;
        }
        if (GOD.BafContr.IsCraft)  //баф увелечения скорости крафта
        {
            newSave.IsCraft = true;
            newSave.CraftTimer = GOD.BafContr.CraftTimer;
        }
        //ПОКУПКИ____________________________________________
        newSave.BoyHairs = GOD.PlayerContr.BoyHairs;
        newSave.GirlHairs = GOD.PlayerContr.GirlHairs;

        newSave.HairColor = GOD.PlayerContr.Colors;

        newSave.BoySkins = GOD.PlayerContr.BoySkins;
        newSave.GirlSkins = GOD.PlayerContr.GirlSkins;
        //СЛОТЫ В ИНВЕНТАРЕ_________________________________________________________________
        newSave.SlotCount = GOD.InventoryContr.SlotCount;// количество слотов
        newSave.ResSlot = GOD.BuySlotScript.ResSlot;// прогресс покупок слотов за ресурсы
        newSave.GoldSlot = GOD.BuySlotScript.GoldSlot;// прогресс покупок слотов за золото
        //ПОСТРОЙКИ________________________________________________________________________
        SaveBuildings(newSave);
        SaveFirecamp(newSave);
        //КРАФТ_________________________________________________________________
        newSave.InCraftTimer = GOD.CraftContr.CraftTimer;
        int buildCount = GOD.BuildingS.Buildings.transform.childCount;
        List<bool> build = new List<bool>();
        List<CraftItem> helpCraft = new List<CraftItem>();
        for (int i = 0; i < buildCount; i++)
        {
            Transform t = GOD.BuildingS.Buildings.transform.GetChild(i);
            if (t.CompareTag("Build"))
            {
                CraftBuilding c = GOD.BuildingS.Buildings.transform.GetChild(i).GetComponent<CraftBuilding>();
                if (c)
                {
                    if (c.thisCraftItem.IsExist)
                    {
                        build.Add(true);
                        helpCraft.Add(GOD.BuildingS.Buildings.transform.GetChild(i).GetComponent<CraftBuilding>().thisCraftItem);
                    }
                    else
                        build.Add(false);
                }
            }
        }
        newSave.CraftBuildings = new bool[build.Count];
        newSave.CraftItems = new CraftItem[helpCraft.Count];
        for(int i=0; i<build.Count;i++)
            newSave.CraftBuildings[i] = build[i];
        for (int i = 0; i < helpCraft.Count; i++)
            newSave.CraftItems[i] = helpCraft[i];

        if (GOD.CraftContr.Usial.thisCraftItem.IsExist)
        {
            newSave.UsialCraftItem = GOD.CraftContr.Usial.thisCraftItem;
        }

        //ИНВЕНТАРЬ______________________________________________________________________
        newSave.InventoryItems = SaveItems(GOD.InventoryContr.AllIcons, "inventory");
        //дополнительные шоповские иконки
        string[] IconsDopName = new string[GOD.InventoryContr.DopIcons.Count];
        int[] IconsDopCount = new int[GOD.InventoryContr.DopIcons.Count];
        for (int i = 0; i < GOD.InventoryContr.DopIcons.Count; i++)
        {
                IconsDopName[i] = GOD.InventoryContr.DopIcons[i].thisName;
                IconsDopCount[i] = GOD.InventoryContr.DopIcons[i].ResourceCount;
        }
        newSave.InventoryDopIconsName = IconsDopName;
        newSave.InventoryDopIconsCount = IconsDopCount;
        //сундуки
        newSave.AllChestItems = new List<SerializableItem>[GOD.InventoryContr.AllChest.Count];
        for (int i = 0; i < GOD.InventoryContr.AllChest.Count; i++)//если есть сундуки, запоминаем их
        {
            newSave.AllChestItems[i] = SaveItems(GOD.InventoryContr.AllChest[i].MyIcons, "chest");
        }
        //сумки после смерти
        SaveDieBags(newSave);
        // СОСТОЯНИЕ МИРА________________________________________________________________________________________________
        //прогресс строительства моста
        SaveBridge(newSave);
        //активны ли объекты
        SaveWorld(newSave);
        //животные
        SaveEnemies(newSave);
        //погода на островах
        SaveWeather(newSave);
        newSave.CurrentIsland = GOD.EnviromentContr.CurrentIsland;
        //активированные головоломки
        SavePuzzle(newSave);
        //МОРОЗ______________________________________________
        newSave.Frost = GOD.PlayerContr.Frost;
        if (!GOD.PlayerInter.FrostWindow.material.name.Contains("Default"))
            newSave.FrostWindow = GOD.PlayerInter.FrostWindow.material.GetFloat("_Cutoff");
        //ПРОГРЕСС ИССЛЕДОВАНИЯ ОСТРОВОВ_____________________
        newSave.OpenIslands = GOD.Map.OpenIslands;
        //КВЕСТЫ_______________________________________
        QuestsTemplate[] allQuests = GOD.QuestContr.AllQuests.QuestsTemplate;
        newSave.Quests = new SaveQuestClass[allQuests.Length];
        for(int i=0; i<allQuests.Length; i++)
        {
            SaveQuestClass s = new SaveQuestClass();
            if (allQuests[i].complete)        //если выполнен квест - останавливаемся
                s.IsQuestComplete = true;
            else
            {
                s.IsNowActive = allQuests[i].isactive;
                s.Current_stage = allQuests[i].current_stage_count;//сохраняем текущий этап
                if (s.Current_stage > -1) //если квест активен и текущий этап не завершен
                {
                    s.IsCurrentStageComplete = allQuests[i].stages[allQuests[i].current_stage_count - 1].complete;
                    if (!s.IsCurrentStageComplete)
                    {
                        QuestsDetail[] qd = allQuests[i].stages[s.Current_stage - 1].details;
                        s.Details_current_count = new int[qd.Length];
                        for (int x = 0; x < qd.Length; x++)
                        {
                            s.Details_current_count[x] = qd[x].current_count;
                        }
                    }
                }
                else
                {
                    if (GOD.QuestContr.QuestsTurn.Contains(allQuests[i]))
                        s.IsNowPlank = true;
                }
            }
            newSave.Quests[i] = s;
        }
        //ПРОГРЕСС ТАЙМЕРА ЭССЕНЦИЙ
        if (GOD.PlayerInter.CurrentEssence != "")
        {
            DateTime time = DateTime.Now;
            int FutureHour = time.Hour;
            int FutureMinute = time.Minute + GOD.PlayerInter.EssenceMinutTimer;
            if ((time.Minute + GOD.PlayerInter.EssenceMinutTimer) > 60)
            {
                FutureHour++;
                FutureMinute -= 60;
            }
            newSave.EssenseHour = FutureHour;
            newSave.EssenceMinute = FutureMinute;
            newSave.EssenceName = GOD.PlayerInter.CurrentEssence;
        }
        //ОФФЕРЫ
        SaveOffers();

        SaveManager.Save(CurrentSlot, newSave, CurentScreen);

        if (IsMenu)
            GOD.Settings.NowLoadMenu();
    }



    //РЕСУРСЫ игрока_______________________________________________________________________________________________
    List<SerializableItem> SaveItems(List<InfoScript> Items, string where)
    {
        List<SerializableItem> NewItems = new List<SerializableItem>();
        for (int i=0; i<Items.Count; i++)
        {
            SerializableItem newitem = new SerializableItem();
            //Debug.Log("SAVE " + Items[i].thisName);
            newitem.Name = Items[i].thisName;
            newitem.Count = Items[i].ResourceCount;
            newitem.Durability = Items[i].currentDurability;
            newitem.Validity = Items[i].currentValidity;
            newitem.WaterCount = Items[i].WaterCount;

            newitem.OnBag = -1;
            newitem.OnPlayer = "";
            newitem.OnGame = -1;
            newitem.ChestPlace = -1;
            if (where == "inventory")
            {
                //в сумке
                int Place = -1;
                for (int x = 0; x < GOD.InventoryContr.Bag.transform.childCount; x++)
                {
                    if (Items[i].ParentSlot == GOD.InventoryContr.Bag.transform.GetChild(x))
                    {
                        Place = x;
                        break;
                    }
                }
                newitem.OnBag = Place;
                //на игроке
                if (Items[i].ParentSlot && Items[i].ParentSlot.parent && Items[i].ParentSlot.parent.name == "WindowPlayer")
                    newitem.OnPlayer = Items[i].ParentSlot.name;
                else
                    newitem.OnPlayer = "";
                // место в панели быстрого доступа
                int OnGamePlace = -1;
                if (Items[i].SlotInGame != null)
                {
                    Transform p = Items[i].SlotInGame.transform.parent;
                    for (int x = 0; x < p.childCount; x++)
                    {
                        if (p.GetChild(x) == Items[i].SlotInGame.transform)
                            OnGamePlace = x;
                    }
                }
                newitem.OnGame = OnGamePlace;
            }
            else if(where == "chest")
            {
                newitem.ChestPlace = Items[i].ChestPlace;
            }
            NewItems.Add(newitem);
        }
        return NewItems;              
    }
    //ПОСТРОЙКИ_____________________________________________________________________________________________________
    public void SaveBuildings(SaveLoadClass newSave)
    {
        List<GameObject> save = new List<GameObject>();
        for (int x = 0; x < GOD.BuildingS.Buildings.transform.childCount; x++)
        {
            if (GOD.BuildingS.Buildings.transform.GetChild(x).name != "Firecamp")
                save.Add(GOD.BuildingS.Buildings.transform.GetChild(x).gameObject);
        }
        int BuildingsCount = save.Count;

        List<string> saveObjectName = new List<string>();
        List<Vector3> saveObjectPos = new List<Vector3>();
        List<Quaternion> saveObjectRotat = new List<Quaternion>();
        List<bool> saveObjectChildrenFence = new List<bool>(); //если забор стоит на фундаменте

        for (int x = 0; x < BuildingsCount; x++)
      {
            if (save[x].GetComponent<ObjectScript>())
            {
                saveObjectName.Add(save[x].name);
                saveObjectPos.Add(save[x].transform.position);
                saveObjectRotat.Add(save[x].transform.rotation);
                saveObjectChildrenFence.Add(false);
                if (!save[x].CompareTag("Build"))
                {
                    int c = save[x].transform.childCount;  //сохраняем стены
                    if (c > 0)
                    {
                        for (int i = 0; i < c; i++)
                        {
                            Transform children = save[x].transform.GetChild(i);
                            if (children.name == "CopyStair")
                                break;
                            saveObjectName.Add(children.name);
                            saveObjectPos.Add(children.position);
                            saveObjectRotat.Add(children.rotation);
                            if (children.name.Contains("Fence"))
                            {
                                saveObjectChildrenFence.Add(true);
                            }
                            else
                                saveObjectChildrenFence.Add(false);

                            int newc = children.childCount;  //сохраняем крышу, окна и двери
                            if (newc > 0)
                            {
                                for (int z = 0; z < newc; z++)
                                {
                                    Transform childrenOfWall = children.transform.GetChild(z);
                                    if (childrenOfWall.name != "Options")
                                    {
                                        saveObjectName.Add(childrenOfWall.name);
                                        saveObjectPos.Add(childrenOfWall.position);
                                        saveObjectRotat.Add(childrenOfWall.rotation);
                                        saveObjectChildrenFence.Add(false);

                                        int newchild = childrenOfWall.childCount;  //сохраняем вторую крышу
                                        if (newchild > 0)
                                        {
                                            for (int w = 0; w < newchild; w++)
                                            {
                                                Transform childrenOfWall2 = childrenOfWall.transform.GetChild(w);
                                                saveObjectName.Add(childrenOfWall2.name);
                                                saveObjectPos.Add(childrenOfWall2.position);
                                                saveObjectRotat.Add(childrenOfWall2.rotation);
                                                saveObjectChildrenFence.Add(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

      }

        string[] saveObjectName1 = new string[saveObjectName.Count];
        Vector3[] saveObjectPos1 = new Vector3[saveObjectPos.Count];
        Quaternion[] saveObjectRotat1 = new Quaternion[saveObjectRotat.Count];
        bool[] saveObjectChildrenFence1 = new bool[saveObjectChildrenFence.Count];
       for (int i=0; i< saveObjectName.Count;i++)
        {
            saveObjectName1[i] = saveObjectName[i];
            saveObjectPos1[i] = saveObjectPos[i];
            saveObjectRotat1[i] = saveObjectRotat[i];
            saveObjectChildrenFence1[i] = saveObjectChildrenFence[i];
        }

        newSave.BuildingName = saveObjectName1;
        SerializableVector3[] s1 = new SerializableVector3[saveObjectPos1.Length];
        for (int i = 0; i < s1.Length; i++)
            s1[i] = saveObjectPos1[i];
        newSave.BuildingPosition= s1;
        SerializableQuaternion[] s2 = new SerializableQuaternion[saveObjectRotat1.Length];
        for (int i = 0; i < s2.Length; i++)
            s2[i] = saveObjectRotat1[i];
        newSave.BuildingRotation = s2;
        newSave.BuildingChildrenFence = saveObjectChildrenFence1;

}



    public void SaveFirecamp(SaveLoadClass newSave)
    {
        int FirecampCount = GOD.Audio.AllFirecamps.Count;
        GameObject[] saveObjects = new GameObject[FirecampCount];
        for (int x = 0; x < FirecampCount; x++)
            saveObjects[x] = GOD.Audio.AllFirecamps[x].transform.parent.gameObject;

        SerializableVector3[] saveObjectPos = new SerializableVector3[FirecampCount];
        SerializableQuaternion[] saveObjectRotat = new SerializableQuaternion[FirecampCount];
        int[] FirecampHP = new int[FirecampCount]; // хп костров

        for (int x = 0; x < FirecampCount; x++)
        {
            saveObjectPos[x] = saveObjects[x].transform.position;
            saveObjectRotat[x] = saveObjects[x].transform.rotation;
            FirecampHP[x] = saveObjects[x].GetComponent<FirecampScript>().FireCampHP;
        }
        newSave.FirecampPosition = saveObjectPos;
        newSave.FirecampRotation = saveObjectRotat;
        newSave.FirecampHP = FirecampHP;
    }

    // СОСТОЯНИЕ МИРА________________________________________________________________________________________________
    public void SaveBridge(SaveLoadClass newSave)//прогресс строительства моста
    {
        bool[] BridgeState = new bool[3];
        BridgeState[0] = GOD.ToSnow.IsOpen;
        BridgeState[1] = GOD.ToRain.IsOpen;
        BridgeState[2] = GOD.ToSand.IsOpen;
        newSave.BridgeState = BridgeState;
    }
    public void SaveWorld(SaveLoadClass newSave)//восстанавливающиеся объекты
    {
        newSave.IsRecoveryObject = new int[GOD.AllObjects.Length];
        newSave.IsRecoveryResources = new int[GOD.AllObjects.Length];
        for (int x = 0; x < GOD.AllObjects.Length; x++)
        {
            if (GOD.RecoveryObject.Contains(GOD.AllObjects[x].GetComponent<ObjectScript>()))
            {
                newSave.IsRecoveryObject[x] = GOD.AllObjects[x].GetComponent<ObjectScript>().RecoveryTime;
                newSave.IsRecoveryResources[x] = 0;
            }
            else if (GOD.RecoveryResourses.Contains(GOD.AllObjects[x].GetComponent<ObjectScript>()))
            {
                newSave.IsRecoveryObject[x] = 0;
                newSave.IsRecoveryResources[x] = GOD.AllObjects[x].GetComponent<ObjectScript>().RecoveryTime;
            }
            else
            {
                newSave.IsRecoveryObject[x] = 0;
                newSave.IsRecoveryResources[x] = 0;
            }
        }
    }
    public void SaveEnemies(SaveLoadClass newSave)//сохранение животных
    {
        newSave.IsRecoveryEnemies = new int[GOD.EnviromentContr.AllEnemies.Count];

        for (int i = 0; i < GOD.EnviromentContr.AllEnemies.Count; i++)
        {
            if (GOD.RecoveryEnemy.Contains(GOD.EnviromentContr.AllEnemies[i]))
                newSave.IsRecoveryEnemies[i] = GOD.EnviromentContr.AllEnemies[i].RecoveryTime;
            else
                newSave.IsRecoveryEnemies[i] = 0;
        }
    }
    public void SaveWeather(SaveLoadClass newSave)//погода
    {
        newSave.IslandWaterState = GOD.EnviromentContr.CWater.CurrentState;
        newSave.IslandSnowState = GOD.EnviromentContr.CSnow.CurrentState;
        newSave.IslandSandState = GOD.EnviromentContr.CSand.CurrentState;
        newSave.IslandSandOldState = GOD.EnviromentContr.CSand.OldState;
    }

    public void SavePuzzle(SaveLoadClass newSave)
    {
        int number = -1;
        if (GOD.EnviromentContr.CSnow.IsIceBreak) // если ледяная глыба включена и лежит
        {
            if (GOD.EnviromentContr.CSnow.IceBreak.activeSelf)
                number = 1;
            else if (GOD.EnviromentContr.CSnow.IceBreak2.activeSelf)
                number = 2;
        }
        newSave.IceBreak = number;

        if(GOD.EnviromentContr.CSand.IsTreeDown)
            newSave.BigTree = 1;
    }

    //ЕСЛИ ПОСЛЕ СМЕРТИ_______________________________________________________________________________________________
    public void SaveDieBags(SaveLoadClass newSave)
    {
        newSave.DeadBagPosition = new SerializableVector3[GOD.InventoryContr.AllBags.Count];
        newSave.AllDeadItems = new List<SerializableItem>[GOD.InventoryContr.AllBags.Count];
        if (GOD.InventoryContr.AllBags.Count > 0) //если есть сумки, запоминаем их места
        {
            SerializableVector3[] AllBagsPosition = new SerializableVector3[GOD.InventoryContr.AllBags.Count];
            for (int i = 0; i < GOD.InventoryContr.AllBags.Count; i++)
            {
                AllBagsPosition[i] = GOD.InventoryContr.AllBags[i].transform.position;
                    List<InfoScript> I = GOD.InventoryContr.AllBags[i].GetComponent<AfterDeadBag>().MyIcons;
                    newSave.AllDeadItems[i] = SaveItems(I, "dead");
            }
            newSave.DeadBagPosition = AllBagsPosition;
        }
    }

    //СКРИНШОТ________________________________________________________________________

    public void MakeScreenShot(Texture2D tex)
    {
        CurentScreen = tex;
        SaveOnDisk();
        if (needToActivate)
            GOD.Settings.SettingsWindow.SetActive(true);
    }

    public IEnumerator TakeScreanShot(Action<Texture2D> callback, int resize = 2)  //делаем скриншот
    {
        yield return new WaitForEndOfFrame();
        Texture2D tex1 = new Texture2D(Screen.width, Screen.height);
        tex1.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        tex1.Apply();
        if (resize == 1)
        {
            callback(tex1);
            yield break;
        }
        int adjustedHeight = tex1.height / (resize * resize);
        int adjustedWidth = tex1.width / (resize * resize);
        Texture2D tex = new Texture2D(adjustedWidth, adjustedHeight, TextureFormat.RGB24, false);
        tex.SetPixels(tex1.GetPixels(resize));
        tex.Apply();
        Destroy(tex1);
        callback(tex);
    }

    //НАСТРОЙКИ_____________________________________________________________________________________

      public void SaveSettings()
    {
        PlayerPrefs.SetFloat("ChibiMusicVolume", GOD.Settings.MusicVolume);
        PlayerPrefs.SetFloat("ChibiSoundVolume", GOD.Settings.SoundVolume);
    }


    public void SaveOffers() //сохранение информации об оферах
    {
        if (!GOD.Offers.Offers[0].DeactivateOffer)
        {
            Vector3 v = new Vector3(GOD.Offers.Offers[0].Timer.Hours, GOD.Offers.Offers[0].Timer.Minutes, GOD.Offers.Offers[0].Timer.Seconds); ;
            PlayerPrefsX.SetVector3("OfferFirstPurchase_Timer", v);
            PlayerPrefsX.SetVector3("OfferFirstPurchase_Date", new Vector3(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
        }
        else
            PlayerPrefsX.SetBool("OfferFirstPurchase_Deactivate", GOD.Offers.Offers[0].DeactivateOffer);
        PlayerPrefsX.SetBool("OfferNoAds_Deactivate", GOD.Offers.Offers[1].DeactivateOffer);
    }
    public void LoadOffers() //сохранение информации об оферах
    {
        if(PlayerPrefs.HasKey("OfferFirstPurchase_Deactivate"))
        {
            GOD.Offers.Offers[0].OfferOn = !PlayerPrefsX.GetBool("OfferFirstPurchase_Deactivate");
            if (!GOD.Offers.Offers[0].OfferOn)
            {
                GOD.Offers.RemoveFromList(GOD.Offers.Offers[0]);
                GOD.Offers.OpenOffer("");
            }
        }
        else if (PlayerPrefs.HasKey("OfferFirstPurchase_Timer"))
        {
            Vector3 v = PlayerPrefsX.GetVector3("OfferFirstPurchase_Timer");
            TimeSpan t = new TimeSpan((int)v.x, (int)v.y, (int)v.z);
            Vector3 v2 = PlayerPrefsX.GetVector3("OfferFirstPurchase_Date");
            if ((v2.y == DateTime.Now.Month && v2.z == DateTime.Now.Day))
            {
                if(t <= GOD.Offers.Offers[0].TimerLimit)
                GOD.Offers.Offers[0].OfferOn = false;
                else
                    GOD.Offers.Offers[0].SetTimer((int)v.x, (int)v.y, (int)v.z);
            }
        }

        if (PlayerPrefs.HasKey("OfferNoAds_Deactivate"))
        {
            GOD.Offers.Offers[1].OfferOn = !PlayerPrefsX.GetBool("OfferNoAds_Deactivate");
            if (!GOD.Offers.Offers[1].OfferOn)
            {
                MyAppodeal.NoAds = true;
                GOD.Offers.RemoveFromList(GOD.Offers.Offers[1]);
                GOD.Offers.OpenOffer("");
            }
        }
    }
    //ЗАГРУЗКА___________________________________________________________________________
    //LOAD___________________________________________________________________________________________________________________
    public void LoadGame()
    {
        SaveLoadClass CurrentSave = SaveManager.AllSaveData[CurrentSlot];
        if (CurrentSave != null && !IsNewGame)
        {
            // ВРЕМЯ_____________________________________________________________________________________________________
            GOD.TG.IYear = CurrentSave.Year;
            GOD.TG.IMonth = CurrentSave.Month;
            GOD.TG.IDay = CurrentSave.Day;
            GOD.TG.IHour = CurrentSave.Hour;
            GOD.TG.IMinute = CurrentSave.Minute;
            GOD.TG.AllDays = CurrentSave.AllDays;
            // ПОЛОЖЕНИЕ ПЕРСОНАЖА________________________________________________________________________________________
            GOD.Player.transform.position = CurrentSave.PlayerPosition;
            GOD.Player.transform.rotation = CurrentSave.PlayerRotation;
            //ПАРАМЕТРЫ ПЕРСОНАЖА_________________________________________________________________________________________
            GOD.PlayerContr.HP = CurrentSave.playerHP;
            if (CurrentSave.playerRealMaxHP > 0)
                GOD.PlayerContr.realmaxHP = CurrentSave.playerRealMaxHP;
            else
                GOD.PlayerContr.realmaxHP = GOD.PlayerContr.maxHP;
            GOD.PlayerContr.EAT = CurrentSave.playerEAT;
            //БАФЫ ПЕРСОНАЖА______________________________________________________________________________________________
            if (CurrentSave.IsSpeed) //баф скорости
            {
                GOD.BafContr.FoodEffects("Candy");
                GOD.BafContr.SpeedTimer = CurrentSave.SpeedTimer;
            }
            if (CurrentSave.IsRegen) //баф регенерации здоровья
            {
                GOD.BafContr.FoodEffects("BerriePie");
                GOD.BafContr.SpeedTimer = CurrentSave.RegenTimer;
            }
            if (CurrentSave.IsSatiety) //баф сытости
            {
                GOD.BafContr.FoodEffects("MeatPies");
                GOD.BafContr.SpeedTimer = CurrentSave.SatietyTimer;
            }
            if (CurrentSave.IsCraft) //баф увелечения скорости крафта
            {
                GOD.BafContr.FoodEffects("FishPie");
                GOD.BafContr.SpeedTimer = CurrentSave.CraftTimer;
            }
            //ПОКУПКИ____________________________________________________
            //ПРИЧЕСКИ
            GOD.PlayerContr.BoyHairs = CurrentSave.BoyHairs;//какие прически куплены
            GOD.PlayerContr.GirlHairs = CurrentSave.GirlHairs;
            GOD.PlayerContr.CurrentBoyHairNumber = PlayerPrefs.GetInt("ChibiCurrentBoyHairNumber");// текущая прическа парня
            GOD.PlayerContr.CurrentGirlHairNumber = PlayerPrefs.GetInt("ChibiCurrentGirlHairNumber");
            //ЦВЕТА
            GOD.PlayerContr.Colors = CurrentSave.HairColor;//какие цвета куплены
            if (PlayerPrefs.HasKey("ChibiCurrentBoyHairColorNumber"))
                GOD.PlayerContr.CurrentBoyHairColorNumber = PlayerPrefs.GetInt("ChibiCurrentBoyHairColorNumber");// текущий цвет
            else
                GOD.PlayerContr.CurrentBoyHairColorNumber = 1;
            if (PlayerPrefs.HasKey("ChibiCurrentGirlHairColorNumber"))
                GOD.PlayerContr.CurrentGirlHairColorNumber = PlayerPrefs.GetInt("ChibiCurrentGirlHairColorNumber");
            else
                GOD.PlayerContr.CurrentGirlHairColorNumber = 13;
            //СКИНЫ
            GOD.PlayerContr.BoySkins = CurrentSave.BoySkins;//какие скины куплены
            GOD.PlayerContr.GirlSkins = CurrentSave.GirlSkins;
            GOD.PlayerContr.CurrentBoySkinNumber = PlayerPrefs.GetInt("ChibiCurrentBoySkinNumber");// текущий скин
            GOD.PlayerContr.CurrentGirlSkinNumber = PlayerPrefs.GetInt("ChibiCurrentGirlSkinNumber");
            //СЛОТЫ В СУМКЕ
            GOD.InventoryContr.SlotCount = CurrentSave.SlotCount;// количество слотов
            GOD.BuySlotScript.ResSlot = CurrentSave.ResSlot;  // прогресс покупок слотов за ресурсы
            GOD.BuySlotScript.GoldSlot = CurrentSave.GoldSlot;  // прогресс покупок слотов за золото
            GOD.InventoryContr.SecondInit();
            //ПОСТРОЙКИ_____________________________________________________________________________________________________
            LoadBuildings(CurrentSave);
            LoadFirecamp(CurrentSave);
            //КРАФТ_________________________________________________________________
            GOD.CraftContr.CraftTimer = CurrentSave.InCraftTimer;
            int buildingCount = GOD.BuildingS.Buildings.transform.childCount;
            int buildCount = 0;
            int craftitemCount = 0;
            if (CurrentSave.CraftBuildings!=null && buildingCount >= CurrentSave.CraftBuildings.Length) 
            {
                for (int i = 0; i < buildingCount; i++)
                {
                    Transform t = GOD.BuildingS.Buildings.transform.GetChild(i);
                    if (t.CompareTag("Build"))
                    {
                        CraftBuilding Build = GOD.BuildingS.Buildings.transform.GetChild(i).GetComponent<CraftBuilding>();
                        if (Build)
                        {
                            if (CurrentSave.CraftBuildings[buildCount])
                            {
                                Build.thisCraftItem = CurrentSave.CraftItems[craftitemCount];
                                craftitemCount++;
                                Build.thisCraftItem.IsExist = true;
                                GOD.CraftContr.CraftQuenue.Add(Build);
                            }
                            buildCount++;
                        }
                    }
                }
            }
            if (CurrentSave.UsialCraftItem != null)
            {
                GOD.CraftContr.Usial.thisCraftItem = CurrentSave.UsialCraftItem;
                GOD.CraftContr.Usial.thisCraftItem.IsExist = true;
                GOD.CraftContr.CraftQuenue.Add(GOD.CraftContr.Usial);
            }
            if (GOD.CraftContr.CraftQuenue.Count > 0)
                GOD.CraftContr.InitCheckDistance();
            //ИНТЕРФЕЙС_____________________________________________________________________________________________________
            for (int i = 0; i < CurrentSave.InventoryItems.Count; i++)
            {
                SerializableItem newitem = CurrentSave.InventoryItems[i];
                GOD.InventoryContr.AddToInventoryWithParametreAndPlace(newitem.Name, newitem.Count, newitem.Durability, newitem.Validity, newitem.WaterCount, newitem.OnBag, newitem.OnPlayer, newitem.OnGame);
            }
            //доп иконки шопа
            for (int i = 0; i < CurrentSave.InventoryDopIconsName.Length; i++)
            {
                GOD.InventoryContr.AddToShopSlots(CurrentSave.InventoryDopIconsName[i], CurrentSave.InventoryDopIconsCount[i]);
            }
            GOD.PlayerInter.OpenInventory();
            GOD.PlayerInter.CloseInventoryImmediatly();
            //сундук
            for (int i = 0; i < GOD.InventoryContr.AllChest.Count; i++)//заполняем сундуки
            {
                List<SerializableItem> CurrentChest = CurrentSave.AllChestItems[i];
                for (int y = 0; y < CurrentChest.Count; y++)
                {
                    SerializableItem Info = CurrentChest[y];
                    InfoScript CurrentInfo = GOD.InventoryContr.CreateNewIcon(Info.Name);
                    CurrentInfo.ResourceCount = Info.Count;
                    CurrentInfo.currentDurability = (int)Info.Durability;
                    CurrentInfo.currentValidity = (int)Info.Validity;
                    CurrentInfo.DurabilityImage.fillAmount = (float)CurrentInfo.currentDurability / (float)CurrentInfo.durability;
                    CurrentInfo.ValidityImage.fillAmount = (float)CurrentInfo.currentValidity / 100;
                    CurrentInfo.ChestPlace = Info.ChestPlace;
                    GOD.InventoryContr.AllChest[i].MyIcons.Add(CurrentInfo);
                    //количство воды__________________________________________________
                    if (CurrentInfo.thisName == "Bucket")
                    {
                        CurrentInfo.SetWaterInBacket(Info.WaterCount);
                    }
                }
            }
            //лут после смерти
            if (CurrentSave.DeadBagPosition.Length > 0)         //создаем сумки с лутом, если они существуют
            {
                GOD.InventoryContr.AllBags = new List<GameObject>();
                for (int i = 0; i < CurrentSave.DeadBagPosition.Length; i++)
                {
                    GOD.PlayerInter.AddNewBag(CurrentSave.DeadBagPosition[i]);
                    GOD.InventoryContr.AllBags[i].GetComponent<AfterDeadBag>().MyNumber = i;
                    GOD.PlayerInter.PlayerLutAnimation();
                }

                for (int i = 0; i < GOD.InventoryContr.AllBags.Count; i++)//заполняем сумки
                {
                    AfterDeadBag a = GOD.InventoryContr.AllBags[i].GetComponent<AfterDeadBag>();
                    List<SerializableItem> CurrentSerBag = CurrentSave.AllDeadItems[i];
                    for (int y = 0; y < CurrentSerBag.Count; y++)
                    {
                        SerializableItem Info = CurrentSerBag[y];
                        InfoScript CurrentInfo = GOD.InventoryContr.CreateNewIcon(Info.Name);
                        CurrentInfo.ResourceCount = Info.Count;
                        CurrentInfo.currentDurability = (int)Info.Durability;
                        CurrentInfo.currentValidity = (int)Info.Validity;
                        CurrentInfo.DurabilityImage.fillAmount = (float)CurrentInfo.currentDurability / (float)CurrentInfo.durability;
                        CurrentInfo.ValidityImage.fillAmount = (float)CurrentInfo.currentValidity / 100;
                       a.MyIcons.Add(CurrentInfo);
                        //количство воды__________________________________________________
                        if (CurrentInfo.thisName == "Bucket")
                            CurrentInfo.SetWaterInBacket(Info.WaterCount);
                    }
                }
            }
            //СОСТОЯНИЕ МИРА
            //прогресс строительства моста
            LoadBridge(CurrentSave);
            //включение выключение объекто
            LoadWorld(CurrentSave);
            //животные
            LoadEnemies(CurrentSave);
            //погода на островах
            LoadWeather(CurrentSave);

            //КВЕСТЫ_______________________________________
            if(CurrentSave.Quests!=null)
            {
                QuestsTemplate[] allQuests = GOD.QuestContr.AllQuests.QuestsTemplate;
                for (int i = 0; i < CurrentSave.Quests.Length; i++)
                {
                    if(i<=allQuests.Length)
                    {
                        allQuests[i].complete = CurrentSave.Quests[i].IsQuestComplete;
                        if (!allQuests[i].complete)
                        {
                            allQuests[i].isactive = CurrentSave.Quests[i].IsNowActive;
                            allQuests[i].current_stage_count = CurrentSave.Quests[i].Current_stage;
                            if (allQuests[i].isactive && allQuests[i].current_stage_count>-1)
                                GOD.QuestContr.InitQuest(allQuests[i]);
                            if (allQuests[i].current_stage_count > -1)
                            {
                                allQuests[i].stages[allQuests[i].current_stage_count - 1].complete = CurrentSave.Quests[i].IsCurrentStageComplete;
                                if (!allQuests[i].stages[allQuests[i].current_stage_count - 1].complete)
                                { 
                                QuestsDetail[] qd = allQuests[i].stages[allQuests[i].current_stage_count - 1].details;
                                if (qd != null)
                                {
                                    for (int x = 0; x < qd.Length; x++)
                                    {
                                        if (x <= CurrentSave.Quests[i].Details_current_count.Length)
                                        {
                                            qd[x].current_count = CurrentSave.Quests[i].Details_current_count[x];
                                                if (qd[x].current_count >= qd[x].count)
                                                qd[x].complete = true;
                                        }
                                    }
                                }
                            }
                            }
                            if (allQuests[i].isactive && allQuests[i].current_stage_count > -1)
                            {
                                GOD.QuestContr.AddQuestToCurrentQuests(allQuests[i]);
                            }
                            else if (CurrentSave.Quests[i].IsNowPlank)
                                GOD.QuestContr.AddNewQuest(allQuests[i]);
                        }
                    }
                }
            }
        }
        else//если игра стартует сначала
        {
            // ВРЕМЯ_____________________________________________________________________________________________________
            GOD.TG.StartTime();
            // ПОЛОЖЕНИЕ ПЕРСОНАЖА________________________________________________________________________________________
            GOD.Player.transform.position = new Vector3(0, 0, -86);
            if (!GOD.PlayerContr.IsAlive)
            {
                GOD.DopEff.Ressurection.transform.position = GOD.Player.transform.position + new Vector3(0, 2, 0);
                GOD.DopEff.Ressurection.transform.SetParent(null);
                GOD.PlayerInter.StopRes();
            }
            GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -11, 0));
            //ПАРАМЕТРЫ ПЕРСОНАЖА_________________________________________________________________________________________
            GOD.PlayerContr.realmaxHP = GOD.PlayerContr.maxHP;
            GOD.PlayerContr.HP = GOD.PlayerContr.realmaxHP;
            GOD.PlayerContr.EAT = GOD.PlayerContr.maxEAT;
            //ПОКУПКИ____________________________________________________
            //ПРИЧЕСКИ
            GOD.PlayerContr.BoyHairs[0] = true;
            for (int i = 1; i > GOD.PlayerContr.BoyHairs.Length; i++)
                GOD.PlayerContr.BoyHairs[i] = false;
            GOD.PlayerContr.GirlHairs[0] = true;
            for (int i = 1; i > GOD.PlayerContr.GirlHairs.Length; i++)
                GOD.PlayerContr.GirlHairs[i] = false;
            GOD.PlayerContr.CurrentBoyHairNumber = 0; // текущая прическа 
            GOD.PlayerContr.CurrentGirlHairNumber = 0;
            //ЦВЕТА
            for (int i = 0; i > GOD.PlayerContr.Colors.Length; i++)
                GOD.PlayerContr.Colors[i] = false;
            GOD.PlayerContr.Colors[1] = true;
            GOD.PlayerContr.Colors[13] = true;
            GOD.PlayerContr.CurrentBoyHairColorNumber = 1;//текущий цвет
            GOD.PlayerContr.CurrentGirlHairColorNumber = 13;
            //СКИНЫ
            GOD.PlayerContr.BoySkins[0] = true;
            for (int i = 1; i > GOD.PlayerContr.BoySkins.Length; i++)
                GOD.PlayerContr.BoySkins[i] = false;
            GOD.PlayerContr.GirlSkins[0] = true;
            for (int i = 1; i > GOD.PlayerContr.GirlSkins.Length; i++)
                GOD.PlayerContr.GirlSkins[i] = false;
            GOD.PlayerContr.CurrentBoySkinNumber = 0;// текущий скин 
            GOD.PlayerContr.CurrentGirlSkinNumber = 0;
            //СЛОТЫ
            GOD.InventoryContr.SlotCount = GOD.InventoryContr.SlotStartCount;
            GOD.InventoryContr.SecondInit();
            LoadTutorial();
        }

        //гололомки
        LoadPuzzle(CurrentSave);

        //задаем параметры внешнего вида
        if (GOD.OurPlayer == "Girl")
        {
            GOD.PlayerContr.SetPlayerHairs(GOD.PlayerContr.CurrentGirlHairNumber);
            GOD.PlayerContr.SetPlayerHairColor(GOD.PlayerContr.CurrentGirlHairColorNumber);
        }
        else
        {
            GOD.PlayerContr.SetPlayerHairs(GOD.PlayerContr.CurrentBoyHairNumber);
            GOD.PlayerContr.SetPlayerHairColor(GOD.PlayerContr.CurrentBoyHairColorNumber);
        }

        //МОРОЗ_______________________________________________________________
        if (GOD.TG.IHour >= 1 && GOD.TG.IHour <= 5)
        {
            if (CurrentSave != null && !IsNewGame)
            {
                GOD.PlayerContr.Frost = CurrentSave.Frost;
                GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff",CurrentSave.FrostWindow);
            }
        }
        else
        {
            GOD.PlayerContr.Frost = 0;
            GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff", 1);
        }
        //ПРОГРЕСС ИССЛЕДОВАНИЯ ОСТРОВОВ_________________________________________________________________________
        if (CurrentSave!=null && !IsNewGame)
        {
            GOD.Map.OpenIslands = CurrentSave.OpenIslands;
            GOD.Map.SetClouds();
        }
        //ПРОГРЕСС ТАЙМЕРА ЭССЕНЦИЙ
        if (CurrentSave!=null && CurrentSave.EssenceName!=null && CurrentSave.EssenceName != "")
        {
            DateTime time = DateTime.Now;
            int NowHour = time.Hour;
            int NowMinute = time.Minute;
            int Current = 0;
            if (CurrentSave.EssenseHour != NowHour)
            {
                Current = (60 - NowMinute) + CurrentSave.EssenceMinute;
            }
            else
                Current = CurrentSave.EssenceMinute - NowMinute;
            if (!BuildSettings.isFree && Current > 5)
                Current = 5;
            else if (BuildSettings.isFree && Current > 60)
                Current = 60;
            GOD.PlayerInter.EssenceMinutTimer = Current;
            GOD.PlayerInter.CraftEssence(CurrentSave.EssenceName);
        }
        //ОФФЕРЫ
        LoadOffers();

        StartCoroutine(WaitSave());
    }

    //ПОСТРОЙКИ
    public Transform currentBase;
    public Transform currentWall;
    public Transform currentRoof;
    public void LoadBuildings(SaveLoadClass CurrentSave)
    {
        string[] saveObjectName = CurrentSave.BuildingName;
        Vector3[] saveObjectPos = new Vector3[CurrentSave.BuildingPosition.Length];
        for (int x = 0; x < CurrentSave.BuildingPosition.Length; x++)
            saveObjectPos[x] = CurrentSave.BuildingPosition[x];
        Quaternion[] saveObjectRotat = new Quaternion[CurrentSave.BuildingRotation.Length];
        for (int x= 0; x < CurrentSave.BuildingRotation.Length; x++)
            saveObjectRotat[x] = CurrentSave.BuildingRotation[x];
        bool[] saveObjectChildrenFence = CurrentSave.BuildingChildrenFence;

        int i = 0;
        while (i < saveObjectName.Length)
        {
            GOD.BuildingS.LoadBuilding("Icon_" + saveObjectName[i], saveObjectPos[i], saveObjectRotat[i]);
            bool boo = true;
            while (boo)
            {
                if (i + 1 < saveObjectName.Length)
                {
                    if (currentBase&& saveObjectName[i + 1].Contains("Wall") || (saveObjectName[i + 1].Contains("Fence") && saveObjectChildrenFence[i + 1])) //добавляем стены
                    {
                        i++;
                        GOD.BuildingS.LoadBuilding("Icon_" + saveObjectName[i], saveObjectPos[i], saveObjectRotat[i], currentBase);
                    }
                    else
                    {//добавляем крыши, окна и двери
                        if(saveObjectName[i + 1].Contains("RoofTop"))
                        {
                            i++;
                            GOD.BuildingS.LoadBuilding("Icon_" + saveObjectName[i], saveObjectPos[i], saveObjectRotat[i], currentRoof);
                        }
                        else if ((saveObjectName[i + 1].Contains("Roof") || saveObjectName[i + 1] == "Window" || saveObjectName[i + 1] == "Door"))
                        {
                            i++;
                            GOD.BuildingS.LoadBuilding("Icon_" + saveObjectName[i], saveObjectPos[i], saveObjectRotat[i], currentWall);
                        }
                        else
                        {
                            i++;
                            GOD.BuildingS.LoadBuilding("Icon_" + saveObjectName[i], saveObjectPos[i], saveObjectRotat[i]);
                        }
                    }
                }
                else
                    boo = false;
            }
            i++;
        }
    }

    GameObject InstantBuild(GameObject p, Vector3 pos, Quaternion rot, Transform thisparent, int chestNumber)
    {
        GameObject newBuild = Instantiate(p);
        newBuild.transform.position = pos;
        newBuild.transform.rotation = rot;
        newBuild.transform.SetParent(thisparent);
        newBuild.name = p.name;
        newBuild.AddComponent<ObjectScript>();
        newBuild.GetComponent<ObjectScript>().InitGOD(GOD);
        newBuild.GetComponent<ObjectScript>().Init();

        // включаем коллайдеры
        if (newBuild.name == "WoodStair")
        {
            newBuild.GetComponent<BoxCollider>().enabled = false;
            CapsuleCollider[] Cs = newBuild.GetComponents<CapsuleCollider>();
            foreach (CapsuleCollider b in Cs)
                if (b.isTrigger)
                    b.isTrigger = false;
        }
        else
        {
            BoxCollider[] BCs = newBuild.GetComponents<BoxCollider>();
            foreach (BoxCollider b in BCs)
                if (b.isTrigger)
                    b.isTrigger = false;
        }

        // заполняем массив сундуков
        if ((newBuild.name == "Chest") || (newBuild.name == "IceChest"))
        {
            GOD.InventoryContr.AllChest.Add(newBuild.GetComponent<ChestScript>());
            for (int x = 0; x < GOD.InventoryContr.AllChest.Count; x++)
            {
                if (GOD.InventoryContr.AllChest[x] == newBuild.GetComponent<ChestScript>())
                {
                    newBuild.GetComponent<ChestScript>().MyNumber = chestNumber;
                    break;
                }
            }
        }
        else if (newBuild.name == "Door")
        {
            newBuild.GetComponent<DoorScript>().Init();
        }
        return newBuild;
    }

    void LoadFirecamp(SaveLoadClass CurrentSave)
    {
        if (CurrentSave.FirecampPosition.Length>0)
        {
            for (int i = 0; i < CurrentSave.FirecampPosition.Length; i++)
            {
                GameObject ParentnewBuild = GOD.BuildingS.SetCurrentBlock("Icon_Firecamp");
                if (ParentnewBuild)
                {
                    GameObject newBuild = Instantiate(ParentnewBuild);
                    newBuild.transform.position = CurrentSave.FirecampPosition[i];
                    newBuild.transform.rotation = CurrentSave.FirecampRotation[i];
                    newBuild.transform.SetParent(GOD.BuildingS.Buildings.transform);
                    newBuild.name = ParentnewBuild.name;
                    // включаем коллайдеры
                    BoxCollider[] BCs = newBuild.GetComponents<BoxCollider>();
                    foreach (BoxCollider b in BCs)
                        if (b.isTrigger)
                            b.isTrigger = false;

                    //включаем костры
                    newBuild.GetComponent<FirecampScript>().FireCampHP =CurrentSave.FirecampHP[i];
                    newBuild.GetComponent<FirecampScript>().GOD = GOD;
                    newBuild.GetComponent<FirecampScript>().StartFirecamp();
                    newBuild.AddComponent<ObjectScript>();
                    newBuild.GetComponent<ObjectScript>().InitGOD(GOD);
                    newBuild.GetComponent<ObjectScript>().Init();
                }
            }
        }
    }

    //СОСТОЯНИЕ МИРА
    //прогресс строительства моста
    void LoadBridge(SaveLoadClass CurrentSave)
    {
        GOD.ToSnow.IsOpen = CurrentSave.BridgeState[0];
        GOD.ToSnow.SetResoures();

        GOD.ToRain.IsOpen = CurrentSave.BridgeState[1];
        GOD.ToRain.SetResoures();

        GOD.ToSand.IsOpen = CurrentSave.BridgeState[2];
        GOD.ToSand.SetResoures();

        Pathfinder.Instance.CreateMap();
    }
    //включение выключение объекто
    void LoadWorld(SaveLoadClass CurrentSave)
    {
        int[] IsRecoveryObjects = CurrentSave.IsRecoveryObject;
        int[] IsRecoveryResources = CurrentSave.IsRecoveryResources;
        for (int x = 0; x < GOD.AllObjects.Length; x++)
        {
            if (IsRecoveryObjects.Length > x)
            {
                if (IsRecoveryObjects[x] > 0)
                {
                    GOD.AllObjects[x].GetComponent<ObjectScript>().RecoveryTime = IsRecoveryObjects[x];
                    GOD.RecoveryObject.Add(GOD.AllObjects[x].GetComponent<ObjectScript>());
                    GOD.AllObjects[x].SetActive(false);
                }
                else if (IsRecoveryResources[x] > 0)
                {
                    GOD.AllObjects[x].GetComponent<ObjectScript>().RecoveryTime = IsRecoveryResources[x];
                    GOD.AllObjects[x].GetComponent<ObjectScript>().CollectionResNumber1 = 0;
                    GOD.AllObjects[x].GetComponent<ObjectScript>().CollectionResNumber2 = 0;
                    GOD.RecoveryResourses.Add(GOD.AllObjects[x].GetComponent<ObjectScript>());
                    GOD.AllObjects[x].SetActive(true);
                }
            }
        }
    }
    //животные
    void LoadEnemies(SaveLoadClass CurrentSave)
    {
        int[] IsRecoveryEnemies = CurrentSave.IsRecoveryEnemies;

        for (int i = 0; i < GOD.EnviromentContr.AllEnemies.Count; i++)
        {
            if (IsRecoveryEnemies[i] > 0)
            {
                GOD.EnviromentContr.AllEnemies[i].RecoveryTime = IsRecoveryEnemies[i];
                GOD.RecoveryEnemy.Add(GOD.EnviromentContr.AllEnemies[i]);
                GOD.EnviromentContr.AllEnemies[i].gameObject.SetActive(false);
            }
        }
    }
    //погода на островах
    void LoadWeather(SaveLoadClass CurrentSave)
    {
        GOD.EnviromentContr.CWater.CurrentState = CurrentSave.IslandWaterState;
        GOD.EnviromentContr.CSnow.CurrentState = CurrentSave.IslandSnowState;
        GOD.EnviromentContr.CSand.CurrentState = CurrentSave.IslandSandState;
        GOD.EnviromentContr.CSand.OldState = CurrentSave.IslandSandOldState;

        GOD.EnviromentContr.CurrentIsland = CurrentSave.CurrentIsland;
        GOD.EnviromentContr.PlayerOnIsland(GOD.ReturnIslandByName(CurrentSave.CurrentIsland));
    }
    //гололомки
    void LoadPuzzle(SaveLoadClass CurrentSave)
    {
        if (CurrentSave!=null)
        {
            GOD.EnviromentContr.CSnow.SetIceBreakLay(CurrentSave.IceBreak);
            GOD.EnviromentContr.CSand.LayBigTree(CurrentSave.BigTree);
        }
        else
        {
            GOD.EnviromentContr.CSnow.SetIceBreakLay(-1);
            GOD.EnviromentContr.CSand.LayBigTree(-1);
        }

    }
    //Сохранение туториала____________________________________________
    public void SaveTutorial()
    {
       
        SaveLoadClass TutorialSave = new SaveLoadClass();
        TutorialSave.InventoryItems = SaveItems(GOD.InventoryContr.AllIcons, "inventory");
        Texture2D icon=new Texture2D(1,1);
        SaveManager.Save(100, TutorialSave, icon);
    }

    void LoadTutorial()
    {
        SaveLoadClass TutorialSave = new SaveLoadClass();
        Sprite currentSp;
        SaveManager.LoadFromDisk(100, out TutorialSave, out currentSp);
        if (TutorialSave!=null)
        {
            //ИНВЕНТАРЬ
            for (int i = 0; i < TutorialSave.InventoryItems.Count; i++)
            {
                SerializableItem newitem = TutorialSave.InventoryItems[i];
                GOD.InventoryContr.AddToInventoryWithParametreAndPlace(newitem.Name, newitem.Count, newitem.Durability, newitem.Validity, newitem.WaterCount, newitem.OnBag, newitem.OnPlayer, newitem.OnGame);
            }
            GOD.PlayerInter.OpenInventory();
            GOD.PlayerInter.CloseInventoryImmediatly();
        }
    }
}
