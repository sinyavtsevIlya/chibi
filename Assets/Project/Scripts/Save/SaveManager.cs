﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class SaveManager
{
    public SettingsController Settings;

    public static SaveLoadClass[] AllSaveData = new SaveLoadClass[6];
    //public static bool UseCloud = true;

    // Текущий слот в котором играем
    public static int CurrentSlot
    {
        get
        {
            return PlayerPrefs.GetInt("SaveManagerCurrentSlot", 0);
        }

        set
        {
            PlayerPrefs.SetInt("SaveManagerCurrentSlot", value);
            PlayerPrefs.Save();
        }
    }

    public static void Save(int selectSlot, SaveLoadClass data, Texture2D icon)
    {
        if ((data == null) || (icon == null)) return;
        //if ((data == null)) return;

        var pathData = GetPathData(selectSlot);
        var saveData = ObjectToByteArray(data);
        SaveData(pathData, saveData);

        var pathPNG = GetPathPNG(selectSlot);
        var jpgData = (icon != null) ? icon.EncodeToJPG() : null;
        SaveData(pathPNG, jpgData);
    }

    public static void LoadFromDisk(int selectSlot, out SaveLoadClass data, out Sprite icon)
    {
        var pathData = GetPathData(selectSlot);
        var pathPNG = GetPathPNG(selectSlot);

        data = ByteArrayToObject<SaveLoadClass>(LoadData(pathData));
        icon = ByteToSprite(LoadData(pathPNG));
    }

    static void SaveData(string path, byte[] data)
    {
        if (data == null || data.Length == 0) return;

        var pathTemp = GetPathTemp(path);
        var pathBackup = GetPathBackup(path);

        try
        {
            File.WriteAllBytes(pathTemp, data);

            if (File.Exists(path))
                File.Replace(pathTemp, path, pathBackup);
            else
                File.Move(pathTemp, path);
        }
        catch
        {

        }
    }

    static byte[] LoadData(string path)
    {
        try
        {
            var data = ReadFile(path);
            if (data != null && data.Length != 0)
                return data;

            var pathTemp = GetPathTemp(path);
            data = ReadFile(pathTemp);
            if (data != null && data.Length != 0)
                return data;

            var pathBackup = GetPathBackup(path);
            data = ReadFile(pathBackup);
            if (data != null && data.Length != 0)
                return data;

            return null;
        }
        catch
        {
            return null;
        }
    }

    //static void WriteFile(string fullPath, byte[] data)
    //{
    //    File.WriteAllBytes(fullPath, data);
    //}

    static byte[] ReadFile(string fullPath)
    {
        if (!File.Exists(fullPath))
            return null;

        return File.ReadAllBytes(fullPath);
    }



    static byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;

        var json = JsonUtility.ToJson(obj);
        var bytes = Encoding.UTF8.GetBytes(json);
        //var base64 = Convert.ToBase64String(bytes);
        return bytes;

    }

    static T ByteArrayToObject<T>(byte[] bytes) where T : SaveLoadClass
    {
        if ((bytes == null) || (bytes.Length == 0)) return null;

        var json = Encoding.UTF8.GetString(bytes);
        var result = JsonUtility.FromJson<T>(json);
        return result;
    }


    static byte[] SpriteToByteArray(Texture2D icon) => (icon != null) ? icon.EncodeToJPG() : null;
    static Sprite ByteToSprite(byte[] data)
    {
        if (data == null) return null;

        var tex = new Texture2D(Screen.width, Screen.height);
        tex.LoadImage(data);
        var sp = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));

        return sp;
    }


    const string SAVE_PREFIX = "SLOT_";
    const string postFixTemp = "_tmp";
    const string postFixBackup = "_backup";

    static string GetSlotName(int selectSlot) => string.Format("{0}{1}", SAVE_PREFIX, selectSlot);
    static string GetPathData(int selectSlot) => string.Format("{0}/{1}.bin", Application.persistentDataPath, GetSlotName(selectSlot));
    static string GetPathPNG(int selectSlot) => string.Format("{0}/{1}_Screen.png", Application.persistentDataPath, GetSlotName(selectSlot));

    static string GetPathTemp(string path) => path + postFixTemp;
    static string GetPathBackup(string path) => path + postFixBackup;
}


[Serializable]
public class SaveLoadClass
{
    public String SaveDate;
    //ВРЕМЯ В ИГРЕ
    public int Year, Month, Day, Hour, Minute;
    public int AllDays = 0;
    //ПОЛОЖЕНИЕ ПЕРСОНАЖА
    public SerializableVector3 PlayerPosition;
    public SerializableQuaternion PlayerRotation;
    //ПАРАМЕТРЫ ПЕРСОНАЖА
    public float playerHP = 100, playerRealMaxHP = 100, playerEAT = 100;
    public string Male = "Boy";
    //БАФЫ ПЕРСОНАЖА
    public bool IsSpeed = false, IsRegen = false, IsSatiety = false, IsCraft = false;
    public int SpeedTimer = 0, RegenTimer = 0, SatietyTimer = 0, CraftTimer = 0;
    //МОРОЗ
    public float Frost = 0, FrostWindow = 0;
    //ПРОГРЕСС ИССЛЕДОВАНИЯ ОСТРОВОВ
    public bool[] OpenIslands = new bool[10];
    //ПОКУПКИ ИГРОКА
    public bool[] BoyHairs, GirlHairs, HairColor, BoySkins, GirlSkins;
    public int SlotCount = 20, ResSlot = 0, GoldSlot = 0;
    //РЕСУРСЫ ИГРОКА 
    public List<SerializableItem> InventoryItems;

    public string[] InventoryDopIconsName;  //шоповсике доп иконки
    public int[] InventoryDopIconsCount;

    public List<SerializableItem>[] AllChestItems;
    //ПОСТРОЙКИ
    public string[] BuildingName;
    public SerializableVector3[] BuildingPosition;
    public SerializableQuaternion[] BuildingRotation;
    public bool[] BuildingChildrenFence;

    public SerializableVector3[] FirecampPosition;
    public SerializableQuaternion[] FirecampRotation;
    public int[] FirecampHP;
    //КРАФТ
    public CraftItem[] CraftItems = new CraftItem[0]; //что сейчас крафтится
    public bool[] CraftBuildings = new bool[0]; //в каких зданиях крафтится
    public CraftItem UsialCraftItem = null;
    public float InCraftTimer = 0;
    //СМЕРТЬ
    public SerializableVector3[] DeadBagPosition;
    public List<SerializableItem>[] AllDeadItems;

    //СОСТОЯНИЕ МИРА
    public bool[] BridgeState; //состояние мостов
    public int[] IsRecoveryObject; //восставливающиеся объекты
    public int[] IsRecoveryResources;// восстанавливающиеся ресурсы
    public int[] IsRecoveryEnemies; //восстановление врагов
    public string IslandWaterState, IslandSandState, IslandSandOldState, IslandSnowState;
    public string CurrentIsland;
    public int IceBreak = -1, BigTree = -1;  //головоломки

    //КВЕСТЫ
    public SaveQuestClass[] Quests = null;

    //ПРОГРЕСС ТАЙМЕРА ЭССЕНЦИЙ
    public string EssenceName = "";
    public int EssenseHour = 0;
    public int EssenceMinute = 0;
}


[Serializable]
public class SaveQuestClass
{
    public bool IsQuestComplete = false;
    public int Current_stage = -1;
    public bool IsCurrentStageComplete = false;
    public int[] Details_current_count = null;
    public bool IsNowActive = false;  //находится ли сейчас в журнале заданий
    public bool IsNowPlank = false; //находится ли сейчас в доске заданий
}