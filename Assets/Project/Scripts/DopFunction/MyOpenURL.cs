using UnityEngine;
using System;

public class MyOpenURL {

    static MyOpenURL _instance;
    public static MyOpenURL instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new MyOpenURL();
            }
            return _instance;
        }
    }


    public static void OnApplicationFocus(bool focusStatus)
    {
        if (!focusStatus)
            instance.isWaiting = false;
    }

    #region OpenURL
    protected System.Timers.Timer aTimer;
    protected Action<bool> lastAction;
    protected bool isWaiting = false;

    protected void OnTimedEvent(object sender, System.EventArgs e)
    {
        if (instance.isWaiting)
        {
            DoOnMainThread.AddAction(() => lastAction(false));
        }
    }

    public static void OpenUrl(Action<bool> action)
    {
        if (instance.isWaiting)
            return;

        instance.lastAction = action;
        instance.lastAction(true);

        instance.isWaiting = true;
        instance.aTimer = new System.Timers.Timer(1000);
        
        instance.aTimer.Elapsed -= instance.OnTimedEvent;
        instance.aTimer.Elapsed += instance.OnTimedEvent;
        instance.aTimer.AutoReset = false;
        instance.aTimer.Enabled = true;
    }

    // переход на страницу компании
    public static void OpenUrlMoreApps()
    {
        // OpenUrl(MyGameManager.urlMoreApps);
        OpenUrl((val) =>
        {
           // Debug.Log(GameURL.urlMoreApps(val));
            Application.OpenURL(GameURL.urlMoreApps(val));
        });
    }

    // переход на страницу игры
    public static void OpenUrlReview()
    {
        // OpenUrl(MyGameManager.urlReview);
        OpenUrl((val) =>
        {
           // Debug.Log(GameURL.urlReview(val));
            Application.OpenURL(GameURL.urlReview(val));
        });
    }

    // переход на страницу компании
    public static void OpenUrlHelp()
    {
        //  OpenUrl(MyGameManager.urlHelp);
        OpenUrl((val) =>
        {
            //Debug.Log(GameURL.urlHelp(val));
            Application.OpenURL(GameURL.urlHelp(val));
        });
    }

    // переход на страницу в контакте
    public static void OpenUrlVKontakte()
    {
        //  OpenUrl(MyGameManager.urlVKontakte);
        OpenUrl((val) =>
        {
            //Debug.Log(GameURL.urlVK(val));
            Application.OpenURL(GameURL.urlVK(val));
        });
    }

    // переход на страницу в файсбуке
    public static void OpenUrlFacebook()
    {
        //  OpenUrl(MyGameManager.urlFacebook);
        OpenUrl((val) =>
        {
           // Debug.Log(GameURL.urlFB(val));
            Application.OpenURL(GameURL.urlFB(val));
        });
    }
    #endregion
}
