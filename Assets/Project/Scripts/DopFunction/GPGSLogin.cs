using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;

using System;
using System.Collections.Generic;

#if!NO_GPGS
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

public class GPGSLogin : MonoBehaviour
{
    static bool isLoging = false;
    public static bool authenticated
    {
        get
        {

            return Social.localUser.authenticated;
        }
    }

   /* private void Start()
    {
        if (BuildSettings.Market != TargetMarket.AMAZON)
            Init();
    }*/

    public void BtnShowLeaderboardUI()
    {
        ShowLeaderboardUI();
    }

    public void BtnShowAchivmentUI()
    {
        ShowAchivmentUI();
    }

    #region Login
    public static void Init()
    {

#if !NO_GPGS
        PlayGamesPlatform.DebugLogEnabled = true;
        //PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        //    //.EnableSavedGames()
        //    .Build();

        //PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
#endif

       // Login();
    }

    public static void Login()
    {
        Debug.Log("GPGS Login");
        Social.localUser.Authenticate(AuthenticateCallBack);
    }

   private static void AuthenticateCallBack(bool authenticated)
    {
       /* if (authenticated)
        {

        }
        else
        {

        }

        SystemEvents.GPGSLogin(authenticated);*/
    }

    #endregion

    #region Leaderboard
    public static void ShowLeaderboardUI()
    {
           // Debug.Log("GPGS ShowLeaderboardUI");


        if (!authenticated)
        {
           // Login();
            return;
        }

        Social.ShowLeaderboardUI();
    }
    #endregion

    #region Achivment
    public static void ShowAchivmentUI()
    {
       // Debug.Log("GPGS ShowAchivmentUI");

        if (!authenticated)
        {
            Login();
            return;
        }

        Social.ShowAchievementsUI();
    }

    public static void ReportScore(string scoreId, int plusScore) //������ ����������
    {
        string key = "ReportScore_" + scoreId;
        int scoreCount = PlayerPrefs.GetInt(key, 0);
        scoreCount += plusScore;
        PlayerPrefs.SetInt(key, scoreCount);
        if (!authenticated) return;
        Social.ReportScore(scoreCount, scoreId, UpdateLeaderboardCallBack);
    }
    static void UpdateLeaderboardCallBack(bool b)
    {
       // Debug.Log("Score " + b);
    }


    public static void ReportProgress(string achievementId)
    {
        PlayerPrefsX.SetBool("AchivmentExecuted_" + achievementId,true); //��������� ��� ������ �����������

        string key = "ReportProgress_" + achievementId;
        bool isSend = PlayerPrefsX.GetBool(key, false);
        if (isSend) return;

        if (!authenticated) return;

        Social.ReportProgress(achievementId, 100.0f, result =>
        {
            if (result)
            {
                PlayerPrefsX.SetBool(key, true);//��������� ��� ������ �����������
                PlayerPrefs.Save();
            }
        }
        );
    }

    public static void IncrementAchievement(string achievementId, float steps, float maxStep)
    {
        if (steps > maxStep) steps = maxStep;
        PlayerPrefs.SetFloat("AchivmentExecuted_" + achievementId, steps); //��������� �������� ������

        if (!authenticated) return;

        string key = "IncrementAchievement_" + achievementId;
        float saveValue = PlayerPrefs.GetFloat(key, 0);
        int deltastep = (int)(steps - saveValue);
        if (deltastep <= 0) return;

#if !NO_GPGS
        PlayGamesPlatform.Instance.IncrementAchievement(achievementId, deltastep, result =>
        {
            if (result)
            {
                PlayerPrefs.SetFloat(key, steps); //��������� ������� �������� � ������
                PlayerPrefs.Save();
            }
        }
        );

#else
       Social.ReportProgress(achievementId, steps/maxStep*100, result =>
        {
            if (result)
            {
                PlayerPrefsX.SetBool(key, true);
                PlayerPrefs.Save();
            }
        }
        );
#endif

    }

    #endregion

}