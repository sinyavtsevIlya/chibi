﻿using System;

public class AdsBaseListener : IDisposable
{
    public Action AdClosed;
    public Action AdOpened;

    public Action AdLoadFailed;
    public Action AdShowFailed;

    protected void ExecuteMainThread(Action action)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            action?.Invoke();
        });
    }

    public virtual void Dispose()
    {

    }
}


public class InterstitialListener : AdsBaseListener
{

}

public class RewardedVideoListener : AdsBaseListener
{
    public Action UserEarnedReward;
}