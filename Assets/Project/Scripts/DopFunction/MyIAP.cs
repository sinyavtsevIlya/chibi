
using UnityEngine;
using UnityEngine.Purchasing;
#if !NO_IAP
using UnityEngine.Purchasing.Security;


using System;
using System.Collections.Generic;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class MyIAP : IStoreListener
{
    static MyIAP _instance;

    public static MyIAP instance
    {
        get
        {
            if (_instance == null)
                _instance = new MyIAP();

            return _instance;
        }
    }

    private IStoreController m_Controller;

#if UNITY_IOS
    private IAppleExtensions m_AppleExtensions;
#endif

    public bool isInitialize = false;

//#if !UNITY_EDITOR
//    private CrossPlatformValidator validator;
//#endif

    string place = "Unknown";
    /// <summary>
    /// This will be called when Unity IAP has finished initialising.
    /// </summary>
    /// 
    public void Init()
    {
        var module = StandardPurchasingModule.Instance();

        // The FakeStore supports: no-ui (always succeeding), basic ui (purchase pass/fail), and 
        // developer ui (initialization, purchase, failure code setting). These correspond to 
        // the FakeStoreUIMode Enum values passed into StandardPurchasingModule.useFakeStoreUIMode.
        module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

        var builder = ConfigurationBuilder.Instance(module);
        //builder.Configure<IGooglePlayConfiguration>().SetPublicKey(BuildSettings.publicKeyGoogle);


        IAPItem[] itemName = new IAPItem[] {
          //  new IAPItem("testitem", ProductType.Consumable, true),

            new IAPItem("gempack1"),
            new IAPItem("gempack2"),
            new IAPItem("gempack3"),
            new IAPItem("gempack4"),
            new IAPItem("gempack5"),
            new IAPItem("gempack6"),
            new IAPItem("offerfirstpurchase"),
            new IAPItem("offernoads"),
            new IAPItem("test"),
        };


        foreach (IAPItem iapItem in itemName)
        {
            if (!BuildSettings.isDebug && iapItem.isDebug) continue;

            builder.AddProduct(iapItem.name, iapItem.type, new IDs
            {
                {BuildSettings.bundleGoogle + "." + iapItem.name, GooglePlay.Name},
                {BuildSettings.bundleAmazon + "." + iapItem.name, AmazonApps.Name},
                {BuildSettings.bundleIOS + "." + iapItem.name, AppleAppStore.Name}
            });
        }


        //if (BuildSettings.isDebug)
        //    builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);

//#if !UNITY_EDITOR
//        validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
//#endif

        // Now we're ready to initialize Unity IAP.
        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        if (m_Controller != null)
            return;

        m_Controller = controller;

#if UNITY_IOS
        m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
        m_AppleExtensions.RegisterPurchaseDeferredListener(OnDeferred);
#endif

        isInitialize = true;

       foreach(Product p in m_Controller.products.all)
        {
            Debug.Log(p.metadata.localizedTitle + p.metadata.localizedPriceString);
        }
    }

    /// <summary>
    /// This will be called when a purchase completes.
    /// </summary>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        BuyProduct(e.purchasedProduct);

        return PurchaseProcessingResult.Complete;
    }

    /// <summary>
    /// This will be called is an attempted purchase fails.
    /// </summary>
    public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
    {
        Debug.Log("Purchase failed: " + item.definition.id);
        Debug.Log(r);
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        isInitialize = false;
        Debug.Log("Billing failed to initialize!");
        switch (error)
        {
            case InitializationFailureReason.AppNotKnown:
                Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                break;
            case InitializationFailureReason.PurchasingUnavailable:
                // Ask the user if billing is disabled in device settings.
                Debug.Log("Billing disabled!");
                break;
            case InitializationFailureReason.NoProductsAvailable:
                // Developer configuration error; check product metadata.
                Debug.Log("No products available for purchase!");
                break;
        }
    }

    /// <summary>
    /// This will be called after a call to <extension>.RestoreTransactions().
    /// </summary>
    public void RestorePurchase()
    {
#if UNITY_IOS
        m_AppleExtensions.RestoreTransactions(OnTransactionsRestored);
#endif
    }

    /// <summary>
    /// This will be called after a call to <extension>.RestoreTransactions().
    /// </summary>
    public void OnTransactionsRestored(bool success)
    {
        foreach (var item in m_Controller.products.all)
        {
            // Collect history status report

            if (item.hasReceipt)
                BuyProduct(item);

        }
    }

    /// <summary>
    /// iOS Specific.
    /// This is called as part of Apple's 'Ask to buy' functionality,
    /// when a purchase is requested by a minor and referred to a parent
    /// for approval.
    /// 
    /// When the purchase is approved or rejected, the normal purchase events
    /// will fire.
    /// </summary>
    /// <param name="item">Item.</param>
    public void OnDeferred(Product item)
    {
        Debug.Log("Purchase deferred: " + item.definition.id);
    }

    public void InitiatePurchase(string str)
    {
        if ((m_Controller == null) || (!isInitialize)) return;

        // MyAppodeal.SetShow(true);
        m_Controller.InitiatePurchase(str);
    }

    // Вызывает окно покупки
    public void Buy(string str)
    {
        if ((m_Controller == null) || (!isInitialize)) return;

       // MyAppodeal.SetShow(true);
        InitiatePurchase(str);
    }

    private void BuyProduct(Product purchasedProduct)
    {
        string productName = purchasedProduct.definition.id;
        string receipt = purchasedProduct.receipt;

        IAPData.Add(productName, receipt);

        IssuePurchase(productName, receipt);
    }

    // Проверка покупки на сервере юнити
    private bool ValidateReceipt(string receipt)
    {
        // if (BuildSettings.isDebug)
        //     return true;
//#if !UNITY_EDITOR
//        if ((validator != null) && (BuildSettings.Market == TargetMarket.GOOGLE) || (BuildSettings.Market == TargetMarket.IOS))
//        {
//            try
//            {
//                Debug.Log("Receipt is valid. Contents:" + receipt);
//                var result = validator.Validate(receipt);
//                MyAppMetrica.LogEvent("Purchase Error", "Valid", receipt);
//                //Analytics.PurchaseError("Valid", receipt);
//                if (result == null) return true;

//                foreach (IPurchaseReceipt productReceipt in result)
//                {
//                    if (productReceipt == null) continue;

//                    Debug.Log("productReceipt.productID" + productReceipt.productID);
//                    Debug.Log("productReceipt.purchaseDate" + productReceipt.purchaseDate);
//                    Debug.Log("productReceipt.transactionID" + productReceipt.transactionID);

//                    GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
//                    if (null != google)
//                    {
//                        Debug.Log("google.purchaseState " + google.purchaseState);
//                        Debug.Log("google.purchaseToken " + google.purchaseToken);
//                    }

//                    AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
//                    if (null != apple)
//                    {
//                        Debug.Log(apple.originalTransactionIdentifier);
//                        Debug.Log(apple.subscriptionExpirationDate);
//                        Debug.Log(apple.cancellationDate);
//                        Debug.Log(apple.quantity);
//                    }
//                }
//                return true;
//            }
//            catch (IAPSecurityException)
//            {
//                //Analytics.PurchaseError("Invalid receipt", receipt);
//                MyAppMetrica.LogEvent("Purchase Error", "Invalid", receipt);
//                Debug.Log("Invalid receipt, not unlocking content" + receipt);
//                return false;
//            }
//            catch (Exception)
//            {
//                MyAppMetrica.LogEvent("Purchase Error", "Other Exception", receipt);
//                //Analytics.PurchaseError("Other Exception", receipt);
//                Debug.Log("Exception" + receipt);
//                return false;
//            }
//        }
//#endif

        return true;
    }

    // Выдача покупки
    private void IssuePurchase(string productName, string receipt)
    {
        if ((receipt == null) || (receipt == "")) return;

        if (IAPData.Issue(receipt)) return;       // Проверяем была ли выдана покупка
        //Debug.Log(ValidateReceipt(receipt));
      //  if (!ValidateReceipt(receipt)) return;    // Проверяем правильность на сервере

        IAPData.IssueSet(receipt, true);

        // Тут выдаем покупку
        if (GameManager.instance)
        {
            if (productName.Contains("gempack"))
            {
                GameManager.instance.ShopScript.SuccesBuyGems(productName);
                return;
            }
            if (productName.Contains("test"))
            {
                GameManager.instance.AdminScript.TestAdd();
                return;
            }
            if (productName.Contains("offer"))
                GameManager.instance.Offers.SuccesBuyOffer(productName);
        }

    }

    // Обновить купленные покупки
    public void UpdateIssuePurchase()
    {
        if ((m_Controller == null) || (!isInitialize)) return;

        Debug.Log("UpdateHistoryUI");
        for (int t = 0; t < m_Controller.products.all.Length; t++)
        {
            var item = m_Controller.products.all[t];
            BuyProduct(item);
        }

        IssueBuyProductList();
    }

    private void IssueBuyProductList()
    {
        IAPData iap = IAPData.Load();
        foreach (IAPDataInfo data in iap.buyList)
        {
            if (!data.isIssued)
                IssuePurchase(data.productName, data.receipt);
        }
    }

    // Возвращет иформацию о товаре
    public ProductMetadata GetInfo(string str)
    {
        if ((m_Controller == null) || (!isInitialize) || (m_Controller.products == null)) return null;

        var item = System.Array.Find(m_Controller.products.all, r => (r != null) && (r.definition != null) && (r.definition.id.CompareTo(str) == 0));
        return (item != null) ? item.metadata : null;
    }

    // Возвращет цену товара
    public string GetLocalizedPriceString(string str)
    {
        ProductMetadata product = GetInfo(str);

        if (product == null)
            return "";

        float price = (float)product.localizedPrice;
        float round = Mathf.Round(price);

        if (price - round < 0.01f)
            price = round;

        return price + " " + product.isoCurrencyCode;
    }



    public static bool GetInappStatus(string productName)
    {
        return IAPData.Status(productName);
    }

    // Для указания места покупки в аналитике
    public static void SetPlace(string place)
    {
        instance.place = place;
    }

    public static string GetPlace()
    {
        return instance.place;
    }
}

#else
        using UnityEngine;
using System;
using UnityEngine.Purchasing;
using System.Collections.Generic;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class MyIAP
{
    static MyIAP _instance;
    public static MyIAP instance
    {
        get
        {
            if (_instance == null)
                _instance = new MyIAP();

            return _instance;
        }
    }

    public bool isInitialize = false;

    public void BuyProduct(Product purchasedProduct)
    {
        return;
    }

    public void Init()
    {

    }

    public void UpdateHistoryUI()
    {
    }

    public void Buy(string str)
    {
        return;
    }

    public ProductMetadata GetInfo(string str)
    {
        return null;
    }

    public string GetLocalizedPriceString(string str)
    {
        return "";
    }
}
#endif

public class IAPItem
{
    public bool isDebug = false;
    public string name;
    public ProductType type;

    public IAPItem(string name)
    {
        this.name = name;
        this.type = ProductType.Consumable;
        this.isDebug = false;
    }

    public IAPItem(string name, ProductType type)
    {
        this.name = name;
        this.type = type;
        this.isDebug = false;
    }

    public IAPItem(string name, ProductType type, bool isDebug)
    {
        this.name = name;
        this.type = type;
        this.isDebug = isDebug;
    }
}

[Serializable]
public class IAPData
{
    const string SAVE_PREFIX = "IAPDataInfo";

    public List<IAPDataInfo> buyList = new List<IAPDataInfo>();

    public void Save()
    {
        byte[] data = ObjectToByteArray(this);
        SaveData(path, data);
    }

    public static IAPData Load()
    {
        byte[] data = LoadByte(path);

        if (data != null)
            return ByteArrayObjectTo(data);
        else
            return new IAPData();
    }


    public static void Add(string productName, string receipt)
    {
        IAPData iap = IAPData.Load();
        iap.AddProduct(productName, receipt);
        iap.Save();
    }

    public void AddProduct(string productName, string receipt)
    {
        if ((receipt == null) || (receipt == "")) return;

        IAPDataInfo product = buyList.Find(match =>
        (match.productName.CompareTo(productName) == 0) &&
        (match.receipt.CompareTo(receipt) == 0)
        );

        if (product == null)
            buyList.Add(new IAPDataInfo(productName, receipt));
    }


    public static bool Status(string productName)
    {
        IAPData iap = IAPData.Load();
        return iap.GetStatus(productName);
    }

    public bool GetStatus(string productName)
    {
        IAPDataInfo product = buyList.Find(match => (match.productName.CompareTo(productName) == 0));
        return product != null;
    }


    public static bool Issue(string receipt)
    {
        IAPData iap = IAPData.Load();
        return iap.IssuePurchase(receipt);
    }

    public bool IssuePurchase(string receipt)
    {
        IAPDataInfo product = buyList.Find(match => (match.receipt.CompareTo(receipt) == 0));
        return (product != null) ? product.isIssued : true;
    }


    public static void IssueSet(string receipt, bool isIssued)
    {
        IAPData iap = IAPData.Load();
        iap.IssuePurchaseSet(receipt, isIssued);
        iap.Save();
    }

    public void IssuePurchaseSet(string receipt, bool isIssued)
    {
        IAPDataInfo product = buyList.Find(match => (match.receipt.CompareTo(receipt) == 0));
        if (product != null)
            product.isIssued = isIssued;
    }



    private static void SaveData(string path, byte[] data)
    {
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                BinaryWriter binary = new BinaryWriter(fs);
                binary.Write(data);
                binary.Close();
            }
        }
        catch
        {
           // WorldEvents.SaveToDiskError();
            Debug.Log("Shop item save error");
        }
    }

    private static byte[] LoadByte(string path)
    {
        if (!File.Exists(path))
            return null;

        return File.ReadAllBytes(path);
    }

    private static byte[] ObjectToByteArray(IAPData obj)
    {
        if (obj == null)
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    private static IAPData ByteArrayObjectTo(byte[] arrBytes)
    {
        if ((arrBytes == null) || (arrBytes.Length == 0)) return null;

        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);

        memStream.Seek(0, SeekOrigin.Begin);
        IAPData obj = (IAPData)binForm.Deserialize(memStream);

        return obj;
    }

    private static string path
    {
        get
        {
            return String.Format("{0}/{1}.bin", Application.persistentDataPath, SAVE_PREFIX);
        }
    }
}

[Serializable]
public class IAPDataInfo
{
    public string productName = "";
    public string receipt = "";
    public bool isIssued = false;

    public IAPDataInfo(string productName, string receipt)
    {
        this.productName = productName;
        this.receipt = receipt;
        isIssued = false;
    }
}
