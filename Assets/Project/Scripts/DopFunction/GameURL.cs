public class GameURL{

    private static string urlMoreAppsGoogleApp = "market:apps/dev?id=6681354196935606052";
    private static string urlMoreAppsGoogle = "https://play.google.com/store/apps/dev?id=6681354196935606052";
    private static string urlMoreAppsAmazone = "https://www.amazon.com/s/ref=bl_sr_mobile-apps?_encoding=UTF8&field-brandtextbin=gamefirstmobile&node=2350149011";
    private static string urlMoreAppsIOS = "https://itunes.apple.com/us/developer/valerii-martynov/id1082649117";

    private static string urlReviewGoogleApp = "market://details?id=com.gamefirst.chibisurvivorlord";
    private static string urlReviewGoogle = "https://play.google.com/store/apps/details?id=com.gamefirst.chibisurvivorlord";
    private static string urlReviewIOS = "https://itunes.apple.com/us/app/chibi-survivor-weather-lord-survival/id1227420359";
    private static string urlReviewAmazone = "";




    private static string urlVKHelpApp = "vk://vk.com/islandsurv";
    private static string urlVKHelp = "https://vk.com/chibisurvivor";
    private static string urlVKGroupApp = "vk://vk.com/islandsurv";
    private static string urlVKGroup = "https://vk.com/chibisurvivor";
    private static string urlFBGroupApp = "fb://fb.com/gamefirst.prod";
    public static string urlFBGroup = "http://fb.com/gamefirst.prod";


    public static string urlMoreApps(bool inApp)
    {
        switch (BuildSettings.Market)
        {
            case TargetMarket.GOOGLE: return inApp ? urlMoreAppsGoogleApp : urlMoreAppsGoogle;
            case TargetMarket.AMAZON: return urlMoreAppsAmazone;
            case TargetMarket.IOS: return urlMoreAppsIOS;
        }
        return "";
    }

    public static string urlReview(bool inApp)
    {
        switch (BuildSettings.Market)
        {
            case TargetMarket.GOOGLE: return inApp ? urlReviewGoogleApp : urlReviewGoogle;
            case TargetMarket.AMAZON: return urlReviewAmazone;
            case TargetMarket.IOS: return urlReviewIOS;
        }
        return "";
    }

    public static string urlHelp(bool inApp)
    {
        return inApp ? urlVKHelpApp : urlVKHelp;
    }

    public static string urlVK(bool inApp)
    {
        return inApp ? urlVKGroupApp : urlVKGroup;
    }

    public static string urlFB(bool inApp)
    {
        return inApp ? urlFBGroupApp : urlFBGroup;
    }
}
