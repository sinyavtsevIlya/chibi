using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Выполняет Action в основном потоке.
/// Для использования обязательно закинуть на сцену
/// </summary>
public class DoOnMainThread : MonoBehaviour
{
    // const int ActionForUpdate = 5;

    public readonly static Queue<Action> ExecuteOnMainThread = new Queue<Action>();

    void Awake()
    {
        DoOnMainThread[] thisScript = GameObject.FindObjectsOfType<DoOnMainThread>();

        if (thisScript.Length > 1)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            DontDestroyOnLoad(this.gameObject);

        StartCoroutine(MyFixedUpdate());

    }

    IEnumerator MyFixedUpdate()
    {
        while (true)
        {
            lock (ExecuteOnMainThread)
            {
                while (ExecuteOnMainThread.Count > 0)
                {
                    Action action = ExecuteOnMainThread.Dequeue();
                    if (action != null)
                        action.Invoke();
                    else
                        Debug.LogError("DoOnMainThread action == null");
                }
            }

            yield return new WaitForSecondsRealtime(0.1f);
        }
    }

    public static void AddAction(Action action)
    {
        if (action == null)
            Debug.LogError("AddAction == null");

        lock (ExecuteOnMainThread)
        {
            ExecuteOnMainThread.Enqueue(action);
        }
    }

    public static void Clear()
    {
        lock (ExecuteOnMainThread)
        {
            ExecuteOnMainThread.Clear();
        }
    }

    void OnApplicationFocus(bool focusStatus)
    {
        //MyAppodeal.OnApplicationFocus(focusStatus);
        MyOpenURL.OnApplicationFocus(focusStatus);
    }

    void OnApplicationQuit()
    {
       // MApplication.instance.OnApplicationQuit();
       // Debug.Log("Application ending after " + Time.time + " seconds");
    }

}
