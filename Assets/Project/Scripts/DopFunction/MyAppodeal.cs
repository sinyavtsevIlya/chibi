#pragma warning disable CS0162

using UnityEngine;

public class MyAppodeal
{
    static MyAppodeal _instance;
    public static MyAppodeal instance
    {
        get
        {
            if (_instance == null)            
                _instance = new MyAppodeal();          
            return _instance;
        }
    }


    public bool isInit = false;

    public static bool NoAds = false;

    private static IronSourceProvider _provider;

    public static GameObject Statue = null;

    public void Init()
    {
        if (!BuildSettings.isFree)
            return;
        if (instance.isInit)
            return;

        instance.isInit = true;

        _provider = new IronSourceProvider();

        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            _provider.Initialize(false);

            var appTracking = new AppTracking();
            appTracking.Initialize(UpdateAppTracking);
        });
    }

    private void UpdateAppTracking(bool trackingEnabled)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {

        });
    }

    // *************************************************************

    //#region INTERSTITIAL
    //public static bool statusInterstitial() => _provider.InterstitialAvailable;    

    //public static bool showInterstitial(string path = "Interstitional")
    //{
    //    if (NoAds)
    //        return true;

    //    if (!statusInterstitial()) return false;

    //    _provider.ShowInterstitial(path);

    //    return true;
    //}
    //#endregion
    // *************************************************************

    #region REWARDED_VIDEO
    public static bool statusRewardedVideo() => _provider.RewardedVideoAvailable;

    public static bool showRewardedVideo(string reward)
    {

#if UNITY_EDITOR
        RewardedVideoAction(reward);
        return true;
#endif
        if (!statusRewardedVideo())        
            return false;

        var listener = _provider.CreateRewardedListener();
        listener.UserEarnedReward += () =>
        {
            RewardedVideoAction(reward);
        };
        _provider.ShowRewardedVideo();
        return true;
    }

    private static void RewardedVideoAction(string reward)
    {
        Debug.Log("RewardedVideoAction " + reward);
        switch (reward)
        {
            case "Essence":
                GameManager.instance.PlayerInter.EssenceMinutTimer = 0;
                GameManager.instance.PlayerInter.EssenceSecondTimer = 1;
                GameManager.instance.PlayerInter.StopWaitEssence();
                MyAppMetrica.LogEvent("ADS", "Rewarded Video", "Skip Essence");
                break;
            case "Statue":
                Debug.Log("ENDREWARD");
                GameManager.instance.PlayerInter.Lut(Statue, "Gem", UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), false);
                MyAppMetrica.LogEvent("ADS", "Rewarded Video", "Statue");
                break;
            case "Ressurection":
                GameManager.instance.PlayerInter.Ressuraction1();
                MyAppMetrica.LogEvent("ADS", "Rewarded Video", "Ressurection");
                break;
            case "StartNabor":
                GameManager.instance.Offers.StartNabor();
                break;
            case "RecoverInstrument":
                GameManager.instance.Offers.RecoverInstrument();
                break;
        }
        reward = "";
    }
    #endregion

    // *************************************************************

    #region MonoCallback
    public static void OnApplicationFocus(bool focusStatus)
    {
        //#if NO_ADS || UNITY_EDITOR
        //        return;
        //#else
        //if (!NoAds)
        //{
        //    if (focusStatus)
        //    {
        //        double totalSeconds = (DateTime.Now - instance.dateTime).TotalSeconds;

        //        if ((!instance.isShow) && (totalSeconds > timerDelay))
        //        {
        //            if (GameManager.instance)
        //            {
        //                GameManager.instance.PlayerInter.SetZaglushka(true);
        //            }
        //        }
        //        else
        //        {
        //            instance.isShow = false;
        //        }
        //    }
        //    else
        //    {
        //        instance.dateTime = DateTime.Now;
        //        instance.isShow = instance.isShow || TouchScreenKeyboard.visible;
        //    }
        //}
        //#endif

    }

    public void ShowOnAplication()
    {
        //showInterstitial();
    }


    public static void GamePause()
    {

    }

    public static void GameResume()
    {

    }
    #endregion
}
