﻿#pragma warning disable CS0162 // Unreachable code detected

//#if UNITY_IRON_SOURCE

using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;


public class IronSourceProvider 
{
    private const float RELOAD_DELAY = 30;

    //private static bool HasConnection => Application.internetReachability != NetworkReachability.NotReachable;


    private static IronSourceMediationSettings _developerSettings;
    private static IronSourceMediationSettings developerSettings
    {
        get
        {

            if (_developerSettings == null)
                _developerSettings = Resources.Load<IronSourceMediationSettings>(IronSourceConstants.IRONSOURCE_MEDIATION_SETTING_NAME);

            return _developerSettings;

        }
    }

    private static string AppKey
    {
        get
        {
#if UNITY_ANDROID
            return developerSettings.AndroidAppKey;
#elif UNITY_IOS
            return developerSettings.IOSAppKey;
#endif
        }
    }


    public void Initialize(bool consent = false)
    {
        if (string.IsNullOrEmpty(AppKey))
        {
            Debug.LogError("IronSourceInitilizer Cannot init without AppKey");
            return;
        }

#if UNITY_EDITOR
        return;
#endif

        IronSource.Agent.setConsent(consent);

        //IronSource.Agent.setManualLoadRewardedVideo(true);

        IronSource.Agent.init(AppKey);

        //if (developerSettings.EnableAdapterDebug)
        //{
        //    IronSource.Agent.setAdaptersDebug(true);
        //}

        //if (developerSettings.EnableIntegrationHelper)
        //{
        //    IronSource.Agent.validateIntegration();
        //}

        //AddInterstitialListener();
        //RequestAndLoadInterstitialAd();


        AddRewardedListener();
        RequestAndLoadRewardedVideo();


    }

//    public bool IsAvailable(bool isRewarded, string placement)
//    {
//#if UNITY_EDITOR
//        return true;
//#endif

//        if (!HasConnection) return false;

//        // ...no Ad base class or interface
//        if (isRewarded)
//        {
//            return IronSource.Agent.isRewardedVideoAvailable();
//        }
//        else
//        {
//            return IronSource.Agent.isInterstitialReady();
//        }
//    }

//#region INTERSTITIAL
//    private IronSourceInterstitialListener _interstitialListener;
//    public IronSourceInterstitialListener CreateInterstitialListener()
//    {
//        _interstitialListener?.Dispose();

//        _interstitialListener = new IronSourceInterstitialListener();
//        return _interstitialListener;
//    }

//    private void AddInterstitialListener()
//    {
//        var listener = new IronSourceInterstitialListener();
//        listener.AdLoadFailed += () => { RequestAndLoadInterstitialAdWithDelay(); };
//        listener.AdShowFailed += () => { RequestAndLoadInterstitialAdWithDelay(); };
//        listener.AdClosed += () => { RequestAndLoadInterstitialAd(); };

//        listener.AdOpened += () => { SetPause(); };
//        listener.AdClosed += () => { SetUnPause(); };
//    }

//    private void RequestAndLoadInterstitialAd()
//    {
//        IronSource.Agent.loadInterstitial();
//    }

//    private void RequestAndLoadInterstitialAdWithDelay()
//    {
//        Observable.Timer(TimeSpan.FromSeconds(RELOAD_DELAY)).Subscribe(x =>
//        {
//            RequestAndLoadInterstitialAd();
//        });
//    }
//    public bool InterstitialAvailable => IronSource.Agent.isInterstitialReady();
//    public void ShowInterstitial(string placementID)
//    {
//        //if (IronSource.Agent.isInterstitialReady())
//            IronSource.Agent.showInterstitial(placementID);
//        //else
//        //    Debug.LogError($"No {placementID} placement is not loaded");
//    }
//#endregion

#region REWARDED
    private IronSourceRewardedVideoListener _rewardedListener;
    public IronSourceRewardedVideoListener CreateRewardedListener()
    {
        _rewardedListener?.Dispose();

        _rewardedListener = new IronSourceRewardedVideoListener();
        return _rewardedListener;
    }

    private void AddRewardedListener()
    {
        var listener = new IronSourceRewardedVideoListener();
        listener.AdLoadFailed += () => { RequestAndLoadRewardedVideoAdWithDelay(); };
        listener.AdShowFailed += () => { RequestAndLoadRewardedVideoAdWithDelay(); };
        listener.AdClosed += () => { RequestAndLoadRewardedVideo(); };

        listener.AdOpened += () => { SetPause(); };
        listener.AdClosed += () => { SetUnPause(); };
    }

    private void RequestAndLoadRewardedVideo()
    {
        //IronSource.Agent.loadRewardedVideo();
    }

    private void RequestAndLoadRewardedVideoAdWithDelay()
    {
        Observable.Timer(TimeSpan.FromSeconds(RELOAD_DELAY)).Subscribe(x =>
        {
            RequestAndLoadRewardedVideo();
        });
    }

    public bool RewardedVideoAvailable => IronSource.Agent.isRewardedVideoAvailable();

    public void ShowRewardedVideo()
    {
        IronSource.Agent.showRewardedVideo();
    }
#endregion

#region Pause
    private void SetPause()
    {
        Time.timeScale = 0;

        var audioSources = FindAudioSources();
        foreach (var audioSource in audioSources)
            audioSource.Pause();
    }

    private void SetUnPause()
    {
        Time.timeScale = 1;

        var audioSources = FindAudioSources();
        foreach (var audioSource in audioSources)
            audioSource.UnPause();
    }
    private IEnumerable<AudioSource> FindAudioSources() => GameObject.FindObjectsOfType<AudioSource>().Where(x => x.clip != null);
#endregion

}


//public class IronSourceInterstitialListener : InterstitialListener
//{
//    public IronSourceInterstitialListener()
//    {
//        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
//        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
//        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
//        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
//        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
//        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
//        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
//    }

//    public override void Dispose()
//    {
//        base.Dispose();
//        IronSourceEvents.onInterstitialAdReadyEvent -= InterstitialAdReadyEvent;
//        IronSourceEvents.onInterstitialAdLoadFailedEvent -= InterstitialAdLoadFailedEvent;
//        IronSourceEvents.onInterstitialAdShowSucceededEvent -= InterstitialAdShowSucceededEvent;
//        IronSourceEvents.onInterstitialAdShowFailedEvent -= InterstitialAdShowFailedEvent;
//        IronSourceEvents.onInterstitialAdClickedEvent -= InterstitialAdClickedEvent;
//        IronSourceEvents.onInterstitialAdOpenedEvent -= InterstitialAdOpenedEvent;
//        IronSourceEvents.onInterstitialAdClosedEvent -= InterstitialAdClosedEvent;
//    }

//    // Invoked when the initialization process has failed.
//    // @param description - string - contains information about the failure.
//    private void InterstitialAdLoadFailedEvent(IronSourceError error)
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdLoadFailedEvent ErrorCode: {error.getErrorCode()} Code: {error.getCode()} Description: {error.getDescription()} ");
//            AdLoadFailed?.Invoke();
//        });
//    }
//    // Invoked when the ad fails to show.
//    // @param description - string - contains information about the failure.
//    private void InterstitialAdShowFailedEvent(IronSourceError error)
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdShowFailedEvent ErrorCode: {error.getErrorCode()} Code: {error.getCode()} Description: {error.getDescription()} ");
//            AdShowFailed?.Invoke();
//        });
//    }
//    // Invoked when end user clicked on the interstitial ad
//    private void InterstitialAdClickedEvent()
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdClickedEvent");
//        });
//    }
//    // Invoked when the interstitial ad closed and the user goes back to the application screen.
//    private void InterstitialAdClosedEvent()
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdClosedEvent");
//            AdClosed?.Invoke();
//        });
//    }
//    // Invoked when the Interstitial is Ready to shown after load function is called
//    private void InterstitialAdReadyEvent()
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdReadyEvent");
//        });
//    }
//    // Invoked when the Interstitial Ad Unit has opened
//    private void InterstitialAdOpenedEvent()
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdOpenedEvent");
//            AdOpened?.Invoke();
//        });
//    }
//    // Invoked right before the Interstitial screen is about to open.
//    // NOTE - This event is available only for some of the networks. 
//    // You should treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
//    private void InterstitialAdShowSucceededEvent()
//    {
//        ExecuteMainThread(() =>
//        {
//            Debug.Log($"IronSource InterstitialAdShowSucceededEvent");
//        });
//    }
//}

public class IronSourceRewardedVideoListener : RewardedVideoListener
{

    public IronSourceRewardedVideoListener()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        IronSourceEvents.onRewardedVideoAdLoadFailedEvent += RewardedVideoAdLoadFailedEvent;
        IronSourceEvents.onRewardedVideoAdReadyEvent += RewardedVideoAdReadyEvent;
    }

    public override void Dispose()
    {
        base.Dispose();
        IronSourceEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent -= RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent -= RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent -= RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;
        IronSourceEvents.onRewardedVideoAdLoadFailedEvent -= RewardedVideoAdLoadFailedEvent;
        IronSourceEvents.onRewardedVideoAdReadyEvent -= RewardedVideoAdReadyEvent;
    }

    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy 
    //tasks till the video ad will be closed.
    private void RewardedVideoAdOpenedEvent()
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdOpenedEvent");
            AdOpened?.Invoke();
        });
    }
    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    private void RewardedVideoAdClosedEvent()
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdClosedEvent");
            AdClosed?.Invoke();
        });
    }
    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available. 
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    private void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAvailabilityChangedEvent Available: {available}");
        });
    }

    //Invoked when the user completed the video and should be rewarded. 
    //If using server-to-server callbacks you may ignore this events and wait for 
    // the callback from the  ironSource server.
    //@param - placement - placement object which contains the reward data
    private void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdRewardedEvent Placement: {placement.getPlacementName()}");
            UserEarnedReward?.Invoke();
        });
    }
    //Invoked when the Rewarded Video failed to show
    //@param description - string - contains information about the failure.
    private void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdShowFailedEvent ErrorCode: {error.getErrorCode()} Code: {error.getCode()} Description: {error.getDescription()} ");
            AdShowFailed?.Invoke();
        });
    }

    // ----------------------------------------------------------------------------------------
    // Note: the events below are not available for all supported rewarded video ad networks. 
    // Check which events are available per ad network you choose to include in your build. 
    // We recommend only using events which register to ALL ad networks you include in your build. 
    // ----------------------------------------------------------------------------------------

    //Invoked when the video ad starts playing. 
    private void RewardedVideoAdStartedEvent()
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdStartedEvent");
        });
    }
    //Invoked when the video ad finishes playing. 
    private void RewardedVideoAdEndedEvent()
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdEndedEvent");
        });
    }
    //Invoked when the video ad is clicked. 
    private void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdClickedEvent Placement: {placement.getPlacementName()}");
        });
    }


    private void RewardedVideoAdLoadFailedEvent(IronSourceError error)
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdLoadFailedEvent ErrorCode: {error.getErrorCode()} Code: {error.getCode()} Description: {error.getDescription()} ");
            AdLoadFailed?.Invoke();
        });
    }

    private void RewardedVideoAdReadyEvent()
    {
        ExecuteMainThread(() =>
        {
            Debug.Log($"IronSource RewardedVideoAdReadyEvent");
        });
    }
}
//#endif