﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyLoadScreen : MonoBehaviour {

    public bool _isInitialized = false;

    private void Awake()
    {
        if (!_isInitialized)
        {
            _isInitialized = true;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
