﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour {

    public GameManager GOD;
    //интерфейс туториала
    [Header("Интрфейс туториала:")]
    public GameObject TutorialWindow;
    public Animator TutorialAnimator;
    public Text TutorialText;
    public GameObject Portal, StartTP;
    public GameObject Hand, HandParent, Hand2, Hand2Parent, Hand3Parent,HandRotation, Canvas; //руки на канвасе
    public Transform HandParentInInventory;
    public GameObject HandMountain, TreeLeg, StoneLeg; //руки в мире
    public GameObject EndOfTutorial;
    public Text EndText, BtnText;
    public Animator CurrentAnimator;
    GameObject OldAnimator;
    public RuntimeAnimatorController AppleAnimator, SlotsColor;
    public int TutorialStep=1;
    int OldTutorialStep = 0;
    public GameObject TutorialGift;
    public Text GiftText, GiftGet;

    public GameObject Limit1, Limit2;
    //Вспомогательные
    [Header("Вспомогательные:")]
    public Transform Joystick;
    Quaternion OldRotation;
    public bool NextStep = false;
    public int GetItems = 0;
    public Transform CraftButton, InventoryButton, WeatherButton, SnowButton, GetButton, AttackButton, Indicators;
    public Enemy Wolf;
    public Animator WeaponSlot;
    public Transform InventoryInGame;
    public Transform[] InventoryInGameSlots = new Transform[3];
    public Transform TutorialSquare; //квадрат с дыркой






    public static float AngleAroundAxisZ(Vector3 dirA, Vector3 dirB)
    {
        return AngleAroundAxis(dirA, dirB, Vector3.up);
    }

    public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
    {
        dirA = dirA - Vector3.Project(dirA, axis);
        dirB = dirB - Vector3.Project(dirB, axis);

        float angle = Vector3.Angle(dirA, dirB);
        return angle * (Vector3.Dot(axis, Vector3.Cross(dirA, dirB)) < 0 ? -1 : 1);
    }
    IEnumerator ShowBtn(Transform Btn)  //мигание кнопки
    {
        Btn.localScale = new Vector3(1.2f, 1.2f, 1.2f);
       // BtnImage.color = new Color(0.78f, 0.78f, 0.78f, 1);
        yield return new WaitForSeconds(0.5f);
        Btn.localScale = new Vector3(1f, 1f, 1f);
        //  BtnImage.color = new Color(1f, 1f, 1f, 1);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ShowBtn(Btn));
    }

    void Update()             //проверяем выполнил игрок что от него требовали
    {
            switch (TutorialStep)
            {
                case 1: //проведи джойстиком
                    float h = CrossPlatformInputManager.GetAxis("Horizontal");
                    float v = CrossPlatformInputManager.GetAxis("Vertical");
                    if (h != 0 || v != 0)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                    }
                    break;
                case 2: //поверни камеру
                    if (GOD.CameraScript.PlayerCamera.transform.rotation != OldRotation)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                    }
                    break;
                case 3:  //подойти к яблоне
                    if (GOD.PlayerInter.GetButton.activeSelf)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                    }
                    break;
                case 4:  //собрать яблоки
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 5: //срубить дерево
                    if (GOD.PlayerInter.ObjectTarget && GOD.PlayerInter.ObjectTarget.name == "AppleTree")
                    {
                        if (CurrentAnimator.enabled == false)
                            CurrentAnimator.enabled = true;
                    }
                    else
                    {
                        if (CurrentAnimator.enabled == true)
                        {
                            CurrentAnimator.enabled = false;
                            AttackButton.localScale = new Vector3(1, 1, 1);
                        }
                    }
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 6: //подобрать лут
                    if (GetItems == 3)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        GetItems = 0;
                    }
                    break;
                case 7: //открыть крафт
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 8: //выбрать кирку
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 9://скрафтить кирку
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 10://открыть инвентарь
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 11://перетянуть и надеть кирку
                if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 12: //сломать камни подобрать лут 
                    if (GOD.PlayerInter.ObjectTarget && GOD.PlayerInter.ObjectTarget.name == "Stone")
                    {
                        if (CurrentAnimator.enabled == false)
                            CurrentAnimator.enabled = true;
                    }
                    else
                    {
                        if (CurrentAnimator.enabled == true)
                        {
                            CurrentAnimator.enabled = false;
                            AttackButton.localScale = new Vector3(1, 1, 1);
                        }
                    }
                    if (GetItems == 2)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        GetItems = 0;
                    }
                    break;
                case 13: //откыть окно смены погоды
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                /*  case 15: //изменить погоду
                      if (NextStep)
                      {
                          TutorialStep++;
                          ShowTutorialStep();
                          NextStep = false;
                      }
                      break;*/
                case 15: //после измения погоды
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 16: //поднятся на гору
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 17: //получить подарок
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 18: //убить врага
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 19: //открыть инвентарь
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
                case 20: //съесть яблоко
                    if (NextStep)
                    {
                        TutorialStep++;
                        ShowTutorialStepTurn();
                        NextStep = false;
                    }
                    break;
            }
    }
    public bool DoNotSendMentrica = false;
    public void ShowTutorialStep()
    {
        switch (TutorialStep)
        {
            case 1:      //проведи джойстиком
                MyAppMetrica.LogEvent("Tutorial", "Step_1", "Start");
                Portal.SetActive(false);
                GOD.PlayerInter.AttackEnemy.interactable = false;
                CraftButton.GetComponent<Button>().interactable = false;
                InventoryButton.GetComponent<Button>().interactable = false;
                WeatherButton.GetComponent<Button>().interactable = false;
                // GOD.PlayerInter.MobileJoystik.GetComponent<PanelJoystick>().UseInviz = false;
                GOD.PlayerInter.MobileJoystik.GetComponent<PanelJoystick>().basejoystick.gameObject.SetActive(true);
                GOD.EnviromentContr.CTut.SetWeather();
                Wolf.Init();
                Wolf.AddEffects();
                Wolf.maxHP = 50;
                Wolf.HP = 50;

                RectTransform rt = Joystick.GetChild(0).GetChild(0).GetComponent<RectTransform>();
                Hand.transform.position = rt.position+new Vector3(rt.sizeDelta.x,0,0);
                 HandParent.SetActive(true);
                 HandParent.GetComponent<Animator>().Play("HandJoystik");
                break;
            case 2: //поверни камеру
                GOD.PlayerInter.MobileJoystik.GetComponent<PanelJoystick>().UseInviz = true;
                MyAppMetrica.LogEvent("Tutorial", "Step_2", "Start");
                HandParent.SetActive(false);
                OldRotation = GOD.CameraScript.PlayerCamera.transform.rotation;

                HandRotation.SetActive(true);
                HandRotation.GetComponent<Animator>().Play("HandRotation");
                break;
            case 3: //подойти к яблоне
                if(!DoNotSendMentrica)
                    MyAppMetrica.LogEvent("Tutorial", "Step_3", "Start");
                DoNotSendMentrica = false;
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
                TutorialSquare.gameObject.SetActive(false);
                HandRotation.SetActive(false);
                TreeLeg.SetActive(true);
                Limit1.SetActive(false);
                break;
            case 4:  //собрать яблоки
                MyAppMetrica.LogEvent("Tutorial", "Step_4", "Start");
                TreeLeg.SetActive(false);
                GetButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = GetButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                TutorialSquare.transform.position = GetButton.transform.position;
                TutorialSquare.transform.localScale = new Vector3(5, 5, 5);
                TutorialSquare.gameObject.SetActive(true);
                StartCoroutine(ScaleTutorialSquare());
                break;
            case 5:  //срубить дерево
                MyAppMetrica.LogEvent("Tutorial", "Step_5", "Start");
                Destroy(CurrentAnimator);
                GetButton.localScale = new Vector3(1, 1, 1);
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);

                GOD.PlayerInter.AttackEnemy.interactable = true;

                AttackButton.gameObject.AddComponent<Animator>();
                 CurrentAnimator = AttackButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                TutorialSquare.transform.position = AttackButton.transform.position;
                TutorialSquare.transform.localScale = new Vector3(5, 5, 5);
                TutorialSquare.gameObject.SetActive(true);
                StartCoroutine(ScaleTutorialSquare());
                break;
            case 6: //подобрать лут
                    MyAppMetrica.LogEvent("Tutorial", "Step_6", "Start");
                Destroy(CurrentAnimator);
                AttackButton.localScale = new Vector3(1, 1, 1);
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
                TutorialSquare.gameObject.SetActive(false);
                break;
            case 7: //открыть крафт
                if (!DoNotSendMentrica)
                    MyAppMetrica.LogEvent("Tutorial", "Step_7", "Start");
                DoNotSendMentrica = false;
                CraftButton.GetComponent<Button>().interactable = true;
                CraftButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = CraftButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                TutorialSquare.transform.position = CraftButton.transform.position;
                TutorialSquare.transform.localScale = new Vector3(5, 5, 5);
                TutorialSquare.gameObject.SetActive(true);
                StartCoroutine(ScaleTutorialSquare());
                break;
            case 8: //выбрать кирку
                MyAppMetrica.LogEvent("Tutorial", "Step_8", "Start");
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
                TutorialSquare.gameObject.SetActive(false);
                Destroy(CurrentAnimator);
                CraftButton.localScale = new Vector3(1, 1,1);

                GameObject g = GOD.CraftContr.UsialCraft.RecipeSlotsParent.GetChild(0).GetChild(0).gameObject;
                g.gameObject.AddComponent<Animator>();
                CurrentAnimator = g.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;
                break;
            case 9://скрафтить кирку
                MyAppMetrica.LogEvent("Tutorial", "Step_9", "Start");
                Destroy(CurrentAnimator);
                GOD.CraftContr.UsialCraft.RecipeSlotsParent.GetChild(0).GetChild(0).localScale = new Vector3(0.7f, 0.7f, 0.7f);

                GOD.CraftContr.UsialCraft.CraftButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = GOD.CraftContr.UsialCraft.CraftButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;
                break;
            case 10: //открыть инвентарь
                if (!DoNotSendMentrica)
                    MyAppMetrica.LogEvent("Tutorial", "Step_10", "Start");
                DoNotSendMentrica = false;
                Destroy(CurrentAnimator);

                GameObject g1 = GOD.InventoryContr.FindItem("Apple");
                if (g1)
                    g1.GetComponent<DragHandler>().enabled = false;

                InventoryButton.GetComponent<Selectable>().interactable = true;
                InventoryButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = InventoryButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                TutorialSquare.transform.position = InventoryButton.transform.position;
                TutorialSquare.transform.localScale = new Vector3(5, 5, 5);
                TutorialSquare.gameObject.SetActive(true);
                StartCoroutine(ScaleTutorialSquare());
                break;
            case 11: //перетянуть кирку
                MyAppMetrica.LogEvent("Tutorial", "Step_11", "Start");
                Destroy(CurrentAnimator);
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
                TutorialSquare.gameObject.SetActive(false);
                InventoryButton.localScale = new Vector3(1, 1, 1);

                g1 = GOD.InventoryContr.FindItem("WoodPick");
                if (g1)
                {
                    g1.gameObject.AddComponent<Animator>();
                    CurrentAnimator = g1.GetComponent<Animator>();
                    CurrentAnimator.runtimeAnimatorController = AppleAnimator;
                    WeaponSlot.runtimeAnimatorController = SlotsColor;
                    OldAnimator = g1.transform.parent.gameObject;
                    OldAnimator.AddComponent<Animator>();
                    Animator a =OldAnimator.GetComponent<Animator>();
                    a.runtimeAnimatorController = SlotsColor;
                   a.updateMode = AnimatorUpdateMode.UnscaledTime;
                }
                //     GOD.PlayerInter.OpenInventory();
               /* for (int i = 0; i < InventoryInGameSlots.Length; i++)
                {
                    InventoryInGameSlots[i].enabled=true;
                }*/
                break;
            case 12: //разбить камень
                MyAppMetrica.LogEvent("Tutorial", "Step_12", "Start");
                g1 = GOD.InventoryContr.FindItem("WoodPick");
                if(g1)
                {
                    g1.transform.localScale = new Vector3(1, 1, 1);
                }
                Destroy(CurrentAnimator);
                Destroy(OldAnimator.GetComponent<Animator>());
                WeaponSlot.runtimeAnimatorController = null;
                WeaponSlot.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                OldAnimator.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                GOD.PlayerInter.CloseInventory();

                AttackButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = AttackButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;
                StoneLeg.SetActive(true);
                Limit2.SetActive(false);

                break;
            case 13: //открыть окно смены погоды
                MyAppMetrica.LogEvent("Tutorial", "Step_13", "Start");
                Destroy(CurrentAnimator);
                StoneLeg.SetActive(false);

                WeatherButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = WeatherButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                WeatherButton.GetComponent<Button>().interactable = true;
                NextStep = false;
                bool NeedToAdd = true;
                for (int x = 0; x < GOD.InventoryContr.AllIcons.Count; x++)
                {
                    if (GOD.InventoryContr.AllIcons[x].name == "Icon_EssenceIce")
                    {
                        NeedToAdd = false;
                        break;
                    }
                }
                if(NeedToAdd)
                GOD.InventoryContr.AddToInventory("EssenceIce", 1);

                TutorialSquare.transform.position = WeatherButton.transform.position;
                TutorialSquare.transform.localScale = new Vector3(5, 5, 5);
                TutorialSquare.gameObject.SetActive(true);
                StartCoroutine(ScaleTutorialSquare());
                break;
            case 14: //сменить погоду
                MyAppMetrica.LogEvent("Tutorial", "Step_14", "Start");
                Destroy(CurrentAnimator);
                WeatherButton.localScale = new Vector3(1, 1, 1);
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
                TutorialSquare.gameObject.SetActive(false);

                SnowButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = SnowButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;
                break;
            case 15: //ожидание смены погоды
                MyAppMetrica.LogEvent("Tutorial", "Step_15", "Start");
                Destroy(CurrentAnimator);
                SnowButton.localScale = new Vector3(1, 1, 1);
                break;
            case 16: //поднятся на гору
                MyAppMetrica.LogEvent("Tutorial", "Step_16", "Start");
                break;
            case 17: //награда
                MyAppMetrica.LogEvent("Tutorial", "Step_17", "Start");
                OpenGift();
                break;
            case 18: //убей врага
                MyAppMetrica.LogEvent("Tutorial", "Step_18", "Start");
              
                AttackButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = AttackButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;
                break;
            case 19: //открой инвентарь
                if (!DoNotSendMentrica)
                    MyAppMetrica.LogEvent("Tutorial", "Step_19", "Start");
                Destroy(CurrentAnimator);

                InventoryButton.gameObject.AddComponent<Animator>();
                CurrentAnimator = InventoryButton.GetComponent<Animator>();
                CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                TutorialSquare.transform.position = InventoryButton.transform.position;
                TutorialSquare.transform.localScale = new Vector3(5, 5, 5);
                TutorialSquare.gameObject.SetActive(true);
                StartCoroutine(ScaleTutorialSquare());
                break;
            case 20: //съешь яблоко
                if (!DoNotSendMentrica)
                    MyAppMetrica.LogEvent("Tutorial", "Step_20", "Start");
                DoNotSendMentrica = false;
                TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
                TutorialSquare.gameObject.SetActive(false);
                Destroy(CurrentAnimator);
                g1 = GOD.InventoryContr.FindItem("Apple");
                if (g1)
                {
                    g1.AddComponent<Animator>();
                    CurrentAnimator = g1.GetComponent<Animator>();
                    CurrentAnimator.runtimeAnimatorController = AppleAnimator;

                    RectTransform currt = g1.GetComponent<RectTransform>();
                    Hand3Parent.transform.SetParent(g1.transform);
                    Hand3Parent.transform.localPosition = new Vector3(0, 0, 0)+new Vector3(currt.sizeDelta.x/2, -currt.sizeDelta.y/2, 0);
                    Hand3Parent.transform.SetParent(HandParentInInventory);
                    Hand3Parent.SetActive(true);
                }
                break;
            case 21: //последнее окно
                MyAppMetrica.LogEvent("Tutorial", "Step_21", "Start");
                Hand3Parent.SetActive(false);
                GOD.PlayerInter.CloseInventory();
                Portal.SetActive(true);
                /* Hand3.SetActive(false);
                 Hand3Parent.SetActive(false);
                 EndText.text = Localization.instance.getTextByKey("#Сongratulations") + "\n" + Localization.instance.getTextByKey("#Tutorial23");
                 BtnText.text = Localization.instance.getTextByKey("#Menu1");
                 EndOfTutorial.SetActive(true);
                 TutorialWindow.SetActive(false);
                 Hand2.SetActive(false);*/
                break;
        }
    }

    public void StopTutorialBlack()
    {
        TutorialSquare.transform.localScale = new Vector3(1, 1, 1);
        TutorialSquare.gameObject.SetActive(false);
    }
    public void ShowTutorialStepTurn()
    {
        StopAllCoroutines();
        StartCoroutine(ShowText(Localization.instance.getTextByKey("#Tutorial" + TutorialStep)));
        ShowTutorialStep();
    }
    IEnumerator ScaleTutorialSquare()//анимируем квадрат с дыркой
    {
        while(TutorialSquare.transform.localScale.x>1)
        {
            TutorialSquare.transform.localScale -= new Vector3(0.4f, 0.4f, 0.4f);
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void OpenGift()
    {
        GiftText.text = Localization.instance.getTextByKey("#Tutorial17");
         GiftGet.text = Localization.instance.getTextByKey("#Get");
        TutorialGift.SetActive(true);
        
    }
    public void CloseGift()
    {
        NextStep = true;
        TutorialGift.SetActive(false);
        GOD.InventoryContr.AddToInventory("WoodSword", 1);
        GOD.PlayerInter.ShowNewItem("WoodSword", 1);
        GOD.InventoryContr.AddToInventory("WoodAxe", 1);
       GOD.PlayerInter.ShowNewItem("WoodAxe", 1);
        GameObject g = GOD.InventoryContr.FindItem("WoodSword");
        if (g)
        {
            g.GetComponent<ClothesScript>().BtnDown();
        }
    }
    public void StartTeleport()
    {
        GOD.PlayerContr.IsStop = true;
        StartTP.SetActive(true);
        StartCoroutine(WaitTeleport());
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.Portal);
    }
    IEnumerator WaitTeleport()
    {
        yield return new WaitForSeconds(3);
        LoadGame();
    }
    public void LoadGame()
    {
        GOD.Save.SaveTutorial();
        PlayerPrefs.SetString("ChibiLevel", "Game");
        SceneManager.LoadScene("Load");
    }
    public void SkipTutorial()
    {
        if(GOD.InventoryContr.InvnentoryGetCount("WoodSword")==0)
        GOD.InventoryContr.AddToInventory("WoodSword", 1);
        if (GOD.InventoryContr.InvnentoryGetCount("WoodAxe") == 0)
            GOD.InventoryContr.AddToInventory("WoodAxe", 1);
        if (GOD.InventoryContr.InvnentoryGetCount("WoodPick") == 0)
            GOD.InventoryContr.AddToInventory("WoodPick", 1);
        GOD.PlayerInter.OpenInventory();
        GOD.PlayerInter.CloseInventoryImmediatly();
        GameObject g =GOD.InventoryContr.FindItem("WoodSword");
        IconInGame[] I = new IconInGame[3];
        I[0] = InventoryInGameSlots[0].transform.GetChild(0).GetComponent<IconInGame>();
        I[1] = InventoryInGameSlots[1].transform.GetChild(0).GetComponent<IconInGame>();
        I[2] = InventoryInGameSlots[2].transform.GetChild(0).GetComponent<IconInGame>();
        if(g.GetComponent<InfoScript>().SlotInGame == null)
        {
            for(int i=0; i<I.Length;i++)
            {
                if(!I[i].InGameImage.gameObject.activeSelf)
                {
                    I[i].SetIcon(g, InventoryInGameSlots[i].transform.GetComponent<Slot>());
                    break;
                }
            }
        }
        g = GOD.InventoryContr.FindItem("WoodAxe");
        if (g.GetComponent<InfoScript>().SlotInGame == null)
        {
            for (int i = 0; i < I.Length; i++)
            {
                if (!I[i].InGameImage.gameObject.activeSelf)
                {
                    I[i].SetIcon(g, InventoryInGameSlots[i].transform.GetComponent<Slot>());
                    break;
                }
            }
        }
        g = GOD.InventoryContr.FindItem("WoodPick");
        if (g.GetComponent<InfoScript>().SlotInGame == null)
        {
            for (int i = 0; i < I.Length; i++)
            {
                if (!I[i].InGameImage.gameObject.activeSelf)
                {
                    I[i].SetIcon(g, InventoryInGameSlots[i].transform.GetComponent<Slot>());
                    break;
                }
            }
        }

        LoadGame();
    }
    IEnumerator ShowText(string s)//постепенно выводим текст
    {
        TutorialText.text = "";
        for (int i = 0; i < s.Length; i++)
        {
            TutorialText.text += s[i];
            yield return StartCoroutine(WaitForSecondsCor(0.05f));
        }
    }

    private IEnumerator WaitForSecondsCor(float t)
    {
        float targetTime = Time.realtimeSinceStartup + t;

        while (Time.realtimeSinceStartup < targetTime)
        {
            yield return null;
        }
    }
}
