﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer : MonoBehaviour {
    public GameManager GOD;


    private void Start()
    {
        GameObject g =GameObject.Find("GameManager");
        if (g)
            GOD = g.GetComponent<GameManager>();
        if(GOD)
        StartCoroutine(RotateAtPlayer());
    }

    IEnumerator RotateAtPlayer()
    {
        yield return new WaitForSeconds(0.2f);
        transform.LookAt(GOD.PlayerCam.transform);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 90);
        StartCoroutine(RotateAtPlayer());
    }
}
