﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class MenuScript : MonoBehaviour
{

    public PlayerProgress Progress;
    public SettingsController Settings;
    public Camera MenuCamera;
    public RectTransform FirstWindow;
    bool CreateNewSave = false;

    //СОХРАНЕНИЯ
    public bool[] AllSaves = new bool[6]; //какие слоты сохранения заняты
    int CurrentGame = -1;         //есть ли текущее сохранение
    int NextGame = -1;
    public SaveSlotScript[] SaveSlots = new SaveSlotScript[6];
    public Button Ok, No;

    //КНОПКИ
    [Header("Кнопки:")]
    public Button PlayBtn;
    public Button NewBtn, ContinueBtn, LoadBtn, DeleteBtn, SettingsBtn;
    public Text PlayBtnTxt, NewBtnTxt, ContinueBtnTxt, LoadBtnTxt, ChooseTxt1, ChooseTxt2;
    public Image ChooseIm1, ChooseIm2;
    public GameObject Choose, CloseChoose;
    public GameObject AskWindow;
    public Text AskQuestion;
    public GameObject ListBtn, AchivmentBtn; //список достижений и ачивки
    bool ChosePlayer = false;
    //АУДИО
    [Header("Аудио:")]
    public AudioSource MenuMusic;
    public AudioSource MenuSnd, Firecamp;
    public AudioClip BtnSound;
    public AudioClip ChooseBoy, ChooseGrl;

    //ПЕРСОНАЖИ
    [Header("Персонажи:")]
    public Transform PlayerBoy, PlayerGirl;
    public RuntimeAnimatorController BoyAnimatorController, GirlAnimatorController;
    Animator BoyAnimator, GirlAnimator;
    string CurrentMale;

    //СКИНЫ
    [Header("Скины:")]
    public GameObject[] BoySkins;
    public GameObject[] GirlSkins;

    //ПРИЧЕСКИ
    [Header("Прически:")]
    public GameObject[] GirlHairsPrefabs = new GameObject[11];
    Vector3[] GirlHairsPosition = new Vector3[11];
    public GameObject[] BoyHairsPrefabs = new GameObject[10];
    Vector3[] BoyHairsPosition = new Vector3[10];
    public Material BoyHairMaterial;
    public Material GirlHairMaterial;
    Transform BoyHead, GirlHead;
    Color[] HairColors;

    //ПОГОДА
    [Header("Погода:")]
    public Material Ground;
    public Texture SnowGround, RainGround, SunGround;
    public Material Plant1, Plant2, Stones;
    public GameObject Snow, Rain;

    //НАСТРОЙКИ
    [Header("Настройки:")]
    public RectTransform NoConnectionPos1, NoConnectionPos2;

    //ПОЛИТИКА
    [Header("Политика:")]
    public GameObject PRIVACY_POLICY;

    public void Del()
    {
        PlayerPrefs.DeleteAll();

    }
    public void Init()
    {
        BoyHairsPosition[0] = new Vector3(-0.596f, 0.003f, -0.009f);
        BoyHairsPosition[1] = new Vector3(-0.555f, -0.051f, -0.03f);
        BoyHairsPosition[2] = new Vector3(-0.559f, -0.05f, -0.019f);
        BoyHairsPosition[3] = new Vector3(-0.55f, -0.002f, -0.011f);
        BoyHairsPosition[4] = new Vector3(-0.541f, -0.075f, -0.017f);
        BoyHairsPosition[5] = new Vector3(-0.519f, -0.109f, -0.054f);
        BoyHairsPosition[6] = new Vector3(-0.529f, -0.027f, -0.064f);
        BoyHairsPosition[7] = new Vector3(-0.579f, -0.059f, -0.016f);
        BoyHairsPosition[8] = new Vector3(-0.588f, -0.059f, 0.011f);
        BoyHairsPosition[9] = new Vector3(-0.627f, -0.044f, -0.018f);

        GirlHairsPosition[0] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[1] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[2] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[3] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[4] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[5] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[6] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[7] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[8] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[9] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[10] = new Vector3(0.002f, -0.142f, -0.012f);

        HairColors = new Color[28];

        HairColors[0] = new Color(0.15f, 0.15f, 0.16f); // черный
        HairColors[1] = new Color(0.09f, 0.17f, 0.47f); // полуночный черный
        HairColors[2] = new Color(0f, 0.22f, 1f);  // синий
        HairColors[3] = new Color(0.19f, 0.09f, 0.5f);//персидский индиго
        HairColors[4] = new Color(0.32f, 0.11f, 0.76f); //сине-лиловый
        HairColors[5] = new Color(0.54f, 0.2f, 1f); // персидский синий
        HairColors[6] = new Color(0.53f, 0.39f, 1f);  //умеренный аспидно синий
        HairColors[7] = new Color(0.6f, 0.22f, 0.73f);  //темная орхидея 153/155/188
        HairColors[8] = new Color(0.52f, 0.19f, 0.42f);  //яркий красновато-пурпурный 133/5/108
        HairColors[9] = new Color(1f, 0.37f, 0.88f);  //звезды в шоке 255/94/224
        HairColors[10] = new Color(0.9f, 0.64f, 0.87f);  //орхидея крайола 231/163/223
        HairColors[11] = new Color(1f, 0f, 0.8f);  //пурпурная пиццца 255/0/206
        HairColors[12] = new Color(1f, 0f, 0f);  //красный 255/0/0
        HairColors[13] = new Color(0.51f, 0f, 0f);  //коричнево-малиновый 131/0/0
        HairColors[14] = new Color(0.35f, 0.2f, 0.2f);  //каштнаово-коричневый 90/51/51
        HairColors[15] = new Color(1f, 0.35f, 0f);  //оранжевый 255/90/0
        HairColors[16] = new Color(1f, 0.57f, 0f);  //яркий оранжево-желтый 255/146/0
        HairColors[17] = new Color(1f, 0.44f, 0.44f);  //горько сладкий 255/113/113
        HairColors[18] = new Color(0.96f, 0.73f, 0.46f);  //красный песок 244/188/118
        HairColors[19] = new Color(1f, 0.76f, 0f);  //янтарный 255/195/0
        HairColors[20] = new Color(0.98f, 0.97f, 0.44f);  //лимонный 251/247/111
        HairColors[21] = new Color(0.36f, 1f, 0f);  //ярко зеленый 93/255/0
        HairColors[22] = new Color(0.05f, 0.57f, 0.15f);  //индийский зеленый 14/146/37
        HairColors[23] = new Color(0.4f, 1f, 0.5f);  //кричащий зеленый  104/255/130
        HairColors[24] = new Color(0f, 0.96f, 1f);  //цвет морской волны 0/244/255
        HairColors[25] = new Color(0.51f, 0.82f, 0.99f);  //небесный 132/208/253
        HairColors[26] = new Color(0f, 0.62f, 1f);  //цвет твиттера 0/160/255
        HairColors[27] = new Color(0f, 0.3f, 0.5f);  //насыщенный синий 0/80/128

    }

    void Start()
    {
        SetWeather();
        MyAppodeal.instance.Init();
        Localization.instance.SetLanguage(PlayerPrefs.GetString("ChibiLanguage"));
        MyIAP.instance.Init();
        if (BuildSettings.isDebug)
        {
            //gameObject.AddComponent<Consolation.Console>();
            // DeleteBtn.gameObject.SetActive(true);
        }
        InitLanguage();
        if (PlayerPrefs.HasKey("ChibiAllSaves"))        //какие слоты заняты сейвами
        {
            AllSaves = PlayerPrefsX.GetBoolArray("ChibiAllSaves");
            for (int i = 0; i < AllSaves.Length; i++)
            {
                if (AllSaves[i])
                {
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < AllSaves.Length; i++)
                AllSaves[i] = false;
            PlayerPrefsX.SetBoolArray("ChibiAllSaves", AllSaves);
        }

        for (int i = 0; i < AllSaves.Length; i++)
        {
            SaveSlots[i].Init();
            if (AllSaves[i])
            {
                Sprite currentSp;
                SaveManager.LoadFromDisk(i, out SaveManager.AllSaveData[i], out currentSp);
                if (SaveManager.AllSaveData[i] == null)
                    AllSaves[i] = false;
                else
                {
                    SaveSlots[i].CurrentDay.text = Localization.instance.getTextByKey("#Day") + " " + SaveManager.AllSaveData[i].Day;
                    SaveSlots[i].Time.text = SaveManager.AllSaveData[i].SaveDate;
                    SaveSlots[i].MyIcon.transform.parent.gameObject.SetActive(true);
                    SaveSlots[i].SetMyIcon(currentSp);

                }
            }
        }
        PlayerPrefsX.SetBoolArray("ChibiAllSaves", AllSaves);

        // MenuMusic.volume = PlayerPrefs.GetFloat("ChibiMusicVolume");
        // MenuSnd.volume = MenuMusic.volume;
        // Firecamp.volume = MenuMusic.volume;

        Settings.Init(null, this);
#if !NO_GPGS || !NO_GC
        if (!Social.localUser.authenticated)
            Settings.LoginPlayServises();
        ListBtn.SetActive(true);
        AchivmentBtn.SetActive(true);
#else
        ListBtn.SetActive(false);
        AchivmentBtn.SetActive(false);
#endif
        Init();

        //Лицензия
        // AN_LicenseManager.OnLicenseRequestResult += LicenseRequestResult;
        // AN_LicenseManager.Instance.StartLicenseRequest(AndroidNativeSettings.Instance.base64EncodedPublicKey);

        //СКИН ПАРНЯ__________________________________________________
        int x = 0;
        if (PlayerPrefs.HasKey("ChibiCurrentBoySkinNumber"))    // текущий скин парня
            x = PlayerPrefs.GetInt("ChibiCurrentBoySkinNumber");
        else
            x = 0;
        GameObject NewSkin = Instantiate(BoySkins[x]);
        NewSkin.transform.SetParent(PlayerBoy.transform);
        NewSkin.transform.localPosition = new Vector3(0, 0, 0);
        NewSkin.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        Destroy(NewSkin.GetComponent<PlayerAnimationController>());
        NewSkin.GetComponent<Animator>().runtimeAnimatorController = BoyAnimatorController;
        BoyAnimator = NewSkin.GetComponent<Animator>();
        BoyHead = NewSkin.transform.Find("Base Human").Find("Base HumanAss for elements").Find("Base HumanSpine1").Find("Base HumanSpine2").Find("Base HumanRibcage").Find("Base HumanNeck").Find("Base HumanHead");

        //ПРИЧЕСКА ПАРНЯ__________________________________________________
        x = 0;
        if (PlayerPrefs.HasKey("ChibiCurrentBoyHairNumber"))    // текущая прическа парня
            x = PlayerPrefs.GetInt("ChibiCurrentBoyHairNumber");
        else
            x = 0;
        GameObject CurrentHair = Instantiate(BoyHairsPrefabs[x]);
        CurrentHair.transform.SetParent(BoyHead);
        CurrentHair.transform.localPosition = BoyHairsPosition[x];
        CurrentHair.transform.localRotation = BoyHairsPrefabs[x].transform.rotation;

        //ЦВЕТ ПРИЧЕСКИ ПАРНЯ_________________________________________________________
        x = 1;
        if (PlayerPrefs.HasKey("ChibiCurrentBoyHairColorNumber"))    // текущий цвет парня
            x = PlayerPrefs.GetInt("ChibiCurrentBoyHairColorNumber");
        PlayerBoy.GetChild(0).GetChild(1).GetComponent<Renderer>().material.SetColor("_Color", HairColors[x]);
        BoyHairMaterial.SetColor("_Color", HairColors[x]);

        //СКИН ДЕВУШКИ__________________________________________________
        x = 0;
        if (PlayerPrefs.HasKey("ChibiCurrentGirlSkinNumber"))    // текущий скин девушки
            x = PlayerPrefs.GetInt("ChibiCurrentGirlSkinNumber");
        else
            x = 0;
        NewSkin = Instantiate(GirlSkins[x]);
        NewSkin.transform.SetParent(PlayerGirl.transform);
        NewSkin.transform.localPosition = new Vector3(0, 0, 0);
        NewSkin.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        Destroy(NewSkin.GetComponent<PlayerAnimationController>());
        NewSkin.GetComponent<Animator>().runtimeAnimatorController = GirlAnimatorController;
        GirlAnimator = NewSkin.GetComponent<Animator>();
        GirlHead = NewSkin.transform.Find("Base Human").Find("Base HumanAss for elements").Find("Base HumanSpine1").Find("Base HumanSpine2").Find("Base HumanRibcage").Find("Base HumanNeck").Find("Base HumanHead");

        //ПРИЧЕСКА ДЕВУШКИ__________________________________________________
        x = 0;
        if (PlayerPrefs.HasKey("ChibiCurrentGirlHairNumber"))    // текущая прическа девушка
            x = PlayerPrefs.GetInt("ChibiCurrentGirlHairNumber");
        else
            x = 0;
        CurrentHair = Instantiate(GirlHairsPrefabs[x]);
        CurrentHair.transform.SetParent(GirlHead);
        CurrentHair.transform.localPosition = GirlHairsPosition[x];
        CurrentHair.transform.localRotation = GirlHairsPrefabs[x].transform.rotation;

        //ЦВЕТ ПРИЧЕСКИ ДЕВУШКИ_________________________________________________________
        x = 13;
        if (PlayerPrefs.HasKey("ChibiCurrentGirlHairColorNumber"))    // текущий цвет парня
            x = PlayerPrefs.GetInt("ChibiCurrentGirlHairColorNumber");
        PlayerGirl.GetChild(0).GetChild(1).GetComponent<Renderer>().material.SetColor("_Color", HairColors[x]);
        GirlHairMaterial.SetColor("_Color", HairColors[x]);

        //ПАРАМЕТРЫ________________________________________________________________

        if (PlayerPrefs.HasKey("ChibiCurrentGame"))        //какое текущее сохранение
        {
            CurrentGame = PlayerPrefs.GetInt("ChibiCurrentGame");
            if (!AllSaves[CurrentGame])
                CurrentGame = -1;
        }
        PlayBtn.gameObject.SetActive(false);
        NewBtn.gameObject.SetActive(true);
        LoadBtn.gameObject.SetActive(true);
        if (CurrentGame > -1)
        {
            ContinueBtn.gameObject.SetActive(true);
            CurrentMale = PlayerPrefs.GetString("ChibiMALE" + CurrentGame);
            StandPlayer(CurrentMale);
        }
        else
            ContinueBtn.gameObject.SetActive(false);

        ChooseIm1.color = new Color(ChooseIm1.color.r, ChooseIm1.color.g, ChooseIm1.color.b, 0.5f);
        ChooseIm2.color = new Color(ChooseIm2.color.r, ChooseIm2.color.g, ChooseIm2.color.b, 0.5f);

        //Политика________________________________________
        PRIVACY_POLICY.SetActive(false);
    }



    public void InitLanguage()
    {
        PlayBtnTxt.text = Localization.instance.getTextByKey("#Menu1");
        NewBtnTxt.text = Localization.instance.getTextByKey("#Menu2");
        ContinueBtnTxt.text = Localization.instance.getTextByKey("#Menu3");
        LoadBtnTxt.text = Localization.instance.getTextByKey("#Menu4");
        ChooseTxt1.text = Localization.instance.getTextByKey("#Menu7");
        ChooseTxt2.text = Localization.instance.getTextByKey("#Menu7");

        for (int i = 0; i < SaveSlots.Length; i++)
            SaveSlots[i].NewGame.text = Localization.instance.getTextByKey("#Menu2");
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray MyRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(MyRay, out hit, Mathf.Infinity))
            {
                if (ChosePlayer)
                {
                    if (hit.transform.name == "PlayerGirl")
                        FirstChoose("Girl");
                    if (hit.transform.name == "PlayerBoy")
                        FirstChoose("Boy");

                }
            }
        }
    }

    public void FirstChoose(string p)
    {
        PlayBtn.gameObject.SetActive(true);
        if (p == "Boy")
        {
            ChooseIm1.color = new Color(ChooseIm1.color.r, ChooseIm1.color.g, ChooseIm1.color.b, 1f);
            ChooseIm2.color = new Color(ChooseIm2.color.r, ChooseIm2.color.g, ChooseIm2.color.b, 0.5f);
        }
        else
        {
            ChooseIm1.color = new Color(ChooseIm1.color.r, ChooseIm1.color.g, ChooseIm1.color.b, 0.5f);
            ChooseIm2.color = new Color(ChooseIm2.color.r, ChooseIm2.color.g, ChooseIm2.color.b, 1f);
        }
        StandPlayer(p);
    }
    public void StandPlayer(string p) // персонаж встает
    {
        CurrentMale = p;
        if (p == "Girl")
        {
            if (!GirlAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pose"))
            {
                GirlAnimator.Play("StandUp");
                MenuSnd.PlayOneShot(ChooseGrl);
            }
            if (!BoyAnimator.GetCurrentAnimatorStateInfo(0).IsName("Sit"))
                BoyAnimator.Play("SitDown");
        }
        else
        {
            if (!BoyAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pose"))
            {
                BoyAnimator.Play("StandUp");
                MenuSnd.PlayOneShot(ChooseBoy);
            }
            if (!GirlAnimator.GetCurrentAnimatorStateInfo(0).IsName("Sit"))
                GirlAnimator.Play("SitDown");
        }
    }

    public void SitPlayer(string p) //персонаж садится
    {
        if (p == "Girl")
        {
            if (!GirlAnimator.GetCurrentAnimatorStateInfo(0).IsName("Sit"))
                GirlAnimator.Play("SitDown");
        }
        else
        {
            if (!BoyAnimator.GetCurrentAnimatorStateInfo(0).IsName("Sit"))
                BoyAnimator.Play("SitDown");
        }
    }

    public void NewGame()
    {
        Choose.SetActive(true);
        CloseChoose.SetActive(true);
        SettingsBtn.gameObject.SetActive(false);
        NewBtn.gameObject.SetActive(false);
        ContinueBtn.gameObject.SetActive(false);
        LoadBtn.gameObject.SetActive(false);
        MenuSnd.PlayOneShot(BtnSound);
        ChosePlayer = true;
        SitPlayer("Girl");
        SitPlayer("Boy");
    }

    public void CloseNewGame()
    {
        ChooseIm1.color = new Color(ChooseIm1.color.r, ChooseIm1.color.g, ChooseIm1.color.b, 0.5f);
        ChooseIm2.color = new Color(ChooseIm2.color.r, ChooseIm2.color.g, ChooseIm2.color.b, 0.5f);
        MenuSnd.PlayOneShot(BtnSound);
        Choose.SetActive(false);
        CloseChoose.SetActive(false);
        SettingsBtn.gameObject.SetActive(true);
        PlayBtn.gameObject.SetActive(false);
        NewBtn.gameObject.SetActive(true);
        if (CurrentGame > -1)
            ContinueBtn.gameObject.SetActive(true);
        else
            ContinueBtn.gameObject.SetActive(false);
        LoadBtn.gameObject.SetActive(true);
        CreateNewSave = false;
        CurrentMale = PlayerPrefs.GetString("ChibiMALE" + CurrentGame);
        StandPlayer(CurrentMale);
    }

    public void Play()
    {
        MenuSnd.PlayOneShot(BtnSound);
        CreateNewSave = true;
        for (int i = 0; i < SaveSlots.Length; i++)
        {
            SaveSlots[i].NewGame.gameObject.SetActive(true);
            SaveSlots[i].Load.text = Localization.instance.getTextByKey("#Menu6");
            //SaveSlots[i].FromCloud.SetActive(false);
        }
        StartCoroutine(MoveCamUp());
        StartCoroutine(MoveCanvasUp());
    }

    public void Continue()
    {
        PlayerPrefs.SetInt("ChibiCurrentGame", CurrentGame);
        PlayerPrefs.SetString("ChibiMALE" + CurrentGame, CurrentMale);
        PlayerPrefsX.SetBool("ChibiIsNewGame", false);
        MenuSnd.PlayOneShot(BtnSound);
        PlayerPrefs.SetString("ChibiLevel", "Game");
        SceneManager.LoadScene("Load");
    }


    //ЗАГРУЗКА СЕЙВОВ_________________________________________________________________________

    public void Load()
    {
        MenuSnd.PlayOneShot(BtnSound);
        for (int i = 0; i < SaveSlots.Length; i++)
        {
            SaveSlots[i].NewGame.gameObject.SetActive(false);
            SaveSlots[i].Load.text = Localization.instance.getTextByKey("#Menu4");
//#if !NO_GPGS
//            if (GPGSLogin.authenticated)
//                SaveSlots[i].FromCloud.SetActive(true);
//#endif
        }
        StopAllCoroutines();
        StartCoroutine(MoveCamUp());
        StartCoroutine(MoveCanvasUp());
    }

    IEnumerator MoveCamUp()
    {
        while (MenuCamera != null && (int)MenuCamera.transform.rotation.eulerAngles.x != 270)
        {
            MenuCamera.transform.rotation = Quaternion.Euler(MenuCamera.transform.rotation.eulerAngles - new Vector3(2f, 0, 0));
            yield return new WaitForFixedUpdate();
        }

    }

    IEnumerator MoveCanvasUp()
    {
        var offset = FirstWindow.parent.GetComponent<RectTransform>().sizeDelta;

        FirstWindow.localPosition = new Vector3(0, FirstWindow.localPosition.y, 0);
        while (FirstWindow.localPosition.y > -offset.y)
        {
            FirstWindow.localPosition -= new Vector3(0, 15, 0);
            yield return new WaitForFixedUpdate();
        }

        FirstWindow.localPosition = new Vector3(0, -offset.y, 0);
    }

    public static Sprite LoadScreanShot(int id)
    {
        string path = Application.persistentDataPath + "/Save" + "_Screen" + id;
        byte[] file = File.ReadAllBytes(path);
        Texture2D tex = new Texture2D(Screen.width, Screen.height);
        tex.LoadImage(file);
        Sprite sp = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(1f, 0.5f));
        return sp;
    }

    public void UnLoad() //возвращаемся на сцену
    {
        if (CreateNewSave)
        {
            Choose.SetActive(false);
            CloseChoose.SetActive(false);
            SettingsBtn.gameObject.SetActive(true);
            PlayBtn.gameObject.SetActive(false);
            NewBtn.gameObject.SetActive(true);
            if (CurrentGame > -1)
                ContinueBtn.gameObject.SetActive(true);
            else
                ContinueBtn.gameObject.SetActive(false);
            LoadBtn.gameObject.SetActive(true);
            CreateNewSave = false;
            CurrentMale = PlayerPrefs.GetString("ChibiMALE" + CurrentGame);
            StandPlayer(CurrentMale);
        }
        MenuSnd.PlayOneShot(BtnSound);
        StopAllCoroutines();
        StartCoroutine(MoveCamDown());
        StartCoroutine(MoveCanvasDown());
    }

    IEnumerator MoveCamDown()
    {
        while ((int)MenuCamera.transform.rotation.eulerAngles.x != 17)
        {
            MenuCamera.transform.rotation = Quaternion.Euler(MenuCamera.transform.rotation.eulerAngles + new Vector3(2f, 0, 0));
            if ((int)MenuCamera.transform.rotation.eulerAngles.x > 17 && (int)MenuCamera.transform.rotation.eulerAngles.x < 20)
                MenuCamera.transform.rotation = Quaternion.Euler(17, 0, 0);
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MoveCanvasDown()
    {
        FirstWindow.localPosition = new Vector3(0, FirstWindow.localPosition.y, 0);
        while (FirstWindow.localPosition.y < 0)
        {
            FirstWindow.localPosition += new Vector3(0, 15, 0);
            yield return new WaitForFixedUpdate();
        }
        FirstWindow.localPosition = new Vector3(0, 0, 0);
    }

    public void LoadSave(int id) //загружаем сейв если он есть или создаем сейв
    {
        MenuSnd.PlayOneShot(BtnSound);
        //Debug.Log(CreateNewSave+" "+ AllSaves[id]);
        if (CreateNewSave)
        {
            NextGame = id;
            if (!AllSaves[id])
            {
                AgreeSave();
            }
            else
            {
                AskQuestion.text = Localization.instance.getTextByKey("#Rewrite");
                AskWindow.SetActive(true);
            }

        }
        else
        {
            if (AllSaves[id])
            {
                CurrentGame = id;
                PlayerPrefs.SetInt("ChibiCurrentGame", CurrentGame);
                PlayerPrefs.SetString("ChibiMALE" + CurrentGame, SaveManager.AllSaveData[CurrentGame].Male);
                PlayerPrefsX.SetBool("ChibiIsNewGame", false);
                PlayerPrefs.SetString("ChibiLevel", "Game");
                SceneManager.LoadScene("Load");
            }
        }
    }


    public void CancelSave()
    {
        MenuSnd.PlayOneShot(BtnSound);
        AskWindow.SetActive(false);
    }
    public void AgreeSave()
    {
        MenuSnd.PlayOneShot(BtnSound);
        CurrentGame = NextGame;
        PlayerPrefs.SetInt("ChibiCurrentGame", CurrentGame);
        PlayerPrefs.SetString("ChibiMALE" + CurrentGame, CurrentMale);
        PlayerPrefsX.SetBool("ChibiIsNewGame", true);
        PlayerPrefs.SetString("ChibiLevel", "Tutorial");
//#if !NO_GPGS
//        if (SettingsController.GoogleAuthenticated)
//        {
//            Ok.onClick.AddListener(() => DiskAsk(true));
//            No.onClick.AddListener(() => DiskAsk(false));
//            AskQuestion.text = Localization.instance.getTextByKey("#DidSave");
//            AskWindow.SetActive(true);
//        }
//        else
//            LoadGame();
//#else
        LoadGame();
//#endif
    }
//    public void DiskAsk(bool use)
//    {
//        if (use)
//        {
//#if !NO_GPGS
//            GPGSSave.SelectSlotToSave(CallBackFileName);
//#endif
//        }
//        else
//        {
//            SaveManager.SetCloudFileName(CurrentGame, "");
//            PlayerPrefsX.SetBool("ChibiIsCloud", false);
//            LoadGame();
//        }
//    }
    //void CallBackFileName(string fileName)
    //{
    //    if ((fileName != null) && (fileName.Length > 0))
    //    {
    //        SaveManager.SetCloudFileName(CurrentGame, fileName);
    //        PlayerPrefsX.SetBool("ChibiIsCloud", true);
    //    }
    //    else
    //    {
    //        SaveManager.SetCloudFileName(CurrentGame, "");
    //        PlayerPrefsX.SetBool("ChibiIsCloud", false);
    //    }
    //    LoadGame();
    //}
    void LoadGame()
    {
        SceneManager.LoadScene("Load");
    }
    //НАСТРОЙКИ________________________________________________________________________
    public void OpenSettings()
    {
        MenuSnd.PlayOneShot(BtnSound);
        StopAllCoroutines();
        StartCoroutine(MoveCamLeft());
        StartCoroutine(MoveCanvasLeft());
    }

    IEnumerator MoveCamLeft()
    {
        MenuCamera.transform.rotation = Quaternion.Euler(new Vector3(17, MenuCamera.transform.rotation.eulerAngles.y, MenuCamera.transform.rotation.eulerAngles.z));
        FirstWindow.localPosition = new Vector3(FirstWindow.localPosition.x, 0, FirstWindow.localPosition.z);
        int y = (int)MenuCamera.transform.rotation.eulerAngles.y;
        if (y > 360)
            y -= 360;
        while ((int)MenuCamera.transform.rotation.eulerAngles.y != 90)
        {
            MenuCamera.transform.rotation = Quaternion.Euler(MenuCamera.transform.rotation.eulerAngles + new Vector3(0, 2f, 0));
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MoveCanvasLeft()
    {
        var offset = FirstWindow.parent.GetComponent<RectTransform>().sizeDelta;

        FirstWindow.localPosition = new Vector3(FirstWindow.localPosition.x, 0, 0);
        while (FirstWindow.localPosition.x > -offset.x)
        {
            FirstWindow.localPosition -= new Vector3(15, 0, 0);
            yield return new WaitForFixedUpdate();
        }
        FirstWindow.localPosition = new Vector3(-offset.x, 0, 0);
    }

    public void CloseSettings()
    {
        MenuSnd.PlayOneShot(BtnSound);
        StopAllCoroutines();
        StartCoroutine(MoveCamRight());
        StartCoroutine(MoveCanvasRight());
    }

    IEnumerator MoveCamRight()
    {
        while ((int)MenuCamera.transform.rotation.eulerAngles.y != 0)
        {
            MenuCamera.transform.rotation = Quaternion.Euler(MenuCamera.transform.rotation.eulerAngles - new Vector3(0, 2f, 0));
            yield return new WaitForFixedUpdate();
        }
    }


    IEnumerator MoveCanvasRight()
    {
        FirstWindow.localPosition = new Vector3(FirstWindow.localPosition.x, 0, 0);
        while (FirstWindow.localPosition.x < 0)
        {
            FirstWindow.localPosition += new Vector3(15, 0, 0);
            yield return new WaitForFixedUpdate();
        }

        FirstWindow.localPosition = new Vector3(0, 0, 0);
    }

    public void ChangeMusicVolume()
    {
        Settings.MusicSlider.value = Settings.MusicVolume;
        MenuMusic.volume = Settings.MusicVolume;
        PlayerPrefs.SetFloat("ChibiMusicVolume", Settings.MusicVolume);
    }

    public void ChangeSoundVolume()
    {
        Settings.SoundSlider.value = Settings.SoundVolume;
        MenuSnd.volume = Settings.SoundVolume;
        Firecamp.volume = Settings.SoundVolume;
        PlayerPrefs.SetFloat("ChibiSoundVolume", Settings.SoundVolume);
    }

    public void Button()
    {
        MenuSnd.PlayOneShot(BtnSound);
    }

    //ПОГОДА__________________________________________________________________
    public void SetWeather()
    {
        string weather = "Green";
        float x = Random.Range(0, 4);
        if (x < 1)
            weather = "Rain";
        else if (x >= 1 && x < 2)
            weather = "Sun";
        else if (x >= 2 && x < 3)
            weather = "Snow";
        else
            weather = "Green";


        Plant1.SetFloat("_Alpha", 0);
        Plant2.SetFloat("_Alpha", 0);
        Stones.SetFloat("_Alpha", 0);
        Plant1.color = new Color(1f, 1f, 1f);
        Plant2.color = new Color(1f, 1f, 1f);
        Snow.SetActive(false);
        Rain.SetActive(false);
        switch (weather)
        {
            case "Green":
                Ground.SetFloat("_Alpha", 1);
                break;
            case "Sun":
                Ground.SetTexture("_DopSplat", SunGround);  // текстура земли;
                Ground.SetFloat("_Alpha", 0);
                Plant1.color = new Color(0.97f, 0.6f, 0.27f);
                Plant2.color = new Color(0.97f, 0.6f, 0.27f);
                break;
            case "Snow":
                Ground.SetTexture("_DopSplat", SnowGround);  // текстура земли;
                Ground.SetFloat("_Alpha", 0);
                Plant1.SetFloat("_Alpha", 1);
                Plant2.SetFloat("_Alpha", 1);
                Stones.SetFloat("_Alpha", 1);
                Snow.SetActive(true);
                break;
            case "Rain":
                Ground.SetTexture("_DopSplat", RainGround);  // текстура земли;
                Ground.SetFloat("_Alpha", 0);
                Rain.SetActive(true);
                break;
        }
    }


    public void PRIVACYPOLICYOpen()
    {
        //GFM.GDPR.GDPRCanvas.instance.Show(true, () =>
        //{
        //});
    }




}
