﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCullDown : MonoBehaviour {

    float timer = 0;

    private void OnEnable()
    {
        timer = 0;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > 5f)
            gameObject.SetActive(false);
    }
}
