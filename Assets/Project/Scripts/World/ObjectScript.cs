﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class ObjectScript : MonoBehaviour {

    public GameManager GOD;
    public string ObjectTag;
    public float HP, maxHP; // количество ударов для добычи
    public int CollectionResCount = 0;   // количество типов ресурсов получаемых с объекта
    public string CollectionResName1="", CollectionResName2="";  // имена ресурсов получаемых с объекта
    public int CollectionResNumber1=0, CollectionResNumber2=0;  // количество получаемых ресурсов
    public int ExtractionResCount = 0;   // количество типов ресурсов ДОБЫВАЕМЫХ с объекта
    public string ExtractionResName1="", ExtractionResName2="", ExtractionResName3 = "";  // имена ресурсов ДОБЫВАЕМЫХ с объекта
    public int ExtractionResNumber1=0, ExtractionResNumber2=0, ExtractionResNumber3 = 0;  // количество ДОБЫВАЕМЫХ ресурсов
    public int RecoveryTime = 0;                            // время восстановления ресурса
    public Animator anim; //аниматор если есть

    public Renderer mapUpdateRenderer; //для апдейта карты
    public DynamicMapUpdate MapUpdate;

    public GameObject CrystallEffect;

    public void AddObjectToRecover()
    {
        if (CrystallEffect)
            CrystallEffect.SetActive(false);
    }
    public void RecoverResourses()
    {
        RecoveryTime = 0;
        float x = HP;
        Init();
        HP = x;
    }
    public void RecoverObject()
    {
        RecoveryTime = 0;
        gameObject.SetActive(true);
        Init();
        if (MapUpdate != null && mapUpdateRenderer!=null)
            MapUpdate.UpdateMapOnce(mapUpdateRenderer);
        if (CrystallEffect && GOD && GOD.DopEff.EffectsOn)
            CrystallEffect.SetActive(true);
    } 
           
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Tutorial")
            InitTutorial();
        else
            Init();
        anim = GetComponent<Animator>();
        if (anim)
            anim.enabled = false;
        MapUpdate = GetComponent<DynamicMapUpdate>();
    }

    public void InitTutorial()
    {
        string s = name;
        switch(s)
        {
            case "AppleTree":           // яблоня
                ObjectTag = "#AppleTree";

                maxHP = 20;
                HP = maxHP;

                CollectionResCount = 2;
                CollectionResName1 = "Apple";
                CollectionResNumber1 = 1;
                CollectionResName2 = "Leaf";
                CollectionResNumber2 = 1;

                ExtractionResCount = 2;
                ExtractionResName1 = "Stick";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Wood";
                ExtractionResNumber2 = 1;
                Transform t = transform.Find("Stick");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                break;
            case "Stone":                  // каменные залежи
                ObjectTag = "#Stone";

                maxHP = 30;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 2;
                ExtractionResName1 = "Stone";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Flint";
                ExtractionResNumber2 = 1;

                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
        }
    }

       public void InitGOD(GameManager g)
    {
        GOD = g;
    }           
    public void Init ()
    {
        string s = name;
       
        if (s.Contains("AppleTree"))
            s = "AppleTree";
        else if (s.Contains("Stone"))
            s = "Stone";
        else if (s.Contains("LongTree") || s.Contains("ColorTree"))
            s = "Tree";
        else if (s.Contains("FirTree"))
            s = "FirTree";
        else if (s.Contains("IcePearTree"))
            s = "IcePearTree";
        else if (s.Contains("PalmTree"))
            s = "PalmTree";
        else if (name == "BigBones2")
            s = "BigBones";
        else if (name.Contains("Cactus"))
            s = "Cactus";
        else if (name.Contains("IceRock"))
            s = "IceRock";
        else if (s.Contains("TV_Statue"))
            s = "TV_Statue";

        if (tag == "Mountain")
            s = "Mountain";
        if (tag == "BigTree")
            s = "BigTree";
        if (tag == "Build" || tag == "GroundHouse" || tag == "House" || name == "Door" || name.Contains("Roof"))
            s = "Build";
        switch (s)
        {
            case "Build":           // постройки
                ObjectTag = "#Name" +name;
                maxHP = 3;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResNumber1 = 0;
                if(GOD)
                {
                    for(int i=0; i<GOD.CraftContr.Recipe.Length; i++)
                    {
                        if(GOD.CraftContr.Recipe[i].name == name)
                        {
                            ExtractionResCount = GOD.CraftContr.Recipe[i].NameOfIngridient.Length;
                            if (GOD.CraftContr.Recipe[i].NameOfIngridient.Length > 0)
                            {
                                ExtractionResName1 = GOD.CraftContr.Recipe[i].NameOfIngridient[0];
                                ExtractionResNumber1 = GOD.CraftContr.Recipe[i].NumberOfIngridient[0];
                            }
                            if (GOD.CraftContr.Recipe[i].NameOfIngridient.Length > 1)
                            {
                                ExtractionResName2 = GOD.CraftContr.Recipe[i].NameOfIngridient[1];
                                ExtractionResNumber2 = GOD.CraftContr.Recipe[i].NumberOfIngridient[1];
                            }
                            if (GOD.CraftContr.Recipe[i].NameOfIngridient.Length > 2)
                            {
                                ExtractionResName3 = GOD.CraftContr.Recipe[i].NameOfIngridient[2];
                                ExtractionResNumber3 = GOD.CraftContr.Recipe[i].NumberOfIngridient[2];
                            }
                        }
                    }
                }
                if (name.Contains("Wall"))
                {
                    /* Transform wall = transform.FindChild("Options").FindChild("Low");
                     if(!wall.gameObject.activeSelf)
                         wall = transform.FindChild("Options").FindChild("High");*/
                    Transform wall = transform.Find("Options").Find("Wall");
                    mapUpdateRenderer = wall.GetComponent<Renderer>();
                }
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break; 
            case "Tree":           // обычное дерево
                ObjectTag = "#Tree";
                maxHP = 100;
                HP = maxHP;
                int LeafChance = Random.Range(0, 101);
                if (LeafChance <= 70)
                {
                    CollectionResCount = 1;
                    CollectionResName1 = "Leaf";
                    CollectionResNumber1 = 1;
                }
                else
                    CollectionResCount = 0;
                ExtractionResCount = 2;
                ExtractionResName1 = "Stick";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Wood";
                int Chance = Random.Range(0,101);                   // шанс выпадения древесины
                if (Chance <=70)
                    ExtractionResNumber2 = 1;
                else
                    ExtractionResCount = 1;

                Transform t = transform.Find("Stick");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                break;
            case "FirTree":           // хвойное дерево
                ObjectTag = "#Fir";
                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 2;
                ExtractionResName1 = "Stick";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Wood";
                Chance = Random.Range(0, 101);                   // шанс выпадения древесины
                if (Chance <= 70)
                    ExtractionResNumber2 = 1;
                else
                    ExtractionResCount = 1;

                t = transform.Find("Stick");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                break;
            case "IcePearTree":           // кристалическая груша
                ObjectTag = "#Pear";
                maxHP = 100;
                HP = maxHP;

                CollectionResCount = 0;

                LeafChance = Random.Range(0, 101);
                if (LeafChance <= 70)
                {
                    CollectionResCount++;
                    CollectionResName1 = "Leaf";
                    CollectionResNumber1 = 1;
                }
                LeafChance = Random.Range(0, 101);
                if (LeafChance <= 70)
                {
                    CollectionResCount++;
                    if (CollectionResCount > 1)
                    {
                        CollectionResName2 = "Pear";
                        CollectionResNumber2 = 1;
                    }
                    else
                    {
                        CollectionResName1 = "Pear";
                        CollectionResNumber1 = 1;
                    }
                }

                ExtractionResCount = 2;
                ExtractionResName1 = "Stick";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Wood";
                Chance = Random.Range(0, 101);                   // шанс выпадения древесины
                if (Chance <= 70)
                    ExtractionResNumber2 = 1;
                else
                    ExtractionResCount = 1;

                t = transform.Find("Stick");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                break;
            case "AppleTree":           // яблоня
                ObjectTag = "#AppleTree";
                maxHP = 100;
                HP = maxHP;

                CollectionResCount = 0;
                LeafChance = Random.Range(0, 101);
                if (LeafChance <= 70)
                {
                    CollectionResCount++;
                    CollectionResName1 = "Leaf";
                    CollectionResNumber1 = 1;
                }
                LeafChance = Random.Range(0, 101);
                if (LeafChance <= 70)
                {
                    CollectionResCount++;
                    if (CollectionResCount > 1)
                    {
                        CollectionResName2 = "Apple";
                        CollectionResNumber2 = 1;
                    }
                    else
                    {
                        CollectionResName1 = "Apple";
                        CollectionResNumber1 = 1;
                    }
                }


                ExtractionResCount = 2;
                ExtractionResName1 = "Stick";
                ExtractionResNumber1 = 1;
                Chance = Random.Range(0,101);
                if(Chance<=70)
                {
                    ExtractionResName2 = "Wood";
                    ExtractionResNumber2 = 1;
                }
                else
                {
                    ExtractionResName2 = "Seed";
                    ExtractionResNumber2 = 1;
                }

                t = transform.Find("Stick");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();

                break;

            case "Cactus":           // кактус
                ObjectTag = "#Cactus";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;

                ExtractionResCount = 1;
                ExtractionResName1 = "CactusPiece";
                ExtractionResNumber1 = 1;
                Chance = Random.Range(0, 101);
                if (Chance <= 50)
                {
                    ExtractionResName2 = "Needle";
                    ExtractionResNumber2 = 1;
                    ExtractionResCount++;
                }
                mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "PalmTree":           // пальма
                ObjectTag = "#PalmTree";
                maxHP = 100;
                HP = maxHP;
                LeafChance = Random.Range(0, 101);
                if (LeafChance <= 70)
                {
                    CollectionResCount = 1;
                    CollectionResName1 = "Leaf";
                    CollectionResNumber1 = 1;
                }
                else
                    CollectionResCount = 0;
                ExtractionResCount = 2;
                ExtractionResName1 = "Stick";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Wood";
                Chance = Random.Range(0, 101);                   // шанс выпадения древесины
                if (Chance <= 70)
                    ExtractionResNumber2 = 1;
                else
                    ExtractionResCount = 1;

                t = transform.Find("Stick");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();

                break;
            case "BushBerries":           // куст с ягодами
                ObjectTag = "#BushBerrie";

                maxHP = 8;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "Berries";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "BushCondiment":           // куст со специями
                ObjectTag = "#BushCondiment";

                maxHP = 8;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "Condiment";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "BushCoconut":           // куст с какао бобами
                ObjectTag = "#BushCoconut";
                maxHP = 8;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "CocoaBeans";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "BushGrapes":           // куст винограда
                ObjectTag = "#BushGrapes";
                maxHP = 8;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "Grapes";
                CollectionResNumber1 =1;

                ExtractionResCount = 0;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "Stone":                  // каменные залежи
                ObjectTag = "#Stone";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 2;
                ExtractionResName1 = "Stone";
                ExtractionResNumber1 = 1;
                ExtractionResName2 = "Flint";
                Chance = Random.Range(0, 101);                   // шанс выпадения кремня
                if (Chance <=70)
                    ExtractionResNumber2 = 1;
                else
                    ExtractionResCount = 1;
                mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "IceRock":                  // лед
                ObjectTag = "#Ice";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Ice";
                ExtractionResNumber1 = 1;
                mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "IronOre":                  // железо
                ObjectTag = "#IronOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "IronOre";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "CrystallOre":                  // кристаллы
                ObjectTag = "#CrystallOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Crystall";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "AmethystOre":                  // залежи аметиста
                ObjectTag = "#AmethystOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Amethyst";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "RubyOre":                  // залежи рубина
                ObjectTag = "#RubyOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Ruby";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "SapphireOre":                  // залежи сапфира
                ObjectTag = "#SapphireOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Sapphire";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "EmeraldOre":                  // залежи изумруда
                ObjectTag = "#EmeraldOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Emerald";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "DimondOre":                  // залежи алмазов
                ObjectTag = "#DimondOre";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Dimond";
                ExtractionResNumber1 = 1;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "Sand":           // куча песка
                ObjectTag = "#Sand";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "Sand";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "BigBones":           //кости
                ObjectTag = "#BigBones";
                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "Bone";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                t = transform.Find("Object");
                if (t)
                    mapUpdateRenderer = t.GetComponent<Renderer>();
                else
                    mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "Snow":           // куча снега
                ObjectTag = "#Snow";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "Snow";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                mapUpdateRenderer = GetComponent<Renderer>();
                break;
            case "FlowerRed":           // цветы
                ObjectTag = "#NameFlowerRed";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "FlowerRed";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                break;
            case "FlowerOrange":           // цветы
                ObjectTag = "#NameFlowerOrange";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "FlowerOrange";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                break;
            case "FlowerBlue":           // цветы
                ObjectTag = "#NameFlowerBlue";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 1;
                CollectionResName1 = "FlowerBlue";
                CollectionResNumber1 = 1;

                ExtractionResCount = 0;
                break;
            case "River":           // река
                ObjectTag = "#River";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;

                ExtractionResCount = 1;
                ExtractionResName1 = "Water";
                ExtractionResNumber1 = 1;
                break;
            case "Mountain":           // гора
                ObjectTag = "#Mountain";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 0;
                break;
            case "BigTree":           // дерево
                ObjectTag = "#Tree";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 0;
                break;
            case "BreakIce":           // лед
                ObjectTag = "#NameBreakIce";

                maxHP = 100;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 0;
                break;
            case "TV_Statue":                  // статуя
                ObjectTag = "#TV_Statue";

                maxHP = 20;
                HP = maxHP;
                CollectionResCount = 0;
                ExtractionResCount = 1;
                ExtractionResName1 = "Gem";
                ExtractionResNumber1 = 1;
                break;
        }
	
	}

    public void PlayAnimator(string animationName)
    {
        if (anim)
        {
            anim.enabled = true;
            anim.Play(animationName);
        }
    }
    public void DisabledAnimator()
    {
        anim.enabled = false;
    }
}
