﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DopEffects : MonoBehaviour {

    public GameManager GOD;
    public bool EffectsOn = true;           // включенны ли эффекты
    public Transform EffectsParent;       // родитель эффектов
    [Header("Эффекты игрока:")]
    public ParticleSystem CirckHit, AxeHit;       // от удара кирки, листья от рубки
    public ParticleSystem SwordAttack2, BigHit;   // от удара мечом
    public ParticleSystem PlayerOnWater;         // игрок на воде
    public ParticleSystem Sucsess, Fail;              // успех и неудача
    public GameObject Ressurection;                  //эффект воскрешения

    [Header("Эффекты островов:")]
    public ParticleSystem Rain;  // дождь
    public AudioSource RainSound;
    public Transform RainCircle; // круги от дождя
    public ParticleSystem Snow;  // снег
    public GameObject AllFoamWater; // пена водопадов на водных островах
    public FoamScript[] AllFoamScriptWater;
    public GameObject AllFoamSand; // пена водопадов на пустынных островах
    public FoamScript[] AllFoamScriptSand;

    [Header("Эффекты животных:")]
    public ParticleSystem Soul;      //после смерти животных

    [Header("Эффекты:")]
    public GameObject FishPlace;          // рябь в месте для рыбы
    public GameObject Puff;               // пуф

    [Header("Эффекты мира:")]

    public List<GameObject> AllEffects = new List<GameObject>();
    public List<GameObject> AllDayEffects = new List<GameObject>();
    public List<GameObject> AllFog = new List<GameObject>();

    public Transform Saphire1Ore, Saphire1UnderSnow, Saphire2Ore, Saphire2UnderSnow, Saphire3Ore, Saphire3UnderSnow;
    public List<GameObject> CrystallOre1, CrystallOre2, CrystallOre3;

    public void InitIsland(Transform Island)
    {
        Transform t = Island.Find("Effects");
        if(t!=null)
        {
            AllEffects.Add(t.gameObject);
            Transform result = t.Find("Fog");
            if (result)
                AllFog.Add(result.gameObject);
            result = null;
            result = t.Find("LightShaft");
            if (result)
                AllDayEffects.Add(result.gameObject);
            result = null;
            result = t.Find("DaySparkl");
            if (result)
                AllDayEffects.Add(result.gameObject);
            result = null;
            result = t.Find("NightSparkl");
            if (result)
                AllDayEffects.Add(result.gameObject);
            result = null;
            result = t.Find("Tornado");
            if (result)
                AllDayEffects.Add(result.gameObject);
            result = null;
            result = t.Find("SnowSparkl");
            if (result)
                AllDayEffects.Add(result.gameObject);
        }
    }
    GameObject FindEffect(Transform t,string eName)
    {
        Transform t2 = t.Find(eName);
        if (t2 != null)
            return t2.gameObject;
        return null;
    }
    public void Init()
    {
        SwordAttack2.GetComponent<ReturnEffectsScript>().Init();
        SwordAttack2.GetComponent<ReturnEffectsScript>().GOD = GOD;
        BigHit.GetComponent<ReturnEffectsScript>().Init();
        BigHit.GetComponent<ReturnEffectsScript>().GOD = GOD;
        Sucsess.GetComponent<ReturnEffectsScript>().Init();
        Sucsess.GetComponent<ReturnEffectsScript>().GOD = GOD;
        Fail.GetComponent<ReturnEffectsScript>().Init();
        Fail.GetComponent<ReturnEffectsScript>().GOD = GOD;
        Puff.GetComponent<ReturnEffectsScript>().Init();
        Puff.GetComponent<ReturnEffectsScript>().GOD = GOD;
        Soul.GetComponent<ReturnEffectsScript>().Init();
        Soul.GetComponent<ReturnEffectsScript>().GOD = GOD;

        if (!EffectsOn)
        {
            for (int i = 0; i < AllEffects.Count; i++)
                AllEffects[i].SetActive(false);
        }
        if (!GOD.IsTutorial)
        {
            AllFoamScriptWater = new FoamScript[AllFoamWater.transform.childCount]; //брызги на водных островах
            for (int i = 0; i < AllFoamWater.transform.childCount; i++)
            {
                AllFoamScriptWater[i] = AllFoamWater.transform.GetChild(i).GetComponent<FoamScript>();
                AllFoamScriptWater[i].Init();
            }
            AllFoamScriptSand = new FoamScript[AllFoamSand.transform.childCount]; //брызги на пустынных островах
            for (int i = 0; i < AllFoamSand.transform.childCount; i++)
            {
                AllFoamScriptSand[i] = AllFoamSand.transform.GetChild(i).GetComponent<FoamScript>();
                AllFoamScriptSand[i].Init();
            }
        }
    }
    public void InitCrystall()
    {
        if (!GOD.IsTutorial)
        {
            //собираем все кристаллы
            for (int i = 0; i < Saphire1Ore.childCount; i++)
            {
                if (Saphire1Ore.GetChild(i).name == "CrystallOre")
                    CrystallOre1.Add(Saphire1Ore.GetChild(i).gameObject);
            }
            for (int i = 0; i < Saphire1UnderSnow.childCount; i++)
            {
                if (Saphire1UnderSnow.GetChild(i).name == "CrystallOre")
                    CrystallOre1.Add(Saphire1UnderSnow.GetChild(i).gameObject);
            }
            for (int i = 0; i < Saphire2Ore.childCount; i++)
            {
                if (Saphire2Ore.GetChild(i).name == "CrystallOre")
                    CrystallOre2.Add(Saphire2Ore.GetChild(i).gameObject);
            }
            for (int i = 0; i < Saphire2UnderSnow.childCount; i++)
            {
                if (Saphire2UnderSnow.GetChild(i).name == "CrystallOre")
                    CrystallOre2.Add(Saphire2UnderSnow.GetChild(i).gameObject);
            }
            for (int i = 0; i < Saphire3Ore.childCount; i++)
            {
                if (Saphire3Ore.GetChild(i).name == "CrystallOre")
                    CrystallOre3.Add(Saphire3Ore.GetChild(i).gameObject);
            }
            for (int i = 0; i < Saphire3UnderSnow.childCount; i++)
            {
                if (Saphire3UnderSnow.GetChild(i).name == "CrystallOre")
                    CrystallOre3.Add(Saphire3UnderSnow.GetChild(i).gameObject);
            }
            for (int i = 0; i < CrystallOre1.Count; i++)      //расставляем эффекты вокруг кристаллов
            {
                if (i < GOD.EnviromentContr.AllSnowSparkls[0].childCount)
                {
                    Transform sparkl = GOD.EnviromentContr.AllSnowSparkls[0].GetChild(i);
                    sparkl.position = new Vector3(CrystallOre1[i].transform.position.x, 0, CrystallOre1[i].transform.position.z);
                    CrystallOre1[i].GetComponent<ObjectScript>().CrystallEffect = sparkl.gameObject;
                }
            }
            for (int i = 0; i < CrystallOre2.Count; i++)
            {
                if (i < GOD.EnviromentContr.AllSnowSparkls[1].childCount)
                {
                    Transform sparkl = GOD.EnviromentContr.AllSnowSparkls[1].GetChild(i);
                    sparkl.position = new Vector3(CrystallOre2[i].transform.position.x, 7, CrystallOre2[i].transform.position.z);
                    CrystallOre2[i].GetComponent<ObjectScript>().CrystallEffect = sparkl.gameObject;
                }
            }
            for (int i = 0; i < CrystallOre3.Count; i++)
            {
                if (i < GOD.EnviromentContr.AllSnowSparkls[2].childCount)
                {
                    Transform sparkl = GOD.EnviromentContr.AllSnowSparkls[2].GetChild(i);
                    sparkl.position = new Vector3(CrystallOre3[i].transform.position.x, 5, CrystallOre3[i].transform.position.z);
                    CrystallOre3[i].GetComponent<ObjectScript>().CrystallEffect = sparkl.gameObject;
                }
            }
        }
    }

    public void ChangeOnOff(bool b)
    {
        EffectsOn = b;
        if (EffectsOn)
        {
            for (int i = 0; i < AllEffects.Count; i++)
                AllEffects[i].SetActive(true);
        }
        else
        {
            for (int i = 0; i < AllEffects.Count; i++)
                AllEffects[i].SetActive(false);
        }
    }

    public void ReturnEffects()
    {
        CirckHit.transform.SetParent(EffectsParent);
        AxeHit.transform.SetParent(EffectsParent);
        BigHit.transform.SetParent(EffectsParent);
        SwordAttack2.transform.SetParent(EffectsParent);
    }

    public void GetRain(bool t)
    {
        if (t)
        {
            RainSound.clip = Resources.Load<AudioClip>("Enviroment/Rain");
            Rain.transform.SetParent(null);
        }
        else
        {
            Rain.transform.SetParent(EffectsParent);
            if (RainSound.clip != null)
            {
                RainSound.clip = null;
                //Resources.UnloadUnusedAssets();
            }
        }
    }

    public void SetCurrentEffects()  //включаем эффекты для текущего острова
    {
       if (EffectsOn)
        {
            for (int i = 0; i < AllEffects.Count; i++)
                AllEffects[i].SetActive(false);
            AllEffects[GOD.EnviromentContr.CurrentIslandNumber].SetActive(true);
            if (GOD.EnviromentContr.CurrentIsland.Contains("Water"))
            {
                AllFoamWater.SetActive(true);
                AllFoamSand.SetActive(false);
            }
            else if (GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
            {
                AllFoamSand.SetActive(true);
                AllFoamWater.SetActive(false);
            }
            else
            {
                AllFoamWater.SetActive(false);
                AllFoamSand.SetActive(false);
            }
        }
    }

    public void ChangeFoam(bool b)//изменить брызги водопадов
    {
        FoamScript[] f = AllFoamScriptWater;
        if(GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
            f = AllFoamScriptSand;
        StartCoroutine(ChangeFoamGradually(b,f));
    }
    IEnumerator ChangeFoamGradually(bool b, FoamScript[] AllFoamScript)
    {
        if (b)//включаем
        {
            for (int i = 0; i < AllFoamScript.Length; i++)  // включаем брызги
                AllFoamScript[i].ChangeFoam(1);
            yield return new WaitForSeconds(2);
            for (int i = 0; i <AllFoamScript.Length; i++)  // включаем брызги
                AllFoamScript[i].ChangeFoam(2);
            yield return new WaitForSeconds(2);
            for (int i = 0; i < AllFoamScript.Length; i++)  // включаем брызги
                AllFoamScript[i].ChangeFoam(3);
        }
        else//отключаем
        {
            for (int i = 0; i < AllFoamScript.Length; i++)  // включаем брызги
                AllFoamScript[i].ChangeFoam(2);
            yield return new WaitForSeconds(2);
            for (int i = 0; i <AllFoamScript.Length; i++)  // включаем брызги
                AllFoamScript[i].ChangeFoam(1);
            yield return new WaitForSeconds(2);
            for (int i = 0; i < AllFoamScript.Length; i++)  // включаем брызги
                AllFoamScript[i].ChangeFoam(0);
        }
    }

    public void GetBush(Transform t) //сбор с куста
    {
        if(t)
        {
            AxeHit.transform.SetParent(t);
            AxeHit.transform.localPosition = new Vector3(0, 3, 0);
            GOD.DopEff.AxeHit.Play();
        }
    }
}
