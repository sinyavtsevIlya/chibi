﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoamScript : MonoBehaviour {

    public GameManager GOD;
    bool IsInit = false;
    public ParticleSystem Foam, TopFoam, Foam2;//партиклы брызг
    public ParticleSystem.MinMaxCurve emFoam, emTopFoam, emFoam2; //заданная в частицах эмиссия
    public AudioSource audio;

    public void Init()
    {
        ParticleSystem p = GetComponent<ParticleSystem>();
        if (p)
        {
            Foam = p;
            emFoam = p.emission.rateOverTime;
        }
        else return;
        Transform t = transform.Find("TopFoam");
        if (t)
        {
            p = t.GetComponent<ParticleSystem>();
            if(p)
            {
                TopFoam = p;
                emTopFoam = p.emission.rateOverTime;
            }
        }
        else return;
        t = transform.Find("Foam2");
        if (t)
        {
            p = t.GetComponent<ParticleSystem>();
            if (p)
            {
                Foam2 = p;
                emFoam2 = p.emission.rateOverTime;
            }
        }
        else return;
        audio = GetComponent<AudioSource>();
        GameObject g = GameObject.Find("GameManager");
        if (g == null)
            return;
        GOD = g.GetComponent<GameManager>();
        if (GOD == null)
            return;
        IsInit = true;
    }

    public void ChangeFoam(int step)
    {
        if (IsInit)
        {
            float currentRate1 = 0;
            float currentRate2 = 0;
            float currentRate3 = 0;
            switch (step)
            {
                case 1:
                    currentRate1 = 50;
                    currentRate2 = 50;
                    currentRate3 = 50;
                    if (audio)
                        audio.volume = GOD.Settings.SoundVolume/3;
                    break;
                case 2:
                    currentRate1 = emFoam.constant / 2;
                    currentRate2 = emTopFoam.constant / 2;
                    currentRate3 = emFoam2.constant / 2;
                    if (audio)
                        audio.volume = GOD.Settings.SoundVolume/2;
                    break;
                case 3:
                    currentRate1 = emFoam.constant;
                    currentRate2 = emTopFoam.constant;
                    currentRate3 = emFoam2.constant;
                    if (audio)
                        audio.volume = GOD.Settings.SoundVolume;
                    break;
                default:
                    if(audio)
                    audio.volume = 0;
                    break;
            }
            var c = Foam.emission;
            c.rateOverTime = currentRate1;
            c = TopFoam.emission;
            c.rateOverTime = currentRate2;
            c = Foam2.emission;
            c.rateOverTime = currentRate3;
        }
    }

    /* public void OnOffFoam(bool b)
     {
         if (IsInit)
         {
             if (b)//включаем брызги
             {
                 var c = Foam.emission;
                 c.rateOverTime = emFoam;
                 c = TopFoam.emission;
                 c.rateOverTime = emTopFoam;
                 c = Foam2.emission;
                 c.rateOverTime = emFoam2;
             }
             else //выключаем брызги
             {
                 var c = Foam.emission;
                 c.rateOverTime = 0f;
                 c = TopFoam.emission;
                 c.rateOverTime = 0f;
                 c = Foam2.emission;
                 c.rateOverTime = 0f;
             }
         }
     }*/
}
