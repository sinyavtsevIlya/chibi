﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeGame : MonoBehaviour {

    public GameManager GOD;

    [Header("Вемя:")]
    public int IYear, IMonth, IDay, IHour, IMinute;
    public int AllDays = 0; //всего игровых дней 
    public float ISecond = 0;
    public float RealSecond = 0;

    public Transform TimeCircle;  //круг времени
    public Animator DayAnimator;
    [Header("Интерфейс времени:")]
    public GameObject DayWindow;
    public Text DayText, WindowTextMain, WindowText, ButtonText;

    Vector3 NewRot;
    public void Init()
    {
        TimeCircle.rotation = Quaternion.Euler(new Vector3(0, 0, 15 * IHour));
        NewRot = TimeCircle.rotation.eulerAngles;
        if (GOD.IsTutorial)
            StartTime();
    }

    public void ShowNewDay() //отображаем новый день
    {
        GOD.GamePause();
        ShowCurrentDay();
        WindowTextMain.text = Localization.instance.getTextByKey("#Сongratulations");
        WindowText.text =Localization.instance.getTextByKey("#Dead1") + " " + (IDay-1) + " " + Localization.instance.getTextByKey("#Dead2");
        ButtonText.text = Localization.instance.getTextByKey("#Share");
        DayWindow.SetActive(true);
        GOD.Progress.SaveMaxDay(IDay-1);
        Resources.UnloadUnusedAssets();  //выгрузка всего
    }

    public void ShowCurrentDay()
    {
        StopAllCoroutines();
        DayText.text = Localization.instance.getTextByKey("#Day") + " " + IDay;
        DayAnimator.Play("OpenDay");
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.FishingSucsess);
        StartCoroutine(WaitDay());
    }
    IEnumerator WaitDay()
    {
        yield return new WaitForSeconds(3f);
        DayAnimator.Play("CloseDay");
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.FishingFail);
        if (GOD.QuestContr.IsInit)
        {
            if(IDay >= 2)
                GOD.QuestContr.AddNewQuest(GOD.QuestContr.Leather_Armor);
            if(IDay>=3)
                GOD.QuestContr.AddNewQuest(GOD.QuestContr.House);
        }
    }

    public void CloseDayWindow()
    {
        GOD.GamePlay();
        DayWindow.SetActive(false);
    }
    public void StartTime()
    {
        IYear = 0;
        IMonth = 0;
        IDay = 1;
        IHour = 6;
        IMinute = 0;

        TimeCircle.rotation = Quaternion.Euler(new Vector3(0, 0, 15*IHour));
        NewRot = TimeCircle.rotation.eulerAngles;
    }
    public void TimeAfterDead()
    {
        IYear = 0;
        IMonth = 0;
        IDay = 1;
    }
    void Update()
    {
        if (!GOD.IsTutorial)
        {
            RealSecond += Time.deltaTime;
            if(RealSecond>=60)
            {
                RealSecond = 0;
                GOD.Progress.SaveMaxTime();
            }


            ISecond += Time.deltaTime;  // секунды
                                        //TimeCircle.rotation = Quaternion.Euler(Vector3.RotateTowards(TimeCircle.rotation.eulerAngles, NewRot,0.1f,0.1f));
            TimeCircle.rotation = Quaternion.Lerp(TimeCircle.rotation, Quaternion.Euler(NewRot), Time.deltaTime * 0.5f);
            if (ISecond >= 1)    // минуты
            {
                IMinute += 1;
                ISecond = 0;
                if (IMinute >= 37)  // часы
                {
                    CheckValidity();  // меняем годность
                    IHour += 1;

                    if (IHour == 24)                          //крутим время
                        NewRot = new Vector3(0, 0, 0);
                    else
                        NewRot = new Vector3(0, 0, 15 * IHour);

                    if (IHour == 6 || IHour == 20 || IHour == 1)
                        GOD.EnviromentContr.ChangeTime();
                    IMinute = 0;
                    if (IHour >= 24)      // дни
                    {
                        IDay += 1;
                        IHour = 0;
                        AllDays++;
                     /*   if (IDay >= 30)       // месяцы
                        {
                            IMonth += 1;
                            IDay = 1;
                            if (IMonth >= 12)   // года
                            {
                                IYear += 1;
                                IMonth = 0;
                            }
                        }*/
                    }
                }
            }
            ToSecond();
        }
    }

    public int ToSecond()    // текущее время в минутах игровых
    {
        // int x = (IYear * 12 * 30 * 24*37) + (IMonth * 30 * 24*37) + (IDay * 24*37)+(IHour * 37)+IMinute;
        int x = (IDay * 24 * 37) + (IHour * 37) + IMinute;
        return x;
    }
    void CheckValidity()      // изменяем годность всех предметов
    {
        for(int i=0; i<GOD.InventoryContr.ObjectsWithValidity.Count; i++)
        {
            GOD.PlayerInter.ChangeValidity(GOD.InventoryContr.ObjectsWithValidity[i], 1);
        }
    }

}
