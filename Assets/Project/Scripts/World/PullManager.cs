﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PullManager : MonoBehaviour {

    public GameObject Pull;
    public GameObject Stick1, Stick2, Wood1, Wood2, Stone1, Stone2, Stone3, Flint1, Ice1, Ice2, IronOre1, IronOre2;
    public GameObject Needle1, Needle2, Meat1, Meat2, Honey1, Honey2, Skin1, Skin2, Amethyst1, Amethyst2;
    public GameObject Web1, Web2, Dimond1, Dimond2, Ruby1, Ruby2, Sapphire1, Sapphire2, Crystall1, Crystall2, Emerald1, Emerald2;
    public GameObject Wool1, Wool2, CrabShell1, CrabShell2, Seed1, Seed2;
    public GameObject StoneBlock1, StoneBlock2, Plank1, Plank2, Rope1, Rope2, IronIngot1, IronIngot2, Snow1, Snow2;
    public GameObject Thread1, Thread2, Glass1, Glass2, Gem1;
    // объекты, хранящиеся в пуле
    public Dictionary<GameObject, bool> Wood = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Stick = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Seed = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Stone = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Flint = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Ice = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> IronOre = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Needle = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Meat = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Skin = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Wool = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> CrabShell = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Honey = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Web = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Amethyst = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Ruby = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Sapphire = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Emerald = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Crystall = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Dimond = new Dictionary<GameObject, bool>();

    public Dictionary<GameObject, bool> StoneBlock = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Plank = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Rope = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> IronIngot = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Snow = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Thread = new Dictionary<GameObject, bool>();
    public Dictionary<GameObject, bool> Glass = new Dictionary<GameObject, bool>();

    public Dictionary<GameObject, bool> Gem = new Dictionary<GameObject, bool>();

    Dictionary<GameObject, bool> CurrentDictionary = new Dictionary<GameObject, bool>();
    GameObject FirstObject = null;

    void Start()
    {
        Wood.Add(Wood1, false);
        Wood.Add(Wood2, false);
        Stick.Add(Stick1, false);
        Stick.Add(Stick2, false);
        Seed.Add(Seed1, false);
        Seed.Add(Seed2, false);
        Stone.Add(Stone1, false);
        Stone.Add(Stone2, false);
        Stone.Add(Stone3, false);
       Flint.Add(Flint1, false);
        Ice.Add(Ice1, false);
        Ice.Add(Ice2, false);
        IronOre.Add(IronOre1, false);
        IronOre.Add(IronOre2, false);
        Needle.Add(Needle1, false);
        Needle.Add(Needle2, false);
        Meat.Add(Meat1, false);
        Meat.Add(Meat2, false);
        Skin.Add(Skin1, false);
        Skin.Add(Skin2, false);
        Wool.Add(Wool1, false);
        Wool.Add(Wool2, false);
        CrabShell.Add(CrabShell1, false);
        CrabShell.Add(CrabShell2, false);
        Honey.Add(Honey1, false);
        Honey.Add(Honey2, false);
        Web.Add(Web1, false);
       Web.Add(Web2, false);
       Crystall.Add(Crystall1, false);
        Crystall.Add(Crystall2, false);
        Ruby.Add(Ruby1, false);
        Ruby.Add(Ruby2, false);
        Sapphire.Add(Sapphire1, false);
        Sapphire.Add(Sapphire2, false);
       Emerald.Add(Emerald1, false);
        Emerald.Add(Emerald2, false);
        Amethyst.Add(Amethyst1, false);
        Amethyst.Add(Amethyst2, false);
        Dimond.Add(Dimond1, false);
       Dimond.Add(Dimond2, false);

        StoneBlock.Add(StoneBlock1, false);
       StoneBlock.Add(StoneBlock2, false);
       Plank.Add(Plank1, false);
        Plank.Add(Plank2, false);
        Rope.Add(Rope1, false);
        Rope.Add(Rope2, false);
        IronIngot.Add(IronIngot1, false);
        IronIngot.Add(IronIngot2, false);
        Snow.Add(Snow1, false);
        Snow.Add(Snow2, false);
        Thread.Add(Thread1, false);
        Thread.Add(Thread2, false);
        Glass.Add(Glass1, false);
        Glass.Add(Glass2, false);

        Gem.Add(Gem1, false);
    }

    public GameObject GetObject(string name, Vector3 Newposition, Quaternion Newrotation)   // берем объекты из пулла
    {
        GameObject Result = null;
        WhatDictionary(name);

        if (CurrentDictionary != null)                          // назначаем объект, если он не используется
        {
            foreach (var kvp in CurrentDictionary)
            {
                if (!kvp.Value)
                {
                    CurrentDictionary[kvp.Key] = true;
                    Result = kvp.Key;
                    break;
                }
            } 
            if(Result == null)                                 // создаем новый объект в пулл, если не хватило старых
            {
                GameObject newPull = Instantiate(FirstObject, FirstObject.transform.position, FirstObject.transform.rotation) as GameObject;
                newPull.transform.SetParent(FirstObject.transform.parent);
                newPull.name = FirstObject.name;
                newPull.GetComponent<Rigidbody>().isKinematic = false;
               newPull.GetComponent<BoxCollider>().isTrigger = false;
                newPull.GetComponent<Animator>().enabled = false;
                CurrentDictionary.Add(newPull, true);
                Result = newPull;
            }
        }

        if (Result != null)                                   // устанавливаем объекту позицию и поворот
        {
            Result.transform.position = Newposition;
            Result.transform.rotation = Newrotation;
            Result.SetActive(true);
        }
        return Result;
    }

    public void ReturnObject(GameObject ReturnedLut)                            // возвращаем объекты в пулл
    {
        WhatDictionary(ReturnedLut.name);
        if (CurrentDictionary != null)                          // скрываем объект и сообщаем, что его можно использовать
        {
            if (CurrentDictionary.ContainsKey(ReturnedLut))
            {
                CurrentDictionary[ReturnedLut] = false;
                ReturnedLut.SetActive(false);
                ReturnedLut.GetComponent<Rigidbody>().isKinematic = false;
                ReturnedLut.GetComponent<BoxCollider>().isTrigger = false;
                ReturnedLut.GetComponent<Animator>().enabled = false;
            }
        }
    }

    public void WhatDictionary(string name)                      
    {
        switch (name)                                            // задаем массив объектов по имени
        {
            case "Wood":
                CurrentDictionary = Wood;
                FirstObject = Wood1;
                break;
            case "Stick":
                CurrentDictionary = Stick;
                FirstObject = Stick1;
                break;
            case "Seed":
                CurrentDictionary = Seed;
                FirstObject = Seed1;
                break;
            case "Stone":
                CurrentDictionary = Stone;
                FirstObject = Stone1;
                break;
            case "Flint":
                CurrentDictionary = Flint;
                FirstObject = Flint1;
                break;
            case "Ice":
                CurrentDictionary = Ice;
                FirstObject = Ice1;
                break;
            case "IronOre":
                CurrentDictionary = IronOre;
                FirstObject = IronOre1;
                break;
            case "Crystall":
                CurrentDictionary = Crystall;
                FirstObject = Crystall1;
                break;
            case "Amethyst":
                CurrentDictionary = Amethyst;
                FirstObject = Amethyst1;
                break;
            case "Ruby":
                CurrentDictionary = Ruby;
                FirstObject = Ruby1;
                break;
            case "Sapphire":
                CurrentDictionary = Sapphire;
                FirstObject = Sapphire1;
                break;
            case "Emerald":
                CurrentDictionary = Emerald;
                FirstObject = Emerald1;
                break;
            case "Dimond":
                CurrentDictionary = Dimond;
                FirstObject = Dimond1;
                break;
            case "Needle":
                CurrentDictionary = Needle;
                FirstObject =Needle1;
                break;
            case "Meat":
                CurrentDictionary = Meat;
                FirstObject = Meat1;
                break;
            case "Skin":
                CurrentDictionary = Skin;
                FirstObject = Skin1;
                break;
            case "Wool":
                CurrentDictionary = Wool;
                FirstObject = Wool1;
                break;
            case "CrabShell":
                CurrentDictionary = CrabShell;
                FirstObject = CrabShell1;
                break;
            case "Honey":
                CurrentDictionary = Honey;
                FirstObject = Honey1;
                break;
            case "Web":
              CurrentDictionary = Web;
                FirstObject = Web1;
                break;
            case "StoneBlock":
                CurrentDictionary = StoneBlock;
                FirstObject = StoneBlock1;
                break;
            case "Plank":
                CurrentDictionary = Plank;
                FirstObject = Plank1;
                break;
            case "Rope":
                CurrentDictionary = Rope;
                FirstObject = Rope1;
                break;
            case "IronIngot":
                CurrentDictionary = IronIngot;
                FirstObject = IronIngot1;
                break;
            case "Snow":
                CurrentDictionary = Snow;
                FirstObject = Snow1;
                break;
            case "Thread":
                CurrentDictionary = Thread;
                FirstObject = Thread1;
                break;
            case "Glass":
                CurrentDictionary = Glass;
                FirstObject = Glass1;
                break;
            case "Gem":
                CurrentDictionary = Gem;
                FirstObject = Gem1;
                break;
            default:
                CurrentDictionary = null;
                break;
        }
    }
}
