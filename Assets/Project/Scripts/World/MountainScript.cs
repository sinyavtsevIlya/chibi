﻿using UnityEngine;
using System.Collections;

public class MountainScript : MonoBehaviour {

    public GameManager GOD;

    public Transform PlayerBottomPosition;
    public Vector3 PlayerBottomRotation;
    public Transform PlayerTopPosition;
    public Vector3 PlayerTopRotation;

    public Transform TopPosition;
    public GameObject BotBtn, TopBtn, AutoBot, AutoUp;

    public void SetMountain(bool b)
    {
        if(b)
        {
            BotBtn.SetActive(true);
            TopBtn.SetActive(true);
            AutoBot.SetActive(true);
            AutoUp.SetActive(true);
        }
        else
        {
            BotBtn.SetActive(false);
            TopBtn.SetActive(false);
            AutoBot.SetActive(false);
            AutoUp.SetActive(false);
        }
        if(GOD.IsTutorial)
            TopBtn.SetActive(false);
    }

    public void MovePlayer(string Place)
    {
        switch(name)
        {
            case "Mountain1":
                GOD.Progress.SetAchivments(6);  //ачивка за ледяную глыбу
                break;
            case "Mountain2":
                GOD.Progress.SetAchivments(7);  //ачивка за лианы
                break;
            case "Mountain3":
                GOD.Progress.SetAchivments(9);  //ачивка за двойную смену погоды
                break;
        }
        if (Place == "Bot" || Place == "AutoBot")
        {
            GOD.Player.transform.position = PlayerBottomPosition.position;
            GOD.Player.transform.eulerAngles = PlayerBottomRotation;
        }
        else
        {
            GOD.Player.transform.position = PlayerTopPosition.position;
            GOD.Player.transform.eulerAngles = PlayerTopRotation;
        }

        GOD.TPUC.PlayerUp = true;
        GOD.PlayerAnimation.m_Animator.SetBool("PlayerUp", true);

        BotBtn.SetActive(false);
        TopBtn.SetActive(false);
        AutoBot.SetActive(false);
        AutoUp.SetActive(false);

        if (GOD.PlayerAnimation.ItemOnHand)
            GOD.PlayerAnimation.ItemOnHand.SetActive(false);
    }

    public void RemovePlayer(string n)
    {
        BotBtn.SetActive(true);
        TopBtn.SetActive(true);
        AutoBot.SetActive(true);
        if (GOD.IsTutorial)
            AutoUp.SetActive(false);
        else
        AutoUp.SetActive(true);

        GOD.TPUC.PlayerUp = false;
        GOD.PlayerAnimation.m_Animator.SetBool("PlayerUp", false);
        if (GOD.PlayerAnimation.ItemOnHand)
            GOD.PlayerAnimation.ItemOnHand.SetActive(true);

        if(n == "StopUp")
        {
            GOD.Player.transform.position = TopPosition.position;

            if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 16)
                GOD.Tutor.NextStep = true;
        }

    }

}
