﻿using UnityEngine;
using System.Collections;

public class TornadoScript : MonoBehaviour {

    public GameManager GOD;
    public bool IsRotate = false;
    Transform TornadoPoint, CenterTornadoPoint;

    float minx;
    float minz;
    float maxx;
    float maxz;

    float minx2;
    float minz2;
    float maxx2;
    float maxz2;

    Vector3 target;

    bool IsDamage = false;
    int CurrentIland = 0;

    public void Init(GameManager g, int num)
    {
        GOD = g;
        CenterTornadoPoint = transform.Find("Center");
        TornadoPoint = transform.Find("Point");
        switch (num)
        {
            case 1:
                minx = 178;
                minz = -50;
                maxx = 290;
                maxz = 67;
                break;
            case 2:
                minx = 307;
                minz = 74;
                maxx = 418;
                maxz = 130;

                minx2 = 388;
                minz2 = 123;
                maxx2 = 467;
                maxz2 = 200;
                break;
            case 3:
                minx = 434;
                minz = -3;
                maxx = 495;
                maxz = 70;

                minx2 = 513;
                minz2 = -62;
                maxx2 = 561;
                maxz2 = 27;
                break;
        }
        CurrentIland = num;
        ChangeTarget();
    }

	void Update()
    {
        if (IsRotate)
        {
            transform.RotateAround(target, Vector3.up,Time.deltaTime*10);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime);
            if (transform.position == target)
                ChangeTarget();
        }
    }

    void ChangeTarget()
    {
        if (IsRotate)
            target = transform.position + new Vector3(3, transform.position.y, 3);
        else
        {
            switch(CurrentIland)
            {
                case 1:
                    target = new Vector3(Random.Range(minx, maxx), transform.position.y, Random.Range(minz, maxz));
                    break;
                case 2:
                    int x = Random.Range(-1, 1);
                    if(x<0)
                        target = new Vector3(Random.Range(minx, maxx), transform.position.y, Random.Range(minz, maxz));
                    else
                        target = new Vector3(Random.Range(minx2, maxx2), transform.position.y, Random.Range(minz2, maxz2));
                    break;
                case 3:
                    x = Random.Range(-1, 1);
                    if (x < 0)
                        target = new Vector3(Random.Range(minx, maxx), transform.position.y, Random.Range(minz, maxz));
                    else
                        target = new Vector3(Random.Range(minx2, maxx2), transform.position.y, Random.Range(minz2, maxz2));
                    break;
            }
        }
    }

    public void SetDamage()
    {
        if (!IsDamage)
        {
            StopCoroutine(TornadoDamage());
            StartCoroutine(TornadoDamage());
            IsDamage = true;
            GOD.TornadoContr.PlayerTornadoMove(TornadoPoint, CenterTornadoPoint);
            //GOD.TPC.MoveCharacterUp(new Vector3(0, 5, 0));
            GOD.PlayerInter.LostSomeLut();
        }
    }

 
    public void StopDamage()
    {
        StopAllCoroutines();
        IsDamage = false;
    }
    IEnumerator TornadoDamage()
    {
        GOD.PlayerContr.HP--;
        GOD.PlayerInter.ShowIndicators();
        if (!GOD.Audio.Sound.isPlaying)
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Pain);
        yield return new WaitForSeconds(1);
        StartCoroutine(TornadoDamage());
    }


}
