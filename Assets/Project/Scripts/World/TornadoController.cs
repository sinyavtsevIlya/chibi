﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoController : MonoBehaviour {

    public GameManager GOD;
    Transform TornadoPoint = null, CenterTornadoPoint = null;

    public void PlayerTornadoMove(Transform tp, Transform ctp)
    {
        if (tp != null && ctp != null && GOD.TPC.UsePlayerMove)
        {
            TornadoPoint = tp;
            CenterTornadoPoint = ctp;
            GOD.TPC.UsePlayerMove = false;
            GOD.Player.transform.SetParent(CenterTornadoPoint);
            StopAllCoroutines();
            StartCoroutine(Move());
            if (GOD.Audio.PlayerSound.isPlaying)
                GOD.Audio.PlayerSound.Stop();
        }
    }
    IEnumerator Move()
    {
        while (Vector3.SqrMagnitude(GOD.Player.transform.position-TornadoPoint.position) > 4f)
        {
            GOD.Player.transform.position = Vector3.MoveTowards(GOD.Player.transform.position, TornadoPoint.position, 0.5f);
            yield return new WaitForFixedUpdate();
        }
        int timer = 0;
        float PlayerPos = GOD.Player.transform.position.y;
        float y = CenterTornadoPoint.rotation.y;
        while (timer < 100)
        {
            y += 4f;
            CenterTornadoPoint.rotation = Quaternion.Euler(new Vector3(0, y, 0));
            GOD.Player.transform.position = new Vector3(GOD.Player.transform.position.x, PlayerPos, GOD.Player.transform.position.z);
            timer++;
            yield return new WaitForFixedUpdate();
        }
        Vector3 v = GOD.Player.transform.position - CenterTornadoPoint.position;
        GOD.TPC.MoveCharacterUp(v * 2f);
        timer = 0;
        while (timer < 10)
        {
            timer++;
            yield return new WaitForFixedUpdate();
        }
        GOD.TPC.UsePlayerMove = true;
        GOD.Player.transform.SetParent(null);
        CenterTornadoPoint.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        TornadoPoint = null;
        CenterTornadoPoint = null;
    }
}
