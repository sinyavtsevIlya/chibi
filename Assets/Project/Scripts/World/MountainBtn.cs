﻿using UnityEngine;
using System.Collections;

public class MountainBtn : MonoBehaviour {

    public MountainScript MyMountain;

    public void MountainBtnUp(bool needCheck=false)
    {
        if(!needCheck || Vector3.SqrMagnitude(MyMountain.GOD.Player.transform.position- transform.position)<25)
            MyMountain.MovePlayer(name);
    }
}
