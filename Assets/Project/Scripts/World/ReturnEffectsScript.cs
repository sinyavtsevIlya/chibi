﻿using UnityEngine;
using System.Collections;

public class ReturnEffectsScript : MonoBehaviour {

    public GameManager GOD;
    ParticleSystem P;

    public void Init()
    {
        P = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if(P.isStopped)
        {
            transform.SetParent(GOD.DopEff.EffectsParent);
        }
    }
}
