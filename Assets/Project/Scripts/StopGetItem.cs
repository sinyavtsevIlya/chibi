﻿using UnityEngine;
using System.Collections;

public class StopGetItem : MonoBehaviour {
    
    public void NoGetItem()
    {
        gameObject.SetActive(false);
    }
}
