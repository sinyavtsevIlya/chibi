#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("/lRHDOdBWcaWieSFzvQax5ZfJcVr+XN2nQ78AFS9UaImc7yHubQMaS3kgZ+jgRpw9Y4sd2s0sgCqs3kZLHEer+CWbfKzrs2YVuxkqgRoO8r4/UDNVOnXOa3lYtvG/Qx/gE6bpdoaCWx0g+p7KgAI5Nhj+vC0rPMO6FGtDPI9Agm357sYdktWOpeRgaSfptgdEubMgmwcokLOt70C0iTmwwlskoHn1EFq6ofC5pypryinjgs+YdNQc2FcV1h71xnXplxQUFBUUVJNx7Fab/n0wyOG92reyuKwhd7v74gBpwvuyp+sl5QyyaB1ab5hudRdutVN5ZXT1jVg60EJqZxUPUr+owvTUF5RYdNQW1PTUFBR1tC5juzI2mdVLHa+XbcsTlNSUFFQ");
        private static int[] order = new int[] { 10,3,4,5,8,9,11,13,11,10,13,11,12,13,14 };
        private static int key = 81;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
