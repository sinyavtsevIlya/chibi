#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("oLv6vReu92qQH1JDdj6yZM73xvmCDEaD2s12mHDD5Bmwdqs/ytHIcTVB47+oV7hIRJJL9kk/ub6Majwx7fH4vd747+n0+/T+/On08vO93Oj6EpUpvWpWMbG98u0ropytESreUiinMGmSk50Pliy8i7PpSKGQRv+L560fnOutk5ueyICSnJximZmen5z0+/T+/On08vO93Ojp9fLv9OnkrBLuHP1bhsaUsg8vZdnVbf2lA4hoNj7sD9rOyFwystwuZWZ+7VB7PtHp9fLv9OnkrIutiZueyJmejpDc7e/8/un0/vi97un86fjw+PPp7rOtm62Sm57IgI6cnGKZmK2enJxirYCQm5S3G9UbapCcnJiYnZ4fnJydwYIYHhiGBKDaqm80Bt0TsUksDY9F//H4ve7p/PP5/O/5ven47/Duvfzq6rP87e3x+LP+8vCy/O3t8fj+/JIAoG621LWHVWNTKCSTRMOBS1agFoQUQ2TW8WiaNr+tn3WFo2XNlE6rBNGw5SpwEQZBbuoGb+tP6q3SXPP5vf7y8/n06fTy8+698vu96O74lcOtH5yMm57IgL2ZH5yVrR+cma2xvf747+n0+/T+/On4ve3y8fT+5LutuZueyJmWjoDc7e3x+L3e+O/pHYm2TfTaCeuUY2n2ELPdO2ra0OKVtpucmJian5yLg/Xp6e3up7Ky6vH4vdTz/rOsu625m57ImZaOgNzti62Jm57ImZ6OkNzt7fH4vc/y8umuq8et/6yWrZSbnsiZm46fyM6sjiNp7gZzT/mSVuTSqUU/o2TlYvZVz/jx9Pzz/vi98vO96fX07r3++O8srcVxx5mvEfUuEoBD+O5i+sP4IbPdO2ra0OKVw62Cm57IgL6Zha2LxDqYlOGK3cuMg+lOKha+pto+SPKtH5kmrR+ePj2en5yfn5yfrZCblL3e3K0fnL+tkJuUtxvVG2qQnJycXf6u6mqnmrHLdkeSvJNHJ+6E0ihEq+JcGshEOgQkr99mRUjsA+M8z7KtHF6blbabnJiYmp+frRwrhxwumJ2eH5ySna0fnJefH5ycnXkMNJSbnsiAk5mLmYm2TfTaCeuUY2n2EAgD55E52hbGSYuqrlZZktBTifRMmnHgpB4Wzr1OpVksIgfSl/ZitmG3G9UbapCcnJiYna3/rJatlJueyOn0+/T+/On4vf/kvfzz5L3t/O/pKoYgDt+5j7dakoAr0AHD/lXWHYqtjJueyJmXjpfc7e3x+L3U8/6zrJmbjp/IzqyOrYybnsiZl46X3O3t5L387u7o8Pjuvfz+/vjt6fzz/vjt8fi9z/Ly6b3e3K2DipCtq62pr/movojWiMSALglqawEDUs0nXMXNqK+sqa2uq8eKkK6ora+tpK+sqa3Y44LR9s0L3BRZ6f+WjR7cGq4XHB+cnZuUtxvVG2r++ZicrRxvrbebuX92TCrtQpLYfLpXbPDlcHooioq9/PP5vf747+n0+/T+/On08vO97dRF6wKuifg86glUsJ+enJ2cPh+cvfL7ven1+L3p9fjzvfzt7fH0/vxUhO9owJNI4sIGb7ieJ8gS0MCQbOLcNQVkTFf7Abn2jE0+JnmGt16CzTcXSEd5YU2Umqot6Oi8");
        private static int[] order = new int[] { 56,15,54,4,21,23,28,11,55,24,23,46,29,47,49,42,36,22,36,48,43,33,44,23,32,35,33,29,55,50,38,33,33,52,56,47,57,58,58,49,58,57,51,51,49,59,52,54,58,55,51,52,54,56,56,59,56,57,58,59,60 };
        private static int key = 157;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
