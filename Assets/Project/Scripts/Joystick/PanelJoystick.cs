﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;


public class PanelJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{

    public Transform basejoystick;
    public Joystick joystick;
    public bool UseInviz = true;
    public void Jump()
    {
        //player.m_Jump = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
#if UNITY_EDITOR
        CrossPlatformInputManager.SwitchActiveInputMethod(CrossPlatformInputManager.ActiveInputMethod.Touch);
#endif

        basejoystick.transform.position = eventData.position;
       // if(UseInviz)
        basejoystick.gameObject.SetActive(true);

        joystick.OnPointerDown(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        PointerUp();
 
    }

    public void PointerUp()
    {
        joystick.PointerUp();
        if (UseInviz)
            basejoystick.gameObject.SetActive(false);

        
#if UNITY_EDITOR
        CrossPlatformInputManager.SwitchActiveInputMethod(CrossPlatformInputManager.ActiveInputMethod.Hardware);
#endif
    }

    public void OnDrag(PointerEventData data)
    {
        joystick.OnDrag(data);
    }

#if UNITY_EDITOR
    void Start()
    {
        CrossPlatformInputManager.SwitchActiveInputMethod(CrossPlatformInputManager.ActiveInputMethod.Hardware);
    }
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.LeftAlt))
        //    CrossPlatformInputManager.SwitchActiveInputMethod();
    }
#endif

}
