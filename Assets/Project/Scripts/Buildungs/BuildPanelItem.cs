﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildPanelItem : MonoBehaviour {


    public int thisCount = 0;
    Text thisText;

    public void Init()
    {
        thisText = transform.GetChild(0).GetChild(0).GetComponent<Text>();
    }
    public void SetTextCount(int x)
    {
        thisCount = x;
        thisText.text = "" + thisCount;
    }
}
