﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

    bool IsInit = false;
    bool DoorOpen = false;

    public void Init()
    {
        IsInit = true;
    }

    public void Door()
    {
        if (IsInit)
        {
            if (DoorOpen)
                CloseDoor();
            else
                OpenDoor();
        }
    }
    public void OpenDoor()
    {
        transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));
                DoorOpen = true;
                Transform p = transform.parent;
                p.tag = "Ignored";
                p.GetComponent<DynamicMapUpdate>().Init(true);
    }

    public void CloseDoor()
    {
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                DoorOpen = false;
                Transform p = transform.parent;
                p.tag = "House";
                p.GetComponent<DynamicMapUpdate>().Init(true);
    }
}
