﻿using UnityEngine;
using System.Collections;

public class BridgeScript : MonoBehaviour {

    public bool IsOpen = false;
    public string ResName1, ResName2;
    public int ResCount1, ResCount2;
    public GameObject Bridge;
    public Transform ForPuffPosition;

    void Start()
    {
        SetResoures();
    }

    public void SetResoures()
    {
        switch(name)
        {
            case "ToSnow":
                ResName1 = "Wood";
                ResName2 = "Thread";
                ResCount1 = 5;
                ResCount2 = 5;
                break;
            case "ToSand":
                ResName1 = "CrabShell";
                ResName2 = "IronOre";
                ResCount1 = 5;
                ResCount2 = 5;
                break;
            case "ToRain":
                ResName1 = "Ice";
                ResName2 = "Crystall";
                ResCount1 = 5;
                ResCount2 = 5;
                break;
        }

        if(IsOpen)
        {
            Bridge.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
