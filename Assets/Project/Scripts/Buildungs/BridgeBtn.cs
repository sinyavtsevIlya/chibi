﻿using UnityEngine;
using System.Collections;

public class BridgeBtn : MonoBehaviour {
    public GameManager GOD;
    public Transform thisBridge;

    private void Start()
    {
        StartCoroutine(RotateAtPlayer());
    }

    IEnumerator RotateAtPlayer()
    {
        yield return new WaitForSeconds(0.2f);
        transform.LookAt(GOD.PlayerCam.transform);
        StartCoroutine(RotateAtPlayer());
    }
    public void BridgeBtnUp()
    {
        GOD.PlayerInter.OpenBridgeCraft(thisBridge);
    /*    switch (N)
        {
            case "BridgeBrokenRight":
                GOD.PlayerInter.OpenBridgeCraft(GOD.RightBridge);
                break;
            case "BridgeBrokenLeft":
                GOD.PlayerInter.OpenBridgeCraft(GOD.LeftBridge);
                break;
            case "BridgeBrokenTop":
                GOD.PlayerInter.OpenBridgeCraft(GOD.TopBridge);
                break;
        }*/
    }
}
