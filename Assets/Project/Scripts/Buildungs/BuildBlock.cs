﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildBlock : MonoBehaviour {

    public bool CanBuild;
    List<string> UnderMe = new List<string>();

    Vector3 thisCenter;
    Vector3 thisSize;
    float checkHeigh = 2f;

    List<string> DestroyableTag = new List<string>() {"Tree","Bush", "Stone" , "Heap"};
    public List<GameObject> DestroyableObject = new List<GameObject>(); //объекты, которые будут удаленны после установки постройки

    public bool  AtPosition=false, IsUnderMe = true, IsNearMe=false;
    public bool DopBool = true;
    bool InsideWall = false;

    public bool ChechForWall = false;

    public void Init()
    {
        BoxCollider b = GetComponent<BoxCollider>();
        thisCenter = b.center;
        thisSize = b.size;
        checkHeigh = 2f;
        CanBuild = false;
        if (name.Contains("Wall")|| name.Contains("Roof"))
            {
            AtPosition = false;
        }
     /*   else if(name.Contains("Fence"))
        {
            UnderMe.Add("GroundHouse");
            UnderMe.Add("Ground");
            AtPosition = false;
        }*/
        else if(name.Contains("Floor") || name.Contains("Base"))
        {
            UnderMe.Add("Ground");
            AtPosition = false;
        }
        else
        {
            UnderMe.Add("GroundHouse");
            UnderMe.Add("Ground");
            AtPosition = false;
        }


        // NotInObject = WhatUnderMe();
        SetCanBuild();
    }


    public void SetCanBuild(Transform BlockParent=null)
    {
        if ((name == "Window") || (name == "Door"))
        {
            IsNearMe = CheckInside(BlockParent, name);
            CanBuild = IsNearMe && AtPosition;
        }
        else if(name.Contains("Stair"))
        {
            IsNearMe = CheckObjectNearWithoutObject(BlockParent);
            CanBuild = AtPosition && IsNearMe;
        }
        else if (name.Contains("Roof"))
            CanBuild = AtPosition;
        else if (name.Contains("Wall"))
        {
            IsNearMe = CheckObjectNear("GroundHouse");
            CanBuild = IsNearMe && AtPosition;
        }
        else if (name.Contains("Fence"))
        {
            IsUnderMe = WhatUnderMe();
            IsNearMe = CheckObjectNear();
            CanBuild = IsUnderMe && (IsNearMe || AtPosition) && DopBool;
        }
        else
        {
            IsUnderMe = WhatUnderMe();
            IsNearMe = CheckObjectNear();
            CanBuild = IsUnderMe && IsNearMe && DopBool;
        }
        // CanBuild = IsUnderMe &&(ObjectIn.Count==0 || AtPosition)&&DopBool;
        // print(InsideWall + "||" + IsUnderMe + " " + ObjectIn.Count + " " + AtPosition + " " + DopBool+"!!!!!!!!" + CanBuild);
    }

    bool CheckObjectNear(string DopTag="")
    {
        Collider[] Colliders  = Physics.OverlapBox(thisCenter + transform.position, thisSize*0.5f * 0.5f, transform.rotation);
        for (int i = 0; i < Colliders.Length; i++)
        {
            if (Colliders[i].transform != transform)
            {
                if ((Colliders[i].tag != "Ground") && Colliders[i].tag != "Ignored" && !DestroyableTag.Contains(Colliders[i].tag)&& Colliders[i].tag != DopTag)
                {
                    return false; 
                }
            }
        }
        return true;
    }

    bool CheckObjectNearWithoutObject(Transform BlockParent, string DopTag = "")
    {

        Collider[] Colliders = Physics.OverlapBox(thisCenter + transform.position, thisSize * 0.5f * 0.7f, transform.rotation);
        for (int i = 0; i < Colliders.Length; i++)
        {
            if (Colliders[i].transform != transform)
            {
                if ((Colliders[i].tag != "Ground") && Colliders[i].tag != "Ignored" && Colliders[i].tag != "GroundHouse"  && !DestroyableTag.Contains(Colliders[i].tag) && Colliders[i].transform!=BlockParent)
                {
                    return false;
                }
            }
        }
        return true;
    }

    bool CheckInside(Transform BlockParent=null, string childName="")
    {
        if(BlockParent!=null && childName!="")
        {
            Transform t = BlockParent.Find(childName);
            if (t == null || t==transform)
                return true;
        }
        return false;
    }
   /* void OnDrawGizmos()
    {
        Gizmos.DrawCube(thisCenter + transform.position, thisSize * 0.7f);
    }*/
    void OnTriggerEnter(Collider other)
    {
        if ((other.tag != "Ground") && other.tag != "Ignored")
        {
            if (DestroyableTag.Contains(other.tag))
                DestroyableObject.Add(other.gameObject);
        }
    }
        
    void OnTriggerExit(Collider other)
    {
        if (DestroyableTag.Contains(other.tag))
            DestroyableObject.Remove(other.gameObject);

    }


    public bool WhatUnderMe()
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(transform.position+new Vector3(0,1f,0), Vector3.down,out hit, checkHeigh))
        {
            if (UnderMe.Contains(hit.collider.tag))
                return true;
        }
            return false;
        }

}
