﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FirecampScript : MonoBehaviour {

    public GameManager GOD;

	public int FireCampHP =5;
    public ParticleSystem Fire;
    public ParticleSystem Smoke;
    public Light FireLight;
    public SphereCollider Sphere;
    public AudioSource thisAudio;
    public Transform FirecampTransform;

    int time = 0;

    public void StartFirecamp()
    {
        FirecampTransform = transform.GetChild(0);
        Fire = FirecampTransform.GetComponent<ParticleSystem>();
        Smoke = FirecampTransform.GetChild(0).GetComponent<ParticleSystem>();
        FireLight = FirecampTransform.GetChild(1).GetComponent<Light>();
        Sphere = FirecampTransform.GetComponent<SphereCollider>();
        Sphere.enabled = true;

        thisAudio = FirecampTransform.GetComponent<AudioSource>();
        thisAudio.volume = GOD.Settings.SoundVolume;
        GOD.Audio.AllFirecamps.Add(thisAudio);
        GOD.Audio.ChangeSoundVolume();

        FireState();
        StartCoroutine(FirecampLife());
    }

    public void AddHp()
    {
        if (FireCampHP < 5)
        {
            FireCampHP++;
            time = 0;
            var c = Fire.emission;
            c.rateOverTime = 60;
            c = Smoke.emission;
            c.rateOverTime = 70;
            StopAllCoroutines();
            StartCoroutine(FirecampLife());
            FireState();
        }  
    }
    IEnumerator FirecampLife()   // отсчет жизни костра
    {
        while (FireCampHP > 0)
        {
            if (time == 37)
            {
                time = 0;
                FireCampHP--;
                FireState();
            }
            yield return new WaitForSeconds(1);
            time++;
        }
       thisAudio.volume = 0;
       StartCoroutine(FirecampDead());
    }

    IEnumerator FirecampDead()  // после того как костер погас
    {
        int wait = 0;
        var c = Fire.emission;
        c.rateOverTime = 0;
        c = Smoke.emission;
        c.rateOverTime = 0;
        StartCoroutine(ChangeLight(0));
        while (wait<10)
        {
            yield return new WaitForSeconds(1);
            wait++;
        }
        GOD.Audio.AllFirecamps.Remove(thisAudio);
        if (GOD.NT.FirecаmpsAround.Contains(FirecampTransform))
            GOD.NT.FirecаmpsAround.Remove(FirecampTransform);
        Destroy(gameObject);
    }

    void FireState()
    {
        StopCoroutine(ChangeLight(8));
        switch (FireCampHP)
        {
            case 5:
                var mainModule = Fire.main;
                mainModule.startSize = 1f;
                mainModule = Smoke.main;
                mainModule.startSize = 1.5f;
                StartCoroutine(ChangeLight(8));
                if (GOD.PlayerInter.FirecampWindow.activeSelf)
                    GOD.PlayerInter.OpenFirecampWindow();
                SetCurrentVolume();
                break;
            case 4:
                mainModule = Fire.main;
                mainModule.startSize = 0.8f;
                mainModule = Smoke.main;
                mainModule.startSize = 1.2f;
                StartCoroutine(ChangeLight(7));
                if (GOD.PlayerInter.FirecampWindow.activeSelf)
                    GOD.PlayerInter.OpenFirecampWindow();
                SetCurrentVolume();
                break;
            case 3:
                mainModule = Fire.main;
                mainModule.startSize = 0.7f;
                mainModule = Smoke.main;
                mainModule.startSize = 1f;
                StartCoroutine(ChangeLight(6));
                SetCurrentVolume();
                break;
            case 2:
                mainModule = Fire.main;
                mainModule.startSize = 0.6f;
                mainModule = Smoke.main;
                mainModule.startSize = 1.9f;
                StartCoroutine(ChangeLight(4));
                SetCurrentVolume();
                break;
            case 1:
                mainModule = Fire.main;
                mainModule.startSize = 0.5f;
                mainModule = Smoke.main;
                mainModule.startSize = 0.7f;
                StartCoroutine(ChangeLight(3));
                SetCurrentVolume();
                break;
        }
        if (GOD.PlayerInter.FirecampWindow.activeSelf)
            GOD.PlayerInter.OpenFirecampWindow();

    }

    IEnumerator ChangeLight(int newL)  // постепенная смена освещения
    {

        if (newL < FireLight.intensity)
        {
            while (FireLight.intensity > newL)
        {
            FireLight.intensity -= 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        }
        else
        {
            FireLight.intensity = newL;
        }
    }

    public void SetCurrentVolume()
    {
        switch (FireCampHP)
        {
            case 5:
                thisAudio.volume = GOD.Settings.SoundVolume;
                break;
            case 4:
                thisAudio.volume = 4 * GOD.Settings.SoundVolume / 5;
                break;
            case 3:
                thisAudio.volume = 3 * GOD.Settings.SoundVolume / 5;
                break;
            case 2:
                thisAudio.volume = 2 * GOD.Settings.SoundVolume / 5;
                break;
            case 1:
                thisAudio.volume = GOD.Settings.SoundVolume / 5;
                break;
            case 0:
                thisAudio.volume = 0;
                break;
        }
    }


}
