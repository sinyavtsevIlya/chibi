﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuildingScript : MonoBehaviour {

    public GameManager GOD;
    public GameObject Buildings;   // родитель построек
    public GameObject[] blockPrefabs;
    public GameObject block;           // текущая постройка

    // панель строительства
    [Header("Панель строительства:")]
    public Transform[] Icons;
    public BuildPanelItem[] IconsText;
    public GameObject BuildButtons, DeleteButtons;     // кнопки строительства
    public Selectable Rotate; //кнопка поворота
    public Transform IconsParent, Group;//родитель иконок строительства, группа кнопок, которые нужно отключить
    public GameObject BuildingPanel, BuildIcons;//панель по которой отлавливается нажатие при строительстве и иконки строительства

    public GameObject SelectCube; //куб для показа удаляемого объекта
    Material SelectCubeImage;

    public bool BuildPanelDown = false;
    string blockname;
    string blocktype;
    BuildBlock BuildB;
    Vector3 blockSize;
    Transform CurrentNear;
    Transform BlockParent;  // родель для двери

    Vector3 CorrectPositionVector;
    string CorrectDirection;

    InfoScript blockIconInGame;   // храним ссылку на нажатую иконку
    public GameObject HighWall, LowWall; //костыль для стен

    public Transform targetToDelete;

    public List<Transform> ChangedElemets = new List<Transform>(); //список изменяющихся элементов
    public bool BuildingsChanged = false; //здания включены или отключены

    public void Init()
    {
        Icons = new Transform[IconsParent.childCount];
        IconsText = new BuildPanelItem[IconsParent.childCount];
        for(int i=0; i<IconsParent.childCount; i++)
        {
            Icons[i] = IconsParent.GetChild(i).GetChild(0);
            IconsText[i] = Icons[i].GetComponent<BuildPanelItem>();
            IconsText[i].Init();
        }
        SelectCubeImage = SelectCube.GetComponent<Renderer>().material;
    }

    public void BuildPanelDownUp(bool b)
    {
        BuildPanelDown = b;
    }
    public void BuildPanelClick()
    {
        if (block == null)
        {
            Ray ray = GOD.PlayerCam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.tag == "GroundHouse" || hit.transform.tag == "Build" || hit.transform.tag == "House")
                {
                    if(hit.transform.name=="CopyStair")
                        targetToDelete = hit.transform.parent;
                    else
                        targetToDelete = hit.transform;
                    DeleteButtons.SetActive(true);
                    //GameObject g = Instantiate(SelectCube);
                    SelectCubeImage.color = new Color(0, 0.04f, 1, 0.3f);
                    ShowBox(targetToDelete);
                }
                else
                    CloseDelete();
            }
            else
                CloseDelete();
        }
    }
    public void CloseDelete()
    {
        DeleteButtons.SetActive(false);
        SelectCube.transform.SetParent(GOD.DopEff.EffectsParent);
    }
    void ShowBox(Transform boxParent)//показываем радиус вокруг построек
    {
        var g = SelectCube;
        var targetBox = boxParent.GetComponent<BoxCollider>();
        g.transform.localScale = targetBox.size + Constants.VectorHalf;
        g.transform.rotation = boxParent.rotation;
        g.transform.SetParent(boxParent);
        g.transform.localPosition = targetBox.center;
    }
    
    void Update()
    {
        if (((BuildPanelDown && GOD.PlayerContr.InBuilding) || (GOD.PlayerContr.InPut&& GOD.CameraScript.OnWalkPanel) && block))
            GOD.BuildingS.MoveBlock();
        if (block && BuildB)
        {
            BuildB.SetCanBuild(BlockParent);
            if ((BuildB.CanBuild))
            {
                if(SelectCubeImage !=null)
                    SelectCubeImage.color = new Color(0, 1, 0, 0.3f);
            }
            else
            {
                if (SelectCubeImage != null)
                   SelectCubeImage.color = new Color(1, 0, 0, 0.3f);
            }
        }

    }
    public void LoadBuilding(string name, Vector3 pos, Quaternion rot, Transform p=null)//загрузка здания
    {
        if (p)
            BlockParent = p;
        else
            BlockParent = Buildings.transform;
        CreateBlock(name, null);
        block.transform.position = pos;
        block.transform.rotation = rot;
        if(blockname.Contains("Base")||blockname.Contains("Floor"))
        GOD.Save.currentBase = block.transform;
        if (blockname.Contains("Wall"))
        {
            GOD.Save.currentWall = block.transform;
            GOD.BuildingS.HighOrLow(p.name);
        }
        if (blockname=="WoodRoof")
            GOD.Save.currentRoof = block.transform;
        BuildB.CanBuild = true;
        AceptBuilding();
    }
    public void CreateBlock(string name, InfoScript ing)
    {
        blockIconInGame = ing;
        if (block != null)
            Destroy(block);
        GameObject blockPredok = SetCurrentBlock(name);
        if (blockPredok != null)
        {
            block = Instantiate(blockPredok, new Vector3(GOD.Player.transform.position.x, GOD.Player.transform.position.y, GOD.Player.transform.position.z + 5), blockPredok.transform.rotation) as GameObject;
            block.name = blockPredok.name;
            blockname = block.name;
            blocktype = SetCurrentType(block.name);
            BuildB = block.GetComponent<BuildBlock>();
            BuildB.Init();
            ShowBox(block.transform);

            blockSize = block.GetComponent<BoxCollider>().size;
            block.transform.SetParent(Buildings.transform);
            if(blockname.Contains("Wall"))
            {
                Transform t = block.transform.Find("Options");
                HighWall = t.Find("High").gameObject;
                LowWall = t.Find("Low").gameObject;
            }
            else
            {
                HighWall = null;
                LowWall = null;
            }

            if (blockname.Contains("Wall") || (blockname.Contains("Roof")&& !blockname.Contains("Corner")) || blockname.Contains("Stair") || blockname == "Window" || blockname == "Door")
                GOD.BuildingS.Rotate.interactable=false;
            else
                GOD.BuildingS.Rotate.interactable=true;
        }
    }
    public GameObject SetCurrentBlock(string name)
    {
        string s = "";
        for (int i = 5; i < name.Length; i++)
            s += name[i];
       for(int i=0; i<blockPrefabs.Length; i++)
        {
            if (blockPrefabs[i].name == s)
            {
                return blockPrefabs[i];
            }
        }
        return null;
    }
    string SetCurrentType(string s)
    {
        if (s.Contains("Wall"))
            return "Wall";
        if (s.Contains("Floor")|| s.Contains("Base"))
            return "Floor";
        else if (s.Contains("Stair"))
            return  "Stairs";
        else if (s.Contains("Fence"))
            return "Fence";
        else if (s.Contains("Window"))
            return "Window";
        else if (s.Contains("Door"))
            return "Door";
        else if (s.Contains("Roof"))
            return "Roof";
        return "";
    }


    public void RotateBuilding()
    {
        if (block != null)
        {
            if (blockname.Contains("Corner"))
            {
                if (CurrentNear != null)
                {
                    float NearY = CurrentNear.rotation.eulerAngles.y;
                    if (NearY < 0)
                        NearY += 360;
                    Vector3 rot = new Vector3(0, NearY - 90, 0);
                    if (rot.y < 0)
                        rot.y += 360;
                    float CurRotY = block.transform.rotation.eulerAngles.y;
                    if (CurRotY < 0)
                        CurRotY += 360;
                    if (CurRotY <= rot.y+5 && CurRotY >= rot.y-5) //если угол такой же - крутим на другой
                        rot = new Vector3(0, NearY - 180, 0);
                    block.transform.rotation = Quaternion.Euler(rot);
                }
            }
            else if (blockname.Contains("Fence"))
            {
                    Vector3 rot = block.transform.rotation.eulerAngles;
                    rot.y += 45;
                    if ((int)rot.y > 135)
                        rot.y= 0;
                    block.transform.rotation = Quaternion.Euler(rot);
            }
            else
            {
                Vector3 rot = block.transform.rotation.eulerAngles;
                rot.y += 45;
                block.transform.rotation = Quaternion.Euler(rot);
            }
        }
    }

    public void AceptBuilding()
    {
        SelectCube.transform.SetParent(GOD.DopEff.EffectsParent);
        if (block != null)
        {
            if ((BuildB.CanBuild))
            {
                AceptBuid(block, blocktype);
                GOD.Progress.SaveMaxBuild();
                if (!GOD.IsTutorial && GOD.QuestContr.IsInit && (GOD.QuestContr.Build.ContainsKey(block.name) || (blocktype == "Roof" && GOD.QuestContr.Build.ContainsKey("WoodRoof"))) )   //если ожидаем такой предмет, то проверяем все квесты на завершенность
                    GOD.QuestContr.CheckAllQuests("build",block.name,1);
                for (int i = 0; i < BuildB.DestroyableObject.Count; i++)//удаляем попавшие в радиус объекты
                {
                    GameObject g = BuildB.DestroyableObject[i];
                    ObjectScript o = g.GetComponent<ObjectScript>();
                    o.AddObjectToRecover();
                    o.RecoveryTime = GOD.TG.ToSecond() + GOD.PlayerInter.KD;
                    GOD.RecoveryObject.Add(o);
                   g.SetActive(false);
                    if (g.tag != "Heap")
                    {
                        if (o.MapUpdate != null && o.mapUpdateRenderer != null)
                            o.MapUpdate.UpdateMapOnce(o.mapUpdateRenderer);
                    }
                }
                GameObject help = block;
                block = null;
                BlockParent = null;
                CurrentNear = null;
                Destroy(help.GetComponent<BuildBlock>());
                Destroy(help.GetComponent<Rigidbody>());
                help.GetComponent<DynamicMapUpdate>().Init();
                GOD.InventoryContr.RemoveFromInventory(blockIconInGame, 1);
                blockIconInGame = null;
              //  GOD.PlayerContr.InBuilding = false;
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Build);
            }
            else
            {
               // GOD.PlayerContr.InBuilding = false;
                Destroy(block.gameObject);
            }
        }
    }

    void AceptBuid(GameObject block, string blocktype)//ставим здание
    {
        if (blockname.Contains("Roof"))
            block.layer = LayerMask.NameToLayer("Roof");
        else if (blockname.Contains("Floor") || blockname.Contains("Base"))
            block.layer = LayerMask.NameToLayer("Ground");
        else
            block.layer = LayerMask.NameToLayer("Default");

        block.AddComponent<ObjectScript>();
        block.GetComponent<ObjectScript>().InitGOD(GOD);

        switch(blocktype)
        {
            case "Wall":
                block.transform.SetParent(BlockParent);
                ChangedElemets.Add(block.transform);
                Transform t = block.transform.Find("Options");
                if (t.GetChild(0).gameObject.activeSelf)
                {
                    Destroy(t.GetChild(1).gameObject);
                    t.GetChild(0).name = "Wall";
                }
                else
                {
                    Destroy(t.GetChild(0).gameObject);
                    t.GetChild(1).name = "Wall";
                }
                if (BuildingsChanged)
                    PlayerIn();
                break;
            case "Roof":
                block.transform.SetParent(BlockParent);
                ChangedElemets.Add(block.transform);
                if (BuildingsChanged)
                    PlayerIn();
                break;
            case "Door":
                block.GetComponent<DoorScript>().Init();
                block.transform.SetParent(BlockParent);
                break;
            case "Stairs":
                CheckStair(block.transform);
                break;
        }
        
        if (blocktype == "Fence" && BlockParent != null)
            block.transform.SetParent(BlockParent);

        if (blockname == "Firecamp")
        {
            block.GetComponent<FirecampScript>().GOD = GOD;
            block.GetComponent<FirecampScript>().StartFirecamp();
        }
        block.GetComponent<ObjectScript>().Init();
        // включаем коллайдеры
        if (block.name == "WoodStair")
        {
            block.GetComponent<BoxCollider>().enabled = false;
            CapsuleCollider[] Cs = block.GetComponents<CapsuleCollider>();
            foreach (CapsuleCollider b in Cs)
                if (b.isTrigger)
                    b.isTrigger = false;
        }
        else
        {
            BoxCollider[] BCs = block.GetComponents<BoxCollider>();
            foreach (BoxCollider b in BCs)
            {
                if (block.name == "WoodWallDoor")
                {
                    b.enabled = !b.enabled;
                    b.isTrigger = !b.isTrigger;
                }
                else
                {
                    if (b.isTrigger)
                        b.isTrigger = false;
                }
            }
        }

        // заполняем массив сундуков
        if ((block.name == "Chest") || (block.name == "IceChest"))
        {
            GOD.InventoryContr.AllChest.Add(block.GetComponent<ChestScript>());
            for (int i = 0; i < GOD.InventoryContr.AllChest.Count; i++)
            {
                if (GOD.InventoryContr.AllChest[i] == block.GetComponent<ChestScript>())
                {
                    block.GetComponent<ChestScript>().MyNumber = i;
                    break;
                }
            }
        }
    }
    void CheckStair(Transform stair) //проверяем, дотягивается ли лестница до земли
    {
        BoxCollider b = stair.GetComponent<BoxCollider>();
        RaycastHit hit = new RaycastHit();
        Vector3 start = stair.transform.position + stair.transform.forward * b.size.z/2 + new Vector3(0, 10, 0);
        if (Physics.Raycast(start, Vector3.down, out hit, Mathf.Infinity, LayerMask.GetMask("Ground")))
        {
            Vector3 s = stair.transform.position + stair.transform.forward * b.size.z / 2;
            Vector3 e = hit.point;
            if (hit.transform.name!="WoodFloor" && Vector3.SqrMagnitude(s - e) > 0.01f)
            {
                Transform g =Instantiate(stair);
                g.transform.position = e+ new Vector3(0,-b.size.y/4,0)+g.transform.forward*b.size.z/2;
                g.transform.rotation = stair.transform.rotation;
                g.SetParent(stair);
                g.name = stair.name;
                AceptBuid(g.gameObject, "copy");
                g.name = "CopyStair";
            }
        }
    }

    void NewElemet(string s, Transform t)    // заменяем старый элемент на новый
    {
        GameObject blockPredok = SetCurrentBlock("Icon_" + s);
        GameObject newwall;
      //  print(s);
        if (blockPredok != null)
        {
            newwall = Instantiate(blockPredok, t.position, t.rotation) as GameObject;
            newwall.transform.SetParent(Buildings.transform);
            newwall.GetComponent<BuildBlock>().ChechForWall = true;
            StartCoroutine(AfterCheck(newwall));
        }
    }
  
    IEnumerator AfterCheck(GameObject g)
    {
        yield return new WaitForFixedUpdate();
        Destroy(g.GetComponent<BuildBlock>());
        Destroy(g.GetComponent<Rigidbody>());
    }

    public void CancelBuilding()                // отмена строительства
    {
        if (block != null)
        {
           // GOD.PlayerContr.InBuilding = false;
            Destroy(block);
            block = null;
            blockIconInGame = null;
        }
    }

   
    public void MoveBlock()// обработка перемещения от типа объектов
    {
        if (block)
        {
            BuildB.AtPosition = false;
            block.transform.position = CorrectVector(blocktype);
            BuildB.SetCanBuild(BlockParent);
        }
      
    }

    Vector3 CorrectVector(string ob)
    {
        BlockParent = null;
        Vector3 Correct = new Vector3(0, 0, 0);
        Ray ray1 = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit1 = new RaycastHit();
        if (Physics.Raycast(ray1, out hit1, Mathf.Infinity))
        {
            Correct = new Vector3(hit1.point.x, hit1.point.y, hit1.point.z);
            if (hit1.transform.gameObject != block)
            {
                CurrentNear = hit1.transform;
                switch (blocktype)
                {
                    case "Floor":
                        Transform t = hit1.transform;
                        if (t.name.Contains("Wall"))
                        {
                            Transform t2 = t.parent;
                            if (t2)
                                t = t2;
                        }
                        if (t.name.Contains("Door")|| t.name.Contains("Window"))
                        {
                            Transform t2 = t.parent;
                            if (t2)
                                t2 = t2.parent;
                            if (t2)
                                t = t2;
                        }
                        return CorrectFloor(Correct, t, hit1.point);
                    case "Wall":
                        return CorrectOnFloor(Correct, hit1.transform, hit1.point, true);
                    case "Stairs":
                        return CorrectOnFloor(Correct, hit1.transform, hit1.point, false);
                    case "Window":
                    case "Door":
                        return CorrectInWall(Correct, hit1.transform, hit1.point);
                    case "Roof":
                        return CorrectRoof(Correct, hit1.transform, hit1.point);
                    case "Fence":
                        return CorrectFence(Correct, hit1.transform, hit1.point);
                    default:
                        return Correct;
                }
            }
            else
            {
                return block.transform.position;
            }
        }
        return Correct;
    }

#region CorrectFloor
    Vector3 CorrectFloor(Vector3 current, Transform near, Vector3 nearPoint)
    {
        if (near!=null) //если рядом есть объект - корректируем позицию
        {
            if (near != block.transform)
            {
                if (near.tag == "GroundHouse") //если попали в пол
                {
                    block.transform.rotation = near.rotation;
                    int y = (int)near.rotation.eulerAngles.y;
                    if ((y >= 40 && y <= 50) ||
                        (y >= 130 && y <= 140) ||
                        (y >= 220 && y <= 230) ||
                        (y >= 310 && y <= 320))
                        return SetFloorNear45(WhatSideNear(near.gameObject, nearPoint), current, near);
                    else
                        return SetFloorNear90(WhatSideNear(near.gameObject, nearPoint), current, near);
                }
                else //если не в пол проверяем вокруг
                {
                    block.transform.position = current;
                    Transform t = FloorNear(block);
                    if (t != null)
                    {
                        block.transform.rotation = t.rotation;
                        int y = (int)t.rotation.eulerAngles.y;
                        if ((y >= 40 && y <= 50) ||
                            (y >= 130 && y <= 140) ||
                            (y >= 220 && y <= 230) ||
                            (y >= 310 && y <= 320))
                            return SetFloorNear45(WhatSideNear(t.gameObject, nearPoint), current, t);
                        else
                            return SetFloorNear90(WhatSideNear(t.gameObject, nearPoint), current, t);
                    }
                    else
                        return current;
                }
            }
        }
        return current;
    }

    Transform FloorNear(GameObject G)
    {
        Vector3 ObjCenter = G.GetComponent<BoxCollider>().center;
        Vector3 ObjSize = G.GetComponent<BoxCollider>().size;
        Collider[] Colliders = Physics.OverlapBox(ObjCenter + G.transform.position, ObjSize, G.transform.rotation);
        List<Transform> t = new List<Transform>();
        for(int i=0; i<Colliders.Length;i++)
        {
            if (Colliders[i].tag == "GroundHouse" && Colliders[i].transform !=G.transform)
                t.Add(Colliders[i].transform);
        }
        if (t.Count > 0)
        {
            Transform oneT = t[0];
            for (int i = 1; i < t.Count; i++)
            {
                if (Vector3.SqrMagnitude(oneT.position - G.transform.position) > Vector3.SqrMagnitude(t[i].position - G.transform.position))
                    oneT = t[i];
            }
            return oneT;
        }
        return null;
    }
    /*Transform FloorNear(GameObject G)
    {
        Vector3 ObjSize = G.GetComponent<BoxCollider>().size;
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(G.transform.position, G.transform.forward, out hit, ObjSize.x))
        {
            if (hit.transform.tag == "GroundHouse")
                return hit.transform;
        }
        if (Physics.Raycast(G.transform.position, G.transform.right, out hit, ObjSize.x))
        {
            if (hit.transform.tag == "GroundHouse")
                return hit.transform;
        }
        if (Physics.Raycast(G.transform.position, -G.transform.right, out hit, ObjSize.x))
        {
            if (hit.transform.tag == "GroundHouse")
                return hit.transform;
        }
        if (Physics.Raycast(G.transform.position, -G.transform.forward, out hit, ObjSize.x))
        {
            if (hit.transform.tag == "GroundHouse")
                return hit.transform;
        }
        return null;
    }*/
    string WhatSideNear(GameObject G, Vector3 point)   //  к какой стороне ближе попали
    {
        string Correct = "right";
        Vector3 ObjSize = G.GetComponent<BoxCollider>().size;
        Vector3 OldObjectPosition = G.transform.position + new Vector3(ObjSize.x / 2, 0, 0);
        Vector3 NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 2, 0, 0);
        if (Vector3.SqrMagnitude(point - OldObjectPosition) > Vector3.SqrMagnitude(point - NewObjectPosition))
        {
            OldObjectPosition = NewObjectPosition;
            Correct = "left";
        }
        NewObjectPosition = G.transform.position + new Vector3(0, 0, -ObjSize.z / 2);
        if (Vector3.SqrMagnitude(point - OldObjectPosition) > Vector3.SqrMagnitude(point - NewObjectPosition))
        {
            OldObjectPosition = NewObjectPosition;
            Correct = "bottom";
        }
        NewObjectPosition = G.transform.position + new Vector3(0, 0, ObjSize.z / 2);
        if (Vector3.SqrMagnitude(point - OldObjectPosition) > Vector3.SqrMagnitude(point - NewObjectPosition))
        {
            OldObjectPosition = NewObjectPosition;
            Correct = "top";
        }
        return Correct;
    }

    Vector3 SetFloorNear90(string nearname, Vector3 current, Transform near)
    {
        Vector3 Correct;
        Vector3 nearSize = near.GetComponent<BoxCollider>().size;
         switch (nearname)
         {
             case "left":
                 BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-nearSize.x / 2, 0, 0) + new Vector3(-blockSize.x/2,0,0);
                return Correct;
             case "right":
                 BuildB.AtPosition = true;
                Correct = near.position + new Vector3(nearSize.x / 2, 0, 0) + new Vector3(blockSize.x / 2, 0, 0);
                return Correct;
             case "top":
                 BuildB.AtPosition = true;
                Correct = near.position + new Vector3(0, 0, nearSize.z / 2) + new Vector3(0, 0, blockSize.z / 2);
                return Correct;
             case "bottom":
                 BuildB.AtPosition = true;
                Correct = near.position + new Vector3(0, 0, -nearSize.z / 2) + new Vector3(0, 0, -blockSize.z / 2);
                return Correct;
         }
        return current;
    }

    Vector3 SetFloorNear45(string nearname, Vector3 current, Transform near)
    {
        Vector3 Correct;
        Vector3 nearSize = near.GetComponent<BoxCollider>().size;
        switch (nearname)
        {
            case "left":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-nearSize.x / 2, 0, -nearSize.z / 2) + new Vector3(-blockSize.x / 5f, 0, -blockSize.z / 5f);
                return Correct;
            case "right":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(nearSize.x / 2, 0, nearSize.z / 2) + new Vector3(blockSize.x / 5f, 0, blockSize.z / 5f);
                return Correct;
            case "top":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-nearSize.x / 2, 0, nearSize.z / 2) + new Vector3(-blockSize.x / 5f, 0, blockSize.z / 5f);
                return Correct;
            case "bottom":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(nearSize.x / 2, 0, -nearSize.z / 2) + new Vector3(blockSize.x / 5f, 0, -blockSize.z / 5f);
                return Correct;
        }
        return current;
    }
    #endregion
#region CorrectOnFloor
    Vector3 CorrectOnFloor(Vector3 current, Transform near, Vector3 nearPoint, bool IsWall=false)
    {
        if (near!=null) //если рядом есть объект - корректируем позицию
        {
            if (IsWall)
            {
                HighOrLow(near.name);
            }
            BlockParent = near;
            // if ((near != block.transform) && (near.name.Contains("WoodBase")|| (IsWall && near.name.Contains("WoodFloor"))))
            if ((near != block.transform) && (near.name.Contains("WoodBase") || (IsWall && near.name.Contains("WoodFloor"))))
            {
                Vector3 nearSize = near.GetComponent<BoxCollider>().size;
                int y = (int)near.rotation.eulerAngles.y;
                if ((y >= 40 && y <= 50) ||
                   (y >= 130 && y <= 140) ||
                   (y >= 220 && y <= 230) ||
                   (y >= 310 && y <= 320))
                    return SetOnFloorNear45(WhatCornerNear(near.gameObject, nearPoint, false), current, near, IsWall);
                else
                    return SetOnFloorNear90(WhatCornerNear(near.gameObject, nearPoint, true), current, near, IsWall);
            }
        }
        return current;
       
    }

    void HighOrLow(string s)
    {
        BoxCollider b = block.GetComponent<BoxCollider>();
        if (s.Contains("Floor"))
        {
            HighWall.SetActive(true);
            LowWall.SetActive(false);
            b.size = new Vector3(3.5f, 5.4f, 0.54f);
            b.center = new Vector3(0, 2.7f, 0);
            ShowBox(block.transform);
        }
        else
        {
            HighWall.SetActive(false);
            LowWall.SetActive(true);
            b.size = new Vector3(3.5f, 4.2f, 0.54f);
            b.center = new Vector3(0, 2.1f, 0);
            ShowBox(block.transform);
        }
    }
    string WhatCornerNear(GameObject G, Vector3 point, bool Is90=true)   //  к какому углу ближе
    {
        string Correct = "right_top";
        Vector3 ObjSize = G.GetComponent<BoxCollider>().size;
   
        if (Is90)//для оснований с поворотом 90
        {
            Vector3 OldObjectPosition = G.transform.position + new Vector3(ObjSize.x/2, 0, ObjSize.z / 4); //правый веохний угол
            Vector3 NewObjectPosition = G.transform.position + new Vector3(ObjSize.x/2, 0, -ObjSize.z / 4);// правый  нижний угол
            if (Vector3.SqrMagnitude(point - OldObjectPosition) > Vector3.SqrMagnitude(point - NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "right_bot";
            }
            NewObjectPosition = G.transform.position + new Vector3(ObjSize.x / 4, 0, -ObjSize.z/2);//нижний правый угол
            if (Vector3.SqrMagnitude(point - OldObjectPosition) > Vector3.SqrMagnitude(point - NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "bot_right";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 4, 0, -ObjSize.z/2);//нижний левый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "bot_left";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x/2, 0, -ObjSize.z / 4);//левый нижний угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "left_bot";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x/2, 0, ObjSize.z / 4);//левый верхний угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "left_top";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 4, 0, ObjSize.z/2);//верхний левый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "top_left";
            }
            NewObjectPosition = G.transform.position + new Vector3(ObjSize.x / 4, 0, ObjSize.z/2);//верхний правый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "top_right";
            }
            return Correct;
        }
        else//для оснований с поворотом 45
        {
            Vector3 OldObjectPosition = G.transform.position + new Vector3(ObjSize.x / 8, 0, ObjSize.z / 2); ; //правый веохний угол
            Vector3 NewObjectPosition = G.transform.position + new Vector3(ObjSize.x / 2, 0, ObjSize.z / 8);// правый  нижний угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "right_bot";
            }
            NewObjectPosition = G.transform.position + new Vector3(ObjSize.x / 2, 0, -ObjSize.z / 8);//нижний правый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "bot_right";
            }
            NewObjectPosition = G.transform.position + new Vector3(ObjSize.x / 8, 0, -ObjSize.z / 2);//нижний левый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "bot_left";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 8, 0, -ObjSize.z / 2);//левый нижний угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "left_bot";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 2, 0, -ObjSize.z / 8);//левый верхний угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "left_top";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 2, 0, ObjSize.z / 8);//верхний левый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "top_left";
            }
            NewObjectPosition = G.transform.position + new Vector3(-ObjSize.x / 8, 0, ObjSize.z / 2);//верхний правый угол
            if (Vector3.SqrMagnitude(point- OldObjectPosition) > Vector3.SqrMagnitude(point- NewObjectPosition))
            {
                OldObjectPosition = NewObjectPosition;
                Correct = "top_right";
            }
            return Correct;
        }
    }

    Vector3 SetOnFloorNear90(string nearname, Vector3 current, Transform near,bool IsWall=false)
    {
        Vector3 Correct;
        Vector3 nearSize = near.GetComponent<BoxCollider>().size;
        float y = 0;
        float dop = blockSize.z / 2;
        if (IsWall || blocktype=="Fence")
        {
            y = nearSize.y;
            // dop = blockSize.x / 2;
            dop = -blockSize.z / 2;
        }
        switch (nearname)
        {
            case "right_top":
                BuildB.AtPosition = true;
                 Correct = near.position + new Vector3(nearSize.x / 2 + dop, y, blockSize.x / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                return Correct;
            case "right_bot":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(nearSize.x / 2 + dop, y, -blockSize.x / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
                return Correct;
            case "bot_right":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(blockSize.x / 2, y, -(nearSize.z / 2+dop));
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                return Correct;
            case "bot_left":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-blockSize.x / 2, y, -(nearSize.z / 2 + dop));
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                return Correct;
            case "left_bot":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-(nearSize.x / 2 + dop), y, -blockSize.x / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
                return Correct;
            case "left_top":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-(nearSize.x / 2 + dop), y, blockSize.x / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
                return Correct;
            case "top_left":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(-blockSize.x / 2, y, (nearSize.z / 2 + dop));
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                return Correct;
            case "top_right":
                BuildB.AtPosition = true;
                Correct = near.position + new Vector3(blockSize.x / 2, y, (nearSize.z / 2 + dop));
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                return Correct;
        }
        return current;
        }

    Vector3 SetOnFloorNear45(string nearname, Vector3 current, Transform near,  bool IsWall=false)
    {
        Vector3 Correct;
        Vector3 nearSize = near.GetComponent<BoxCollider>().size;
        float y = 0;
        float dop = blockSize.z / 2;
        if (blocktype == "Fence")
            IsWall = true;
        if (IsWall)
        {
            y = nearSize.y;
            dop = blockSize.z / 2;
        }
        switch (nearname)
        {
            case "right_top":
                BuildB.AtPosition = true;
                if(IsWall)
                 Correct = near.position + (new Vector3(nearSize.x /8, y, nearSize.z/2) + new Vector3(blockSize.z / 4, 0, -blockSize.z / 8));
                else
                    Correct = near.position + new Vector3(nearSize.x * 0.25f, y, nearSize.z * 0.75f - blockSize.z / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 45, 0));
                return Correct;
            case "right_bot":
                BuildB.AtPosition = true;
                if(IsWall)
                Correct = near.position + (new Vector3(nearSize.x / 2, y, nearSize.z / 8) + new Vector3(-blockSize.z / 8, 0, blockSize.z / 4));
                else
                    Correct = near.position + new Vector3(nearSize.x * 0.75f - blockSize.z / 2, y, nearSize.z * 0.25f);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 45, 0));
                return Correct;
            case "bot_right":
                BuildB.AtPosition = true;
                if (IsWall)
                    Correct = near.position + (new Vector3(nearSize.x / 2, y, -nearSize.z / 8) +new Vector3(-blockSize.z / 8, 0, -blockSize.z / 4));
                else 
                    Correct = near.position + new Vector3(nearSize.x*0.75f - blockSize.z / 2, y, -nearSize.z*0.25f);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 135, 0));
                return Correct;
            case "bot_left":
                BuildB.AtPosition = true;
                if (IsWall)
                    Correct = near.position + (new Vector3(nearSize.x / 8, y, -nearSize.z / 2) + new Vector3(blockSize.z / 4, 0, blockSize.z / 8));
                else
                    Correct = near.position + new Vector3(nearSize.x * 0.25f, y, -nearSize.z * 0.75f + blockSize.z / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0,135, 0));
                return Correct;
            case "left_bot":
                BuildB.AtPosition = true;
                if (IsWall)
                    Correct = near.position + (new Vector3(-nearSize.x / 8, y, -nearSize.z / 2) + new Vector3(-blockSize.z / 4, 0, blockSize.z / 8));
                else
                    Correct = near.position + new Vector3(-nearSize.x * 0.25f, y, -(nearSize.z * 0.75f - blockSize.z / 2));
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 225, 0));
                return Correct;
            case "left_top":
                BuildB.AtPosition = true;
                if (IsWall)
                    Correct = near.position + (new Vector3(-nearSize.x / 2, y, -nearSize.z / 8) + new Vector3(blockSize.z / 8, 0, -blockSize.z / 4));
                else
                    Correct = near.position + new Vector3(-(nearSize.x * 0.75f - blockSize.z / 2), y, -nearSize.z * 0.25f);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 225, 0));
                return Correct;
            case "top_left":
                BuildB.AtPosition = true;
                if (IsWall)
                    Correct = near.position + (new Vector3(-nearSize.x / 2, y, nearSize.z / 8) + new Vector3(blockSize.z / 8, 0,blockSize.z / 4));
                else
                    Correct = near.position + new Vector3(-(nearSize.x * 0.75f - blockSize.z / 2), y, nearSize.z * 0.25f);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 315, 0));
                return Correct;
            case "top_right":
                BuildB.AtPosition = true;
                if (IsWall)
                    Correct = near.position + (new Vector3(-nearSize.x / 8, y, nearSize.z / 2) + new Vector3(-blockSize.z / 4, 0, -blockSize.z / 8));
                else
                    Correct = near.position + new Vector3(-nearSize.x * 0.25f, y, nearSize.z * 0.75f - blockSize.z / 2);
                block.transform.rotation = Quaternion.Euler(new Vector3(0, 315, 0));
                return Correct;
        }
        return current;
    }
    #endregion
#region CorrectInWall

    Vector3 CorrectInWall(Vector3 current, Transform near, Vector3 nearPoint)
    {
        if (near != null)
        {
            if ((blocktype=="Window" && near.name == "WoodWallWindow")||(blocktype=="Door" && near.name=="WoodWallDoor"))
            {
                BuildB.AtPosition = true;
                BlockParent = near;
                block.transform.SetParent(near);
                block.transform.rotation = near.rotation;
                block.transform.position = near.position;
                if (blocktype == "Window")
                    block.transform.localPosition = new Vector3(0, 0, 0);
                //block.transform.localPosition = new Vector3(0, near.GetComponent<BoxCollider>().size.y/2+blockSize.y/4, 0);
                if (blocktype == "Door")
                    block.transform.localPosition = new Vector3(-blockSize.x/2, 0, 0);
                return block.transform.position;
            }
        }
        return current;

    }
    #endregion
#region CorrectRoof
    Vector3 CorrectRoof(Vector3 current, Transform near, Vector3 nearPoint)
    {
        if (near != null)
        {
            if (blockname.Contains("Top"))
            {
                if (near.name=="WoodRoofTop")
                {
                    Vector3 Correct = near.position;
                    BlockParent = near.parent;
                    int y = (int)near.rotation.eulerAngles.y;
                    if ((y >= 40 && y <= 50) ||
                        (y >= 130 && y <= 140) ||
                        (y >= 220 && y <= 230) ||
                        (y >= 310 && y <= 320))
                        Correct = SetFloorNear45(WhatSideNear(near.gameObject, nearPoint), current, near);
                    else
                        Correct =  SetFloorNear90(WhatSideNear(near.gameObject, nearPoint), current, near);
                    block.transform.rotation = Quaternion.Euler(new Vector3(0, near.rotation.eulerAngles.y, 0));
                    Vector2 nearSize = near.GetComponent<BoxCollider>().size;
                    Ray ray = new Ray(Correct, Correct - new Vector3(0, 1000, 0));
                    RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity);
                    BuildB.AtPosition = false;
                    foreach (RaycastHit hit in hits)
                    {
                        if (hit.collider.tag == "GroundHouse")
                        {
                            BuildB.AtPosition = true;
                           // break;
                        }
                    }
                    return Correct;
                }
                else if(near.name=="WoodRoof")
                {
                    BlockParent = near;
                    BuildB.AtPosition = true;
                    block.transform.rotation = Quaternion.Euler(new Vector3(0, near.rotation.eulerAngles.y, 0));
                    Vector2 nearSize = near.GetComponent<BoxCollider>().size;
                    Vector3 Correct = near.position + new Vector3(0, nearSize.y/2-0.2f,0) +near.forward * (blockSize.x/2+nearSize.x-0.23f);
                    return Correct;
                }
            }
            else
            {
                if (near.name.Contains("Wall"))
                {
                    BlockParent = near;
                    BuildB.AtPosition = true;
                    float y = near.rotation.eulerAngles.y;
                    if (y < 0)
                        y += 360;
                    block.transform.rotation = Quaternion.Euler(new Vector3(0, y - 180, 0));
                    Vector2 nearSize = near.GetComponent<BoxCollider>().size;
                    Vector3 Correct;
                    if (blockname.Contains("Corner"))
                        Correct = near.position + new Vector3(0, 4.2f, 0) - near.forward * (1.56f) ;
                    else
                        Correct = near.position + new Vector3(0, nearSize.y+0.5f, 0) - new Vector3(0, 0.5f, 0);
                    return Correct;
                }
            }
        }
            return current;
        }
    #endregion
#region CorrectFence

    Vector3 CorrectFence(Vector3 current, Transform near, Vector3 point)   // забор на земле
    {
        Vector3 Correct = current;
        float yNear = near.rotation.eulerAngles.y;
        if (yNear < 0)
            yNear += 360;
        float y = block.transform.rotation.eulerAngles.y;
        if (y < 0)
            y += 360;
        if (near.name == "WoodFence")
        {
            if ((int)y == (int)yNear)//если заборы в одинаковом повороте
                Correct = SetNearFence(near, point, true, (int)y, (int)yNear);
            else
                Correct = SetNearFence(near, point, false, (int)y, (int)yNear);
            return Correct;
        }
        else if (near.tag == "GroundHouse")
        {
            return CorrectOnFloor(Correct, near, point, false);
        }
        return current;

    }
    Vector3 SetNearFence(Transform near, Vector3 point, bool OneRotate = true, int y=0, int yNear=0)
    {
        Vector3 Correct;
        string side = "right";
        Vector3 nearSize = near.GetComponent<BoxCollider>().size;
        // Vector3 FencePointLeft = near.position + new Vector3(0, 0, nearSize.z / 2);
        Vector3 FencePointLeft = near.position + near.right *(nearSize.x / 2);
        Vector3 FencePointRight = near.position - near.right* (nearSize.x / 2);
        Vector3 FencePoint = FencePointRight;
        if (Vector3.SqrMagnitude(point -  FencePointRight) > Vector3.SqrMagnitude(point- FencePointLeft))
        {
            side = "left";
            FencePoint = FencePointLeft;
        }
        if (OneRotate)
        {
            BuildB.AtPosition = true;
            if (side=="left")
                Correct = near.position + near.right * (blockSize.x);
            else
                Correct = near.position - near.right * (blockSize.x);
            return Correct;
        }
        else if(Mathf.Abs(y-yNear) == 90)
        {
            BuildB.AtPosition = true;
            Vector3 FenceBlockPointRight = FencePoint+near.forward*(blockSize.z / 2);
            Vector3 FenceBlockPointLeft= FencePoint - near.forward * (blockSize.z / 2);
            Vector3 FenceBlockCorrect = -near.forward*(blockSize.x/2-nearSize.z/2);
            if (Vector3.SqrMagnitude(point- FenceBlockPointLeft) > Vector3.SqrMagnitude(point-FenceBlockPointRight))
                FenceBlockCorrect = near.forward * (blockSize.x / 2 - nearSize.z / 2);
            if (side == "left")
                Correct = near.position + near.right * (blockSize.x/2+blockSize.z/2)+FenceBlockCorrect;
            else
                Correct = near.position - near.right * (blockSize.x/2 + blockSize.z / 2) + FenceBlockCorrect;
            return Correct;
        }
        return block.transform.position;
    }
    #endregion
#region Delete
    public Dictionary<string,int> DeleteBuilding(ObjectScript OS)
    {
        string n = OS.name;
        Dictionary<string, int> Luts = new Dictionary<string, int>();
        if (n == "Firecamp")
            GOD.Audio.AllFirecamps.Remove(OS.GetComponent<FirecampScript>().thisAudio);
        else
        {
            if ((OS.name.Contains("Base") || OS.name.Contains("Floor"))) //фундамент
            {
                DeleteOneBuild(OS.transform, Luts);
                for (int x = 0; x < OS.transform.childCount; x++)       //стены
                {
                    DeleteWall(OS.transform.GetChild(0), Luts);
                }
            }
            else if (OS.name.Contains("Wall"))  //стены
                {
                    DeleteWall(OS.transform, Luts);
                }
            else if (OS.name.Contains("Roof"))  //крыша
                {
                    DeleteRoof(OS.transform, Luts);
                }
            else
                    GOD.PlayerInter.AddLuts(OS, Luts);
        }
        return Luts;
    }

    void DeleteOneBuild(Transform t, Dictionary<string, int> Luts)
    {
        ObjectScript childOS = t.GetComponent<ObjectScript>();
        if (childOS)
            GOD.PlayerInter.AddLuts(childOS, Luts);
    }
    void DeleteWall(Transform t, Dictionary<string, int> Luts)
    {
        DeleteOneBuild(t, Luts);
        if (t.childCount > 0) //окно, крыша, дверь
        {
            for (int i = 0; i < t.childCount; i++)
            {
                if (t.GetChild(i).name != "Options")
                {
                    if (t.GetChild(i).name.Contains("Roof"))
                        DeleteRoof(t.GetChild(i), Luts);
                    else
                        DeleteOneBuild(t.GetChild(i), Luts);
                }
            }
        }
    }
    void DeleteRoof(Transform t, Dictionary<string, int> Luts)
    {
        DeleteOneBuild(t, Luts);
        if (t.childCount > 0) //плоская крыша
        {
           // for (int i = 0; i < t.childCount; i++)
           // {
                TopRoofCheck(t.GetChild(0));
                DeleteOneBuild(t.GetChild(0), Luts);
           // }
        }
    }
    public void TopRoofCheck(Transform t)
    {
        float dis = t.GetComponent<BoxCollider>().size.x;
        dis += dis / 2;
        Transform newParent= Check(t.position, t.forward,dis);
        if (newParent)
        {
            for(int i=0; i<CheckList.Count;i++)
            {
                if (CheckList[i].parent == t.parent)
                    CheckList[i].SetParent(newParent);
            }
        }
        CheckList.Clear();
        newParent = Check(t.position, t.right,dis);
        if (newParent)
        {
            for (int i = 0; i < CheckList.Count; i++)
            {
                if (CheckList[i].parent == t.parent)
                    CheckList[i].SetParent(newParent);
            }
        }
        CheckList.Clear();
        newParent = Check(t.position, -t.right,dis);
        if (newParent)
        {
            for (int i = 0; i < CheckList.Count; i++)
            {
                if (CheckList[i].parent == t.parent)
                    CheckList[i].SetParent(newParent);
            }
        }
    }

    List<Transform> CheckList = new List<Transform>();
    Transform Check(Vector3 pos, Vector3 dur, float dis)
    {
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(pos, dur, out hit, dis))
        {
            if (hit.transform.name == "WoodRoof")
                return hit.transform;
            if (hit.transform.name.Contains("RoofTop"))
            {
                if(!CheckList.Contains(hit.transform))
                    CheckList.Add(hit.transform);
                Transform t;
                if (-dur!= hit.transform.forward)
                {
                    t = Check(hit.transform.position, hit.transform.forward,dis);
                    if (t)
                        return t;
                }
                if (-dur != -hit.transform.forward)
                {
                    t=Check(hit.transform.position, -hit.transform.forward,dis);
                    if (t)
                        return t;
                }
                if (-dur != hit.transform.right)
                {
                    t=Check(hit.transform.position, hit.transform.right,dis);
                    if (t)
                        return t;
                }
                if (-dur != -hit.transform.right)
                {
                    t=Check(hit.transform.position, -hit.transform.right,dis);
                    if (t)
                        return t;
                }
            }
            return null;
        }
        return null;
    }
    #endregion
# region WalkIn
    public void ChnagePlayerInOut()
    {
        BuildingsChanged = !BuildingsChanged;
        if (BuildingsChanged)
            PlayerIn();
        else
            PlayerOut();
    }
    public void PlayerIn() //игрок зашел в здание
    {
            BuildingsChanged = true;
            for (int i = 0; i < ChangedElemets.Count; i++)
            {
                if (ChangedElemets[i].name.Contains("Wall"))
                {
                    if (GOD.PlayerContr.InBuilding)
                    {
                        OnOffCollider(ChangedElemets[i], false);
                    }
                    Transform t = ChangedElemets[i].transform.Find("Options");
                    if (t)
                    {
                        if (t.GetChild(0).name == "Wall")
                            t.GetChild(0).gameObject.SetActive(false);
                        else
                            t.GetChild(1).gameObject.SetActive(false);
                        if (t.GetChild(1).name != "Changed")
                            t.GetChild(2).gameObject.SetActive(true);
                        else
                            t.GetChild(1).gameObject.SetActive(true);
                    }
                }
                else
                    ChangedElemets[i].gameObject.SetActive(false);

            }
    }
    public void PlayerOut() //игрок вышел из здания
    {
        BuildingsChanged = false;
        for (int i = 0; i < ChangedElemets.Count; i++)
        {
            if (ChangedElemets[i].name.Contains("Wall"))
            {
                OnOffCollider(ChangedElemets[i], true);
                Transform t = ChangedElemets[i].transform.Find("Options");
                if (t)
                {
                    t.GetChild(0).gameObject.SetActive(true);
                    t.GetChild(1).gameObject.SetActive(false);
                }
            }
            else
                ChangedElemets[i].gameObject.SetActive(true);
        }
    }
    public void OnOffCollider(Transform t, bool bo)
    {
        BoxCollider[] BCs = t.GetComponents<BoxCollider>();
        foreach (BoxCollider b in BCs)
        {
            b.enabled = bo;
        }
    }
    #endregion

}
