﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour {

    ShopWindow MyShop;
    public Sprite ItemImage;
    public string ItemName = "";
    public string ItemDescription = "";
    public int ItemCost = 10;
    public int ItemCount = 1;
    GameObject ItemsBack=null;
    Transform ItemsParent = null;
    GameObject OneItem;
    public ResourceChest ItemResChest;
    public InstrumentChest ItemInstrChest;
    public BuildingChest ItemBuildChest;
    public EssenceChest ItemEssenceChest;
    public int DonatNumber = -1;                 //для причесок, красок и скинов, указывает под каким номером находится этот предмет

    public Image Hair;


    public void InfoOn()
    {
        ItemsBack.SetActive(true);
    }

    public void InfoOff()
    {
        ItemsBack.SetActive(false);
    }

    public void Init(ShopWindow sh)
    {
        MyShop = sh;
        sh.AllShopItems.Add(this);
        if (name.Contains("Hair"))               // картинка в магазине
        {
            //transform.FindChild("Item1").GetComponent<Image>().sprite = ItemImage;
            transform.Find("ItemBack").GetChild(0).gameObject.SetActive(false);
            if (transform.Find("ItemHead2"))
            Hair = transform.Find("ItemHead2").GetComponent<Image>();
        }
        else if (name.Contains("Color"))
        {
            if(ItemImage)
                transform.Find("Item").GetComponent<Image>().sprite = ItemImage;
        }
        else if (name.Contains("Skin"))
        {

        }
        else
        {
            ItemImage = MyShop.GOD.InventoryContr.FindIcon(name);
            transform.Find("ItemBack").GetChild(0).GetComponent<Image>().sprite = ItemImage;                                 
        }

        if (transform.Find("ItemsParent"))
            ItemsParent = transform.Find("ItemsParent");
        if(transform.Find("OneItem"))
            OneItem = transform.Find("OneItem").gameObject;

        switch (name)
        {
            case "ResourceChest":
                ItemCost = 34;
                gameObject.AddComponent<ResourceChest>();
                ItemResChest = GetComponent<ResourceChest>();
                ItemResChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;
                MyShop.ShowRes(ItemResChest.UsualResource, ItemResChest.UsualResourceCount, ItemsParent, OneItem);
                MyShop.ShowRes(ItemResChest.RareResource, ItemResChest.RareResourceCount, ItemsParent, OneItem);
                break;
            case "BigResourceChest":
                ItemCost = 52;
                gameObject.AddComponent<ResourceChest>();
                ItemResChest = GetComponent<ResourceChest>();
                ItemResChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;

                MyShop.ShowRes(ItemResChest.UsualResource, ItemResChest.UsualResourceCount,ItemsParent, OneItem);
                MyShop.ShowRes(ItemResChest.RareResource, ItemResChest.RareResourceCount,ItemsParent, OneItem);
                break;
            case "InstrumentChest":
                ItemCost = 52;
                gameObject.AddComponent<InstrumentChest>();
                ItemInstrChest = GetComponent<InstrumentChest>();
                ItemInstrChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;

                MyShop.ShowRes(ItemInstrChest.Instruments, ItemInstrChest.InstrumentsCount,ItemsParent, OneItem);
                break;
            case "BigInstrumentChest":
                ItemCost = 52;
                gameObject.AddComponent<InstrumentChest>();
                ItemInstrChest = GetComponent<InstrumentChest>();
                ItemInstrChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;

                MyShop.ShowRes(ItemInstrChest.Instruments, ItemInstrChest.InstrumentsCount,ItemsParent, OneItem);
                break;
            case "BuildingChest":
                ItemCost = 130;

                gameObject.AddComponent<BuildingChest>();
                ItemBuildChest = GetComponent<BuildingChest>();
                ItemBuildChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;

                MyShop.ShowRes(ItemBuildChest.Buildings, ItemBuildChest.BuildingsCount,ItemsParent, OneItem);
                break;
            case "BigBuildingChest":
                ItemCost = 258;

                gameObject.AddComponent<BuildingChest>();
                ItemBuildChest = GetComponent<BuildingChest>();
                ItemBuildChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;

                MyShop.ShowRes(ItemBuildChest.Buildings,ItemBuildChest.BuildingsCount, ItemsParent, OneItem);
                break;
            case "Smithy":                                                               //ЗДАНИЯ
                ItemCost = 118;
                break;
            case "Windmill":
                ItemCost = 118;
                break;
            case "Stove":
                ItemCost = 110;
                break;
            case "Mannequie":
                ItemCost = 78;
                break;
            case "Firecamp":
                ItemCost = 10;
                break;
            case "Chest":
                ItemCost = 50;
                break;
            case "IceChest":
                ItemCost = 70;
                break;
            case "WoodBase":
                ItemCost = 40;
                ItemCount = 10;

                GameObject X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodStair":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodFence":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodWall":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodWallDoor":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodWallWindow":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "Window":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "Door":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodRoof":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "WoodRoofTop":
                ItemCost = 40;
                ItemCount = 10;

                X = transform.Find("X").gameObject;
                X.SetActive(true);
                X.GetComponent<Text>().text = "x" + ItemCount;
                break;
            case "EssenceChest":                                                                             //МАГИЯ
                ItemCost = 196;

                gameObject.AddComponent<EssenceChest>();
                ItemEssenceChest = GetComponent<EssenceChest>();
                ItemEssenceChest.Init();

                transform.Find("Info").gameObject.SetActive(true);
                ItemsBack = transform.Find("ItemsBack").gameObject;
                ItemsParent = ItemsBack.transform.Find("ItemsParent");
                OneItem = transform.Find("OneItem").gameObject;
                MyShop.ShowRes(ItemEssenceChest.Essence,ItemEssenceChest.EssenceCount, ItemsParent, OneItem);
                break;
            case "EssenceSun":                                                                            
                ItemCost = 10;
                transform.Find("ItemsBack").gameObject.SetActive(false);
                break;
            case "EssenceWater":                                                                            
                transform.Find("ItemsBack").gameObject.SetActive(false);
                ItemCost = 10;
                break;
            case "EssenceIce":                                                                             
                transform.Find("ItemsBack").gameObject.SetActive(false);
                ItemCost = 10;
                break;
            case "BoyHair0":  //ПРИЧЕСКИ
                DonatNumber = 0;
                ItemCost = 70;
                break;
            case "BoyHair1":
                DonatNumber = 1;
                ItemCost = 70;
                break;
            case "BoyHair2":
                DonatNumber = 2;
                ItemCost = 70;
                break;
            case "BoyHair3":
                DonatNumber = 3;
                ItemCost = 70;
                break;
            case "BoyHair4":
                DonatNumber = 4;
                ItemCost = 70;
                break;
            case "BoyHair5":
                DonatNumber = 5;
                ItemCost = 70;
                break;
            case "BoyHair6":
                DonatNumber = 6;
                ItemCost = 70;
                break;
            case "BoyHair7":
                DonatNumber = 7;
                ItemCost = 70;
                break;
            case "BoyHair8":
                DonatNumber = 8;
                ItemCost = 70;
                break;
            case "BoyHair9":
                DonatNumber = 9;
                ItemCost = 70;
                break;
            case "GirlHair0":
                DonatNumber = 0;
                ItemCost = 70;
                break;
            case "GirlHair1":
                DonatNumber = 1;
                ItemCost = 70;
                break;
            case "GirlHair2":
                DonatNumber = 2;
                ItemCost = 70;
                break;
            case "GirlHair3":
                DonatNumber = 3;
                ItemCost = 70;
                break;
            case "GirlHair4":
                DonatNumber = 4;
                ItemCost = 70;
                break;
            case "GirlHair5":
                DonatNumber = 5;
                ItemCost = 70;
                break;
            case "GirlHair6":
                DonatNumber = 6;
                ItemCost = 70;
                break;
            case "GirlHair7":
                DonatNumber = 7;
                ItemCost = 70;
                break;
            case "GirlHair8":
                DonatNumber = 8;
                ItemCost = 70;

                break;
            case "GirlHair9":
                DonatNumber = 9;
                ItemCost = 70;
                break;
            case "Color0":                                                                            //КРАСИТЕЛИ
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[0];   
                ItemCost = 40;
                DonatNumber = 0;
                break;
            case "Color1":                                                                        
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[1]; 
                ItemCost = 40;
                DonatNumber = 1;
                break;
            case "Color2":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[2];  
                ItemCost = 40;
                DonatNumber = 2;
                break;
            case "Color3":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[3]; 
                ItemCost = 40;
                DonatNumber = 3;
                break;
            case "Color4":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[4]; 
                ItemCost = 40;
                DonatNumber = 4;
                break;
            case "Color5":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[5];  
                ItemCost = 40;
                DonatNumber = 5;
                break;
            case "Color6":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[6];  
                ItemCost = 40;
                DonatNumber = 6;
                break;
            case "Color7":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[7];  
                ItemCost = 40;
                DonatNumber = 7;
                break;
            case "Color8":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[8];
                ItemCost = 40;
                DonatNumber = 8;
                break;
            case "Color9":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[9];
                ItemCost = 40;
                DonatNumber = 9;
                break;
            case "Color10":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[10];
                ItemCost = 40;
                DonatNumber = 10;
                break;
            case "Color11":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[11];
                ItemCost = 40;
                DonatNumber = 11;
                break;
            case "Color12":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[12];
                ItemCost = 40;
                DonatNumber = 12;
                break;
            case "Color13":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[13];
                ItemCost = 40;
                DonatNumber = 13;
                break;
            case "Color14":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[14];
                ItemCost = 40;
                DonatNumber = 14;
                break;
            case "Color15":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[15];
                ItemCost = 40;
                DonatNumber = 15;
                break;
            case "Color16":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[16];
                ItemCost = 40;
                DonatNumber = 16;
                break;
            case "Color17":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[17];
                ItemCost = 40;
                DonatNumber = 17;
                break;
            case "Color18":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[18];
                ItemCost = 40;
                DonatNumber = 18;
                break;
            case "Color19":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[19];
                ItemCost = 40;
                DonatNumber = 19;
                break;
            case "Color20":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[20];
                ItemCost = 40;
                DonatNumber = 20;
                break;
            case "Color21":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[21];
                ItemCost = 40;
                DonatNumber = 21;
                break;
            case "Color22":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[22];
                ItemCost = 40;
                DonatNumber = 22;
                break;
            case "Color23":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[23];
                ItemCost = 40;
                DonatNumber = 23;
                break;
            case "Color24":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[24];
                ItemCost = 40;
                DonatNumber = 24;
                break;
            case "Color25":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[25];
                ItemCost = 40;
                DonatNumber = 25;
                break;
            case "Color26":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[26];
                ItemCost = 40;
                DonatNumber = 26;
                break;
            case "Color27":
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[27];
                ItemCost = 40;
                DonatNumber = 27;
                break;
            case "BoySkin0":
                ItemCost = 0;
                DonatNumber = 0;
                break;
            case "BoySkin1":
                ItemCost = 120;
                DonatNumber = 1;
                break;
            case "BoySkin2":
                ItemCost = 120;
                DonatNumber = 2;
                break;
            case "BoySkin3":
                ItemCost = 120;
                DonatNumber = 3;
                break;
            case "BoySkin4":
                ItemCost = 120;
                DonatNumber = 4;
                break;
            case "BoySkin5":
                ItemCost = 120;
                DonatNumber = 5;
                break;
            case "BoySkin6":
                ItemCost = 120;
                DonatNumber = 6;
                break;
            case "BoySkin7":
                ItemCost = 120;
                DonatNumber = 7;
                break;
            case "GirlSkin0":
                ItemCost = 0;
                DonatNumber = 0;
                break;
            case "GirlSkin1":
                ItemCost = 120;
                DonatNumber = 1;
                break;
            case "GirlSkin2":
                ItemCost = 120;
                DonatNumber = 2;
                break;
            case "GirlSkin3":
                ItemCost = 120;
                DonatNumber = 3;
                break;
            case "GirlSkin4":
                ItemCost = 120;
                DonatNumber = 4;
                break;
            case "GirlSkin5":
                ItemCost = 120;
                DonatNumber = 5;
                break;
            case "GirlSkin6":
                ItemCost = 120;
                DonatNumber = 6;
                break;
            case "GirlSkin7":
                ItemCost = 120;
                DonatNumber = 7;
                break;
        }

        InitLanguage();
       // transform.FindChild("Buy").GetChild(0).GetComponent<Text>().text = "" + ItemCost;
        if (DonatNumber >= 0)
        {
            bool[] NewBool;
            if (name.Contains("GirlHair"))
            {
                NewBool = MyShop.GOD.PlayerContr.GirlHairs;
                if (Hair)
                    Hair.color = MyShop.GOD.PlayerContr.HairColors[MyShop.GOD.PlayerContr.CurrentGirlHairColorNumber];
            }
            else if (name.Contains("BoyHair"))
            {
                NewBool = MyShop.GOD.PlayerContr.BoyHairs;
                if (Hair)
                    Hair.color = MyShop.GOD.PlayerContr.HairColors[MyShop.GOD.PlayerContr.CurrentBoyHairColorNumber];
            }
            else if (name.Contains("GirlSkin"))
                NewBool = MyShop.GOD.PlayerContr.GirlSkins;
            else if (name.Contains("BoySkin"))
                NewBool = MyShop.GOD.PlayerContr.BoySkins;
            else
                NewBool = MyShop.GOD.PlayerContr.Colors;
           /* if (NewBool[DonatNumber])
                {
                    transform.FindChild("Buy").GetChild(0).GetComponent<Text>().text = Localization.instance.getTextByKey("#Use");
                }*/
        }

    }

    public void InitLanguage()
    {
        switch (name)
        {
            case "ResourceChest":
                ItemName = Localization.instance.getTextByKey("#NameResourceChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionResourceChest");
                break;
            case "BigResourceChest":
                ItemName = Localization.instance.getTextByKey("#NameBigResourceChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBigResourceChest");
                break;
            case "InstrumentChest":
                ItemName = Localization.instance.getTextByKey("#NameInstrumentChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionInstrumentChest");
                break;
            case "BigInstrumentChest":
                ItemName = Localization.instance.getTextByKey("#NameBigInstrumentChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBigInstrumentChest");
                break;
            case "BuildingChest":
                ItemName = Localization.instance.getTextByKey("#NameBuildingChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBuildingChest");
                break;
            case "BigBuildingChest":
                ItemName = Localization.instance.getTextByKey("#NameBigBuildingChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBigBuildingChest");
                break;
            case "Smithy":                                                               //ЗДАНИЯ
                ItemName = Localization.instance.getTextByKey("#NameSmithy");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionSmithy");
                break;
            case "Windmill":
                ItemName = Localization.instance.getTextByKey("#NameWindmill");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWindmill");
                break;
            case "Stove":
                ItemName = Localization.instance.getTextByKey("#NameStove");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionStove");
                break;
            case "Mannequie":
                ItemName = Localization.instance.getTextByKey("#NameMannequie");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionMannequie");
                break;
            case "Firecamp":
                ItemName = Localization.instance.getTextByKey("#NameFirecamp");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionFirecamp");
                break;
            case "Chest":
                ItemName = Localization.instance.getTextByKey("#NameChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionChest");
                break;
            case "IceChest":
                ItemName = Localization.instance.getTextByKey("#NameIceChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionIceChest");
                break;
            case "WoodBase":
                ItemName = Localization.instance.getTextByKey("#NameWoodBase");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodBase");
                break;
            case "WoodStair":
                ItemName = Localization.instance.getTextByKey("#NameWoodStair");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodStair");
                break;
            case "WoodFence":
                ItemName = Localization.instance.getTextByKey("#NameWoodFence");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodFence");
                break;
            case "WoodWall":
                ItemName = Localization.instance.getTextByKey("#NameWoodWall");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodWall");
                break;
            case "WoodWallDoor":
                ItemName = Localization.instance.getTextByKey("#NameWoodWallDoor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodWallDoor");
                break;
            case "WoodWallWindow":
                ItemName = Localization.instance.getTextByKey("#NameWoodWallWindow");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodWallWindow");
                break;
            case "Window":
                ItemName = Localization.instance.getTextByKey("#NameWindow");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWindow");
                break;
            case "Door":
                ItemName = Localization.instance.getTextByKey("#NameDoor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionDoor");
                break;
            case "WoodRoof":
                ItemName = Localization.instance.getTextByKey("#NameWoodRoof");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodRoof");
                break;
            case "WoodRoofTop":
                ItemName = Localization.instance.getTextByKey("#NameWoodRoofTop");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionWoodRoofTop");
                break;
            case "EssenceChest":                                                                             //МАГИЯ
                ItemName = Localization.instance.getTextByKey("#NameEssenceChest");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionEssenceChest");
                break;
            case "EssenceSun":
                ItemName = Localization.instance.getTextByKey("#NameEssenceSun");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionEssenceSun");
                break;
            case "EssenceWater":
                ItemName = Localization.instance.getTextByKey("#NameEssenceWater");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionEssenceWater");
                break;
            case "EssenceIce":
                ItemName = Localization.instance.getTextByKey("#NameEssenceIce");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionEssenceIce");
                break;
            case "BoyHair0":  //ПРИЧЕСКИ
                DonatNumber = 0;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair1":
                DonatNumber = 1;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair2":
                DonatNumber = 2;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair3":
                DonatNumber = 3;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair4":
                DonatNumber = 4;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair5":
                DonatNumber = 5;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair6":
                DonatNumber = 6;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair7":
                DonatNumber = 7;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair8":
                DonatNumber = 8;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "BoyHair9":
                DonatNumber = 9;
                ItemName = Localization.instance.getTextByKey("#NameBoyHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                ItemCost = 35;
                break;
            case "GirlHair0":
                DonatNumber = 0;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair1":
                DonatNumber = 1;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair2":
                DonatNumber = 2;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair3":
                DonatNumber = 3;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair4":
                DonatNumber = 4;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair5":
                DonatNumber = 5;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair6":
                DonatNumber = 6;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair7":
                DonatNumber = 7;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair8":
                DonatNumber = 8;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "GirlHair9":
                DonatNumber = 9;
                ItemName = Localization.instance.getTextByKey("#NameGirlHair") + " " + (DonatNumber + 1);
                ItemDescription = Localization.instance.getTextByKey("#DescriptionHair");
                break;
            case "Color0":                                                                            //КРАСИТЕЛИ
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color1":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color2":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                transform.Find("Item").GetComponent<Image>().color = MyShop.GOD.PlayerContr.HairColors[2];
                ItemCost = 20;
                DonatNumber = 2;
                break;
            case "Color3":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color4":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color5":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color6":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color7":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color8":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color9":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color10":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color11":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color12":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color13":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color14":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color15":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color16":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color17":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color18":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color19":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color20":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color21":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color22":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color23":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color24":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color25":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color26":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "Color27":
                ItemName = Localization.instance.getTextByKey("#NameColor");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionColor");
                break;
            case "BoySkin0":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin0");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin0");
                break;
            case "BoySkin1":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin1");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin1");
                break;
            case "BoySkin2":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin2");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin2");
                break;
            case "BoySkin3":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin3");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin3");
                break;
            case "BoySkin4":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin4");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin4");
                break;
            case "BoySkin5":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin5");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin5");
                break;
            case "BoySkin6":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin6");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin6");
                break;
            case "BoySkin7":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin7");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionBoySkin7");
                break;
            case "GirlSkin0":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin0");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin0");
                break;
            case "GirlSkin1":
                ItemName = Localization.instance.getTextByKey("#NameBoySkin1");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin1");
                break;
            case "GirlSkin2":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin2");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin2");
                break;
            case "GirlSkin3":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin3");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin3");
                break;
            case "GirlSkin4":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin4");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin4");
                break;
            case "GirlSkin5":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin5");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin5");
                break;
            case "GirlSkin6":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin6");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin6");
                break;
            case "GirlSkin7":
                ItemName = Localization.instance.getTextByKey("#NameGirlSkin7");
                ItemDescription = Localization.instance.getTextByKey("#DescriptionGirlSkin7");
                break;
        }

        transform.Find("Name").GetComponent<Text>().text = ItemName;
        transform.Find("Description").GetComponent<Text>().text = ItemDescription;
    }


    
}
