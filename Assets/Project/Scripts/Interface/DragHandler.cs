﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerClickHandler {

    PlayerInterface PlayerInter;
    Transform Content;
    public static GameObject itemBeingDragged;
    Vector3 StartPosition;
    public static Transform startParent;
    public bool IsUsialItem = true;
    public bool IsDrag = false;


    void Start()
    {
        PlayerInter = GameObject.Find("GameManager").GetComponent<PlayerInterface>();
        Content = GameObject.Find("Canvas").transform.GetChild(0);
    }


  
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (itemBeingDragged == null)
        {
            IsDrag = true;
            itemBeingDragged = gameObject;
            StartPosition = transform.position;
            startParent = transform.parent;
            transform.SetParent(Content);
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        itemBeingDragged.transform.position = Input.mousePosition;
    }

    public  void OnEndDrag(PointerEventData eventData)
    {
        EndDrag();
    }

    public void EndDrag()
    {
        IsDrag = false;
        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        //if (transform.parent != startParent)
        {
            transform.position = StartPosition;
        }
        if (transform.parent == Content)
            transform.SetParent(startParent);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.Log(gameObject.GetComponent<RecipeScript>());
        // if ((gameObject.GetComponent<RecipeScript>()==null))
        //PlayerInter.ShowInfo(this.gameObject);
    }

    public void OnPointerClick(PointerEventData eventData)
    {

           if(IsUsialItem)
        {
            if (PlayerInter.Inventory.activeSelf)
            {
                if (!PlayerInter.InfoWindow.activeSelf)
                    PlayerInter.OpenVkladky2();
                PlayerInter.ShowInfo(this.gameObject);
            }
        }
    }
}
