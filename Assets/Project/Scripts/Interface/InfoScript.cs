﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoScript : MonoBehaviour {

    [Header("Параметры иконки:")]
    public string thisName;                   // имя
    public int ResourceCount;              // количество ресурсов в стаке
    public int MaxResourceCount = 1;       //максимальное количество ресурсов в стаке
    public string thisObjName;                   // имя в локализации
                                                 //  public string type;                         // resources, clothes, weapon, instrument, rod, building, chest, mantle, LeatherBelt...
    public enum ItemType
    {
        resources,
        clothes,
        weapon,
        instrument,
        rod,
        building, // то что строится через режим строительства
        construction, //то что строится сразу
        chest,
        food,
        poition,
        untyped
    }
    public ItemType type;
    public int durability, currentDurability;                // прочность, если есть
    public int currentValidity=0;                // годность, если есть
    public int attack;                    // сила аттаки, если есть
    public int armor;                     // броня, если есть
    public int instrumentSpeed;        // скорость добычи, если есть
    public int instrumentDamage;
    public int FoodRes = 0;            // сколько голода еда восстанавливает
    public int HPRes = 0;            // сколько хп еда восстанавливает
    public string effects;                // дополнительные эффеты
    string description;            // описание
    public string whereDescription;       //описание, где можно добыть
    public string OnPlayerSlot = "";       // слот в который можно надеть предмет

    [Header("Вода в ведре:")]
    public bool IsWaterInBacket = false;      // есть ли вода в ведре
    public Sprite[] Water = new Sprite[2];   // спрайт ведра с водой и без воды
    public int WaterCount = -1;

    [Header("Положение иконки:")]
    public bool NowOnPlayer = false;
    public Transform ParentSlot = null;        // место где хранится иконка
    public Slot SlotInGame = null;   // есть ли ссылка на эту иконку в панели быстрого доступ
    public IconInGame InGame;

    public ChestScript InChest; //лежит ли в сундуке
    public int ChestPlace = -1; // номер места в котором лежит

    public AfterDeadBag InDeadBag = null; //лежит ли в сумке после смерти

    [Header("Картинки на иконке:")]
    public Image DurabilityImage;
    public Image ValidityImage;
    public Text TextCount;
    public Image FonText;

    [Header("Всопомогательные:")]
    public bool HaveRecipe = false;
    public bool IsFromShop = false; //предмет из магазина

    public void Init()
    {
       // Localization.instance.SetLanguage("RU");
       // SetParamenre();
    }

    public void Delete(GameManager GOD)//удалем везде этот объект
    {
        if (GOD)
        {
            if (ParentSlot != null && ParentSlot.GetComponent<Slot>().SlotsOnPlayer)//снимаем с игрока
                GOD.PlayerContr.RemovePlayerParametre(gameObject, false);
            if (GOD.InventoryContr.ObjectsWithValidity.Contains(this))//удаляем из списка годности
                GOD.InventoryContr.ObjectsWithValidity.Remove(this);
            if (InChest) //удаляем из сундука, если был там
                InChest.MyIcons.Remove(this);
            if (SlotInGame) //убираем из панели быстрого доступа
                InGame.OutIcon();
            if (InDeadBag) //удаляем из сумки после смерти
            {
                InDeadBag.MyIcons.Remove(this);
                if (InDeadBag.MyIcons.Count == 0)
                {
                    GOD.InventoryContr.AllBags.Remove(InDeadBag.gameObject);
                    Destroy(InDeadBag.gameObject);
                }
            }
        }

    }

    public void OnorOutIcon()
    {
        if (SlotInGame)
        {
            if (NowOnPlayer)
                SlotInGame.thisImage.color = new Color(0.37f, 0.96f, 0.52f, 1);
            else
                SlotInGame.thisImage.color = new Color(1f, 1f, 1f, 1);
        }
    }
    public void SetTextCount()
    {
        TextCount.text = "" + ResourceCount;
    }
    public void SetWaterInBacket(int count)
    {
        WaterCount = count;
        if(count>0)
        {
            IsWaterInBacket = true;
            GetComponent<Image>().sprite = Water[1];
        }
        else
        {
            IsWaterInBacket = false;
            GetComponent<Image>().sprite = Water[0];
        }
        if(SlotInGame!=null)
        {
            InGame.InGameImage.sprite = GetComponent<Image>().sprite;
        }
      
    }
    public void SetParameter(string s)
    {
        thisName = "";
        if (s.Contains("Icon"))
        {
            for (int i = 5; i < s.Length; i++)
                thisName = thisName + s[i];
        }
        else
            thisName = s;
        ResourceCount = 0;
        MaxResourceCount = 1;
        thisObjName = "";
        type = ItemType.untyped;
        currentValidity = 0;
        currentDurability = 0;
        durability = 0;
        attack = 0;
        armor = 0;
        instrumentSpeed = 0;
        instrumentDamage = 0;
        effects = "";
        description = "";
        whereDescription = "";
        OnPlayerSlot = "";
        IsWaterInBacket = false;
        ParentSlot = null;
        SlotInGame = null;
        InGame = null;
        ChestPlace = -1;
        HaveRecipe = false;
        IsFromShop = false;
        if (DurabilityImage)
        {
            DurabilityImage.gameObject.SetActive(false);
            ValidityImage.gameObject.SetActive(false);
            TextCount.gameObject.SetActive(false);
            FonText.gameObject.SetActive(false);
        }

        SetItems();
        currentDurability = durability;
        SetSlot(thisName);

        if (TextCount)
        {
            switch (type)
            {
                case ItemType.resources:
                case ItemType.building:
                case ItemType.construction:
                case ItemType.chest:
                case ItemType.poition:
                    TextCount.gameObject.SetActive(true);
                    FonText.gameObject.SetActive(true);
                    break;
                case ItemType.clothes:
                    DurabilityImage.gameObject.SetActive(true);
                    break;
                case ItemType.instrument:
                    if (thisObjName != "Bucket")
                        DurabilityImage.gameObject.SetActive(true);
                    break;
                case ItemType.rod:
                case ItemType.weapon:
                    DurabilityImage.gameObject.SetActive(true);
                    break;
                case ItemType.food:
                    ValidityImage.gameObject.SetActive(true);
                    FonText.gameObject.SetActive(true);
                    TextCount.gameObject.SetActive(true);
                    break;
            }
        }

    }


    public void SetItems()
    {
        switch (thisName)
        {
            // РЕСУРСЫ_____________________________________________________________________________________________________________________________________________________________
            case "Leaf":     // листья                             
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameLeaf");
                description = Localization.instance.getTextByKey("#DescriptionLeaf");
                whereDescription = Localization.instance.getTextByKey("#FromAll");
                MaxResourceCount = 10;
                break;
            case "FlowerRed":     // цветы                             
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameFlowerRed");
                description = Localization.instance.getTextByKey("#DescriptionFlowerRed");
                whereDescription = Localization.instance.getTextByKey("#FromIzumrud");
                MaxResourceCount = 10;
                break;
            case "FlowerOrange":     // цветы                             
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameFlowerOrange");
                description = Localization.instance.getTextByKey("#DescriptionFlowerOrange");
                whereDescription = Localization.instance.getTextByKey("#FromIzumrud");
                MaxResourceCount = 10;
                break;
            case "FlowerBlue":     // цветы                             
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameFlowerBlue");
                description = Localization.instance.getTextByKey("#DescriptionFlowerBlue");
                whereDescription = Localization.instance.getTextByKey("#FromWater");
                MaxResourceCount = 10;
                break;
            case "Wood":
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameWood");
                description = Localization.instance.getTextByKey("#DescriptionWood");
                whereDescription = Localization.instance.getTextByKey("#FromAll");
                MaxResourceCount = 10;
                break;
            case "Stick":  // палка                               
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameStick");
                description = Localization.instance.getTextByKey("#DescriptionStick");
                whereDescription = Localization.instance.getTextByKey("#FromAll");
                MaxResourceCount = 10;
                break;
            case "Needle":  // игокли                              
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameNeedle");
                description = Localization.instance.getTextByKey("#DescriptionNeedle");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;
            case "Stone":
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameStone");
                description = Localization.instance.getTextByKey("#DescriptionStone");
                whereDescription = Localization.instance.getTextByKey("#FromAll");
                MaxResourceCount = 10;
                break;
            case "Flint":          // кремень
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameFlint");
                description = Localization.instance.getTextByKey("#DescriptionFlint");
                whereDescription = Localization.instance.getTextByKey("#FromAll");
                MaxResourceCount = 10;
                break;
            case "IronOre":          // железо
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameIronOre");
                description = Localization.instance.getTextByKey("#DescriptionIronOre");
                whereDescription = Localization.instance.getTextByKey("#FromIce");
                MaxResourceCount = 10;
                break;
            case "Ice":          // лед
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameIce");
                description = Localization.instance.getTextByKey("#DescriptionIce");
                whereDescription = Localization.instance.getTextByKey("#FromIce");
                MaxResourceCount = 10;
                break;
            case "Crystall":   //кристалл
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameCrystall");
                description = Localization.instance.getTextByKey("#DescriptionCrystall");
                whereDescription = Localization.instance.getTextByKey("#FromIce");
                MaxResourceCount = 10;
                break;
            case "Emerald":   //изумруд
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameEmerald");
                description = Localization.instance.getTextByKey("#DescriptionEmerald");
                whereDescription = Localization.instance.getTextByKey("#FromIzumrud");
                MaxResourceCount = 10;
                break;
            case "Sapphire":  //сапфир
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameSapphire");
                description = Localization.instance.getTextByKey("#DescriptionSapphire");
                whereDescription = Localization.instance.getTextByKey("#FromIce");
                MaxResourceCount = 10;
                break;
            case "Amethyst":  //аметист
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameAmethyst");
                description = Localization.instance.getTextByKey("#DescriptionAmethyst");
                whereDescription = Localization.instance.getTextByKey("#FromWater");
                MaxResourceCount = 10;
                break;
            case "Ruby":  //рубин
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameRuby");
                description = Localization.instance.getTextByKey("#DescriptionRuby");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;
            case "Dimond":  //алмаз
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameDimond");
                description = Localization.instance.getTextByKey("#DescriptionDimond");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;

            case "Sand":          // песок
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameSand");
                description = Localization.instance.getTextByKey("#DescriptionSand");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;
            case "Snow":          // снег
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameSnow");
                description = Localization.instance.getTextByKey("#DescriptionSnow");
                whereDescription = Localization.instance.getTextByKey("#FromIce");
                MaxResourceCount = 10;
                break;
            case "Bone":          // кость
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBone");
                description = Localization.instance.getTextByKey("#DescriptionBone");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;


            case "CrabShell":   // панцирь краба
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameCrabShell");
                description = Localization.instance.getTextByKey("#DescriptionCrabShell");
                whereDescription = Localization.instance.getTextByKey("#FromCrab");
                MaxResourceCount = 10;
                break;
            case "Skin":   // необработанная кожа
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameSkin");
                description = Localization.instance.getTextByKey("#DescriptionSkin");
                whereDescription = Localization.instance.getTextByKey("#FromSkin");
                MaxResourceCount = 10;
                break;
            case "Web":   // паутина
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameWeb");
                description = Localization.instance.getTextByKey("#DescriptionWeb");
                whereDescription = Localization.instance.getTextByKey("#FromSpider");
                MaxResourceCount = 10;
                break;
            case "Wool":   // шерсть
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameWool");
                description = Localization.instance.getTextByKey("#DescriptionWool");
                whereDescription = Localization.instance.getTextByKey("#FromBear");
                MaxResourceCount = 10;
                break;

            case "Rope":   // веревка
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameRope");
                description = Localization.instance.getTextByKey("#DescriptionRope");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Plank":   // доска
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NamePlank");
                description = Localization.instance.getTextByKey("#DescriptionPlank");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "SharpStone":   // острый камень
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameSharpStone");
                description = Localization.instance.getTextByKey("#DescriptionSharpStone");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "StoneBlock":   // каменный блок
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameStoneBlock");
                description = Localization.instance.getTextByKey("#DescriptionStoneBlock");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "IronIngot":   // железный слиток
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameIronIngot");
                description = Localization.instance.getTextByKey("#DescriptionIronIngot");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "DimondPlate":   // алмазная пластина
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameDimondPlate");
                description = Localization.instance.getTextByKey("#DescriptionDimondPlate");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "BonePlate":   // костяная пластина
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBonePlate");
                description = Localization.instance.getTextByKey("#DescriptionBonePlate");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "ChitinPlate":   // хитиновая пластина
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameChitinPlate");
                description = Localization.instance.getTextByKey("#DescriptionChitinPlate");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Leather":   // обр кожа
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameLeather");
                description = Localization.instance.getTextByKey("#DescriptionLeather");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Thread":   // нитки
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameThread");
                description = Localization.instance.getTextByKey("#DescriptionThread");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Cord":   // шнур
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameCord");
                description = Localization.instance.getTextByKey("#DescriptionCord");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "DimondCord":   // шнур
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameDimondCord");
                description = Localization.instance.getTextByKey("#DescriptionDimondCord");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Glass":   // стекло
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameGlass");
                description = Localization.instance.getTextByKey("#DescriptionGlass");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "BerrieSouce":   // ягодный соус
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBerrieSouce");
                description = Localization.instance.getTextByKey("#DescriptionBerrieSouce");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Flour":   // мука
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameFlour");
                description = Localization.instance.getTextByKey("#DescriptionFlour");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;

            // СУНДУКИ_____________________________________________________________________________________________________________________________________________________________
            case "FishChestGreen":
                type = ItemType.chest;
                thisObjName = Localization.instance.getTextByKey("#NameFishChestGreen");
                description = Localization.instance.getTextByKey("#DescriptionFishChestGreen");
                MaxResourceCount = 10;
                break;
            case "FishChestBlue":
                type = ItemType.chest;
                thisObjName = Localization.instance.getTextByKey("#NameFishChestBlue");
                description = Localization.instance.getTextByKey("#DescriptionFishChestBlue");
                MaxResourceCount = 10;
                break;
            case "FishChestPurpule":
                type = ItemType.chest;
                thisObjName = Localization.instance.getTextByKey("#NameFishChestPurpule");
                description = Localization.instance.getTextByKey("#DescriptionFishChestPurpule");
                MaxResourceCount = 10;
                break;
            case "FishChestYellow":
                type = ItemType.chest;
                thisObjName = Localization.instance.getTextByKey("#NameFishChestYellow");
                description = Localization.instance.getTextByKey("#DescriptionFishChestYellow");
                MaxResourceCount = 10;
                break;
            case "FishChestRed":
                type = ItemType.chest;
                thisObjName = Localization.instance.getTextByKey("#NameFishChestRed");
                description = Localization.instance.getTextByKey("#DescriptionFishChestRed");
                MaxResourceCount = 10;
                break;
            // ОДЕЖДА_____________________________________________________________________________________________________________________________________________________________
            case "LeatherCoat":                              // куртки
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameLeatherCoat");
                durability = 200;
                armor = 8;
                description = Localization.instance.getTextByKey("#DescriptionLeatherCoat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronCoat":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameIronCoat");
                durability = 400;
                armor = 10;
                description = Localization.instance.getTextByKey("#DescriptionIronCoat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BoneCoat":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameBoneCoat");
                durability = 600;
                armor = 14;
                description = Localization.instance.getTextByKey("#DescriptionBoneCoat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondCoat":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameDimondCoat");
                durability = 800;
                armor = 18;
                description = Localization.instance.getTextByKey("#DescriptionDimondCoat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "LeatherPants":                              // штаны
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameLeatherPants");
                durability = 200;
                armor = 6;
                description = Localization.instance.getTextByKey("#DescriptionLeatherPants");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronPants":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameIronPants");
                durability = 400;
                armor = 8;
                description = Localization.instance.getTextByKey("#DescriptionIronPants");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BonePants":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameBonePants");
                durability = 600;
                armor = 10;
                description = Localization.instance.getTextByKey("#DescriptionBonePants");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondPants":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameDimondPants");
                durability = 800;
                armor = 12;
                description = Localization.instance.getTextByKey("#DescriptionDimondPants");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "LeatherHat":                              // шапки
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameLeatherHat");
                durability = 200;
                armor = 3;
                description = Localization.instance.getTextByKey("#DescriptionLeatherHat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronHat":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameIronHat");
                durability = 400;
                armor = 6;
                description = Localization.instance.getTextByKey("#DescriptionIronHat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BoneHat":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameBoneHat");
                durability = 600;
                armor = 8;
                description = Localization.instance.getTextByKey("#DescriptionBoneHat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondHat":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameDimondHat");
                durability = 800;
                armor = 10;
                description = Localization.instance.getTextByKey("#DescriptionDimondHat");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "LeatherShoes":                              // обувь
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameLeatherShoes");
                durability = 200;
                armor = 3;
                description = Localization.instance.getTextByKey("#DescriptionLeatherShoes");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronShoes":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameIronShoes");
                durability = 400;
                armor = 6;
                description = Localization.instance.getTextByKey("#DescriptionIronShoes");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BoneShoes":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameBoneShoes");
                durability = 600;
                armor = 8;
                description = Localization.instance.getTextByKey("#DescriptionBoneShoes");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondShoes":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameDimondShoes");
                durability = 800;
                armor = 10;
                description = Localization.instance.getTextByKey("#DescriptionDimondShoes");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "ChitinMantle":                                                              // плащи
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameChitinMantle");
                description = Localization.instance.getTextByKey("#DescriptionChitinMantle");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "WoolMantle":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameWoolMantle");
                description = Localization.instance.getTextByKey("#DescriptionWoolMantle");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "WebMantle":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameWebMantle");
                description = Localization.instance.getTextByKey("#DescriptionWebMantle");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "LeatherBelt":                                                               // пояса
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameLeatherBelt");
                description = Localization.instance.getTextByKey("#DescriptionLeatherBelt");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "EmeraldBelt":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameEmeraldBelt");
                description = Localization.instance.getTextByKey("#DescriptionEmeraldBelt");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "SapphireBelt":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameSapphireBelt");
                description = Localization.instance.getTextByKey("#DescriptionSapphireBelt");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "AmethystBelt":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameAmethystBelt");
                description = Localization.instance.getTextByKey("#DescriptionAmethystBelt");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "RubyBelt":
                type = ItemType.clothes;
                thisObjName = Localization.instance.getTextByKey("#NameRubyBelt");
                description = Localization.instance.getTextByKey("#DescriptionRubyBelt");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            // ИНСТРУМЕНТЫ_____________________________________________________________________________________________________________________________________________________________
            case "WoodAxe":                              // топоры
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameWoodAxe");
                durability = 200;
                instrumentDamage = 9;
                instrumentSpeed = 13;//8 ударов
                description = Localization.instance.getTextByKey("#DescriptionWoodAxe");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "StoneAxe":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameStoneAxe");
                durability = 300;
                instrumentDamage = 11;
                instrumentSpeed = 15;//7 ударов
                description = Localization.instance.getTextByKey("#DescriptionStoneAxe");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronAxe":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameIronAxe");
                durability = 522;
                instrumentDamage = 13;
                instrumentSpeed = 18;//6 ударов
                description = Localization.instance.getTextByKey("#DescriptionIronAxe");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BoneAxe":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameBoneAxe");
                durability = 748;
                instrumentDamage = 15;
                instrumentSpeed = 20;//5 ударов
                description = Localization.instance.getTextByKey("#DescriptionBoneAxe");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondAxe":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameDimondAxe");
                durability = 936;
                instrumentDamage = 18;
                instrumentSpeed = 25;  // 4 удара
                description = Localization.instance.getTextByKey("#DescriptionDimondAxe");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "WoodPick":
                type = ItemType.instrument;                                                     // кирки
                thisObjName = Localization.instance.getTextByKey("#NameWoodPick");
                durability = 200;
                instrumentDamage = 9;
                instrumentSpeed = 13; // 8ударов
                description = Localization.instance.getTextByKey("#DescriptionWoodPick");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "StonePick":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameStonePick");
                durability = 300;
                instrumentDamage = 11;
                instrumentSpeed = 15;// 7 ударов
                description = Localization.instance.getTextByKey("#DescriptionStonePick");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronPick":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameIronPick");
                durability = 522;
                instrumentDamage = 13;
                instrumentSpeed = 18;// 6 ударов
                description = Localization.instance.getTextByKey("#DescriptionIronPick");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BonePick":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameBonePick");
                durability = 748;
                instrumentDamage = 15;
                instrumentSpeed = 20; // 5 ударов
                description = Localization.instance.getTextByKey("#DescriptionBonePick");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondPick":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameDimondPick");
                durability = 936;
                instrumentDamage = 18;
                instrumentSpeed = 25;//4 удара
                description = Localization.instance.getTextByKey("#DescriptionDimondPick");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "WoodShovel":                              // лопаты
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameWoodShovel");
                durability = 200;
                instrumentDamage = 9;
                instrumentSpeed = 13;// 8 удара
                description = Localization.instance.getTextByKey("#DescriptionWoodShovel");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "StoneShovel":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameStoneShovel");
                durability = 300;
                instrumentDamage = 11;
                instrumentSpeed = 15;// 7 ударов
                description = Localization.instance.getTextByKey("#DescriptionIronShovel");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronShovel":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameIronShovel");
                durability = 522;
                instrumentDamage = 13;
                instrumentSpeed = 18;//6 ударов
                description = Localization.instance.getTextByKey("#DescriptionIronShovel");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BoneShovel":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameBoneShovel");
                durability = 748;
                instrumentDamage = 15;
                instrumentSpeed = 20; //5 ударов
                description = Localization.instance.getTextByKey("#DescriptionBoneShovel");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondShovel":
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameDimondShovel");
                durability = 936;
                instrumentDamage = 18;
                instrumentSpeed = 25;//4 удара
                description = Localization.instance.getTextByKey("#DescriptionDimondShovel");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "FragileRod":                              // удочки
                type = ItemType.rod;
                thisObjName = Localization.instance.getTextByKey("#NameFragileRod");
                durability = 10;
                description = Localization.instance.getTextByKey("#DescriptionFragileRod");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "NormalRod":
                type = ItemType.rod;
                thisObjName = Localization.instance.getTextByKey("#NameNormalRod");
                durability = 20;
                description = Localization.instance.getTextByKey("#DescriptionNormalRod");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "StrongRod":
                type = ItemType.rod;
                thisObjName = Localization.instance.getTextByKey("#NameStrongRod");
                durability = 30;
                description = Localization.instance.getTextByKey("#DescriptionStrongRod");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DurableRod":
                type = ItemType.rod;
                thisObjName = Localization.instance.getTextByKey("#NameDurableRod");
                durability = 40;
                description = Localization.instance.getTextByKey("#DescriptionDurableRod");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondRod":
                type = ItemType.rod;
                thisObjName = Localization.instance.getTextByKey("#NameDimondRod");
                durability = 50;
                description = Localization.instance.getTextByKey("#DescriptionDimondRod");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            case "Bucket":                              // ведро
                type = ItemType.instrument;
                thisObjName = Localization.instance.getTextByKey("#NameBucket");
                description = Localization.instance.getTextByKey("#DescriptionBucket");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            // ОРУЖИЕ_____________________________________________________________________________________________________________________________________________________________

            case "WoodSword":                              // мечи
                type = ItemType.weapon;
                thisObjName = Localization.instance.getTextByKey("#NameWoodSword");
                durability = 100;
                attack = 12;
                description = Localization.instance.getTextByKey("#DescriptionWoodSword");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "StoneSword":
                type = ItemType.weapon;
                thisObjName = Localization.instance.getTextByKey("#NameStoneSword");
                durability = 200;
                attack = 16;
                description = Localization.instance.getTextByKey("#DescriptionStoneSword");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "IronSword":
                type = ItemType.weapon;
                thisObjName = Localization.instance.getTextByKey("#NameIronSword");
                durability = 300;
                attack = 22;
                description = Localization.instance.getTextByKey("#DescriptionIronSword");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "BoneSword":
                type = ItemType.weapon;
                thisObjName = Localization.instance.getTextByKey("#NameBoneSword");
                durability = 400;
                attack = 26;
                description = Localization.instance.getTextByKey("#DescriptionBoneSword");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;
            case "DimondSword":
                type = ItemType.weapon;
                thisObjName = Localization.instance.getTextByKey("#NameDimondSword");
                durability = 500;
                attack = 30;
                description = Localization.instance.getTextByKey("#DescriptionDimondSword");
                MaxResourceCount = 1;
                HaveRecipe = true;
                break;

            // СТРОИТЕЛЬСТВО_____________________________________________________________________________________________________________________________________________________________

            case "Smithy":                              // кузница
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameSmithy");
                description = Localization.instance.getTextByKey("#DescriptionSmithy");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Windmill":                              // мельница
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameWindmill");
                description = Localization.instance.getTextByKey("#DescriptionWindmill");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Stove":                              // печь
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameStove");
                description = Localization.instance.getTextByKey("#DescriptionStove");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Mannequie":                              // маннекен
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameMannequie");
                description = Localization.instance.getTextByKey("#DescriptionMannequie");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Firecamp":                              // костер
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameFirecamp");
                description = Localization.instance.getTextByKey("#DescriptionFirecamp");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Chest":                              // сундук
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameChest");
                description = Localization.instance.getTextByKey("#DescriptionChest");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "IceChest":                              // ледяной сундук
                type = ItemType.construction;
                thisObjName = Localization.instance.getTextByKey("#NameIceChest");
                description = Localization.instance.getTextByKey("#DescriptionIceChest");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodFloor":                              // деревянный пол
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodBase");
                description = Localization.instance.getTextByKey("#DescriptionWoodBase");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodBase":                              // деревянный фундамент
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodBase");
                description = Localization.instance.getTextByKey("#DescriptionWoodBase");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodStair":                              // деревянная лестница
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodStair");
                description = Localization.instance.getTextByKey("#DescriptionWoodStair");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodFence":                              // деревянный забор
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodFence");
                description = Localization.instance.getTextByKey("#DescriptionWoodFence");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodWall":                              // деревянная стена
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodWall");
                description = Localization.instance.getTextByKey("#DescriptionWoodWall");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodWallDoor":                              // деревянная стена с дверью
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodWallDoor");
                description = Localization.instance.getTextByKey("#DescriptionWoodWallDoor");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodWallWindow":                              // деревянная стена с окном
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodWallWindow");
                description = Localization.instance.getTextByKey("#DescriptionWoodWallWindow");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Window":                              //окно
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWindow");
                description = Localization.instance.getTextByKey("#DescriptionWindow");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Door":                              // дверь
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameDoor");
                description = Localization.instance.getTextByKey("#DescriptionDoor");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodRoof":                              //деревянная крыша
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodRoof");
                description = Localization.instance.getTextByKey("#DescriptionWoodRoof");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodRoofTop":                              //деревянная крыша верхушка
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodRoofTop");
                description = Localization.instance.getTextByKey("#DescriptionWoodRoofTop");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "WoodRoofCorner":                              //деревянная крыша угол
                type = ItemType.building;
                thisObjName = Localization.instance.getTextByKey("#NameWoodRoofCorner");
                description = Localization.instance.getTextByKey("#DescriptionWoodRoofCorner");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;

            //ЕДА________________________________________________________________________________________________________
            case "Berries":                              //ягоды
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameBerry");
                description = Localization.instance.getTextByKey("#DescriptionBerry");
                whereDescription = Localization.instance.getTextByKey("#FromIzumrud");
                MaxResourceCount = 10;
                break;
            case "Apple":                              //яблоки
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameApple");
                description = Localization.instance.getTextByKey("#DescriptionApple");
                whereDescription = Localization.instance.getTextByKey("#FromIzumrud");
                MaxResourceCount = 10;
                break;
            case "Grapes":                              //виноград
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameGrape");
                description = Localization.instance.getTextByKey("#DescriptionGrape");
                whereDescription = Localization.instance.getTextByKey("#FromWater");
                MaxResourceCount = 10;
                break;
            case "Pear":                              //груша
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NamePear");
                description = Localization.instance.getTextByKey("#DescriptionPear");
                whereDescription = Localization.instance.getTextByKey("#FromIce");
                MaxResourceCount = 10;
                break;
            case "Condiment":                              //специи
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameCondiment");
                description = Localization.instance.getTextByKey("#DescriptionCondiment");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;
            case "CocoaBeans":                              //какао-бобы
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameCocoaBeans");
                description = Localization.instance.getTextByKey("#DescriptionCocoaBeans");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;
            case "Honey":                              //мед
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameHoney");
                description = Localization.instance.getTextByKey("#DescriptionHoney");
                whereDescription = Localization.instance.getTextByKey("#FromBee");
                MaxResourceCount = 10;
                break;
            case "Seed":                              //семечко
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameSeed");
                description = Localization.instance.getTextByKey("#DescriptionSeed");
                whereDescription = Localization.instance.getTextByKey("#FromIzumrud");
                MaxResourceCount = 10;
                break;
            case "Meat":                              //мясо
                type = ItemType.food;
                currentValidity =100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameMeat");
                description = Localization.instance.getTextByKey("#DescriptionMeat");
                whereDescription = Localization.instance.getTextByKey("#FromMeat");
                MaxResourceCount = 10;
                break;
            case "CactusPiece":                              //мякоть кактуса
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameCactusPiece");
                description = Localization.instance.getTextByKey("#DescriptionCactusPiece");
                whereDescription = Localization.instance.getTextByKey("#FromSand");
                MaxResourceCount = 10;
                break;
            case "Alga":                              //водоросли
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 10;
                thisObjName = Localization.instance.getTextByKey("#NameAlga");
                description = Localization.instance.getTextByKey("#DescriptionAlga");
                whereDescription = Localization.instance.getTextByKey("#FromRod");
                MaxResourceCount = 10;
                break;
            case "Sprat":                              //шпрота
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameSprat");
                description = Localization.instance.getTextByKey("#DescriptionSprat");
                whereDescription = Localization.instance.getTextByKey("#FromRod");
                MaxResourceCount = 10;
                break;
            case "Carp":                              //карп
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameCarp");
                description = Localization.instance.getTextByKey("#DescriptionCarp");
                whereDescription = Localization.instance.getTextByKey("#FromRod");
                MaxResourceCount = 10;
                break;
            case "Som":                              //сом
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameSom");
                description = Localization.instance.getTextByKey("#DescriptionSom");
                whereDescription = Localization.instance.getTextByKey("#FromRod");
                MaxResourceCount = 10;
                break;
            case "Trout":                              //форель
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameTrout");
                description = Localization.instance.getTextByKey("#DescriptionTrout");
                whereDescription = Localization.instance.getTextByKey("#FromRod");
                MaxResourceCount = 10;
                break;
            case "Salmon":                              //лосось
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 15;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameSalmon");
                description = Localization.instance.getTextByKey("#DescriptionSalmon");
                whereDescription = Localization.instance.getTextByKey("#FromRod");
                MaxResourceCount = 10;
                break;
            case "Chocolate":                              //шоколад
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameChocolate");
                description = Localization.instance.getTextByKey("#DescriptionChocolate");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Candy":                              //конфеты
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameCandy");
                description = Localization.instance.getTextByKey("#DescriptionCandy");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "HoneyDesert":                              //медовый десерт
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameHoneyDessert");
                description = Localization.instance.getTextByKey("#DescriptionHoneyDessert");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "FruitSalad":                              //фруктовый салат
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameFruitSalad");
                description = Localization.instance.getTextByKey("#DescriptionFruitSalad");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "GrapesInChocolate":                              //вишня в шоколаде
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameGrapesInChocolate");
                description = Localization.instance.getTextByKey("#DescriptionGrapesInChocolate");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "FiredFish":                              //жаренная рыба
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameFiredFish");
                description = Localization.instance.getTextByKey("#DescriptionFiredFish");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Cutlet":                              //котлета
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameCutlet");
                description = Localization.instance.getTextByKey("#DescriptionCutlet");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "AlgaeSalad":                              //салат из водорослей
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameAlgaeSalad");
                description = Localization.instance.getTextByKey("#DescriptionAlgaeSalad");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "CactusPure":                              //кактусовое пюре
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameCactusPure");
                description = Localization.instance.getTextByKey("#DescriptionCactusPure");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "FiredMeat":                              //жаренное мясо
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 20;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameFiredMeat");
                description = Localization.instance.getTextByKey("#DescriptionFiredMeat");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "FishSoup":                              //рыбный суп
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 30;
                thisObjName = Localization.instance.getTextByKey("#NameFishSoup");
                description = Localization.instance.getTextByKey("#DescriptionFishSoup");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Soup":                              //суп
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 30;
                thisObjName = Localization.instance.getTextByKey("#NameSoup");
                description = Localization.instance.getTextByKey("#DescriptionSoup");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "FishPie":                              //рыбный пирог
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 30;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameFishPie");
                description = Localization.instance.getTextByKey("#DescriptionFishPie");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "BerriePie":                              //ягодный пирог
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 20;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameBerriePie");
                description = Localization.instance.getTextByKey("#DescriptionBerriePie");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "MeatPies":                              //мясной пирожок
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 10;
                HPRes = 15;
                thisObjName = Localization.instance.getTextByKey("#NameMeatPies");
                description = Localization.instance.getTextByKey("#DescriptionMeatPies");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Vine":                              //вино
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 5;
                HPRes = 5;
                thisObjName = Localization.instance.getTextByKey("#NameVine");
                description = Localization.instance.getTextByKey("#DescriptionVine");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "Roll":                              //ролл
                type = ItemType.food;
                currentValidity = 100;
                FoodRes = 100;
                HPRes = 100;
                thisObjName = Localization.instance.getTextByKey("#NameRoll");
                description = Localization.instance.getTextByKey("#DescriptionRoll");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;

            // МАГИЯ______________________________________________________________________________________________________
            case "EssenceIce":                              //Сущность холода
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameEssenceIce");
                description = Localization.instance.getTextByKey("#DescriptionEssenceIce");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "EssenceSun":                              //Сущность жары
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameEssenceSun");
                description = Localization.instance.getTextByKey("#DescriptionEssenceSun");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "EssenceWater":                              //Сущность дождя
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameEssenceWater");
                description = Localization.instance.getTextByKey("#DescriptionEssenceWater");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            // АЛХИМИЯ______________________________________________________________________________________________________
            case "HealingSalve":                              //целебная мазь
                type = ItemType.poition;
                thisObjName = Localization.instance.getTextByKey("#NameHealingSalve");
                description = Localization.instance.getTextByKey("#DescriptionHealingSalve");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            case "StrongHealingSalve":                              //целебная мазь
                type = ItemType.poition;
                thisObjName = Localization.instance.getTextByKey("#NameStrongHealingSalve");
                description = Localization.instance.getTextByKey("#DescriptionStrongHealingSalve");
                MaxResourceCount = 10;
                HaveRecipe = true;
                break;
            // ДОНАТ______________________________________________________________________________________________________
            case "ResourceChest":                              //сундук с ресурсами
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameResourceChest");
                description = Localization.instance.getTextByKey("#DescriptionResourceChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
            case "BigResourceChest":                              //большой сундук с ресурсами
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBigResourceChest");
                description = Localization.instance.getTextByKey("#DescriptionBigResourceChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
            case "InstrumentChest":                              //сундук с инструментами
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameInstrumentChest");
                description = Localization.instance.getTextByKey("#DescriptionInstrumentChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
            case "BigInstrumentChest":                              //большой сундук с инструментами
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBigInstrumentChest");
                description = Localization.instance.getTextByKey("#DescriptionBigInstrumentChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
            case "BuildingChest":                              //сундук со зданиями
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBuildingChest");
                description = Localization.instance.getTextByKey("#DescriptionBuildingChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
            case "BigBuildingChest":                              //сундук со зданиями
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameBigBuildingChest");
                description = Localization.instance.getTextByKey("#DescriptionBigBuildingChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
            case "EssenceChest":                              //сундук с эссенциями
                type = ItemType.resources;
                thisObjName = Localization.instance.getTextByKey("#NameEssenceChest");
                description = Localization.instance.getTextByKey("#DescriptionEssenceChest");
                MaxResourceCount = 10;
                HaveRecipe = false;
                IsFromShop = true;
                break;
        }
    }

        void SetSlot(string name)
    {
        if ((name.Contains("Axe")) || (name.Contains("Pick")) || (name.Contains("Shovel")) || (name.Contains("Bucket")) || (name.Contains("Rod")) || (name.Contains("Sword")))
        {
            OnPlayerSlot = "RightArm";
        }
        else if (name.Contains("Coat"))         // куртки
        {
            OnPlayerSlot = "Body";
        }
        else if (name.Contains("Pants"))      // штаны
        {
            OnPlayerSlot = "Legs";
        }
        else if (name.Contains("Shoes"))      // обувь
        {
            OnPlayerSlot = "Feet";
        }
        else if (name.Contains("Hat"))     // шлемы
        {
            OnPlayerSlot = "Head";
        }
        else if (name.Contains("Belt"))    // пояса
        {
            OnPlayerSlot = "Belt";
        }
        else if (name.Contains("Mantle"))     // плащи
        {
            OnPlayerSlot = "Mantle";
        }
    }

    public string fullDescription
    {
        get
        {
            string s = description;
            if (type == ItemType.food)
                s = description + ". "+ Localization.instance.getTextByKey("#Food1")+" " + HPRes + " " + Localization.instance.getTextByKey("#Food2")+ " " +FoodRes+" " + Localization.instance.getTextByKey("#Food3");
            return s;
        }
        set
        {
        }
    }
}
