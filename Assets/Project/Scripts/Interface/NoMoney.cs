﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NoMoney : MonoBehaviour {

    public GameManager GOD;
    public GameObject NoMoneyWindow;
    public Text MainText, GemText;
    CloseWindow NoMoneyClose;

    public void Init(GameManager t)
    {
        GOD = t;
        NoMoneyClose = gameObject.GetComponent<CloseWindow>();
        InitLanguage();
    }
    public void InitLanguage()
    {
        MainText.text = Localization.instance.getTextByKey("#NoMoney");
    }
	public void OpenNoMoney()
    {
        NoMoneyWindow.transform.localScale = Constants.NoMoneyWindow;
        GemText.text = "" + Balance.Gem;
        NoMoneyWindow.SetActive(true);
    }

    public void CloseNoMoney()
    {
        NoMoneyClose.StartClose();
       // NoMoneyWindow.SetActive(false);
    }

    public void Buy()
    {
        GOD.ShopScript.OpenGemWindow();
        CloseNoMoney();
    }
}
