﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour {

    public GameManager GOD;
    public GameObject  ChestInventory, ChestChest;
    public RectTransform Bag;
    public GameObject[] SlotInBag;
    public GameObject[] SlotInChest; //слоты в сундуке
    public List<ChestScript> AllChest = new List<ChestScript>();  // все сундуки
    public ChestScript CurrentChest = null; // текущий открытый сундук

    public List<GameObject> AllBags = new List<GameObject>();  //весь выпавший из игрока лут
    public GameObject CurrentBag = null;

  //  public Transform[] Icons = new Transform[1]; // все иконки
    public Dictionary<string, Sprite> Icons = new Dictionary<string, Sprite>(); // все иконки
    public Transform IconsParent;    // родитель всех иконок

    // Сумка после смерти
    [Header("Сумка после смерти:")]
    public Transform AfterDead;
    public Transform AfterDeadSlot;
    public RectTransform AfterDeadContent;
    public Transform AfterDeadBag;
    public Transform AfterDeadSlotBag;
    public RectTransform AfterDeadContentBag;
    public int SlotInDeadLine = 0;

    // Сундук
    [Header("Сундук:")]
    public Transform ChestInventorySlot;
    public Transform ChestInventoryBag;
    public int SlotInChestLine = 0;
    public RectTransform ChestInventoryContentBag;

    public List<InfoScript> ObjectsWithValidity = new List<InfoScript>();  // список продуктов, которые могут портится

    public GameObject Icon;
    public Transform AllIconsParent;
    public List<InfoScript> AllIcons = new List<InfoScript>();
    public List<InfoScript> DopIcons = new List<InfoScript>(); // иконки из магазина которые не влезли

    public Transform InventorySlot;   // ячейка в сумке
    public Transform InventoryDopSlot;   // ячейка в сумке для шопа
    public int SlotStartCount = 20;
    public int SlotCount = 0;        //количество ячеек в сумке
    int SlotInLine = 1;               //подсчет сколько в строке слотов
    public Transform NextSlot;       //кнопка добавить слот

    public void Init()
    {
        for (int i = 0; i < IconsParent.childCount; i++)          // заполняем массив иконок
        {
            var child = IconsParent.GetChild(i);
            Icons.Add(child.name, child.GetComponent<Image>().sprite);
        }

        SlotInChest = new GameObject[ChestChest.transform.childCount];
        for (int i = 0; i < ChestChest.transform.childCount; i++)
            SlotInChest[i] = ChestChest.transform.GetChild(i).gameObject;
    }

    public void SecondInit()   //после загрузки сейвов
    {
        SlotInBag = new GameObject[1];
        SlotInBag[0] = InventorySlot.gameObject;
        int Limit = SlotCount;
        for (int i = 1; i < Limit; i++)
        {
            AddSlot("first");
        }

        ShowIcons(SlotInBag);
    }

    public GameObject AddSlot(string s)   //добавляем новый слот
    {
        Transform t = Instantiate(InventorySlot);
        t.name = InventorySlot.name;
        t.SetParent(Bag.transform);
        t.GetComponent<Slot>().Init();
        t.localScale = InventorySlot.localScale;
        if (t.childCount > 0)
            Destroy(t.GetChild(0).gameObject);
        SlotInLine++;
        if (SlotInLine == 4)
        {
            SlotInLine = 0;
            Bag.sizeDelta = Bag.sizeDelta + new Vector2(0, 100);
        }

        GameObject[] dop = SlotInBag;                                 //добавляем новый слот в массив
        SlotInBag = new GameObject[SlotInBag.Length + 1];
        for (int i = 0; i < SlotInBag.Length - 1; i++)
            SlotInBag[i] = dop[i];
        SlotInBag[SlotInBag.Length - 1] = t.gameObject;

      //  if (s == "second")
            //RemoveFromShopSlots();

        for (int i = 0; i < DopIcons.Count; i++)
        {
            DopIcons[i].transform.parent.SetParent(null);
            DopIcons[i].transform.parent.SetParent(Bag.transform);
        }

        NextSlot.SetParent(null);
        NextSlot.SetParent(Bag.transform);

        if (SlotCount >= 100)
            NextSlot.gameObject.SetActive(false);
        return t.gameObject;
    }

    public GameObject AddShopSlot()
    {
        Transform t = Instantiate(InventoryDopSlot);
        t.name = InventoryDopSlot.name;
        t.gameObject.SetActive(true);
        t.SetParent(Bag.transform);
        t.localScale = InventoryDopSlot.localScale;
        SlotInLine++;
        if (SlotInLine == 4)
        {
            SlotInLine = 0;
            Bag.sizeDelta = Bag.sizeDelta + new Vector2(0, 75);
        }
        NextSlot.SetParent(null);
        NextSlot.SetParent(Bag.transform);
        return t.gameObject;
    }

    public void ShowIconsInChest(ChestScript Ch)                 // отрисовываем иконки в сундуке
    {
        CurrentChest = Ch;

        GameObject[] ChestSlots = SlotInChest;
        for (int i = 0; i < ChestSlots.Length; i++)   // сначала опустошаем
        {
            if (ChestSlots[i].transform.childCount > 0)
                ChestSlots[i].transform.GetChild(0).transform.SetParent(GOD.InventoryContr.IconsParent.transform);
        }
        for (int i = 0; i < ChestInventoryBag.childCount; i++)
        {
            Transform p = ChestInventoryBag.transform.GetChild(i).transform;
            if (p.childCount > 0)
                p.GetChild(0).transform.SetParent(GOD.InventoryContr.IconsParent.transform);
        }

        for (int i = ChestInventoryBag.childCount; i < SlotCount; i++)                               //создаем слотов столько, сколько мест в инвентаре
        {
            Transform t = Instantiate(ChestInventorySlot);
            t.name = ChestInventorySlot.name;
            t.gameObject.SetActive(true);
            t.GetComponent<Slot>().Init();
            t.SetParent(ChestInventoryBag);
            t.localScale = new Vector3(1, 1, 1);
            SlotInChestLine++;
            if (SlotInChestLine == 4)
            {
                SlotInChestLine = 0;
                ChestInventoryContentBag.sizeDelta = ChestInventoryContentBag.sizeDelta + new Vector2(0, 75);
            }
        }


        for (int i = 0; i < Ch.MyIcons.Count; i++) //  кладем то что лежит в сундуке
        {
            Ch.MyIcons[i].transform.SetParent(ChestSlots[Ch.MyIcons[i].ChestPlace].transform);
            Ch.MyIcons[i].transform.position = Ch.MyIcons[i].transform.parent.position;
            Ch.MyIcons[i].transform.localScale = Constants.VectorOne;
            Ch.MyIcons[i].SetTextCount();   // отрисовываем количество
        }
        int slotI = 0;
        for (int i = 0; i < AllIcons.Count; i++) //  кладем то что лежит в сумке
            {
                if (!AllIcons[i].NowOnPlayer)
                {
                    if (slotI < ChestInventoryBag.transform.childCount)
                    {
                        AllIcons[i].transform.SetParent(ChestInventoryBag.transform.GetChild(slotI).transform);
                        AllIcons[i].transform.position = AllIcons[slotI].transform.parent.position;
                        AllIcons[i].SetTextCount();   // отрисовываем количество
                        AllIcons[i].transform.localScale = Constants.VectorOne;
                        slotI++;
                    }
                    else
                        break;
                }
        }
    }
    public void ShowIcons(GameObject[] need)                 // отрисовываем иконки в нужную таблицу
    {
        GameObject[] slots = need;

        for (int i = 0; i < slots.Length; i++)           // опустошаем
        {
            if (slots[i].transform.childCount > 0)
                slots[i].transform.GetChild(0).SetParent(AllIconsParent);
        }

        for (int i = 0; i < AllIcons.Count; i++)
        {
            if (AllIcons[i].ParentSlot != null)           // если за иконкой закреплен слот, то ставим ее туда
            {
                AllIcons[i].transform.SetParent(AllIcons[i].ParentSlot);
                AllIcons[i].transform.position = AllIcons[i].ParentSlot.position;
                AllIcons[i].SetTextCount();   // отрисовываем количество
                AllIcons[i].transform.localScale = Constants.VectorOne;
            }
        }

        for (int i = 0; i < AllIcons.Count; i++)  // если слота нет, то в первый пустой ставим и закрепляем
        {
            if (AllIcons[i].ParentSlot == null)
            {
                    for (int x = 0; x < slots.Length; x++)
                    {
                        if (slots[x].transform.childCount == 0)
                        {
                            AllIcons[i].transform.SetParent(slots[x].transform);
                            AllIcons[i].transform.position = AllIcons[i].transform.parent.position;
                            AllIcons[i].ParentSlot = slots[x].transform;
                            AllIcons[i].SetTextCount();   // отрисовываем количество
                            AllIcons[i].transform.localScale = Constants.VectorOne;
                        break;
                        }
                    }
              /*  if (AllIcons[i].ParentSlot==null)//если так и не нашел куда встать
                {
                    
                }*/
                }
        }
        for(int i=0; i<DopIcons.Count;i++)  //вытаскиваем из доп слотов
        {
            for (int x = 0; x < slots.Length; x++)
            {
                if (slots[x].transform.childCount == 0)
                {
                    RemoveFromShopSlots();
                }
            }
        }

        for (int i = 0; i < DopIcons.Count; i++)
            DopIcons[i].SetTextCount();
    }
    //________________________________________________________________________________________________________________
    public void ShowIconsInAfterDead(GameObject g)                 // отрисовываем иконки в сумку после смерти
    {
        CurrentBag = g;
        int SlotInLine = 0;

        int w = AfterDeadBag.childCount - 1;
        while (w > 0)                                                 //убираем иконки инвентаря
        {
            if (AfterDeadBag.GetChild(w).childCount > 0)
                AfterDeadBag.GetChild(w).GetChild(0).SetParent(IconsParent);
            w--;
        }


        if (AfterDeadBag.childCount != SlotCount + 1)
        {

            for (int i = AfterDeadBag.childCount - 1; i < SlotCount; i++)                               //создаем слотов столько, сколько мест в инвентаре
            {
                Transform t = Instantiate(AfterDeadSlotBag);
                t.gameObject.SetActive(true);
                t.GetComponent<Slot>().Init();
                t.SetParent(AfterDeadBag);
                t.localScale = new Vector3(1, 1, 1);
                SlotInLine++;
                if (SlotInLine == 4)
                {
                    SlotInLine = 0;
                    AfterDeadContentBag.sizeDelta = AfterDeadContentBag.sizeDelta + new Vector2(0, 75);
                }
            }
        }
        int Bagnumber = 1;
        for (int i = 0; i < AllIcons.Count; i++)                    //заполняем слоты вещами из сумки
        {
            if (!AllIcons[i].NowOnPlayer)
            {
                AllIcons[i].transform.SetParent(AfterDeadBag.GetChild(Bagnumber));
                AllIcons[i].SetTextCount();   // отрисовываем количество
                AllIcons[i].transform.localScale = Constants.VectorOne;
                Bagnumber++;
            }
        }

        int x = AfterDead.childCount - 1;
        while (x > 0)
        {
            if (AfterDead.GetChild(x).childCount > 0)
                AfterDead.GetChild(x).GetChild(0).SetParent(null);
            Destroy(AfterDead.GetChild(x).gameObject);
            x--;
        }

        SlotInDeadLine = 0;
        AfterDeadContent.sizeDelta = new Vector2(8, 95);

        AfterDeadBag a = g.GetComponent<AfterDeadBag>();

        for (int i = 0; i < a.MyIcons.Count; i++)                    //заполняем слоты выпавшими вещами
        {
            {
                Transform t = Instantiate(AfterDeadSlot);
                t.gameObject.SetActive(true);
                t.SetParent(AfterDead);
                t.name = "AfterSlot";
                t.localScale = new Vector3(1, 1, 1);
                a.MyIcons[i].transform.SetParent(t);
                a.MyIcons[i].transform.position = t.position;
                a.MyIcons[i].ParentSlot = t;
                a.MyIcons[i].SetTextCount();   // отрисовываем количество
                a.MyIcons[i].transform.localScale = Constants.VectorOne;
                SlotInDeadLine++;
                if (SlotInDeadLine > 4)
                {
                    SlotInDeadLine = 0;
                    AfterDeadContent.sizeDelta = AfterDeadContent.sizeDelta + new Vector2(0, 75);
                }
            }
        }

    }

    public void RemoveFromAfterDead()
    {
        SlotInDeadLine--;
        if (SlotInDeadLine <= 0)
        {
            SlotInDeadLine = 4;
            AfterDeadContent.sizeDelta = AfterDeadContent.sizeDelta - new Vector2(0, 75);
        }
        if (AfterDead.childCount == 2)  //убираем выпавший лут
        {
            AllBags.Remove(CurrentBag);
            Destroy(CurrentBag);
            CurrentBag = null;
            GOD.PlayerInter.CloseAfterDead();
        }

    }

    //________________________________________________________________________________________________________________
    public InfoScript CreateNewIcon(string IconName)
    {
        var g = Instantiate(Icon);
        var iconSprite = FindIcon(IconName);     
        g.GetComponent<Image>().sprite = iconSprite;
        g.GetComponent<InfoScript>().SetParameter(IconName);
        g.transform.SetParent(AllIconsParent);
        g.transform.localPosition = Constants.VectorZero;
        g.transform.localScale = Constants.VectorOne;
        g.name = "Icon_" + IconName;
        return g.GetComponent<InfoScript>();
    }

    public void AddToInventory(string s, int count, bool IsStart = false)            // добавление нового предмета в инвентарь
    {

        string name = "";
        if (!s.Contains("Icon"))
            name = s;
        else
        {
            for (int i = 5; i < s.Length; i++)
                name += s[i];
        }
        if (!GOD.InventoryContr.CanAdd(name, count))
        {
            AddToShopSlots(name, count);
        }
        else
        {
            List<InfoScript> L = new List<InfoScript>();

            for (int i = 0; i < AllIcons.Count; i++)           // пока можем добавлять в существующий слот - добавляем
            {
                if (AllIcons[i].thisName == name)
                {
                    int maxCount = count;
                    for (int x = 0; x < maxCount; x++)
                    {
                        if (AllIcons[i].ResourceCount < AllIcons[i].MaxResourceCount)
                        {
                            AllIcons[i].ResourceCount++;
                            if (!L.Contains(AllIcons[i]))
                                L.Add(AllIcons[i]);
                            count--;
                        }
                    }
                }
            }
            for (int i = 0; i < L.Count; i++)  //отрисовываем в панель быстрого доступа новое количество
            {
                IconInGame iig = L[i].InGame;
                if (iig)
                    iig.ShowCount();
            }


            InfoScript I = Icon.GetComponent<InfoScript>();
            I.SetParameter(name);
            int newNumber = count / I.MaxResourceCount;           // создаем новые слоты
            if (count % I.MaxResourceCount > 0)
                newNumber++;
            for (int i = 0; i < newNumber; i++)
            {
                InfoScript newIcon = CreateNewIcon(name);
                int maxCount = count;
                for (int x = 0; x < maxCount; x++)
                {
                    if (newIcon.ResourceCount < newIcon.MaxResourceCount)
                    {
                        newIcon.ResourceCount++;
                        count--;
                    }
                }
                AllIcons.Add(newIcon);
                if (newIcon.currentValidity > 0)
                    ObjectsWithValidity.Add(newIcon);
                AddDopFunction(newIcon);
            }
            if (!GOD.IsTutorial && GOD.QuestContr.IsInit)
            {
                if (name == "Stone")
                {
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.Stone_Instruments);
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.StoneSword);
                }
                if (GOD.QuestContr.AddToInventory.ContainsKey(name))   //если ожидаем такой предмет, то проверяем все квесты на завершенность
                    GOD.QuestContr.CheckAllQuests("get", name, count);
            }

            if (!IsStart)
            {
                GOD.Save.SaveGame(); //добавление в инвентарь
            }

        }
    }

   
    public void AddToShopSlots(string s, int count)  // если нет места то предметы из шопа помещаем так
    {
        for (int i = 0; i < DopIcons.Count; i++)           // пока можем добавлять в существующий слот - добавляем
        {
            if (DopIcons[i].thisName == s)
            {
                int maxCount = count;
                for (int x = 0; x < maxCount; x++)
                {
                    if (DopIcons[i].ResourceCount < DopIcons[i].MaxResourceCount)
                    {
                        DopIcons[i].ResourceCount++;
                        count--;
                    }
                }
            }
        }

        InfoScript I = Icon.GetComponent<InfoScript>();
        I.SetParameter(s);
        int newNumber = count / I.MaxResourceCount;           // создаем новые слоты
        if (count % I.MaxResourceCount > 0)
            newNumber++;
        for (int i = 0; i < newNumber; i++)
        {
            InfoScript newIcon = CreateNewIcon(s);
            int maxCount = count;
            for (int x = 0; x < maxCount; x++)
            {
                if (newIcon.ResourceCount < newIcon.MaxResourceCount)
                {
                    newIcon.ResourceCount++;
                    count--;
                }
            }
            Destroy(newIcon.GetComponent<DragHandler>());
            newIcon.transform.SetParent(AddShopSlot().transform);
            newIcon.transform.localScale = Constants.VectorOne;
            DopIcons.Add(newIcon);
        }
    }

    public void RemoveFromShopSlots()     // перемещаем из дополнительных слотов в нормальные
    {
        if (DopIcons.Count > 0)
        {
            AddToInventory(DopIcons[0].thisName, DopIcons[0].ResourceCount);
            GameObject p = DopIcons[0].transform.parent.gameObject;
            DopIcons.Remove(DopIcons[0]);
            Destroy(p);
            if (SlotInLine > 0)
                SlotInLine--;
            if (!GOD.PlayerInter.Inventory.activeSelf)
                GOD.PlayerInter.CloseInventoryImmediatly();
            else
            {
                GOD.PlayerInter.CloseInventoryImmediatly();
                GOD.PlayerInter.OpenInventory();
            }
        }
    }

    public void RemoveFromInventoryByName(string nameI, int c)
    {
        List<InfoScript> ForDelete = new List<InfoScript>();

      //  GOD.PlayerInter.ShowLoseItem("Icon_" + nameI, c, false);
        if (nameI == "Bucket")
        {
            for (int i = 0; i < AllIcons.Count; i++)
            {
                if (AllIcons[i].thisName == nameI)
                {
                    int maxc = c;
                    for (int x = 0; x < maxc; x++)
                    {
                        if (AllIcons[i].WaterCount > 0)
                        {
                            AllIcons[i].WaterCount--;
                            c--;
                        }
                        if (AllIcons[i].WaterCount == 0)
                            AllIcons[i].GetComponent<InfoScript>().SetWaterInBacket(0);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < AllIcons.Count; i++)
            {
                if (AllIcons[i].thisName == nameI)
                {
                    int maxc = c;
                    for (int x = 0; x < maxc; x++)
                    {
                        if (AllIcons[i].ResourceCount > 0 && AllIcons[i].MaxResourceCount > 1)
                        {
                            AllIcons[i].ResourceCount--;
                            c--;
                            if (AllIcons[i].ResourceCount == 0)
                            {
                                if (!ForDelete.Contains(AllIcons[i]))
                                    ForDelete.Add(AllIcons[i]);
                            }
                        }
                        else
                        {
                            if (!ForDelete.Contains(AllIcons[i]))
                                ForDelete.Add(AllIcons[i]);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < ForDelete.Count; i++)
        {
            ForDelete[i].Delete(GOD);
            AllIcons.Remove(ForDelete[i]);
            Destroy(ForDelete[i].gameObject);
            GOD.PlayerInter.UpdateInventory();
            //RemoveFromShopSlots();
        }

        GOD.Save.SaveGame();//удаление из инвентаря
    }
    public void RemoveFromInventory(InfoScript g, int c)
    {
        if (g)
        {
                GOD.PlayerInter.ShowLoseItem(g.name, c, false);
            g.ResourceCount -= c;
            if (g.ResourceCount <= 0)
            {
                if (g.SlotInGame)
                    g.InGame.OutIcon();
                if (g.InChest) //удаляем из сундука, если был там
                    g.InChest.MyIcons.Remove(g);
                if (ObjectsWithValidity.Contains(g))
                    ObjectsWithValidity.Remove(g);
                AllIcons.Remove(g);
                Destroy(g);
                // RemoveFromShopSlots();
            }
            else
            {
                if (g.SlotInGame)
                    g.InGame.ShowCount();
            }
            GOD.PlayerInter.UpdateInventory();
            GOD.Save.SaveGame();//удаление из инвентаря
        }

    }

    public void DeleteIcon(GameObject G, bool NeedToShow = true)
    {
        InfoScript I = G.GetComponent<InfoScript>();
        I.Delete(GOD);
        if (I.SlotInGame)
            I.InGame.OutIcon();
        if (I.InChest) //удаляем из сундука, если был там
            I.InChest.MyIcons.Remove(I);
        if (ObjectsWithValidity.Contains(I))
            ObjectsWithValidity.Remove(I);
        if (NeedToShow)
        {
            int count = 1;
            count = I.ResourceCount;
            GOD.PlayerInter.ShowLoseItem(G.name, count, false);
        }
        AllIcons.Remove(G.GetComponent<InfoScript>());
        Destroy(G);
        //RemoveFromShopSlots();
        if(!GOD.PlayerInter.ChestWindow.activeSelf)
            GOD.PlayerInter.UpdateInventory();
        GOD.Save.SaveGame();//удаление иконки
    }
    //ГЕТЫ_____________________________________________________________________________________________________
    public int InvnentoryGetCount(string s)                    // возвращаем значение по имени
    {
        int value = 0;

        if (s == "Bucket")
        {
            for (int i = 0; i < AllIcons.Count; i++)
            {
                if (s == AllIcons[i].thisName)
                    value += AllIcons[i].WaterCount;
            }
        }
        else
        {
            for (int i = 0; i < AllIcons.Count; i++)
            {
                if (s == AllIcons[i].thisName)
                    value += AllIcons[i].ResourceCount;
            }
        }
        return value;
    }

    public Sprite FindIcon(string name)          // находим иконку по имени   
    {
        var iconName = "Icon_" + name;
        if (Icons.TryGetValue(iconName, out var sprite))
            return sprite;

        Debug.LogError("Not found: " + iconName);
        return null;
    }

     public GameObject FindItem(string n)        //возвращаем первую найденную иконку
    {
        for (int x = 0; x < AllIcons.Count; x++)
        {
            if (AllIcons[x].name == "Icon_" + n)            // есть ли нужные элементы в  инвентаре
            {
                return AllIcons[x].gameObject;
            }
        }
        return null; 
    }
    //______________________________________________________________________________________________________________
    public bool CanAdd(string AddName, int AddCount)
    {
        bool Result = false;

        GOD.InventoryContr.Icon.GetComponent<InfoScript>().SetParameter(AddName);
        if (GOD.InventoryContr.Icon.GetComponent<InfoScript>().MaxResourceCount == 1)        // проверка для не стакающихся
        {
            int FreeSlot = 0;
            for (int i = 0; i < SlotInBag.Length; i++)
            {
                if (SlotInBag[i].transform.childCount == 0)
                {
                    FreeSlot++;
                    if (FreeSlot >= AddCount)
                    {
                        Result = true;
                        break;
                    }
                }
            }
        }
        else                                                                                 // проверка для  стакающихся
        {
            for (int i = 0; i < AllIcons.Count; i++)
            {
                if (AllIcons[i].thisName == AddName)
                {
                    int maxCount = AddCount;
                    int SlotCount = AllIcons[i].ResourceCount;
                    for (int x = 0; x < maxCount; x++)
                    {
                        if (SlotCount < AllIcons[i].MaxResourceCount)
                        {
                            AddCount--;
                            SlotCount++;
                        }
                        if (AddCount == 0)
                        {
                            Result = true;
                            break;
                        }
                    }
                }
            }

            int newNumber = AddCount / GOD.InventoryContr.Icon.GetComponent<InfoScript>().MaxResourceCount;           //  новые слоты
            if (AddCount % GOD.InventoryContr.Icon.GetComponent<InfoScript>().MaxResourceCount > 0)
                newNumber++;
            int FreeSlot = 0;
            for (int i = 0; i < SlotInBag.Length; i++)
            {
                if (SlotInBag[i].transform.childCount == 0)
                {
                    FreeSlot++;
                    if (FreeSlot >= newNumber)
                    {
                        Result = true;
                        break;
                    }
                }
            }
        }
        return Result;
    }


    public bool CanAddSeveral(int AddCount)
    {
        int ReservSlot = 0;
        for (int i = 0; i < AllIcons.Count; i++)
        {
            if (!AllIcons[i].NowOnPlayer)
                ReservSlot++;

        }
        bool Result = false;
        if ((SlotInBag.Length - ReservSlot) >= AddCount)
            Result = true;
        return Result;
    }






    public void AddIcon(InfoScript newIcon)
    {
        AllIcons.Add(newIcon);
        AddDopFunction(newIcon);
    }

    //добавляем с определенными параметрами

    public void AddToInventoryWithParametre(string s, int count, int dur, int val, int waterCount)
    {
        string name = "";
        if (!s.Contains("Icon"))
            name = s;
        else
        {
            for (int i = 5; i < s.Length; i++)
                name += s[i];
        }


        for (int i = 0; i < AllIcons.Count; i++)           // пока можем добавлять в существующий слот - добавляем
        {
            if (AllIcons[i].thisName == name)
            {
                int maxCount = count;
                for (int x = 0; x < maxCount; x++)
                {
                    if (AllIcons[i].ResourceCount < AllIcons[i].MaxResourceCount)
                    {
                        AllIcons[i].ResourceCount++;
                        count--;
                    }
                }
            }
        }

        InfoScript I = Icon.GetComponent<InfoScript>();
        I.SetParameter(name);
        I.currentDurability = dur;
        I.currentValidity = val;
        I.DurabilityImage.fillAmount = (float)I.currentDurability / (float)I.durability;
        I.ValidityImage.fillAmount = (float)I.currentValidity / 100;
        int newNumber = count / I.MaxResourceCount;           // создаем новые слоты
        if (count % I.MaxResourceCount > 0)
            newNumber++;
        for (int i = 0; i < newNumber; i++)
        {
            InfoScript newIcon = CreateNewIcon(name);
            newIcon.currentDurability = dur;
            newIcon.currentValidity = val;
            newIcon.DurabilityImage.fillAmount = (float)I.currentDurability / (float)I.durability;
            newIcon.ValidityImage.fillAmount = (float)I.currentValidity / 100;
            int maxCount = count;
            for (int x = 0; x < maxCount; x++)
            {
                if (newIcon.ResourceCount < newIcon.MaxResourceCount)
                {
                    newIcon.ResourceCount++;
                    count--;
                }
            }
            AllIcons.Add(newIcon);
            if (newIcon.currentValidity > 0)
                ObjectsWithValidity.Add(newIcon);
            AddDopFunction(newIcon);

            newIcon.WaterCount = waterCount;
            if (newIcon.WaterCount >= 0)
                newIcon.SetWaterInBacket(waterCount);
        }


    }
    //ДОАБВЛЯЕМ КАК ЕСТЬ
    public void AddToInventoryWithParametreAndPlace(string s, int count, int dur, int val, int waterCount=-1, int IconsOnBag = -1, string IconsOnPlayer = "", int IconsOnGame = -1)
    {
        string name = "";
        if (!s.Contains("Icon"))
            name = s;
        else
        {
            for (int i = 5; i < s.Length; i++)
                name += s[i];
        }
        InfoScript I = CreateNewIcon(name);
        I.ResourceCount = count;
        I.currentDurability = dur;
        I.currentValidity = val;
        I.DurabilityImage.fillAmount = (float)I.currentDurability / (float)I.durability;
        I.ValidityImage.fillAmount = (float)I.currentValidity / 100;
        AllIcons.Add(I);
        if (I.currentValidity > 0)
            ObjectsWithValidity.Add(I);
        AddDopFunction(I);
       // print(waterCount);
        I.WaterCount = waterCount;
        if (I.WaterCount > -1)
            I.SetWaterInBacket(waterCount);

       // print(IconsOnBag + " " + IconsOnPlayer);
        //ставим по местам________________________________________
        if (IconsOnBag > 0)
        {
            I.ParentSlot = GOD.InventoryContr.Bag.transform.GetChild(IconsOnBag);            // устанавливаем в сумку
        }
        else if (IconsOnPlayer != "")                                                                                           // устанавливаем на персонажа
        {
            for (int x = 0; x < GOD.PlayerContr.EqupmentSlots.Length; x++)
            {
                if (IconsOnPlayer == GOD.PlayerContr.EqupmentSlots[x].name)
                {
                    I.ParentSlot = GOD.PlayerContr.EqupmentSlots[x].transform;
                    GOD.PlayerContr.SetPlayerParametre(I.gameObject, false);
                }
            }
        }
        // в панель быстрого доступа__________________________________________________________________________
        if (IconsOnGame >= 0)
        {
            Transform help = GOD.PlayerInter.InventoryOnGame.GetChild(IconsOnGame).GetChild(0);
            help.GetComponent<IconInGame>().SetIcon(I.gameObject, help.parent.GetComponent<Slot>());
            help.gameObject.SetActive(true);
        }
    }



    void AddDopFunction(InfoScript newIcon)
    {

        if (newIcon.name.Contains("FishChest"))
        {
            if (!newIcon.GetComponent<FishChestScript>())
                newIcon.gameObject.AddComponent<FishChestScript>();
            FishChestScript fishchestS = newIcon.gameObject.GetComponent<FishChestScript>();
            fishchestS.GOD = GOD;
            fishchestS.thisDrag = newIcon.GetComponent<DragHandler>();
            fishchestS.thisDrag.IsUsialItem = false;
            if (!newIcon.GetComponent<Button>())
                newIcon.gameObject.AddComponent<Button>();
            newIcon.gameObject.GetComponent<Button>().onClick.AddListener(() => { newIcon.gameObject.GetComponent<FishChestScript>().OnChestPress(); });
        }
        else if (newIcon.name == "Icon_ResourceChest" || newIcon.name == "Icon_BigResourceChest")
        {
            if (!newIcon.GetComponent<ResourceChest>())
                newIcon.gameObject.AddComponent<ResourceChest>();
            ResourceChest resourceS = newIcon.gameObject.GetComponent<ResourceChest>();
            resourceS.Init();
            resourceS.GOD = GOD;
            resourceS.thisDrag = newIcon.GetComponent<DragHandler>();
            resourceS.thisDrag.IsUsialItem = false;
            if (!newIcon.GetComponent<Button>())
                newIcon.gameObject.AddComponent<Button>();
            newIcon.gameObject.GetComponent<Button>().onClick.AddListener(() => { newIcon.gameObject.GetComponent<ResourceChest>().OnChestPress(); });
        }
        else if (newIcon.name == "Icon_InstrumentChest" || newIcon.name == "Icon_BigInstrumentChest")
        {
            if (!newIcon.GetComponent<InstrumentChest>())
                newIcon.gameObject.AddComponent<InstrumentChest>();
            InstrumentChest instrumentChest = newIcon.gameObject.GetComponent<InstrumentChest>();
            instrumentChest.Init();
            instrumentChest.GOD = GOD;
            instrumentChest.thisDrag = newIcon.GetComponent<DragHandler>();
            instrumentChest.thisDrag.IsUsialItem = false;
            if (!newIcon.GetComponent<Button>())
                newIcon.gameObject.AddComponent<Button>();
            newIcon.gameObject.GetComponent<Button>().onClick.AddListener(() => { newIcon.gameObject.GetComponent<InstrumentChest>().OnChestPress(); });
        }
        else if (newIcon.name == "Icon_BuildingChest" || newIcon.name == "Icon_BigBuildingChest")
        {
            if (!newIcon.GetComponent<BuildingChest>())
                newIcon.gameObject.AddComponent<BuildingChest>();
            BuildingChest buildingChest = newIcon.gameObject.GetComponent<BuildingChest>();
            buildingChest.Init();
            buildingChest.GOD = GOD;
            buildingChest.thisDrag = newIcon.GetComponent<DragHandler>();
            buildingChest.thisDrag.IsUsialItem = false;
            if (!newIcon.GetComponent<Button>())
                newIcon.gameObject.AddComponent<Button>();
            newIcon.gameObject.GetComponent<Button>().onClick.AddListener(() => { newIcon.gameObject.GetComponent<BuildingChest>().OnChestPress(); });
        }
        else if (newIcon.name == "Icon_EssenceChest")
        {
            if (!newIcon.GetComponent<EssenceChest>())
                newIcon.gameObject.AddComponent<EssenceChest>();
            EssenceChest essenceChest = newIcon.gameObject.GetComponent<EssenceChest>();
            essenceChest.Init();
            essenceChest.GOD = GOD;
            essenceChest.thisDrag = newIcon.GetComponent<DragHandler>();
            essenceChest.thisDrag.IsUsialItem = false;
            if (!newIcon.GetComponent<Button>())
                newIcon.gameObject.AddComponent<Button>();
            newIcon.gameObject.GetComponent<Button>().onClick.AddListener(() => { newIcon.gameObject.GetComponent<EssenceChest>().OnChestPress(); });
        }
        else if (newIcon.type == InfoScript.ItemType.food)
        {
            if (!newIcon.GetComponent<FoodScript>())
                newIcon.gameObject.AddComponent<FoodScript>();
            FoodScript foodS = newIcon.gameObject.GetComponent<FoodScript>();
            foodS.GOD = GOD;
            foodS.I = newIcon.GetComponent<InfoScript>();
            foodS.thisDrag = newIcon.GetComponent<DragHandler>();
            foodS.thisDrag.IsUsialItem = false;
        }
        else if (newIcon.type == InfoScript.ItemType.poition)
        {
            if (!newIcon.GetComponent<PoitionScript>())
                newIcon.gameObject.AddComponent<PoitionScript>();
            PoitionScript poitionS = newIcon.gameObject.GetComponent<PoitionScript>();
            poitionS.GOD = GOD;
            poitionS.I = newIcon.GetComponent<InfoScript>();
            poitionS.thisDrag = newIcon.GetComponent<DragHandler>();
            poitionS.thisDrag.IsUsialItem = false;
        }
        else if (newIcon.type == InfoScript.ItemType.building || newIcon.type == InfoScript.ItemType.construction)
        {
            if (!newIcon.GetComponent<BuildScript>())
                newIcon.gameObject.AddComponent<BuildScript>();
            BuildScript buildS = newIcon.gameObject.GetComponent<BuildScript>();
            buildS.GOD = GOD;
            buildS.I = newIcon.GetComponent<InfoScript>();
            buildS.thisDrag = newIcon.GetComponent<DragHandler>();
            buildS.thisDrag.IsUsialItem = false;
        }
        // else if (newIcon.type == "clothes" || newIcon.type == "instrument" || newIcon.type == "weapon" || newIcon.type == "mantle" || newIcon.type == "rod" || newIcon.type.Contains("Belt"))
        else if((newIcon.type == InfoScript.ItemType.clothes)|| (newIcon.type == InfoScript.ItemType.instrument)|| (newIcon.type == InfoScript.ItemType.weapon)|| (newIcon.type == InfoScript.ItemType.rod))
        {
            if (!newIcon.GetComponent<ClothesScript>())
                newIcon.gameObject.AddComponent<ClothesScript>();
            ClothesScript clotheS = newIcon.gameObject.GetComponent<ClothesScript>();
            clotheS.GOD = GOD;
            clotheS.I = newIcon.GetComponent<InfoScript>();
            clotheS.thisDrag = newIcon.GetComponent<DragHandler>();
            clotheS.thisDrag.IsUsialItem = false;
        }
    }

    public void NeedToShowOffer() //показываем оффер если нет ни топоров ни кирок
    {
        if (BuildSettings.isFree && GOD.Offers)
        {
            GOD.Offers.OpenOffersWindow("IconRecoverInstrument");
        }
    }
}
