﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseWindow : MonoBehaviour {

    GameManager GOD;
    MenuScript Menu;
    Animator anim;

    void Start()
    {
        Init();
    }
    public void Init()
    {
        if (gameObject.name == "Shop" || gameObject.name == "BuyGemWindow" || gameObject.name == "Offer")
            GOD = GameObject.Find("GameManager").GetComponent<GameManager>();
        anim = transform.GetComponent<Animator>();
    }

    public void StartClose()
    {
        if (anim)
        {
            anim.enabled = true;
            anim.Play("InventoryWindowClose");
        }
        else
            gameObject.SetActive(false);
    }
    public void Close()
    {
        if (gameObject.name == "Shop")
            GOD.ShopScript.UnloadImage();
        else if (gameObject.name == "BuyGemWindow")
            GOD.ShopScript.UnloadGemImage();
        else if (gameObject.name == "Offer")
            GOD.Offers.Unloadimage();
        if (anim)
            anim.enabled = false;
        gameObject.SetActive(false);
    }

}
