﻿using UnityEngine;
using System.Collections;

public class ItemsPositionOnPlayer : MonoBehaviour {


    [Header("Boy:")]
    public Vector3 BoyPositionOnHand;
    public Vector3 BoyRotationOnHand;
    public Vector3 BoyPositionOnPlayer;
    public Vector3 BoyRotationOnPlayer;
    [Header("Girl:")]
    public Vector3 GirlPositionOnHand;
    public Vector3 GirlRotationOnHand;
    public Vector3 GirlPositionOnPlayer;
    public Vector3 GirlRotationOnPlayer;

    public Vector3 PosOnHand(string s)
    {
        if (s == "Girl")
            return GirlPositionOnHand;
        else
            return BoyPositionOnHand;
    }

    public Vector3 RotOnHand(string s)
    {
        if (s == "Girl")
            return GirlRotationOnHand;
        else
            return BoyRotationOnHand;
    }
    public Vector3 PosOnPlayer(string s)
    {
        if (s == "Girl")
            return GirlPositionOnPlayer;
        else
            return BoyPositionOnPlayer;
    }

    public Vector3 RotOnPlayer(string s)
    {
        if (s == "Girl")
            return GirlRotationOnPlayer;
        else
            return BoyRotationOnPlayer;
    }
}
