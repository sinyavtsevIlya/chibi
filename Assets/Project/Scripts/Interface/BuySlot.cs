﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuySlot : MonoBehaviour {

    public GameManager GOD;
    public Text GemText;
    public int GoldSlot = 0; // сколько открыто слотов за голд
    public int ResSlot = 0; // сколько открыто слотов за ресурсы

    public Image ResImage, GoldImage;
    public Text ResCount, GoldCount;
    public Button ResButton, GoldButton;

    int CurrentGoldCount = 0;
    int CurrentResCount = 0;
    string CurrentResName = "";

    public void ButtonRes()
    {
        if (GOD.InventoryContr.InvnentoryGetCount(CurrentResName) >= CurrentResCount)
        {
            GOD.InventoryContr.AddSlot("second");
            GOD.InventoryContr.RemoveFromInventoryByName(CurrentResName, CurrentResCount);
            GOD.PlayerInter.OpenInventory();
            ResSlot++;
            ShowRes();
            GOD.InventoryContr.SlotCount++;
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Buy);
            GOD.Save.SaveGame(false);//сохраняем слоты
        }
        else //иначе открываем крафт
        {
            GOD.CraftContr.OpenResByName(CurrentResName);
        }
    }

    public void ButtonGold()
    {
        if ((Balance.Gem - CurrentGoldCount) > 0)
        {
            Balance.Gem = Balance.Gem - 10;
            GOD.InventoryContr.AddSlot("second");
            GoldSlot++;
            MyAppMetrica.LogEvent("Purchases", "Services", "New Inventory Slot", ""+GoldSlot);
            ShowGold();
            GOD.InventoryContr.SlotCount++;
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Buy);
            GOD.Save.SaveGame(false); //сохраняем слоты
        }
        else
            GOD.ShopScript.OpenGemWindow();
    }

    public void ShowRes()
    {

        switch(ResSlot)
        {
            case 0:
                CurrentResName = "Thread";
                CurrentResCount = 5;
                break;
            case 1:
                CurrentResName = "Thread";
                CurrentResCount = 10;
                break;
            case 2:
                CurrentResName = "Thread";
                CurrentResCount = 15;
                break;
            case 3:
                CurrentResName = "Thread";
                CurrentResCount = 20;
                break;
            case 4:
                CurrentResName = "Thread";
                CurrentResCount = 25;
                break;
            case 5:
                CurrentResName = "Skin";
                CurrentResCount = 5;
                break;
            case 6:
                CurrentResName = "Skin";
                CurrentResCount = 10;
                break;
            case 7:
                CurrentResName = "Skin";
                CurrentResCount = 15;
                break;
            case 8:
                CurrentResName = "Skin";
                CurrentResCount = 20;
                break;
            case 9:
                CurrentResName = "Skin";
                CurrentResCount = 25;
                break;
            case 10:
                CurrentResName = "Leather";
                CurrentResCount = 5;
                break;
            case 11:
                CurrentResName = "Leather";
                CurrentResCount = 10;
                break;
            case 12:
                CurrentResName = "Leather";
                CurrentResCount = 15;
                break;
            case 13:
                CurrentResName = "Leather";
                CurrentResCount = 20;
                break;
            case 14:
                CurrentResName = "Leather";
                CurrentResCount = 25;
                break;
            case 15:
                CurrentResName = "Emerald";
                CurrentResCount = 5;
                break;
            case 16:
                CurrentResName = "Emerald";
                CurrentResCount = 10;
                break;
            case 17:
                CurrentResName = "Emerald";
                CurrentResCount = 15;
                break;
            case 18:
                CurrentResName = "Emerald";
                CurrentResCount = 20;
                break;
            case 19:
                CurrentResName = "Emerald";
                CurrentResCount = 25;
                break;
            case 20:
                CurrentResName = "Sapphire";
                CurrentResCount = 5;
                break;
            case 21:
                CurrentResName = "Sapphire";
                CurrentResCount = 10;
                break;
            case 22:
                CurrentResName = "Sapphire";
                CurrentResCount = 15;
                break;
            case 23:
                CurrentResName = "Sapphire";
                CurrentResCount = 20;
                break;
            case 24:
                CurrentResName = "Sapphire";
                CurrentResCount = 25;
                break;
            case 25:
                CurrentResName = "Amethyst";
                CurrentResCount = 5;
                break;
            case 26:
                CurrentResName = "Amethyst";
                CurrentResCount = 10;
                break;
            case 27:
                CurrentResName = "Amethyst";
                CurrentResCount = 15;
                break;
            case 28:
                CurrentResName = "Amethyst";
                CurrentResCount = 20;
                break;
            case 29:
                CurrentResName = "Amethyst";
                CurrentResCount = 25;
                break;
            case 30:
                CurrentResName = "Ruby";
                CurrentResCount = 5;
                break;
            case 31:
                CurrentResName = "Ruby";
                CurrentResCount = 10;
                break;
            case 32:
                CurrentResName = "Ruby";
                CurrentResCount = 15;
                break;
            case 33:
                CurrentResName = "Ruby";
                CurrentResCount = 20;
                break;
            case 34:
                CurrentResName = "Ruby";
                CurrentResCount = 25;
                break;
            case 35:
                CurrentResName = "Dimond";
                CurrentResCount = 5;
                break;
            case 36:
                CurrentResName = "Dimond";
                CurrentResCount = 10;
                break;
            case 37:
                CurrentResName = "Dimond";
                CurrentResCount = 15;
                break;
            case 38:
                CurrentResName = "Dimond";
                CurrentResCount = 20;
                break;
            case 39:
                CurrentResName = "Dimond";
                CurrentResCount = 25;
                break;
            case 40:
                ResImage.enabled=false;
                ResCount.text = "";
                ResButton.GetComponent<Selectable>().interactable = false;
                ResButton.GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
                break;

        }

        ResImage.sprite = GOD.InventoryContr.FindIcon(CurrentResName);
        ResCount.text = "" + CurrentResCount;
        if (GOD.InventoryContr.InvnentoryGetCount(CurrentResName) < CurrentResCount)
        {
            ResCount.color = new Color(1, 0.1f, 0.1f);
        }
        else
            ResCount.color = new Color(1, 1, 1);
    }


    public void ShowGold()
    {
        if(GoldSlot>0)
            CurrentGoldCount = 10;

        GOD.ShopScript.ShowGemCount();
        if (Balance.Gem < CurrentGoldCount)
            GoldCount.color = new Color(1, 0.1f, 0.1f);
        else
            GoldCount.color = new Color(1, 1, 1);

        if (GoldSlot>=40)
        {
            GoldImage.enabled = false;
            GoldCount.text = "";
            GoldButton.GetComponent<Selectable>().interactable = false;
            GoldButton.GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
        }

    }

 }
