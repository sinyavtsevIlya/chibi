﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;



public class PlayerInterface : MonoBehaviour {

    public GameManager GOD;

    public GameObject ObjectTarget, EnemyTarget;

    public Image HPpanel, HPoldPanel, HPblock, EATpanel;  // индикаторы
    bool NowChangeHP = false;
    public Image TargetHP, TargetOldHP;                          // хп цели
    bool NowChangeEnemyHP = false;

    public GameObject AllCanvas;
    public GameObject Black;
    public GameObject MobileJoystik;
    public GameObject NewItem, NewItem2, NewItem3;
    public Animator NewItemAnim, NewItem2Anim, NewItem3Anim;

    // кнопки  взаимодействия
    [Header("Кнопки взаимодействия:")]
    public GameObject TargetHPPanel;                // панелька с хп цели
    public GameObject AttackEnemyButton, GetButton;      // кнопки взаимодействия
    public Button AttackEnemy, Get;
    public Text TargetNameText;                    // отображении имени цели
    public int KD = 300;                       // время отката
    public Sprite AttackS, AxeS, PickS, ShovelS;  // спрайты для кнопки атаки
    public Image AttackImage;
    public bool FightIsDown = false;   //зажата ли кнопка атаки
    string FightDownTag = "";
    bool IsFishingDown = false;

    // Инвентарь
    [Header("Инвентарь:")]
    public GameObject Inventory;            // окно инвентаря
    public GameObject PlayerWindow, InfoWindow;       // вкладки инфо и игрока
    GameObject CurrentShownObject=null;
    public Transform Icon;                            // иконка в окне информации
    public Text InfoText, InfoNameText, InfoPlayerText, InfoDayText;                              // текст в окне информации
    public Image InfoHP, InfoHPblock, InfoEAT;//индикаторы в окне информации
    List<string> NewItemsName = new List<string>();
    List<int> NewItemsCount = new List<int>();
    List<string> OldItemsName = new List<string>();
    List<int> OldItemsCount = new List<int>();
    List<bool> OldItemsBool = new List<bool>();
    public Slot DeleteBtn;  //слот удаления
    public Slot PlayerBtn;  //персонаж в окне персонажа
    CloseWindow InventoryClose;
    public GameObject ListBtn, AchivmentBtn;

    //Панель быстрого доступа
    [Header("Панель быстрого доступа:")]
    public Transform InventoryOnGame;
    public int OnGameSlotsCount=0; //занятые слоты в панели быстрого доступа(нужны для старта квеста с поясами)

    //Название острова
    [Header("Название острова:")]
    public GameObject IslandsName;
    public Text IslandNameText;
    Animator IslandsNameAnimator;

    // Костер
    [Header("Костер:")]
    public GameObject FirecampWindow;
    public Transform CurrentFirecamp=null;
    public Image FireIcon;
    public Button AddWood;
    CloseWindow FirecampClose;


    // Крафт
    [Header("Крафт:")]
    public GameObject BridgeCraftWindow;
    public Selectable BridgeCraftButton;
    public Transform Res1, Res2;
    Image IRes1, IRes2;
    Text TRes1, TRes2;          // текст сколько нужно ресурсов, текст сколько есть
    public Transform bridge;
    public Text CraftBridgeName, CraftBridgeText;
    CloseWindow  CraftBridgeClose;

    // Сундуки
    [Header("Сундуки:")]
    public GameObject ChestWindow;
    public GameObject ChestBlack;
    public Text ChestChest, ChestInventory; //название в сундуках
    CloseWindow ChestInventoryClose;

    //Смена погоды
    [Header("Погода:")]
    public GameObject ChangeWeatherWindow;
    public Image ChangeWeatherBtn;
    public Text WindowText, SnowText1, SnowText2, SunText1, SunText2, WaterText1, WaterText2;
    public Button ToSnow, ToSun, ToWater;
    Text ToSnowTxt, ToSunTxt, ToWaterTxt;
    public Text IceCount, SunCount, WaterCount;
    public GameObject TimeWindow; //окошко с таймером
    public Text TimeTxt;   //текс отсчета времени
    public int EssenceMinutTimer =0;
    public int EssenceSecondTimer = 0;
    public string CurrentEssence = "";
    public Button SunBtn, SnowBtn, WaterBtn;
    public Text SunTxt, SnowTxt, WaterTxt;
    public GameObject SunEs, SnowEs, WaterEs; //эссенции в крафте
    CloseWindow ChangeWeatherClose;

    //Замерзание
    [Header("Замерзание:")]
    public Material FrostMaterial;
    public Image FrostWindow;

    //Окно покупки слота
    [Header("Покупка слота:")]
    public GameObject BuySlotWindow;
    public BuySlot BuySlotScript;
    public Text TOr, TUp;   // надписи в окне увелечения инвентаря
    CloseWindow BuySlotClose;

    //Окно недостаточно места
    [Header("Недостаточно места:")]
    public GameObject CantOpen;               // окно невозможно добавить
    public Text UpgradeBag, CantAdd;
    CloseWindow CantOpenClose;

    //Окно смерти
    [Header("Смерть:")]
    public GameObject Dead;
    public GameObject BagAfterDead, BagAfterDeadBlack;           
    public Text Head1, Head2, Head3, BtnName1, BtnDes1, BtnName2, BtnDes2, Btn1, Btn2;
    public GameObject PlayerLut;
    public Text DeadChest, DeadInventory;
    CloseWindow BagAfterDeadClose;

    [Header("Заглушка:")]
    public GameObject Zaglushka;
    Image ZImage;

    public void InitGame()
    {
        AttackEnemy = AttackEnemyButton.GetComponent<Button>();
        Get = GetButton.GetComponent<Button>();
        Get.gameObject.SetActive(false);
        TargetHPPanel.SetActive(false);
        Inventory.SetActive(false);

        IRes1 = Res1.GetChild(0).GetComponent<Image>();         // для окна крафта моста
        IRes2 = Res2.GetChild(0).GetComponent<Image>();
        TRes1 = Res1.GetChild(2).GetComponent<Text>();
        TRes2 = Res2.GetChild(2).GetComponent<Text>();
        CraftBridgeClose = BridgeCraftWindow.GetComponent<CloseWindow>();

        WaterTxt = WaterBtn.transform.GetChild(1).GetComponent<Text>();
        SnowTxt = SnowBtn.transform.GetChild(1).GetComponent<Text>();
        SunTxt = SunBtn.transform.GetChild(1).GetComponent<Text>();
        ToWaterTxt = ToWater.transform.GetChild(0).GetComponent<Text>();
        ToSnowTxt = ToSnow.transform.GetChild(0).GetComponent<Text>();
        ToSunTxt = ToSun.transform.GetChild(0).GetComponent<Text>();

        for (int i = 0; i < InventoryOnGame.childCount; i++)
            InventoryOnGame.GetChild(i).GetComponent<Slot>().Init();

        NewItemAnim = NewItem.GetComponent<Animator > ();
        NewItem2Anim = NewItem2.GetComponent<Animator>();
        NewItem3Anim = NewItem3.GetComponent<Animator>();

        IslandsNameAnimator = IslandsName.GetComponent<Animator>();

        if(!GOD.IsTutorial)
            ZImage = Zaglushka.transform.GetChild(0).GetComponent<Image>();

        InventoryClose = Inventory.GetComponent<CloseWindow>();
        FirecampClose = FirecampWindow.GetComponent<CloseWindow>();
        ChestInventoryClose = ChestWindow.GetComponent<CloseWindow>();
        BuySlotClose = BuySlotWindow.GetComponent<CloseWindow>();
        ChangeWeatherClose = ChangeWeatherWindow.GetComponent<CloseWindow>();
        CantOpenClose = CantOpen.GetComponent<CloseWindow>();
        BagAfterDeadClose = BagAfterDead.GetComponent<CloseWindow>();

#if NO_GPGS && NO_GC
        ListBtn.SetActive(false);
        AchivmentBtn.SetActive(false);
#endif

        InitLanguage();

    }

    public void ShowIslandName(string islandname)
    {
        IslandNameText.text = Localization.instance.getTextByKey("#Name" + islandname);
        IslandsName.SetActive(true);
        IslandsNameAnimator.Play("OpenIslandName");
    }
    public void InitLanguage()
    {
        CraftBridgeText.text = Localization.instance.getTextByKey("#Craft");
        TOr.text = Localization.instance.getTextByKey("#Or");
        TUp.text = Localization.instance.getTextByKey("#UpInterface");
        CantAdd.text = Localization.instance.getTextByKey("#NoPlace");
        UpgradeBag.text = Localization.instance.getTextByKey("#UpBag");

        ChestChest.text = Localization.instance.getTextByKey("#Chest");
        ChestInventory.text = Localization.instance.getTextByKey("#Inventory");
       DeadChest.text = Localization.instance.getTextByKey("#Bag");
        DeadInventory.text = Localization.instance.getTextByKey("#Inventory");

        WaterTxt.text = Localization.instance.getTextByKey("#Craft");
        SunTxt.text = Localization.instance.getTextByKey("#Craft");
        SnowTxt.text = Localization.instance.getTextByKey("#Craft");
        ToWaterTxt.text = Localization.instance.getTextByKey("#Use");
        ToSunTxt.text = Localization.instance.getTextByKey("#Use");
        ToSnowTxt.text = Localization.instance.getTextByKey("#Use");
        WindowText.text = Localization.instance.getTextByKey("#WeatherChange");
        SnowText1.text = Localization.instance.getTextByKey("#Cold");
        SnowText2.text = Localization.instance.getTextByKey("#ColdDescription");
        SunText1.text = Localization.instance.getTextByKey("#Hot");
        SunText2.text = Localization.instance.getTextByKey("#HotDescription");
        WaterText1.text = Localization.instance.getTextByKey("#Wet");
        WaterText2.text = Localization.instance.getTextByKey("#WetDescription");

        GOD.FishingWindow.FishingStage.text = Localization.instance.getTextByKey("#Stage");
    }
    public void InitTut()
    {
        AttackEnemy = AttackEnemyButton.GetComponent<Button>();
        Get = GetButton.GetComponent<Button>();
        Get.gameObject.SetActive(false);
        TargetHPPanel.SetActive(false);
        Inventory.SetActive(false);

        for (int i = 0; i < InventoryOnGame.childCount; i++)
            InventoryOnGame.GetChild(i).GetComponent<Slot>().Init();

    }

    void Update()
    {
        if (GOD.PlayerContr.IsAlive)
        {
            if (FightIsDown)
            {
                if (GOD.PlayerAnimation.m_Animator.GetFloat("Speed") < 0)
                {
                    if (ObjectTarget == null || ObjectTarget.CompareTag("Enemy"))
                        GOD.PlayerAnimation.AttackAnimation();
                    else
                    {
                        if (ObjectTarget != null)
                            GOD.PlayerAnimation.CollectAnimation(true, FightDownTag);
                        else
                            FightUp();
                    }
                }
                else
                    FightUp();
            }
        }
    }

    // Сохранения
    public void SaveGame()
    {
        GOD.Save.SaveGame(false);//кнопка
    }
   
    // Перезагрузка
    public void Reload()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("Game");
    }
    // СБОР
    public void SetTarget(GameObject CurrentTarget)               // определяем какие кнопки включать
    {
        //Target = CurrentTarget;
        if (CurrentTarget != null)
        {
            //print(CurrentTarget.name);
            if (CurrentTarget != ObjectTarget)
            {
                string s = CurrentTarget.tag;
                switch (s)
                {
                    case "Door":
                        Get.gameObject.SetActive(true);
                        ObjectTarget = CurrentTarget;
                        TargetHPPanel.SetActive(false);
                        if (ObjectTarget)
                            TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        AttackImage.sprite = AttackS;
                        break;
                    case "Enemy":
                        Get.gameObject.SetActive(false);
                        ObjectTarget = CurrentTarget;
                        SetTargetHP("Enemy");
                        TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<Enemy>().EnemyTag);
                        AttackImage.sprite = AttackS;
                        break;
                    case "Tree":
                        if (CurrentTarget.name.Contains("Stick"))
                            ObjectTarget = CurrentTarget.transform.parent.gameObject;
                        else
                            ObjectTarget = CurrentTarget;
                        if (CurrentTarget.GetComponent<ObjectScript>().CollectionResNumber1 == 0 && ObjectTarget.GetComponent<ObjectScript>().CollectionResNumber2 == 0)
                            Get.gameObject.SetActive(false);
                        else
                            Get.gameObject.SetActive(true);
                        SetTargetHP("Object");
                        TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        AttackImage.sprite = AxeS;
                        break;
                    case "Stone":
                        Get.gameObject.SetActive(false);
                        ObjectTarget = CurrentTarget;
                        SetTargetHP("Object");
                        TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        AttackImage.sprite = PickS;
                        break;
                    case "Bush":
                        ObjectTarget = null;
                        ObjectTarget = CurrentTarget;
                        if (CurrentTarget.GetComponent<ObjectScript>().ObjectTag != "")
                        {
                            if (CurrentTarget.GetComponent<ObjectScript>().CollectionResNumber1 == 0 && ObjectTarget.GetComponent<ObjectScript>().CollectionResNumber2 == 0)
                                Get.gameObject.SetActive(false);
                            else
                            {
                                Get.gameObject.SetActive(true);
                            }
                        }
                        else
                            Get.gameObject.SetActive(false);
                        TargetHPPanel.SetActive(false);
                        if (ObjectTarget)
                            TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        AttackImage.sprite = AttackS;
                        break;
                    case "Heap":
                        if (GOD.PlayerContr.ShovelSpeed > 0)
                        {
                            Get.gameObject.SetActive(false);
                        }
                        ObjectTarget = CurrentTarget;
                        SetTargetHP("Object");
                        TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        AttackImage.sprite = ShovelS;
                        break;
                    case "Fish":
                        Get.gameObject.SetActive(false);

                        ObjectTarget = CurrentTarget;
                        TargetHPPanel.SetActive(false);
                        TargetNameText.text = Localization.instance.getTextByKey("#Fish");
                        AttackImage.sprite = AttackS;
                        break;
                    case "Water":
                        if ((CurrentTarget.name == "River") && (GOD.PlayerContr.HaveBucket))
                            Get.gameObject.SetActive(true);
                        else
                            Get.gameObject.SetActive(false);

                        if (CurrentTarget.name.Contains("Limit"))
                            ObjectTarget = CurrentTarget.transform.parent.gameObject;
                        else
                            ObjectTarget = CurrentTarget;
                        TargetHPPanel.SetActive(false);
                        TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        AttackImage.sprite = AttackS;
                        break;
                    case "Mountain":
                        Get.gameObject.SetActive(false);
                        ObjectTarget = CurrentTarget;
                        if (GOD.TPUC.PlayerUp)
                        {
                            ObjectTarget.transform.parent.GetComponent<MountainScript>().RemovePlayer(ObjectTarget.name);
                        }
                        AttackImage.sprite = PickS;
                        break;
                    case "BigTree":
                        if (CurrentTarget.name.Contains("Stick"))
                            ObjectTarget = CurrentTarget.transform.parent.parent.gameObject;
                        else
                            ObjectTarget = CurrentTarget.transform.parent.gameObject;
                        if (ObjectTarget.GetComponent<ObjectScript>().HP > 0)
                        {
                            Get.gameObject.SetActive(false);
                            SetTargetHP("Object");
                            TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        }
                        AttackImage.sprite = AxeS;
                        break;
                    case "BreakIce":
                        ObjectTarget = CurrentTarget;
                        if (ObjectTarget.GetComponent<ObjectScript>().HP > 0)
                        {
                            Get.gameObject.SetActive(false);
                            SetTargetHP("Object");
                            TargetNameText.text = Localization.instance.getTextByKey(ObjectTarget.GetComponent<ObjectScript>().ObjectTag);
                        }
                        AttackImage.sprite = PickS;
                        break;
                    case "BridgeButton":
                        Get.gameObject.SetActive(false);
                        ObjectTarget = CurrentTarget;
                        TargetHPPanel.SetActive(false);
                        BridgeBtn b = ObjectTarget.GetComponent<BridgeBtn>();
                        if (b)
                        {
                            TargetNameText.text = Localization.instance.getTextByKey("#Name" + b.thisBridge.name);
                            if (GOD.QuestContr.IsInit && GOD.QuestContr.Find.Contains(b.thisBridge.name))
                                GOD.QuestContr.CheckAllQuests("find", b.thisBridge.name, 1);
                        }
                        else
                            TargetNameText.text = "";
                        AttackImage.sprite = AttackS;
                        break;
                    default:
                        Get.gameObject.SetActive(false);
                        ObjectTarget = null;
                        AttackImage.sprite = AttackS;
                        break;
                }
            }
        }
        else
        {
            // if(EnemyTarget == null)
            //  AttackEnemy.interactable = false;
            if (GOD.IsTutorial)//шаг назад в туторе
            {
                if (GOD.Tutor.TutorialStep == 4)
                {
                    GOD.Tutor.DoNotSendMentrica = true;
                    GOD.Tutor.TutorialStep = 3;
                    GOD.Tutor.ShowTutorialStepTurn();
                }
            }
            Get.gameObject.SetActive(false);
            TargetNameText.text = "";
            TargetHPPanel.SetActive(false);
            ObjectTarget = null;
            AttackImage.sprite = AttackS;

        }
    }

  
    public void CollectObject()                      // собираем с объектов
    {
        if (ObjectTarget)
        {
            switch (ObjectTarget.tag)
            {
                case "Tree":
                    Get.gameObject.SetActive(false);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Get);
                    ObjectScript OS = ObjectTarget.GetComponent<ObjectScript>();
                    HelpCollect(OS);

                    if (GOD.IsTutorial)
                        GOD.Tutor.NextStep = true;
                    OS.PlayAnimator("GetTree");

                    break;
                case "Bush":
                    Get.gameObject.SetActive(false);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Get);
                    OS = ObjectTarget.GetComponent<ObjectScript>();
                    HelpCollect(OS);
                    break;
                case "Water":
                    Get.gameObject.SetActive(false);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWater);
                    GOD.PlayerAnimation.CollectAnimation(true, ObjectTarget.tag);
                    for (int i = 0; i < GOD.PlayerContr.EqupmentSlots.Length; i++)
                    {
                        if (GOD.PlayerContr.EqupmentSlots[i].name == "RightArm")
                        {
                            if (GOD.PlayerContr.EqupmentSlots[i].transform.childCount > 0)
                            {
                                GOD.PlayerContr.EqupmentSlots[i].transform.GetChild(0).GetComponent<InfoScript>().SetWaterInBacket(4);
                                GOD.Save.SaveGame(); //сохранение набора воды в ведро
                            }
                        }
                    }
                    break;
                case "Door":
                    ObjectTarget.GetComponent<DoorScript>().Door();
                    break;
            }
        }

      

    }

    void HelpCollect(ObjectScript OS)
    {
        if (OS.CollectionResCount > 0)
        {
            string Name1 = "";
            string Name2 = "";
            int Count1 = 0;
            int Count2 = 0;
            bool CanAdd = false;

            Name1 = ObjectTarget.GetComponent<ObjectScript>().CollectionResName1;
            Count1 = ObjectTarget.GetComponent<ObjectScript>().CollectionResNumber1;

            if (OS.CollectionResCount > 1)
            {
                Name2 = ObjectTarget.GetComponent<ObjectScript>().CollectionResName2;
                Count2 = ObjectTarget.GetComponent<ObjectScript>().CollectionResNumber2;
                if (GOD.InventoryContr.CanAdd(Name1, Count1) && GOD.InventoryContr.CanAdd(Name2, Count2))
                    CanAdd = true;
            }
            else
            {
                if (GOD.InventoryContr.CanAdd(Name1, Count1))
                    CanAdd = true;
            }


            if (CanAdd)
            {
                GOD.Progress.SetAchivments(15);       //ачивка за сбор
                GOD.InventoryContr.AddToInventory(Name1, Count1);
                ShowNewItem(Name1, Count1);
                OS.CollectionResNumber1 = 0;
                if (OS.CollectionResCount > 1)
                {
                    GOD.InventoryContr.AddToInventory(Name2, Count2);
                    ShowNewItem(Name2, Count2);
                    OS.CollectionResNumber2 = 0;
                }
                OS.PlayAnimator("Vibration");
 

                if (!GOD.IsTutorial)
                {
                    ObjectScript o = ObjectTarget.GetComponent<ObjectScript>();
                    o.RecoveryTime = GOD.TG.ToSecond() + KD;
                    GOD.RecoveryResourses.Add(o);
                }
            }
            else
            {
                GOD.DialogS.ShowDialog("CantAdd");
            }
        }
    }
    //ПРОЧНОСТЬ__________________________________________________________________________________________
    public void ChangeDurability(GameObject g, int count = 1)   // вычитаем прочность
    {
        if (g)
        {
            InfoScript I = g.GetComponent<InfoScript>();
            if (I)
            {
                I.currentDurability-=count;
                if (I.DurabilityImage)
                {
                    I.DurabilityImage.fillAmount = (float)I.currentDurability / (float)I.durability;
                    if (I.SlotInGame)
                    {
                        I.InGame.Durability.fillAmount = I.DurabilityImage.fillAmount;  // меняем прочность в панели быстрого доступа
                    }
                    if (I.DurabilityImage.fillAmount <= 0)
                    {
                        GOD.InventoryContr.DeleteIcon(I.gameObject);
                        GOD.InventoryContr.NeedToShowOffer();
                        GOD.Audio.Sound.PlayOneShot(GOD.Audio.BreakItem);
                    }
                }
            }
            GOD.Save.SaveGame();//при изменения прочности
        }
    }
    public void ChangeArmorDurability()
    {
        for (int i = 0; i < GOD.PlayerContr.Clothes.Count; i++)
        {
            if (GOD.PlayerContr.Clothes[i])
            {
                //print(GOD.PlayerContr.Clothes[i]);
                InfoScript I = GOD.PlayerContr.Clothes[i].GetComponent<InfoScript>();
                if (I.DurabilityImage)
                {
                    I.currentDurability--;
                    I.DurabilityImage.fillAmount = (float)I.currentDurability / (float)I.durability;
                    if (I.SlotInGame)
                    {
                        I.InGame.Durability.fillAmount = I.DurabilityImage.fillAmount;  // меняем прочность в панели быстрого доступа
                    }
                    if (I.DurabilityImage.fillAmount <= 0)
                    {
                        GOD.InventoryContr.DeleteIcon(I.gameObject);
                    }
                }
            }
        }
    }
    //ГОДНОСТЬ___________________________________________________________________________________________________
    public void ChangeValidity(InfoScript I, int val)
    {
            I.currentValidity -= val*1;
        ShowValidity(I);
        if (I.currentValidity <= 0)
            {
                GOD.InventoryContr.DeleteIcon(I.gameObject);
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.BadFood);
            }

    }
    public void ShowValidity(InfoScript I)
    {
        I.ValidityImage.fillAmount = (float)I.currentValidity / 100;
        if (I.SlotInGame)
            I.InGame.Validity.fillAmount = I.ValidityImage.fillAmount;  // меняем годность в панели быстрого доступа
    }
    //___________________________________________________________________________________________________________
    public void ExtractObject()                      // добываем с объектов
    {
        if (ObjectTarget != null)
        {
            ObjectScript OS = ObjectTarget.GetComponent<ObjectScript>();
            float Damage = 0;
            bool NewItem = true;
            string s = ObjectTarget.tag;
            switch (s)
            {
                case "Tree":
                    Damage = GOD.PlayerContr.AxeSpeed;
                    if (!GOD.PlayerContr.OnHand || !GOD.PlayerContr.OnHand.name.Contains("Axe"))
                        GOD.DialogS.ShowDialog("UseAxe");
                    OS.PlayAnimator("CutTree");
                    break;
                case "BigTree":
                    Damage = GOD.PlayerContr.AxeSpeed;
                    if (!GOD.PlayerContr.OnHand || !GOD.PlayerContr.OnHand.name.Contains("Axe"))
                        GOD.DialogS.ShowDialog("UseAxe");
                    break;
                case "BreakIce":
                    Damage = GOD.PlayerContr.PickSpeed;
                    if (!GOD.PlayerContr.OnHand || !GOD.PlayerContr.OnHand.name.Contains("Pick"))
                        GOD.DialogS.ShowDialog("UsePick");
                    break;
                case "Stone":
                    Damage = GOD.PlayerContr.PickSpeed;
                    if (!GOD.PlayerContr.OnHand || !GOD.PlayerContr.OnHand.name.Contains("Pick"))
                        GOD.DialogS.ShowDialog("UsePick");
                    OS.PlayAnimator("Vibration");
                    break;
                case "Heap":
                    Damage = GOD.PlayerContr.ShovelSpeed;
                    NewItem = false;
                    OS.PlayAnimator("Vibration");
                    break;
            }
            if ((OS.HP - Damage) > 0)
            {
                if (s == "Heap")
                {
                    if (GOD.InventoryContr.CanAdd(OS.ExtractionResName1, OS.ExtractionResNumber1))
                    {
                        OS.HP = OS.HP - Damage;       // вычитаем хп
                        ChangeDurability(GOD.PlayerContr.OnHand);
                    }
                    else
                        GOD.DialogS.ShowDialog("CantAdd");
                }
                else
                {
                    OS.HP = OS.HP - Damage;       // вычитаем хп
                    ChangeDurability(GOD.PlayerContr.OnHand);
                }
                MakeTargetHP("Object");                                // отображаем хп
            }
            else
            {
                if (ObjectTarget.CompareTag("BigTree"))
                {
                    OS.HP = OS.HP - Damage;
                    GOD.EnviromentContr.CSand.IsTreeDown = true;
                    GOD.EnviromentContr.CSand.SetDownTree();

                    ObjectTarget.GetComponent<Animator>().enabled = true;
                    ObjectTarget.GetComponent<Animator>().Play("FallTree");
                    ObjectTarget.GetComponent<AudioSource>().Play();

                    TargetHPPanel.SetActive(false);
                    GOD.Save.SaveGame(false);//уронили дерево
                    GOD.Progress.SetAchivments(8);  //ачивка за большое дерево

                }
                else if (ObjectTarget.CompareTag("BreakIce"))
                {
                    OS.HP = OS.HP - Damage;
                    ObjectTarget.GetComponent<Animator>().enabled = true;
                    ObjectTarget.GetComponent<Animator>().Play("IceFall");
                    ObjectTarget.GetComponent<AudioSource>().Play();
                    GOD.EnviromentContr.CSnow.IsIceBreak = true;
                    GOD.EnviromentContr.CSnow.DopLimit.SetActive(false);
                    GOD.EnviromentContr.CSnow.DopLimit2.SetActive(false);
                    TargetHPPanel.SetActive(false);
                    GOD.Save.SaveGame(false);//уронили ледяную глыбу
                    GOD.Progress.SetAchivments(4);  //ачивка за ледяную глыбу
                    if (GOD.QuestContr.IsInit && GOD.QuestContr.Do.Contains("BreakIce"))
                        GOD.QuestContr.CheckAllQuests("do", "BreakIce", 1);

                }
                else if(ObjectTarget.name.Contains("TV_Statue"))
                  {
                      OS.HP = OS.HP - Damage;
                    MyAppodeal.Statue = ObjectTarget;
                    if (!MyAppodeal.showRewardedVideo("Statue"))//если видео загружено
                        GOD.Settings.OpenNoConnection(true);
                      ObjectTarget.SetActive(false);
                      ObjectScript o = ObjectTarget.GetComponent<ObjectScript>();
                      o.AddObjectToRecover();
                      o.RecoveryTime = GOD.TG.ToSecond() + KD;
                      GOD.RecoveryObject.Add(o);
                  }
                else
                {
                    if (ObjectTarget.CompareTag("Heap"))
                        GOD.Progress.SetAchivments(12);       //ачивка за выкапывание
                    if (ObjectTarget.CompareTag("Stone"))
                        GOD.Progress.SetAchivments(13);       //ачивка за руду
                    if (ObjectTarget.CompareTag("Tree"))
                        GOD.Progress.SetAchivments(14);       //ачивка за рубку
                   

                    if (GOD.RecoveryResourses.Contains(ObjectTarget.GetComponent<ObjectScript>()))
                        GOD.RecoveryResourses.Remove(ObjectTarget.GetComponent<ObjectScript>());
                    if (!GOD.IsTutorial)
                    {
                        ObjectScript o = ObjectTarget.GetComponent<ObjectScript>();
                        o.AddObjectToRecover();
                        o.RecoveryTime = GOD.TG.ToSecond() + KD;
                        GOD.RecoveryObject.Add(o);
                    }
                    ObjectTarget.SetActive(false);

                    if (!ObjectTarget.CompareTag("Heap"))
                    {
                        if (OS.MapUpdate != null && OS.mapUpdateRenderer!=null)
                            OS.MapUpdate.UpdateMapOnce(OS.mapUpdateRenderer);
                    }
                    if (NewItem)
                    {
                        for (int i = 0; i < OS.ExtractionResNumber1; i++)
                            Lut(ObjectTarget,OS.ExtractionResName1, Random.Range(-5, 5), Random.Range(-5, 5), false);
                        if (OS.ExtractionResCount > 1)
                        {
                            for (int i = 0; i < OS.ExtractionResNumber2; i++)
                                Lut(ObjectTarget,OS.ExtractionResName2, Random.Range(-5, 5), Random.Range(-5, 5), false);
                                Lut(ObjectTarget, OS.ExtractionResName2, Random.Range(-5, 5), Random.Range(-5, 5), false);
                        }
                    }
                    else
                    {
                        if (OS.CollectionResCount > 0)
                        {
                            GOD.InventoryContr.AddToInventory(ObjectTarget.GetComponent<ObjectScript>().CollectionResName1, ObjectTarget.GetComponent<ObjectScript>().CollectionResNumber1, false);
                            ShowNewItem(OS.CollectionResName1, OS.CollectionResNumber1);
                            OS.CollectionResNumber1 = 0;
                            if (OS.CollectionResCount > 1)
                            {
                                GOD.InventoryContr.AddToInventory(ObjectTarget.GetComponent<ObjectScript>().CollectionResName2, ObjectTarget.GetComponent<ObjectScript>().CollectionResNumber2);
                                ShowNewItem(OS.CollectionResName2, OS.CollectionResNumber2);
                                OS.CollectionResNumber2 = 0;
                            }
                        }

                    }
                    TargetHPPanel.SetActive(false);
                    SetTarget(null);

                    if (GOD.IsTutorial)
                        GOD.Tutor.NextStep = true;
                }
            }
            //}
        }
    }


    public void AddLuts(ObjectScript OS, Dictionary<string,int> Luts)
    {
        if (Luts.ContainsKey(OS.ExtractionResName1))
        {
            Luts[OS.ExtractionResName1] += OS.ExtractionResNumber1;
        }
        else
            Luts.Add(OS.ExtractionResName1, OS.ExtractionResNumber1);

        if (OS.ExtractionResCount > 1)
        {
            if (Luts.ContainsKey(OS.ExtractionResName2))
            {
                Luts[OS.ExtractionResName2] += OS.ExtractionResNumber2;
            }
            else
                Luts.Add(OS.ExtractionResName2, OS.ExtractionResNumber2);
        }
        if (OS.ExtractionResCount > 2)
        {
            if (Luts.ContainsKey(OS.ExtractionResName3))
            {
                Luts[OS.ExtractionResName3] += OS.ExtractionResNumber3;
            }
            else
                Luts.Add(OS.ExtractionResName3, OS.ExtractionResNumber3);
        }
    }

    //ПОКАЗЫВАЕМ ИТЕМЫ_______________________________________________________________________
    public void ShowNewItem(string itemname, int count)   // показываем новый итем
    {
        if (!NewItem.activeSelf || !NewItem2.activeSelf || !NewItem3.activeSelf)
        {
            var sp = GOD.InventoryContr.FindIcon(itemname);
            if (sp == null) return;

            if (!NewItem.activeSelf)
            {
                NewItem.GetComponent<Image>().sprite = sp;
                NewItem.transform.GetChild(0).GetComponent<Text>().text = "+ " + count;
                NewItem.SetActive(true);
                NewItemAnim.Play("GetItem");
            }
            else if (!NewItem2.activeSelf)
            {
                NewItem2.GetComponent<Image>().sprite = sp;
                NewItem2.transform.GetChild(0).GetComponent<Text>().text = "+ " + count;
                NewItem2.SetActive(true);
                NewItem2Anim.Play("GetItem2");
            }
            else if (!NewItem3.activeSelf)
            {
                NewItem3.GetComponent<Image>().sprite = sp;
                NewItem3.transform.GetChild(0).GetComponent<Text>().text = "+ " + count;
                NewItem3.SetActive(true);
                NewItem3Anim.Play("GetItem3");
            }
        }
        else
        {
            NewItemsName.Add(itemname);
            NewItemsCount.Add(count);
            if (NewItemsName.Count == 1)
                StartCoroutine(WaitNewItem());
        }

    }

    IEnumerator WaitNewItem()
    {
        while(NewItemsName.Count>0)
        {
            if(!NewItem.activeSelf || !NewItem2.activeSelf || !NewItem3.activeSelf)
            {
                ShowNewItem(NewItemsName[0], NewItemsCount[0]);
                NewItemsName.Remove(NewItemsName[0]);
                NewItemsCount.Remove(NewItemsCount[0]);
            }
            yield return new WaitForSecondsRealtime(1);
        }
    }
    public void ShowLoseItem(string name, int count, bool b)   // показываем исчезнувшие итемы
    {
        string itemname="";
        if (name.Contains("clone"))
        {
            for (int i = 5; i < name.Length; i++)
                itemname += name[i];
        }
        else
            itemname = name;

        string info = "";
        if (b)
            info = "испортилось";
        else
            info = "- " + count;

        if (!NewItem.activeSelf || !NewItem2.activeSelf || !NewItem3.activeSelf)
        {
            var sp = GOD.InventoryContr.Icons[itemname];
            if (!NewItem.activeSelf)
            {
                NewItem.GetComponent<Image>().sprite = sp;
                NewItem.transform.GetChild(0).GetComponent<Text>().text = info;
                NewItem.SetActive(true);
                NewItemAnim.Play("GetItem");
            }
            else if (!NewItem2.activeSelf)
            {
                NewItem2.GetComponent<Image>().sprite = sp;
                NewItem2.transform.GetChild(0).GetComponent<Text>().text = info;
                NewItem2.SetActive(true);
                NewItem2Anim.Play("GetItem2");
            }
            else if (!NewItem3.activeSelf)
            {
                NewItem3.GetComponent<Image>().sprite = sp;
                NewItem3.transform.GetChild(0).GetComponent<Text>().text = info;
                NewItem3.SetActive(true);
                NewItem3Anim.Play("GetItem3");
            }
        }
        else
        {
            OldItemsName.Add(itemname);
            OldItemsCount.Add(count);
            OldItemsBool.Add(b);
            if (OldItemsName.Count == 1)
                StartCoroutine(WaitOldItem());
        }
    }

    IEnumerator WaitOldItem()
    {
        while (OldItemsName.Count > 0)
        {
            if (!NewItem.activeSelf || !NewItem2.activeSelf || !NewItem3.activeSelf)
            {
                ShowLoseItem(OldItemsName[0], OldItemsCount[0], OldItemsBool[0]);
                OldItemsName.Remove(OldItemsName[0]);
               OldItemsCount.Remove(OldItemsCount[0]);
            }
            yield return new WaitForSeconds(1);
        }
    }
    //____________________________________________________________________________________

    public void CollectDown(bool State)            // включаем или выключаем уменьшение хп цели
    {
        if (ObjectTarget != null)
        {
            GOD.Player.transform.LookAt(ObjectTarget.transform);
            GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, GOD.Player.transform.rotation.eulerAngles.y, 0));
            if ((ObjectTarget.CompareTag("Fish")) && (GOD.PlayerContr.HaveRod))
            {
                GOD.FishingWindow.FishingSpeed = 1;
                GOD.FishingWindow.Fishing();
            }
            else
            {
                GOD.PlayerAnimation.CollectAnimation(State, ObjectTarget.tag);
            }
        }                                                                                                                   
    }


    //БОЙ_________________________________________________________________________________________________________________________

    public void FightDown()  // нажатие на кнопку атаки
    {
        if (AttackEnemy.interactable)
        {
            if (GOD.IsTutorial && GOD.Tutor.TutorialStep ==5)
                GOD.Tutor.StopTutorialBlack();
            {

            }
            if (GOD.PlayerAnimation.m_Animator.GetFloat("Speed") < 0)
            {
                if (ObjectTarget)
                {
                    if (ObjectTarget.CompareTag("Enemy"))
                        FightIsDown = true;
                    //GOD.PlayerAnimation.AttackAnimation();
                    else
                    {
                        bool Can = true;
                        if (ObjectTarget.CompareTag("Heap"))
                        {
                            if (GOD.PlayerAnimation.ItemOnHand)
                            {
                                if (!GOD.PlayerAnimation.ItemOnHand.name.Contains("Shovel"))
                                {
                                    Can = false;
                                    GOD.DialogS.ShowDialog("NeedShovel");
                                }
                            }
                            else
                            {
                                Can = false;
                                GOD.DialogS.ShowDialog("NeedShovel");
                            }
                        }
                        else if (ObjectTarget.CompareTag("Fish"))
                        {
                            if (GOD.PlayerAnimation.ItemOnHand)
                            {
                                if (!GOD.PlayerAnimation.ItemOnHand.name.Contains("Rod"))
                                {
                                    Can = false;
                                    GOD.DialogS.ShowDialog("NeedRod");
                                }
                            }
                            else
                            {
                                Can = false;
                                GOD.DialogS.ShowDialog("NeedRod");
                            }
                        }

                        if (ObjectTarget.CompareTag("Water"))
                            Can = false;

                        if (Can)//рубим,колим,копаем
                        {
                            GOD.Player.transform.LookAt(ObjectTarget.transform);
                            GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, GOD.Player.transform.rotation.eulerAngles.y, 0));
                            if ((ObjectTarget.CompareTag("Fish")) && (GOD.PlayerContr.HaveRod))
                            {
                                GOD.FishingWindow.FishingSpeed = 1;
                                GOD.FishingWindow.Fishing();
                            }
                            else
                            {
                                string s = ObjectTarget.tag;
                                if (ObjectTarget.CompareTag("GroundHouse") || ObjectTarget.CompareTag("House"))
                                    s = "Build";
                                FightDownTag = s;
                                FightIsDown = true;
                                // GOD.PlayerAnimation.CollectAnimation(true, s);
                            }
                        }
                    }
                }
                else
                    FightIsDown = true;
            }
        }
    }
    public void FightUp()
    {
        FightDownTag = "";
        FightIsDown = false;
    }
    /* public void HitEnemy()             // анимация врага, что его ударили
     {
         if(ObjectTarget != null && ObjectTarget.tag == "Enemy")
             ObjectTarget.GetComponent<Enemy>().EnemyHit();
     }*/

    public void DamageEnemy(float dopDamage, bool UseTarget)
    {
        if (UseTarget)
        {
            if(ObjectTarget && ObjectTarget.CompareTag("Enemy"))
                DamageOneEnemy(ObjectTarget.transform, dopDamage);
        }
        else
        {
            List<Transform> Enemies = GOD.AttackS.EnemiesAround;
            if (Enemies.Count > 0)
            {
                ChangeDurability(GOD.PlayerContr.OnHand);
                for (int i = 0; i < Enemies.Count; i++)
                    DamageOneEnemy(Enemies[i], dopDamage);
            }
        }
    }

    void DamageOneEnemy(Transform enemy, float dopDamage)
    {
        Enemy E = enemy.GetComponent<Enemy>();
        E.HP = E.HP - GOD.PlayerContr.Damage - dopDamage;
        E.GetDamage();
        if (ObjectTarget == enemy.gameObject)
            MakeTargetHP("Enemy");
        if (E.HP <= 0)
        {
            GOD.AttackS.EnemiesAround.Remove(E.transform);
            E.EnemyDeath();
            GOD.Progress.SaveMaxKill();
            if (GOD.QuestContr.IsInit && GOD.QuestContr.KillCount < 8)//количество видов животных
            {
                if (GOD.QuestContr.Enemies.Contains(E.name))
                    GOD.QuestContr.CheckAllQuests("kill", E.name, 1);
                GOD.QuestContr.AddNewQuest(GOD.QuestContr.GetQuestFromTag(E.name));
            }
            if (ObjectTarget == enemy)
                SetTarget(null);
        }
    }
    public void SetTargetHP(string Type)     // отображаем хп цели
    {
        TargetHPPanel.SetActive(true);
            if (ObjectTarget != null)
            {
                switch (Type)
                {
                    case "Object":
                        ObjectScript OS = ObjectTarget.GetComponent<ObjectScript>();
                        if (OS != null)
                            TargetHP.fillAmount = OS.HP / OS.maxHP;
                        break;
                    case "Enemy":
                        Enemy E = ObjectTarget.GetComponent<Enemy>();
                        if (E != null)
                            TargetHP.fillAmount = E.HP / E.maxHP;
                        break;
                }

                TargetOldHP.fillAmount = TargetHP.fillAmount;
        }
    }

    public void MakeTargetHP(string Type) // постепенно отображаем хп цели
    {
        if (ObjectTarget != null)
        {
            switch (Type)
            {
                case "Object":
                    ObjectScript OS = ObjectTarget.GetComponent<ObjectScript>();
                    if (OS != null)
                        TargetHP.fillAmount = OS.HP / OS.maxHP;
                    break;
                case "Enemy":
                    Enemy E = ObjectTarget.GetComponent<Enemy>();
                    if (E != null)
                        TargetHP.fillAmount = E.HP / E.maxHP;
                    break;
            }
            if (TargetHP.fillAmount > TargetOldHP.fillAmount)
                TargetOldHP.fillAmount = TargetHP.fillAmount;
            else
            {
                if (!NowChangeEnemyHP)
                    StartCoroutine(ChangeOldEnemyHP());
            }
        }
    }

    IEnumerator ChangeOldEnemyHP()
    {
        NowChangeEnemyHP = true;;
        while (TargetOldHP.fillAmount > TargetHP.fillAmount)
        {
            float x = Mathf.Abs(TargetOldHP.fillAmount - TargetHP.fillAmount) / 0.5f*Time.deltaTime;
            yield return new WaitForEndOfFrame();
            TargetOldHP.fillAmount -= x;
        }
        NowChangeEnemyHP = false;
    }



    public void Lut(GameObject Target, string LutName, float x, float z, bool IsEnemy, int LutCount=1)               // отображаем выпавший лут
    {
        if (Target != null)
        {
            if (IsEnemy)
            {
                GameObject EnemyLut = GOD.Pull.GetObject(LutName, Target.transform.position + new Vector3(x, 2, z), Target.transform.rotation);
                //  print(EnemyLut);
                if (EnemyLut != null)
                    StartCoroutine(AfterLutAnimation(EnemyLut));
            }
            else
            {
                GameObject TargetLut = GOD.Pull.GetObject(LutName, Target.transform.position + new Vector3(x, 2, z), Target.transform.rotation);
                if (TargetLut != null)
                {
                    TargetLut.GetComponent<LutScript>().LutCount = LutCount;
                    StartCoroutine(AfterLutAnimation(TargetLut));
                }

                if (GOD.IsTutorial)
                {
                    GameObject t = Instantiate(GOD.Tutor.TreeLeg);
                    t.transform.SetParent(TargetLut.transform);
                    t.SetActive(true);
                    t.transform.localPosition = new Vector3(0, 2, 0);
                    // t.AddComponent<LookAtPlayer>();
                    //  t.GetComponent<LookAtPlayer>().GOD = GOD;
                }
            }
        }
    }

    public void BuildLut(string LutName, Transform OldPosition, float x, float z, int LutCount = 1) // отображаем лут зданий
    {
        GameObject TargetLut = GOD.Pull.GetObject(LutName, OldPosition.position + new Vector3(x, 2, z), OldPosition.rotation);
            if (TargetLut != null)
            {
                TargetLut.GetComponent<LutScript>().LutCount = LutCount;
                StartCoroutine(AfterLutAnimation(TargetLut));
            }
    }

    IEnumerator AfterLutAnimation(GameObject Lut)             // включаем анимацию лута
    {
        yield return new WaitForSeconds(1);
       // if(Lut.name!="BagAfterDead")
           // Lut.tag = "Lut";
        //Lut.GetComponent<Rigidbody>().isKinematic = true;
       // Lut.GetComponent<BoxCollider>().isTrigger = true;
        Lut.GetComponent<Animator>().enabled = true;
       // Lut.layer = 0;
    }

    // ИНДИКАТОРЫ
    public void ShowIndicators()
    {
        HPpanel.fillAmount = GOD.PlayerContr.HP / GOD.PlayerContr.maxHP;
        HPblock.fillAmount = (1-GOD.PlayerContr.realmaxHP / GOD.PlayerContr.maxHP);
        EATpanel.fillAmount = GOD.PlayerContr.EAT / GOD.PlayerContr.maxEAT;

        InfoHP.fillAmount = HPpanel.fillAmount;
        InfoHPblock.fillAmount = HPblock.fillAmount;
        InfoEAT.fillAmount = EATpanel.fillAmount;

        if (HPpanel.fillAmount > HPoldPanel.fillAmount)
            HPoldPanel.fillAmount = HPpanel.fillAmount;
        else if(HPpanel.fillAmount>0)
        {
            if (!NowChangeHP)
                StartCoroutine(ChangeoldHP());
        }

        if (GOD.PlayerContr.HP <= 0)             //смерть персонажа
            GOD.PlayerContr.StartDeat();
    }

    IEnumerator ChangeoldHP()
    {
        NowChangeHP = true;
        while (HPoldPanel.fillAmount > HPpanel.fillAmount)
        {
            float x = Mathf.Abs(HPoldPanel.fillAmount - HPpanel.fillAmount) /0.1f*Time.deltaTime;
            yield return new WaitForSeconds(0.1f);
            HPoldPanel.fillAmount -= x;
        }
        NowChangeHP = false;
        
    }

    // ИНВЕНТАРЬ

    public void UpdateInventory()
    {
        GOD.InventoryContr.ShowIcons(GOD.InventoryContr.SlotInBag);
    }
    public void OpenInventory()       // открываем инвентарь
    {
        CloseInventoryImmediatly();
        GOD.GamePause();
        OpenVkladky1();
        Black.SetActive(true);
        Inventory.transform.localScale = Constants.Inventory;
        Inventory.SetActive(true);
        UpdateInventory();
        HideInfo();
        ShowPlayerInfo();

        if (InfoWindow.activeSelf)
            ShowInfo(CurrentShownObject);

        if (GOD.IsTutorial && (GOD.Tutor.TutorialStep == 10 || GOD.Tutor.TutorialStep ==19))
            GOD.Tutor.NextStep = true;
    }

    public void CloseInventoryImmediatly()
    {
        GOD.GamePlay();
        InventoryClose.Close();
        Black.SetActive(false);
        // Inventory.SetActive(false);
        CurrentShownObject = null;
    }
    public void CloseInventory()       // закрываем инвентарь
    {
        GOD.GamePlay();
        Black.SetActive(false);
        InventoryClose.StartClose();
       // Inventory.SetActive(false);
        CurrentShownObject = null;

        if (DragHandler.itemBeingDragged != null)
            DragHandler.itemBeingDragged.GetComponent<DragHandler>().EndDrag();

        if (GOD.IsTutorial)
        {
            if (GOD.Tutor.TutorialStep == 11)
            {
                GOD.Tutor.DoNotSendMentrica = true;
                    GOD.Tutor.TutorialStep = 10;
                GOD.Tutor.ShowTutorialStepTurn();
            }
            if (GOD.Tutor.TutorialStep == 20)
            {
                GOD.Tutor.DoNotSendMentrica = true;
                GOD.Tutor.TutorialStep = 19;
                GOD.Tutor.ShowTutorialStepTurn();
            }
        }
    }

    public void OpenVkladky1()
    {
        PlayerWindow.SetActive(true);
        InfoWindow.SetActive(false);
        ShowPlayerInfo();
    }

    public void OpenVkladky2()
    {
        if (!GOD.IsTutorial)
        {
            PlayerWindow.SetActive(false);
            InfoWindow.SetActive(true);

            if (CurrentShownObject == null)
                ShowInfo(null);
        }
    }

    public void ShowPlayerInfo()
    {
        InfoDayText.text = "";
        InfoPlayerText.text = "";
        InfoDayText.text = Localization.instance.getTextByKey("#Day") + " " + GOD.TG.IDay;
        InfoPlayerText.text += Localization.instance.getTextByKey("#RegenHP") + ": " + GOD.PlayerContr.RegenHPSpeed;
        InfoPlayerText.text += "\n" + Localization.instance.getTextByKey("#Attack") + ": " + GOD.PlayerContr.Damage;
        InfoPlayerText.text += "\n" + Localization.instance.getTextByKey("#Armor") + ": " + GOD.PlayerContr.Armor;
        InfoPlayerText.text += "\n" + Localization.instance.getTextByKey("#Speed") + ": " + GOD.PlayerContr.Speed;
    }

    public void ShowInfo(GameObject ShowObject)
    {
        if (ShowObject)
        {
            CurrentShownObject = ShowObject;
            Icon.gameObject.SetActive(true);
            Icon.GetComponent<Image>().sprite = ShowObject.GetComponent<Image>().sprite;
            InfoScript Info = ShowObject.GetComponent<InfoScript>();
            InfoNameText.text = Info.thisObjName;

            switch (Info.type)
            {
                case InfoScript.ItemType.food:
                case InfoScript.ItemType.poition:
                case InfoScript.ItemType.chest:
                case InfoScript.ItemType.building:
                    InfoText.text =  Info.fullDescription;
                    break;
                case InfoScript.ItemType.resources:
                    InfoText.text =  Info.fullDescription + "\n" + Info.whereDescription;
                    break;
                case InfoScript.ItemType.clothes:
                    if (Info.thisName.Contains("Mantle") || Info.thisName.Contains("Belt"))
                        InfoText.text = Info.fullDescription;
                    else
                    InfoText.text =  Localization.instance.getTextByKey("#Armor") + ": " + Info.armor + "\n" + Localization.instance.getTextByKey("#Durability") + ": " + Info.currentDurability + " / " + Info.durability + "\n" + Info.fullDescription;
                    break;
                case InfoScript.ItemType.rod:
                    InfoText.text =  Localization.instance.getTextByKey("#Durability") + ": " + Info.currentDurability + " / " + Info.durability + "\n" + Info.fullDescription;
                    break;
                case InfoScript.ItemType.instrument:
                    string speedtype = "";
                    switch (Info.instrumentSpeed)
                    {
                        case 10:
                            speedtype = Localization.instance.getTextByKey("#InstrumentSpeedVeryLow");
                            break;
                        case 9:
                            speedtype = Localization.instance.getTextByKey("#InstrumentSpeedLow");
                            break;
                        case 6:
                            speedtype = Localization.instance.getTextByKey("#InstrumentSpeedMedium");
                            break;
                        case 5:
                            speedtype = Localization.instance.getTextByKey("#InstrumentSpeedHight");
                            break;
                        case 4:
                            speedtype = Localization.instance.getTextByKey("#InstrumentSpeedVeryHight");
                            break;
                    }
                    InfoText.text = Localization.instance.getTextByKey("#InstrumentSpeed") + ": " + speedtype + "\n" + Localization.instance.getTextByKey("#Durability") + ": " + Info.currentDurability + " / " + Info.durability + "\n" + Info.fullDescription;
                    break;
                case InfoScript.ItemType.weapon:
                    InfoText.text =  Localization.instance.getTextByKey("#Attack") + ": " + Info.attack + "\n" + Localization.instance.getTextByKey("#Durability") + ": " + Info.currentDurability + " / " + Info.durability;
                    break;
            }
        }
        else
        {
            CurrentShownObject = null;
            Icon.gameObject.SetActive(false);
            InfoText.text = "";
            InfoNameText.text = "";
        }
    }

    public void HideInfo()
    {
        Icon.GetComponent<Image>().sprite = null;
    }

    //СУНДУКИ____________________________________________________________________________________
    public void OpenChestInventory(ChestScript Ch)
    {
        ChestWindow.transform.localScale = Constants.ChestWindow;
        ChestBlack.SetActive(true);
        ChestWindow.SetActive(true);
        GOD.InventoryContr.ShowIconsInChest(Ch);
    }
    public void CloseChestInventory()
    {
        //  ChestWindow.SetActive(false);
        if (DragHandler.itemBeingDragged != null)
            DragHandler.itemBeingDragged.GetComponent<DragHandler>().EndDrag();

        ChestBlack.SetActive(false);
        ChestInventoryClose.StartClose();
        GOD.InventoryContr.CurrentChest = null;
    }
    // КРАФТ_____________________________________________________________________________________

    public void OpenCraft()
    {
        GOD.CraftContr.OpenCraft();
        if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 7)
            GOD.Tutor.NextStep= true;
    }


    public void OpenBridgeCraft(Transform b) // крафт моста
    {
        BridgeCraftWindow.transform.localScale = Constants.BridgeCraftWindow;
        bridge = b;
        BridgeScript bs = bridge.GetComponent<BridgeScript>();
        int value1, value2;
        IRes1.sprite = GOD.InventoryContr.FindIcon(bs.ResName1);
        IRes1.transform.name = bs.ResName1;
        value1 =GOD.InventoryContr.InvnentoryGetCount(bs.ResName1);
        IRes2.transform.name = bs.ResName2;
        IRes2.sprite = GOD.InventoryContr.FindIcon(bs.ResName2);
        value2 = GOD.InventoryContr.InvnentoryGetCount(bs.ResName2);
        TRes1.text = "" + value1+ "/" + bs.ResCount1;
        TRes2.text = "" + value2 + "/" + bs.ResCount2;
        if ((bs.ResCount1 <= value1) && (bs.ResCount2 <= value2))
            BridgeCraftButton.interactable = true;
        else
            BridgeCraftButton.interactable = false;
        CraftBridgeName.text = Localization.instance.getTextByKey("#Name" + bs.name);
        BridgeCraftWindow.SetActive(true);
        Black.SetActive(true);
    }

    public void CraftBridge()
    {
        if(bridge!=null)
        {
            BridgeScript bs = bridge.GetComponent<BridgeScript>();
            GOD.InventoryContr.RemoveFromInventoryByName(bs.ResName1, bs.ResCount1);
            GOD.InventoryContr.RemoveFromInventoryByName(bs.ResName2, bs.ResCount2);
            BridgeCraftWindow.SetActive(false);
            Black.SetActive(false);
            bs.IsOpen = true;
            bs.SetResoures();
            Pathfinder.Instance.CreateMap();
            GOD.DopEff.Puff.transform.position = bs.ForPuffPosition.position;
            GOD.DopEff.Puff.transform.SetParent(null);
            GOD.DialogS.EnableDialogSphereBridge();
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.FishingSucsess);
            if (GOD.QuestContr.IsInit && GOD.QuestContr.Do.Contains(bs.name))
                GOD.QuestContr.CheckAllQuests("do", bs.name, 1);
        }
    }

    public void CloseBridgeCraft() // крафт моста
    {
        CraftBridgeClose.StartClose();
       // BridgeCraftWindow.SetActive(false);
        Black.SetActive(false);
    }

 
    public void CloseAll() // при нажатии на фон, закрывает все окна
    {
        GOD.GamePlay();
        if (GOD.CraftContr.CurrentWindow != null)
        {
            GOD.CraftContr.CurrentWindow.CloseCraft();
        }
        else if (Inventory.activeSelf)
        {
            CloseInventory();
        }
        else if (BridgeCraftWindow.activeSelf)
        {
            CloseBridgeCraft();
        }
        else if (ChangeWeatherWindow.activeSelf)
            CloseChangeWeather();
        else if (FirecampWindow.activeSelf)  //огонь
            CloseFirecampWindow();
        else if (GOD.TG.DayWindow.activeSelf)     //время
            GOD.TG.CloseDayWindow();
        else if (ChestWindow.activeSelf)   //сундук
            CloseChestInventory();
        else if (GOD.FishingWindow.FishingWindow.activeSelf) //рыбалка
            GOD.FishingWindow.Closefish();
        else if (ChangeWeatherWindow.activeSelf)  //погода
            CloseChangeWeather();
        else if (CantOpen.activeSelf)   //не хватает места
            CloseCantOpen();
        else if (GOD.ShopScript.Shop.activeSelf)//магазин
        {
            GOD.ShopScript.CloseShop();
            GOD.ShopScript.CloseGemWindow();
        }
        else if (GOD.ShopScript.GemWindow.activeSelf)//кристаллы
            GOD.ShopScript.CloseGemWindow();
        else if (BagAfterDead.activeSelf)//сумка после смерти
            CloseAfterDead();
        else if (GOD.Map.MapWindow.activeSelf)//карта
            GOD.Map.CloseMap();
        else if (GOD.Settings.SettingsWindow.activeSelf)//настройки
            GOD.Settings.CloseSettingsWindow();
        else if (GOD.QuestContr.QuestWindow.QuestWindow.activeSelf)
        {
            GOD.QuestContr.QuestWindow.CloseQuestWindow();
        }
        else if (GOD.Offers.OffersWindow.activeSelf)
            GOD.Offers.CloseOfferImmediatly();
        Black.SetActive(false);
    }

    //Покупка слота________________________________________________________________________________________________________________
    public void OpenBuySlot()
    {
        BuySlotWindow.transform.localScale = Constants.BuySlotWindow;
        BuySlotScript.ShowRes();
        BuySlotScript.ShowGold();
        BuySlotWindow.SetActive(true);
    }
    public void CloseBuySlot()
    {
        BuySlotClose.StartClose();
        //BuySlotWindow.SetActive(false);
    }
    //Недостаточно места________________________________________________________________________________________________________________
    public void OpenCantOpen()
    {
        CantOpen.transform.localScale = Constants.CantOpen;
        CantOpen.SetActive(true);
    }
    public void CloseCantOpen()
    {
        CantOpenClose.StartClose();
        //CantOpen.SetActive(false);
    }
    public void UpBag()
    {
        if(GOD.CraftContr.CurrentWindow!=null || GOD.AdminScript.AdminWindow.activeSelf || ChangeWeatherWindow.activeSelf)
        {
            if (GOD.CraftContr.CurrentWindow)
                GOD.CraftContr.CurrentWindow.CloseImmediatly();
            else if (ChangeWeatherWindow.activeSelf)
                CloseChangeWeatherImmidiatly();
            else
                GOD.AdminScript.CloseAdminWindowImmediatly();
            OpenInventory();
        }
        CantOpen.SetActive(false);
        OpenBuySlot();
    }
    //Желаете посмотреть рекламу__________________________________________________________________________________________________
   /* string CurrentIndicator = "HP";

    public void OpenWantToWatchAD(string s)
    {

            GOD.GamePause();
            CurrentIndicator = s;
            WantToWatchAD.SetActive(true);
    }
    public void CloseWantToWatchADActive()
    {
        GOD.GamePlay();
        WantToWatchAD.SetActive(false);
    }
    public void YesWantToWatchAD()
    {
        if (MyAppodeal.statusRewardedVideo())
        {
            if (MyAppodeal.showRewardedVideo())
            {
                if (CurrentIndicator == "EAT")
                {
                    EatPlus.SetActive(false);
                    GOD.PlayerContr.EAT = GOD.PlayerContr.maxEAT;
                    ShowIndicators();
                    MyAppMetrica.LogEvent("ADS", "Rewarded Video", "Add Food");
                }
                else
                {
                    HpPlus.SetActive(false);
                    GOD.PlayerContr.HP = GOD.PlayerContr.maxHP;
                    ShowIndicators();
                    MyAppMetrica.LogEvent("ADS", "Rewarded Video", "Add HP");
                }
                CloseWantToWatchADActive();
            }
            else
            {
                CloseWantToWatchADActive();
                GOD.Settings.OpenNoConnection(true);
            }
        }
        else
        {
            CloseWantToWatchADActive();
            GOD.Settings.OpenNoConnection(true);
        }
    }*/
    //КОCТЕР______________________________________________________________________________________________________________________
    public void OpenFirecampWindow()
    {
        if (CurrentFirecamp)
        {
            FirecampWindow.transform.localScale = Constants.FirecampWindow;
           FirecampScript currentFirecampScript = CurrentFirecamp.GetComponent<FirecampScript>();
            FireIcon.fillAmount = (float)currentFirecampScript.FireCampHP / 5f;
            if (currentFirecampScript.FireCampHP < 5 && GOD.InventoryContr.InvnentoryGetCount("Wood") > 0)
                AddWood.GetComponent<Selectable>().interactable = true;
            else
                AddWood.GetComponent<Selectable>().interactable = false;
            FirecampWindow.SetActive(true);
            Black.SetActive(true);
        }
    }
    public void SetWood()
    {
        if (CurrentFirecamp && GOD.InventoryContr.InvnentoryGetCount("Wood")>0)
        {
            GOD.InventoryContr.RemoveFromInventoryByName("Wood", 1);
            CurrentFirecamp.GetComponent<FirecampScript>().AddHp();
           // OpenFirecampWindow();
        }
    }
    public void CloseFirecampWindow()
    {
        FirecampClose.StartClose();
        //FirecampWindow.SetActive(false);
        Black.SetActive(false);
    }
#region Building  //СТРОИТЕЛЬСТВО_______________________________________________________________________________________________________________

    public void StartBuildingFromPanel(BuildPanelItem b)
    {
        if (b.thisCount > 0)
        {
            GOD.BuildingS.CloseDelete();
            GameObject g = GOD.InventoryContr.FindItem(b.transform.name);
            InfoScript i = g.GetComponent<InfoScript>();
            // StartBuilding(g.name, i);
           Build(g.name, i);
        }
    }
    public void StartBuilding(string n, InfoScript ing)
    {
        GOD.PlayerInter.MobileJoystik.GetComponent<PanelJoystick>().joystick.PointerUp();
      //  GOD.BuildingS.CloseDelete();
        ShowCountBuildingPanel();
        GOD.BuildingS.BuildingPanel.SetActive(true);
        GOD.BuildingS.BuildIcons.SetActive(true);
        GOD.BuildingS.Group.gameObject.SetActive(false);
        InventoryOnGame.gameObject.SetActive(false);
        GOD.QuestContr.QuestInGame.SetActive(false);
        MobileJoystik.SetActive(false);
        if (GOD.BuildingS.BuildingsChanged)
        {
            for (int i = 0; i < GOD.BuildingS.ChangedElemets.Count; i++)
            {
                if (GOD.BuildingS.ChangedElemets[i].name.Contains("Wall"))
                {
                    GOD.BuildingS.OnOffCollider(GOD.BuildingS.ChangedElemets[i], false);
                }
            }
        }
        GOD.PlayerContr.InBuilding = true;
        Build(n, ing);
    }
    public void Build(string n, InfoScript ing)
    {
        GOD.BuildingS.CreateBlock(n, ing);
        GOD.BuildingS.BuildButtons.SetActive(true);
        GOD.PlayerContr.PlayerZone.enabled = false;
    }
    public void RotateBuilding()
    {
        GOD.BuildingS.RotateBuilding(); 
    }
    public void AceptBuilding()
    {
        GameObject g = GOD.BuildingS.block;
        GOD.BuildingS.AceptBuilding();
        GOD.BuildingS.BuildButtons.SetActive(false);
        GOD.PlayerContr.PlayerZone.enabled = true;
        ShowCountBuildingPanel();
        GOD.PlayerContr.InPut = false;
    }
    public void CancelBuilding()
    {
        GOD.BuildingS.CancelBuilding();
        GOD.BuildingS.BuildButtons.SetActive(false);
        GOD.BuildingS.CloseDelete();
        GOD.PlayerContr.InPut = false;
    }

    public void DeleteBuild()
    {
        if (GOD.BuildingS.targetToDelete)
        {
            if (GOD.BuildingS.targetToDelete.name.Contains("Chest") && GOD.BuildingS.targetToDelete.GetComponent<ChestScript>().MyIcons.Count > 0)
                return;
            if(GOD.BuildingS.targetToDelete.CompareTag("Build"))
            {
                CraftBuilding c = GOD.BuildingS.targetToDelete.GetComponent<CraftBuilding>();
                if (c && c.NowCraft)
                    return;
            }
            GOD.BuildingS.CloseDelete();
            Dictionary<string, int> Luts = new Dictionary<string, int>();
            ObjectScript os = GOD.BuildingS.targetToDelete.GetComponent<ObjectScript>();
            Luts = GOD.BuildingS.DeleteBuilding(os);
            foreach (KeyValuePair<string, int> k in Luts)
            {
                BuildLut(k.Key, GOD.BuildingS.targetToDelete, Random.Range(-5, 5), Random.Range(-5, 5), k.Value);
            }
            if (GOD.BuildingS.ChangedElemets.Contains(GOD.BuildingS.targetToDelete))
                GOD.BuildingS.ChangedElemets.Remove(GOD.BuildingS.targetToDelete);
            if (GOD.NT.BuildingsAround.Contains(GOD.BuildingS.targetToDelete))
                GOD.NT.BuildingsAround.Remove(GOD.BuildingS.targetToDelete);
            if (GOD.BuildingS.targetToDelete.name == "Firecamp")
            {
                FirecampScript f = GOD.BuildingS.targetToDelete.GetComponent<FirecampScript>();
                if(f && GOD.NT.FirecаmpsAround.Contains(f.FirecampTransform))
                    GOD.NT.FirecаmpsAround.Remove(f.FirecampTransform);
            }
            Destroy(GOD.BuildingS.targetToDelete.gameObject);
            GOD.BuildingS.DeleteButtons.SetActive(false);
        }
    }

    public void CloseBuildings()
    {
        CancelBuilding();
        GOD.PlayerContr.PlayerZone.enabled = true;
        GOD.PlayerContr.InBuilding = false;
        GOD.BuildingS.BuildButtons.SetActive(false);
        GOD.BuildingS.BuildingPanel.SetActive(false);
        GOD.BuildingS.BuildIcons.SetActive(false);
        GOD.BuildingS.Group.gameObject.SetActive(true);
        GOD.BuildingS.CloseDelete();
        InventoryOnGame.gameObject.SetActive(true);
        GOD.QuestContr.QuestInGame.SetActive(true);
        MobileJoystik.SetActive(true);
        GOD.CameraScript.AfterBuildingCamera();
        if (GOD.NT.BuildingsAround.Count == 0)
            GOD.BuildingS.PlayerOut();
        if (GOD.BuildingS.BuildingsChanged)
        {
            for (int i = 0; i < GOD.BuildingS.ChangedElemets.Count; i++)
            {
                if (GOD.BuildingS.ChangedElemets[i].name.Contains("Wall"))
                {
                    GOD.BuildingS.OnOffCollider(GOD.BuildingS.ChangedElemets[i], true);
                }
            }
        }
    }
    void ShowCountBuildingPanel()
    {
        for(int i=0; i<GOD.BuildingS.Icons.Length;i++)
        {
            GOD.BuildingS.IconsText[i].SetTextCount(GOD.InventoryContr.InvnentoryGetCount(GOD.BuildingS.Icons[i].name));
        }
    }
#endregion
    #region Weather  //СМЕНА ПОГОДЫ_________________________________________________________________________________________________
    public void ChangeWeather()  // открываем окно смены погоды
    {
        if(ChangeWeatherWindow.activeSelf)
            CloseChangeWeatherImmidiatly();
        ChangeWeatherWindow.transform.localScale = Constants.ChangeWeatherWindow;
        if (GOD.TPC.CurrentUnder == "Ground" || GOD.TPC.CurrentUnderTag == "WaterPuddle")
        {
            UpdateWeatherButton();
            ChangeWeatherWindow.SetActive(true);
            Black.SetActive(true);
        }
        else
            GOD.DialogS.ShowDialog("CantChangeWeather");
        if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 13)
            GOD.Tutor.NextStep = true;

    }
    public void CloseChangeWeatherImmidiatly()   
    {
        ChangeWeatherClose.Close();
        Black.SetActive(false);
    }
    public void CloseChangeWeather()   //закрываем окно смены погоды
    {
        ChangeWeatherClose.StartClose();
        //ChangeWeatherWindow.SetActive(false);
        Black.SetActive(false);
    }
    public void ChangeWeatherTo(string s)
    {
        if (GOD.IsTutorial && (GOD.Tutor.TutorialStep == 14))
        {
            GOD.Tutor.TutorialStep = 15;
            GOD.Tutor.ShowTutorialStepTurn();
        }
        switch (s)
        {
            case "Snow":
                GOD.PI.WeatherStaff.GetComponent<Renderer>().material = GOD.PI.WeatherSnow;
                break;
            case "Sun":
                GOD.PI.WeatherStaff.GetComponent<Renderer>().material = GOD.PI.WeatherSun;
                break;
            case "Water":
                GOD.PI.WeatherStaff.GetComponent<Renderer>().material = GOD.PI.WeatherRain;
                break;

        }
        if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 14)
        {
            GOD.Tutor.TutorialStep = 15;
            GOD.Tutor.ShowTutorialStepTurn();
        }
        GOD.PlayerAnimation.HelpStartChangeWeather(s);
        CloseChangeWeather();

    }
    public void MinusIngridient(string s)
    {
        if (s == "Snow")
        {
            GOD.InventoryContr.RemoveFromInventoryByName("EssenceIce", 1);
            if(!GOD.IsTutorial)
                GOD.Progress.SetChangeWeather(1);     //смена погоды
        }
        else if (s == "Water")
        {
            GOD.InventoryContr.RemoveFromInventoryByName("EssenceWater", 1);
            GOD.Progress.SetChangeWeather(2);     //смена погоды
        }
        else if (s == "Sun")
        {
            GOD.InventoryContr.RemoveFromInventoryByName("EssenceSun", 1);
            GOD.Progress.SetChangeWeather(3);     //смена погоды
        }
        ShowIndicators();
    }

    public void UpdateWeatherButton()
    {
        ToSnow.interactable = true;
        ToSun.interactable = true;
        ToWater.interactable = true;

        int Ice = GOD.InventoryContr.InvnentoryGetCount("EssenceIce"); ;
        int Sun = GOD.InventoryContr.InvnentoryGetCount("EssenceSun");
        int Water = GOD.InventoryContr.InvnentoryGetCount("EssenceWater");

        IceCount.text = "" + Ice;
        SunCount.text = "" + Sun;
        WaterCount.text = "" + Water;

        if (!GOD.EnviromentContr.CanChange  || GOD.EnviromentContr.CurrentIsland == "GreenIsland")
        {
            ToSnow.interactable = false;
            ToSun.interactable = false;
            ToWater.interactable = false;
        }
        else
        {
            if(GOD.EnviromentContr.CurrentState == "Snow" || Ice==0)
                ToSnow.interactable = false;
             if (GOD.EnviromentContr.CurrentState == "Sun" || Sun == 0)
                ToSun.interactable = false;
            if (GOD.EnviromentContr.CurrentState == "Water" || Water == 0)
                ToWater.interactable = false;
        }
    } 

    public void CraftFromGame(string s)
    {
        if (BuildSettings.isFree)
            EssenceMinutTimer =60;
        else
            EssenceMinutTimer = 5;
        EssenceSecondTimer = 0;
        CraftEssence(s);
    }
    public void CraftEssence(string s)
    {
        if (BuildSettings.isFree && TimeWindow.activeSelf)  //пропуск ожидания
        {
            if (!MyAppodeal.showRewardedVideo("Essence"))//если видео загружено
               GOD.Settings.OpenNoConnection(true);
            return;
        }
        if (SunEs.activeSelf || WaterEs.activeSelf || SnowEs.activeSelf)  //забираем результат крафта
        {
            string Name ="";
                switch (CurrentEssence)
                {
                    case "Water":
                    Name = "EssenceWater";
                        break;
                    case "Sun":
                    Name = "EssenceSun";
                        break;
                    case "Snow":
                    Name = "EssenceIce";
                        break;
                }

            if (GOD.InventoryContr.CanAdd(Name, 1))
            {
                WaterBtn.interactable = true;
                SunBtn.interactable = true;
                SnowBtn.interactable = true;
                WaterEs.SetActive(false);
                SunEs.SetActive(false);
                SnowEs.SetActive(false);
                WaterTxt.text =  Localization.instance.getTextByKey("#Craft");
                SunTxt.text =  Localization.instance.getTextByKey("#Craft");
                SnowTxt.text =  Localization.instance.getTextByKey("#Craft");

                GOD.InventoryContr.AddToInventory(Name, 1);
                GOD.PlayerInter.ShowNewItem(Name, 1);
                UpdateWeatherButton();
                CurrentEssence = "";
            }
            else
                GOD.PlayerInter.OpenCantOpen();
        }
        else  //начало крафта
        {
            if (s != "")
            {
                CurrentEssence = s;
                TimeWindow.SetActive(true);
                Text CurrentTxt = null;
                Button CurrentButton = null;
                switch (CurrentEssence)
                {
                    case "Water":
                        TimeWindow.transform.position = WaterBtn.transform.position + new Vector3(0, TimeWindow.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
                        CurrentTxt = WaterTxt;
                        CurrentButton = WaterBtn;
                        break;
                    case "Sun":
                        TimeWindow.transform.position = SunBtn.transform.position + new Vector3(0, TimeWindow.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
                        CurrentTxt = SunTxt;
                        CurrentButton = SunBtn;
                        break;
                    case "Snow":
                        TimeWindow.transform.position = SnowBtn.transform.position + new Vector3(0, TimeWindow.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
                        CurrentTxt = SnowTxt;
                        CurrentButton = SnowBtn;
                        break;
                }

                WaterBtn.interactable = false;
                SunBtn.interactable = false;
                SnowBtn.interactable = false;
                TimeTxt.text = "" + EssenceMinutTimer + ":" + EssenceSecondTimer;
                if(BuildSettings.isFree)
                {
                    CurrentButton.interactable = true;
                    CurrentTxt.text = Localization.instance.getTextByKey("#Skip");
                }
                else
                CurrentTxt.text = Localization.instance.getTextByKey("#Get");

                StartCoroutine(StartEssenceTimer(CurrentButton, CurrentTxt));
            }
        }
    }

   IEnumerator StartEssenceTimer(Button CurrentButton, Text CurrentTxt)
    {
        while (EssenceMinutTimer > 0 || EssenceSecondTimer >0)
        {
            yield return new WaitForSeconds(1);
            if (EssenceSecondTimer == 0)
            {
                EssenceMinutTimer--;
                EssenceSecondTimer = 59;
            }
            else
                EssenceSecondTimer--;
            TimeTxt.text = "" + EssenceMinutTimer + ":" + EssenceSecondTimer;
        }
        CurrentButton.interactable = true;
        StopWaitEssence();
        if (SunEs.activeSelf || WaterEs.activeSelf || SnowEs.activeSelf)
            CurrentTxt.text = Localization.instance.getTextByKey("#Get");

    }

    public void StopWaitEssence()
    {
        TimeWindow.SetActive(false);
        switch (CurrentEssence)
        {
            case "Water":
                WaterEs.SetActive(true);
                break;
            case "Sun":
                SunEs.SetActive(true);
                break;
            case "Snow":
                SnowEs.SetActive(true);
                break;
        }
    }
#endregion

    //ЛУТ ПОСЛЕ СМЕРТИ_________________________________________________________________________________
    public void OpenAfterDead(GameObject g)
    {
        BagAfterDead.transform.localScale = Constants.BagAfterDead;
        BagAfterDeadBlack.SetActive(true);
        GOD.InventoryContr.ShowIconsInAfterDead(g);
        BagAfterDead.SetActive(true);
    }
    public void CloseAfterDead()
    {
        // GOD.InventoryContr.CloseAfterDead();
        if (DragHandler.itemBeingDragged != null)
            DragHandler.itemBeingDragged.GetComponent<DragHandler>().EndDrag();

        BagAfterDeadBlack.SetActive(false);
        GOD.InventoryContr.CurrentBag = null;
        BagAfterDeadClose.StartClose();
      //  BagAfterDead.SetActive(false);
    }

    //СМЕРТЬ___________________________________________________________________________________________ 
    public void OpenDead()
    {
        Head1.text = Localization.instance.getTextByKey("#Dead1");
        Head2.text = "" + GOD.TG.IDay;
        Head3.text = Localization.instance.getTextByKey("#Dead2");
        BtnName1.text = Localization.instance.getTextByKey("#Dead3");
        BtnDes1.text = Localization.instance.getTextByKey("#Dead5") + "\n" + Localization.instance.getTextByKey("#Dead6") + "\n" + Localization.instance.getTextByKey("#Dead7");
        BtnName2.text = Localization.instance.getTextByKey("#Dead4");
        BtnDes2.text = Localization.instance.getTextByKey("#Dead5") + "\n" + Localization.instance.getTextByKey("#Dead8");
        Btn1.text = Localization.instance.getTextByKey("#Dead9");
        Btn2.text = Localization.instance.getTextByKey("#Dead9");
        Dead.SetActive(true);
    }
    public void BeforeRessurection1()
    {
#if !NO_ADS
        if (!MyAppodeal.showRewardedVideo("Ressurection"))
            GOD.Settings.OpenNoConnection(true);
#else
        Ressurection1();
#endif
    }
    public void Ressuraction1()        //воскрешение на месте
    {

            MyAppMetrica.LogEvent("Game", "Ressurection", "Ressurection1");
            if (GOD.PlayerContr.realmaxHP > 10) //блокируем хп
                GOD.PlayerContr.realmaxHP -= ((GOD.PlayerContr.maxHP * 10) / 100);
            for (int i = 0; i < 5; i++)//снижаем прочность предметов
            {
                if (GOD.PlayerContr.EqupmentSlots[i].transform.childCount > 0)
                {
                    InfoScript I = GOD.PlayerContr.EqupmentSlots[i].transform.GetChild(0).GetComponent<InfoScript>();
                    if (I)
                        ChangeDurability(I.gameObject, ((I.durability * 10) / 100));
                }
            }
            if (GOD.PlayerContr.OnHand != null)
                ChangeDurability(GOD.PlayerContr.OnHand, ((GOD.PlayerContr.OnHand.GetComponent<InfoScript>().durability * 10) / 100));
            Ressurection();
      
    }


    void Ressurection()
    {
        GOD.GamePlay();
        GOD.PlayerContr.enabled = true;
        GOD.PlayerContr.IsAlive = true;
        GOD.PlayerContr.HP = GOD.PlayerContr.realmaxHP;
        GOD.PlayerContr.EAT = GOD.PlayerContr.maxEAT;
        Dead.SetActive(false);
        MobileJoystik.SetActive(true);
        GOD.PlayerInter.AllCanvas.SetActive(true);
        GOD.PlayerInter.InventoryOnGame.gameObject.SetActive(true);
        GOD.Player.GetComponent<CapsuleCollider>().enabled = true;
        GOD.Player.GetComponent<Rigidbody>().isKinematic = false;
        GOD.PlayerAnimation.enabled = true;
        GOD.PlayerAnimation.m_Animator.Play("Idle 1");
        GOD.PlayerCam.GetComponent<Grayscale>().enabled = false;
        GOD.CameraScript.ToAliveCamera();

        GOD.TG.TimeAfterDead();
        GOD.PlayerContr.Frost = 0;
        GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff", 1);

        GOD.EnviromentContr.SetTime();  // выставляем свет
        GOD.DopEff.Ressurection.transform.position = GOD.Player.transform.position + new Vector3(0, 2, 0);
        GOD.DopEff.Ressurection.transform.SetParent(null);
        StartCoroutine(StopRessurection());
        GOD.Save.SaveGame(false);//после восрешения на месте
    }


    public void StopRes()
    {
        StartCoroutine(StopRessurection());
    }
    IEnumerator StopRessurection()
    {
        yield return new WaitForSeconds(5);
        GOD.DopEff.Ressurection.transform.SetParent(GOD.DopEff.EffectsParent);
    }

    public void BeforeRessurection2()
    {
#if !NO_ADS
        //MyAppodeal.showInterstitial();
        Ressuraction2();
#else
        Ressurection2();
#endif
    }
    public void Ressuraction2()
    {
            MyAppMetrica.LogEvent("Game", "Ressurection", "Ressurection2");
            GOD.GamePlay();
            // GameObject g = AddNewBag(GOD.Player.transform.position);
            // AfterDeadBag a = g.GetComponent<AfterDeadBag>();
            List<InfoScript> MyIcons = new List<InfoScript>();
            for (int i = 0; i < GOD.InventoryContr.AllIcons.Count; i++)
            {
                if (!GOD.InventoryContr.AllIcons[i].NowOnPlayer && !GOD.InventoryContr.AllIcons[i].IsFromShop)     //не трогаем то, что надето на персонажа
                {
                    InfoScript I = GOD.InventoryContr.AllIcons[i];
                    if (I.SlotInGame)
                        I.InGame.OutIcon();
                    GOD.PlayerInter.ShowLoseItem("Icon_" + I.thisName, I.ResourceCount, false);
                    MyIcons.Add(I);
                    I.transform.SetParent(GOD.InventoryContr.IconsParent);
                }
            }
            if (MyIcons.Count > 0)
            {
                GameObject g = AddNewBag(GOD.Player.transform.position);
                AfterDeadBag a = g.GetComponent<AfterDeadBag>();
                a.MyIcons = MyIcons;
                for (int i = 0; i < MyIcons.Count; i++)
                {
                    MyIcons[i].InDeadBag = a;
                    GOD.InventoryContr.AllIcons.Remove(MyIcons[i]);
                }
                StartCoroutine(AfterLutAnimation(GOD.InventoryContr.AllBags[GOD.InventoryContr.AllBags.Count - 1]));
                StartCoroutine(ShowPlayerLut(true));
            }
            else
            {
                StartCoroutine(ShowPlayerLut(false));
            }
    }

    public void PlayerLutAnimation()
    {
        StartCoroutine(AfterLutAnimation(GOD.InventoryContr.AllBags[GOD.InventoryContr.AllBags.Count - 1]));
    }

    IEnumerator ShowPlayerLut(bool t)
    {
        Dead.SetActive(false);
        yield return new WaitForSeconds(2f);
        GOD.TG.StartTime();
        GOD.Player.transform.position = new Vector3(0, 0, -86);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -11, 0));
        GOD.CameraScript.TeleportCamera();
        GOD.PlayerInter.StopRes();
        Ressurection();
    }

    public GameObject AddNewBag(Vector3 p)
    {
        GameObject g = Instantiate(PlayerLut);
        GOD.InventoryContr.AllBags.Add(g);
        g.GetComponent<AfterDeadBag>().MyNumber = GOD.InventoryContr.AllBags.Count - 1;
        g.transform.SetParent(null); 
        g.name = "BagAfterDead";
        g.tag = "DeadLut";
        g.transform.position = p + new Vector3(0, 3, 0);
        g.SetActive(true);
        return g;
    }

    public void LostSomeLut()
    {
        List<InfoScript> InfoList = new List<InfoScript>();
        int lutCount = Random.Range(0, 5);
        for (int i = 0; i < lutCount; i++)
        {
            if (GOD.InventoryContr.AllIcons.Count > 0)
            {
                int lutNumber = Random.Range(0, GOD.InventoryContr.AllIcons.Count);
                InfoScript I = GOD.InventoryContr.AllIcons[lutNumber];
                if (!I.NowOnPlayer)
                {
                    if (I.SlotInGame)
                        I.InGame.OutIcon();
                    ShowLoseItem("Icon_" + I.thisName, I.ResourceCount, false);
                    I.transform.SetParent(GOD.InventoryContr.IconsParent);
                    InfoList.Add(I);
                    GOD.InventoryContr.AllIcons.Remove(I);
                }
            }
        }
        if (InfoList.Count > 0)
        {
            GameObject g = GOD.PlayerInter.AddNewBag(GOD.Player.transform.position);
            g.tag = "Untagged";
            g.GetComponent<Rigidbody>().AddForce(-GOD.Player.transform.forward*0.1f, ForceMode.Impulse);
            AfterDeadBag a = g.GetComponent<AfterDeadBag>();
            a.MyIcons = InfoList;
            a.StartStopFly();
        }
    }

    public void SetZaglushka(bool b)
    {
        if (b)
        {
            ZImage.sprite = Resources.Load<Sprite>("logo");
            Zaglushka.SetActive(true);
            Time.timeScale = 1;
            Invoke("AfterZaglushka", 1);
        }
        else
        {
            Zaglushka.SetActive(false);
            ZImage.sprite = null;
            Resources.UnloadUnusedAssets();
        }
    }

    void AfterZaglushka()
    {
        if (MyAppodeal.instance.isInit)
            MyAppodeal.instance.ShowOnAplication();
        SetZaglushka(false);
    }

}
