﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopIslandNameAnimation : MonoBehaviour {
    public void Stop()
    {
        gameObject.SetActive(false);
    }
}
