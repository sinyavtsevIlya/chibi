﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestWindowScript : MonoBehaviour {

    public GameManager GOD;
    public List<QuestsTemplate> CurrentQuests= new List<QuestsTemplate>();  //квесты активные у игрока
    // Квест в окне
    [Header("Квест в окне:")]
    public GameObject QuestWindow;
    public GameObject QuestPanel;  //окно с описание квеста
    public Text Quest_Zagolovok;
    public GameObject QuestInWindow_Table;//табличка прародитель
    public RectTransform QuestInWindow_TableParent; //панель для табличек\
    Dictionary<QuestsTemplate, QuestWindowTable> QuestTable = new Dictionary<QuestsTemplate, QuestWindowTable>(); //массив табличек
    QuestsTemplate CurrentOpenQuest = null;
    [Header("Окно квеста:")]
    public Image Progress;
    public Text QuestName, QuestDescription, StageName, StageDescription,PrizeText, GetText;
    public CloseWindow QuestClose;
    [Header("Детали этапа квеста:")]
    public RectTransform DetailsParent;
    public GameObject[] Details = new GameObject[5];
    public Text[] DetailsText = new Text[5];
    public Image[] DetailsImage = new Image[5];
    public GameObject DetailsWindow, PrizeWindow; //для кнопки призов 

    [Header("Квест выполнен:")]
    public Image[] AllPrizes = new Image[5]; //все награды
    public GameObject[] AllPrizesImage = new GameObject[5]; 
    public Text[] AllPrizesCount = new Text[5]; //все награды
    public Animator[] AllPrizesAnimators = new Animator[5]; //все награды
    public Animator QuestCompleteAnimator;
    public GameObject Quest, CompleteQuest; //окно с текстом и окно с завершенным квестом
    public Sprite NoPrize; // картинка которая ставится если не найдено изображение награды

    bool NowEnding = false; //переменная для отслеживания прерывания анимации завершения квеста

    public void InitLanguage()
    {
        Quest_Zagolovok.text = Localization.instance.getTextByKey("#Quests");
        GetText.text = Localization.instance.getTextByKey("#Get");
        foreach (QuestsTemplate q in QuestTable.Keys)
        {
            QuestTable[q].TableText.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_Name");
        }
    }

    public void AddQuest(QuestsTemplate q)
    {
        if (!CurrentQuests.Contains(q))
        {
            CurrentQuests.Add(q);
            GameObject g = Instantiate(QuestInWindow_Table) as GameObject;
            g.name = QuestInWindow_Table.name;
            QuestInWindow_TableParent.sizeDelta = new Vector2(QuestInWindow_TableParent.sizeDelta.x, QuestInWindow_TableParent.sizeDelta.y + 100);
            g.transform.SetParent(QuestInWindow_TableParent.transform);
            g.transform.SetAsFirstSibling();
            g.SetActive(true);
            g.transform.localPosition = Constants.VectorZero;
            g.transform.localScale = Constants.VectorOne;
            QuestWindowTable qwt = g.GetComponentInChildren<QuestWindowTable>();
            q.quest_table = qwt;
            qwt.TableIcon.sprite = q.icon;
            qwt.TableText.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_Name");
            qwt.thisQuest = q;
            QuestTable.Add(q,qwt);
        }
   
    }

    public void RemoveQuest()
    {
        if (CurrentOpenQuest!=null)
        {
            for(int i=0; i<CurrentOpenQuest.prizes.Length; i++)
            {
                GOD.InventoryContr.AddToInventory(CurrentOpenQuest.prizes[i].name, CurrentOpenQuest.prizes[i].count);
                GOD.PlayerInter.ShowNewItem(CurrentOpenQuest.prizes[i].name, CurrentOpenQuest.prizes[i].count);
            }
            if (QuestTable.ContainsKey(CurrentOpenQuest))
                QuestTable.Remove(CurrentOpenQuest);
            CurrentQuests.Remove(CurrentOpenQuest);
            Destroy(CurrentOpenQuest.quest_table.gameObject);
            QuestInWindow_TableParent.sizeDelta = new Vector2(QuestInWindow_TableParent.sizeDelta.x, QuestInWindow_TableParent.sizeDelta.y - 100);
            CurrentOpenQuest = null;
            if(!NowEnding)
                OpenQuestWindow();
        }
    }

    //КВЕСТ В ОКНЕ________________________________________________________________________________________________
    public void OpenQuestWindow()
    {
        GOD.PlayerInter.CloseAll();
        QuestWindow.transform.localScale = Constants.QuestWindow;
        GOD.PlayerInter.Black.SetActive(true);
        QuestWindow.SetActive(true);
        PrizeText.text = Localization.instance.getTextByKey("#Quest_Prize");
        if (CurrentQuests.Count > 0)
        {
            QuestPanel.SetActive(true);
            if (CurrentOpenQuest != null)
                OpenQuest(CurrentOpenQuest);
            else
                OpenQuest(CurrentQuests[0]);
        }
        else
            QuestPanel.SetActive(false);
        //QuestInWindow_TableParent.localPosition = new Vector3(0, QuestInWindow_TableParent.sizeDelta.y * 2, 0);
    }
    public void CloseQuestWindow()
    {
        GOD.PlayerInter.Black.SetActive(false);
        if (NowEnding)
        {
            EndQuest();
            CurrentOpenQuest = null;
        }
        QuestClose.StartClose();
       // QuestWindow.SetActive(false);
    }

    public void CloseQuestWindowImmidiatly()
    {
        GOD.PlayerInter.Black.SetActive(false);
        if (NowEnding)
        {
            EndQuest();
            CurrentOpenQuest = null;
        }
        QuestClose.Close();
    }

    public void OpenQuestFromTable(QuestWindowTable q)
    {
        if (NowEnding)
            EndQuest();
        OpenQuest(q.thisQuest);
    }
    public void OpenQuest(QuestsTemplate q)  //отображаем конкретный квест
    {
        if (q!=null)
        {
            if (CurrentOpenQuest != null)
                CurrentOpenQuest.quest_table.TableText.color = new Color(0, 0.28f, 0.36f, 1);
            CurrentOpenQuest = q;
            if (q.current_stage_count < q.stages_count)//если квест не завершен
                GOD.QuestContr.CheckQuestComplete(q); //на всякий случай этап проверяем
            StopAllCoroutines();
            q.quest_table.TableText.color = new Color(1, 1, 1, 1);
            //  Debug.Log(q.current_stage_count +" " + q.stages_count +" "+ q.stages[q.stages_count - 1].complete);
            if (q.current_stage_count == q.stages_count && q.stages[q.stages_count - 1].complete) //квест выполнен
            {
                ShowNewQuest(q);
                Progress.fillAmount = 1;
                Quest.SetActive(false);
                CompleteQuest.SetActive(true);
                PrizeWindow.SetActive(true);
                DetailsWindow.SetActive(false);
                QuestCompleteAnimator.enabled = true;
                StartCoroutine(DifferentAnimation(1));
                GOD.QuestContr.CloseNewQuestInGame(q);
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.QuestComplete);
            }
            else
            {
                Quest.SetActive(true);
                CompleteQuest.SetActive(false);
                QuestCompleteAnimator.enabled = false;
                for (int i = 0; i < AllPrizesAnimators.Length; i++)
                {
                    AllPrizesAnimators[i].enabled = false;
                    AllPrizesAnimators[i].transform.localPosition = new Vector3(0, 0, 0);
                }
                DetailsParent.sizeDelta = new Vector2(13, 0);
                PrizeWindow.SetActive(false);
                DetailsWindow.SetActive(true);
                ShowNewQuest(q);
                ShowProgress(q);
            }
            GOD.QuestContr.QuestDot("REMOVE", q);
        }
    }

    void ShowProgress(QuestsTemplate q)
    {
        float maxcount = 1;
        float currentcount = 0;
        float onestage = 1;
        int stage = q.current_stage_count - 1;
        if (q.stages[stage].complete)
            stage++;
            QuestsStage qs = q.stages[stage];
        for(int i=0;i<qs.details.Length;i++)
        {
            currentcount += qs.details[i].current_count;
        }
            onestage = (float)(1f / q.stages_count);
            maxcount = qs.stage_length;
        Progress.fillAmount = q.last_show_progress;
        //if (qs.last_show_completes_details<qs.completes_details)  //если этап выполнен а мы открыли окно квеста
                StartCoroutine(UpdateQuest(q, onestage, maxcount, currentcount, stage));
    }

    IEnumerator UpdateQuest(QuestsTemplate q, float onestage =1, float maxcount = 1, float currentcount = 0, int stage=0)
    {
        float new_fillamount = (onestage * (stage)) + (onestage / maxcount) * currentcount;
        while (Progress.fillAmount < new_fillamount)
        {
            Progress.fillAmount += new_fillamount * 0.01f;
            yield return new WaitForSeconds(0.01f);
        }

        q.last_show_progress = Progress.fillAmount;
    }

    void ShowNewQuest(QuestsTemplate q)
    {
        QuestName.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_Name");
        QuestDescription.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_Description");
        if (q.current_stage_count > q.stages_count) //квест завершен
        {
            StageName.text = "";
            StageDescription.text = "";
        }
        else
        {
            StageName.text = Localization.instance.getTextByKey("#Quest_Stage") + " " + q.current_stage_count + ". " + Localization.instance.getTextByKey("#Quest_" + q.tag + "_StageName_" + q.current_stage_count);
            StageDescription.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_StageDescription_" + q.current_stage_count);
            QuestsDetail[] qd = q.stages[q.current_stage_count - 1].details;
            for (int i = 0; i < Details.Length; i++)
            {
                if (i < qd.Length)
                {
                    string type = q.stages[q.current_stage_count - 1].type;
                    string x = "";
                    if (qd[i].complete)
                        x = "" + '✔';
                    else
                        x = "" + qd[i].current_count + "/" + qd[i].count;
                    string n = Localization.instance.getTextByKey("#Name" + qd[i].name);
                    Details[i].SetActive(true);
                    DetailsText[i].text = n + " " + x;
                    DetailsImage[i].sprite = qd[i].detail_image;
                    DetailsImage[i].name = qd[i].name;
                    DetailsParent.sizeDelta += new Vector2(0, 45);
                }
                else
                    Details[i].SetActive(false);
            }
        }
        if (q.prizes_image == null)
            q.prizes_image = new Sprite[q.prizes.Length];
        for(int i=0; i<AllPrizes.Length; i++)
        {
            if(i< q.prizes.Length)
            {
                AllPrizes[i].transform.parent.gameObject.SetActive(true);
                if (q.prizes_image[i] == null)
                {
                    Sprite t = GOD.InventoryContr.FindIcon(q.prizes[i].name);
                    if (t)
                        q.prizes_image[i] = t;
                    else
                        q.prizes_image[i] = NoPrize;
                }
                AllPrizes[i].sprite = q.prizes_image[i];
                AllPrizes[i].color = new Color(1, 1, 1, 1);
                AllPrizesCount[i].text = "" + q.prizes[i].count;
                AllPrizesImage[i].SetActive(true);
                AllPrizesCount[i].gameObject.SetActive(true);
            }
            else
                AllPrizes[i].transform.parent.gameObject.SetActive(false);
        }
    }

    public void ShowPrize(bool b)
    {
        PrizeWindow.SetActive(b);
        DetailsWindow.SetActive(!b);
    }

    public void Get() //выдача награды с квеста
    {
        //GOD.QuestContr.QuestInGame_Down();
        bool rezult = true;
        for(int i=0; i<CurrentOpenQuest.prizes.Length;i++)
        {
            rezult = rezult && GOD.InventoryContr.CanAdd(CurrentOpenQuest.prizes[i].name, CurrentOpenQuest.prizes[i].count);
        }
        if (rezult)
        {
            NowEnding = true;
            StartCoroutine(DifferentAnimation(2));
        }
        else
            GOD.PlayerInter.OpenCantOpen();
    }

    IEnumerator DifferentAnimation(int what)  //подергивание наград или их выдача
    {
        if (what == 1)
        {
            for (int i = 0; i < AllPrizes.Length; i++)
            {
                AllPrizes[i].color = new Color(1, 1, 1, 0);
                AllPrizesImage[i].SetActive(false);
                AllPrizesCount[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < AllPrizesAnimators.Length; i++)
        {
            float x = 0;
            if (what == 2)
            {
                AllPrizesAnimators[i].Play("QC_GetPrize");
                x = Random.Range(0.1f, 0.5f);
            }
            else
            {
                AllPrizesAnimators[i].enabled = true;
                AllPrizesAnimators[i].Play("QC_StartPrize");
                x = Random.Range(0.2f, 1f);
            }
            yield return new WaitForSeconds(x);
        }
        if(what==2)
        {
            yield return new WaitForSeconds(0.5f);
            EndQuest();
        }
    }

    void EndQuest()
    {
        if (CurrentOpenQuest != null)
        {
            StopAllCoroutines();
            CurrentOpenQuest.complete = true;
            CurrentOpenQuest.isactive = false;
            if (CurrentOpenQuest.next_quest_tag != "")
            {
                QuestsTemplate t = GOD.QuestContr.GetQuestFromTag(CurrentOpenQuest.next_quest_tag);
                GOD.QuestContr.ShowNewQuest(t);  //вызываем следующи квест если есть
            }
            RemoveQuest();
            CurrentOpenQuest = null;
            NowEnding = false;
        }
    }
}
