using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using System.IO;

using UnityEngine;

[XmlType("quests")]
public class QuestCollection
{
    [XmlElement("quest")]
    public QuestsTemplate[] QuestsTemplate;
    // ***********************************************************************

    #region Load

    public static QuestCollection LoadFromTextAsset(string path)
    {
        TextAsset loadtext = Resources.Load<TextAsset>(path);
        return LoadFromText(loadtext.text);
    }

    public static QuestCollection LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(QuestCollection), new XmlRootAttribute("quests"));
        return serializer.Deserialize(new StringReader(text)) as QuestCollection;
    }
    #endregion

    // ***********************************************************************
}