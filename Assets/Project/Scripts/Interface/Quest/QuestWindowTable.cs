﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestWindowTable : MonoBehaviour {

    public Image TableIcon;
    public Text TableText;
    public QuestsTemplate thisQuest;
    public GameObject Dot;

}
