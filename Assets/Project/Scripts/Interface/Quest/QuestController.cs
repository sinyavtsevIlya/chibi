﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestController : MonoBehaviour {

    public GameManager GOD;
    public bool IsInit = false;

    public QuestCollection AllQuests;
    public QuestsTemplate Firecamp, LeatherBelt, WebMantle, ChitinMantle, WoolMantle, Stone_Instruments;
    public QuestsTemplate StoneSword, Leather_Armor, House, Craftsman, FirstBridge, SnowBiom, RainBiom, SandBiom, Puzzle;
    public int KillCount = 0; //сколько квестов на убийство выполнено(костыль)

    public Dictionary<string, int> AddToInventory = new Dictionary<string, int>(); //элементы, которые должны быть добавленны в инвентарь для выполнения квеста
    public Dictionary<string, int> Build = new Dictionary<string, int>(); //элементы, которые должны быть построенны для выполнения квеста
    public List<string> Clothes = new List<string>(); //элементы, которые должны быть надеты для выполнения квеста
    public List<string> Enemies = new List<string>(); //существа, которых надо убить
    public List<string> Find = new List<string>(); //объекты, которые надо найти
    public List<string> Do = new List<string>(); //объекты, с которыми надо что то сделать

    public List<QuestsTemplate> QuestsTurn; //квесты, ожидающие показа

    [Header("Точка:")]
    public int AllDots = 0;//сколько непрочитанных квестов
    public GameObject Dot;
    Animator DotAnimator;
    // Квест в игре
    [Header("Квест в игре:")]
    public GameObject QuestInGame;
    public Animator QuestInGameAnimator;
    public Text QuestInGame_Accept, QuestInGame_Name, QuestInGame_Description;
    public Image QuestInGame_Image;
    QuestsTemplate CurrentQuest = null; //какой сейчас квест отображается в игре
    bool IsPlayerOk = false; //принял ли игрок квест
    // Квест в окне
    [Header("Квест в окне:")]
    public QuestWindowScript QuestWindow;
    public Sprite QuestImage; //иконка которую подставляем в квест, если не найдена иконка

    public void Init()
    {
        AllQuests = QuestCollection.LoadFromTextAsset("xml/quests");
        InitLanguage();
        Firecamp = GetQuestFromTag("Firecamp");
       LeatherBelt = GetQuestFromTag("LeatherBelt");
        WebMantle = GetQuestFromTag("WebMantle");
        ChitinMantle = GetQuestFromTag("ChitinMantle");
        WoolMantle = GetQuestFromTag("WoolMantle");
        Stone_Instruments = GetQuestFromTag("Stone_Instruments");
        StoneSword = GetQuestFromTag("StoneSword");
        Leather_Armor = GetQuestFromTag("Leather_Armor");
        House = GetQuestFromTag("House");
        Craftsman = GetQuestFromTag("Craftsman");
        FirstBridge = GetQuestFromTag("FirstBridge");
        SnowBiom = GetQuestFromTag("SnowBiom");
        RainBiom = GetQuestFromTag("RainBiom");
        SandBiom = GetQuestFromTag("SandBiom");
        Puzzle = GetQuestFromTag("Puzzle");
        DotAnimator = Dot.GetComponent<Animator>();

        
    }

    public void InitLanguage()
    {
        QuestInGame_Accept.text = Localization.instance.getTextByKey("#Accept");
        QuestWindow.InitLanguage();
    }

    public QuestsTemplate GetQuestFromTag(string tag)   //возвращаем квест по названию
    {
        for (int i = 0; i < AllQuests.QuestsTemplate.Length; i++)
        {
            if (tag == AllQuests.QuestsTemplate[i].tag)
                return AllQuests.QuestsTemplate[i];
        }
        return null;
    }


    //КВЕСТ В ИГРЕ________________________________________________________________________________________________
    public void QuestInGame_Down()//нажали на окно квеста
    {
        if (CurrentQuest != null)
        {
            if (CurrentQuest.current_stage_count == -1)
            {
                CurrentQuest.current_stage_count = 1;
                CloseNewQuestInGame(CurrentQuest);
                //  ShowQuestInGame(CurrentQuest);
            }
            else if (CurrentQuest.current_stage_count == CurrentQuest.stages_count && CurrentQuest.stages[CurrentQuest.stages_count - 1].complete) //квест выполнен
            {
                CurrentQuest.lastshown = true;
               // QuestWindow.QuestWindow.SetActive(true);
                QuestWindow.OpenQuestWindow();
                QuestWindow.OpenQuest(CurrentQuest);
                CloseNewQuestInGame(CurrentQuest);
            }
           /* else
            {
                CurrentQuest.current_stage_count++;
                CloseNewQuestInGame(CurrentQuest);
            }*/
        }
        CloseQuestTable();
    }
	public void ShowQuestInGame(QuestsTemplate q)  //показываем новый квест в игре
    {
        if (q!=null)
        {
            CurrentQuest = q;
            if (q.current_stage_count == -1) //первый показ квеста
            {
                QuestInGame_Name.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_Name");
                QuestInGame_Description.text = Localization.instance.getTextByKey("#Quest_" + q.tag + "_Description");
                InitQuest(q);
            }
            else if (q.current_stage_count==q.stages_count && q.stages[q.stages_count-1].complete) //квест выполнен
            {
                QuestInGame_Name.text = Localization.instance.getTextByKey("#Quest_Quest") + " \"" + Localization.instance.getTextByKey("#Quest_" + q.tag + "_Name") + "\" "+ Localization.instance.getTextByKey("#Quest_Completed");
                QuestInGame_Description.text = Localization.instance.getTextByKey("#Quest_GetPrize");
            }
            QuestInGame_Image.sprite = q.icon;
            QuestInGame.SetActive(true);
            QuestInGameAnimator.Play("OpenQuest");
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.NewQuest);
        }
    }

    public void InitQuest(QuestsTemplate q)
    {
        if (q != null)
        {
            if (q.icon_tag == "load")
            {
                Sprite sprite = Resources.Load<Sprite>(q.icon_name);
                if (sprite != null)
                    q.icon = sprite;
                else
                    q.icon = QuestImage;
            }
            else
            {
                Sprite t = GOD.InventoryContr.FindIcon(q.icon_name);
                if (t)
                    q.icon = t;
                else
                    q.icon = QuestImage;
            }
                for (int i = 0; i < q.stages.Length; i++)
                {
                    if (q.stages[i].recipe_name != "")//если нужно использовать рецепт
                    {
                        if (q.stages[i].recipe_name == "Bridge")//исключение для мостов
                        {
                            q.stages[i].details = new QuestsDetail[2];
                            BridgeScript b;
                            switch (q.tag)
                            {
                                case "SecondBridge":
                                    b = GOD.ToRain;
                                    break;
                                case "ThirdBridge":
                                    b = GOD.ToSand;
                                    break;
                                default:
                                    b = GOD.ToSnow;
                                    break;
                            }
                            QuestsDetail qd1 = new QuestsDetail();
                            qd1.name = b.ResName1;
                            qd1.count = b.ResCount1;
                            q.stages[i].details[0] = qd1;
                            QuestsDetail qd2 = new QuestsDetail();
                            qd2.name = b.ResName2;
                            qd2.count = b.ResCount2;
                            q.stages[i].details[1] = qd2;
                        }
                        else
                        {
                            RecipeScript r = GOD.CraftContr.GetRecipeByName(q.stages[i].recipe_name);
                            if (r != null)
                            {
                                q.stages[i].details = new QuestsDetail[r.NameOfIngridient.Length];
                                for (int x = 0; x < q.stages[i].details.Length; x++)
                                {
                                    QuestsDetail d = new QuestsDetail();
                                    d.name = r.NameOfIngridient[x];
                                    d.count = r.NumberOfIngridient[x];
                                    q.stages[i].details[x] = d;
                                }
                            }
                        }
                    }
                    for (int x = 0; x < q.stages[i].details.Length; x++)  //считаем на сколько будем бить прогресс и ищем иконки деталей
                    {
                        q.stages[i].stage_length += q.stages[i].details[x].count;
                    Sprite d = GOD.InventoryContr.FindIcon(q.stages[i].details[x].name);
                    if (d)
                        q.stages[i].details[x].detail_image = d;
                    else
                        q.stages[i].details[x].detail_image = q.icon;
                }
                }
            }
        }

    public void AddQuestToCurrentQuests(QuestsTemplate q)
    {
        if (q != null)
        {
            QuestWindow.AddQuest(q);
            q.isactive = true;
            if ((q.current_stage_count >= q.stages_count && q.stages[q.stages_count - 1].complete) || q.current_stage_count<0)
            {

            }
            else
            {
                string type = q.stages[q.current_stage_count - 1].type;
                switch (type)
                {
                    case "build": //будем ожидать установку этого предмета
                        QuestsDetail[] qd = q.stages[q.current_stage_count - 1].details;
                        for (int i = 0; i < qd.Length; i++)
                        {
                            if (Build.ContainsKey(qd[i].name))
                                Build[qd[i].name]++;
                            else
                                Build.Add(qd[i].name, 1);
                        }
                        break;
                    case "get":  //будем ожидать добавления этого итема в инвентарь
                        if (CheckQuestComplete(q)) //если ресурсы уже есть
                        {
                            // q.stages[q.current_stage_count - 1].complete = true;
                            // ShowNewQuest(q);
                        }
                        else
                        {
                            qd = q.stages[q.current_stage_count - 1].details;
                            for (int i = 0; i < qd.Length; i++)
                            {
                                if (AddToInventory.ContainsKey(qd[i].name))
                                    AddToInventory[qd[i].name]++;
                                else
                                    AddToInventory.Add(qd[i].name, 1);
                            }
                        }
                        break;
                    case "use": //будем ожидать надевание этого предмета
                        qd = q.stages[q.current_stage_count - 1].details;
                        for (int i = 0; i < qd.Length; i++)
                        {
                            Clothes.Add(qd[i].name);
                        }
                        break;
                    case "kill": //будем ожидать убийства
                        qd = q.stages[q.current_stage_count - 1].details;
                        for (int i = 0; i < qd.Length; i++)
                        {
                            Enemies.Add(qd[i].name);
                            if (qd[i].current_count==0)
                            qd[i].current_count = 1;
                        }
                        break;
                    case "find": //будем ожидать нахождения
                        if (q.tag.Contains("Biom"))
                        {
                            if (CheckQuestComplete(q, GOD.EnviromentContr.CurrentIsland, 1))
                            {
                                q.stages[q.current_stage_count - 1].complete = true;
                                //ShowNewQuest(q);
                            }
                        }
                        qd = q.stages[q.current_stage_count - 1].details;
                        for (int i = 0; i < qd.Length; i++)
                        {
                            Find.Add(qd[i].name);
                        }
                        break;
                    case "do": //будем ожидать действий
                        if (CheckQuestComplete(q)) //если уже выполнено
                        {
                            q.stages[q.current_stage_count - 1].complete = true;
                            //ShowNewQuest(q);
                        }
                        else
                        {
                            qd = q.stages[q.current_stage_count - 1].details;
                            for (int i = 0; i < qd.Length; i++)
                            {
                                Do.Add(qd[i].name);
                            }
                        }
                        break;
                }
            }
        }
    }
    public void CloseNewQuestInGame(QuestsTemplate q)  //закрываем новый квест в игре
    {
        if (q != null)
        {
            if (QuestsTurn.Contains(q))
            {
                QuestsTurn.Remove(q);
                AddQuestToCurrentQuests(q);
                QuestDot("ADD", CurrentQuest);
                if (CurrentQuest == q)
                {
                    IsPlayerOk = true;
                    CurrentQuest = null;
                }
            }
        }
    }
    void CloseQuestTable()
    {
        QuestInGameAnimator.Play("CloseQuest");
        StartCoroutine(CloseNewQuestInGame_End());
    }
    IEnumerator CloseNewQuestInGame_End()
    {
        yield return new WaitForSeconds(1f);
        if(QuestInGameAnimator.GetCurrentAnimatorStateInfo(0).IsName("Close"))
            QuestInGame.SetActive(false);
    }

    public void AddNewQuest(QuestsTemplate q) //активация квеста из игры
    {
        if (!q.complete && !QuestWindow.CurrentQuests.Contains(q))
            ShowNewQuest(q);
    }
    //очередь показа квестов
    public void ShowNewQuest(QuestsTemplate q) //показываем новый квест
    {
        if (!GOD.IsTutorial)
        {
          //  QuestsTemplate q = GetQuestFromTag(tag);
            if (q!= null  && !QuestsTurn.Contains(q) && !q.lastshown)
            {
                QuestsTurn.Add(q);
                if (QuestsTurn.Count == 1)
                    StartCoroutine(Turn());
            }
        }
    }
   /* public void ShowNewQuest(QuestsTemplate q) //показываем новый квест
    {
        if (q != null && !QuestsTurn.Contains(q))
            QuestsTurn.Add(q);
        if (QuestsTurn.Count == 1)
            StartCoroutine(Turn());
    }*/
    IEnumerator Turn()
    {
        while (QuestsTurn.Count > 0)
        {
            ShowQuestInGame(QuestsTurn[0]);
            IsPlayerOk = false;
            while (!IsPlayerOk)
            {
                yield return new WaitForSeconds(1.5f);
            }
        }
    }
    // ___________________________________________________________________________________________________________

    //ПРОВЕРКИ ВЫПОЛНЕНОСТИ КВЕСТОВ____________________________________________________________________________________

    public void CheckAllQuests(string questtype, string itemname, int itemcount)
    {
        for(int i=0; i < QuestWindow.CurrentQuests.Count; i++)
        {
            QuestsStage qs = QuestWindow.CurrentQuests[i].stages[QuestWindow.CurrentQuests[i].current_stage_count - 1];
            if (questtype == qs.type)
                CheckQuestComplete(QuestWindow.CurrentQuests[i], itemname,itemcount);
        }
    }
    public bool CheckQuestComplete(QuestsTemplate q, string itemname = "NO", int itemcount = 0)
    {
            QuestsStage qs = q.stages[q.current_stage_count - 1];
            QuestsDetail[] qd = qs.details;
            for (int i = 0; i < qd.Length; i++)
            {
                if (itemname == qd[i].name || itemname == "NO")//проверка на добавление итема
                {
                    if (qs.type == "get")//различия в проверках
                        qd[i].current_count = GOD.InventoryContr.InvnentoryGetCount(qd[i].name);
                    else if (qs.type == "do")
                    {
                        if (q.tag.Contains("Bridge"))
                        {
                            switch (q.tag)
                            {
                                case "FirstBridge":
                                    if (GOD.ToSnow.IsOpen)
                                        qd[i].current_count++;
                                    break;
                                case "SecondBridge":
                                    if (GOD.ToRain.IsOpen)
                                        qd[i].current_count++;
                                    break;
                                case "ThirdBridge":
                                    if (GOD.ToSand.IsOpen)
                                        qd[i].current_count++;
                                    break;
                            }
                        }
                        else if ((q.current_stage_count - 1) == i && q.tag == "Puzzle")
                        {
                            if (GOD.EnviromentContr.CurrentState == qd[i].name)
                                qd[i].current_count++;
                        }
                        else
                            qd[i].current_count += itemcount;
                    }
                    else
                        qd[i].current_count += itemcount;
                    if (!qd[i].complete && qd[i].current_count >= qd[i].count)//если детали выполнилась
                    {
                        qs.completes_details++;
                        qd[i].complete = true;
                        switch (qs.type)
                        {
                            case "get":
                                if (AddToInventory.ContainsKey(qd[i].name))
                                {
                                    int z = 0;
                                    AddToInventory.TryGetValue(qd[i].name, out z);
                                    if (z == 0)
                                        AddToInventory.Remove(qd[i].name);
                                    else
                                        AddToInventory[qd[i].name]--;
                                }
                                break;
                            case "build":
                                if (Build.ContainsKey(qd[i].name))  //убираем ожидание пострйоки этого предмета
                                {
                                    int z = 0;
                                    Build.TryGetValue(qd[i].name, out z);
                                    if (z == 0)
                                        Build.Remove(qd[i].name);
                                    else
                                        Build[qd[i].name]--;
                                }
                                break;
                            case "use":
                                Clothes.Remove(qd[i].name);
                                break;
                            case "kill":
                                Enemies.Remove(qd[i].name);
                                KillCount++;
                                break;
                            case "find":
                                Find.Remove(qd[i].name);
                                break;
                            case "do":
                                Do.Remove(qd[i].name);
                                break;
                        }
                    }
                }
            }
            if (qs.completes_details >= qs.details.Length)//этап завершен
            {
                qs.complete = true;
                if (q.current_stage_count >= q.stages_count && q.stages[q.current_stage_count - 1].complete)
                    ShowNewQuest(q);
                else
                {
                    q.current_stage_count++;
                    QuestDot("ADD", q);
                q.quest_table.transform.SetAsFirstSibling();
                    GOD.QuestContr.AddQuestToCurrentQuests(q);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.StageComplete);
                }
                return true;
            }
            return false;
    }

    public void QuestDot(string s, QuestsTemplate q)
    {
        if (q.quest_table)
        {
            if (s == "ADD")
            {
                if (!q.quest_table.Dot.activeSelf)
                {
                    q.quest_table.Dot.SetActive(true);
                    AllDots++;
                    GOD.QuestContr.Dot.SetActive(true);
                }
                GOD.QuestContr.DotAnimator.Play("ExclamentionMark");
            }
            else if (s == "REMOVE")
            {
                if (q.quest_table.Dot.activeSelf)
                {
                    q.quest_table.Dot.SetActive(false);
                    AllDots--;
                    if (AllDots <= 0)
                        Dot.SetActive(false);
                }
            }
        }
    }
}
