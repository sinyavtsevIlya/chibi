using System;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

[Serializable]
[XmlType("quest")]
public class QuestsTemplate
{
    [XmlAttribute("tag")]                  //��� �� �������� ������� ������� �����
    public string tag;

    [XmlAttribute("icon_tag")]           //��� ����� ������ ������, ��������� �� �������� ��� ����� �� ������������ ������
    public string icon_tag= "use";

    [XmlAttribute("icon_name")]           //��� �� �������� ������� ������ ��� ������
    public string icon_name;

    [XmlIgnore]
    public Sprite icon;  //������ ������

    //������� �� ������ ����� ��� ������
    [XmlIgnore]
    public bool isactive = false;

    [XmlAttribute("stages_count")]   //����� ������ � ������
    public int stages_count;

    //������� ����
    [XmlIgnore]
    public int current_stage_count=-1;

    //������ ������
    [XmlArray("stages"), XmlArrayItem("stage")]
    public QuestsStage[] stages;


    //�������� �� ����� � �������� �� �������
    [XmlIgnore]
    public bool lastshown = false;

    //�������� �� ������� �� �����
    [XmlIgnore]
    public bool complete = false;

    //�������
    [XmlArray("prizes"), XmlArrayItem("prize")]
    public QuestsPrize[] prizes;

    //��� ���������� �� ���� ������
    [XmlAttribute("next_quest_tag")]
    public string next_quest_tag = "";

    [XmlIgnore]
    public Sprite[] prizes_image;

    [XmlIgnore]
    public QuestWindowTable quest_table;//��������

    [XmlIgnore]
    public float last_show_progress = 0; //��������� ���������� ��������

}


[XmlRoot("stage")]
public class QuestsStage
{
    [XmlAttribute("type")]
    public string type;             //get - ��������� ��������, ������� �����; craft - ������� ��������, ������������ + �������� ��������� �� �������; build - ��������� ��������; use - ������ �������

    [XmlAttribute("recipe_name")]
    public string recipe_name = "";             //

    [XmlArray("details"), XmlArrayItem("detail")]
    public QuestsDetail[] details;

    [XmlIgnore]
    public int completes_details = 0;//������� ����� ����� ���������

    [XmlIgnore]
    public int stage_length;//���������� ���������� � �����

    [XmlIgnore]
    public bool complete = false;
}

[XmlRoot("detail")]
public class QuestsDetail
{
    [XmlAttribute("name")]
    public string name;

    [XmlAttribute("count")]
    public int count;

    [XmlIgnore]
    public int current_count;

    [XmlIgnore]
    public Sprite detail_image;

    [XmlIgnore]
    public bool complete = false;
}

[XmlRoot("prize")]
public class QuestsPrize
{
    [XmlAttribute("name")]
    public string name;

    [XmlAttribute("count")]
    public int count;
}

