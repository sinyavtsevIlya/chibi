﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildScript : MonoBehaviour, IPointerDownHandler
{

    public GameManager GOD;
    public InfoScript I;
    public DragHandler thisDrag;
    int PressCount = 0;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)                           //открываем строительство
            {
                BtnDown();
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount++;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }

    public void BtnDown()
    {
        if (!GOD.IsTutorial)
        {
            GOD.PlayerInter.CloseAll();
            if (I.type == InfoScript.ItemType.building)
                GOD.PlayerInter.StartBuilding(I.name, I);
            else
            {
                GOD.PlayerInter.Build(I.name,I);
                GOD.PlayerContr.InPut = true;
            }
        }
    }
    IEnumerator PressKd()
    {
        yield return StartCoroutine(WaitForSecondsCor(0.2f));
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    private IEnumerator WaitForSecondsCor(float t)
    {
        float targetTime = Time.realtimeSinceStartup + t;

        while (Time.realtimeSinceStartup < targetTime)
        {
            yield return null;
        }
    }
}
