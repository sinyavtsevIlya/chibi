﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ClothesScript : MonoBehaviour, IPointerDownHandler
{

    public GameManager GOD;
    public InfoScript I;
    public DragHandler thisDrag;
    int PressCount = 0;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)                           //надеваем
            {
                    BtnDown();
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount++;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }

    public void BtnDown()
    {
        if (!GOD.IsTutorial || GOD.Tutor&&GOD.Tutor.TutorialStep==17)
        {
            if (I.NowOnPlayer)       //снять одежду
            {
                if (GOD.InventoryContr.CanAddSeveral(1))
                {
                    transform.SetParent(null);
                    I.ParentSlot = null;
                    GOD.PlayerContr.RemovePlayerParametre(gameObject, true);
                }
                else
                    GOD.PlayerInter.OpenCantOpen();
            }
            else                                                 //надеть одежду
            {
                if (I.OnPlayerSlot == "Mantle")      // для плащей
                {
                    Transform[] slots = new Transform[3];
                    int x = 0;
                    for (int i = 0; i < GOD.PlayerContr.EqupmentSlots.Length; i++)
                    {
                        Transform t = GOD.PlayerContr.EqupmentSlots[i].transform;
                        if (t.name.Contains("Mantle"))
                        {
                            slots[x] = t;
                            x++;
                        }
                    }

                    for (int i = 0; i < slots.Length; i++)  //если слоты с плащами пусты
                    {
                        if (slots[i].childCount <= 0)
                        {
                            transform.SetParent(slots[i]);
                            I.ParentSlot = slots[i];
                            GOD.PlayerContr.SetPlayerParametre(gameObject, true);
                            return;
                        }
                    }

                }
                else
                {
                    for (int i = 0; i < GOD.PlayerContr.EqupmentSlots.Length; i++)
                    {
                        Transform t = GOD.PlayerContr.EqupmentSlots[i].transform;
                        if (t.name == I.OnPlayerSlot)
                        {
                            if (t.childCount > 0)                        // убираем предыдущий предмет
                            {
                                Transform oldicon = t.GetChild(0);
                                GOD.PlayerContr.RemovePlayerParametre(oldicon.gameObject, false);
                                oldicon.SetParent(I.ParentSlot);
                                oldicon.GetComponent<InfoScript>().ParentSlot = I.ParentSlot;
                            }
                            transform.SetParent(t);
                            I.ParentSlot = t;
                            GOD.PlayerContr.SetPlayerParametre(gameObject, true);                         // пересчитываем параметры игрока                                                                      // InventoryContr.RemoveFromShopSlots();
                            break;
                        }
                    }
                }
            }

            if (GOD.PlayerInter.Inventory.activeSelf)
                GOD.PlayerInter.OpenInventory();
            else
            {
                GOD.PlayerInter.OpenInventory();
                GOD.PlayerInter.CloseInventoryImmediatly();
            }
        }
    }
    IEnumerator PressKd()
    {
        yield return StartCoroutine(WaitForSecondsCor(0.2f));
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    private IEnumerator WaitForSecondsCor(float t)
    {
        float targetTime = Time.realtimeSinceStartup + t;

        while (Time.realtimeSinceStartup < targetTime)
        {
            yield return null;
        }
    }
}
