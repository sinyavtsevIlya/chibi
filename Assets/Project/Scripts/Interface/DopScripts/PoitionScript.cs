﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PoitionScript : MonoBehaviour, IPointerDownHandler
{

    public GameManager GOD;
    public InfoScript I;
    public DragHandler thisDrag;
    int PressCount = 0;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
                if (PressCount > 0)                           //используем
                {
                    BtnDown();
                    PressCount = 0;
                    StopAllCoroutines();
                }
                else
                {
                    PressCount++;
                    StopAllCoroutines();
                    StartCoroutine(PressKd());
                }
        }
    }
    public void BtnDown()
    {
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.Eat);

        GOD.BafContr.PoitionEffects(name);       // включаем доп эффект если есть
        GOD.PlayerInter.ShowIndicators();
        GOD.InventoryContr.RemoveFromInventory(I, 1);
        if (GOD.PlayerInter.Inventory.activeSelf)
            GOD.PlayerInter.OpenInventory();
        else
        {
            GOD.PlayerInter.OpenInventory();
            GOD.PlayerInter.CloseInventoryImmediatly();
        }

    }
    IEnumerator PressKd()
    {
        yield return StartCoroutine(WaitForSecondsCor(0.2f));
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }
    private IEnumerator WaitForSecondsCor(float t)
    {
        float targetTime = Time.realtimeSinceStartup + t;

        while (Time.realtimeSinceStartup < targetTime)
        {
            yield return null;
        }
    }
}
