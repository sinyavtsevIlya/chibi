﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class FoodScript : MonoBehaviour, IPointerDownHandler
{
    public GameManager GOD;
    public InfoScript I;
    public DragHandler thisDrag;
    int PressCount = 0;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)                            // восстанавливаем голод
            {
                if (!GOD.IsTutorial)
                    BtnDown();
                else
                {
                    if (GOD.Tutor.TutorialStep == 20)
                        BtnDown();
                }
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount++;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }

    public void BtnDown()
    {
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.Eat);

        GOD.PlayerContr.EAT += I.FoodRes;
        if (GOD.PlayerContr.EAT > 100)
            GOD.PlayerContr.EAT = 100;
        GOD.PlayerContr.HP += I.HPRes;
        if (GOD.PlayerContr.HP > GOD.PlayerContr.realmaxHP)
            GOD.PlayerContr.HP = GOD.PlayerContr.realmaxHP;
        GOD.BafContr.FoodEffects(name);       // включаем доп эффект если есть
        GOD.PlayerInter.ShowIndicators();
        GOD.InventoryContr.RemoveFromInventory(I, 1);
        if (GOD.PlayerInter.Inventory.activeSelf)
            GOD.PlayerInter.OpenInventory();
        else
        {
            GOD.PlayerInter.OpenInventory();
            GOD.PlayerInter.CloseInventoryImmediatly();
        }

        if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 20)
            GOD.Tutor.NextStep = true;
    }
    IEnumerator PressKd()
    {
        yield return StartCoroutine(WaitForSecondsCor(0.2f));
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    private IEnumerator WaitForSecondsCor(float t)
    {
        float targetTime = Time.realtimeSinceStartup + t;

        while (Time.realtimeSinceStartup < targetTime)
        {
            yield return null;
        }
    }

}
