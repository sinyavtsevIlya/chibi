﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShopWindow : MonoBehaviour {

    public GameManager GOD;
    public GameObject Shop;
    public Text MainShop; //надпись Магазин
    public Text ShopGem, BuyGem;

    public List<ShopItem> AllShopItems = new List<ShopItem>();
    public RectTransform ChestPanel, BuildingsPanel, MagicPanel, HairBoyPanel, HairGirlPanel, ColorPanel, SkinBoyPanel, SkinGirlPanel;
    public GameObject Offer, Chests, Buildings, Magic; // вкладки магазина
    public GameObject HairBoy, HairGirl, HairColor, SkinBoy, SkinGirl; //доп вкладки магазина
    public Text TChests, TBuildings, TMagic, THair, TSkin; // вкладки магазина

    public ShopItem[] ChestsItems, BuildingsItems, MagicItems, HairBoyItems, HairGirlItems, ColorItems, SkinBoyItems, SkinGirlItems;
    public ShopItem[] CurrentItems;

    public Image OfferImage;
    public Text OfferText;

    public GameObject BuyBtn; //кнопка покупки
    public Text BuyText; //текст на кнопке покупки

    Image[] HairBoyImage, HairBoyImage2;
    Image[] HairGirlImage, HairGirlImage2, HairGirlImage3;

    Image[] SkinBoyImage;
    Image[] SkinGirlImage;
    //дополнительные кнопки для доп вкладок
    public GameObject SkinBtnBack, HairBtnBack;
    public Image HairColorBtn, HairBoyBtn, HairGirlBtn, SkinBoyBtn, SkinGirlBtn;


    RectTransform CurrentPanel = null;
    public GameObject Left, Right;



    bool InMove = false;     // перемещается ли сейчас панель
    int MaxWindow = 4;
    int CurrentWindow = 1;
    float BeginX = 0;         // место начала нажатия

    //Окно покупки самоцветов
    [Header("Покупка самоцветов:")]
    public GameObject GemWindow;               // окно покупки самоцветов
    public GameObject GemBlack;
    public Text GemShop; //надпись Магазин
    public GemItem[] GemPacks = new GemItem[6];
    public Image[] GemWindows = new Image[6];
    public Text[] GemsCountText;                   //количество
    public Text[] GemsCostText;                   //цены

    CloseWindow ShopClose, GemClose;
    public void Init()
    {
        //САМОЦВЕТЫ________________________________________
        GemPacks[0].GemName = "gempack1";
        GemPacks[0].GemCount = 60;
        GemPacks[0].GemCost = MyIAP.instance.GetLocalizedPriceString(GemPacks[0].GemName);
        GemPacks[1].GemName = "gempack2";
        GemPacks[1].GemCount = 260;
        GemPacks[1].GemCost = MyIAP.instance.GetLocalizedPriceString(GemPacks[1].GemName);
        GemPacks[2].GemName = "gempack3";
        GemPacks[2].GemCount = 600;
        GemPacks[2].GemCost = MyIAP.instance.GetLocalizedPriceString(GemPacks[2].GemName);
        GemPacks[3].GemName = "gempack4";
        GemPacks[3].GemCount = 1300;
        GemPacks[3].GemCost = MyIAP.instance.GetLocalizedPriceString(GemPacks[3].GemName);
        GemPacks[4].GemName = "gempack5";
        GemPacks[4].GemCount = 3400;
        GemPacks[4].GemCost = MyIAP.instance.GetLocalizedPriceString(GemPacks[4].GemName);
        GemPacks[5].GemName = "gempack6";
        GemPacks[5].GemCount = 7000;
        GemPacks[5].GemCost = MyIAP.instance.GetLocalizedPriceString(GemPacks[5].GemName);

        for (int i = 0; i < GemsCountText.Length; i++)
            GemsCountText[i].text = "" + GemPacks[i].GemCount;
        for (int i = 0; i < GemsCostText.Length; i++)
        {
            GemsCostText[i].text = "" + GemPacks[i].GemCost;
        }

      
        //СУНДУКИ_____________________________________________
        ChestsItems = new ShopItem[ChestPanel.transform.childCount];
        for (int i = 0; i < ChestPanel.transform.childCount; i++)
        {
            ChestsItems[i] = ChestPanel.transform.GetChild(i).GetComponent<ShopItem>();
            ChestsItems[i].Init(this);
        }
        //ЗДАНИЯ_____________________________________________
        BuildingsItems = new ShopItem[BuildingsPanel.transform.childCount];
        for (int i = 0; i < BuildingsPanel.transform.childCount; i++)
        {
            BuildingsItems[i] = BuildingsPanel.transform.GetChild(i).GetComponent<ShopItem>();
            BuildingsItems[i].Init(this);
        }
        //МАГИЯ_____________________________________________
        MagicItems = new ShopItem[MagicPanel.transform.childCount];
        for (int i = 0; i < MagicPanel.transform.childCount; i++)
        {
            MagicItems[i] = MagicPanel.transform.GetChild(i).GetComponent<ShopItem>();
            MagicItems[i].Init(this);
        }

        //ПРИЧЕСКИ____________________________________________
        HairBoyItems = new ShopItem[HairBoyPanel.transform.childCount];
        HairBoyImage = new Image[HairBoyPanel.transform.childCount];
        HairBoyImage2 = new Image[HairBoyPanel.childCount];
        HairGirlImage3 = new Image[5];
        for (int i = 0; i < HairBoyPanel.transform.childCount; i++)
        {
            HairBoyItems[i] = HairBoyPanel.transform.GetChild(i).GetComponent<ShopItem>();
            HairBoyItems[i].Init(this);
            HairBoyImage[i] = HairBoyItems[i].transform.Find("ItemHead").GetComponent<Image>();
            HairBoyImage2[i] = HairBoyItems[i].transform.Find("ItemHead2").GetComponent<Image>();
        }

        HairGirlItems = new ShopItem[HairGirlPanel.transform.childCount];
        HairGirlImage = new Image[HairGirlPanel.transform.childCount];
        HairGirlImage2 = new Image[HairGirlPanel.childCount];
        HairGirlImage3 = new Image[5];
        int i3 = 0;
        for (int i = 0; i < HairGirlPanel.transform.childCount; i++)
        {
            HairGirlItems[i] = HairGirlPanel.transform.GetChild(i).GetComponent<ShopItem>();
            HairGirlItems[i].Init(this);
            HairGirlImage[i] = HairGirlItems[i].transform.Find("ItemHead").GetComponent<Image>();
            HairGirlImage2[i] = HairGirlItems[i].transform.Find("ItemHead2").GetComponent<Image>();
            Transform t = HairGirlItems[i].transform.Find("ItemHead3");
            if (t)
            {
                HairGirlImage3[i3] = t.GetComponent<Image>();
                i3++;
            }
        }

        ColorItems = new ShopItem[ColorPanel.transform.childCount];
        for (int i = 0; i < ColorPanel.transform.childCount; i++)
        {
           ColorItems[i] = ColorPanel.transform.GetChild(i).GetComponent<ShopItem>();
            ColorItems[i].Init(this);
        }
        //СКИНЫ____________________________________________
        SkinBoyItems = new ShopItem[SkinBoyPanel.transform.childCount];
        SkinBoyImage = new Image[SkinBoyPanel.transform.childCount];
        for (int i = 0; i < SkinBoyPanel.transform.childCount; i++)
        {
            SkinBoyItems[i] = SkinBoyPanel.transform.GetChild(i).GetComponent<ShopItem>();
            SkinBoyItems[i].Init(this);
            SkinBoyImage[i] = SkinBoyItems[i].transform.Find("ItemBack").GetChild(0).GetComponent<Image>();
        }
        SkinGirlItems = new ShopItem[SkinGirlPanel.transform.childCount];
        SkinGirlImage = new Image[SkinGirlPanel.transform.childCount];
        for (int i = 0; i < SkinBoyPanel.transform.childCount; i++)
        {
            SkinGirlItems[i] = SkinGirlPanel.transform.GetChild(i).GetComponent<ShopItem>();
            SkinGirlItems[i].Init(this);
            SkinGirlImage[i] = SkinGirlItems[i].transform.Find("ItemBack").GetChild(0).GetComponent<Image>();
        }

        ShopClose = gameObject.GetComponent<CloseWindow>();
        GemClose = GemWindow.GetComponent<CloseWindow>();
        OpenShop();
        CloseShop();
       // SetButtons();

        InitLanguage();
    }

    public void InitLanguage()
    {
        MainShop.text = Localization.instance.getTextByKey("#Shop");
        GemShop.text = Localization.instance.getTextByKey("#Shop");
        TChests.text = Localization.instance.getTextByKey("#ShopType1");
        TBuildings.text = Localization.instance.getTextByKey("#ShopType2");
        TMagic.text = Localization.instance.getTextByKey("#ShopType3");
        THair.text = Localization.instance.getTextByKey("#ShopType4");
        TSkin.text = Localization.instance.getTextByKey("#ShopType5");
        OfferText.text = Localization.instance.getTextByKey("#Offer1");
    }
    public void OpenShop()
    {
        if(Shop.activeSelf)
        CloseShopImmediatly();
        GOD.GamePause();
        ShowGemCount();
        LoadImage();
        GOD.PlayerInter.Black.SetActive(true);
        Shop.transform.localScale = Constants.Shop;
        Shop.SetActive(true);
        OpenRazdel("Chests");
    }
    public void CloseShop()
    {
        GOD.PlayerInter.Black.SetActive(false);
        GOD.GamePlay();
        ShopClose.StartClose();
      //  Shop.SetActive(false);
       // UnloadImage();
    }
    public void CloseShopImmediatly()
    {
        ShopClose.Close();
        GOD.PlayerInter.Black.SetActive(false);
        GOD.GamePlay();
    }
    void LoadImage()
    {
        //ОФФЕР
        OfferImage.sprite = Resources.Load<Sprite>("Shop/offer");
        //ПРИЧЕСКИ
        Sprite[] boyhairs = Resources.LoadAll<Sprite>("Hairs/BoyHairs");

        HairBoyImage[0].sprite = boyhairs[0];
        HairBoyImage2[0].sprite = boyhairs[1];
        HairBoyImage[1].sprite = boyhairs[2];
        HairBoyImage2[1].sprite = boyhairs[3];
        HairBoyImage[2].sprite = boyhairs[4];
        HairBoyImage2[2].sprite = boyhairs[5];
        HairBoyImage[3].sprite = boyhairs[4];
        HairBoyImage2[3].sprite = boyhairs[6];
        HairBoyImage[4].sprite = boyhairs[8];
        HairBoyImage2[4].sprite = boyhairs[9];
        HairBoyImage[5].sprite = boyhairs[4];
        HairBoyImage2[5].sprite = boyhairs[7];
        HairBoyImage[6].sprite = boyhairs[10];
        HairBoyImage2[6].sprite = boyhairs[11];
        HairBoyImage[7].sprite = boyhairs[12];
        HairBoyImage2[7].sprite = boyhairs[13];
        HairBoyImage[8].sprite = boyhairs[14];
        HairBoyImage2[8].sprite = boyhairs[15];
        HairBoyImage[9].sprite = boyhairs[16];
        HairBoyImage2[9].sprite = boyhairs[17];

        Sprite[] girlhairs = Resources.LoadAll<Sprite>("Hairs/GirlHairs");

        HairGirlImage[0].sprite = girlhairs[0];
        HairGirlImage2[0].sprite = girlhairs[1];
        HairGirlImage[1].sprite = girlhairs[0];
        HairGirlImage2[1].sprite = girlhairs[2];
        HairGirlImage3[0].sprite = girlhairs[3]; //0
        HairGirlImage[2].sprite = girlhairs[4];
        HairGirlImage2[2].sprite = girlhairs[5];
        HairGirlImage3[1].sprite = girlhairs[6]; //1
        HairGirlImage[3].sprite = girlhairs[7];
        HairGirlImage2[3].sprite = girlhairs[8];
        HairGirlImage[4].sprite = girlhairs[9];
        HairGirlImage2[4].sprite = girlhairs[10];
        HairGirlImage[5].sprite = girlhairs[11];
        HairGirlImage2[5].sprite = girlhairs[12];
        HairGirlImage[6].sprite = girlhairs[13];
        HairGirlImage2[6].sprite = girlhairs[14];
        HairGirlImage3[2].sprite = girlhairs[15]; //2
        HairGirlImage[7].sprite = girlhairs[16];
        HairGirlImage2[7].sprite = girlhairs[17];
        HairGirlImage3[3].sprite = girlhairs[18]; //3
        HairGirlImage[8].sprite = girlhairs[19];
        HairGirlImage2[8].sprite = girlhairs[22];
        HairGirlImage[9].sprite = girlhairs[23];
        HairGirlImage2[9].sprite = girlhairs[24];
        HairGirlImage3[4].sprite = boyhairs[18]; //4

        //СКИНЫ
        Sprite[] skins = Resources.LoadAll<Sprite>("Skins/Skins");
        for(int i=0; i<skins.Length; i++)
        {
            if (i < SkinBoyImage.Length)
                SkinBoyImage[i].sprite = skins[i];
            else
                SkinGirlImage[i-7].sprite = skins[i];
        }

    }
    public void UnloadImage()
    {
        //ОФФЕР
        OfferImage.sprite = null;
        //ПРИЧЕСКИ
        for (int i=0; i<HairBoyImage.Length; i++)
        {
            HairBoyImage[i].sprite = null;
            HairBoyImage2[i].sprite = null;
        }
        for (int i = 0; i < HairGirlImage.Length; i++)
        {
            HairGirlImage[i].sprite = null;
            HairGirlImage2[i].sprite = null;
        }
        for (int i = 0; i < HairGirlImage3.Length; i++)
            HairGirlImage3[i].sprite = null;

        //СКИНЫ
        for (int i = 0; i < SkinBoyImage.Length; i++)
            SkinBoyImage[i].sprite = null;
        for (int i = 0; i < SkinGirlImage.Length; i++)
            SkinGirlImage[i].sprite = null;
        Resources.UnloadUnusedAssets();
    }
    public void OpenRazdel(string s)
    {
        StopAllCoroutines();
        InMove = false;
        Left.SetActive(false);
        Right.SetActive(false);
        Offer.SetActive(false);
        Chests.SetActive(false);
        Buildings.SetActive(false);
        Magic.SetActive(false);
        HairBoy.SetActive(false);
        HairGirl.SetActive(false);
        HairColor.SetActive(false);
        SkinBoy.SetActive(false);
        SkinGirl.SetActive(false);

        SkinBtnBack.SetActive(false);
        HairBtnBack.SetActive(false);

        TChests.color = new Color(0, 0.28f, 0.36f);
        TBuildings.color = new Color(0, 0.28f, 0.36f);
        TMagic.color = new Color(0, 0.28f, 0.36f);
        THair.color = new Color(0, 0.28f, 0.36f);
        TSkin.color = new Color(0, 0.28f, 0.36f);

        BuyBtn.SetActive(true);

        switch (s)
        {
            case "Chests":
                Chests.SetActive(true);
                CurrentPanel = ChestPanel;
                CurrentPanel.anchoredPosition = new Vector2(0, 0);
                MaxWindow = ChestPanel.childCount;
                TChests.color = new Color(1, 1, 1);
                CurrentItems = ChestsItems;
                BuyText.text = "" + CurrentItems[0].ItemCost;
                break;
            case "Buildings":
                Buildings.SetActive(true);
                CurrentPanel = BuildingsPanel;
                CurrentPanel.anchoredPosition = new Vector2(0, 0);
                MaxWindow = BuildingsPanel.childCount;
                TBuildings.color = new Color(1, 1, 1);
                CurrentItems = BuildingsItems;
                BuyText.text = "" + CurrentItems[0].ItemCost;
                break;
            case "Magic":
                Magic.SetActive(true);
                CurrentPanel =MagicPanel;
                CurrentPanel.anchoredPosition = new Vector2(0, 0);
                MaxWindow = MagicPanel.childCount;
                TMagic.color = new Color(1, 1, 1);
                CurrentItems = MagicItems;
                BuyText.text = "" + CurrentItems[0].ItemCost;
                break;
            case "Hair":
                HairBtnBack.SetActive(true);
                THair.color = new Color(1, 1, 1);
                VkladkyHair(GOD.OurPlayer);
                break;
            case "Skin":
                SkinBtnBack.SetActive(true);
                TSkin.color = new Color(1, 1, 1);
                VkladkySkin(GOD.OurPlayer);
                break;
            default:
                Offer.SetActive(true);
                MaxWindow = 0;
                break;
        }

        if(MaxWindow>0)
            Right.SetActive(true);
        CurrentWindow = 1;
    }
    



    public void Next(string s) // стрелки
    {
        if (!InMove)
        {
            StartCoroutine(ShowNext(s));
        }
    }
    IEnumerator ShowNext(string s)  // постепенно передвигаем окна
    {
        if (MaxWindow > 0)
        {
            InMove = true;
            if (s == "right")
            {
                if (CurrentWindow + 1 >= MaxWindow)
                    Right.SetActive(false);
                Left.SetActive(true);
                if (CurrentWindow < MaxWindow)
                {
                    int newpos = 0;
                    while (newpos < 500)
                    {
                        newpos += 50;
                        CurrentPanel.anchoredPosition -= new Vector2(50, 0);
                        yield return new WaitForSecondsRealtime(0.01f);
                    }
                    CurrentWindow++;
                }
            }
            else
            {
                if (CurrentWindow - 1 <= 1)
                    Left.SetActive(false);
                Right.SetActive(true);
                if (CurrentWindow > 1)
                {
                    int newpos = 0;
                    while (newpos < 500)
                    {
                        newpos += 50;
                        CurrentPanel.anchoredPosition += new Vector2(50, 0);
                        yield return new WaitForSecondsRealtime(0.01f);
                    }

                    CurrentWindow--;
                }
            }

            if(CurrentItems[CurrentWindow-1])        //выводим цену на кнопку
            {
                bool b = false;
                if (CurrentItems == HairBoyItems)
                    b = GOD.PlayerContr.BoyHairs[CurrentItems[CurrentWindow - 1].DonatNumber];
                else if (CurrentItems == HairGirlItems)
                    b = GOD.PlayerContr.GirlHairs[CurrentItems[CurrentWindow - 1].DonatNumber];
                else if (CurrentItems == ColorItems)
                    b = GOD.PlayerContr.Colors[CurrentItems[CurrentWindow - 1].DonatNumber];
                else if (CurrentItems == SkinBoyItems)
                    b = GOD.PlayerContr.BoySkins[CurrentItems[CurrentWindow - 1].DonatNumber];
                else if (CurrentItems == SkinGirlItems)
                    b = GOD.PlayerContr.GirlSkins[CurrentItems[CurrentWindow - 1].DonatNumber];

                if (b)
                    BuyText.text = Localization.instance.getTextByKey("#Use");
                else
                    BuyText.text = "" + CurrentItems[CurrentWindow - 1].ItemCost;
            }
            InMove = false; 
        }
    }
    public void BeginDragPos()
    {
        BeginX = Input.mousePosition.x;
    }
    public void DragPanel()   // перемещение панели перетягиванием
    {
        if (!InMove)
        {
            if (BeginX > Input.mousePosition.x)
                StartCoroutine(ShowNext("right"));
            else
                StartCoroutine(ShowNext("left"));
        }
        BeginX = 0;
    }


    public void Buy()
    {
        if (CurrentItems!=null && CurrentItems[CurrentWindow-1]!=null)
        {
            ShopItem sI = CurrentItems[CurrentWindow - 1];
            if (sI.name.Contains("Hair"))
            {
                bool[] Hairs;
                if (sI.name.Contains("Girl"))
                    Hairs = GOD.PlayerContr.GirlHairs;
                else
                    Hairs = GOD.PlayerContr.BoyHairs;

                int x = sI.DonatNumber;
                if (!Hairs[x])
                {
                    NowBuy(sI);
                    if (NowBuy(sI))
                    {
                        Hairs[x] = true;
                       BuyText.text = Localization.instance.getTextByKey("#Use");
                        GOD.Save.SaveGame(false);//сохраняем покупку
                    }
                }
                else
                {
                    if ((GOD.OurPlayer == "Girl" && CurrentItems == HairGirlItems) || (GOD.OurPlayer == "Boy" && CurrentItems == HairBoyItems))
                        GOD.PlayerContr.SetPlayerHairs(x);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Btn);
                }
            }
            else if (sI.name.Contains("Color"))
            {
                int x = sI.DonatNumber;
                if (!GOD.PlayerContr.Colors[x])
                {
                    NowBuy(sI);
                    if (NowBuy(sI))
                    {
                        GOD.PlayerContr.Colors[x] = true;
                       BuyText.text = Localization.instance.getTextByKey("#Use");
                        GOD.Save.SaveGame(false); //сохраняем покупку
                    }
                }
                else
                {
                    GOD.PlayerContr.SetPlayerHairColor(x);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Btn);
                    UpdateHairColor();
                }
            }
            else if (sI.name.Contains("Skin"))
            {
                bool[] Skins;
                if (sI.name.Contains("Girl"))
                    Skins = GOD.PlayerContr.GirlSkins;
                else
                    Skins = GOD.PlayerContr.BoySkins;
                int x = sI.DonatNumber;
                if (!Skins[x])
                {
                    if (NowBuy(sI))
                    {
                        Skins[x] = true;
                       BuyText.text = Localization.instance.getTextByKey("#Use");
                        GOD.Save.SaveGame(); //сохраняем покупку
                    }
                }
                else
                {
                    if((GOD.OurPlayer=="Girl"&&CurrentItems==SkinGirlItems)|| (GOD.OurPlayer == "Boy" && CurrentItems == SkinBoyItems))
                        GOD.PlayerContr.SetPlayerSkins(x);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Btn);
                }
            }
            else
            {
                if (NowBuy(sI))
                {
                    GOD.PlayerInter.ShowNewItem(sI.name, sI.ItemCount);
                    int count = sI.ItemCount;
                    if (GOD.InventoryContr.CanAdd(sI.name, 1))
                        GOD.InventoryContr.AddToInventory(sI.name, count);
                    else
                        GOD.InventoryContr.AddToShopSlots(sI.name, count);
                    GOD.PlayerInter.OpenInventory();
                    GOD.PlayerInter.CloseInventoryImmediatly();
                    GOD.PlayerInter.Black.SetActive(true);
                    GOD.Save.SaveGame(false); //сохраняем покупку
                }
            }
        }
    }

    bool NowBuy(ShopItem s)
    {
        int x = Balance.Gem - s.ItemCost;
        if (x > 0) //если хватает денег - покупаем
        {
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Buy);
            Balance.Gem = Balance.Gem - s.ItemCost;
            ShowGemCount();
            MyAppMetrica.LogEvent("Purchases", "Shop", s.name);
            return true;
        }
        else
        {
            GOD.MoneyNo.OpenNoMoney();
            return false;
        }
    }

    public void ShowRes(string[] Massiv, int[] Counts, Transform InfoItems, GameObject OneItem)
    {
        for (int i = 0; i < Massiv.Length; i++)
        {
            var sp = GOD.InventoryContr.FindIcon( Massiv[i]);   
            if (sp != null)
            {
                GameObject t = Instantiate(OneItem);
                t.SetActive(true);
                t.GetComponent<Image>().sprite = sp;
                t.transform.GetChild(0).GetComponent<Text>().text = "" + Counts[i];
                t.transform.SetParent(InfoItems);
                t.transform.localScale = Constants.VectorOne;
            }
        }
    }


    public void VkladkyHair(string vkladka)
    {
        if(vkladka == "Girl")
        {
            HairBoy.SetActive(false);
            HairColor.SetActive(false);
            HairBoyBtn.color = new Color(1f, 1f, 1f, 1f);       //отключаем 
            HairColorBtn.color = new Color(1f, 1f, 1f, 1f);
            HairGirlBtn.color = new Color(0.3f, 0.3f, 0.3f, 1f);
            Left.SetActive(false);
            Right.SetActive(true);
            CurrentWindow = 1;
            HairGirl.SetActive(true);
            CurrentPanel = HairGirlPanel;
            CurrentPanel.anchoredPosition = new Vector2(-49, 2);
            MaxWindow = HairGirlPanel.childCount;
            CurrentItems = HairGirlItems;
            if (GOD.PlayerContr.GirlHairs[CurrentItems[0].DonatNumber])
                BuyText.text = Localization.instance.getTextByKey("#Use");
            else
                BuyText.text = "" + CurrentItems[0].ItemCost;
            if (GOD.OurPlayer != vkladka)
                BuyBtn.SetActive(false);
            else
                BuyBtn.SetActive(true);
        }
        else if(vkladka == "Color")
        {
            HairBoy.SetActive(false);
            HairGirl.SetActive(false);
            HairBoyBtn.color = new Color(1f, 1f, 1f, 1f);       //отключаем 
            HairGirlBtn.color = new Color(1f, 1f, 1f, 1f);
            HairColorBtn.color = new Color(0.3f, 0.3f, 0.3f, 1f);
            Left.SetActive(false);
            Right.SetActive(true);
            CurrentWindow = 1;
            HairColor.SetActive(true);
            CurrentPanel = ColorPanel;
            CurrentPanel.anchoredPosition = new Vector2(-49, 2);
            MaxWindow = ColorPanel.childCount;
            CurrentItems = ColorItems;
            if (GOD.PlayerContr.Colors[CurrentItems[0].DonatNumber])
                BuyText.text = Localization.instance.getTextByKey("#Use");
            else
                BuyText.text = "" + CurrentItems[0].ItemCost;
            BuyBtn.SetActive(true);
        }
        else
        {
            HairGirl.SetActive(false);
            HairColor.SetActive(false);
            HairGirlBtn.color = new Color(1f, 1f, 1f, 1f);       //отключаем 
            HairColorBtn.color = new Color(1f, 1f, 1f, 1f);
            HairBoyBtn.color = new Color(0.3f, 0.3f, 0.3f, 1f);
            Left.SetActive(false);
            Right.SetActive(true);
            CurrentWindow = 1;
            HairBoy.SetActive(true);
            CurrentPanel = HairBoyPanel;
            CurrentPanel.anchoredPosition = new Vector2(-49, 2);
            MaxWindow = HairBoyPanel.childCount;
            CurrentItems = HairBoyItems;
            if (GOD.PlayerContr.GirlHairs[CurrentItems[0].DonatNumber])
                BuyText.text = Localization.instance.getTextByKey("#Use");
            else
                BuyText.text = "" + CurrentItems[0].ItemCost;
            if (GOD.OurPlayer != vkladka)
                BuyBtn.SetActive(false);
            else
                BuyBtn.SetActive(true);
        }
    }

    public void VkladkySkin(string player)
    {
        if (player == "Girl")
        {
            SkinBoy.SetActive(false);
            SkinBoyBtn.color = new Color(1f, 1f, 1f, 1f);       //отключаем скины мальчика
            SkinGirlBtn.color = new Color(0.3f, 0.3f, 0.3f, 1f);
            Left.SetActive(false);
            Right.SetActive(true);
            CurrentWindow = 1;
            SkinGirl.SetActive(true);
            CurrentPanel = SkinGirlPanel;
            CurrentPanel.anchoredPosition = new Vector2(-49, 2);
            MaxWindow = SkinGirlPanel.childCount;
            CurrentItems = SkinGirlItems;
            if(GOD.PlayerContr.GirlSkins[CurrentItems[0].DonatNumber])
                BuyText.text = Localization.instance.getTextByKey("#Use");
            else
                BuyText.text = "" + CurrentItems[0].ItemCost;
        }
        else
        {
            SkinGirl.SetActive(false);
            SkinGirlBtn.color = new Color(1f, 1f, 1f, 1f);       //отключаем скины девочки
            SkinBoyBtn.color = new Color(0.3f, 0.3f, 0.3f, 1f);       
            SkinBoy.SetActive(true);
            Left.SetActive(false);
            Right.SetActive(true);
            CurrentWindow = 1;
            CurrentPanel = SkinBoyPanel;
            CurrentPanel.anchoredPosition = new Vector2(-49, 2);
            MaxWindow = SkinBoyPanel.childCount;
            CurrentItems = SkinBoyItems;
            if (GOD.PlayerContr.BoySkins[CurrentItems[0].DonatNumber])
                BuyText.text = Localization.instance.getTextByKey("#Use");
            else
                BuyText.text = "" + CurrentItems[0].ItemCost;
        }
        if (GOD.OurPlayer != player)
            BuyBtn.SetActive(false);
        else
            BuyBtn.SetActive(true);
    }

    public void UpdateHairColor() //меняем цвета причесок в магазине
    {
        for(int i=0; i<HairGirlItems.Length;i++)
            HairGirlItems[i].Hair.color = GOD.PlayerContr.HairColors[GOD.PlayerContr.CurrentGirlHairColorNumber];

        for (int i = 0; i < HairBoyItems.Length; i++)
            HairBoyItems[i].Hair.color = GOD.PlayerContr.HairColors[GOD.PlayerContr.CurrentBoyHairColorNumber];
    }

    //Покупка самоцветов_______________________________________________________
    public void OpenGemWindow()
    {
        GemBlack.SetActive(true);
        ShowGemCount();
        LoadGemImage();
        GemWindow.transform.localScale = Constants.GemWindow;
        GemWindow.SetActive(true);
        GOD.GamePause();
    }
    public void CloseGemWindow()
    {
        GemBlack.SetActive(false);
        if (!Shop.activeSelf)
            GOD.GamePlay();
        GemClose.StartClose();
      //  GemWindow.SetActive(false);
       // UnloadGemImage();
    }
    void LoadGemImage()   //подгружаем изображения
    {
        GemWindows[0].sprite = Resources.Load<Sprite>("Shop/Gem1");
        GemWindows[1].sprite = Resources.Load<Sprite>("Shop/Gem2");
        GemWindows[2].sprite = Resources.Load<Sprite>("Shop/Gem3");
        GemWindows[3].sprite = Resources.Load<Sprite>("Shop/Gem4");
        GemWindows[4].sprite = Resources.Load<Sprite>("Shop/Gem5");
        GemWindows[5].sprite = Resources.Load<Sprite>("Shop/Gem6");
    }
    public void UnloadGemImage()
    {
        for (int i = 0; i < GemWindows.Length; i++)
            GemWindows[i].sprite = null;
        Resources.UnloadUnusedAssets();
    }
    public void BuyGems(string productname)
    {
        //MyAppodeal.instance.isShow = true;
        MyIAP.instance.Buy(productname);
    }
    public void SuccesBuyGems(string productname)
    {
        for(int i=0; i< GemPacks.Length; i++)
        {
            if (GemPacks[i].GemName == productname)
            {
                GOD.Audio.BuyGemSound();
                GOD.Progress.SetAchivments(2); //за первую покупку
                MyAppMetrica.LogEvent("Purchases", "Inapp", productname);
                Balance.Gem = Balance.Gem+GemPacks[i].GemCount;
                ShowGemCount();
                if (GOD.PlayerInter.BuySlotWindow.activeSelf)
                    GOD.BuySlotScript.ShowGold();
                GOD.Save.SaveGame();
                break;
            }
            else
                Debug.Log("No shop item");
        }
    }

    public void ShowGemCount()
    {
        BuyGem.text = "" + Balance.Gem;
        ShopGem.text = "" + Balance.Gem;
        GOD.BuySlotScript.GemText.text = "" + Balance.Gem;
    }




}
