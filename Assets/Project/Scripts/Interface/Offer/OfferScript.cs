﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OfferScript : MonoBehaviour {

    public GameManager GOD;
    CloseWindow OfferClose;

    [Header("подгрузка изображений")]
    public GameObject OffersWindow;
    public Image OfferWindowImage, FlagImage;

    [Header("окно")]
    int Move=0;
    public RectTransform Content;
    public RectTransform Vieport;
    public GameObject ArrowTop, ArrowBot; //стрелки вверх и вниз

    public Text ButtonBuyText1, ButtonBuyText2, ButtonWatchText1, ButtonWatchText2;
    public Text FirstPurchaseCost1, FirstPurchaseCost2, NoAdsCost;
    public Text FPText1, FPText2, FPTimerText;
    public Text FirstPurchaseText, NoAdsText, StartNaborText, RecoverInstrumentText;

    [Header("офферы")]
    public OfferItem[] Offers;

    public void Init()
    {
        InitLanguage();
        AfterListChange();

        OfferClose = OffersWindow.GetComponent<CloseWindow>();
        OfferClose.Init();

        Offers[0].OfferName = "offerfirstpurchase";
        Offers[0].OfferGemCount = 260;
        Offers[0].OfferSkin = 3;
        Offers[0].SetTimer(3,0,0);
        Offers[1].OfferName = "offernoads";
        Offers[1].OfferGemCount = 300;
        Offers[1].OfferSkin = 1;

        for (int i = 0; i < Offers.Length; i++)
        {
            Offers[i].OfferOn = true;
            AddToList(Offers[i]);
        }
        StartCoroutine(OfferTimer());

        FirstPurchaseCost1.text = MyIAP.instance.GetLocalizedPriceString("offerfirstpurchase");
        FirstPurchaseCost2.text = MyIAP.instance.GetLocalizedPriceString("gempack3");
        NoAdsCost.text = MyIAP.instance.GetLocalizedPriceString("offernoads");
    }
    public void InitLanguage()
    {
        ButtonBuyText1.text = Localization.instance.getTextByKey("#Buy");
        ButtonBuyText2.text = Localization.instance.getTextByKey("#Buy");
        ButtonWatchText1.text = Localization.instance.getTextByKey("#Watch");
        ButtonWatchText2.text = Localization.instance.getTextByKey("#Watch");

        FirstPurchaseText.text = Localization.instance.getTextByKey("#OfferFirstPurchase");
        NoAdsText.text = Localization.instance.getTextByKey("#OfferNoAds");
        StartNaborText.text = Localization.instance.getTextByKey("#OfferStartNabor");
        RecoverInstrumentText.text = Localization.instance.getTextByKey("#OfferRecoverInstrument");

        FPText1.text = Localization.instance.getTextByKey("#OfferTimeLeft")+ " :";
        FPText2.text = "+"+Localization.instance.getTextByKey("#OfferSkin");
    }

    private void Update()
    {
        if(Move!=0)
        {
            if(Move<0 && ArrowBot.activeSelf || Move > 0 && ArrowTop.activeSelf)
                Content.localPosition += new Vector3(0, -Move * Time.deltaTime*300f, 0);
        }
    }

    public void OpenOffersWindow(string s="")
    {
        if (OffersWindow.activeSelf)
            CloseOfferImmediatly();
        GOD.GamePause();
        OffersWindow.transform.localScale =Constants.OffersWindow;
        LoadImage();
        GOD.PlayerInter.Black.SetActive(true);
        OffersWindow.SetActive(true);
        OpenOffer(s);
    }

    void LoadImage()
    {
        OfferWindowImage.sprite = Resources.Load<Sprite>("Offer/offer-window");
        FlagImage.sprite = Resources.Load<Sprite>("Offer/offer-window-menu");
        for(int i=0; i<Offers.Length; i++)
        {
            Offers[i].mainImage.sprite = Resources.Load<Sprite>("Offer/main_" + Offers[i].name);
            Offers[i].OfferIconImage.sprite = Resources.Load<Sprite>("Offer/icon_" + Offers[i].name);
            if (Offers[i].Lenta!=null)
            {
                Offers[i].Lenta.sprite = Resources.Load<Sprite>("Offer/offer_lenta");
               Offers[i].LentaBtn.sprite = Resources.Load<Sprite>("Offer/offer_lenta_btn");
            }
        }

    }
    public void Unloadimage()
    {
        OfferWindowImage.sprite = null;
        FlagImage.sprite = null;
        for (int i = 0; i < Offers.Length; i++)
        {
            Offers[i].mainImage.sprite = null;
            Offers[i].OfferIconImage.sprite = null;
            if (Offers[i].Lenta != null)
            {
                Offers[i].Lenta.sprite = null;
                Offers[i].LentaBtn.sprite = null;
            }
        }
        Resources.UnloadUnusedAssets();
    }
    public void CloseOffersWindow()
    {
        GOD.GamePlay();
        GOD.PlayerInter.Black.SetActive(false);
        OfferClose.StartClose();
    }

    public void CloseOfferImmediatly()
    {
        GOD.GamePlay();
        OfferClose.Close();
        GOD.PlayerInter.Black.SetActive(false);
    }

    public void OpenOfferFromGame(Transform offerIcon)
    {
        OpenOffer(offerIcon.name);
    }
    public void OpenOffer(string offerName)//открыть офер
    {
        for (int i = 0; i < Offers.Length; i++)
        {
            if (Offers[i].gameObject.activeSelf)
            {
                Offers[i].gameObject.SetActive(false);
                for (int x = 0; x < Offers[i].Flare.Flares.Count; x++)
                {
                    Offers[i].Flare.Flares[x].Play("FlareIdle");
                }
            }
        }

        switch (offerName)
        {
            case "IconNoAds":
                Offers[1].gameObject.SetActive(true);
                break;
            case "IconStartNabor":
                Offers[2].gameObject.SetActive(true);
                break;
            case "IconRecoverInstrument":
                Offers[3].gameObject.SetActive(true);
                break;
            default:
                for (int i = 0; i < Offers.Length; i++)
                {
                    if (Offers[i].OfferOn)
                    {
                        Offers[i].gameObject.SetActive(true);
                        break;
                    }
                }
                break;
        }
    }

    public void AddToList(OfferItem oi)//добавляем иконку в список оферов
    {
        Content.sizeDelta += new Vector2(0,90f);
        oi.OfferIcon.SetActive(true);
        oi.OfferIcon.transform.SetParent(Content);
    }
    public void RemoveFromList(OfferItem oi)//убираем иконку
    {
        oi.OfferIcon.transform.SetParent(oi.transform);
        oi.OfferIcon.SetActive(false);
        Content.sizeDelta -= new Vector2(0, 90f);
    }
    public void AfterListChange() //включение отключение стрелочек вниз и вверх
    {
        bool b = ((Content.rect.height - Content.localPosition.y) < Vieport.rect.height + 5f);
        if (b)
        {
            ArrowBot.SetActive(false);
            if (Move < 0)
                Move = 0;
        }
        else
            ArrowBot.SetActive(true);

        if (Content.localPosition.y > 5f)
            ArrowTop.SetActive(true);
        else
        {
            ArrowTop.SetActive(false);
            if (Move > 0)
                Move = 0;
        }
    }

    public void ChangeMove(int m)
    {
        Move = m;
    }
     
    public void Watch(string s) //кнопка смотреть
    {
       if (!MyAppodeal.showRewardedVideo(s))
            GOD.Settings.OpenNoConnection(true);
    }

    public void StartNabor()
    {
        GOD.InventoryContr.AddToInventory("WoodAxe", 1);
       // GOD.PlayerInter.ShowNewItem("WoodAxe", 1);

        GOD.InventoryContr.AddToInventory("WoodPick", 1);
       // GOD.PlayerInter.ShowNewItem("WoodPick", 1);

        GOD.InventoryContr.AddToInventory("WoodShovel", 1);
       // GOD.PlayerInter.ShowNewItem("WoodShovel", 1);

        GOD.InventoryContr.AddToInventory("FragileRod", 1);
       // GOD.PlayerInter.ShowNewItem("FragileRod", 1);
    }

    public void RecoverInstrument()
    {
        GOD.InventoryContr.AddToInventory("WoodAxe", 1);
       // GOD.PlayerInter.ShowNewItem("WoodAxe", 1);

        GOD.InventoryContr.AddToInventory("WoodPick", 1);
       // GOD.PlayerInter.ShowNewItem("WoodPick", 1);
    }

    public void BuyOffer(OfferItem offerItem)
    {
        //MyAppodeal.instance.isShow = true;
        MyIAP.instance.Buy(offerItem.OfferName);
    }

    public void SuccesBuyOffer(string productname)
    {
        GOD.Audio.BuyGemSound();
        OfferItem current=null;
        switch(productname)
        {
            case "offerfirstpurchase":
                current = Offers[0];
                break;
            case "offernoads":
                current = Offers[1];
                MyAppodeal.NoAds = true;
                break;
        }
        if(current!=null)
        {
            Balance.Gem = Balance.Gem + current.OfferGemCount;
            int x = current.OfferSkin;
            if (!GOD.PlayerContr.GirlSkins[x])
                GOD.PlayerContr.GirlSkins[x] = true;
            if (!GOD.PlayerContr.BoySkins[x])
                GOD.PlayerContr.BoySkins[x] = true;
            current.DeactivateOffer = true;
            current.OfferOn = false;
            RemoveFromList(current);
            OpenOffer("");
        }
        GOD.ShopScript.ShowGemCount();
        GOD.Save.SaveGame();
    }

    IEnumerator OfferTimer()
    {
        while (Offers[0].OfferOn)
        {
            yield return StartCoroutine(WaitForSecondsCor(1));
            Offers[0].ChangeTimer();
            FPTimerText.text = Offers[0].TimerString;
        }
        RemoveFromList(Offers[0]);
        OpenOffer("");
        
    }

    private IEnumerator WaitForSecondsCor(float t)
    {
        float targetTime = Time.realtimeSinceStartup + t;

        while (Time.realtimeSinceStartup < targetTime)
        {
            yield return null;
        }
    }
}


