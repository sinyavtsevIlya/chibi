﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfferFlare : MonoBehaviour {

    public List<Animator> Flares = new List<Animator>();

    float Timer = 0;

    private void Start()
    { 
        for(int i=0; i<transform.childCount;i++)
        {
            Animator a = transform.GetChild(i).GetComponent<Animator>();
            if(a)
            Flares.Add(a);
        }
    }

    void Update ()
    {
        //Timer += Time.deltaTime;
        Timer += 0.05f;
        if (Timer >= 0.5f)
        {
            int x = Random.Range(0, Flares.Count);
            if (!Flares[x].GetCurrentAnimatorStateInfo(0).IsName("Flare"))
                Flares[x].Play("Flare");
            Timer = 0;
        }
	}


}
