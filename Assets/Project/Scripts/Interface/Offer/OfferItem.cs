﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class OfferItem : MonoBehaviour {

    public bool OfferOn = false; // активен ли офер
    public bool DeactivateOffer = false; //оофер был куплен

    public string OfferName;
    public int OfferGemCount;
    public int OfferSkin;
    public GameObject OfferIcon;
    public OfferFlare Flare;

    bool IsTimer = false;
    public TimeSpan Timer= new TimeSpan(0,0,0);
    public TimeSpan TimerLimit = new TimeSpan(0,0,0);

    [Header("подгрузка изображений")]
    public Image mainImage;
    public Image Lenta, LentaBtn;
    public Image OfferIconImage;

    public void SetTimer(int h, int m, int s )
    {
        IsTimer = true;
        Timer = new TimeSpan(h, m, s);
    }

    public string TimerString
    {
        get
        {
            string s = "" + Timer.Hours + " : " + Timer.Minutes + " : " + Timer.Seconds;
            return s;
        }
    }
    public void ChangeTimer()
    {
        if(IsTimer)
        {
            Timer = new TimeSpan(Timer.Hours, Timer.Minutes, Timer.Seconds-1);
            if(Timer <=TimerLimit)
            {
                OfferOn = false;
            }
        }
    }
}
