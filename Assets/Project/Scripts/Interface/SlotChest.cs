﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class SlotChest : MonoBehaviour, IDropHandler
{


    public GameObject InGameSlotChild;     // ребенок слота в игре
    IconInGame ChildScript;

    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (DragHandler.itemBeingDragged != null)
        {
            DragHandler.itemBeingDragged.transform.SetParent(transform);
        }
    }

}
