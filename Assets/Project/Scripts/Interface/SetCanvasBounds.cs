using UnityEngine;

namespace Game.UI
{
    public class SetCanvasBounds : MonoBehaviour
    {

        [SerializeField]
        private Vector2Int _offset;

        private RectTransform _parent;
        private RectTransform _panel;

        private void Start()
        {
            _panel = this.GetComponent<RectTransform>();
            _parent = this.GetComponentInParent<Canvas>().GetComponent<RectTransform>();

            UpdateSafeArea();


            ScreenExtensions.ScreenOrientationChanged += UpdateSafeArea;
            ScreenExtensions.ScreenResolutionsChanged += UpdateSafeArea;

        }

        private void OnDestroy()
        {

            ScreenExtensions.ScreenOrientationChanged -= UpdateSafeArea;
            ScreenExtensions.ScreenResolutionsChanged -= UpdateSafeArea;

        }

#if UNITY_EDITOR
        private void Update()
        {
            UpdateOffset();
        }
#endif
        private void UpdateSafeArea()
        {
            var safeArea = Screen.safeArea;
            var screenWidth = Screen.width;
            var screenHeight = Screen.height;

            var leftOffset = safeArea.x;
            var rightOffset = screenWidth - safeArea.width - leftOffset;

            //���������� ������, �.�. ��������� � ��� ����������� �� ������
            safeArea = new Rect(leftOffset, 0, screenWidth - leftOffset - rightOffset, screenHeight);

            //���� ��� ��������, ������ �� ������, ����� ������� ������
            if (leftOffset > 0 || rightOffset > 0)
                ApplySafeArea(safeArea, screenWidth, screenHeight);
            else
                UpdateOffset();
        }

        private void ApplySafeArea(Rect area, float screenWidth, float screenHeight)
        {
            var anchorMin = area.position;
            anchorMin.x /= screenWidth;
            anchorMin.y /= screenHeight;
            _panel.anchorMin = anchorMin;

            var anchorMax = area.position + area.size;
            anchorMax.x /= screenWidth;
            anchorMax.y /= screenHeight;
            _panel.anchorMax = anchorMax;

            UpdateOffset();
        }

        private void UpdateOffset()
        {
            if (_offset != Vector2Int.zero)
            {


                _panel.anchoredPosition = new Vector2(_parent.sizeDelta.x * _offset.x, _parent.sizeDelta.y * _offset.y);
            }
        }





    }


}
