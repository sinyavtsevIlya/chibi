﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
#if !NO_FACEBOOK
using Facebook.Unity;
#endif
#if !NO_VK
using com.playGenesis.VkUnityPlugin;
using com.playGenesis.VkUnityPlugin.MiniJSON;
#endif
#if !NO_GPGS
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using UnityEngine.SocialPlatforms;
using UnityEngine.SceneManagement;

public class SettingsController : MonoBehaviour
{

    public GameManager GOD;
    public MenuScript Menu;
    public GameObject SettingsWindow;
    public Text MainSettings;
    public GameObject SettingsBlack;

    //востановление покупки
    [Header("Покупки")]
    public GameObject transactionsRestoredBtn;
    public Text transactionsRestoredBtnText;

    //Язык__________________________________________
    [Header("Язык:")]
    public Text LanguageName;
    public Text Language;  //язык
    string[] AllLanguages = new string[4];
    int CurrentLanguage;
    //МУЗЫКА__________________________________________
    [Header("Музыка:")]
    public Text Music;
    public Text Sound, MusicOn, SoundOn;
    public Image MusicOnOff, SoundOnOff;
    public float SoundVolume = 1; //громкость костра нужно менять относительно его текущей громкости!
    public float MusicVolume = 1;
    public Slider SoundSlider, MusicSlider;
    Color redColor = new Color(0.91f, 0.22f, 0.2f, 1);
    Color greenColor = new Color(0.06f, 0.68f, 0.46f, 1f);
    Color blueColor = new Color(0, 0.7f, 0.78f, 1f);
    Color darkblueColor = new Color(0.04f, 0.49f, 0.55f, 1f);
    //ГРАФИКА__________________________________________
    [Header("Графика:")]
    public Text VideoText;
    public Text GraphicText, ShadowText, Effects, CameraText;
    public Image ShadowBtnI, EffectsBtnI, CameraBtnI;
    public Selectable ShadowBtn;
    bool EffectIsOn = true, CameraIsOn = true;
    int GraphicsLevel = 0;
    bool ShadowsOn = false;
    //ИНТЕГРАЦИЯ__________________________________________
    [Header("Интеграция:")]
    public Text SocialText;
    public GameObject VKontakteBtn, FacebookBtn, GooglePlayBtn;
    public GameObject FacebookNever, VKontakteNever, GooglePlayNever;
    public Image Facebook, VKontakte, GooglePlay;
    public Text FacebookTxt, VKontakteTxt, GooglePlayTxt, FacebookTxt2, VKontakteTxt2, GooglePlayTxt2;
    public GameObject LoadWindow; //окно ожидания подключения
    public Image ChangeIcon1, ChangeIcon2;
    public Sprite GoogleSprite, IosSprite;
    //ССЫЛКИ__________________________________________
    [Header("Ссылки:")]
    public Button VkAdressBtn;
    public Button FacebookAdressBtn, NewGamesAdressBtn, ReviewAdressBtn, ReviewAdressBtn2;
    public Text We, NewGameTxt, ReviewTxt, ReviewTxt2;
    //ОЦЕНКА_____________________________________________
    [Header("Оценка:")]
    public GameObject RateWindow;
    public GameObject AfterRateWindow;
    public Text TopText, RateText, RateOkText, AfterRateText;
    public GameObject[] Stars;
    public Button RateButton;
    int CurrentStars = 0;

    //КНОПКИ
    [Header("Кнопки:")]
    //public GameObject SaveBtn;
    //public GameObject Mark;
    //public Text SaveTxt;
    public Text MenuTxt;

    //Окно проверьте подключение
    [Header("Проверьте подключение в интернету:")]
    public GameObject NoConnection;               // окно проверть подключение
    public Text NoConnectionTxt;

    CloseWindow SettingsClose;
    CloseWindow NoConnectionClose;

    public void Init(GameManager g = null, MenuScript m = null)
    {
#if !NO_FACEBOOK
        FB.Init();
#endif
        if (BuildSettings.Market != TargetMarket.AMAZON)
            GPGSLogin.Init();
#if NO_VK
        VKontakteBtn.SetActive(false);
        // float y = 25f/Screen.height * 600;;
        // FacebookBtn.position -= new Vector3(0,y,0);
        //  GooglePlayBtn.position += new Vector3(0, y, 0);
#endif
#if !NO_FACEBOOK
       FacebookBtn.SetActive(true);
#endif
#if NO_GPGS && NO_GC
        GooglePlayBtn.SetActive(false);
        ReviewAdressBtn.gameObject.SetActive(false);
        if(ReviewAdressBtn2!=null)
        ReviewAdressBtn2.gameObject.SetActive(false);
#endif
        SetEnableTransactionsRestoredBtn();
        // GPGSLogin.in
        // PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
        // PlayGamesPlatform.InitializeInstance(config);
        // PlayGamesPlatform.Activate();

        string currentLanguage = PlayerPrefs.GetString("ChibiLanguage");
        switch (currentLanguage)
        {
            case "RU":
                CurrentLanguage = 1;
                break;
            case "TH":
                CurrentLanguage = 2;
                break;
            case "JP":
                CurrentLanguage = 3;
                break;
            default:
                CurrentLanguage = 0;
                break;
        }
        AllLanguages[0] = "EN";
        AllLanguages[1] = "RU";
        AllLanguages[2] = "TH";
        AllLanguages[3] = "JP";
        if (SceneManager.GetActiveScene().name == "Menu")
            Menu = m;
        else
            GOD = g;

#if !NO_GPGS
        //if (GOD && !GOD.IsTutorial)
        //{
        //    SaveBtn.SetActive(true);
        //}
        //if (SceneManager.GetActiveScene().name == "Game")
        //{
        //    Mark.SetActive(false);
        //    if (GoogleAuthenticated)
        //    {
        //        if (SaveManager.GetCloudFileName(GOD.Save.CurrentSlot) != "")
        //        {
        //            Mark.SetActive(true);
        //            SaveManager.UseCloud = true;
        //        }
        //    }
        //}

        ChangeIcon1.sprite = GoogleSprite;
        ChangeIcon2.sprite = GoogleSprite;
#else
        ChangeIcon1.sprite = IosSprite;
        ChangeIcon2.sprite = IosSprite;
#endif
        SettingsClose = gameObject.GetComponent<CloseWindow>();
        NoConnectionClose = NoConnection.GetComponent<CloseWindow>();
        InitLanguage();
    }

    public void InitLanguage()
    {
        if (transactionsRestoredBtnText != null)
            transactionsRestoredBtnText.text = Localization.GetText("#TransactionsRestored");

        MainSettings.text = Localization.instance.getTextByKey("#Setting");
        LanguageName.text = Localization.instance.getTextByKey("#SettingLanguageName");
        Language.text = Localization.instance.getTextByKey("#SettingLanguage");
        Music.text = Localization.instance.getTextByKey("#SettingMusic");
        Sound.text = Localization.instance.getTextByKey("#SettingSound");
        VideoText.text = Localization.instance.getTextByKey("#SettingGraphic");
        ShadowText.text = Localization.instance.getTextByKey("#SettingShadow");
        Effects.text = Localization.instance.getTextByKey("#SettingEffects");
        CameraText.text = Localization.instance.getTextByKey("#SettingCamera");
        SocialText.text = Localization.instance.getTextByKey("#SettingSocial");
        We.text = Localization.instance.getTextByKey("#SettingWe");
        NewGameTxt.text = Localization.instance.getTextByKey("#SettingNewGame");
        ReviewTxt.text = Localization.instance.getTextByKey("#SettingReview");
        if (ReviewTxt2 != null)
            ReviewTxt2.text = Localization.instance.getTextByKey("#SettingReview");

        TopText.text = Localization.instance.getTextByKey("#RateTop");
        RateText.text = "";
        RateOkText.text = Localization.instance.getTextByKey("#RateOk");


        NoConnectionTxt.text = Localization.instance.getTextByKey("#NoConnection");

        if (GOD)
        {
//#if !NO_GPGS
//            SaveTxt.text = Localization.instance.getTextByKey("#SettingSave");
//#endif
            MenuTxt.text = Localization.instance.getTextByKey("#SettingMenu");
        }

        if (PlayerPrefs.HasKey("ChibiMusicVolume"))
            MusicVolume = PlayerPrefs.GetFloat("ChibiMusicVolume");
        else
            MusicVolume = 0.5f;
        MusicSlider.value = MusicVolume;
        if (GOD)
            GOD.Audio.ChangeMusicVolume();
        if (Menu)
            Menu.ChangeMusicVolume();
        if (MusicVolume == 0)
        {
            MusicOn.text = Localization.instance.getTextByKey("#SettingOff");
            MusicOnOff.color = redColor;
        }
        else
        {
            MusicOn.text = Localization.instance.getTextByKey("#SettingOn");
            MusicOnOff.color = greenColor;
        }
        if (PlayerPrefs.HasKey("ChibiSoundVolume"))
            SoundVolume = PlayerPrefs.GetFloat("ChibiSoundVolume");
        else
            SoundVolume = 0.5f;
        SoundSlider.value = SoundVolume;
        if (GOD)
            GOD.Audio.ChangeSoundVolume();
        if (Menu)
            Menu.ChangeSoundVolume();
        if (SoundVolume == 0)
        {
            SoundOn.text = Localization.instance.getTextByKey("#SettingOff");
            SoundOnOff.color = redColor;
        }
        else
        {
            SoundOn.text = Localization.instance.getTextByKey("#SettingOn");
            SoundOnOff.color = greenColor;
        }
        //__________________________________________________________________________
        GraphicText.text = Localization.instance.getTextByKey("#SettingGraphic" + GraphicsLevel);

        if (PlayerPrefs.HasKey("ChibiGraphic"))
        {
            GraphicsLevel = PlayerPrefs.GetInt("ChibiGraphic");
            ShadowsOn = PlayerPrefsX.GetBool("ChibiShadow");
        }
        else
        {
            GraphicsLevel = 1;
            ShadowsOn = false;
            /* int height = Screen.height;
             int width = Screen.width;
             if ((width < 1280) && (height < 720))
                 GraphicsLevel = 0;
             if ((width < 1334) && (height < 750))
             {
                 GraphicsLevel = 1;
                 ShadowsOn = true;
             }
             if ((width < 1920) && (height < 1080))
                 GraphicsLevel = 2;*/
        }
        SetGraphics();
        SetGraphicsBtn();

        if (PlayerPrefs.HasKey("ChibiEffect"))
            EffectIsOn = PlayerPrefsX.GetBool("ChibiEffect");
        if (GOD)
            GOD.DopEff.EffectsOn = EffectIsOn;
        if (EffectIsOn)
            EffectsBtnI.color = blueColor;
        else
            EffectsBtnI.color = darkblueColor;


        if (PlayerPrefs.HasKey("ChibiCamera"))
            CameraIsOn = PlayerPrefsX.GetBool("ChibiCamera");
        if (GOD)
            GOD.CameraScript.UsialCamera = CameraIsOn;
        if (CameraIsOn)
            CameraBtnI.color = blueColor;
        else
            CameraBtnI.color = darkblueColor;

        //_________________________________________________________________
        if (PlayerPrefsX.GetBool("ChibiFacebook"))
        {
#if !NO_FACEBOOK
            if (FB.IsLoggedIn)
            {
                Facebook.color = greenColor;
                FacebookTxt.text = Localization.instance.getTextByKey("#SettingLoginYet");
            }
            else
            {
                Facebook.color = redColor;
                FacebookTxt.text = Localization.instance.getTextByKey("#SettingLogin");
            }
#endif
        }
        else
        {
            Facebook.gameObject.SetActive(false);
            FacebookTxt2.text = Localization.instance.getTextByKey("#SettingLogin");
            FacebookNever.SetActive(true);
        }

#if !NO_VK
        if (PlayerPrefsX.GetBool("ChibiVKontakte"))
        {
            if (VkApi.VkApiInstance.IsUserLoggedIn)
            {
                VKontakte.color = greenColor;
                VKontakteTxt.text = Localization.instance.getTextByKey("#SettingLoginYet");
            }
            else
            {
                VKontakte.color = redColor;
                VKontakteTxt.text = Localization.instance.getTextByKey("#SettingLogin");
            }
        }
        else
        {
            VKontakte.gameObject.SetActive(false);
            VKontakteTxt2.text = Localization.instance.getTextByKey("#SettingLogin");
            VKontakteNever.SetActive(true);
        }
#endif
        if (PlayerPrefsX.GetBool("ChibiPlayService"))
        {
            GooglePlay.gameObject.SetActive(false);
            if (Social.localUser.authenticated)
            {
                GooglePlay.color = greenColor;
                GooglePlayTxt.text = Localization.instance.getTextByKey("#SettingLoginYet");
                //  OnOffCloud.interactable = true;
            }
            else
            {
                GooglePlay.color = redColor;
                GooglePlayTxt.text = Localization.instance.getTextByKey("#SettingLogin");
                // OnOffCloud.interactable = false;
            }
        }
        else
        {
            GooglePlay.gameObject.SetActive(false);
            GooglePlayTxt2.text = Localization.instance.getTextByKey("#SettingLogin");
            GooglePlayNever.SetActive(true);
            //OnOffCloud.interactable = false;
        }

        //_________________________________________________________________

        // AfterRateText.text = Localization.instance.getTextByKey("#RateBad")+"\n" +ReviewAdress;

    }
    public void OpenSettingsWindow()
    {
        GOD.GamePause();
        if (SettingsBlack)
            SettingsBlack.SetActive(true);
        SettingsWindow.transform.localScale =Constants.SettingsWindow;
        SettingsWindow.SetActive(true);
    }

    public void CloseSettingsWindow()
    {
        GOD.GamePlay();
        if (SettingsBlack)
            SettingsBlack.SetActive(false);
        SettingsClose.StartClose();
        //SettingsWindow.SetActive(false);
    }

    // Восстановление покупки
    public void SetEnableTransactionsRestoredBtn()
    {
        if (transactionsRestoredBtn != null)
            transactionsRestoredBtn.SetActive(BuildSettings.Market == TargetMarket.IOS);
    }

    public void TransactionsRestoredBtnClick()
    {
        MyIAP.instance.RestorePurchase();
    }


    //ЯЗЫК________________________________________________________________________________________________________________
    public void ChangeLanguage()
    {
        CurrentLanguage++;
        if (CurrentLanguage >= AllLanguages.Length)
            CurrentLanguage = 0;
        Localization.instance.SetLanguage(AllLanguages[CurrentLanguage]);
        PlayerPrefs.SetString("ChibiLanguage", Localization.instance.CurrentLanguage);
        if (GOD)
        {
            InitLanguage();
            //иконки
            for (int i = 0; i < GOD.InventoryContr.AllIcons.Count; i++)
                GOD.InventoryContr.AllIcons[i].SetItems();
            for (int i = 0; i < GOD.InventoryContr.AllBags.Count; i++)
            {
                List<InfoScript> Icons = GOD.InventoryContr.AllBags[i].GetComponent<AfterDeadBag>().MyIcons;
                for (int x = 0; x < Icons.Count; x++)
                    Icons[x].SetItems();
            }

            GOD.PlayerInter.InitLanguage();
            GOD.CraftContr.InitLanguage();
            GOD.ShopScript.InitLanguage();
            if (GOD.QuestContr.IsInit)
                GOD.QuestContr.InitLanguage();
            for (int i = 0; i < GOD.ShopScript.AllShopItems.Count; i++)
                GOD.ShopScript.AllShopItems[i].InitLanguage();
            GOD.MoneyNo.Init(GOD);
            GOD.Map.InitLanguage();
            GOD.MoneyNo.InitLanguage();
            GOD.Offers.InitLanguage();
        }
        else if (Menu)
        {
            InitLanguage();
            Menu.InitLanguage();
        }
    }

    //МУЗЫКА________________________________________________________________________________________________________________
    public void OnOffMusic()      //меняем при нажатии на кнопку
    {
        if (MusicVolume == 0)
        {
            MusicVolume = 0.5f;
            if (Menu)
                Menu.ChangeMusicVolume();
            else
                GOD.Audio.ChangeMusicVolume();
            SetOnOffMusic();
        }
        else
        {
            MusicVolume = 0f;
            if (Menu)
                Menu.ChangeMusicVolume();
            else
                GOD.Audio.ChangeMusicVolume();
            SetOnOffMusic();
        }
        PlayerPrefs.SetFloat("ChibiMusicVolume", MusicVolume);
    }

    public void SetOnOffMusic()   //отображаем вкл и выкл
    {
        if (MusicVolume == 0)
        {
            MusicOn.text = Localization.instance.getTextByKey("#SettingOff");
            MusicOnOff.color = redColor;
        }
        else
        {
            if (MusicOnOff.color != greenColor)
            {
                MusicOn.text = Localization.instance.getTextByKey("#SettingOn");
                MusicOnOff.color = greenColor;
            }
        }
    }

    public void OnOffSound()      //меняем при нажатии на кнопку
    {
        if (SoundVolume == 0)
        {
            SoundVolume = 0.5f;
            if (Menu)
                Menu.ChangeSoundVolume();
            else
                GOD.Audio.ChangeSoundVolume();
            SetOnOffSound();
        }
        else
        {
            SoundVolume = 0f;
            if (Menu)
                Menu.ChangeSoundVolume();
            else
                GOD.Audio.ChangeSoundVolume();
            SetOnOffSound();
        }
        PlayerPrefs.SetFloat("ChibiSoundVolume", SoundVolume);
    }

    public void SetOnOffSound()   //отображаем вкл и выкл
    {
        if (SoundVolume == 0)
        {
            SoundOn.text = Localization.instance.getTextByKey("#SettingOff");
            SoundOnOff.color = redColor;
        }
        else
        {
            if (SoundOnOff.color != greenColor)
            {
                SoundOn.text = Localization.instance.getTextByKey("#SettingOn");
                SoundOnOff.color = greenColor;
            }
        }
    }

    public void ChangeMusicVolumeOnSlider()
    {
        MusicVolume = MusicSlider.value;
        if (Menu)
            Menu.ChangeMusicVolume();
        else
            GOD.Audio.ChangeMusicVolume();
        SetOnOffMusic();
        PlayerPrefs.SetFloat("ChibiMusicVolume", MusicVolume);
    }
    public void ChangeSoundVolumeOnSlider()
    {
        SoundVolume = SoundSlider.value;
        if (Menu)
            Menu.ChangeSoundVolume();
        else
            GOD.Audio.ChangeSoundVolume();
        SetOnOffSound();
        PlayerPrefs.SetFloat("ChibiSoundVolume", SoundVolume);
    }


    //ГРАФИКА____________________________________________________________________
    public void Graphics()
    {
        GraphicsLevel++;
        if (GraphicsLevel > 2)
            GraphicsLevel = 0;
        SetGraphics();
        SetGraphicsBtn();
        PlayerPrefs.SetInt("ChibiGraphic", GraphicsLevel);
    }

    public void SetGraphics()
    {
        if (Menu)
        {
            QualitySettings.SetQualityLevel(2, true);
        }
        else
        {
            if (GraphicsLevel == 0)
            {
                QualitySettings.SetQualityLevel(GraphicsLevel, true);
                // Camera.main.GetComponent<AmplifyColorEffect>().enabled = false;
            }
            else
            {
                if (ShadowsOn)
                {
                    if (GraphicsLevel == 1)
                    {
                        QualitySettings.SetQualityLevel(3, true);
                        // Camera.main.GetComponent<AmplifyColorEffect>().enabled = false;
                    }
                    if (GraphicsLevel == 2)
                    {
                        QualitySettings.SetQualityLevel(4, true);
                        //Camera.main.GetComponent<AmplifyColorEffect>().enabled = true;
                    }
                }
                else
                    QualitySettings.SetQualityLevel(GraphicsLevel, true);
            }
        }
    }
    public void SetGraphicsBtn()
    {
        GraphicText.text = Localization.instance.getTextByKey("#SettingGraphic" + GraphicsLevel);
        if (GraphicsLevel == 0)
            ShadowBtn.interactable = false;
        else
        {
            ShadowBtn.interactable = true;
            if (ShadowsOn)
                ShadowBtnI.color = blueColor;
            else
                ShadowBtnI.color = darkblueColor;
        }

    }
    public void Shadows()
    {
        ShadowsOn = !ShadowsOn;
        if (GOD)
            SetGraphics();
        SetGraphicsBtn();
        PlayerPrefsX.SetBool("ChibiShadow", ShadowsOn);
    }

    public void EffectsOnOff()
    {
        EffectIsOn = !EffectIsOn;
        if (GOD)
            GOD.DopEff.ChangeOnOff(EffectIsOn);
        if (EffectIsOn)
            EffectsBtnI.color = blueColor;
        else
            EffectsBtnI.color = darkblueColor;
        PlayerPrefsX.SetBool("ChibiEffect", EffectIsOn);
    }
    public void CameraOnOff()
    {
        CameraIsOn = !CameraIsOn;
        if (GOD)
            GOD.CameraScript.UsialCamera = CameraIsOn;
        if (CameraIsOn)
            CameraBtnI.color = blueColor;
        else
            CameraBtnI.color = darkblueColor;
        PlayerPrefsX.SetBool("ChibiCamera", CameraIsOn);

    }

    //ИНТЕГРАЦИЯ____________________________________________________________________

    public void LoginFacebook()
    {
#if !NO_FACEBOOK
        if (FB.IsLoggedIn)
        {
            FB.LogOut();
            Facebook.color = redColor;
            FacebookTxt.text = Localization.instance.getTextByKey("#SettingLogin");
        }
        else
        {
            FB.LogInWithPublishPermissions(null, AfterFacebook);
            LoadWindow.SetActive(true);
        }
#endif
    }
#if !NO_FACEBOOK
    void AfterFacebook(ILoginResult r)
    {
        LoadWindow.SetActive(false);
        if (FB.IsLoggedIn)
        {
            Facebook.color = greenColor;
            FacebookTxt.text = Localization.instance.getTextByKey("#SettingLoginYet");
            if (!PlayerPrefsX.GetBool("ChibiFacebook"))
            {
                Balance.Gem = Balance.Gem + 15;
                FacebookNever.SetActive(false);
                Facebook.gameObject.SetActive(true);
                PlayerPrefsX.SetBool("ChibiFacebook", true);
            }
        }
    }
#endif
#if !NO_VK
    public void LoginVKontakte()
    {
        if (VkApi.VkApiInstance.IsUserLoggedIn)
        {
            VkApi.VkApiInstance.LoggedOut += AfterVkLogOut;
            VkApi.VkApiInstance.Logout();
        }
        else
        {
            VkApi.VkApiInstance.LoggedIn += AfterVKLogin;
            VkApi.VkApiInstance.Login();
        }
    }

    void AfterVKLogin()
    {
        VKontakte.color = greenColor;
        VKontakteTxt.text = Localization.instance.getTextByKey("#SettingLoginYet");
        if (!PlayerPrefsX.GetBool("ChibiVKontakte"))
        {
            Balance.Gem = Balance.Gem + 15;
            VKontakteNever.SetActive(false);
            VKontakte.gameObject.SetActive(true);
            PlayerPrefsX.SetBool("ChibiVKontakte", true);
        }
        VkApi.VkApiInstance.LoggedIn -= AfterVKLogin;
    }
    void AfterVkLogOut()
    {

        VKontakte.color = redColor;
        VKontakteTxt.text = Localization.instance.getTextByKey("#SettingLogin");
        VkApi.VkApiInstance.LoggedOut -= AfterVkLogOut;
    }
#endif

    string NeedToShow = "";
    public void LoginPlayServises(string show = "")
    {
        if (Social.localUser.authenticated)
        {
#if !NO_GPGS

            //PlayGamesPlatform.Instance.SignOut();
            GooglePlay.color = redColor;
            GooglePlayTxt.text = Localization.instance.getTextByKey("#SettingLogin");
            //Mark.SetActive(false);
#endif
        }
        else
        {
            if (Menu)
            {
                if (NeedToShow == "")
                    LoadWindow.transform.position = Menu.NoConnectionPos1.position;
                else
                    LoadWindow.transform.position = Menu.NoConnectionPos2.position;
            }
            LoadWindow.SetActive(true);
            NeedToShow = show;
            //  PlayGamesPlatform.Instance.Authenticate(AfterLoginPlayServises, false);
            Social.localUser.Authenticate(AfterLoginPlayServises);
        }
    }
    void AfterLoginPlayServises(bool authenticated)
    {
        LoadWindow.SetActive(false);
        if (Social.localUser.authenticated)
        {
            if (GOD)
            {
                GOD.Progress.LoadAchivments();
                GOD.Progress.SetAchivments(1);
                //if (SceneManager.GetActiveScene().name == "Game")//галочка сохранения на облако
                //{
                //    if (SaveManager.GetCloudFileName(GOD.Save.CurrentSlot) != "")
                //    {
                //        Mark.SetActive(true);
                //        SaveManager.UseCloud = true;
                //    }
                //}
            }
            if (Menu)
            {
                Menu.Progress.LoadAchivments();
                Menu.Progress.SetAchivments(1);
            }
            GooglePlay.color = greenColor;
            GooglePlayTxt.text = Localization.instance.getTextByKey("#SettingLoginYet");
            if (!PlayerPrefsX.GetBool("ChibiPlayService"))
            {
                Balance.Gem = Balance.Gem + 15;
                GooglePlayNever.SetActive(false);
                GooglePlay.gameObject.SetActive(true);
                PlayerPrefsX.SetBool("ChibiPlayService", true);
            }
            // print(NeedToShow + " " + GOD + " " + Menu);
            if (NeedToShow == "Achivments")
            {
                if (GOD)
                    GOD.Progress.ShowAchivment();
                if (Menu)
                    Menu.Progress.ShowAchivment();
            }
            if (NeedToShow == "Lidearbords")
            {
                if (GOD)
                    GOD.Progress.ShowLeaderbords();
                if (Menu)
                    Menu.Progress.ShowLeaderbords();
            }
            if (NeedToShow == "Save")
            {
                Save();
            }
            NeedToShow = "";
        }
        else
            OpenNoConnection(true);
    }

    public static bool GoogleAuthenticated
    {
        get
        {
            return Social.localUser.authenticated;
        }
    }


    public void BtnShowSavedGame()
    {
        if (Menu)
            Menu.MenuSnd.PlayOneShot(Menu.BtnSound);
#if !NO_GPGS
        if (!Social.localUser.authenticated) return;
        PlayGamesPlatform.Instance.SavedGame.ShowSelectSavedGameUI(
            "Saved Game UI",
            10,
            false,
            true,
            (status, savedGame) =>
            {

            });
#endif
    }

    public void CloseLoad()
    {
        LoadWindow.SetActive(false);
    }

    //ССЫЛКИ____________________________________________________________________
    public void OurVk()
    {
        MyOpenURL.OpenUrlVKontakte();
    }
    public void OurFacebook()
    {
        MyOpenURL.OpenUrlFacebook();
    }
    public void NewGames()
    {
        MyOpenURL.OpenUrlMoreApps();
    }

    public void OpenRate()
    {
        RateButton.interactable = false;
        CurrentStars = 0;
        for (int i = 0; i < Stars.Length; i++)   //включаем или выключаем звезды
            Stars[i].SetActive(false);
        RateWindow.SetActive(true);
    }
    public void CloseRate()
    {
        RateWindow.SetActive(false);
    }
    public void SetStar(int n)
    {
        for (int i = 0; i < Stars.Length; i++)   //включаем или выключаем звезды
        {
            if (i <= n)
                Stars[i].SetActive(true);
            else
                Stars[i].SetActive(false);
        }
        RateText.text = Localization.instance.getTextByKey("#Rate" + n);
        RateButton.interactable = true;
        CurrentStars = n;
    }
    public void Rate()
    {
        if (CurrentStars >= 3)
        {
            CloseRate();
            MyOpenURL.OpenUrlReview();
        }
        else
        {
            CloseRate();
            AfterRateText.text = Localization.instance.getTextByKey("#RateBad") + "/n" + GameURL.urlFBGroup;
            AfterRateWindow.SetActive(true);
        }
    }
    public void CloseAfterRateWindow()
    {
        AfterRateWindow.SetActive(false);
    }

    //КНОПКИ____________________________________________________________________
    public void Save()
    {
        // Debug.Log(SaveManager.GetCloudFileName(GOD.Save.CurrentSlot));
#if !NO_GPGS
        //if (GoogleAuthenticated)
        //{
        //    if (SaveManager.GetCloudFileName(GOD.Save.CurrentSlot) == "")
        //    {
        //        GPGSSave.SelectSlotToSave(CallBackFileName);
        //    }
        //    else
        //    {
        //        bool b= SaveManager.UseCloud;
        //        SaveManager.UseCloud = !b;
        //        Mark.SetActive(!b);
        //    }
        //}
        //else
        //{
            //Mark.SetActive(false);
            LoginPlayServises("Save");
        //}
#endif
    }

    //void CallBackFileName(string fileName)
    //{
    //    if ((fileName != null) && (fileName.Length > 0))
    //    {
    //        SaveManager.SetCloudFileName(GOD.Save.CurrentSlot, fileName);
    //        GOD.Save.SaveGame(false);
    //        Mark.SetActive(true);
    //    }
    //    else
    //    {
    //        SaveManager.SetCloudFileName(GOD.Save.CurrentSlot, "");
    //        Mark.SetActive(false);
    //    }
    //}

    public void LoadMenu()
    {
        GOD.Save.IsMenu = true;
        CloseSettingsWindow();
        if (GOD.IsTutorial)
            NowLoadMenu();
        else
            GOD.Save.SaveGame(false);
    }
    public void NowLoadMenu()
    {
        PlayerPrefs.SetString("ChibiLevel", "Menu");
        SceneManager.LoadScene("Load");
    }

    public void Share()
    {
//        Texture2D ShareImage = Resources.Load<Texture2D>("Share/ShareImage");
//        string ShareText;
//        string ShareAdress;
//        if (GOD.TG.IDay >= 100)
//            ShareAdress = "#Share13";
//        else if (GOD.TG.IDay >= 90)
//            ShareAdress = "#Share12";
//        else if (GOD.TG.IDay >= 80)
//            ShareAdress = "#Share11";
//        else if (GOD.TG.IDay >= 70)
//            ShareAdress = "#Share10";
//        else if (GOD.TG.IDay >= 60)
//            ShareAdress = "#Share9";
//        else if (GOD.TG.IDay >= 50)
//            ShareAdress = "#Share8";
//        else if (GOD.TG.IDay >= 40)
//            ShareAdress = "#Share7";
//        else if (GOD.TG.IDay >= 30)
//            ShareAdress = "#Share6";
//        else if (GOD.TG.IDay >= 20)
//            ShareAdress = "#Share5";
//        else if (GOD.TG.IDay >= 10)
//            ShareAdress = "#Share4";
//        else if (GOD.TG.IDay >= 5)
//            ShareAdress = "#Share3";
//        else if (GOD.TG.IDay >= 3)
//            ShareAdress = "#Share2";
//        else
//            ShareAdress = "#Share1";

//        ShareText = Localization.instance.getTextByKey(ShareAdress);
//#if !NO_SHARE
//        AndroidSocialGate.StartShareIntent("Share Chibi", ShareText + " " + Localization.instance.getTextByKey("#Share0") + " " + +GOD.TG.IDay + " " + Localization.instance.getTextByKey("#Dead2") + "!", ShareImage);
//#endif
    }

    //Видео недоступно___________________________________________________________________________________________________________
    public void OpenNoConnection(bool b)
    {
        if (b)
        {
            if (Menu)
            {
                if (NeedToShow == "")
                    NoConnection.transform.position = Menu.NoConnectionPos1.position;
                else
                    NoConnection.transform.position = Menu.NoConnectionPos2.position;
            }
            NoConnection.transform.localScale = Constants.NoConnection;
            NoConnection.SetActive(true);
        }
        else
        {
            NoConnectionClose.StartClose();
            if (GOD)
                GOD.GamePlay();
        }
    }


}
