﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemItem : MonoBehaviour {

    public string GemName;
    public int GemCount;
    public string GemCost;
}
