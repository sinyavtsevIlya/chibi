﻿using UnityEngine;
using System.Collections;

public class InstrumentChest : MonoBehaviour {

    public GameManager GOD;
    public DragHandler thisDrag;
    int PressCount = 0;
    string Resource = "";
    public string[] Instruments;
    public int[] InstrumentsCount;

    public void Init()
    {
        if (name.Contains("BigInstrumentChest"))
        {
            Instruments = new string[13];
            InstrumentsCount = new int[13];
            Instruments[0] = "IronAxe";
            Instruments[1] = "IronPick";
            Instruments[2] = "IronShovel";
            Instruments[3] = "StrongRod";
            Instruments[4] = "Bucket";
            Instruments[5] = "BoneAxe";
            Instruments[6] = "BonePick";
            Instruments[7] = "BoneShovel";
            Instruments[8] = "DurableRod";
            Instruments[9] = "DimondAxe";
            Instruments[10] = "DimondPick";
            Instruments[11] = "DimondShovel";
            Instruments[12] = "DimondRod";
            for (int i = 0; i < InstrumentsCount.Length; i++)
                InstrumentsCount[i] = 1;

        }
        else
        {
            Instruments = new string[17];
            InstrumentsCount = new int[17];
            Instruments[0] = "StoneAxe";
            Instruments[1] = "StonePick";
            Instruments[2] = "StoneShovel";
            Instruments[3] = "NormalRod";
            Instruments[4] = "IronAxe";
            Instruments[5] = "IronPick";
            Instruments[6] = "IronShovel";
            Instruments[7] = "StrongRod";
            Instruments[8] = "Bucket";
            Instruments[9] = "BoneAxe";
            Instruments[10] = "BonePick";
            Instruments[11] = "BoneShovel";
            Instruments[12] = "DurableRod";
            Instruments[13] = "DimondAxe";
            Instruments[14] = "DimondPick";
            Instruments[15] = "DimondShovel";
            Instruments[16] = "DimondRod";

            for (int i = 0; i < InstrumentsCount.Length; i++)
                InstrumentsCount[i] = 1;
        }

    }

    public void OnChestPress()
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)
            {
                OpenChest();
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount = 1;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }
    IEnumerator PressKd()
    {
        yield return new WaitForSeconds(0.2f);
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    public void OpenChest()
    {
        if (Resource == "")
            Resource = WhatResource();
        if (Resource != "")
        {
            if (GOD.InventoryContr.CanAddSeveral(1))
            {
                GOD.InventoryContr.AddToInventory(Resource, 1);
                GOD.InventoryContr.RemoveFromInventory(GetComponent<InfoScript>(), 1);
                GOD.PlayerInter.OpenInventory();
                GOD.PlayerInter.ShowNewItem(Resource, 1);
                Resource = "";
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.OpenChest);

            }
            else
                GOD.PlayerInter.OpenCantOpen();
        }
    }

    public string WhatResource()
    {
        int RandomX = 0;
        if (!name.Contains("BigInstrumentChest"))
        {
            RandomX = Random.Range(0, 101);
            if (RandomX <= 70)
            {
                RandomX = Random.Range(0, 5);
                if (RandomX < Instruments.Length)
                    return Instruments[RandomX];
                else
                    return "";
            }
        }

        RandomX = Random.Range(0, 101);
        if (RandomX <= 70)
        {
            RandomX = Random.Range(4, 9);
            if (RandomX < Instruments.Length)
                return Instruments[RandomX];
            else
                return "";
        }

        RandomX = Random.Range(0, 101);
        if (RandomX <= 70)
        {
            RandomX = Random.Range(9, 13);
            if (RandomX < Instruments.Length)
                return Instruments[RandomX];
            else
                return "";
        }


            RandomX = Random.Range(13, 16);
        if (RandomX < Instruments.Length)
            return Instruments[RandomX];
        else
            return "";



    }

}
