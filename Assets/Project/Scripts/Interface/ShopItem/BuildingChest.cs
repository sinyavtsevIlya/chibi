﻿using UnityEngine;
using System.Collections;

public class BuildingChest : MonoBehaviour {

    public GameManager GOD;
    public DragHandler thisDrag;
    int PressCount = 0;
    public string[] Buildings;
    public int[] BuildingsCount;

    public void Init()
    {
        if (name.Contains("BigBuildingChest"))
        {
            Buildings = new string[10];
            BuildingsCount = new int[10];
            Buildings[0] = "WoodBase";
            Buildings[1] = "WoodStair";
            Buildings[2] = "WoodFence";
            Buildings[3] = "WoodWall";
            Buildings[4] = "WoodWallDoor";
            Buildings[5] = "WoodWallWindow";
            Buildings[6] = "Door";
            Buildings[7] = "Window";
            Buildings[8] = "WoodRoof";
            Buildings[9] = "WoodRoofTop";

            for (int i = 0; i < Buildings.Length; i++)
            {
                    BuildingsCount[i] = 10;
            }
        }
        else
        {
            Buildings = new string[7];
            BuildingsCount = new int[7];
            Buildings[0] = "Smithy";
            Buildings[1] = "Windmill";
            Buildings[2] = "Stove";
            Buildings[3] = "Mannequie";
            Buildings[4] = "Firecamp";
            Buildings[5] = "Chest";
            Buildings[6] = "IceChest";

            for (int i = 0; i < Buildings.Length; i++)
                BuildingsCount[i] = 1;
        }
    }

    public void OnChestPress()
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)
            {
                OpenChest();
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount = 1;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }
    IEnumerator PressKd()
    {
        yield return new WaitForSeconds(0.2f);
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    public void OpenChest()
    {
        if (GOD.InventoryContr.CanAddSeveral(Buildings.Length))
        {
            for (int i = 0; i < Buildings.Length; i++)
            {
                GOD.InventoryContr.AddToInventory(Buildings[i], BuildingsCount[i]);
                GOD.PlayerInter.ShowNewItem(Buildings[i], BuildingsCount[i]);
            }
            GOD.InventoryContr.RemoveFromInventory(GetComponent<InfoScript>(), 1);
            GOD.PlayerInter.OpenInventory();
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.OpenChest);

        }
        else
            GOD.PlayerInter.OpenCantOpen();
    }
}
