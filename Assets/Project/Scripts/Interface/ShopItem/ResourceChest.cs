﻿using UnityEngine;
using System.Collections;

public class ResourceChest : MonoBehaviour {

    public GameManager GOD;
    public DragHandler thisDrag;
    int PressCount = 0;
    string Resource1="", Resource2="", Resource3="", Resource4 = "", Resource5 = "";
    int Count1 = 0, Count2 = 0, Count3 = 0, Count4 = 0, Count5 = 0;
    public int ItemCount = 0;
    public string[] UsualResource = new string[26];
    public int[] UsualResourceCount = new int[26];
    public string[] RareResource = new string[7];
    public int[] RareResourceCount = new int[7];

    public void Init()
    {
        RareResource[0] = "Emerald";
        RareResource[1] = "Sapphire";
        RareResource[2] = "Amethyst";
        RareResource[3] = "Ruby";
        RareResource[4] = "Dimond";
        RareResource[5] = "DimondPlate";
        RareResource[6] = "DimondCord";

        UsualResource[0] = "Leaf";
        UsualResource[1] = "Wood";
        UsualResource[2] = "Stick";
        UsualResource[3] = "Stone";
        UsualResource[4] = "Flint";
        UsualResource[5] = "Skin";
        UsualResource[6] = "Wool";
        UsualResource[7] = "Crystall";
        UsualResource[8] = "Snow";
        UsualResource[9] = "Ice";
        UsualResource[10] = "IronOre";
        UsualResource[11] = "CrabShell";
        UsualResource[12] = "Sand";
        UsualResource[13] = "Needle";
        UsualResource[14] = "Web";
        UsualResource[15] = "Thread";
        UsualResource[16] = "Rope";
        UsualResource[17] = "Cord";
        UsualResource[18] = "Plank";
        UsualResource[19] = "SharpStone";
        UsualResource[20] = "StoneBlock";
        UsualResource[21] = "IronIngot";
        UsualResource[22] = "BonePlate";
        UsualResource[23] = "ChitinPlate";
        UsualResource[24] = "Leather";
        UsualResource[25] = "Glass";

        for (int i = 0; i < UsualResourceCount.Length; i++)
        {
            if (i< 14)
                UsualResourceCount[i] = 10;
            else
                UsualResourceCount[i] = 5;
        }
        for (int i = 0; i < RareResourceCount.Length; i++)
        {
               RareResourceCount[i] = 1;
        }
    }

    public void OnChestPress()
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)
            {
                OpenChest();
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount++;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }
    IEnumerator PressKd()
    {
        yield return new WaitForSeconds(0.2f);
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    public void OpenChest()
    {
        if (Resource1 == "")
        {
            Resource1 = WhatResource();
            Count1 = ItemCount;
            Resource2 = WhatResource();
            Count2 = ItemCount;
            Resource3 = WhatResource();
            Count3 = ItemCount;
            if (name.Contains("Big"))
            {
                Resource4 = WhatResource();
                Count4 = ItemCount;
                Resource5 = WhatResource();
                Count5 = ItemCount;
            }
        }
        print(Resource1 + " " + Resource2 + " " + Resource3);
        int slots = 3;
        if (name.Contains("Big"))
            slots = 5;

            if (GOD.InventoryContr.CanAddSeveral(slots))
        {
            GOD.InventoryContr.AddToInventory(Resource1, Count1);
            GOD.InventoryContr.AddToInventory(Resource2, Count2);
            GOD.InventoryContr.AddToInventory(Resource3, Count3);
            if (name.Contains("Big"))
            {
                GOD.InventoryContr.AddToInventory(Resource4, Count4);
                GOD.InventoryContr.AddToInventory(Resource5, Count5);
            }
            GOD.InventoryContr.RemoveFromInventory(GetComponent<InfoScript>(), 1);
            GOD.PlayerInter.OpenInventory();
            GOD.PlayerInter.ShowNewItem(Resource1, Count1);
            GOD.PlayerInter.ShowNewItem(Resource2, Count2);
            GOD.PlayerInter.ShowNewItem(Resource3, Count3);
            if (name.Contains("Big"))
            {
                GOD.PlayerInter.ShowNewItem(Resource4, Count4);
                GOD.PlayerInter.ShowNewItem(Resource5, Count5);
            }
            Resource1 = "";
            Resource2 = "";
            Resource3 = "";
            if (name.Contains("Big"))
            {
                Resource4 = "";
                Resource5 = "";
            }
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.OpenChest);

        }
        else
            GOD.PlayerInter.OpenCantOpen();

    }
	public string WhatResource()
    {
        int RandomX = Random.Range(0, 101);
        if (RandomX >= 95) // редкие ресурсы
        {
            int ThisRandom = Random.Range(0, 8);
            ItemCount = 1;
            return RareResource[ThisRandom];
        }
        else  // обычные ресурсы
        {
            int ThisRandom = Random.Range(0, 26);
            if(ThisRandom <14)
                ItemCount = 10;
            else
                ItemCount = 5;
            return UsualResource[ThisRandom];

        }

    }
}
