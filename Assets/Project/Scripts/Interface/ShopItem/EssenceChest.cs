﻿using UnityEngine;
using System.Collections;

public class EssenceChest : MonoBehaviour {

    public GameManager GOD;
    public DragHandler thisDrag;
    int PressCount = 0;
    public string[] Essence = new string[3];
    public int[] EssenceCount = new int[3];

    public void Init()
    {
        Essence[0] = "EssenceSun";
        Essence[1] = "EssenceIce";
        Essence[2] = "EssenceWater";

        EssenceCount[0] = 10;
        EssenceCount[1] = 10;
        EssenceCount[2] = 10;
    }

    public void OnChestPress()
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (GOD.PlayerInter.Inventory.activeSelf)
            {
                if (PressCount > 0)
                {
                    OpenChest();
                    PressCount = 0;
                    StopAllCoroutines();
                }
                else
                {
                    PressCount = 1;
                    StopAllCoroutines();
                    StartCoroutine(PressKd());
                }
            }
        }
    }
    IEnumerator PressKd()
    {
        yield return new WaitForSeconds(0.2f);
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    public void OpenChest()
    {
        if (GOD.InventoryContr.CanAddSeveral(Essence.Length))
        {
            for (int i = 0; i < Essence.Length; i++)
            {
                GOD.InventoryContr.AddToInventory(Essence[i], 10);
                GOD.PlayerInter.ShowNewItem(Essence[i], 10);
            }
            GOD.InventoryContr.RemoveFromInventory(GetComponent<InfoScript>(), 1);
            GOD.PlayerInter.OpenInventory();
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.OpenChest);

        }
        else
            GOD.PlayerInter.OpenCantOpen();
    }
}
