﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IconInGame : MonoBehaviour {

    GameManager GOD;

    public Image InGameImage;          //спрайт в слоте в игре
    public GameObject TextImage;       // текст с фоном
    public Text IconText;              // текст с количеством
    public Image Durability;           // прочность
    public Image Validity;           // годность
    public GameObject RealIcon;        // ссылка на реальну иконку
    public InfoScript RealIconScript;

    string type = "";

    public void Init()
    {
      
      GOD = GameObject.Find("GameManager").GetComponent<GameManager>();
        InGameImage = GetComponent<Image>();
        TextImage = transform.GetChild(2).gameObject;
        IconText = TextImage.transform.GetChild(0).GetComponent<Text>();
        Durability = transform.GetChild(0).GetComponent<Image>();
        Validity = transform.GetChild(1).GetComponent<Image>();

       // TextImage.gameObject.SetActive(false);
       // Durability.gameObject.SetActive(false);
       // Validity.gameObject.SetActive(false);
    }
    public void OutIcon()
    {
        RealIconScript.InGame = null;
        RealIconScript.SlotInGame = null;
        GOD.PI.DeletePreItem(RealIconScript.thisName);
        InGameImage.gameObject.SetActive(false);
        RealIconScript = null;
        RealIcon = null;
    }
    public void OutIconOld()
    {
        RealIconScript.SlotInGame.thisImage.color = new Color(1f, 1f, 1f, 1);
        InGameImage.gameObject.SetActive(false);
       RealIconScript = null;
        RealIcon = null;
    }
    public void SetIcon(GameObject prototype, Slot myparent)
    {
        bool IsFirstTime = true;
        if (RealIcon != prototype)
        {
            if (RealIconScript)                   //если в слоте что то лежит - сбрасываем это
            {
                InfoScript I = RealIconScript;
                RealIconScript.InGame.OutIcon();
                I.SlotInGame = null;
                I.InGame = null;
            }
            else if(prototype.GetComponent<InfoScript>().InGame==null)   //если в первый раз кладем
            {
                if (GOD.PlayerInter.OnGameSlotsCount < 3)
                    GOD.PlayerInter.OnGameSlotsCount++;
                if(GOD.QuestContr.IsInit && GOD.PlayerInter.OnGameSlotsCount>=3)
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.LeatherBelt);
            }
            RealIcon = prototype;
            RealIconScript = RealIcon.GetComponent<InfoScript>();
            InGameImage.gameObject.SetActive(true);
            if (RealIconScript.SlotInGame != null)          // если этот предмет уже раньше лежал в слоте
            {
                RealIconScript.InGame.OutIconOld();
                RealIconScript.InGame = null;
                RealIconScript.SlotInGame=null;
                IsFirstTime = false;
            }
            InGameImage.sprite = prototype.GetComponent<Image>().sprite;
            RealIconScript.SlotInGame = myparent;
            RealIconScript.InGame = this;
           // RealIconScript.DurabilityImage= Durability;
            if (RealIconScript.MaxResourceCount>1)
            {
                TextImage.SetActive(true);
                ShowCount();
            }
            else
                TextImage.SetActive(false);
         
            if (RealIconScript.DurabilityImage.gameObject.activeSelf)
            {
                Durability.fillAmount = RealIconScript.DurabilityImage.fillAmount;
                Durability.gameObject.SetActive(true);
            }
            else
                Durability.gameObject.SetActive(false);

            if (RealIconScript.ValidityImage.gameObject.activeSelf)
            {
                Validity.fillAmount = RealIconScript.ValidityImage.fillAmount;
                Validity.gameObject.SetActive(true);
            }
            else
                Validity.gameObject.SetActive(false);

            if((RealIconScript.type == InfoScript.ItemType.clothes)|| (RealIconScript.type == InfoScript.ItemType.weapon)|| (RealIconScript.type == InfoScript.ItemType.instrument)|| (RealIconScript.type == InfoScript.ItemType.rod))
            {
                if (RealIconScript.thisName.Contains("Mantle"))
                    type = "useMantle";
                else
                {
                    type = "useIt";
                    if (IsFirstTime)
                    {
                        if (RealIconScript.NowOnPlayer)
                            GOD.PI.AddPreItem(GOD.PlayerAnimation.ItemOnHand);
                        else
                            GOD.PI.CreatePreItem(RealIconScript.thisName, RealIconScript.type);
                    }
                }
            }
            else if (RealIconScript.type == InfoScript.ItemType.building)
                type = "buildIt";
            else if (RealIconScript.type == InfoScript.ItemType.construction)
                type = "putIt";
            else if (RealIconScript.type == InfoScript.ItemType.food)
                type = "eatIt";
            RealIconScript.OnorOutIcon();
        }
    }

    public void ShowCount()  // отрисовываем количество на иконке
    {
        IconText.text = "" + RealIconScript.ResourceCount;
    }
    public void IconOn()                                           // при нажатии на иконку в панели быстрого доступа
    {
        switch (type)
        {
            case "useIt":
                GameObject[] G = GOD.PlayerContr.EqupmentSlots;
                for (int i = 0; i < G.Length; i++)
                {
                    if (G[i].name == RealIconScript.OnPlayerSlot)
                    {
                        if (RealIconScript.ParentSlot.name != G[i].name)                         // если этак иконка еще не лежит в слоте - переместить
                        {
                            if (G[i].transform.childCount > 0)                                      // если в слоте есть иконка - убираем ее
                            {
                                Transform child = G[i].transform.GetChild(0);
                                Transform p = RealIcon.transform.parent;
                                child.SetParent(p);
                                InfoScript oldInfo = child.GetComponent<InfoScript>();
                                oldInfo.ParentSlot = p;
                                GOD.PlayerContr.RemovePlayerParametre(child.gameObject, false);
                                GOD.InventoryContr.ShowIcons(GOD.InventoryContr.SlotInBag);
                            }
                            RealIcon.transform.SetParent(G[i].transform);
                            RealIconScript.ParentSlot = G[i].transform;
                            GOD.PlayerContr.SetPlayerParametre(RealIcon, true);
                        }
                        else                                                                      // иначе снять ее
                        {
                            if (GOD.InventoryContr.CanAddSeveral(1))
                            {
                                RealIcon.transform.SetParent(null);
                                RealIconScript.ParentSlot = null;
                                GOD.PlayerContr.RemovePlayerParametre(RealIcon, true);
                                GOD.InventoryContr.ShowIcons(GOD.InventoryContr.SlotInBag);
                                if (GOD.PlayerInter.Inventory.activeSelf)
                                    GOD.PlayerInter.OpenInventory();
                                else
                                {
                                    GOD.PlayerInter.OpenInventory();
                                    GOD.PlayerInter.CloseInventoryImmediatly();
                                }
                            }
                            else                                                    // не можем снять
                            {
                                if(GOD.PlayerInter.Inventory.activeSelf)
                                    GOD.PlayerInter.OpenCantOpen();
                                else
                                    GOD.DialogS.ShowDialog("CantAdd");
                            }
                        }
                    }
                }
                break;
            case "useMantle":

                if (RealIconScript.NowOnPlayer)       //снять плащ
                {
                    if (GOD.InventoryContr.CanAddSeveral(1))
                    {
                        RealIcon.transform.SetParent(null);
                        RealIconScript.ParentSlot = null;
                        GOD.PlayerContr.RemovePlayerParametre(RealIcon, true);
                        GOD.InventoryContr.ShowIcons(GOD.InventoryContr.SlotInBag);
                    }
                    else
                    {
                        if (GOD.PlayerInter.Inventory.activeSelf)
                            GOD.PlayerInter.OpenCantOpen();
                        else
                            GOD.DialogS.ShowDialog("CantAdd");
                    }
                }
                else                //надеть плащ
                {
                    Transform[] slots = new Transform[3];
                    int x = 0;
                    for (int i = 0; i < GOD.PlayerContr.EqupmentSlots.Length; i++)
                    {
                        Transform t = GOD.PlayerContr.EqupmentSlots[i].transform;
                        if (t.name.Contains("Mantle"))
                        {
                            slots[x] = t;
                            x++;
                        }
                    }

                    for (int i = 0; i < slots.Length; i++)  //если слоты с плащами пусты
                    {
                        if (slots[i].childCount <= 0)
                        {
                            RealIcon.transform.SetParent(slots[i]);
                            RealIconScript.ParentSlot = slots[i];
                            GOD.PlayerContr.SetPlayerParametre(RealIcon, true);
                            return;
                        }
                    }
                }
                break;
            case "eatIt":
                RealIcon.GetComponent<FoodScript>().BtnDown();
                break;
            case "buildIt":
               // GOD.PlayerInter.Build(RealIcon.name, RealIconScript);
                 GOD.PlayerInter.StartBuilding(RealIcon.name,RealIconScript);
                break;
            case "putIt":
                GOD.PlayerContr.InPut = true;
                GOD.PlayerInter.Build(RealIcon.name, RealIconScript);
                break;
        }
       
    }
}



