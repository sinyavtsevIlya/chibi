using System.Collections;
using UnityEngine;
using System;

namespace Game.UI
{

    public class ScreenExtensions : MonoBehaviour
    {
        private const float interval = 1f;

        #region Instance
        private static ScreenExtensions _instance;
        public static ScreenExtensions Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject("ScreenExtensions");
                    DontDestroyOnLoad(go);

                    _instance = go.AddComponent<ScreenExtensions>();
                }
                return _instance;
            }
        }
        #endregion

        public static event Action ScreenOrientationChanged;
        public static event Action ScreenResolutionsChanged;

        private ScreenOrientation _screenOrientation;

        private int _screenWidth = Screen.width;
        private int _screenHeight = Screen.height;

        public void Create() { }

        private void Start()
        {
            StartCoroutine(Loop());
        }

        private IEnumerator Loop()
        {
            _screenOrientation = Screen.orientation;

            _screenWidth = Screen.width;
            _screenHeight = Screen.height;

            var waitForSecondsRealtime = new WaitForSecondsRealtime(interval);
            while (true)
            {
                yield return waitForSecondsRealtime;

                var newScreenOrientation = Screen.orientation;
                if (_screenOrientation != newScreenOrientation)
                {
                    _screenOrientation = newScreenOrientation;
                    ScreenOrientationChanged?.Invoke();
                }

                var newScreenWidth = Screen.width;
                var newScreenHeight = Screen.height;
                if (_screenWidth != newScreenWidth || _screenHeight != newScreenHeight)
                {
                    _screenWidth = newScreenWidth;
                    _screenHeight = newScreenHeight;

                    ScreenResolutionsChanged?.Invoke();
                }


            }
        }
    }
}




