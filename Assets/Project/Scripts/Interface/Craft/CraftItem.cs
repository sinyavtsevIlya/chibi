﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class CraftItem {
    [System.NonSerialized]
    public bool IsExist = false; //существует ли данный объект
    public string ItemName;
    public int ItemCount;
    [System.NonSerialized]
    public GameObject ItemPlace; //где крафтится
    public float ItemTimeCraft;// сколько будет идти крафт
    public float ItemTimeStop;// когда по мировому времени закончится крафт
    public float ItemfillAmount = 0;
    [System.NonSerialized]
    public bool IsNowShow = false;


    public void SetCraftItem(string Name, int Count, GameObject Place, float iTime1,float iTime2)
    {
        ItemName = Name;
        ItemCount = Count;
        ItemPlace = Place;
        ItemTimeCraft = iTime1;
        ItemTimeStop = iTime2;
        ItemfillAmount = 0;
    }

    public void SetItemfillAmount()
    {
        //ItemfillAmount += ((100f / ItemTime) / 1000) * (1f / ItemCount);
          ItemfillAmount += ((1f / ItemTimeCraft) / 10f);
    }
}
