﻿using UnityEngine;
using System.Collections;

public class CraftResource : MonoBehaviour {

    public string ResName = "";
    public Sprite ResSprite;
    public string ResNameText = "";
    public string ResDescription = "";

    public void Init()
    {
        ResName = "";
        ResSprite=null;
        ResNameText = "";
        ResDescription = "";
    }
}
