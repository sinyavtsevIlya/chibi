﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftWindow : MonoBehaviour {

    public GameManager GOD;
    public CraftController CraftContr;
    [Header("Окно:")]
    public bool IsBuildingWindow = false;
    public GameObject CraftWindoww;//полностью окно крафта
    public GameObject CraftBlack; 
    public GameObject WindowForCraft, ResInfo;//окно с крафтом, окно с инфо
    public CloseWindow CloseCraftS; //скрипт закрывающий окно
    [Header("Рецепты:")]
    public Transform RecipeSlotsParent; //родитель рецептов в окне
    public RectTransform Content;         // окно рецептов
    public Transform[] RecipeSlots;    // слоты в окне рецептов
    [Header("Окно крафта:")]
    public Transform Res1;
    public Transform Res2, Res3, Result;    // окошки с ресурсами и результатом
    public CraftResource CraftRes1, CraftRes2, CraftRes3, CraftResult;
    Image IRes1, IRes2, IRes3, IResult;
    Text TResult, TCur1, TCur2, TCur3;
    public Text NameCur1, NameCur2, NameCur3, NameRes;
    public Text CraftButtonText; // текст на кнопке крафта
    public GameObject Plus, Minus, CraftButton, CraftPanel;
    GameObject theirParent;
    public Selectable CraftButtonSelectable, PlusSelectable, MinusSelectable;
    public Image CraftTime;
    public Text TimeText;
    public Text BuildingName; //название здания в окне кратфа зданий

    public Text ResNameT, ResDescriptionT;
    public Image ResImage;

    public void Init()
    {
        RecipeSlots = new Transform[RecipeSlotsParent.childCount];
        for (int i = 0; i < RecipeSlotsParent.childCount; i++)                 // инициализация слотов рецептов
            RecipeSlots[i] = RecipeSlotsParent.GetChild(i);

        IRes1 = Res1.GetChild(0).GetComponent<Image>();
        IRes2 = Res2.GetChild(0).GetComponent<Image>();
        IRes3 = Res3.GetChild(0).GetComponent<Image>();
        IResult = Result.GetChild(0).GetComponent<Image>();
        TResult = Result.GetChild(2).GetComponent<Text>();
        TCur1 = Res1.GetChild(2).GetComponent<Text>();
        TCur2 = Res2.GetChild(2).GetComponent<Text>();
        TCur3 = Res3.GetChild(2).GetComponent<Text>();

        CraftButtonSelectable = CraftButton.GetComponent<Selectable>();
        PlusSelectable = Plus.GetComponent<Selectable>();
        MinusSelectable = Minus.GetComponent<Selectable>();

        theirParent = CraftButton.transform.parent.gameObject;
        CraftTime.fillAmount = 0;
    }

    public void OpenCraft(string s)//открытие окна
    {
        if(CraftWindoww.activeSelf)
            CloseImmediatly();
        CraftWindoww.transform.localScale = Constants.CraftScale;
        CraftWindoww.SetActive(true);
        CraftBlack.SetActive(true);
        ResInfo.SetActive(false);
        WindowForCraft.SetActive(false);
        if (IsBuildingWindow)
        {
            Plus.SetActive(false);
            Minus.SetActive(false);
            BuildingName.text = Localization.instance.getTextByKey("#Name"+s);
        }
    }
    public void ClearCraft()
    {
        Res1.gameObject.SetActive(false);
        Res2.gameObject.SetActive(false);
        Res3.gameObject.SetActive(false);
        Result.gameObject.SetActive(false);
        NameCur1.text = "";
        NameCur2.text = "";
        NameCur3.text = "";
        NameRes.text = "";
        Plus.SetActive(false);
        Minus.SetActive(false);
        theirParent.SetActive(false);
        CraftPanel.SetActive(false);
    }

    public void CloseCraft()
    {
        CraftContr.CloseCraft();
        CraftBlack.SetActive(false);
        CloseCraftS.StartClose();
       // GOD.CraftContr.CloseCraft();
       // if (!GOD.CraftContr.StartCraft)
          //  GOD.CraftContr.CurrentBuild = null;
    }
    public void CloseImmediatly()
    {
       // CraftContr.CloseCraft();
        CraftBlack.SetActive(false);
        CloseCraftS.Close();
    }
    public void StartCraft(RecipeScript R, int CraftCount)
    {
        PlusSelectable.interactable = false;
        MinusSelectable.interactable = false;
        CraftButtonSelectable.interactable = false;
        switch (R.NumberOfIngridient.Length)                                      // в зависимости от количества ресурсов активируем иконки и показываем количество
        {
            case 1:
                TCur2.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[0]) + "/" + R.NumberOfIngridient[0] * CraftCount;
                break;
            case 2:
                TCur1.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[0]) + "/" + R.NumberOfIngridient[0] * CraftCount;
                TCur3.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[1]) + "/" + R.NumberOfIngridient[1] * CraftCount;
                break;
            case 3:
                TCur1.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[0]) + "/" + R.NumberOfIngridient[0] * CraftCount;
                TCur2.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[1]) + "/" + R.NumberOfIngridient[1] * CraftCount;
                TCur3.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[2]) + "/" + R.NumberOfIngridient[2] * CraftCount;
                break;
        }
    }
#region Сортировка
    public void RecipePlace(List<RecipeScript> AviableRecipe, List<RecipeScript> FutureRecipe, string CurrentType) // выставляем доступные рецепты            
    {
        int ContentScaler = 0;
        Content.sizeDelta = new Vector2(Content.sizeDelta.x, 120);
        for (int i = 0; i < AviableRecipe.Count; i++)                 // достпуный крафт
        {
            if (IsBuildingWindow || AviableRecipe[i].type == CurrentType || CurrentType == "All" )
            {
                Transform Slot = FreeSlot();
                Slot.gameObject.SetActive(true);
                AviableRecipe[i].transform.SetParent(Slot);
                AviableRecipe[i].transform.localScale = Constants.Recipe;
                AviableRecipe[i].GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                ContentScaler++;
                if (ContentScaler > 4)
                {
                    Content.sizeDelta = Content.sizeDelta + new Vector2(0, 120);                         // длина прокрутки
                    ContentScaler = 0;
                }
            }
        }
        for (int i = 0; i < FutureRecipe.Count; i++)            // недоступный крафт
        {
            if (IsBuildingWindow || FutureRecipe[i].type == CurrentType || CurrentType == "All")
            {
                Transform Slot = FreeSlot();
                Slot.gameObject.SetActive(true);
                FutureRecipe[i].transform.SetParent(Slot);
                FutureRecipe[i].transform.localScale = Constants.Recipe;
                FutureRecipe[i].GetComponent<Image>().color = new Color(0.39f, 0.39f, 0.39f, 1f);
                ContentScaler++;
                //print("--" + FutureRecipe[i]);
                if (ContentScaler > 4)
                {
                    Content.sizeDelta = Content.sizeDelta + new Vector2(0, 120);
                    ContentScaler = 0;
                }
            }
        }
    }
    Transform FreeSlot()  // ищем пустой слот в рецептах
    {
        for (int i = 0; i < RecipeSlots.Length; i++)
        {
            if (RecipeSlots[i].childCount == 0)
                return RecipeSlots[i];
        }
        return null;
    }

    #endregion
#region Окно инфо
    public void OpenResInfo(Sprite iconRes, string nameRes, string nameRestext, string descriptionRes)
    {
         WindowForCraft.SetActive(false);
         ResInfo.SetActive(true);
        ResImage.sprite = iconRes;
        ResImage.name = nameRes;
        ResNameT.text = nameRestext;
        if (nameRes == "Bucket")
            ResDescriptionT.text = Localization.instance.getTextByKey("#FromBucket");
        else
            ResDescriptionT.text = descriptionRes;
    }
    public void OpenBuildInfo(string ResName, Sprite BuildSprite, string loc)
    {
        WindowForCraft.SetActive(false);
        ResInfo.SetActive(true);
        ResNameT.text = ResName;
        ResDescriptionT.text = loc;
        ResImage.sprite = BuildSprite;
    }
    #endregion
#region Окно крафта
    public void SetWaterIngridient(Image I, Text T, Text Count, int resCount, int CraftCount)
    {
        I.sprite = GOD.InventoryContr.Icon.GetComponent<InfoScript>().Water[1];
        T.text = Localization.instance.getTextByKey("#Water");
        Count.text = "" + GOD.InventoryContr.InvnentoryGetCount("Bucket") + "/" + resCount * CraftCount;
    }

    public void ShowCraft(GameObject G, RecipeScript R, int CraftCount, float CraftLimit)
    {
        WindowForCraft.SetActive(true);
        ResInfo.SetActive(false);
        if (!IsBuildingWindow)
        {
            Plus.SetActive(true);
            Minus.SetActive(true);
        }

            theirParent.SetActive(true);
        CraftPanel.SetActive(true);
        TimeText.text = "" + CraftLimit * CraftCount + " " + Localization.instance.getTextByKey("#Second");
        CraftTime.fillAmount = 0;
         Plus.GetComponent<Selectable>().interactable = true;
         Minus.GetComponent<Selectable>().interactable = true;
       //NeedBuilding.gameObject.SetActive(false);
       IResult.sprite = G.GetComponent<Image>().sprite;
       NameCur1.text = "";
       NameCur2.text = "";
       NameCur3.text = "";
       NameRes.text = "";
       CraftRes1.Init();          //запоминаем составные ресурсы для их открывания
       CraftRes2.Init();
       CraftRes3.Init();
       CraftResult.Init();
       switch (R.NameOfIngridient.Length)    // в зависимости от количества ресурсов активируем иконки и показываем количество
       {
           case 1:
               Res1.gameObject.SetActive(false);
               Res2.gameObject.SetActive(true);
               Res3.gameObject.SetActive(false);
               Result.gameObject.SetActive(true);

               CraftRes2.ResName = R.NameOfIngridient[0];
               if (R.NameOfIngridient[0] == "Bucket")
               {
                   SetWaterIngridient(IRes2, NameCur2, TCur2, R.NumberOfIngridient[0], CraftCount);
                   CraftRes2.ResSprite = IRes2.sprite;
                   CraftRes2.ResNameText = NameCur2.text;
               }
               else
               {

                   IRes2.sprite = GOD.InventoryContr.FindIcon(R.NameOfIngridient[0]);
                   //подписываем
                   InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                   I.SetParameter(R.NameOfIngridient[0]);
                   NameCur2.text = I.thisObjName;
                   TCur2.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[0]) + "/" + R.NumberOfIngridient[0] * CraftCount;

                   if (!I.HaveRecipe)
                   {
                       CraftRes2.ResSprite = IRes2.sprite;
                       CraftRes2.ResNameText = NameCur2.text;
                       CraftRes2.ResDescription = I.whereDescription + "\n" + I.fullDescription;
                   }

               }
               TResult.text = "" + CraftCount;
               GOD.InventoryContr.Icon.GetComponent<InfoScript>().SetParameter(R.name);
               InfoScript Info = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
               NameRes.text = Info.thisObjName;

               CraftResult.ResName = Info.thisName;

               CraftResult.ResSprite = GOD.InventoryContr.FindIcon(R.name);
               CraftResult.ResNameText = Info.thisObjName;
               CraftResult.ResDescription = Info.fullDescription;
               break;
           case 2:
               Res1.gameObject.SetActive(true);
               Res2.gameObject.SetActive(false);
               Res3.gameObject.SetActive(true);
               Result.gameObject.SetActive(true);

               CraftRes1.ResName = R.NameOfIngridient[0];
               if (R.NameOfIngridient[0] == "Bucket")
               {
                   SetWaterIngridient(IRes1, NameCur1, TCur1, R.NumberOfIngridient[0], CraftCount);
                   CraftRes1.ResSprite = IRes1.sprite;
                   CraftRes1.ResNameText = NameCur1.text;
               }
               else
               {

                   IRes1.sprite = GOD.InventoryContr.FindIcon(R.NameOfIngridient[0]);
                   TCur1.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[0]) + "/" + R.NumberOfIngridient[0] * CraftCount;
                   //подписываем
                   InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                   I.SetParameter(R.NameOfIngridient[0]);
                   NameCur1.text = I.thisObjName;

                   if (!I.HaveRecipe)
                   {
                       CraftRes1.ResSprite = IRes1.sprite;
                       CraftRes1.ResNameText = NameCur1.text;
                       CraftRes1.ResDescription = I.whereDescription + "\n" + I.fullDescription;
                   }
               }

               CraftRes3.ResName = R.NameOfIngridient[1];
               if (R.NameOfIngridient[1] == "Bucket")
               {
                   SetWaterIngridient(IRes3, NameCur3, TCur3, R.NumberOfIngridient[1], CraftCount);
                   CraftRes3.ResSprite = IRes3.sprite;
                   CraftRes3.ResNameText = NameCur3.text;
               }
               else
               {
                   IRes3.sprite = GOD.InventoryContr.FindIcon(R.NameOfIngridient[1]);
                   //подписываем
                   InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                   I.SetParameter(R.NameOfIngridient[1]);
                   NameCur3.text = I.thisObjName;
                   TCur3.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[1]) + "/" + R.NumberOfIngridient[1] * CraftCount;

                   if (!I.HaveRecipe)
                   {
                       CraftRes3.ResSprite = IRes3.sprite;
                       CraftRes3.ResNameText = NameCur3.text;
                       CraftRes3.ResDescription = I.whereDescription + "\n" + I.fullDescription;
                   }
               }

               TResult.text = "" + CraftCount;
               GOD.InventoryContr.Icon.GetComponent<InfoScript>().SetParameter(R.name);
               Info = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
               NameRes.text = Info.thisObjName;

               CraftResult.ResName = Info.thisName;

               CraftResult.ResSprite = GOD.InventoryContr.FindIcon(R.name);
               CraftResult.ResNameText = Info.thisObjName;
               CraftResult.ResDescription = Info.fullDescription;
               break;
           case 3:
               Res1.gameObject.SetActive(true);
               Res2.gameObject.SetActive(true);
               Res3.gameObject.SetActive(true);
               Result.gameObject.SetActive(true);

               CraftRes1.ResName = R.NameOfIngridient[0];
               if (R.NameOfIngridient[0] == "Bucket")
               {
                   SetWaterIngridient(IRes1, NameCur1, TCur1, R.NumberOfIngridient[0],CraftCount);
                   CraftRes1.ResSprite = IRes1.sprite;
                   CraftRes1.ResNameText = NameCur1.text;
               }
               else
               {
                   IRes1.sprite = GOD.InventoryContr.FindIcon(R.NameOfIngridient[0]);
                   //подписываем
                   InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                   I.SetParameter(R.NameOfIngridient[0]);
                   NameCur1.text = I.thisObjName;
                   TCur1.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[0]) + "/" + R.NumberOfIngridient[0] * CraftCount;

                   if (!I.HaveRecipe)
                   {
                       CraftRes1.ResSprite = IRes1.sprite;
                       CraftRes1.ResNameText = NameCur1.text;
                       CraftRes1.ResDescription = I.whereDescription + "\n" + I.fullDescription;
                   }
               }

               CraftRes2.ResName = R.NameOfIngridient[1];
               if (R.NameOfIngridient[1] == "Bucket")
               {
                   SetWaterIngridient(IRes2, NameCur2, TCur2, R.NumberOfIngridient[1],CraftCount);
                   CraftRes2.ResSprite = IRes2.sprite;
                   CraftRes2.ResNameText = NameCur2.text;
               }
               else
               {
                   IRes2.sprite = GOD.InventoryContr.FindIcon(R.NameOfIngridient[1]);
                   //подписываем
                   InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                   I.SetParameter(R.NameOfIngridient[1]);
                   NameCur2.text = I.thisObjName;
                   TCur2.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[1]) + "/" + R.NumberOfIngridient[1] * CraftCount;

                   if (!I.HaveRecipe)
                   {
                       CraftRes2.ResSprite = IRes2.sprite;
                       CraftRes2.ResNameText = NameCur2.text;
                       CraftRes2.ResDescription = I.whereDescription + "\n" + I.fullDescription;
                   }
               }

               CraftRes3.ResName = R.NameOfIngridient[2];
               if (R.NameOfIngridient[2] == "Bucket")
               {
                   SetWaterIngridient(IRes3, NameCur3, TCur3, R.NumberOfIngridient[2],CraftCount);
                   CraftRes3.ResSprite = IRes3.sprite;
                   CraftRes3.ResNameText = NameCur3.text;
               }
               else
               {
                   IRes3.sprite = GOD.InventoryContr.FindIcon(R.NameOfIngridient[2]);
                   //подписываем
                   InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                   I.SetParameter(R.NameOfIngridient[2]);
                   NameCur3.text = I.thisObjName;
                   TCur3.text = "" + GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[2]) + "/" + R.NumberOfIngridient[2] * CraftCount;

                   if (!GOD.InventoryContr.Icon.GetComponent<InfoScript>().HaveRecipe)
                   {
                       CraftRes3.ResSprite = IRes3.sprite;
                       CraftRes3.ResNameText = NameCur3.text;
                       CraftRes3.ResDescription = I.whereDescription + "\n" + I.fullDescription;
                   }
               }

               TResult.text = "" + CraftCount;
               GOD.InventoryContr.Icon.GetComponent<InfoScript>().SetParameter(R.name);
               Info = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
               NameRes.text = Info.thisObjName;

               CraftResult.ResName = Info.thisName;
               CraftResult.ResSprite = GOD.InventoryContr.FindIcon(R.name);
               CraftResult.ResNameText = Info.thisObjName;
               CraftResult.ResDescription = Info.fullDescription;
               break;
       }
   }
#endregion

}
