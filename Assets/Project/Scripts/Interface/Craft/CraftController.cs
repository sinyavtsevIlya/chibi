﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CraftController : MonoBehaviour {

    public GameManager GOD;
    public CraftWindow UsialCraft; //параметры окна обычного крафта
    public CraftWindow BuildingCraft;//параметры окна крафта со зданиями
    public CraftWindow CurrentWindow = null;

    public List<CraftBuilding> CraftQuenue = new List<CraftBuilding>(); //очередь на крафт
    public CraftBuilding Usial;
    CraftBuilding Build;

    CraftBuilding CurrentCraftPlace; //что сейчас открыто
    GameObject CurrentCraft;          // текущий крафт

    public string CurrentBuilding = ""; // используемый домик
    public GameObject CurrentBuild = null;
    public Sprite SmithySprite, MannequinSprite, StoveSprite, WindmillSprite;
    public RecipeScript[] Recipe;    // массив рецептов
    public GameObject RecipeParent;  // объект с рецептами

    [Header("Количество крафта:")]
    public float CraftLimit;
    public float CraftLimitUsialConst = 5, CraftLimitBuildConst = 60;
    public float CraftLimitUsial, CraftLimitBuild;


    public bool StartCraft = false;

    [Header("Сортировка:")]
    List<RecipeScript> AviableRecipe = new List<RecipeScript>();    // список доступных рецептов прямо сейчас
    List<RecipeScript> FutureRecipe = new List<RecipeScript>();    // список доступных рецептов, но не хватает ингридиентов                                                  // List<string> CurrentTypes = new List<string>();                              
    string CurrentType = "";  // типы рецептов для сортировки
    Image OldTypeBtn; //предыдущая активная кнопка раздела
    public GameObject AllBtn; //кнопка все разделы по умолчанию нажата
    [Header("Показать все:")]
    bool IsShowAll = false; // показывать ли все
    public Text TextShowAll;
    public Transform OneRecipe;

    int CraftCount = 1;      // количество крафта
    float CraftSecond, CraftMicroSecond;
    public float CraftTimer = 0;

    public void Init()
    {
        CraftLimitUsial = CraftLimitUsialConst;
        CraftLimitBuild = CraftLimitBuildConst;
        foreach (var key in GOD.InventoryContr.Icons.Keys)  // инициализируем рецепты
        {
            GOD.InventoryContr.Icon.GetComponent<InfoScript>().SetParameter(key);
            if (GOD.InventoryContr.Icon.GetComponent<InfoScript>().HaveRecipe)
            {
                Transform g = Instantiate(OneRecipe);
                string newname = "";
                for (int x = 5; x < key.Length; x++)
                    newname += key[x];
                g.name = newname;
                g.GetComponent<Image>().sprite = GOD.InventoryContr.Icons[key];
                g.SetParent(RecipeParent.transform);
                g.GetComponent<RecipeScript>().Initialize();
                g.gameObject.SetActive(true);
            }
        }
        Recipe = new RecipeScript[RecipeParent.transform.childCount];          
        for (int i = 0; i < Recipe.Length; i++)
        {
            Recipe[i] = RecipeParent.transform.GetChild(i).gameObject.GetComponent<RecipeScript>();
        }

        UsialCraft.Init();
        BuildingCraft.Init();
  
        /*types[0] = "resource";
        types[1] = "food";
        types[2] = "instrument";
        types[3] = "weapon";
        types[4] = "clothe";
        types[5] = "building";

        for (int i = 0; i < types.Length; i++)
            CurrentTypes.Add(types[i]);*/
        InitLanguage();
    }

    public void InitLanguage()
    {
        UsialCraft.CraftButtonText.text = Localization.instance.getTextByKey("#Craft");
        BuildingCraft.CraftButtonText.text = Localization.instance.getTextByKey("#Craft");
        TextShowAll.text = Localization.instance.getTextByKey("#ShowAll");
    }
    float ItemfillAmount = 0;
    void Update()
    {
        if (CraftQuenue.Count>0) //если что стоит в очереди на крафт
        {
            CraftMicroSecond += Time.deltaTime;
            if (CraftMicroSecond >= 0.1f)
            {
                for (int i = 0; i < CraftQuenue.Count; i++)
                {
                    CraftQuenue[i].thisCraftItem.SetItemfillAmount();
                }
                if (CurrentCraftPlace && CurrentCraftPlace.thisCraftItem.IsExist)
                {
                    CurrentWindow.CraftTime.fillAmount = CurrentCraftPlace.thisCraftItem.ItemfillAmount;
                }
                CraftMicroSecond = 0;
                CraftSecond += 0.1f;
            }
            if (CraftSecond >= 1)
            {
                CraftSecond = 0;
                CraftTimer++;
                if (CurrentCraftPlace && CurrentCraftPlace.thisCraftItem.IsExist)
                {
                    CraftItem c = CurrentCraftPlace.thisCraftItem;
                    CurrentWindow.TimeText.text = "" + (c.ItemTimeStop - CraftTimer) + " " + Localization.instance.getTextByKey("#Second");
                }
                for (int i=0; i<CraftQuenue.Count; i++)
                {
                    if (CraftQuenue[i].thisCraftItem.ItemTimeStop <= CraftTimer)
                    {
                        CompleteCraft(CraftQuenue[i]);
                        CraftQuenue.Remove(CraftQuenue[i]);
                        break;
                    }
                }
            }
        }
        /*if(CurrentWindow!=null && StartCraft)
        {
            CraftSecond += Time.deltaTime;
            CraftMicroSecond += Time.deltaTime;
            if(CraftMicroSecond>=0.1f)
            {
                CurrentWindow.CraftTime.fillAmount += ((100f / CraftLimit) / 1000) * (1f / CraftCount);
                CraftMicroSecond = 0;
            }
            if(CraftSecond>=1)
            {
                CraftSecond = 0;
                CraftTimer++;
                CurrentWindow.TimeText.text = "" + (CraftLimit*CraftCount-CraftTimer) + " " + Localization.instance.getTextByKey("#Second");
            }
            if ((CraftLimit * CraftCount - CraftTimer)<=0)
            {
                CompleteCraft(CurrentUsialCraft);
               /* StartCraft = false;
                CurrentWindow.CraftTime.fillAmount = 0;
                CraftTimer = 0;
                GOD.InventoryContr.AddToInventory(InCraftObject.name, CraftCount);
                GOD.PlayerInter.ShowNewItem(InCraftObject.name, CraftCount);
                GOD.Progress.SaveMaxCraft(CraftCount);
                if (GOD.IsTutorial && GOD.Tutor.TutorialStep==9 && InCraftObject.name == "WoodPick")
                    GOD.Tutor.NextStep = true;
                CraftCount = 1;
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.AfterCraft);
                InCraftObject = null;
                TimeText.text = "";
                if (CurrentWindow.CraftWindoww.activeSelf)
                {
                    CurrentWindow.Close();
                    if (CurrentBuilding == "")
                        OpenCraft();
                    else
                        OpenCraftInBuilding(CurrentBuild);
                }
                else if (GOD.PlayerInter.Inventory.activeSelf)
                {
                    GOD.PlayerInter.OpenInventory();
                }

                if (GOD.IsTutorial)
                    CurrentWindow.Close();
            }
        }*/
    }


    public void CompleteCraft(CraftBuilding CB)//завершаем крафт
    {
        CraftItem CI = CB.thisCraftItem;
        GOD.InventoryContr.AddToInventory(CI.ItemName, CI.ItemCount);
        GOD.PlayerInter.ShowNewItem(CI.ItemName, CI.ItemCount);
        GOD.Progress.SaveMaxCraft(CI.ItemCount);
        if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 9 && CI.ItemName == "WoodPick")
            GOD.Tutor.NextStep = true;
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.AfterCraft);
        CB.thisCraftItem.IsExist = false;
        CB.StopCraftAnimation();
        StartCraft = false;
        CraftCount = 1;
        if (GOD.IsTutorial && CurrentWindow)
            CurrentWindow.CloseCraft();
        else
        {
            if (CurrentCraftPlace && CurrentCraftPlace==CB)
            {
                UpdateCraft();
                CurrentWindow.WindowForCraft.SetActive(false);
               // CurrentWindow.CloseImmediatly();
               // OpenCraft();
            }
        }
    }
    
    public void SetShowAll()
    {
        IsShowAll = !IsShowAll;
        UpdateCraft();
       // CurrentWindow.CloseImmediatly();
        //OpenCraft();
    }
    public void OpenCraft(GameObject building = null)                     // открытие крафта
    {
        CraftWindow W;
        if (building == null)
        {
            W = UsialCraft;
            CurrentBuilding = "";
        }
        else
        {
            W = BuildingCraft;
            CurrentBuilding = building.name;
            CurrentBuild = building;
        }
        CurrentWindow = W;
        W.OpenCraft(CurrentBuilding);
        CurrentWindow = W;
        if (CurrentType == "")
                SortRecipe(AllBtn);
            CreateList();
        CurrentWindow.RecipePlace(AviableRecipe, FutureRecipe, CurrentType);
        if (CurrentBuilding == "") //открываем текущий крафт
        {
            CurrentCraftPlace = Usial;
        }
        else 
        {
            Build= CurrentBuild.GetComponent<CraftBuilding>();
            CurrentCraftPlace = Build;
        }
        if (CurrentCraftPlace && CurrentCraftPlace.thisCraftItem.IsExist)
        {
            CurrentCraftPlace.thisCraftItem.IsNowShow = false;
            OpenResByName(CurrentCraftPlace.thisCraftItem.ItemName);
            CurrentWindow.TimeText.text = "" + (CurrentCraftPlace.thisCraftItem.ItemTimeStop - CraftTimer) + " " + Localization.instance.getTextByKey("#Second");
            CurrentWindow.CraftTime.fillAmount = CurrentCraftPlace.thisCraftItem.ItemfillAmount;
        }

    }
    public void CloseCraft()                           // закрываем крафт
    {
         for (int i = 0; i < CurrentWindow.RecipeSlots.Length; i++)
         {
            CurrentWindow.RecipeSlots[i].gameObject.SetActive(false);
             if (CurrentWindow.RecipeSlots[i].childCount > 0)
                CurrentWindow.RecipeSlots[i].GetChild(0).SetParent(RecipeParent.transform);
         }
         AviableRecipe.Clear();
         FutureRecipe.Clear();
         if(GOD.IsTutorial && (GOD.Tutor.TutorialStep==8))
         {
            GOD.Tutor.DoNotSendMentrica = true;
            GOD.Tutor.TutorialStep = 7;
             GOD.Tutor.ShowTutorialStepTurn();
         }
        CurrentWindow = null;
        CurrentCraftPlace = null;
    }

    public void UpdateCraft()
    {
        for (int i = 0; i < CurrentWindow.RecipeSlots.Length; i++)
        {
            CurrentWindow.RecipeSlots[i].gameObject.SetActive(false);
            if (CurrentWindow.RecipeSlots[i].childCount > 0)
                CurrentWindow.RecipeSlots[i].GetChild(0).SetParent(RecipeParent.transform);
        }
        AviableRecipe.Clear();
        FutureRecipe.Clear();

        CreateList();
        CurrentWindow.RecipePlace(AviableRecipe, FutureRecipe, CurrentType);
    }

    void CreateList()
    {
        if (GOD.IsTutorial)
        {
            for (int i = 0; i < Recipe.Length; i++)
            {
                if (Recipe[i].name == "WoodPick")
                {
                    AviableRecipe.Add(Recipe[i]);
                    break;
                }
            }
        }
        else
        {
            if (CurrentBuilding == "")
            {
                for (int i = 0; i < Recipe.Length; i++)
                {
                    for (int x = 0; x < Recipe[i].NameOfIngridient.Length; x++)
                    {
                        if (IsCraftAviable(Recipe[i]))
                        {
                            if (!AviableRecipe.Contains(Recipe[i]))
                            {
                                AviableRecipe.Add(Recipe[i]);
                                break;
                            }
                        }
                        else
                        {
                            if (IsShowAll)
                            {
                                FutureRecipe.Add(Recipe[i]);
                                break;
                            }
                            else
                            {
                                if (IsCraftShow(Recipe[i]))
                                {
                                    FutureRecipe.Add(Recipe[i]);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < Recipe.Length; i++)
                {
                    if (Recipe[i].building == CurrentBuilding)
                    {
                        for (int x = 0; x < Recipe[i].NameOfIngridient.Length; x++)
                        {
                            if (IsCraftAviable(Recipe[i]))
                            {
                                if (!AviableRecipe.Contains(Recipe[i]))
                                {
                                    AviableRecipe.Add(Recipe[i]);
                                    break;
                                }
                            }
                            else
                            {
                                FutureRecipe.Add(Recipe[i]);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    bool IsCraftAviable(RecipeScript R)      // возможен ли крафт
    {
          bool[] HelpBool = new bool[R.NameOfIngridient.Length];
          for (int i = 0; i < R.NameOfIngridient.Length; i++)
          {
              int resCount = 0;
              for(int x=0; x<GOD.InventoryContr.AllIcons.Count; x++)
              {
                  if (GOD.InventoryContr.AllIcons[x].name == "Icon_" + R.NameOfIngridient[i])            // есть ли нужные элементы в  инвентаре
                  {
                      resCount = GOD.InventoryContr.InvnentoryGetCount(R.NameOfIngridient[i]);
                    if (resCount >= R.NumberOfIngridient[i]*CraftCount) // достаточно ли их
                      {
                          HelpBool[i] = true;
                         // break;
                      }

                  }
              }
          }

        bool NeedBuilding = false;
        if (R.building == CurrentBuilding)
            NeedBuilding = true;
        bool Answer = true;
        for (int i = 0; i < HelpBool.Length; i++)
        {
            Answer = Answer && HelpBool[i] && NeedBuilding;
        }
        return Answer;
    }
    bool IsCraftShow(RecipeScript R)      //есть ли хоть один элемент
    {
        for (int i = 0; i < R.NameOfIngridient.Length; i++)
        {
            for (int x = 0; x < GOD.InventoryContr.AllIcons.Count; x++)
            {
                if (GOD.InventoryContr.AllIcons[x].name == "Icon_" + R.NameOfIngridient[i])        
                {
                    return true;             // есть ли нужные элементы в  инвентаре
                } 
            }
        }
        return false;
    }
    public void ShowCraft(GameObject G)   // показываем состав крафта
    {
        if (!CurrentCraftPlace.thisCraftItem.IsExist || (CurrentCraftPlace.thisCraftItem.IsExist && !CurrentCraftPlace.thisCraftItem.IsNowShow))
        {
            if (CurrentCraftPlace.thisCraftItem.IsExist)
                CurrentCraftPlace.thisCraftItem.IsNowShow = true;
            if (CurrentCraft != G)
                CraftCount = 1;
            CurrentCraft = G;
            RecipeScript R = G.GetComponent<RecipeScript>();

            if ((CurrentBuilding == "") && (R.building != ""))   // показываем сообщение о необходимости здания
            {
                InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
                I.SetParameter(R.name);
                OpenBuild(I.thisObjName, R.building);
            }
            else if ((CurrentBuilding == "") || (CurrentBuilding == R.building))
            {
                if (CurrentCraftPlace && !CurrentCraftPlace.thisCraftItem.IsExist)
                {
                    if (IsCraftAviable(R))
                        CurrentWindow.CraftButtonSelectable.interactable = true;
                    else
                        CurrentWindow.CraftButtonSelectable.interactable = false;
                    if (CurrentBuilding == "")
                        CraftLimit = CraftLimitUsial;
                    else
                        CraftLimit = CraftLimitBuild;
                }
                else
                    CurrentWindow.CraftButtonSelectable.interactable = false;
                CurrentWindow.ShowCraft(G, R, CraftCount, CraftLimit);

            }
            else 
            {
                // CurrentWindow.CloseCraft();
                //  OpenCraft();
                UpdateCraft();
                OpenResByName(G.name);
            }

            if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 8 && G.name == "WoodPick")
                GOD.Tutor.NextStep = true;
        }
    }
    public void PlusMinusCraft(bool IsPlus)                // кнопка плюс минус крафт
    {
        if (IsPlus)
        {
            CraftCount++;
            ShowCraft(CurrentCraft);
        }
        else
        {
            if (CraftCount > 1)
            {
                CraftCount--;
                ShowCraft(CurrentCraft);
            }
        }
        if (IsCraftAviable(CurrentCraft.GetComponent<RecipeScript>()))
            CurrentWindow.CraftButtonSelectable.interactable = true;
        else
            CurrentWindow.CraftButtonSelectable.interactable = false;
    }  
    public void StartTimeCraft()                     // кнопка запуска крафта
    {
        if(GOD.IsTutorial)
        {
            UsialCraft.CraftButton.gameObject.GetComponent<Animator>().enabled=false;
            UsialCraft.CraftButton.transform.localScale = Constants.VectorOne;
        }
        if (GOD.InventoryContr.CanAdd(CurrentCraft.name, CraftCount))
        {
            RecipeScript R = CurrentCraft.GetComponent<RecipeScript>();
            for (int i = 0; i < R.NameOfIngridient.Length; i++)
            {
                GOD.InventoryContr.RemoveFromInventoryByName(R.NameOfIngridient[i], R.NumberOfIngridient[i] * CraftCount);
            }
            CurrentWindow.StartCraft(R, CraftCount);
            CraftItem CI = new CraftItem();
            CI.SetCraftItem(CurrentCraft.name, CraftCount, null,CraftLimit * CraftCount, CraftTimer + CraftLimit * CraftCount);
            CurrentCraftPlace.thisCraftItem = CI;
            CurrentCraftPlace.thisCraftItem.IsExist = true;
            CurrentCraftPlace.StartCraftAnimation();
            CraftQuenue.Add(CurrentCraftPlace);
            if (CraftQuenue.Count == 1)
                StartCheckDistance();
            StartCraft = true;
            UpdateCraft();
            CurrentCraftPlace.thisCraftItem.IsNowShow = true;
            //  CurrentWindow.CloseImmediatly();
            //  OpenCraft(CurrentBuild);
        }
        else
            GOD.PlayerInter.OpenCantOpen();
    }


    public void SortRecipe(GameObject G) // сортировка рецептов
    {
        if(OldTypeBtn)
            OldTypeBtn.color = new Color(1f, 1f, 1f, 1f);
        Image I = G.GetComponent<Image>();
        if (I.color == new Color(1, 1, 1, 1))
        {
            CurrentWindow.Content.anchoredPosition = new Vector2(0, 0);
            G.GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1f);
            CurrentType = G.name;
            OldTypeBtn = I;
        }
        for (int i = 0; i < CurrentWindow.RecipeSlots.Length; i++)
        {
            CurrentWindow.RecipeSlots[i].gameObject.SetActive(false);
            if (CurrentWindow.RecipeSlots[i].childCount > 0)
                CurrentWindow.RecipeSlots[i].GetChild(0).SetParent(RecipeParent.transform);
        }
        CurrentWindow.RecipePlace(AviableRecipe, FutureRecipe,CurrentType);
    }

    public void OpenResInfo(CraftResource c)  //для открытия из обычного крафта
    {
        if (c.ResSprite)
        {
            CurrentWindow.OpenResInfo(c.ResSprite, c.ResName, c.ResNameText, c.ResDescription);
        }
        else
        {
            for (int i = 0; i < Recipe.Length; i++)
            {
                if (Recipe[i].name == c.ResName)
                {
                    ShowCraft(Recipe[i].gameObject);
                    break;
                }
            }
        }
    }

    public void OpenResInfoBuild(CraftResource c)  //для открытия из  крафта зданий
    {
        if (c.ResSprite)
        {
            CurrentWindow.OpenResInfo(c.ResSprite, c.ResName, c.ResNameText, c.ResDescription);
        }
        else
        {
            for (int i = 0; i < Recipe.Length; i++)
            {
                if (Recipe[i].name == c.ResName)
                {
                    if (Recipe[i].building == CurrentBuilding)
                    {
                        ShowCraft(Recipe[i].gameObject);
                        return;
                    }
                    else
                    {
                        CurrentWindow.CloseCraft();
                        OpenCraft();
                        ShowCraft(Recipe[i].gameObject);
                        return;
                    }
                }
            }
        }
    }

    public void OpenResFromInfo(GameObject g)
    {
        for (int i = 0; i < Recipe.Length; i++)
        {
            if (Recipe[i].name == g.name)
            {
                ShowCraft(Recipe[i].gameObject);
                break;
            }
        }
    }
    public void OpenBuild(string ResName, string Building)
    {
        Sprite s;
        string st;
        switch (Building)
        {
            case "Stove":
                st = Localization.instance.getTextByKey("#NeedStove");
                s = StoveSprite;
                break;
            case "Mannequie":
                st = Localization.instance.getTextByKey("#NeedMannequi");
                s = MannequinSprite;
                break;
            case "Windmill":
                st = Localization.instance.getTextByKey("#NeedWindmeel");
                s = WindmillSprite;
                break;
            default:
               st = Localization.instance.getTextByKey("#NeedSmithy");
                s = SmithySprite;
                break;
        }
        CurrentWindow.OpenBuildInfo(ResName, s, st);
    }

    /*public void OpenBuildRes() //открыть крафт здания
    {
        if (ResImage.sprite != null)
        {
            for (int i = 0; i < Recipe.Length; i++)
            {
                if (Recipe[i].name == ResImage.sprite.name)
                {
                    ShowCraft(Recipe[i].gameObject);
                    break;
                }
            }
        }
    }*/

    public void OpenResFromInterface(GameObject g)
    {
        if (GetRecipeByName(g.name) != null)
        {
            if (GOD.QuestContr.QuestWindow.QuestWindow.activeSelf)
                GOD.QuestContr.QuestWindow.CloseQuestWindowImmidiatly();
            OpenResByName(g.name);
        }
    }

    public void OpenResByName(string n)//открыть крафт по имени
    {
      //  GOD.PlayerInter.CloseAll(); //закрываем все окна
      if(CurrentWindow==null)
            GOD.PlayerInter.OpenCraft();
        CurrentWindow.WindowForCraft.SetActive(true);
        InfoScript I = GOD.InventoryContr.Icon.GetComponent<InfoScript>();
        I.SetParameter(n);
        if(I.HaveRecipe)//если есть рецепт - открываем крафт
        {
            for (int i = 0; i < Recipe.Length; i++)
            {
                if (Recipe[i].name == n)
                {
                    ShowCraft(Recipe[i].gameObject);
                    break;
                }
            }
        }
        else //открываем описание
        {
            string s = I.whereDescription + "\n" + I.fullDescription;
            CurrentWindow.OpenResInfo(GOD.InventoryContr.FindIcon(n), I.name, I.thisObjName, s);
               /* ResImage.sprite = GOD.InventoryContr.FindIcon(n);
            ResNameT.text = I.thisObjName;  //подписываем
            ResDescriptionT.text = I.whereDescription + "\n" + I.fullDescription;
            CraftRes2.ResSprite = IRes2.sprite;
            CraftRes2.ResNameText = NameCur2.text;
            CraftRes2.ResDescription = I.whereDescription;*/
        }
    }

    public RecipeScript GetRecipeByName(string craftName) //получаем рецепт по имени
    {
        for(int i=0; i<Recipe.Length; i++)
        {
            if (Recipe[i].name == craftName)
                return Recipe[i];
        }
        return null;
    }

    public void StartCheckDistance()
    {
        StartCoroutine(CheckDistance());
    }
    IEnumerator CheckDistance() //включение отключение анимаций
    {
        while (CraftQuenue.Count > 0)
        {
            for(int i=0; i< CraftQuenue.Count;i++)
            {
                if (Vector3.SqrMagnitude(CraftQuenue[i].transform.position-GOD.Player.transform.position)>500f)
                    CraftQuenue[i].Pause(false);
                else
                    CraftQuenue[i].Pause(true);
            }
            yield return new WaitForSeconds(2f);
        }
    }

    public void InitCheckDistance()
    {
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < CraftQuenue.Count; i++)
            CraftQuenue[i].StartCraftAnimation();
        StartCoroutine(CheckDistance());
    }
}
