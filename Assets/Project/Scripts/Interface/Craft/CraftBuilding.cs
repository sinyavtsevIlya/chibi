﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftBuilding : MonoBehaviour {

    public CraftItem thisCraftItem;
    string thisName;
    public bool NowCraft = false;
    bool NowPlay = false;
    Animator thisAnimator;
    Transform thisParticle;

    private void Start()
    {
        thisName = name;
        switch(thisName)
        {
            case "Windmill":
            case "Mannequie":
                thisAnimator = transform.GetComponent<Animator>();
                break;
            case "Stove":
            case "Smithy":
                thisParticle = transform.Find("Particle");
                break;
        }
    }


    public void StartCraftAnimation()
    {
        NowCraft = true;
        StartAnimation();
    }
    void StartAnimation()
    {
        NowPlay = true;
        switch (thisName)
        {
            case "Windmill":
            case "Mannequie":
                thisAnimator.enabled = true;
                break;
            case "Stove":
            case "Smithy":
                thisParticle.gameObject.SetActive(true);
                break;
        }
    }
    public void StopCraftAnimation()
    {
        NowCraft = false;
        StopAnimation();
    }

    void StopAnimation()
    {
        NowPlay = false;
        switch (thisName)
        {
            case "Windmill":
            case "Mannequie":
                thisAnimator.enabled = false;
                break;
            case "Stove":
            case "Smithy":
                thisParticle.gameObject.SetActive(false);
                break;
        }
    }

    public void Pause(bool pause)
    {
        if (NowCraft)
        {
            if (pause)
            {
                if(!NowPlay)
                StartAnimation();
            }
            else if(NowPlay)
                StopAnimation();
        }
    }
}
