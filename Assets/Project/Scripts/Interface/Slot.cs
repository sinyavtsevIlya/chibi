﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IDropHandler {

    public GameManager GOD;
    InventoryController InventoryContr;
    PlayerController PlayerContr;
    public Image thisImage;
    string SlotName = "";
    InfoScript.ItemType CurrentType;
    public bool SlotsOnPlayer = false;
    bool Delete = false;
    public bool InChest = false;
    public bool InChestInventory = false;
    bool InGame = false;
    public bool IsPlayer = false;
    public bool IsInAfterDead = false;
   public  GameObject InGameSlotChild;     // ребенок слота в игре
    IconInGame ChildScript;

    void Start()
    {
       // Init();
    }
    public void Init()
    {
        GOD = GameObject.Find("GameManager").GetComponent<GameManager>();
        thisImage = GetComponent<Image>();
        SlotName = name;
        InventoryContr = GameObject.Find("GameManager").GetComponent<InventoryController>();
        PlayerContr = GOD.Player.GetComponent<PlayerController>();
        if ((name == "Head") || (name == "Body") || (name == "Legs") || (name == "Feet") || (name == "LeftArm") || (name == "RightArm")|| (name == "Belt")|| name == "Mantle1"|| name == "Mantle2"|| name == "Mantle3")
            SlotsOnPlayer = true;
        if (name == "Delete")
            Delete = true;
        if (name == "Player")
            IsPlayer = true;
        if (name == "AfterDeadSlot")
            IsInAfterDead = true;
        if (name == "SlotInGame")
        {
            InGame = true;
            InGameSlotChild = transform.GetChild(0).gameObject;
            ChildScript = InGameSlotChild.GetComponent<IconInGame>();
            ChildScript.Init();
        }
        if (name == "ChestSlot")
            InChest = true;
        if (name == "ChestInventory")
            InChestInventory = true;
        if (name.Contains("Mantle"))
            SlotName = "Mantle";


    }
    public GameObject item
    {
        get
        {
            if(transform.childCount>0)
            {
                CurrentType = transform.GetChild(0).GetComponent<InfoScript>().type;
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (DragHandler.itemBeingDragged != null)
        {
            InfoScript IconS = DragHandler.itemBeingDragged.GetComponent<InfoScript>();
            if (Delete)                                                                      // если перетягиваем на удаление
            {
                //print("DELETE");
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Delete);
                Slot S = IconS.SlotInGame;
                if (S != null)
                    S.InGameSlotChild.SetActive(false);
                InventoryContr.DeleteIcon(DragHandler.itemBeingDragged);

            }
            else if(InGame)                                                                        // если перетягиваем на панель быстрого доступа
            { 
                if (IconS.type != InfoScript.ItemType.resources && IconS.type != InfoScript.ItemType.clothes&&IconS.type != InfoScript.ItemType.chest)
                {
                    if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 11)
                        GOD.Tutor.NextStep = true;

                    InGameSlotChild.SetActive(true);
                    ChildScript.SetIcon(DragHandler.itemBeingDragged, this);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                }
            }
            else if (InChest)            // если перетягиваем в судук                                                        
            {
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                IconS.ParentSlot = null;
                if (item)
                {
                    Transform T = transform.GetChild(0);
                    InfoScript I = T.GetComponent<InfoScript>();
                    if ((IconS.type == CurrentType) && (IconS.thisName == I.thisName)) //если в слотах одинаковые иконки
                        CombineIcon(I, IconS);
                    else
                    {
                        if (DragHandler.startParent.name != "ChestSlot") //достаем из сундука
                        {
                            int n = I.ChestPlace;
                            OutChest(I, DragHandler.startParent);
                            AddChest(IconS, transform);
                            IconS.ChestPlace = n;
                        }
                        else //просто меняем местами
                        {
                            T.SetParent(DragHandler.startParent);
                            int n = I.ChestPlace;
                            I.ChestPlace = IconS.ChestPlace;
                            IconS.ChestPlace = n;
                            DragHandler.itemBeingDragged.transform.SetParent(transform);
                        }
                    }
                }
                else
                {
                    ChestScript curChest = GOD.InventoryContr.CurrentChest.GetComponent<ChestScript>();
                    if (!curChest.MyIcons.Contains(IconS))
                    {
                        AddChest(IconS, transform);
                    }
                    else
                    {
                        DragHandler.itemBeingDragged.transform.SetParent(transform);
                    }
                    for (int i = 0; i < transform.parent.childCount; i++)                  //запоминаем слот в сундуке
                    {
                        if (transform.parent.GetChild(i) == transform)
                        {
                            IconS.ChestPlace = i;
                            break;
                        }
                    }
                }
            }
            else if (InChestInventory)                        // если перетягиваем в инвентарь сундука
            {
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                if (item)
                {
                    Transform T = transform.GetChild(0);
                    InfoScript I = T.GetComponent<InfoScript>();
                      if ((IconS.type == CurrentType) && (IconS.thisName == I.thisName)) //если в слотах одинаковые иконки
                        CombineIcon(I, IconS);
                    else
                    {
                        if (DragHandler.startParent.name == "ChestSlot") //достаем из сундука
                        {
                            int n = IconS.ChestPlace;
                            OutChest(IconS, transform);
                            AddChest(I, DragHandler.startParent);
                            I.ChestPlace = n;
                        }
                        else //просто меняем местами
                        {
                            T.SetParent(DragHandler.startParent);
                            DragHandler.itemBeingDragged.transform.SetParent(transform);
                        }
                    }
                }
                else
                {
                    ChestScript curChest = GOD.InventoryContr.CurrentChest.GetComponent<ChestScript>();
                    if (curChest.MyIcons.Contains(IconS))             //убираем из сундука
                        OutChest(IconS, transform);
                    else
                        DragHandler.itemBeingDragged.transform.SetParent(transform); //просто перемещаем внутриинвенатря
                }
 
            }
            else if(IsPlayer)      //если отпустили на персонаже
            {
                if (DragHandler.itemBeingDragged != null)
                {
                    ClothesScript c = DragHandler.itemBeingDragged.GetComponent<ClothesScript>();
                    if (c)
                        c.BtnDown();
                }
            }
            else if (IsInAfterDead)      //если это в инвентаре после смерти
            {
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                if (DragHandler.startParent!= transform && DragHandler.itemBeingDragged != null && transform.childCount==0)
                {
                    InfoScript I = DragHandler.itemBeingDragged.GetComponent<InfoScript>();
                    if (I.ParentSlot != transform)
                    {
                        Destroy(I.ParentSlot.gameObject);

                        GOD.InventoryContr.AddIcon(I);

                        //InventoryContr.AddToInventoryWithParametre(I.thisName, I.ResourceCount, I.currentDurability, I.currentValidity, I.WaterCount);
                        GOD.PlayerInter.ShowNewItem(I.thisName, I.ResourceCount);
                        InventoryContr.CurrentBag.GetComponent<AfterDeadBag>().MyIcons.Remove(I);
                        // Destroy(I.gameObject);
                        DragHandler.itemBeingDragged.transform.SetParent(transform);

                        InventoryContr.RemoveFromAfterDead();
                        if (InventoryContr.CurrentBag)
                            InventoryContr.ShowIconsInAfterDead(InventoryContr.CurrentBag);
                    }
                }
            }
            else
            {
                if (!item)                                                                  // если нет объектов в слоте
                {
                    if (DragHandler.itemBeingDragged != null)
                    {
                        if (SlotsOnPlayer)                                                      // если слот на игроку
                        {
                           // if (!GOD.IsTutorial)
                            {
                                if (IconS.ParentSlot != this.transform)   // проверка на повторое перетягивание сюда предмета
                                {
                                    if (IconS.OnPlayerSlot == SlotName)
                                    {
                                        DragHandler.itemBeingDragged.transform.SetParent(transform);
                                        IconS.ParentSlot = transform;
                                        PlayerContr.SetPlayerParametre(DragHandler.itemBeingDragged, true);                         // пересчитываем параметры игрока
                                    }
                                  //  InventoryContr.RemoveFromShopSlots();
                                }
                            }
                        }
                        else                                                                                         // если слот в инвентаре
                        {
                            if (IconS.ParentSlot.GetComponent<Slot>().SlotsOnPlayer)                // пересчитывае параметры игрока
                                PlayerContr.RemovePlayerParametre(DragHandler.itemBeingDragged, true);
                            else
                                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                            DragHandler.itemBeingDragged.transform.SetParent(transform);
                            IconS.ParentSlot = transform;
                        }
                    }
                }
                else   // если в слоте есть иконка
                {
                    InfoScript I = item.GetComponent<InfoScript>();
                    MoveIcons(I, IconS);
                }
            }
        }
    }


    public void OutChest(InfoScript IconS, Transform newParent)
    {
        InventoryContr.AddIcon(IconS);
        //InventoryContr.AddToInventoryWithParametre(IconS.thisName, IconS.ResourceCount, IconS.currentDurability, IconS.currentValidity);
       // GOD.PlayerInter.ShowNewItem(IconS.thisName, IconS.ResourceCount);
        ChestScript curChest = GOD.InventoryContr.CurrentChest.GetComponent<ChestScript>();
        curChest.MyIcons.Remove(IconS);
        IconS.transform.SetParent(newParent);
        IconS.InChest = null;
        if (curChest.name == "IceChest" && IconS.currentValidity>0)        // если еда в холодильнике
            InventoryContr.ObjectsWithValidity.Add(IconS);
    }
    public void AddChest(InfoScript IconS, Transform newParent)
    {
        IconS.transform.SetParent(newParent);
        ChestScript curChest = GOD.InventoryContr.CurrentChest.GetComponent<ChestScript>();
        curChest.MyIcons.Add(IconS);
        if (IconS.SlotInGame)
            IconS.InGame.OutIcon();
        GOD.InventoryContr.AllIcons.Remove(IconS);
       // GOD.PlayerInter.ShowLoseItem("Icon_" + IconS.thisName, IconS.ResourceCount, false);
        IconS.InChest = curChest;
        if (curChest.name == "IceChest")        // если еда в холодильнике
            InventoryContr.ObjectsWithValidity.Remove(IconS);
    }


    void MoveIcons(InfoScript I, InfoScript IconS)
    {
        if ((IconS.type == CurrentType) && (IconS.thisName == I.thisName))  // если в слотах одинаковые объекты
        {
            if (IconS.MaxResourceCount > 1)            // для стакающихся
            {
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                CombineIcon(I, IconS);
              
            }
            else                           // для нестакающихся
            {
                if (SlotsOnPlayer || IconS.ParentSlot.GetComponent<Slot>().SlotsOnPlayer)                                                      // если слот на игроку
                {
                    string HName2 = IconS.ParentSlot.name;
                    if (HName2.Contains("Mantle"))
                        HName2 = "Mantle";
                    if (IconS.OnPlayerSlot == SlotName)
                        PlayerContr.SetPlayerParametre(DragHandler.itemBeingDragged, true);                         // пересчитываем параметры игрока
                    else if (IconS.OnPlayerSlot == HName2)
                        PlayerContr.SetPlayerParametre(transform.GetChild(0).gameObject, true);
                }

                Transform T = transform.GetChild(0);
                Transform OldParent = IconS.ParentSlot;
                T.SetParent(OldParent);
                T.GetComponent<InfoScript>().ParentSlot = OldParent;

                DragHandler.itemBeingDragged.transform.SetParent(transform);
                IconS.ParentSlot = transform;
            }
        }
        else                        // меняем объекты местами так как разыне объекты
        {
            if ((!SlotsOnPlayer && !IconS.ParentSlot.GetComponent<Slot>().SlotsOnPlayer) || transform.GetChild(0).GetComponent<InfoScript>().OnPlayerSlot == IconS.ParentSlot.GetComponent<Slot>().SlotName || IconS.OnPlayerSlot == SlotName)
            {
                if (SlotsOnPlayer)
                {
                    if ((DragHandler.itemBeingDragged.GetComponent<InfoScript>().OnPlayerSlot == name))
                    {
                        Transform T = transform.GetChild(0);
                        T.SetParent(IconS.ParentSlot);
                        T.GetComponent<InfoScript>().ParentSlot = IconS.ParentSlot;

                        DragHandler.itemBeingDragged.transform.SetParent(transform);
                        IconS.ParentSlot = transform;
                        PlayerContr.RemovePlayerParametre(T.gameObject, false);                         // пересчитываем параметры игрока
                        PlayerContr.SetPlayerParametre(DragHandler.itemBeingDragged, true);                         // пересчитываем параметры игрока
                    }
                }
                else
                {
                    Transform T = transform.GetChild(0);
                    Transform OldParent = IconS.ParentSlot;
                    T.SetParent(OldParent);
                    T.GetComponent<InfoScript>().ParentSlot = OldParent;

                    DragHandler.itemBeingDragged.transform.SetParent(transform);
                    IconS.ParentSlot = transform;

                    if (OldParent.GetComponent<Slot>().SlotsOnPlayer)
                    {
                        PlayerContr.RemovePlayerParametre(DragHandler.itemBeingDragged, false);                         // пересчитываем параметры игрока
                        PlayerContr.SetPlayerParametre(T.gameObject, true);
                    }
                    else
                        GOD.Audio.Sound.PlayOneShot(GOD.Audio.Slot);
                }
            }

        }
    }

    void CombineIcon(InfoScript I, InfoScript IconS) //объединяем иконки
    {
        if (I.ResourceCount < I.MaxResourceCount)      // складываем объекты
        {
            int max = IconS.ResourceCount;
            for (int i = 0; i < max; i++)
            {
                if (I.ResourceCount < 10)
                {
                    I.ResourceCount++;
                    IconS.ResourceCount--;
                }
            }
            I.SetTextCount();
            if (I.currentValidity > 0) //берем среднюю годность
            {
                I.currentValidity = (I.currentValidity + IconS.currentValidity) / 2;
                GOD.PlayerInter.ShowValidity(I);
            }
            if (IconS.ResourceCount > 0)
                IconS.SetTextCount();
            else
                InventoryContr.DeleteIcon(IconS.gameObject, false);
        }
    }
}
