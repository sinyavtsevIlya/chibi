﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapController : MonoBehaviour {

    public GameManager GOD;
    public GameObject MapWindow;
    public RectTransform Map;
    public Slider MapSlider;
    public Image BackCloud;
    public RectTransform Player;
    Image PlayerImage;
    public GameObject[] Islands;
    public Transform Lenta;
    GameObject[] IslandsLenta; //ленты с названием островов
    Text[] IslandsNames;
    public GameObject[] ClosedIslands;
    Image[] IslandsImage, ClosedIslandsImage;
    public GameObject AllClouds;
    public RectTransform[] Clouds;
    public Vector3[] CloudsPositions;

    Vector3[] PlayerPosition = new Vector3[10];
    public bool[] OpenIslands = new bool[10];


    public void Init()
    {
        if (!GOD.IsTutorial)
        {
            PlayerPosition[0] = new Vector3(21.7f, -43.8f, 0);
            PlayerPosition[1] = new Vector3(-102, -79, 0);
            PlayerPosition[2] = new Vector3(-205, -144.5f, 0);
            PlayerPosition[3] = new Vector3(-315, -44, 0);
            PlayerPosition[4] = new Vector3(-57, 42, 0);
            PlayerPosition[5] = new Vector3(17, 126, 0);
            PlayerPosition[6] = new Vector3(-132, 174, 0);
            PlayerPosition[7] = new Vector3(130, -5, 0);
            PlayerPosition[8] = new Vector3(200, 62, 0);
            PlayerPosition[9] = new Vector3(310, -17, 0);

            OpenIslands[0] = true;
            OpenIslands[1] = false;
            OpenIslands[2] = false;
            OpenIslands[3] = false;
            OpenIslands[4] = false;
            OpenIslands[5] = false;
            OpenIslands[6] = false;
            OpenIslands[7] = false;
            OpenIslands[8] = false;
            OpenIslands[9] = false;

            //берем иммейджи для дальнейшей подгрузки и выгрузки
            IslandsImage = new Image[Islands.Length];
            ClosedIslandsImage = new Image[ClosedIslands.Length];
            for (int i = 0; i < Islands.Length; i++)
            {
                IslandsImage[i] = Islands[i].GetComponent<Image>();
                ClosedIslandsImage[i] = ClosedIslands[i].GetComponent<Image>();
            }
            PlayerImage = Player.GetComponent<Image>();

            IslandsLenta = new GameObject[Lenta.childCount];
            IslandsNames = new Text[Lenta.childCount];
            for (int i = 0; i < Lenta.childCount; i++)//заполняем массив лент и текстов
            {
                IslandsLenta[i] = Lenta.GetChild(i).gameObject;
                IslandsNames[i] = IslandsLenta[i].transform.GetChild(0).GetComponent<Text>();
            }
            InitLanguage();
        }
    }
  
    public void InitLanguage()
    {
        if (!GOD.IsTutorial)
        {
            IslandsNames[0].text = Localization.instance.getTextByKey("#NameGreenIsland");
            IslandsNames[1].text = Localization.instance.getTextByKey("#NameWinterIsland1");
            IslandsNames[2].text = Localization.instance.getTextByKey("#NameWinterIsland2");
            IslandsNames[3].text = Localization.instance.getTextByKey("#NameWinterIsland3");
            IslandsNames[4].text = Localization.instance.getTextByKey("#NameWaterIsland1");
            IslandsNames[5].text = Localization.instance.getTextByKey("#NameWaterIsland2");
            IslandsNames[6].text = Localization.instance.getTextByKey("#NameWaterIsland3");
            IslandsNames[7].text = Localization.instance.getTextByKey("#NameSandIsland1");
            IslandsNames[8].text = Localization.instance.getTextByKey("#NameSandIsland2");
            IslandsNames[9].text = Localization.instance.getTextByKey("#NameSandIsland3");
        }

    }

    public void OpenMap()
    {
        SetPlayerOnMap();
        LoadImages();
        MapWindow.SetActive(true);
        GOD.GamePause();
    }
    void LoadImages()  //подгружаем изображения
    {
        IslandsImage[0].sprite = Resources.Load<Sprite>("Map/MainIsland");
        IslandsImage[1].sprite = Resources.Load<Sprite>("Map/WinterIsland1");
        IslandsImage[2].sprite = Resources.Load<Sprite>("Map/WinterIsland2");
        IslandsImage[3].sprite = Resources.Load<Sprite>("Map/WinterIsland3");
        IslandsImage[4].sprite = Resources.Load<Sprite>("Map/WaterIsland1");
        IslandsImage[5].sprite = Resources.Load<Sprite>("Map/WaterIsland2");
        IslandsImage[6].sprite = Resources.Load<Sprite>("Map/WaterIsland3");
        IslandsImage[7].sprite = Resources.Load<Sprite>("Map/SandIsland1");
        IslandsImage[8].sprite = Resources.Load<Sprite>("Map/SandIsland2");
        IslandsImage[9].sprite = Resources.Load<Sprite>("Map/SandIsland3");

        for (int i = 0; i < IslandsImage.Length; i++)
            ClosedIslandsImage[i].sprite = IslandsImage[i].sprite;

        PlayerImage.sprite = Resources.Load<Sprite>("Map/Character");
        BackCloud.sprite = Resources.Load<Sprite>("Map/Clouds");
    }
    void UnLoadImages()
    {
        for (int i = 0; i < IslandsImage.Length; i++)
        {
            IslandsImage[i].sprite = null;
            ClosedIslandsImage[i].sprite = null;
        }
        PlayerImage.sprite = null;
        BackCloud.sprite = null;
        Resources.UnloadUnusedAssets();
    }
    public void CloseMap()
    {
        Time.timeScale = 1;
        MapWindow.SetActive(false);
        UnLoadImages();
    }
    public void ScaleMap()
    {
        Map.localScale = new Vector3(MapSlider.value, MapSlider.value, MapSlider.value);
    }
    public void SetPlayerOnMap()  //ставим персонажа на тот остров, на котором он находится
    {
        switch(GOD.EnviromentContr.CurrentIsland)
        {
            case "GreenIsland":
                Player.localPosition = PlayerPosition[0];
                break;
            case "WinterIsland1":
                Player.localPosition = PlayerPosition[1];
                break;
            case "WinterIsland2":
                Player.localPosition = PlayerPosition[2];
                break;
            case "WinterIsland3":
                Player.localPosition = PlayerPosition[3];
                break;
            case "WaterIsland1":
                Player.localPosition = PlayerPosition[4];
                break;
            case "WaterIsland2":
                Player.localPosition = PlayerPosition[5];
                break;
            case "WaterIsland3":
                Player.localPosition = PlayerPosition[6];
                break;
            case "SandIsland1":
                Player.localPosition = PlayerPosition[7];
                break;
            case "SandIsland2":
                Player.localPosition = PlayerPosition[8];
                break;
            case "SandIsland3":
                Player.localPosition = PlayerPosition[9];
                break;
        }
    }

    public void SetOpenIslands()         //устанавливаем какие острова открыты
    {
        int x = 0;
        switch (GOD.EnviromentContr.CurrentIsland)
        {
            case "GreenIsland":
                x = 0;
                break;
            case "WinterIsland1":
                x = 1;
                break;
            case "WinterIsland2":
                x = 2;
                break;
            case "WinterIsland3":
                x = 3;
                break;
            case "WaterIsland1":
                x = 4;
                break;
            case "WaterIsland2":
                x = 5;
                break;
            case "WaterIsland3":
                x = 6;
                break;
            case "SandIsland1":
                x = 7;
                break;
            case "SandIsland2":
                x = 8;
                break;
            case "SandIsland3":
                x = 9;
                break;
        }
        GOD.Progress.SetOpenIsland(x);  //ачивка Открытие островов
        if (GOD.EndOfLoad && !OpenIslands[x])
        {
            GOD.Offers.OpenOffersWindow("IconStartNabor");
            OpenIslands[x] = true;

            if (PlayerPrefs.HasKey("AllMapProgress"))
            {
                bool[] all = PlayerPrefsX.GetBoolArray("AllMapProgress");    //проверяем что открыто впервые на этом устройстве
                if (!all[x])
                {
                    all[x] = true;
                    PlayerPrefsX.SetBoolArray("AllMapProgress", all);
                    MyAppMetrica.LogEvent("Game", "Open Islands", GOD.EnviromentContr.CurrentIsland);
                }
            }
            else
                PlayerPrefsX.SetBoolArray("AllMapProgress", OpenIslands);

            SetClouds();
        }
        SetPlayerOnMap();
    }

    public void SetClouds()  //устанавливаем облака, в зависимости от открытости островов
    {
        if (OpenIslands[1])
        {
            Clouds[0].gameObject.SetActive(false);
            Islands[1].SetActive(true);
            IslandsLenta[1].SetActive(true);
            ClosedIslands[1].SetActive(false);
        }
        if (OpenIslands[2])
        {
            Clouds[1].gameObject.SetActive(false);
            Clouds[2].gameObject.SetActive(false);
            Clouds[3].gameObject.SetActive(false);
            Islands[2].SetActive(true);
            IslandsLenta[2].SetActive(true);
            ClosedIslands[2].SetActive(false);
        }
        if (OpenIslands[3])
        {
            Clouds[4].gameObject.SetActive(false);
            Clouds[5].gameObject.SetActive(false);
            Clouds[6].gameObject.SetActive(false);
            Islands[3].SetActive(true);
            IslandsLenta[3].SetActive(true);
            ClosedIslands[3].SetActive(false);
        }
        if (OpenIslands[4])
        {
            Clouds[7].gameObject.SetActive(false);
            Clouds[8].gameObject.SetActive(false);
            Islands[4].SetActive(true);
            IslandsLenta[4].SetActive(true);
            ClosedIslands[4].SetActive(false);
        }
        if (OpenIslands[5])
        {
            Clouds[9].gameObject.SetActive(false);
            Clouds[10].gameObject.SetActive(false);
            Clouds[11].gameObject.SetActive(false);
            Islands[5].SetActive(true);
            IslandsLenta[5].SetActive(true);
            ClosedIslands[5].SetActive(false);
        }
        if (OpenIslands[6])
        {
            Clouds[12].gameObject.SetActive(false);
            Clouds[13].gameObject.SetActive(false);
            Clouds[14].gameObject.SetActive(false);
            Islands[6].SetActive(true);
            IslandsLenta[6].SetActive(true);
            ClosedIslands[6].SetActive(false);
        }
        if (OpenIslands[7])
        {
            Clouds[15].gameObject.SetActive(false);
            Clouds[16].gameObject.SetActive(false);
            Islands[7].SetActive(true);
            IslandsLenta[7].SetActive(true);
            ClosedIslands[7].SetActive(false);
        }
        if (OpenIslands[8])
        {
            Clouds[17].gameObject.SetActive(false);
            Clouds[18].gameObject.SetActive(false);
            Islands[8].SetActive(true);
            IslandsLenta[8].SetActive(true);
            ClosedIslands[8].SetActive(false);
        }
        if (OpenIslands[9])
        {
            Clouds[19].gameObject.SetActive(false);
            Clouds[20].gameObject.SetActive(false);
            Clouds[21].gameObject.SetActive(false);
            Clouds[22].gameObject.SetActive(false);
            Islands[9].SetActive(true);
            IslandsLenta[9].SetActive(true);
            ClosedIslands[9].SetActive(false);
        }

        bool all = true;        //проверяем все ли открыты
        for (int i = 0; i < OpenIslands.Length; i++)
        {
            if(!OpenIslands[i])
            {
                all = false;
                break;
            }
        }
        if (all)
            AllClouds.SetActive(false);
    }


}
