﻿using UnityEngine;
using System.Collections;

public class RecipeScript : MonoBehaviour {

     public string type;  // resources, instrument, food, weapon, clothes, buildings
    public string[] NameOfIngridient;
    public int[] NumberOfIngridient;
    public string building = "";     // smithy - кузня, stove - печь, windmill - мельница, mannequie - манекен


    public void Initialize()
    {
        switch(name)
        {
            // РЕСУРСЫ______________________________________________________________________________________________________________________________________________________
          
            case "Plank":                                   // доска
                type = "resources";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Wood";
                NumberOfIngridient[0] = 2;
                break;
            case "SharpStone":                              // острый камень
                type = "resources";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Stone";
                NumberOfIngridient[0] = 2;
                break;
            case "StoneBlock":                              // каменный блок
                type = "resources";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Stone";
                NumberOfIngridient[0] = 3;
                break;
            case "IronIngot":                               // железный слиток
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "IronOre";
                NumberOfIngridient[0] = 2;
                building = "Stove";
                break;
            case "DimondPlate":                             // алмазная пластина
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Dimond";
                NumberOfIngridient[0] = 1;
                building = "Stove";
                break;
            case "BonePlate":                                   // костяная пластина
                type = "resource";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Bone";
                NameOfIngridient[1] = "ChitinPlate";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                break;
            case "ChitinPlate":                              // хитиновая пластина
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "CrabShell";
                NumberOfIngridient[0] = 2;
                break;
            case "Leather":                              // обработанная кожа
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Skin";
                NumberOfIngridient[0] = 2;
                break;
            case "Thread":                                    // нить
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Leaf";
                NumberOfIngridient[0] = 2;
                break;
            case "Cord":                                       // шнур
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Wool";
                NumberOfIngridient[0] = 2;
                break;
            case "Rope":                                       // веревка
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Web";
                NumberOfIngridient[0] = 2;
                break;
            case "DimondCord":                                    // алмазный шнур
                type = "resource";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Rope";
                NameOfIngridient[1] = "Dimond";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 1;
                break;
            case "Glass":                                   // стекло
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Sand";
                NumberOfIngridient[0] = 2;
                building = "Stove";
                break;
            case "BerrieSouce":                                    // шоколад
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Berries";
                NumberOfIngridient[0] = 4;
                break;
            case "Flour":                                    // мука
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Seed";
                NumberOfIngridient[0] = 4;
                building = "Windmill";
                break;
            case "Dimond":                                    // алмаз
                type = "resource";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Crystall";
                NumberOfIngridient[0] = 20;
                break;

            // ОДЕЖДА______________________________________________________________________________________________________________________________________________________
            case "LeatherHat":                              // кожанная шляпа
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Leather";
                NameOfIngridient[1] = "Thread";
                NameOfIngridient[2] = "Wool";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                break;
            case "LeatherCoat":                           // кожанный доспех
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Leather";
                NameOfIngridient[1] = "Thread";
                NameOfIngridient[2] = "Wool";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 2;
                break;
            case "LeatherPants":                          // кожанные штаны
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Leather";
                NameOfIngridient[1] = "Thread";
                NameOfIngridient[2] = "Wool";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 2;
                break;
            case "LeatherShoes":                         // кожанная обувь
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Leather";
                NameOfIngridient[1] = "Thread";
                NameOfIngridient[2] = "Wool";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                break;
            case "IronHat":                              // железная шляпа
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "LeatherHat";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Cord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                break;
            case "IronCoat":                              // железный нагрудник
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "LeatherCoat";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Cord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 2;
                break;
            case "IronPants":                              // железные штаны
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "LeatherPants";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Cord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                break;
            case "IronShoes":                              // железная обувь
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "LeatherPants";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Cord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                break;
            case "BoneHat":                              // костяной шлем
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "IronHat";
                NameOfIngridient[1] = "BonePlate";
                NameOfIngridient[2] = "Rope";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                building = "Mannequie";
                break;
            case "BoneCoat":                              // костяной нагрудник
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "IronCoat";
                NameOfIngridient[1] = "BonePlate";
                NameOfIngridient[2] = "Rope";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 3;
                building = "Mannequie";
                break;
            case "BonePants":                              //костяный штаны
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "IronPants";
                NameOfIngridient[1] = "BonePlate";
                NameOfIngridient[2] = "Rope";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 2;
                building = "Mannequie";
                break;
            case "BoneShoes":                              // костяная обувь
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "IronShoes";
                NameOfIngridient[1] = "BonePlate";
                NameOfIngridient[2] = "Rope";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                building = "Mannequie";
                break;
            case "DimondHat":                              // алмазный шлем
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "BoneHat";
                NameOfIngridient[1] = "DimondPlate";
                NameOfIngridient[2] = "DimondCord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                building = "Mannequie";
                break;
            case "DimondCoat":                              // алмазный нагрудник
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "BoneCoat";
                NameOfIngridient[1] = "DimondPlate";
                NameOfIngridient[2] = "DimondCord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 2;
                building = "Mannequie";
                break;
            case "DimondPants":                              // алмазные штаны
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "BonePants";
                NameOfIngridient[1] = "DimondPlate";
                NameOfIngridient[2] = "DimondCord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 2;
                building = "Mannequie";
                break;
            case "DimondShoes":                              // алмазная обувь
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "BoneShoes";
                NameOfIngridient[1] = "DimondPlate";
                NameOfIngridient[2] = "DimondCord";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 2;
                building = "Mannequie";
                break;


            case "LeatherBelt":                              // кожанный пояс
                type = "clothe";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "SharpStone";
                NameOfIngridient[1] = "Leather";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                break;
            case "EmeraldBelt":                              // пояс с изумрудом
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "LeatherBelt";
                NameOfIngridient[1] = "Leather";
                NameOfIngridient[2] = "Emerald";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 4;
                NumberOfIngridient[2] = 1;
                building = "Mannequie";
                break;
            case "SapphireBelt":                              // пояс с сапфиром
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "EmeraldBelt";
                NameOfIngridient[1] = "Leather";
                NameOfIngridient[2] = "Sapphire";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 6;
                NumberOfIngridient[2] = 2;
                building = "Mannequie";
                break;
            case "AmethystBelt":                              // пояс с аметистом
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "SapphireBelt";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Amethyst";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 3;
                building = "Mannequie";
                break;
            case "RubyBelt":                              // пояс с рубином
                type = "clothe";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "AmethystBelt";
                NameOfIngridient[1] = "BonePlate";
                NameOfIngridient[2] = "Ruby";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 4;
                building = "Mannequie";
                break;
           
            

            case "WebMantle":                              // плащ из паутины
                type = "clothe";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Web";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 10;
                NumberOfIngridient[1] = 10;
                building = "Mannequie";
                break;
            case "ChitinMantle":                              // хитиновый плащ
                type = "clothe";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "CrabShell";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 10;
                NumberOfIngridient[1] = 10;
                building = "Mannequie";
                break;

            case "WoolMantle":                              // шерстяной плащ
                type = "clothe";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Wool";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 10;
                NumberOfIngridient[1] = 10;
                building = "Mannequie";
                break;

            // ИНСТРУМЕНТЫ______________________________________________________________________________________________________________________________________________________
            case "WoodAxe":                                   // деревянный топор
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Wood";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                break;
            case "StoneAxe":                               // каменный топор
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Stone";
                NameOfIngridient[2] = "SharpStone";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 2;
                break;
            case "IronAxe":                                // железный топор
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "IronIngot";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 2;
                building = "Smithy";
                break;
            case "BoneAxe":                              // костяной топор
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Bone";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 3;
                building = "Smithy";
                break;
            case "DimondAxe":                            // алмазный топор
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Dimond";
                NameOfIngridient[2] = "IronIngot";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                building = "Smithy";
                break;


            case "WoodPick":                          // деревянная кирка
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Wood";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                break;
            case "StonePick":                     // каменный кирка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Flint";
                NameOfIngridient[2] = "SharpStone";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                break;
            case "IronPick":                          // железная кирка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                building = "Smithy";
                break;
            case "BonePick":                          // костяная кирка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Bone";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 3;
                NumberOfIngridient[2] = 2;
                building = "Smithy";
                break;
            case "DimondPick":                          // алмазная кирка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Dimond";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 2;
                building = "Smithy";
                break;


            case "WoodShovel":                          // деревянная лопата
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Wood";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                break;
            case "StoneShovel":                     // каменный лопата
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Flint";
                NameOfIngridient[2] = "SharpStone";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                break;
            case "IronShovel":                          // железная лопата
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                building = "Smithy";
                break;
            case "BoneShovel":                          // костяная лопата
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "BonePlate";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 2;
                building = "Smithy";
                break;
            case "DimondShovel":                          // алмазная лопата
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Dimond";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 3;
                building = "Smithy";
                break;


            case "FragileRod":                          //хрупкая удочка
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                break;
            case "NormalRod":                          // обычная удочка
                type = "instrument";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Cord";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                break;
            case "StrongRod":                          // крепкая удочка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Cord";
                NameOfIngridient[2] = "IronIngot";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                building = "Smithy";
                break;
            case "DurableRod":                          // прочная удочка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "BonePlate";
                NameOfIngridient[2] = "Rope";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 2;
                building = "Smithy";
                break;
            case "DimondRod":                          // алмазная удочка
                type = "instrument";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "DimondCord";
                NameOfIngridient[2] = "Dimond";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[2] = 1;
                building = "Smithy";
                break;

            case "Bucket":                          // железное ведро
                type = "instrument";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "IronIngot";
                NumberOfIngridient[0] = 3;
                building = "Smithy";
                break;

            // ОРУЖИЕ______________________________________________________________________________________________________________________________________________________
            case "WoodSword":
                type = "weapon";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Stick";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                break;
            case "StoneSword":
                type = "weapon";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Flint";
                NameOfIngridient[2] = "SharpStone";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 4;
                NumberOfIngridient[2] = 2;
                break;
            case "IronSword":
                type = "weapon";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "IronIngot";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 4;
                building = "Smithy";
                break;
            case "BoneSword":
                type = "weapon";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Bone";
                NameOfIngridient[2] = "BonePlate";
                NumberOfIngridient[0] = 5;
                NumberOfIngridient[1] = 4;
                NumberOfIngridient[2] = 2;
                building = "Smithy";
                break;
            case "DimondSword":
                type = "weapon";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Dimond";
                NameOfIngridient[2] = "DimondPlate";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 1;
                building = "Smithy";
                break;

            // ЕДА______________________________________________________________________________________________________________________________________________________
            case "Chocolate":                                    // шоколад
                type = "food";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "CocoaBeans";
                NumberOfIngridient[0] = 2;
                building = "Windmill";
                break;

            case "Candy":                                        // конфеты

                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Pear";
                NameOfIngridient[1] = "Chocolate";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 1;
                break;
            case "HoneyDessert":                                 // медовый десерт
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Honey";
                NameOfIngridient[1] = "Berrie";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 3;
                break;
            case "FruitSalad":                          // Фруктовый салат
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Pear";
                NameOfIngridient[1] = "Berries";
                NameOfIngridient[2] = "Apple";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[2] = 2;
                break;
            case "GrapesInChocolate":                                        // виноград в шоколаде
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Chocolate";
                NameOfIngridient[1] = "Grapes";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 3;
                break;
            case "FiredFish":                                        // жаренная рыба
                type = "food";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Som";
                NumberOfIngridient[0] = 1;
                building = "Stove";
                break;
            case "Cutlet":                                        // котлетта
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Meat";
                NameOfIngridient[1] = "Carp";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                building = "Stove";
                break;
            case "AlgaeSalad":                                        // салат из водорослей
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Alga";
                NameOfIngridient[1] = "Leaf";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 6;
                break;
            case "CactusPure":                                        // кактосовое пюре
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "CactusPiece";
                NameOfIngridient[1] = "Grapes";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 4;
                break;
            case "FiredMeat":                                        // жаренная мясо
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Meat";
                NameOfIngridient[1] = "Condiment";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                building = "Stove";
                break;
            case "FishSoup":                                        // уха
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Sprat";
                NameOfIngridient[1] = "Bucket";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                building = "Stove";
                break;
            case "Soup":                                        // суп
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Meat";
                NameOfIngridient[1] = "Bucket";
                NameOfIngridient[2] = "Condiment";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[1] = 1;
                building = "Stove";
                break;
            case "FishPie":                                        // рыбный пирог
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Trout";
                NameOfIngridient[1] = "Flour";
                NameOfIngridient[2] = "Condiment";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[1] = 2;
                building = "Stove";
                break;
            case "BerriePie":                                        // ягодный пирог
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "BerrieSouce";
                NameOfIngridient[1] = "Flour";
                NameOfIngridient[2] = "Berries";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                NumberOfIngridient[1] = 4;
                building = "Stove";
                break;
            case "MeatPies":                                        // пирожки с мясом
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Meat";
                NameOfIngridient[1] = "Flour";
                NumberOfIngridient[0] = 2;
                NumberOfIngridient[1] = 2;
                building = "Stove";
                break;
            case "Vine":                                        // вино
                type = "food";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Berries";
                NameOfIngridient[1] = "Bucket";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 4;
                break;
            case "Roll":                                        // ролл
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Alga";
                NameOfIngridient[1] = "Salmon";
                NameOfIngridient[2] = "Seed";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 1;
                NumberOfIngridient[1] = 2;
                building = "Stove";
                break;


           
            // ЗДАНИЯ______________________________________________________________________________________________________________________________________________________
            case "Smithy":                        // кузница
                type = "building";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "StoneBlock";
                NameOfIngridient[1] = "Plank";
                NameOfIngridient[2] = "IronOre";
                NumberOfIngridient[0] = 10;
                NumberOfIngridient[1] = 5;
                NumberOfIngridient[2] = 10;
                break;
            case "Windmill":                      // мельница
                type = "building";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "StoneBlock";
                NameOfIngridient[2] = "IronOre";
                NumberOfIngridient[0] = 10;
                NumberOfIngridient[1] = 5;
                NumberOfIngridient[2] = 10;
                break;
            case "Stove":                         // печь
                type = "building";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "StoneBlock";
                NameOfIngridient[1] = "Wood";
                NameOfIngridient[2] = "Flint";
                NumberOfIngridient[0] = 10;
                NumberOfIngridient[1] = 10;
                NumberOfIngridient[2] = 10;
                break;
            case "Mannequie":                       // манекен
                type = "building";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Thread";
                NameOfIngridient[1] = "Needle";
                NameOfIngridient[2] = "Stick";
                NumberOfIngridient[0] = 5;
                NumberOfIngridient[1] = 10;
                NumberOfIngridient[2] = 10;
                break;
            case "Firecamp":                        // костер
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Stick";
                NameOfIngridient[1] = "Flint";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 2;
                break;
            case "Chest":                     // сундук
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "IronIngot";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 4;
                break;
            case "IceChest":                 // ледянной сундук
                type = "building";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Snow";
                NameOfIngridient[2] = "Ice";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 10;
                NumberOfIngridient[2] = 10;
                break;

            case "WoodBase":                 // фундамент
                type = "building";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Plank";
                NumberOfIngridient[0] = 2;
                break;
            case "WoodFloor":                 //пол
                type = "building";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Plank";
                NumberOfIngridient[0] = 2;
                break;
            case "WoodStair":                 // лестница
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 4;
                break;
            case "WoodFence":                 // забор
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Stick";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 6;
                break;
            case "WoodWall":                 // стена
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 4;
                break;
            case "WoodWallDoor":                 // стена с дверью
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 2;
                break;
            case "WoodWallWindow":                 // стена с окном
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 3;
                NumberOfIngridient[1] = 3;
                break;
            case "Window":                 // окно
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Glass";
                NameOfIngridient[1] = "Stick";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 4;
                break;
            case "Door":                 // дверь
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "IronIngot";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 1;
                break;
            case "WoodRoof":                 // крыша
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 4;
                NumberOfIngridient[1] = 4;
                break;
            case "WoodRoofTop":                 // крыша
                type = "building";
                NameOfIngridient = new string[2];
                NumberOfIngridient = new int[2];
                NameOfIngridient[0] = "Plank";
                NameOfIngridient[1] = "Thread";
                NumberOfIngridient[0] = 6;
                NumberOfIngridient[1] = 6;
                break;
            case "WoodRoofCorner":                 // крыша угол
                type = "building";
                NameOfIngridient = new string[1];
                NumberOfIngridient = new int[1];
                NameOfIngridient[0] = "Plank";
                NumberOfIngridient[0] = 4;
                break;

            // Алхимия_________________________________________________________________
            case "HealingSalve":                 // исцеляющая мазь
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Leaf";
                NameOfIngridient[1] = "FlowerRed";
                NameOfIngridient[2] = "FlowerOrange";
                NumberOfIngridient[0] = 5;
                NumberOfIngridient[1] = 5;
                NumberOfIngridient[2] = 5;
                break;
            case "StrongHealingSalve":                 //  сильная исцеляющая мазь
                type = "food";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "FlowerBlue";
                NameOfIngridient[1] = "FlowerRed";
                NameOfIngridient[2] = "FlowerOrange";
                NumberOfIngridient[0] = 5;
                NumberOfIngridient[1] = 5;
                NumberOfIngridient[2] = 5;
                break;
            // Магия_________________________________________________________________
            case "EssenceIce":                 // сущность холода
                type = "resource";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Sapphire";
                NameOfIngridient[1] = "Snow";
                NameOfIngridient[2] = "Ice";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 5;
                NumberOfIngridient[2] = 5;
                break;
            case "EssenceSun":                 // сущность жары
                type = "resource";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Ruby";
                NameOfIngridient[1] = "Sand";
                NameOfIngridient[2] = "Needle";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 5;
                NumberOfIngridient[2] = 5;
                break;
            case "EssenceWater":                 // сущность дождя
                type = "resource";
                NameOfIngridient = new string[3];
                NumberOfIngridient = new int[3];
                NameOfIngridient[0] = "Amethyst";
                NameOfIngridient[1] = "Bucket";
                NameOfIngridient[2] = "Grapes";
                NumberOfIngridient[0] = 1;
                NumberOfIngridient[1] = 4;
                NumberOfIngridient[2] = 5;
                break;

        }
    }

}
