public enum TargetMarket
{
    GOOGLE = 0,
    AMAZON = 1,
    IOS = 2
}

public class BuildSettings
{
    public static bool isDebug = false;
    public static bool isFree = true;

#if UNITY_ANDROID
    public static TargetMarket Market = TargetMarket.GOOGLE;
#elif UNITY_IOS
    public static TargetMarket Market = TargetMarket.IOS;
#endif

    private static string nameGooglePaid = "Chibi Survivor - Weather Lord";

    private static string bundleGooglePaid = "com.gamefirst.chibisurvivorlord";
    private static string AppMetricaKeyGooglePaid = "4cdc876e-5671-43d4-8373-888125c08843";
    private static string publicKeyGooglePaid = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh4Ho372Zi9lQ9lq/m879xsVjmPEkOO8w6IUMOqgiJ8xfrVEF7ADzdyLt1ujlXTh8tdDO8tBLIaTffSY6ZeNR++IoSH0gT/6xxzyj4v+cyQe9NftVOWqbWD3D0LaFEDu71pO3zfj+efbfWm8cluALPqilknLXpjuPm7Ph1I++vqmsEZwFuIZo/LQzipesXS7RH8r0i0tYPSXSuyp7UVm1iTKroeX9ol+vBRZdthAIl8fYtdSfpUuWxw50lLkA/F2jbFNY5rbqSScaB2vGwND164QctMSCh2QxuhBY+M0FbBuv8lrO94lMQ7ed0z1N8xOf5uxTg3W3kjYEfSfvDOZ9HwIDAQAB";

    private static string bundleGoogleFree = "com.gamefirst.chibisurvivor";
    private static string AppMetricaKeyGoogleFree = "d1b02a36-86f5-4f0e-b579-fa717e1ef250";
    private static string AppodealAppKeyGoogleFree = "90c8c91fbc44c39fb9d473dfcebc38a69a2f8415293000f3";
    private static string publicKeyGoogleFree = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxrjFoyFZSVIke2pwYqsBPehdWxkAfE0z5AH5n29dp6tcZAemed9oXvECEoDar41foLqH8dzhWoghy9u9tTquv9yYNDDDd4m5nIA41I4I4lqTyHSvn8otT7wO5DBR5VinAHDDWSjrGPORYnXJ2npXsyzmvt+cC94zVnhNh39HFyXh4tkwzDhr/4nXD1uQZZ/GepCEsMveSbNZJQhLhf8RlMsCSUe4NmzLkfLGKlS1TL5yEzfrB5YKL1p5ZMA+1nqT9mq3N7m+vcvE0XGuBlpHsuT+Ok63XPw0Xde3p34rJRasfBLIXY1/j2EPyqSlAl60zouQFdquJKg7wNNgW6Na4QIDAQAB";


    private static string nameAmazonPaid = "Chibi Survivor - Weather Lord";
    private static string bundleAmazonPaid = "com.gamefirst.chibisurvivorlord";
    private static string AppodealAppKeyAmazonFree = "";
    private static string AppMetricaKeyAmazon = "";


    private static string nameIOSFree = "Chibi Survivor - Weather Lord";
    private static string bundleIOSFree = "com.gamefirst.chibisurvivor";
    private static string AppMetricaKeyIOSFree = "d1b02a36-86f5-4f0e-b579-fa717e1ef250";
    private static string AppodealAppKeyIOSFree = "facd74edb649b8a858fc39674281c2dc295c89938f178f57";

    public static string AppodealAppKey
    {
        get
        {
            switch (Market)
            {
                case TargetMarket.GOOGLE: return AppodealAppKeyGoogleFree;
                case TargetMarket.AMAZON: return AppodealAppKeyAmazonFree;
                case TargetMarket.IOS: return AppodealAppKeyIOSFree;
            }

            return "";
        }
    }

    public static string AppMetricaKey
    {
        get
        {
            switch (Market)
            {
                case TargetMarket.GOOGLE: if (isFree) return AppMetricaKeyGoogleFree; else return AppMetricaKeyGooglePaid;
                case TargetMarket.AMAZON: return AppMetricaKeyAmazon;
                case TargetMarket.IOS: return AppMetricaKeyIOSFree;
            }
            return "";
        }
    }

    public static string bundle
    {
        get
        {
            switch (Market)
            {
                case TargetMarket.GOOGLE: if (isFree) return bundleGoogleFree; else return bundleGooglePaid;
                case TargetMarket.AMAZON: return bundleAmazonPaid;
                case TargetMarket.IOS: return bundleIOSFree;
            }

            return "";
        }
    }

    public static string productName
    {
        get
        {
            switch (Market)
            {
                case TargetMarket.GOOGLE: return nameGooglePaid;
                case TargetMarket.AMAZON: return nameAmazonPaid;
                case TargetMarket.IOS: return nameIOSFree;
            }

            return "";
        }
    }

    public static string bundleGoogle
    {
        get { if (isFree) return bundleGoogleFree; else return bundleGooglePaid; }
    }

    public static string publicKeyGoogle
    {
        get { if (isFree) return publicKeyGoogleFree; return publicKeyGooglePaid; }
    }

    public static string bundleAmazon
    {
        get { return bundleAmazonPaid; }
    }

    public static string bundleIOS
    {
        get { return bundleIOSFree; }
    }


}


