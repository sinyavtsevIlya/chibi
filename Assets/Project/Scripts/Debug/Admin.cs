﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Admin : MonoBehaviour {

    public GameManager GOD;
    public GameObject AdminIconParent, AdminBlack;
    public List<GameObject> AllAdminIcons = new List<GameObject>();
    public GameObject SlotForAdminIcons;
    public GameObject AdminWindow;
    public GameObject FPSWindow;
    CloseWindow AdminClose;



    public void Init()
    {
        if (BuildSettings.isDebug)
        {
            for (int i = 0; i < GOD.InventoryContr.IconsParent.childCount; i++)
            {
                Transform Current = GOD.InventoryContr.IconsParent.GetChild(i);
                if (Current.name != "Icon_Gem")
                {
                    GameObject g = Instantiate(AdminIconParent) as GameObject;
                    g.GetComponent<Image>().sprite = Current.GetComponent<Image>().sprite;
                    g.name = Current.name;
                    g.GetComponent<AdminIcon>().GOD = GOD;
                    g.SetActive(true);
                    AllAdminIcons.Add(g);
                }
            }

            for (int x = 0; x < AllAdminIcons.Count; x++)
            {
                for (int i = 0; i < SlotForAdminIcons.transform.childCount; i++)
                {
                    if (SlotForAdminIcons.transform.GetChild(i).transform.childCount <= 0)
                    {
                        AllAdminIcons[x].transform.SetParent(SlotForAdminIcons.transform.GetChild(i));
                        AllAdminIcons[x].transform.localScale = SlotForAdminIcons.transform.localScale;
                        break;
                    }
                }
            }
            AdminClose = gameObject.GetComponent<CloseWindow>();
        }
        else
        {
            this.enabled = false;
        }
    }

   
    public void OpenAdminWindow()
    {
        if (BuildSettings.isDebug)
        {
            AdminBlack.SetActive(true);
            AdminWindow.transform.localScale = Constants.AdminWindow;
            AdminWindow.SetActive(true);
        }
    }
    public void CloseAdminWindow()
    {

        AdminBlack.SetActive(false);
        AdminClose.StartClose();
        //AdminWindow.SetActive(false);
    }
    public void CloseAdminWindowImmediatly()
    {

        AdminBlack.SetActive(false);
        AdminClose.Close();
        //AdminWindow.SetActive(false);
    }

    public void SkipTutorial()
    {
        if (GOD.Tutor)
            GOD.Tutor.SkipTutorial();
    }
    public void PlusTime()
    {
        if (Time.timeScale == 0)
            GOD.GamePlay();
        GOD.TG.IMinute = 36;
    }

    public void ResAll()
    {
        GOD.PlayerContr.HP = 100;
        GOD.PlayerContr.EAT = 100;
    }

    public void OpenBridge()
    {
        GOD.ToSnow.IsOpen = true;
        GOD.ToSnow.SetResoures();

        GOD.ToRain.IsOpen = true;
        GOD.ToRain.SetResoures();

        GOD.ToSand.IsOpen = true;
        GOD.ToSand.SetResoures();

        GOD.DialogS.EnableDialogSphereBridge();

        Pathfinder.Instance.CreateMap();
    }

    public void AddGems()
    {
        Balance.Gem = Balance.Gem + 500;
        GOD.ShopScript.ShowGemCount();
    }

    public void ActivateFPS()
    {
        FPSWindow.SetActive(!FPSWindow.activeSelf);
    }
    public void ActivateConsole()
    {
        Consolation.Console c = GOD.GetComponent<Consolation.Console>();
        if (c == null)
            GOD.gameObject.AddComponent<Consolation.Console>();
        else
            Destroy(c);
    }
    public void ChangeMale()
    {
        if (GOD.OurPlayer == "Girl")
        {
            GOD.OurPlayer = "Boy";
            GOD.CreateNewPlayer(GOD.PlayerContr.CurrentBoySkinNumber);
        }
        else
        {
            GOD.OurPlayer = "Girl";
            GOD.CreateNewPlayer(GOD.PlayerContr.CurrentGirlSkinNumber);
        }
        GOD.Audio.ChangeMale();
    }

    void AfterTeleport()
    {
        GOD.EnviromentContr.PlayerOnIsland(GOD.ReturnIslandByName(GOD.EnviromentContr.CurrentIsland));
        GOD.CameraScript.TeleportCamera();
        GOD.EnviromentContr.CSnow.SetIceBreakLay(-1);
        GOD.EnviromentContr.CSand.SetCurrentBigTree();
        CloseAdminWindow();
    }
    public void TeleportToIzumrud()
    {
        GOD.Player.transform.position = new Vector3(0, 0, -86);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -11, 0));
        GOD.EnviromentContr.CurrentIsland = "GreenIsland";
        AfterTeleport();
    }
    public void TeleportToIce1()
    {
        GOD.Player.transform.position = new Vector3(-306, -0.7f, -14);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -117, 0));
        GOD.EnviromentContr.CurrentIsland = "WinterIsland1";
        AfterTeleport();
    }
    public void TeleportToIce2()
    {
        GOD.Player.transform.position = new Vector3(-438, 6, -63);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -22, 0));
        GOD.EnviromentContr.CurrentIsland = "WinterIsland2";
        AfterTeleport();
    }
    public void TeleportToIce3()
    {
        GOD.Player.transform.position = new Vector3(-532, 5.74f, 5);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -45, 0));
          GOD.EnviromentContr.CurrentIsland = "WinterIsland3";
        AfterTeleport();
    }
    public void TeleportToWater1()
    {
        GOD.Player.transform.position = new Vector3(-7.7f, -2, 258);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, 36, 0));
        GOD.EnviromentContr.CurrentIsland = "WaterIsland1";
        AfterTeleport();
    }
    public void TeleportToWater2()
    {
        GOD.Player.transform.position = new Vector3(26, 48, 306);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -44, 0));
        GOD.EnviromentContr.CurrentIsland = "WaterIsland2";
        AfterTeleport();
    }
    public void TeleportToWate3()
    {
        GOD.Player.transform.position = new Vector3(-108, 92.7f, 321);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -150, 0));
        GOD.EnviromentContr.CurrentIsland = "WaterIsland3";
        AfterTeleport();
    }
    public void TeleportToSun1()
    {
        GOD.Player.transform.position = new Vector3(307, 0, 37.7f);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, 44, 0));
        GOD.EnviromentContr.CurrentIsland = "SandIsland1";
        AfterTeleport();
    }
    public void TeleportToSun2()
    {
        GOD.Player.transform.position = new Vector3(408, -0.3f, 76);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, 103, 0));
        GOD.EnviromentContr.CurrentIsland = "SandIsland2";
        AfterTeleport();
    }
    public void TeleportToSun3()
    {
        GOD.Player.transform.position = new Vector3(544, -41f, 18);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -11, 0));
        GOD.EnviromentContr.CurrentIsland = "SandIsland3";
        AfterTeleport();
    }
    public void Test()
    {
        //MyIAP.instance.Buy("test");
    }
    public void TestAdd()
    {
        GOD.InventoryContr.AddToInventory("Wood",1);
    }
}
