﻿using UnityEngine;
using System.Collections.Generic;

namespace SurvivalClient.EditorHelper
{
    public class SetOnTerrain : MonoBehaviour
    {

        public class MaxPoin
        {
            public Vector3 normal;
            public Vector3 point;

            public MaxPoin(Vector3 n, Vector3 p)
            {
                this.normal = n;
                this.point = p;
            }
        }

        public Vector3 Position;
        public Vector3 Normal;

        [ContextMenu("SetOnTerrain")]
        public void Set()
        {
            List<MaxPoin> Points = new List<MaxPoin>();

            RaycastHit[] hits = Physics.RaycastAll(this.transform.position + this.transform.up * 10, -this.transform.up);
            foreach (RaycastHit hit in hits)
            {
                // if (hit.collider.tag == "Map" || hit.collider.tag == "StaticResource")
                if (hit.collider.tag == "Ground")
                 {
                    Points.Add(new MaxPoin(hit.normal, hit.point));
                }
            }

            if (Points.Count > 0)
            {
                float MaxY = Points[0].point.y;
                int select = 0;

                for (int i = 1; i < Points.Count; i++)
                {
                    if (Points[i].point.y > MaxY)
                    {
                        select = i;
                        MaxY = Points[i].point.y;
                    }
                }

                this.transform.up = Normal = Points[select].normal;
                this.transform.position = Position = Points[select].point;
                //  this.transform.position = Position = Points[select].point - new Vector3(0, 0.4f, 0);
            }
        }

        public void SetRotatioOnCamera()
        {
            //  Vector3 forward = Camera.main.transform.position - this.transform.position;

            // this.transform.forward = forward;
            // this.transform.up = Normal;
            //.SetLookRotation(forward, this.transform.up);
        }


    }
}
