﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSScript : MonoBehaviour {

    //ФПС
    [Header("ФПС:")]
    public Text FpsText;
    float deltaTime;

    void Update()
    {
        if (Time.deltaTime > 0)
        {
            deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
            FpsText.text = "" + Mathf.Round(1 / deltaTime);
        }
    }

}
