﻿using UnityEngine;
using System.Collections;

public class AdminIcon : MonoBehaviour {

    public GameManager GOD;

    public void AddThis()
    {
        if (GOD.InventoryContr.CanAdd(name, 1))
        {
            GOD.InventoryContr.AddToInventory(name, 1);
            GOD.PlayerInter.OpenInventory();
            GOD.PlayerInter.CloseInventoryImmediatly();
        }
        else
            GOD.PlayerInter.OpenCantOpen();

    }
}
