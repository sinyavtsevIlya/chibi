﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool EndOfLoad = false;
    public bool IsTutorial = false;
    public string OurPlayer = "Girl";
    [Header("Скрипты игры:")]
    public TimeGame TG;                                     // управление временем
    public SettingsController Settings;                     //настройки
    public EnviromentController EnviromentContr;           // управление окружающим миром
    public GameObject Player;                              // игрок
    public PlayerController PlayerContr;
    public PlayerAnimationController PlayerAnimation;
    public PlayerItems PI;
    public GameObject MainPlayer, PlayerStep, PlayerSphere, AttackSphere;
    public AttackSphere AttackS; //животные в ркадиусе атаки
    public GameObject Boy, Girl; //мальчик и девочка
    public Camera PlayerCam;                              // камера игрока
    public MoveCameraScript CameraScript;
    public ThirdPersonCharacter TPC;
    public ThirdPersonUserControl TPUC;
    public NearTarget NT;
    public Pathfinder PathF;
    public InventoryController InventoryContr;
    public PlayerInterface PlayerInter;
    public PullManager Pull;
    public BuildingScript BuildingS;
    public CraftController CraftContr;
    public DopEffects DopEff;
    public FishingWindowScript FishingWindow; //окно рыбалки
    public FishingPlace FishingP;
    public SaveLoad Save;
    public BafController BafContr;                  // бафы на игроке
    public DialogScript DialogS;
    public BuySlot BuySlotScript;                   // покупка доп инвентаря
    public NoMoney MoneyNo;                          //окно нехватки денег
    public ShopWindow ShopScript;
    public AudioController Audio;                //звуки и музыка
    public MapController Map;                       //карта
    public Admin AdminScript;
    public QuestController QuestContr;
    public OfferScript Offers;
    public TutorialController Tutor;
    public PlayerProgress Progress;      // лидерборды и ачивки

    [Header("Мир:")]
    public GameObject[] AllObjects;
    List<GameObject> HelpAllObjects = new List<GameObject>();
    public BridgeScript ToRain, ToSnow, ToSand;   // мосты
    public List<ObjectScript> RecoveryResourses = new List<ObjectScript>();  // массив восстанавливающихся ресурсов
    public List<ObjectScript> RecoveryObject = new List<ObjectScript>();  // массив восстанавливающихся объектов
    public List<Enemy> RecoveryEnemy = new List<Enemy>();  // массив восстанавливающихся врагов
    public GameObject IslandsParent;    // острова
    public Dictionary<string, IslandScript> AllIsland = new Dictionary<string, IslandScript>();
    public CapsuleCollider Lake; //костыль для озера
    public TornadoController TornadoContr;
    GameObject NewSkin;
    public static GameManager instance;

    public void CreateNewPlayer(int x)    //обновляем скин
    {
        GameObject CurrentSkin;
        if (OurPlayer == "Girl")
            CurrentSkin = PI.GetSkin(x);
        else
            CurrentSkin = PI.GetSkin(x);

        Player.SetActive(false);
        Destroy(Player.transform.GetChild(0).gameObject);
        PlayerSphere.transform.SetParent(null);
        AttackSphere.transform.SetParent(null);
        PlayerStep.transform.SetParent(null);
        string OldHand = "";
        if (PlayerContr.OnHand)
        {
            OldHand = PlayerAnimation.ItemOnHand.name;
            PlayerAnimation.NoItem();
        }
        NewSkin = Instantiate(CurrentSkin);
        NewSkin.transform.SetParent(Player.transform);
        NewSkin.transform.localPosition = new Vector3(0, 0, 0);
        NewSkin.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        NewSkin.name = "Char";
        PlayerSphere.transform.SetParent(Player.transform);
        AttackSphere.transform.SetParent(Player.transform);
        PlayerStep.transform.SetParent(Player.transform);

        PlayerContr = Player.GetComponent<PlayerController>();
        CameraScript.Player = Player;

        Player.SetActive(true);
        PlayerAnimation = NewSkin.GetComponent<PlayerAnimationController>();
        TPUC = Player.GetComponent<ThirdPersonUserControl>();
        TPC = Player.GetComponent<ThirdPersonCharacter>();
        TPC.Init();
        PlayerAnimation.Init(this);
        if (OurPlayer == "Girl")
        {
            PlayerContr.SetPlayerHairs(PlayerContr.CurrentGirlHairNumber);
            PlayerContr.SetPlayerHairColor(PlayerContr.CurrentGirlHairColorNumber);
            NewSkin.transform.GetChild(1).GetComponent<Renderer>().material.SetColor("_Color", PlayerContr.HairColors[PlayerContr.CurrentGirlHairColorNumber]);
        }
        else
        {
            PlayerContr.SetPlayerHairs(PlayerContr.CurrentBoyHairNumber);
            PlayerContr.SetPlayerHairColor(PlayerContr.CurrentBoyHairColorNumber);
            NewSkin.transform.GetChild(1).GetComponent<Renderer>().material.SetColor("_Color", PlayerContr.HairColors[PlayerContr.CurrentBoyHairColorNumber]);
        }
        if (OldHand!="")
        {
            PlayerAnimation.GetItem(OldHand); //оружие в руке 
        }
        NewSkin = null;
        Resources.UnloadUnusedAssets();
    }


    void Start()
    { 
         Init();
        instance = this;
    }

    void Init()
    {
        if(SceneManager.GetActiveScene().name == "Tutorial")
            IsTutorial = true;
        Localization.instance.SetLanguage(PlayerPrefs.GetString("ChibiLanguage"));

        Save.IsNewGame = PlayerPrefsX.GetBool("ChibiIsNewGame");
        Save.CurrentSlot = PlayerPrefs.GetInt("ChibiCurrentGame");
        if (PlayerPrefs.HasKey("ChibiMALE" + Save.CurrentSlot))
            OurPlayer = PlayerPrefs.GetString("ChibiMALE" + Save.CurrentSlot);
        else
            OurPlayer = "Boy";
        int x = 0;
        if (OurPlayer == "Girl")
        {
            if (PlayerPrefs.HasKey("ChibiCurrentGirlSkinNumber"))    // текущий скин девушки
                x = PlayerPrefs.GetInt("ChibiCurrentGirlSkinNumber");
            else
                x = 0;
        }
        else
        {
            if (PlayerPrefs.HasKey("ChibiCurrentBoySkinNumber"))    // текущий скин парня
                x = PlayerPrefs.GetInt("ChibiCurrentBoySkinNumber");
            else
                x = 0;
        }

        Player = MainPlayer;
        GameObject NewSkin = Instantiate(PI.GetSkin(x));
        NewSkin.transform.SetParent(Player.transform);
        NewSkin.transform.localPosition = new Vector3(0, 0, 0);
        NewSkin.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        NewSkin.name = "Char";
        PlayerSphere.transform.SetParent(Player.transform);
        AttackSphere.transform.SetParent(Player.transform);
        PlayerSphere.transform.localPosition = new Vector3(0, 0, 0);
        AttackSphere.transform.localPosition = new Vector3(0, 0, 0);
        PlayerStep.transform.SetParent(Player.transform);
        Player.SetActive(true);
        PlayerContr = Player.GetComponent<PlayerController>();
        CameraScript.Player = Player;
        PlayerAnimation = Player.transform.GetChild(0).GetComponent<PlayerAnimationController>();
        TPUC = Player.GetComponent<ThirdPersonUserControl>();
        TPC = Player.GetComponent<ThirdPersonCharacter>();

        NT = Player.transform.GetChild(1).GetComponent<NearTarget>();
        PI.Init();
        PathF.Init();
        PlayerContr.Init();
        PlayerInter.InitGame();
        if(Offers)
        Offers.Init();
        InventoryContr.Init();
        CraftContr.Init();
        BuildingS.Init();
        Progress.Init();

        for (int i = 0; i < IslandsParent.transform.childCount; i++)
            InitIsland(IslandsParent.transform.GetChild(i));
        AllObjects = new GameObject[HelpAllObjects.Count];
        for (int i = 0; i < AllObjects.Length; i++)
            AllObjects[i] = HelpAllObjects[i];
        HelpAllObjects = null;
        DopEff.Init();
        if (!IsTutorial)
            EnviromentContr.Init();
        else
        EnviromentContr.InitTutorial();
        DopEff.InitCrystall();
        MoneyNo.Init(this);
        PlayerAnimation.Init(this);
        DialogS.Init(this);
        Audio.Init();
        Map.Init();
        AdminScript.Init();

        if (!IsTutorial)
            InitGame();
        else
            InitTutorial();
    }

    void InitGame()
    {

        /* for (int i = 0; i < World.childCount; i++)             // инициализируем скрипты для объектов влияющих на карту
         {
             DynamicMapUpdate D = World.GetChild(i).GetComponent<DynamicMapUpdate>();
             Debug.Log(D);
             if (D != null)
                 D.Init();
         }*/
        Lake.gameObject.layer =2;
        Settings.Init(this);
        QuestContr.Init();
        Save.LoadGame();
        EnviromentContr.InitWeather();
        QuestContr.IsInit = true;
        for (int i = 0; i < IslandsParent.transform.childCount; i++)
            EnviromentContr.InitEnemies(IslandsParent.transform.GetChild(i)); //инициализация врагов
        CameraScript.TeleportCamera();
        TG.Init();
        ShopScript.Init();
        for (int i = 0; i < PlayerInter.InventoryOnGame.childCount; i++)
            PlayerInter.InventoryOnGame.GetChild(i).GetComponent<Slot>().Init();
        for (int i = 0; i < PlayerContr.EqupmentSlots.Length; i++)
            PlayerContr.EqupmentSlots[i].GetComponent<Slot>().Init();

        for (int i = 0; i < InventoryContr.Bag.transform.childCount; i++)
        {
            Slot s = InventoryContr.Bag.transform.GetChild(i).GetComponent<Slot>();
            if (s)
                s.Init();
        }
        for (int i = 0; i < InventoryContr.SlotInChest.Length; i++)
        {
            Slot s = InventoryContr.SlotInChest[i].GetComponent<Slot>();
            if (s)
                s.Init();
        }
        for (int i = 0; i < InventoryContr.ChestInventoryBag.childCount; i++)
        {
            Slot s = InventoryContr.ChestInventoryBag.GetChild(i).GetComponent<Slot>();
            if (s)
                s.Init();
        }
        PlayerInter.DeleteBtn.Init();
        PlayerInter.PlayerBtn.Init();

            FishingP.Init();
            DialogS.EnableDialogSphere(); // включаем или отключаем диалоги в зависимости от погоды
            DialogS.EnableDialogSphereBridge(); // включаем или отключаем диалоги в зависимости от того открыты ли мосты
            Audio.SetMusic();    //переключаем музыку
            EnviromentContr.SetTime();  // выставляем свет

        StartCoroutine(NextInit());
    }

    void InitTutorial()
    {
        CameraScript.TeleportCamera();
        Settings.Init(this);
        PlayerInter.FrostMaterial.SetFloat("_Cutoff", 1);
        if (PlayerPrefs.HasKey("ChibiBoyCurrentHair"))    // текущая прическа парня
            PlayerContr.CurrentBoyHairNumber = PlayerPrefs.GetInt("ChibiBoyCurrentHair");
        else
            PlayerContr.CurrentBoyHairNumber = 0;

        if (PlayerPrefs.HasKey("ChibiGirlCurrentHair"))    // текущая прическа девушки
            PlayerContr.CurrentGirlHairNumber = PlayerPrefs.GetInt("ChibiGirlCurrentHair");
        else
           PlayerContr.CurrentGirlHairNumber = 0;

        //ЦВЕТА______________________________________
        if (PlayerPrefs.HasKey("ChibiHairColor"))     //какие цвета куплены
           PlayerContr.Colors = PlayerPrefsX.GetBoolArray("ChibiHairColor");
        else
        {
            for (int i = 0; i > PlayerContr.Colors.Length; i++)
                PlayerContr.Colors[i] = false;
            PlayerContr.Colors[1] = true;
        }
        if (PlayerPrefs.HasKey("ChibiCurrentBoyHairColor"))    // текущий цвет
        {
            PlayerContr.CurrentBoyHairColorNumber = PlayerPrefs.GetInt("ChibiCurrentBoyHairColor");
            PlayerContr.CurrentGirlHairColorNumber = PlayerPrefs.GetInt("ChibiCurrentGirlHairColor");
        }
        else
        {
            PlayerContr.CurrentBoyHairColorNumber = 1;
            PlayerContr.CurrentGirlHairColorNumber = 13;
        }

        //задаем параметры
        if (OurPlayer == "Girl")
        {
            PlayerContr.SetPlayerHairs(PlayerContr.CurrentGirlHairNumber);
            PlayerContr.SetPlayerHairColor(PlayerContr.CurrentGirlHairColorNumber);
        }
        else
        {
           PlayerContr.SetPlayerHairs(PlayerContr.CurrentBoyHairNumber);
          PlayerContr.SetPlayerHairColor(PlayerContr.CurrentBoyHairColorNumber);
        }


        InventoryContr.SlotCount = InventoryContr.SlotStartCount;
        InventoryContr.SecondInit();

        Tutor.ShowTutorialStepTurn();

        TG.Init();
        ShopScript.Init();
        for (int i = 0; i < PlayerInter.InventoryOnGame.childCount; i++)
            PlayerInter.InventoryOnGame.GetChild(i).GetComponent<Slot>().Init();
        for (int i = 0; i < PlayerContr.EqupmentSlots.Length; i++)
            PlayerContr.EqupmentSlots[i].GetComponent<Slot>().Init();
        for (int i = 0; i < InventoryContr.Bag.transform.childCount; i++)
        {
            Slot s = InventoryContr.Bag.transform.GetChild(i).GetComponent<Slot>();
            if (s)
                s.Init();
        }
        for (int i = 0; i < InventoryContr.SlotInChest.Length; i++)
        {
            Slot s = InventoryContr.SlotInChest[i].GetComponent<Slot>();
            if (s)
                s.Init();
        }
        for (int i = 0; i < InventoryContr.ChestInventoryBag.childCount; i++)
        {
            Slot s = InventoryContr.ChestInventoryBag.GetChild(i).GetComponent<Slot>();
            if (s)
                s.Init();
        }
        PlayerInter.DeleteBtn.Init();
        PlayerInter.PlayerBtn.Init();
        Audio.SetPlayerStep();  //шаги
        StartCoroutine(NextInit());
    }

    IEnumerator NextInit()
    {
        yield return new WaitForFixedUpdate();
        EndOfLoad = true;
        EnviromentContr.ControlEnemys();
         if(Save.IsNewGame)
            Save.SaveGame(false);
        Progress.SetAchivments(3); //ачивка Начало пути
        if (!IsTutorial && BuildSettings.isFree)
            Offers.OpenOffersWindow("IconStartNabor");
        if (QuestContr.IsInit)
        {
            QuestContr.AddNewQuest(QuestContr.Firecamp);
            QuestContr.AddNewQuest(QuestContr.Craftsman);
            QuestContr.AddNewQuest(QuestContr.FirstBridge);
        }
    }

    //МИР____________________________________________________________________________
    public IslandScript ReturnIslandByName(string IslandName) //возвращаем остров по названию
    {
        IslandScript r = null;
        if (AllIsland.ContainsKey(IslandName))
            AllIsland.TryGetValue(IslandName, out r);
        return r;
    }
    void InitIsland(Transform Island) //инициализируем остров
    {
        IslandScript I = Island.GetComponent<IslandScript>();
       // I.IslandNumber = AllIsland.Count;
        AllIsland.Add(Island.name, I);
        DopEff.InitIsland(Island); //инициализация эффектов
        Audio.InitIsland(Island); //инициализация птиц

        Transform t = Island.Find("World");
        if(t!=null)
        {
            for(int i=0; i<t.childCount; i++)
            {
                Transform c = t.GetChild(i);
                if (!BuildSettings.isFree && c.name == "TV_Statue")
                    c.gameObject.SetActive(false);
                for (int x = 0; x < c.childCount; x++)
                    HelpAllObjects.Add(c.GetChild(x).gameObject);
            }
        }
        //EnviromentContr.InitEnemies(Island); //инициализация врагов
    }

    void FixedUpdate()
    {
        if (PlayerContr.IsAlive)
        {
            for (int i = 0; i < RecoveryResourses.Count; i++)                       // восстанавливаем ресурсы
            {
                if (RecoveryResourses[i].RecoveryTime <= TG.ToSecond())
                {
                    RecoveryResourses[i].RecoverResourses();
                    RecoveryResourses.Remove(RecoveryResourses[i]);
                }
            }
            for (int i = 0; i < RecoveryObject.Count; i++)                       // восстанавливаем объекты
            {
                if (RecoveryObject[i].RecoveryTime <= TG.ToSecond())
                {
                    Collider[] hitColliders = Physics.OverlapSphere(RecoveryObject[i].transform.position, 10f);   // проверям что бы рядом никого не было
                    int x = 0;
                    bool Recover = true;
                    while (x < hitColliders.Length)
                    {
                        if (hitColliders[x].CompareTag("Player") || hitColliders[x].CompareTag("GroundHouse") || hitColliders[x].CompareTag("Build") ||( !RecoveryObject[i].CompareTag("Heap") && hitColliders[x].CompareTag("Enemy")))
                            Recover = false;
                        x++;
                    }
                    if (Recover)
                    {
                        RecoveryObject[i].InitGOD(this);
                        RecoveryObject[i].RecoverObject();
                        RecoveryObject.Remove(RecoveryObject[i]);
                    }
                    else
                        RecoveryObject[i].RecoveryTime = TG.ToSecond() + PlayerInter.KD;
                }
            }
            for (int i = 0; i < RecoveryEnemy.Count; i++)   // восстанавливаем животных
            {
                if (RecoveryEnemy[i].RecoveryTime <= TG.ToSecond())
                {
                    Collider[] hitColliders = Physics.OverlapSphere(RecoveryEnemy[i].transform.position, 10f);   // проверям что бы рядом никого не было
                    int x = 0;
                    bool Recover = true;
                    while (x < hitColliders.Length)
                    {
                        if (hitColliders[x].CompareTag("Player") || hitColliders[x].CompareTag("GroundHouse") || hitColliders[x].CompareTag("Build") || hitColliders[x].CompareTag("Enemy"))
                            Recover = false;
                        x++;
                    }
                    if (Recover)
                    {
                        RecoveryEnemy[i].UpdateEnemy();
                        RecoveryEnemy.Remove(RecoveryEnemy[i]);
                    }
                    else
                        RecoveryEnemy[i].RecoveryTime = TG.ToSecond() + PlayerInter.KD;
                }

            }
        }
    }
    
    public void UpdateRecover()//когда игрок умер, вычитаем прожитые дни
    {
        for (int i = 0; i < RecoveryObject.Count; i++)                    
        {
            RecoveryObject[i].RecoveryTime -= TG.ToSecond();
        }
        for (int i = 0; i < RecoveryEnemy.Count; i++)  
        {
            RecoveryEnemy[i].RecoveryTime -= TG.ToSecond();
        }
    }
    //_________________________________
    public void GamePause()
    {
        if (Audio.PlayerSound.isPlaying)
            Audio.PlayerSound.Stop();
        PlayerAnimation.m_Animator.SetFloat("Speed", -1);
        Time.timeScale = 0;        
    }
    public void GamePlay()
    {
       // if (Audio.PlayerSound.isPlaying)
           // Audio.PlayerSound.Stop();
        //PlayerAnimation.m_Animator.SetFloat("Speed", -1);
        Time.timeScale = 1;
    }

    public void OnApplicationFocus(bool focusStatus)
    {
        MyAppodeal.OnApplicationFocus(focusStatus);
    }


}

