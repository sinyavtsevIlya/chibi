﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeSand : MonoBehaviour {

    public GameManager GOD;
    public string CurrentState = "Sun";
    public string OldState;

    public Material SandIsland;              // материал земли
    public Texture SandText, SnowText, GrassText;
    public Material SandObjects;  // материал объектов на острове
    public Material Ykazateli, LimitStone;                            // материал указателей и камней на границе и статуй
    public Transform Oazis1, Oazis2, Oazis3;
    public List<Transform> AllOazis = new List<Transform>();
    public GameObject Bush1, Bush2, Bush3, Trees1, Trees2, Trees3; // объекты  у оазисов
    public List<Transform> AllOazisPlants = new List<Transform>();
    public GameObject Sand1, Sand2, Sand3, UnderSand1, UnderSand2, UnderSand3;
    public Material River, Mountain;
    public GameObject WaterFall, IceWaterFall;

    [Header("Дерево головоломка:")]
    public Animator BigTreeAnimator;
    public GameObject BigTree, DopLimit;
    Transform BigTreeRoot, BigTreeStick, BigTreeLeafs;

    int ShowStep = 0;
    bool IsTextChange = false;

    public bool IsTreeDown = false;//срубленно ли дерево

    public void Init()
    {
        //Оазисы
        for (int i = 0; i < Oazis1.childCount; i++)
        {
            AllOazis.Add(Oazis1.GetChild(i));
        }
        for (int i = 0; i < Oazis2.childCount; i++)
        {
            AllOazis.Add(Oazis2.GetChild(i));
        }
        for (int i = 0; i < Oazis3.childCount; i++)
        {
            AllOazis.Add(Oazis3.GetChild(i));
        }

      
        //Растения оазисы
        for (int i = 0; i < Bush1.transform.childCount;i++)
            AllOazisPlants.Add(Bush1.transform.GetChild(i));
        for (int i = 0; i < Bush2.transform.childCount; i++)
            AllOazisPlants.Add(Bush2.transform.GetChild(i));
        for (int i = 0; i < Bush3.transform.childCount; i++)
            AllOazisPlants.Add(Bush3.transform.GetChild(i));
        for (int i = 0; i < Trees1.transform.childCount; i++)
            AllOazisPlants.Add(Trees1.transform.GetChild(i));
        for (int i = 0; i < Trees2.transform.childCount; i++)
            AllOazisPlants.Add(Trees2.transform.GetChild(i));
        for (int i = 0; i < Trees3.transform.childCount; i++)
            AllOazisPlants.Add(Trees3.transform.GetChild(i));

        ChangeSandState();

    }

    void PreInit()
    {
        BigTreeAnimator = BigTree.GetComponent<Animator>();
        BigTreeRoot = BigTree.transform.GetChild(0);
        BigTreeStick = BigTree.transform.GetChild(0).GetChild(0);
        BigTreeLeafs = BigTree.transform.GetChild(0).GetChild(0).GetChild(1);
    }
    public void ChangeSandState()
    {
        switch (CurrentState)
        {
            case "Snow":
                SandIsland.SetTexture("_Splat0", SnowText); // текстуры земли
                SandIsland.SetTexture("_DopSplat", SnowText);
                SandIsland.SetFloat("_Alpha", 1);

                SandObjects.SetFloat("_Alpha", 1);   // текстура растений и камней
                Ykazateli.SetFloat("_Alpha", 1);//текстура указателей
                LimitStone.SetFloat("_Alpha", 1);  //текстура камнец у границ
             //   TV_statue.SetFloat("_Alpha", 1);  //текстура статуй

                Sand1.SetActive(false);  // песок и под песком
                Sand2.SetActive(false);
                Sand3.SetActive(false);
                Sand1.transform.localPosition += new Vector3(0, -1.2f, 0);
                Sand2.transform.localPosition += new Vector3(0, -1.2f, 0);
                Sand3.transform.localPosition += new Vector3(0, -1.2f, 0);
                UnderSand1.SetActive(true);
                UnderSand2.SetActive(true);
                UnderSand3.SetActive(true);
                UnderSand1.transform.localPosition = new Vector3(0, 0f, 0);
                UnderSand2.transform.localPosition = new Vector3(0, 0f, 0);
                UnderSand3.transform.localPosition = new Vector3(0, 0f, 0);

                if(OldState == "Water")
                    IceWaterFall.SetActive(true);
                else
                    IceWaterFall.SetActive(false);  // водопады
                WaterFall.SetActive(false);
                for (int i = 0; i < GOD.DopEff.AllFoamScriptSand.Length; i++)  // выключаем брызги
                    GOD.DopEff.AllFoamScriptSand[i].ChangeFoam(0);
                River.SetFloat("_Blend", 1);

               // SetCurrentBigTree();// дерево головоломка

                for (int i = 0; i < AllOazis.Count; i++)  // оазисы
                {
                    AllOazis[i].transform.localScale = new Vector3(0, 1, 0);
                    AllOazis[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < AllOazisPlants.Count; i++)
                {
                    AllOazisPlants[i].transform.localScale = new Vector3(0, 0, 0);
                    AllOazisPlants[i].gameObject.SetActive(false);
                }
                break;
            case "Sun":
                SandIsland.SetTexture("_Splat0", SandText); // текстуры земли
                SandIsland.SetTexture("_DopSplat", SandText);
                SandIsland.SetFloat("_Alpha", 1);

                SandObjects.SetFloat("_Alpha", 0);   // текстура растений и камней
                Ykazateli.SetFloat("_Alpha", 0);//текстура указателей
                LimitStone.SetFloat("_Alpha", 0);  //текстура камнец у границ
               // TV_statue.SetFloat("_Alpha", 0);  //текстура статуй

                Sand1.SetActive(true);  // песок и под песком
                Sand2.SetActive(true);
                Sand3.SetActive(true);
                UnderSand1.SetActive(false);
                UnderSand2.SetActive(false);
                UnderSand3.SetActive(false);

                IceWaterFall.SetActive(false);  // водопады
                WaterFall.SetActive(false);
                for (int i = 0; i < GOD.DopEff.AllFoamScriptSand.Length; i++)  // выключаем брызги
                    GOD.DopEff.AllFoamScriptSand[i].ChangeFoam(0);
                River.SetFloat("_Blend", 1);

               // SetCurrentBigTree();// дерево головоломка

                for (int i = 0; i < AllOazis.Count; i++)  // оазисы
                {
                    AllOazis[i].transform.localScale = new Vector3(0, 1, 0);
                    AllOazis[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < AllOazisPlants.Count; i++)
                {
                    AllOazisPlants[i].transform.localScale = new Vector3(0, 0, 0);
                    AllOazisPlants[i].gameObject.SetActive(false);
                }
                break;
            case "Water":
                SandIsland.SetTexture("_Splat0", GrassText); // текстуры земли
                SandIsland.SetTexture("_DopSplat", GrassText);
                SandIsland.SetFloat("_Alpha", 1);

                SandObjects.SetFloat("_Alpha", 0);   // текстура растений и камней
                Ykazateli.SetFloat("_Alpha", 0);//текстура указателей
                LimitStone.SetFloat("_Alpha", 0);  //текстура камнец у границ
                //TV_statue.SetFloat("_Alpha", 0);  //текстура статуй

                Sand1.SetActive(true);  // песок и под песком
                Sand2.SetActive(true);
                Sand3.SetActive(true);
                UnderSand1.SetActive(false);
                UnderSand2.SetActive(false);
                UnderSand3.SetActive(false);

                IceWaterFall.SetActive(false);  // водопады
                WaterFall.SetActive(true);
                for (int i = 0; i < GOD.DopEff.AllFoamScriptSand.Length; i++)  // включаем брызги
                    GOD.DopEff.AllFoamScriptSand[i].ChangeFoam(3);
                River.SetFloat("_Blend", 1);
                River.SetColor("_Color", new Color(1, 1, 1, 1));


              //  SetCurrentBigTree();// дерево головоломка

                for (int i = 0; i < AllOazis.Count; i++)  // оазисы
                {
                    AllOazis[i].transform.localScale = new Vector3(1, 1, 1);
                    AllOazis[i].gameObject.SetActive(true);
                }
                for (int i = 0; i < AllOazisPlants.Count; i++)
                {
                    AllOazisPlants[i].transform.localScale = new Vector3(1, 1, 1);
                    AllOazisPlants[i].gameObject.SetActive(true);
                }
                break;
        }
    }

    public void LayBigTree(int x) // при включении игры кладем дерево, если оно лежало
    {
        PreInit();
        if (x > 0)
            IsTreeDown = true;
        SetCurrentBigTree();
    }

    public void SetDownTree()
    {
        BigTree.transform.localScale = new Vector3(1, 1, 1);
        BigTree.tag = "Untagged";
        BigTreeRoot.tag = "Untagged";
        BigTreeStick.tag = "Untagged";
        BigTreeLeafs.gameObject.SetActive(true);
        BigTree.SetActive(true);
        DopLimit.SetActive(false);
    }

    public void SetCurrentBigTree()
    {
        if (IsTreeDown)//если дерево срублено
        {
            SetDownTree();

            BigTreeAnimator.enabled = true;
            BigTreeAnimator.Play("Lay");
            BigTree.GetComponent<AudioSource>().Play();
        }
        else
        {
            if (CurrentState == "Water")
            {
                BigTree.transform.localScale = new Vector3(1, 1, 1);
                BigTree.tag = "BigTree";
                BigTreeRoot.tag = "BigTree";
                BigTreeStick.tag = "BigTree";
                BigTreeLeafs.gameObject.SetActive(true);
                BigTree.SetActive(true);
            }
            else
            {
                BigTree.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);  // дерево головоломка
                BigTree.tag = "Untagged";
                BigTreeRoot.tag = "Untagged";
                BigTreeStick.tag = "Untagged";
                BigTree.SetActive(true);
                BigTreeAnimator.enabled = false;
                BigTreeLeafs.gameObject.SetActive(false);
            }
        }
        }
    public void ChangeWeather(string state)
    {
        if (GOD.EnviromentContr.CanChange)
        {
            OldState = CurrentState;
            if ((state == "Snow") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;

                SandIsland.SetTexture("_Splat0", SandIsland.GetTexture("_DopSplat"));
                SandIsland.SetTexture("_DopSplat", SnowText);
                SandIsland.SetFloat("_Alpha", 1);
                StartCoroutine(ChangeTexture());
                StartCoroutine(ChangePlantTexture(1));
                ShowStep = 0;
                IsTextChange = false;
                StartCoroutine(Porydok());
                StartCoroutine(ChangeSandPosition(-1));

                ChangeSandPosition(-1);

                if (OldState == "Water")
                {
                    StartCoroutine(ShowOazis(-1));
                    StartCoroutine(OazisPlants(-1));
                    StartCoroutine(SetBigTree(-1));
                    StartCoroutine(ChangeRiverText(-1));
                    IceWaterFall.GetComponent<MountainScript>().BotBtn.SetActive(false);
                    IceWaterFall.GetComponent<MountainScript>().TopBtn.SetActive(false);
                    StartCoroutine(ChangeIceWaterFall(-1));
                    GOD.DopEff.ChangeFoam(false);//брызги
                }
                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);
            }
            else if ((state == "Sun") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;

                SandIsland.SetTexture("_Splat0", SandIsland.GetTexture("_DopSplat"));
                SandIsland.SetTexture("_DopSplat", SandText);
                SandIsland.SetFloat("_Alpha", 1);
                StartCoroutine(ChangeTexture());
                StartCoroutine(ChangePlantTexture(-1));
                if (OldState == "Water")
                {
                    StartCoroutine(ShowOazis(-1));
                    StartCoroutine(SetBigTree(-1));
                    StartCoroutine(OazisPlants(-1));
                    GOD.DopEff.ChangeFoam(false);//брызги
                }
                ShowStep = 0;
                IsTextChange = false;
                StartCoroutine(Porydok());

                River.SetFloat("_Blend", 1);
                if (OldState == "Snow")
                {
                    StartCoroutine(ChangeSandPosition(1));
                    StartCoroutine(ChangeIceWaterFall(1));
                }
                else
                    StartCoroutine(ShowRiver(-1));//убираем реку



                Sand1.SetActive(true);
                Sand2.SetActive(true);
                Sand3.SetActive(true);
                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);

            }
            else if ((state == "Water") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;

                SandIsland.SetTexture("_Splat0", SandIsland.GetTexture("_DopSplat"));
                SandIsland.SetTexture("_DopSplat", GrassText);
                SandIsland.SetFloat("_Alpha", 1);
                StartCoroutine(ChangeTexture());
                StartCoroutine(ChangePlantTexture(-1));
                StartCoroutine(ShowOazis(1));
                StartCoroutine(OazisPlants(1));
                StartCoroutine(SetBigTree(1));
                ShowStep = 0;
                IsTextChange = false;
                StartCoroutine(Porydok());
                if (IceWaterFall.activeSelf)
                {
                    StartCoroutine(ChangeSandPosition(1));
                    StartCoroutine(ChangeRiverText(1));
                    StartCoroutine(ChangeIceWaterFall(1));
                }
                GOD.DopEff.ChangeFoam(true);//брызги
                StartCoroutine(ShowRiver(1));//показываем реку
                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);
            }
            GOD.EnviromentContr.CurrentState = CurrentState;
        }
    }



    IEnumerator Porydok()
    {
        while (ShowStep < 1)
        {
            if (IsTextChange)
            {
                GOD.EnviromentContr.UpdateCanChange();
                if (CurrentState == "Water")
                {
                    BigTree.SetActive(true);
                    Sand1.SetActive(true);
                    Sand2.SetActive(true);
                    Sand3.SetActive(true);
                }
                ShowStep++;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    //ТЕКСТУРЫ_______________________________________________________________________________________________________________
    IEnumerator ChangeTexture()         // меняем текстуры
    {
        while (SandIsland.GetFloat("_Alpha") > 0f)
        {
            SandIsland.SetFloat("_Alpha", SandIsland.GetFloat("_Alpha") - 0.01f);
            if (SandIsland.GetFloat("_Alpha") <= 0)
                IsTextChange = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangePlantTexture(int x)
    {
        if (x < 0)
        {
            while (SandObjects.GetFloat("_Alpha") > 0f)
            {
                float z = SandObjects.GetFloat("_Alpha") - 0.01f;
                SandObjects.SetFloat("_Alpha", z);
                if (OldState == "Snow")
                {
                    Ykazateli.SetFloat("_Alpha", z);
                    LimitStone.SetFloat("_Alpha", z);
                  //  TV_statue.SetFloat("_Alpha", z);
                }
                yield return new WaitForSeconds(0.1f);
            }
          //  if (SandObjects.GetFloat("_Alpha") <= 0)
              //  IsPlantTextChange = true;
        }
        else
        {
            while (SandObjects.GetFloat("_Alpha") < 0.5f)
            {
                float z = SandObjects.GetFloat("_Alpha") + 0.01f;
                SandObjects.SetFloat("_Alpha", z);
                if (CurrentState == "Snow")
                {
                    Ykazateli.SetFloat("_Alpha", z);
                    LimitStone.SetFloat("_Alpha", z);
                   // TV_statue.SetFloat("_Alpha", z);
                }
                yield return new WaitForSeconds(0.1f);
            }
          //  if (SandObjects.GetFloat("_Alpha") >= 0.5f)
               // IsPlantTextChange = true;
        }
    }


    //ОАЗИСЫ______________________________________________________________________________________________________________________
    IEnumerator ShowOazis(int x)         // постепенно убираем\показываем лужи
    {
    
        if (AllOazis.Count > 0)
        {
            if (x < 0)
            {
                while (AllOazis[0].localScale.x > 0)
                {
                    for (int i = 0; i < AllOazis.Count; i++)
                    {
                        AllOazis[i].localScale -= new Vector3(0.01f, 0, 0.01f);
                        if (AllOazis[i].localScale.x < 0)
                        {
                            AllOazis[i].localScale = new Vector3(0, 1, 0);
                            GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                            for (int y = 0; y < AllOazis.Count; y++)
                            {
                                AllOazis[y].gameObject.SetActive(false);

                            }
                        }
                    }
                    yield return new WaitForSeconds(0.1f);
                }
              //  if (AllOazis[0].localScale.x <= 0)
                  //  IsPuddleStop = true;
            }
            else
            {
                for (int i = 0; i < AllOazis.Count; i++)
                {
                    AllOazis[i].gameObject.SetActive(true);

                }
                while (AllOazis[0].localScale.x < 1f)
                {
                    for (int i = 0; i < AllOazis.Count; i++)
                    {
                        AllOazis[i].localScale += new Vector3(0.01f, 0, 0.01f);
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                if (AllOazis[0].localScale.x >= 1f)
                    WaterFall.SetActive(true);
            }
        }
       // else
           // IsPuddleStop = true;
    }

    public IEnumerator OazisPlants(int x)   // выращиваем убираем оазиcные растения
    {
        if (x < 0)
        {
            while (AllOazisPlants[0].localScale.x > 0)
            {
                for (int i = 0; i < AllOazisPlants.Count; i++)
                {
                    AllOazisPlants[i].localScale -= new Vector3(0.01f, 0.01f, 0.01f);
                    if (AllOazisPlants[i].localScale.x < 0)
                    {
                        AllOazisPlants[i].localScale = new Vector3(0, 0, 0);
                        AllOazisPlants[i].gameObject.SetActive(false);
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            for (int i = 0; i < AllOazisPlants.Count; i++)
                AllOazisPlants[i].gameObject.SetActive(true);

            while (AllOazisPlants[0].localScale.x < 1)
            {
                for (int i = 0; i < AllOazisPlants.Count; i++)
                {
                    AllOazisPlants[i].localScale += new Vector3(0.01f, 0.01f, 0.01f);
                    if (AllOazisPlants[i].localScale.x > 1)
                        AllOazisPlants[i].localScale = new Vector3(1, 1, 1);
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }



    public IEnumerator SetBigTree(int x)   // выращиваем убираем большое дерево
    {
        if (!IsTreeDown)//если дерево еще не было срублено
        {
            BigTree.tag = "Untagged";
            BigTreeRoot.tag = "Untagged";
           BigTreeStick.tag = "Untagged";

            if (x < 0)// убираем
            {
                while (BigTree.transform.localScale.x > 0.5f)
                {
                    BigTree.transform.localScale -= new Vector3(0.04f, 0.04f, 0.04f);
                    if (BigTree.transform.localScale.x <= 0.5f)
                    {
                        BigTree.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        BigTreeAnimator.enabled = false;
                        BigTreeLeafs.gameObject.SetActive(false);
                    }
                    yield return new WaitForSeconds(0.1f);
                }
            }
            else// выращиваем
            {
                while (BigTree.transform.localScale.x < 1f)
                {
                    BigTree.transform.localScale += new Vector3(0.02f, 0.02f, 0.02f);
                    if (BigTree.transform.localScale.x > 1)
                    {
                        BigTree.transform.localScale = new Vector3(1, 1, 1);
                        DopLimit.SetActive(false);
                        BigTree.tag = "BigTree";
                        BigTreeRoot.tag = "BigTree";
                        BigTreeStick.tag = "BigTree";
                        BigTreeLeafs.gameObject.SetActive(true);
                    }
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
    }


    //ПЕСОК___________________________________________________________________________________________________________
    IEnumerator ChangeSandPosition(int x)        // опускаем поднимаем песок
    {
        if (x < 0)
        {
            UnderSand1.SetActive(true);
            UnderSand2.SetActive(true);
            UnderSand3.SetActive(true);
            while (Sand1.transform.localPosition.y > -1)
            {
                Sand1.transform.localPosition -= new Vector3(0, 0.04f, 0);
                Sand2.transform.localPosition -= new Vector3(0, 0.04f, 0);
                Sand3.transform.localPosition -= new Vector3(0, 0.04f, 0);
                if (UnderSand1.transform.localPosition.y < 0)
                {
                    UnderSand1.transform.localPosition += new Vector3(0, 0.04f, 0);
                    UnderSand2.transform.localPosition += new Vector3(0, 0.04f, 0);
                    UnderSand3.transform.localPosition += new Vector3(0, 0.04f, 0);
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (Sand1.transform.localPosition.y <= -1)
            {
               Sand1.SetActive(false);
                Sand2.SetActive(false);
                Sand3.SetActive(false);
            }

        }
        else
        {
            Sand1.SetActive(true);
            while (Sand1.transform.localPosition.y < 0)
            {
                Sand1.transform.localPosition += new Vector3(0, 0.04f, 0);
                Sand2.transform.localPosition += new Vector3(0, 0.04f, 0);
                Sand3.transform.localPosition += new Vector3(0, 0.04f, 0);
                if (UnderSand1.transform.localPosition.y > -1)
                {
                    UnderSand1.transform.localPosition -= new Vector3(0, 0.04f, 0);
                    UnderSand2.transform.localPosition -= new Vector3(0, 0.04f, 0);
                    UnderSand3.transform.localPosition -= new Vector3(0, 0.04f, 0);
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (Sand1.transform.localPosition.y >= 0)
            {
                UnderSand1.SetActive(false);
                UnderSand2.SetActive(false);
                UnderSand3.SetActive(false);
            }
        }
    }

    //ЗАМОРАЖИВАЕМ РЕКУ_____________________________________________________________________________

    IEnumerator ShowRiver(int x)        //постепенно включаем и выключаем воду
    {
        if (x > 0)
        {
            WaterFall.SetActive(true);
            River.SetColor("_Color", new Color(1, 1, 1, 0));
            while (River.GetColor("_Color").a<1)
            {
                River.SetColor("_Color", River.GetColor("_Color") + new Color(0, 0, 0, 0.05f));
                yield return new WaitForSeconds(0.1f);
            }
            if (River.GetColor("_Color").a >= 1)
            {
                River.SetColor("_Color", new Color(1, 1, 1, 1));
            }
        }
        else
        {
            while (River.GetColor("_Color").a >0)
            {
                River.SetColor("_Color", River.GetColor("_Color") - new Color(0, 0, 0, 0.05f));
                if (River.GetColor("_Color").a <= 0)
                {
                    WaterFall.SetActive(false);
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    IEnumerator ChangeRiverText(int x)        // меняем воду на лед и обратно
    {
        if (x > 0)
        {
            while (River.GetFloat("_Blend") < 1f)
            {
                River.SetFloat("_Blend", River.GetFloat("_Blend") + 0.01f);
                yield return new WaitForSeconds(0.1f);
            }
            if (River.GetFloat("_Blend") >= 1)
            {

            }
        }
        else
        {
            while (River.GetFloat("_Blend") > 0f)
            {
                River.SetFloat("_Blend", River.GetFloat("_Blend") - 0.01f);
                if (River.GetFloat("_Blend") <= 0)
                {
                    WaterFall.SetActive(false);
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    IEnumerator ChangeIceWaterFall(int x)  // появление исчезание ледяного водопада
    {
        MountainScript MScript = IceWaterFall.GetComponent<MountainScript>();
        if (x > 0)
        {
            MScript.SetMountain(false);
            Color c = Mountain.GetColor("_Color");
            Mountain.SetColor("_Color", new Color(c.r, c.g, c.b, 1f));
            while (Mountain.GetColor("_Color").a > 0f)
            {
                Mountain.SetColor("_Color", Mountain.GetColor("_Color") - new Color(0, 0, 0, 0.02f));
                yield return new WaitForSeconds(0.1f);
            }
            if (Mountain.GetColor("_Color").a <= 0)
                IceWaterFall.SetActive(false);
        }
        else
        {
            MScript.SetMountain(false);
            Color c = Mountain.GetColor("_Color");
            Mountain.SetColor("_Color", new Color(c.r, c.g, c.b, 0f));
            c = Mountain.GetColor("_Color");
            IceWaterFall.SetActive(true);
            while (Mountain.GetColor("_Color").a < 1f)
            {
                Mountain.SetColor("_Color", Mountain.GetColor("_Color") + new Color(0, 0, 0, 0.01f));
                yield return new WaitForSeconds(0.1f);
            }
            if(Mountain.GetColor("_Color").a>=1)
            {
                MScript.SetMountain(true);
            }

        }
    }
}
