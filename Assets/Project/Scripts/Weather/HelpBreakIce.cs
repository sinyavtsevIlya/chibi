﻿using UnityEngine;
using System.Collections;

public class HelpBreakIce : MonoBehaviour {
    public GameManager GOD;

	public void Help()
    {
        GetComponent<Animator>().enabled = false;
        GetComponent<Animator>().enabled = true;
        GetComponent<Animator>().Play("IceStay");
        StartCoroutine(GOD.EnviromentContr.CSnow.ChangeIceBreakTexture());
    }
}
