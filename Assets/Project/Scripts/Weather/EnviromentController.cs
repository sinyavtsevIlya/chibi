﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class EnviromentController : MonoBehaviour {

    public GameManager GOD;
    public ChangeWater CWater;
    public ChangeSand CSand;
    public ChangeSnow CSnow;
    public ChangeTutorial CTut;

    public string CurrentIsland = "Green";
    public int CurrentIslandNumber = 0;
    public IslandScript CurrentIslandScript = null;
    public string OldIsland = "";
    public string CurrentState = "Null";  // Null, Sun, Snow, Wetly
    public bool CanChange = true;

    public GameObject[] EnemiesPrefabs;
    public List<Enemy> AllEnemies = new List<Enemy>(); //все враги
    public List<GameObject> IslandEnemies=new List<GameObject>();

    public Light Sun;

    public Material SkyBox;
    public Texture Front1, Back1, Left1, Right1, Up1, Down1, Front2, Back2, Left2, Right2, Up2, Down2, Front3, Back3, Left3, Right3, Up3, Down3;

    ParticleSystem[] AllFogs;
    List<ParticleSystem> AllYellowSparks = new List<ParticleSystem>();
    List<ParticleSystem> AllBlueSparks = new List<ParticleSystem>();
   public  List<ParticleSystem> AllLightShaft = new List<ParticleSystem>();
    List<TornadoScript> AllTornado = new List<TornadoScript>();
    public List<Transform> AllSnowSparkls = new List<Transform>();

    public string LastShowIsland = "";

    //Фоны карты
    [Header("Фоны карты:")]
    public Image MapBack1;
    public Image MapBack2;
    public Sprite DayMap, EveningMap, NightMap;

    public void InitEnemies(Transform Island)
    {
        Transform t = Island.Find("Enemies");
        if(t!=null)
        {
            IslandEnemies.Add(t.gameObject);
            for(int i=0; i<t.childCount; i++)
            {
                Transform PositionsParent = t.GetChild(i);
                GameObject g = GetEnemyPrefab(PositionsParent.name);
                if(g!=null)
                {
                    for (int y = 0; y <PositionsParent.childCount; y++)
                    {
                        Collider[] hitColliders = Physics.OverlapSphere(PositionsParent.GetChild(y).position, 10f);   // проверям что бы рядом никого не было
                        int x = 0;
                        bool Recover = true;
                        while (x < hitColliders.Length)
                        {
                            if (hitColliders[x].tag == "Player" || hitColliders[x].tag == "GroundHouse" || hitColliders[x].tag == "Build")
                                Recover = false;
                            x++;
                        }
                        GameObject newg = Instantiate(g, PositionsParent.GetChild(y).position, g.transform.rotation) as GameObject;
                        newg.name = g.name;
                        newg.transform.SetParent(PositionsParent.GetChild(y));
                        Enemy e = newg.GetComponent<Enemy>();
                        e.EnemyBase = PositionsParent.GetChild(y);
                        e.AddEffects();
                        e.GOD = GOD;
                        e.Init();
                        if (Recover)
                            AllEnemies.Add(e);
                        else
                        {
                            e.DeadState();
                            e.Dead();
                        }
                        
                    }
                }
            }
        }
    }
    GameObject GetEnemyPrefab(string eName)
    {
        for(int i=0; i<EnemiesPrefabs.Length; i++)
        {
            if (eName == EnemiesPrefabs[i].name)
                return EnemiesPrefabs[i];
        }
        return null;
    }
    public void Init()
    {
        AllFogs = new ParticleSystem[GOD.DopEff.AllFog.Count];    //заполняем массив туманов
        for (int i = 0; i < GOD.DopEff.AllFog.Count; i++)
            AllFogs[i] = GOD.DopEff.AllFog[i].GetComponent<ParticleSystem>();

        int num = 1; //счетчик для островов с торнадо
        for (int i = 0; i < GOD.DopEff.AllDayEffects.Count; i++)
        {
            GameObject t = GOD.DopEff.AllDayEffects[i];
            switch (t.name)
            {
                case "DaySparkl": // заполняем массив желтых светлячков
                    for (int x = 0; x < t.transform.childCount; x++)
                        AllYellowSparks.Add(t.transform.GetChild(x).GetComponent<ParticleSystem>());
                    break;
                case "NightSparkl": // заполняем массив синих светлячков
                    for (int x = 0; x < t.transform.childCount; x++)
                        AllBlueSparks.Add(t.transform.GetChild(x).GetComponent<ParticleSystem>());
                    break;
                case "LightShaft":// заполняем массив лучей
                    for (int x = 0; x < t.transform.childCount; x++)
                        AllLightShaft.Add(t.transform.GetChild(x).GetComponent<ParticleSystem>());
                    break;
                case "Tornado":// заполняем массив торнадо
                    for (int x = 0; x < t.transform.childCount; x++)
                    {
                        TornadoScript ts = t.transform.GetChild(x).GetComponent<TornadoScript>();
                        AllTornado.Add(ts);
                        ts.Init(GOD,num);
                    }
                    num++;
                    break;
                case "SnowSparkl":
                    AllSnowSparkls.Add(t.transform);
                    break;
            }
        }
    }

    public void InitWeather()
    {
        CWater.Init();
        CSand.Init();
        CSnow.Init();
    }
    public void InitTutorial()
    {
        SkyBox.SetTexture("_FrontTex", Front1);
        SkyBox.SetTexture("_BackTex", Back1);
        SkyBox.SetTexture("_LeftTex", Left1);
        SkyBox.SetTexture("_RightTex", Right1);
        SkyBox.SetTexture("_UpTex", Up1);
        SkyBox.SetTexture("_DownTex", Down1);
        SkyBox.SetFloat("_Blend", 0);
    }
    public void UpdateCanChange()
    {
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.StopWeather);
        GOD.Audio.WeatherSound.enabled = false;
        CanChange = true;
        GOD.PlayerInter.UpdateWeatherButton();
        GOD.PlayerAnimation.StopChangeWeather();
       
    }
    public void PlayerOnIsland(IslandScript Island)
    {
       
        //if (CurrentIsland != islandName)
        if (!GOD.IsTutorial)
        {
            string islandName = CurrentIsland;
            if (Island)
            {
                islandName = Island.transform.name;
                CurrentIslandScript = Island;
                CurrentIslandNumber = Island.IslandNumber;
            }
            CurrentIsland = islandName;
            if (LastShowIsland != CurrentIsland)
            {
                GOD.PlayerInter.ShowIslandName(Island.transform.name);
                LastShowIsland = CurrentIsland;
            }
            GOD.Map.SetOpenIslands();
            GOD.Audio.SetPlayerStep();    //переключаем звуки шагов
            GOD.Audio.ChangeMusic();    //переключаем музыку
            GOD.Audio.GetRiver();
            GOD.Audio.GetWaterFall();
            GOD.DopEff.SetCurrentEffects();
            if (islandName == "GreenIsland")             // зеленый остров
            {
                GOD.DopEff.GetRain(false);
                GOD.DopEff.RainCircle.transform.SetParent(GOD.DopEff.EffectsParent);
                GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                CurrentState = "Null";
            }
            else if (islandName.Contains("WaterIsland")) // водные острова
            {
                if (CWater.CurrentState == "Water")
                {
                    GOD.DopEff.GetRain(true);
                    GOD.DopEff.RainCircle.transform.SetParent(null);
                    GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(true);
                }
                else if (CWater.CurrentState == "Snow")
                {
                    GOD.DopEff.Snow.transform.SetParent(null);
                    GOD.DopEff.GetRain(false);
                    GOD.DopEff.RainCircle.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                else if (CWater.CurrentState == "Sun")
                {
                    GOD.DopEff.GetRain(false);
                    GOD.DopEff.RainCircle.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                CurrentState = CWater.CurrentState;
                if (GOD.QuestContr.IsInit)
                {
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.RainBiom);
                }
            }
            else if (islandName.Contains("WinterIsland")) // ледяные острова
            {
                GOD.DopEff.RainCircle.transform.SetParent(GOD.DopEff.EffectsParent);
                if (CSnow.CurrentState == "Water")
                {
                    GOD.DopEff.GetRain(true);
                    GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                else if (CSnow.CurrentState == "Snow")
                {
                    GOD.DopEff.Snow.transform.SetParent(null);
                    GOD.DopEff.GetRain(false);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                else if (CSnow.CurrentState == "Sun")
                {
                    GOD.DopEff.GetRain(false);
                    GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                CurrentState = CSnow.CurrentState;
                if (GOD.QuestContr.IsInit)
                {
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.SnowBiom);
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.Puzzle);
                }
            }
            else if (islandName.Contains("SandIsland"))
            {
                GOD.DopEff.RainCircle.transform.SetParent(GOD.DopEff.EffectsParent);
                if (CSand.CurrentState == "Water")
                {
                    GOD.DopEff.GetRain(true);
                    GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(true);
                }
                else if (CSand.CurrentState == "Snow")
                {
                    GOD.DopEff.Snow.transform.SetParent(null);
                    GOD.DopEff.GetRain(false);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                else if (CSand.CurrentState == "Sun")
                {
                    GOD.DopEff.GetRain(false);
                    GOD.DopEff.Snow.transform.SetParent(GOD.DopEff.EffectsParent);
                    GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                }
                CurrentState = CSand.CurrentState;
                if (GOD.QuestContr.IsInit)
                {
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.SandBiom);
                }
            }

            ControlEnemys();
            GOD.DialogS.EnableDialogSphere();
            if (GOD.QuestContr.IsInit)
            {
                if(GOD.QuestContr.Find.Contains(CurrentIsland))
                GOD.QuestContr.CheckAllQuests("find",CurrentIsland,1);
                if (GOD.QuestContr.Do.Contains(CurrentState))
                    GOD.QuestContr.CheckAllQuests("do", CurrentState, 1);
            }

        }

    }
    public void ControlEnemys()   // всегда включены только враги текущего острова
    {
        if (GOD.EndOfLoad && CurrentIsland != OldIsland)
        {
            OldIsland = CurrentIsland;
            for (int i = 0; i < IslandEnemies.Count; i++)
                IslandEnemies[i].SetActive(false);
            if(IslandEnemies.Count> GOD.EnviromentContr.CurrentIslandNumber)
            IslandEnemies[GOD.EnviromentContr.CurrentIslandNumber].SetActive(true);
        }
    }

    public void ChangeWeather(string state)          // меняем погоду
    {
        if (GOD.IsTutorial)
        {
            CTut.ChangeWeather();
        }
        else
        {
            if (CurrentIsland.Contains("WaterIsland"))
                CWater.ChangeWeather(state);
            else if (CurrentIsland.Contains("SandIsland"))
                CSand.ChangeWeather(state);
            else if (CurrentIsland.Contains("WinterIsland"))
                CSnow.ChangeWeather(state);
        }
        GOD.Audio.SetPlayerStep(); //шаги
    }


    public void SetTime()       // устанавливаем день/вечер/ночь сразу
    {
        if(GOD.TG.IHour >=6 && GOD.TG.IHour <20)    //день
        {
            MapBack1.sprite = DayMap;
            Sun.color = new Color(0.98f, 0.83f, 0.4f);
            RenderSettings.ambientSkyColor = new Color(1, 1, 1);
            Sun.transform.rotation =  Quaternion.Euler(new Vector3(70, 0, 0));
            for (int i = 0; i < AllFogs.Length; i++)  // выключаем туман
            {
                var c = AllFogs[i].emission;
                c.rateOverTime = 0f;
            }
            for (int i = 0; i < AllYellowSparks.Count; i++)  // включаем желтых светляков
            {
                var c = AllYellowSparks[i].emission;
                c.rateOverTime = 10f;
            }
            for (int i = 0; i < AllBlueSparks.Count; i++)  // выключаем синих светляков
            {
                var c = AllBlueSparks[i].emission;
                c.rateOverTime = 0f;
            }
            for (int i = 0; i < AllLightShaft.Count; i++)  // лучи дневные
            {
                var mainModul = AllLightShaft[i].main;
                mainModul.startColor = new Color(1,0.87f,0.18f);
                var col = AllLightShaft[i].colorOverLifetime;
                Gradient grad = new Gradient();
                grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.black, 0), new GradientColorKey(new Color(0.93f, 0.63f, 0.07f), 0.5f), new GradientColorKey(Color.black, 1) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 0.0f) } );
                col.color = grad;
            }
            //скайбокс
            SkyBox.SetTexture("_FrontTex", Front1);
            SkyBox.SetTexture("_BackTex", Back1);
            SkyBox.SetTexture("_LeftTex", Left1);
            SkyBox.SetTexture("_RightTex", Right1);
            SkyBox.SetTexture("_UpTex", Up1);
            SkyBox.SetTexture("_DownTex", Down1);

            SkyBox.SetTexture("_FrontTex1", Front2);
            SkyBox.SetTexture("_BackTex1", Back2);
            SkyBox.SetTexture("_LeftTex1", Left2);
            SkyBox.SetTexture("_RightTex1", Right2);
            SkyBox.SetTexture("_UpTex1", Up2);
            SkyBox.SetTexture("_DownTex1", Down2);

            SkyBox.SetFloat("_Blend", 0);

            for (int i = 0; i < AllEnemies.Count; i++)      //делаем животных агрессивными
            {
               AllEnemies[i].IsAgressive = false;
            }
        }
        else if(GOD.TG.IHour>=20 || GOD.TG.IHour == 0)
        {
            MapBack1.sprite = EveningMap;
            Sun.color = new Color(0.98f, 0.83f, 0.4f);
            RenderSettings.ambientSkyColor = new Color(0.99f, 0.66f, 0.66f);
            Sun.transform.rotation = Quaternion.Euler(new Vector3(30, 0, 0));
            for (int i = 0; i < AllFogs.Length; i++)  // выключаем туман
            {
                var c = AllFogs[i].emission;
                c.rateOverTime = 0f;
            }
            for (int i = 0; i < AllYellowSparks.Count; i++)  // включаем желтых светляков
            {
                var c = AllYellowSparks[i].emission;
                c.rateOverTime = 10f;
            }
            for (int i = 0; i < AllBlueSparks.Count; i++)  // выключаем синих светляков
            {
                var c = AllBlueSparks[i].emission;
                c.rateOverTime = 0f;
            }
            for (int i = 0; i < AllLightShaft.Count; i++)  // лучи дневные
            {
                var mainModul = AllLightShaft[i].main;
                mainModul.startColor = new Color(1, 0.87f, 0.18f);
                var col = AllLightShaft[i].colorOverLifetime;
                Gradient grad = new Gradient();
                grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.black, 0), new GradientColorKey(new Color(0.93f, 0.63f, 0.07f), 0.5f), new GradientColorKey(Color.black, 1) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 0.0f) });
                col.color = grad;
            }
            //скайбокс
            SkyBox.SetTexture("_FrontTex", Front2);
            SkyBox.SetTexture("_BackTex", Back2);
            SkyBox.SetTexture("_LeftTex", Left2);
            SkyBox.SetTexture("_RightTex", Right2);
            SkyBox.SetTexture("_UpTex", Up2);
            SkyBox.SetTexture("_DownTex", Down2);

            SkyBox.SetTexture("_FrontTex1", Front3);
            SkyBox.SetTexture("_BackTex1", Back3);
            SkyBox.SetTexture("_LeftTex1", Left3);
            SkyBox.SetTexture("_RightTex1", Right3);
            SkyBox.SetTexture("_UpTex1", Up3);
            SkyBox.SetTexture("_DownTex1", Down3);

            SkyBox.SetFloat("_Blend", 0);
        }
        else if(GOD.TG.IHour>=1 && GOD.TG.IHour<=5)
        {
            MapBack1.sprite = NightMap;
            Sun.color = new Color(0.63f, 0.7f, 0.98f);
            RenderSettings.ambientSkyColor = new Color(0, 0.13f, 0.52f);
            Sun.transform.rotation = Quaternion.Euler(new Vector3(30, 0, 0));
            for (int i = 0; i < AllFogs.Length; i++)  // выключаем туман
            {
                var c = AllFogs[i].emission;
                c.rateOverTime = 10f;
            }
            for (int i = 0; i < AllYellowSparks.Count; i++)  // включаем желтых светляков
            {
                var c = AllYellowSparks[i].emission;
                c.rateOverTime = 0f;
            }
            for (int i = 0; i < AllBlueSparks.Count; i++)  // выключаем синих светляков
            {
                var c = AllBlueSparks[i].emission;
                c.rateOverTime = 10f;
            }
            for (int i = 0; i < AllLightShaft.Count; i++)  // лучи ночные
            {
                var mainModul = AllLightShaft[i].main;
                mainModul.startColor = new Color(1, 1, 1);
                var col = AllLightShaft[i].colorOverLifetime;
                Gradient grad = new Gradient();
                grad.SetKeys(new GradientColorKey[] { new GradientColorKey(new Color(0, 0, 0), 0.0f), new GradientColorKey(new Color(0.39f, 0.86f, 0.98f), 0.5f),  new GradientColorKey(new Color(0, 0, 0), 1f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 0f) });
                col.color = grad;
            }

            //скайбокс
            SkyBox.SetTexture("_FrontTex", Front3);
            SkyBox.SetTexture("_BackTex", Back3);
            SkyBox.SetTexture("_LeftTex", Left3);
            SkyBox.SetTexture("_RightTex", Right3);
            SkyBox.SetTexture("_UpTex", Up3);
            SkyBox.SetTexture("_DownTex", Down3);

            SkyBox.SetTexture("_FrontTex1", Front1);
            SkyBox.SetTexture("_BackTex1", Back1);
            SkyBox.SetTexture("_LeftTex1", Left1);
            SkyBox.SetTexture("_RightTex1", Right1);
            SkyBox.SetTexture("_UpTex1", Up1);
            SkyBox.SetTexture("_DownTex1", Down1);

            SkyBox.SetFloat("_Blend", 0);

            for (int i = 0; i < AllEnemies.Count; i++)      //делаем животных агрессивными
            {
                AllEnemies[i].IsAgressive = true;
            }
            GOD.PlayerContr.IsFrostMaterial = false;
            GOD.PlayerContr.IsFrostDialog = false;
            GOD.PlayerContr.Frost = 0;
        }
    }


    public void ChangeTime()   // устанавливаем день/вечер/ночь постепенно
    {
        if (GOD.TG.IHour >= 6 && GOD.TG.IHour < 20)
        {
            GOD.TG.ShowNewDay();
            StartCoroutine(ToDay());
            for (int i = 0; i < AllEnemies.Count; i++)      //делаем животных  не агрессивными
            {
                AllEnemies[i].IsAgressive = false;
            }
        }
        else if (GOD.TG.IHour >= 20 || GOD.TG.IHour == 0)
        {
            StartCoroutine(ToEvening());
        }
        else if (GOD.TG.IHour >= 1 && GOD.TG.IHour <= 5)
        {
            StartCoroutine(ToNight());
            for(int i =0; i< AllEnemies.Count; i++)      //делаем животных агрессивными
            {
                AllEnemies[i].IsAgressive = true;
            }
        }
    }

    IEnumerator ToEvening()
    {
        //скайбокс
        SkyBox.SetTexture("_FrontTex", Front1);
        SkyBox.SetTexture("_BackTex", Back1);
        SkyBox.SetTexture("_LeftTex", Left1);
        SkyBox.SetTexture("_RightTex", Right1);
        SkyBox.SetTexture("_UpTex", Up1);
        SkyBox.SetTexture("_DownTex", Down1);

        SkyBox.SetTexture("_FrontTex1", Front2);
        SkyBox.SetTexture("_BackTex1", Back2);
        SkyBox.SetTexture("_LeftTex1", Left2);
        SkyBox.SetTexture("_RightTex1", Right2);
        SkyBox.SetTexture("_UpTex1", Up2);
        SkyBox.SetTexture("_DownTex1", Down2);

        SkyBox.SetFloat("_Blend", 0);

        MapBack2.sprite = EveningMap;
        MapBack2.color = new Color(1, 1, 1, 1);

        float x = 0;
        while (RenderSettings.ambientSkyColor.g > 0.66f)
        {
            RenderSettings.ambientSkyColor -= new Color(0, 0.01f, 0.01f);
            if (x < 1)
                x += 0.07f;
            if (x > 1)
                x = 1;
            SkyBox.SetFloat("_Blend", x);
            if(MapBack1.color.a>0)
            {
                MapBack1.color -= new Color(0, 0, 0, 0.1f);
            }
            Sun.transform.rotation = Quaternion.Lerp(Sun.transform.rotation, Quaternion.Euler(new Vector3(30,0,0)), Time.deltaTime*5f);
            yield return new WaitForSeconds(0.1f);
        }
        RenderSettings.ambientSkyColor = new Color(0.99f, 0.66f, 0.66f);
        MapBack1.sprite = MapBack2.sprite;
        MapBack1.color = new Color(1, 1, 1, 1);
        MapBack2.color = new Color(1, 1, 1, 0);
    }

    IEnumerator ToNight()
    {
        GOD.PlayerContr.IsFrostMaterial = false;
        GOD.PlayerContr.IsFrostDialog = false;
        GOD.PlayerContr.Frost = 0;

        //скайбокс
        SkyBox.SetTexture("_FrontTex", Front2);
        SkyBox.SetTexture("_BackTex", Back2);
        SkyBox.SetTexture("_LeftTex", Left2);
        SkyBox.SetTexture("_RightTex", Right2);
        SkyBox.SetTexture("_UpTex", Up2);
        SkyBox.SetTexture("_DownTex", Down2);

        SkyBox.SetTexture("_FrontTex1", Front3);
        SkyBox.SetTexture("_BackTex1", Back3);
        SkyBox.SetTexture("_LeftTex1", Left3);
        SkyBox.SetTexture("_RightTex1", Right3);
        SkyBox.SetTexture("_UpTex1", Up3);
        SkyBox.SetTexture("_DownTex1", Down3);

        SkyBox.SetFloat("_Blend", 0);

        for (int i = 0; i < AllFogs.Length; i++)  // включаем туман
        {
            var c = AllFogs[i].emission;
            c.rateOverTime = 10f;
        }
        for (int i = 0; i < AllYellowSparks.Count; i++)  // выключаем желтых светляков
        {
            var c = AllYellowSparks[i].emission;
            c.rateOverTime = 0f;
        }
        for (int i = 0; i < AllBlueSparks.Count; i++)  // включаем синих светляков
        {
            var c = AllBlueSparks[i].emission;
            c.rateOverTime = 10f;
        }
        for (int i = 0; i < AllLightShaft.Count; i++)  // лучи ночные
        {
            var mainModul = AllLightShaft[i].main;
            mainModul.startColor = new Color(1, 1, 1);
            var col = AllLightShaft[i].colorOverLifetime;
            Gradient grad = new Gradient();
            grad.SetKeys(new GradientColorKey[] { new GradientColorKey(new Color(0, 0, 0), 0.0f), new GradientColorKey(new Color(0.39f, 0.86f, 0.98f), 0.5f), new GradientColorKey(new Color(0, 0, 0), 1f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 0f) });
            col.color = grad;
        }


        MapBack2.sprite = NightMap;
        MapBack2.color = new Color(1, 1, 1, 1);
        float x = 0;
        while (RenderSettings.ambientSkyColor.r > 0)
        {
            if(RenderSettings.ambientSkyColor.r>0)
                RenderSettings.ambientSkyColor -= new Color(0.03f, 0, 0);
            if (RenderSettings.ambientSkyColor.g > 0.13f)
                RenderSettings.ambientSkyColor -= new Color(0, 0.015f, 0);
            if (RenderSettings.ambientSkyColor.b > 0.52f)
                RenderSettings.ambientSkyColor -= new Color(0, 0, 0.01f);

            if (Sun.color.r > 0.63f)
               Sun.color -= new Color(0.01f, 0, 0);
            if (Sun.color.g > 0.7f)
                Sun.color -= new Color(0, 0.01f, 0);
            if (Sun.color.g < 0.98f)
                Sun.color += new Color(0, 0, 0.017f);

            if (x < 1)
                x += 0.07f;
            if (x > 1)
                x = 1;
            SkyBox.SetFloat("_Blend", x);
            if (MapBack1.color.a > 0)
                MapBack1.color -= new Color(0, 0, 0, 0.1f);

            yield return new WaitForSeconds(0.1f);
        }
       RenderSettings.ambientSkyColor = new Color(0, 0.13f, 0.52f);
        Sun.color = new Color(0.63f, 0.7f, 0.98f);
        MapBack1.sprite = MapBack2.sprite;
        MapBack1.color = new Color(1, 1, 1, 1);
        MapBack2.color = new Color(1, 1, 1, 0);
    }

    IEnumerator ToDay()
    {
        //скайбокс
        SkyBox.SetTexture("_FrontTex", Front3);
        SkyBox.SetTexture("_BackTex", Back3);
        SkyBox.SetTexture("_LeftTex", Left3);
        SkyBox.SetTexture("_RightTex", Right3);
        SkyBox.SetTexture("_UpTex", Up3);
        SkyBox.SetTexture("_DownTex", Down3);

        SkyBox.SetTexture("_FrontTex1", Front1);
        SkyBox.SetTexture("_BackTex1", Back1);
        SkyBox.SetTexture("_LeftTex1", Left1);
        SkyBox.SetTexture("_RightTex1", Right1);
        SkyBox.SetTexture("_UpTex1", Up1);
        SkyBox.SetTexture("_DownTex1", Down1);

        SkyBox.SetFloat("_Blend", 0);

        for (int i = 0; i < AllFogs.Length; i++)  // выключаем туман
        {
            var c = AllFogs[i].emission;
            c.rateOverTime = 0f;
        }
        for (int i = 0; i < AllYellowSparks.Count; i++)  // включаем желтых светляков
        {
            var c = AllYellowSparks[i].emission;
            c.rateOverTime = 10f;
        }
        for (int i = 0; i < AllBlueSparks.Count; i++)  // выключаем синих светляков
        {
            var c = AllBlueSparks[i].emission;
            c.rateOverTime = 0f;
        }
        for (int i = 0; i < AllLightShaft.Count; i++)  // лучи дневные
        {
            var mainModule = AllLightShaft[i].main;
            mainModule.startColor = new Color(1, 0.87f, 0.18f);
            var col = AllLightShaft[i].colorOverLifetime;
            Gradient grad = new Gradient();
            grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.black, 0), new GradientColorKey(new Color(0.93f, 0.63f, 0.07f), 0.5f), new GradientColorKey(Color.black, 1) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 0.0f) });
            col.color = grad;
        }

        MapBack2.sprite = DayMap;
        MapBack2.color = new Color(1, 1, 1, 1);
        float x = 0;
        while (RenderSettings.ambientSkyColor.r< 1)
        {
            if (RenderSettings.ambientSkyColor.r <1)
                RenderSettings.ambientSkyColor += new Color(0.03f, 0, 0);
            if (RenderSettings.ambientSkyColor.g < 1)
                RenderSettings.ambientSkyColor += new Color(0, 0.03f, 0);
            if (RenderSettings.ambientSkyColor.b< 1)
                RenderSettings.ambientSkyColor += new Color(0, 0, 0.03f);

            if (Sun.color.r < 0.98f)
                Sun.color += new Color(0.01f, 0, 0);
            if (Sun.color.g <0.83f)
                Sun.color += new Color(0, 0.01f, 0);
            if (Sun.color.g > 0.4f)
                Sun.color -= new Color(0, 0, 0.017f);

            if (x < 1)
                x += 0.07f;
            if(x>1)
                x = 1;
            SkyBox.SetFloat("_Blend", x);
            if (MapBack1.color.a > 0)
                MapBack1.color -= new Color(0, 0, 0, 0.1f);
            Sun.transform.rotation = Quaternion.Lerp(Sun.transform.rotation, Quaternion.Euler(new Vector3(70, 0, 0)), Time.deltaTime * 5f);
            yield return new WaitForSeconds(0.1f);
        }
        RenderSettings.ambientSkyColor = new Color(1, 1 ,1);
        Sun.color = new Color(0.98f, 0.83f, 0.4f);
        MapBack1.sprite = MapBack2.sprite;
        MapBack1.color = new Color(1, 1, 1, 1);
        MapBack2.color = new Color(1, 1, 1, 0);
    }



}
