﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeSnow : MonoBehaviour {

    public GameManager GOD;
    public string CurrentState = "Snow";
    public string OldState;

    public Material SnowIsland;              // материал земли
    public Texture SnowText, IceText, GrassText;
    public Material Ykazateli, LimitStone;                            // материал указателей и камней на границе и статуй
    public Material Plant1, Plant2, Objects;
    public Texture WinterSnow, WinterIce;
    public GameObject Snow1, Snow2, Snow3, UnderSnow1, UnderSnow2, UnderSnow3;
    public GameObject IcePear1, IcePear2, IcePear3;
    public List<Transform> AllIcePlants = new List<Transform>();
    public Material IceRockMat;
    public GameObject IceRock1, IceRock2, IceRock3;

    public Animator IceBreakAnimator, IceBreakAnimator2;    // глыба
    public GameObject IceBreak, IceBreak2;
    public Material IceBreakMat;
    public GameObject DopLimit, DopLimit2;

    public GameObject IceBridgeBroken1, IceBridgeBroken2;
    public MeshCollider IceBridge1, IceBridge2;  // ледяной мост
    public Material IceBridgeMat;
    public GameObject DopLimitBridge1;

    int ShowStep = 0;
    bool IsTextChange = false;
    bool IsIceStopGrow = false;

    public bool IsIceBreak = false;

    public void Init()
    {
      
        for (int i = 0; i < IcePear1.transform.childCount; i++)
            AllIcePlants.Add(IcePear1.transform.GetChild(i));
        for (int i = 0; i < IcePear2.transform.childCount; i++)
            AllIcePlants.Add(IcePear2.transform.GetChild(i));
        for (int i = 0; i < IcePear3.transform.childCount; i++)
            AllIcePlants.Add(IcePear3.transform.GetChild(i));

        SetSnowState();
    }

    public void SetSnowState() // устанавливаем определенное состояние для снежного острова
    {
        switch(CurrentState)
        {
            case "Snow":
                SnowIsland.SetTexture("_Splat0", SnowText);  // текстура земли
                SnowIsland.SetTexture("_DopSplat", SnowText);
                SnowIsland.SetFloat("_Alpha", 1);

                Plant1.SetTexture("_SecondTex", WinterSnow);  // текстура растений и каменй
                Plant1.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Plant1.SetFloat("_Alpha", 1);
                Plant2.SetTexture("_SecondTex", WinterSnow);
                Plant2.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Plant2.SetFloat("_Alpha", 1);
                Objects.SetTexture("_SecondTex", WinterSnow);
                Objects.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Objects.SetFloat("_Alpha", 1);

                Ykazateli.SetTexture("_SecondTex", WinterSnow);  //текстура указателей
                Ykazateli.SetFloat("_Alpha", 1); 
                LimitStone.SetTexture("_SecondTex", WinterSnow);  //текстура камнец у границ
                LimitStone.SetFloat("_Alpha", 1);
               // TV_statue.SetTexture("_SecondTex", WinterSnow);  //текстура статуй
              // TV_statue.SetFloat("_Alpha", 1);

                IceRockMat.SetColor("_Color", new Color(1, 1, 1, 1));  // глыбы льда
                IceRock1.SetActive(true);
                IceRock2.SetActive(true);
                IceRock3.SetActive(true);

                IceBridgeMat.SetColor("_Color", new Color(1, 1, 1, 0f));  // ледяной мост
                IceBridge1.enabled = false;
                IceBridge2.enabled = false;
                DopLimitBridge1.SetActive(true);

                UnderSnow1.SetActive(false);  // объекты под сугробами
                UnderSnow2.SetActive(false);
                UnderSnow3.SetActive(false);

                for (int i = 0; i < AllIcePlants.Count; i++)  // ледяные груши
                {
                    AllIcePlants[i].transform.localScale = new Vector3(0, 0, 0);
                    AllIcePlants[i].gameObject.SetActive(false);
                }
                break;
            case "Sun":
                SnowIsland.SetTexture("_Splat0", GrassText);  // текстура земли
                SnowIsland.SetTexture("_DopSplat", GrassText);
                SnowIsland.SetFloat("_Alpha", 1);

                Plant1.SetTexture("_SecondTex", WinterSnow);  // текстура растений и каменй
                Plant1.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Plant1.SetFloat("_Alpha", 0);
                Plant2.SetTexture("_SecondTex", WinterSnow);
                Plant2.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Plant2.SetFloat("_Alpha", 0);
                Objects.SetTexture("_SecondTex", WinterSnow);
                Objects.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Objects.SetFloat("_Alpha", 0);

                Ykazateli.SetTexture("_SecondTex", WinterSnow);  //текстура указателей
                Ykazateli.SetFloat("_Alpha", 0);
                LimitStone.SetTexture("_SecondTex", WinterSnow);  //текстура камнец у границ
                LimitStone.SetFloat("_Alpha", 0);
                //TV_statue.SetTexture("_SecondTex", WinterSnow);  //текстура статуй
                //TV_statue.SetFloat("_Alpha", 0);

                IceRockMat.SetColor("_Color", new Color(1, 1, 1, 0));  // глыбы льда
                IceRock1.SetActive(false);
                IceRock2.SetActive(false);
                IceRock3.SetActive(false);
                IsIceStopGrow = true;

                IceBridgeMat.SetColor("_Color", new Color(1, 1, 1, 0f));  // ледяной мост
                IceBridge1.enabled = false;
                IceBridge2.enabled = false;
                DopLimitBridge1.SetActive(true);

                UnderSnow1.SetActive(true);  // объекты под сугробами
                UnderSnow2.SetActive(true);
                UnderSnow3.SetActive(true);
                UnderSnow1.transform.localPosition = new Vector3(0, 0, 0);
                UnderSnow2.transform.localPosition = new Vector3(0, 0, 0);
                UnderSnow3.transform.localPosition = new Vector3(0, 0, 0);

                for (int i = 0; i < AllIcePlants.Count; i++)  // ледяные груши
                {
                    AllIcePlants[i].transform.localScale = new Vector3(0, 0, 0);
                    AllIcePlants[i].gameObject.SetActive(false);
                }

                Snow1.transform.localPosition -= new Vector3(0, -1, 0);//снег
                Snow2.transform.localPosition -= new Vector3(0, -1, 0);
                Snow3.transform.localPosition -= new Vector3(0, -1, 0);
                Snow1.SetActive(false);
                Snow2.SetActive(false);
                Snow3.SetActive(false);
                break;
            case "Water":
                SnowIsland.SetTexture("_Splat0", IceText);  // текстура земли
                SnowIsland.SetTexture("_DopSplat", IceText);
                SnowIsland.SetFloat("_Alpha", 1);

                Plant1.SetTexture("_SecondTex", WinterIce);  // текстура растений и каменй
                Plant1.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Plant1.SetFloat("_Alpha", 1);
                Plant2.SetTexture("_SecondTex", WinterIce);
                Plant2.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Plant2.SetFloat("_Alpha", 1);
                Objects.SetTexture("_SecondTex", WinterIce);
                Objects.SetTextureScale("_SecondTex", new Vector2(3, 3));
                Objects.SetFloat("_Alpha", 1);

                Ykazateli.SetTexture("_SecondTex", WinterIce);  //текстура указателей
                Ykazateli.SetFloat("_Alpha", 1);
                LimitStone.SetTexture("_SecondTex", WinterIce);  //текстура камнец у границ
                LimitStone.SetFloat("_Alpha", 1);
               // TV_statue.SetTexture("_SecondTex", WinterIce);  //текстура статуй
                //TV_statue.SetFloat("_Alpha", 1);

                IceRockMat.SetColor("_Color", new Color(1, 1, 1, 1));  // глыбы льда
                IceRock1.SetActive(true);
                IceRock2.SetActive(true);
                IceRock3.SetActive(true);
                IsIceStopGrow = true;

                IceBridgeMat.SetColor("_Color", new Color(1, 1, 1, 1f));  // ледяной мост
                IceBridge1.enabled = true;
                IceBridge2.enabled = true;
                DopLimitBridge1.SetActive(false);

                UnderSnow1.SetActive(false);  // объекты под сугробами
                UnderSnow2.SetActive(false);
                UnderSnow3.SetActive(false);

                for (int i = 0; i < AllIcePlants.Count; i++)  // ледяные груши
                {
                    AllIcePlants[i].transform.localScale = new Vector3(1, 1, 1);
                    AllIcePlants[i].gameObject.SetActive(true);
                }

                break;
        }
    }

    public void SetIceBreakLay(int x)//глыба лежит
    {
        if (x > 0)
        {
            GameObject g;
            if (x == 1)
            {
                g = IceBreak;
                IceBreak2.SetActive(false);
            }
            else
            {
                g = IceBreak2;
                IceBreak.SetActive(false);
            }
            g.SetActive(true);
            g.GetComponent<Animator>().Play("FallStay");
            IsIceBreak = true;
            DopLimit.SetActive(false);
            DopLimit2.SetActive(false);

        }
        else
        {
            IceBreakMat.SetColor("_Color", new Color(1, 1, 1, 1));
            if (GOD.EnviromentContr.CurrentIsland == "WinterIsland2" || GOD.EnviromentContr.CurrentIsland == "WinterIsland3")
            {
                IceBreak2.SetActive(true);
                IceBreak.SetActive(false);
                if (CurrentState == "Sun")
                    IceBreak2.tag = "BreakIce";
                else
                    IceBreak2.tag = "Ignored";
            }
            else
            {
                IceBreak.SetActive(true);
                IceBreak2.SetActive(false);
                if (CurrentState == "Sun")
                    IceBreak.tag = "BreakIce";
                else
                    IceBreak.tag = "Ignored";
            }
        }
    }

    public void ChangeWeather(string state)
    {
        if (GOD.EnviromentContr.CanChange)
        {
            OldState = CurrentState;
            if ((state == "Snow") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;

                SnowIsland.SetTexture("_Splat0", SnowIsland.GetTexture("_DopSplat"));     // вторая текстура теперь первая
                SnowIsland.SetTexture("_DopSplat", SnowText);                             // указываем вторую текстуру
                SnowIsland.SetFloat("_Alpha", 1);                                         // активна первая текстцура
                StartCoroutine(ChangeTexture());                                          // меняем текстуру земли
                StartCoroutine(ChangePlantTexture(1));                                   // меняем текстуру растений
                StartCoroutine(ChangeSnowPosition(1));                                   // поднимаем снег
                StartCoroutine(ChangeIceBridgeTexture(-1));                              // убираем ледяной мост

                GOD.EnviromentContr.CSnow.DopLimit.SetActive(true);                       // включаем заграждение за ледяными глыбами
                GOD.EnviromentContr.CSnow.DopLimit2.SetActive(true);

                if (OldState == "Water")
                    StartCoroutine(IcePlants(-1));                                       // убираем ледяные деревья
                if (OldState == "Sun")
                    StartCoroutine(ChangeIceRockTexture(1));                             // возвращаем ледяные глыбы

                if(IsIceBreak)                                                       // обрушиваем ледяные глыбы, если они лежали
                {
                    if (IceBreak2.activeSelf)
                    {
                        IceBreakAnimator2.Play("IceFallBot");
                        IceBreak2.tag = "Ignored";
                    }
                    else
                    {
                        IceBreakAnimator.Play("IceFallBot");
                        IceBreak.tag = "Ignored";
                    }
                    IsIceBreak = false;
                    //IsIceStopGrow = false;
                }

                ShowStep = 0;
                IsTextChange = false;
                if(OldState =="Water")
                    IsIceStopGrow = false;
                else
                    IsIceStopGrow = true;

                StartCoroutine(Porydok());
                // делаем задержку следующей смены погоды
                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);
            }
            else if ((state == "Sun") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;

                SnowIsland.SetTexture("_Splat0", SnowIsland.GetTexture("_DopSplat"));        
                SnowIsland.SetTexture("_DopSplat", GrassText);                            
                SnowIsland.SetFloat("_Alpha", 1);                                               
                StartCoroutine(ChangeTexture());
                StartCoroutine(ChangePlantTexture(-1));                                 // убираем снег или иней с текстуры растений
                StartCoroutine(ChangeSnowPosition(-1));                                 // опускаем снег
                StartCoroutine(ChangeIceBridgeTexture(-1));                             // убираем ледяной мост
                 
                if (OldState == "Water")
                    StartCoroutine(IcePlants(-1));

                StartCoroutine(ChangeIceRockTexture(-1));                             // ледяные глыбы таят

                ShowStep = 0;
                IsTextChange = false;
                if (OldState == "Water")
                    IsIceStopGrow = false;
                else
                    IsIceStopGrow = true;
                StartCoroutine(Porydok());
                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);

            }
            else if ((state == "Water") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;

                SnowIsland.SetTexture("_Splat0", SnowIsland.GetTexture("_DopSplat"));
                SnowIsland.SetTexture("_DopSplat", IceText);
                SnowIsland.SetFloat("_Alpha", 1);
                StartCoroutine(ChangeTexture());
                StartCoroutine(ChangePlantTexture(1));                                      // покрываем растения инеем
                StartCoroutine(ChangeSnowPosition(1));                                      // поднимаем снег
                StartCoroutine(IcePlants(1));                                               // вырастают ледяные растения
                StartCoroutine(ChangeIceBridgeTexture(1));                                  // появляется ледяной мост

                GOD.EnviromentContr.CSnow.DopLimit.SetActive(true);
                GOD.EnviromentContr.CSnow.DopLimit2.SetActive(true);

                if (OldState == "Sun")
                    StartCoroutine(ChangeIceRockTexture(1));

                IceBridgeBroken1.SetActive(false);
                IceBridgeBroken2.SetActive(false);


                if (IsIceBreak)
                {
                    if (IceBreak2.activeSelf)
                    {
                        IceBreakAnimator2.Play("IceFallBot");
                        IceBreak2.tag = "Ignored";
                    }
                    else
                    {
                        IceBreakAnimator.Play("IceFallBot");
                        IceBreak.tag = "Ignored";
                    }
                    IsIceBreak = false;
                    // IsIceStopGrow = false;
                }

                ShowStep = 0;
                IsTextChange = false;
                IsIceStopGrow = false;

                StartCoroutine(Porydok());
                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);

                // GOD.DopEff.PlayerOnWater.gameObject.SetActive(true);
            }
            GOD.EnviromentContr.CurrentState = CurrentState;
        }
    }

   

    IEnumerator Porydok()
    {
        while (ShowStep < 1)
        {
            if (IsTextChange&&IsIceStopGrow)
            {
                GOD.EnviromentContr.UpdateCanChange();
                ShowStep++;
                if (CurrentState == "Sun")
                {
                    if (GOD.EnviromentContr.CurrentIsland == "WinterIsland2" || GOD.EnviromentContr.CurrentIsland == "WinterIsland3")
                        IceBreak2.tag = "BreakIce";
                    else
                        IceBreak.tag = "BreakIce";
                }
            }
            yield return new WaitForFixedUpdate();
        }
    }

    //ТЕКСТУРЫ_______________________________________________________________________________________________________________
    IEnumerator ChangeTexture()         // меняем текстуры земли
    {
        while (SnowIsland.GetFloat("_Alpha") > 0f)
        {
            SnowIsland.SetFloat("_Alpha", SnowIsland.GetFloat("_Alpha") - 0.01f);
            if (SnowIsland.GetFloat("_Alpha") <= 0)
                IsTextChange = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangeIceRockTexture(int x)         // меняем текстуры льда
    {
        if (x < 0)
        {
            while (IceRockMat.GetColor("_Color").a > 0f)
            {
                IceRockMat.SetColor("_Color", IceRockMat.GetColor("_Color") - new Color(0, 0, 0, 0.015f));
                if (IceRockMat.GetColor("_Color").a <=0)
                {
                    IceRock1.SetActive(false);
                    IceRock2.SetActive(false);
                    IceRock3.SetActive(false);
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            IceRock1.SetActive(true);
            IceRock2.SetActive(true);
            IceRock3.SetActive(true);
            while (IceRockMat.GetColor("_Color").a < 1f)
            {
                IceRockMat.SetColor("_Color", IceRockMat.GetColor("_Color") + new Color(0, 0, 0, 0.015f));
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    public IEnumerator ChangeIceBreakTexture()         // меняем текстуры ледяной глыбы
    {
        GameObject Ice = IceBreak;
        if (GOD.EnviromentContr.CurrentIsland == "WinterIsland2" || GOD.EnviromentContr.CurrentIsland == "WinterIsland3")
        {
            Ice = IceBreak2;
            IceBreak.SetActive(false);
        }
        else
            IceBreak2.SetActive(false);

        Ice.SetActive(true);
        Ice.GetComponent<ObjectScript>().Init();
         IceBreakMat.SetColor("_Color", new Color(1,1,1,0));
        StartCoroutine(S());
        yield return new WaitForSeconds(0.1f);

    }

    IEnumerator S()
    {
        while (IceBreakMat.GetColor("_Color").a < 1f)
        {
            IceBreakMat.SetColor("_Color", IceBreakMat.GetColor("_Color") + new Color(0, 0, 0, 0.1f));
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangeIceBridgeTexture(int x)       // включаем отключаем ледяной мост
    {
        if (x > 0)
        {
            while (IceBridgeMat.GetColor("_Color").a < 1f)
            {
                IceBridgeMat.SetColor("_Color", IceBridgeMat.GetColor("_Color") + new Color(0, 0, 0, 0.1f));
                if(IceBridgeMat.GetColor("_Color").a>=1f)
                {
                    IceBridge1.enabled = true;
                    IceBridge2.enabled = true;
                    DopLimitBridge1.SetActive(false);
                    DopLimitBridge1.SetActive(false);
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            IceBridgeBroken1.SetActive(true);
            IceBridgeBroken2.SetActive(true);
            IceBridge1.enabled = false;
            IceBridge2.enabled = false;
            DopLimitBridge1.SetActive(true);
            DopLimitBridge1.SetActive(true);
            while (IceBridgeMat.GetColor("_Color").a > 0f)
            {
                IceBridgeMat.SetColor("_Color", IceBridgeMat.GetColor("_Color") - new Color(0, 0, 0, 0.1f));
                yield return new WaitForSeconds(0.1f);
            }

        }
    }
    IEnumerator ChangePlantTexture(int x)
    {
        if(x<0)                                                                        // топим все
        {
            while (Plant1.GetFloat("_Alpha") > 0f)
            {
                float z = Plant1.GetFloat("_Alpha") - 0.01f;
                Plant1.SetFloat("_Alpha", z);
                Plant2.SetFloat("_Alpha", z);
                Objects.SetFloat("_Alpha", z);
                Ykazateli.SetFloat("_Alpha", z);
               LimitStone.SetFloat("_Alpha", z);
               // TV_statue.SetFloat("_Alpha", z);
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            if(OldState=="Sun")                                                    // покрываем снегом\льдом
            {
                if (CurrentState == "Snow")
                {
                    Plant1.SetTexture("_SecondTex", WinterSnow);
                    Plant1.SetTextureScale("_SecondTex", new Vector2(3, 3));
                    Plant2.SetTexture("_SecondTex", WinterSnow);
                    Plant2.SetTextureScale("_SecondTex", new Vector2(3, 3));
                    Objects.SetTexture("_SecondTex", WinterSnow);
                   Objects.SetTextureScale("_SecondTex", new Vector2(3, 3));
                    Ykazateli.SetTexture("_SecondTex", WinterSnow);
                    LimitStone.SetTexture("_SecondTex", WinterSnow);
                   // TV_statue.SetTexture("_SecondTex", WinterSnow);
                }
                else
                {
                    Plant1.SetTexture("_SecondTex", WinterIce);
                    Plant1.SetTextureScale("_SecondTex", new Vector2(10, 10));
                    Plant2.SetTexture("_SecondTex", WinterIce);
                    Plant2.SetTextureScale("_SecondTex", new Vector2(10, 10));
                   Objects.SetTexture("_SecondTex", WinterIce);
                   Objects.SetTextureScale("_SecondTex", new Vector2(10, 10));
                   Ykazateli.SetTexture("_SecondTex", WinterIce);
                    LimitStone.SetTexture("_SecondTex", WinterIce);
                  //  TV_statue.SetTexture("_SecondTex", WinterIce);

                }

                while (Plant1.GetFloat("_Alpha") < 1f)
                {
                    float z = Plant1.GetFloat("_Alpha") + 0.01f;
                    Plant1.SetFloat("_Alpha", z);
                    Plant2.SetFloat("_Alpha", z);
                    Objects.SetFloat("_Alpha", z);
                    Ykazateli.SetFloat("_Alpha", z);
                   LimitStone.SetFloat("_Alpha", z);
                   // TV_statue.SetFloat("_Alpha", z);
                    yield return new WaitForSeconds(0.1f);
                }
            }
            else
            {
                while (Plant1.GetFloat("_Alpha") > 0f)
                {
                    float z = Plant1.GetFloat("_Alpha") - 0.05f;
                    Plant1.SetFloat("_Alpha", z);
                    Plant2.SetFloat("_Alpha", z);
                    Objects.SetFloat("_Alpha", z);
                    Ykazateli.SetFloat("_Alpha", z);
                    LimitStone.SetFloat("_Alpha", z);
                   // TV_statue.SetFloat("_Alpha", z);
                    yield return new WaitForSeconds(0.1f);
                }
                if (CurrentState == "Snow")
                {
                    Plant1.SetTexture("_SecondTex", WinterSnow);
                    Plant1.SetTextureScale("_SecondTex", new Vector2(3, 3));
                    Plant2.SetTexture("_SecondTex", WinterSnow);
                    Plant2.SetTextureScale("_SecondTex", new Vector2(3, 3));
                   Objects.SetTexture("_SecondTex", WinterSnow);
                    Objects.SetTextureScale("_SecondTex", new Vector2(3, 3));
                    Ykazateli.SetTexture("_SecondTex", WinterSnow);
                    LimitStone.SetTexture("_SecondTex", WinterSnow);
                  // TV_statue.SetTexture("_SecondTex", WinterSnow);
                }
                else
                {
                    Plant1.SetTexture("_SecondTex", WinterIce);
                    Plant1.SetTextureScale("_SecondTex", new Vector2(10, 10));
                    Plant2.SetTexture("_SecondTex", WinterIce);
                    Plant2.SetTextureScale("_SecondTex", new Vector2(10, 10));
                   Objects.SetTexture("_SecondTex", WinterIce);
                    Objects.SetTextureScale("_SecondTex", new Vector2(10, 10));
                    Ykazateli.SetTexture("_SecondTex", WinterIce);
                    LimitStone.SetTexture("_SecondTex", WinterIce);
                   // TV_statue.SetTexture("_SecondTex", WinterIce);
                }
                while (Plant1.GetFloat("_Alpha") < 1f)
                {
                    float z = Plant1.GetFloat("_Alpha") + 0.05f;
                    Plant1.SetFloat("_Alpha", z);
                    Plant2.SetFloat("_Alpha", z);
                    Objects.SetFloat("_Alpha", z);
                    Ykazateli.SetFloat("_Alpha", z);
                   LimitStone.SetFloat("_Alpha", z);
                   // TV_statue.SetFloat("_Alpha", z);
                    yield return new WaitForSeconds(0.1f);
                }

            }
        }
    }

    //СНЕГ___________________________________________________________________________________________________________
    IEnumerator ChangeSnowPosition(int x)        // опускаем поднимаем снег
    {
        if (x < 0)
        {
            UnderSnow1.SetActive(true);
            UnderSnow2.SetActive(true);
            UnderSnow3.SetActive(true);
            while (Snow1.transform.localPosition.y > -1)
            {
                Snow1.transform.localPosition -= new Vector3(0, 0.04f, 0);
                Snow2.transform.localPosition -= new Vector3(0, 0.04f, 0);
                Snow3.transform.localPosition -= new Vector3(0, 0.04f, 0);
                if (UnderSnow1.transform.localPosition.y < 0)
                {
                    UnderSnow1.transform.localPosition += new Vector3(0, 0.04f, 0);
                    UnderSnow2.transform.localPosition += new Vector3(0, 0.04f, 0);
                    UnderSnow3.transform.localPosition += new Vector3(0, 0.04f, 0);
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (Snow1.transform.localPosition.y <= -1)
            {
                Snow1.SetActive(false);
                Snow2.SetActive(false);
                Snow3.SetActive(false);
            }

        }
        else
        {
            Snow1.SetActive(true);
            Snow2.SetActive(true);
            Snow3.SetActive(true);
            while (Snow1.transform.localPosition.y < 0)
            {
                Snow1.transform.localPosition += new Vector3(0, 0.04f, 0);
                Snow2.transform.localPosition += new Vector3(0, 0.04f, 0);
                Snow3.transform.localPosition += new Vector3(0, 0.04f, 0);
                if (UnderSnow1.transform.localPosition.y > -1)
                {
                    UnderSnow1.transform.localPosition -= new Vector3(0, 0.04f, 0);
                    UnderSnow2.transform.localPosition -= new Vector3(0, 0.04f, 0);
                    UnderSnow3.transform.localPosition -= new Vector3(0, 0.04f, 0);
                }
                yield return new WaitForSeconds(0.1f);
            }
            if (Snow1.transform.localPosition.y >= 0)
            {
                UnderSnow1.SetActive(false);
                UnderSnow2.SetActive(false);
                UnderSnow3.SetActive(false);
            }
        }
    }

    //ДЕРЕВЬЯ__________________________________________________________________________
    public IEnumerator IcePlants(int x)   // выращиваем убираем  растения
    {
        if (x < 0)
        {
            while (AllIcePlants[0].localScale.x > 0)
            {
                for (int i = 0; i < AllIcePlants.Count; i++)
                {
                    AllIcePlants[i].localScale -= new Vector3(0.015f, 0.015f, 0.015f);
                    if (AllIcePlants[i].localScale.x < 0)
                    {
                        AllIcePlants[i].localScale = new Vector3(0, 0, 0);
                        AllIcePlants[i].gameObject.SetActive(false);
                        IsIceStopGrow = true;
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
        else
        {
            for (int i = 0; i < AllIcePlants.Count; i++)
                AllIcePlants[i].gameObject.SetActive(true);

            while (AllIcePlants[0].localScale.x < 1f)
            {
                for (int i = 0; i < AllIcePlants.Count; i++)
                {
                    AllIcePlants[i].localScale += new Vector3(0.015f, 0.015f, 0.015f);
                    if (AllIcePlants[i].localScale.x > 1f)
                    {
                        AllIcePlants[i].localScale = new Vector3(1f, 1f, 1f);
                        IsIceStopGrow = true;
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
