﻿using UnityEngine;
using System.Collections;

public class ChangeTutorial : MonoBehaviour {

    public GameManager GOD;
    public string CurrentState = "Green";
    public Material TutGround;
    public Material Water;
    //public GameObject Foam;
    public FoamScript Foam;
    public GameObject Mountain;
    public Material MountainMat;
    public GameObject Oazis;
    public Material Plant, Stone, Teleport;

    public void SetWeather()
    {
        TutGround.SetFloat("_Alpha", 1);
        Water.SetFloat("_Blend", 1);
        Color c = MountainMat.GetColor("_Color");
        MountainMat.SetColor("_Color", new Color(c.r, c.g, c.b, 0f));
        Mountain.SetActive(false);
        Plant.SetFloat("_Alpha", 0);
        Plant.color = new Color(1, 1, 1);
        Stone.SetFloat("_Alpha", 0);
        Foam.Init();
    }

    public void ChangeWeather()
    {
        GOD.EnviromentContr.CanChange = false;
        GOD.DopEff.Snow.transform.SetParent(null);
        StartCoroutine(ChangeFoamGradually());
        StartCoroutine(ChangeTexture());
        StartCoroutine(ChangeWaterText());
        StartCoroutine(ChangeIceWaterFall());
        StartCoroutine(ChangePlantTexture());
        Oazis.GetComponent<BoxCollider>().enabled = false;
        CurrentState = "Snow";
        GOD.Audio.SetPlayerStep();    //переключаем звуки шагов
        for (int i = 0; i < GOD.Audio.Waterfalls.Length; i++)
            GOD.Audio.Waterfalls[i].enabled = false;
    }


    //ТЕКСТУРЫ_______________________________________________________________________________________________________________
    IEnumerator ChangeTexture()         // меняем текстуры земли
    {
        while (TutGround.GetFloat("_Alpha") > 0f)
        {
            TutGround.SetFloat("_Alpha", TutGround.GetFloat("_Alpha") - 0.01f);
           // if (TutGround.GetFloat("_Alpha") <= 0)
               // IsTextChange = true;
            yield return new WaitForSeconds(0.1f);
        }
        GOD.EnviromentContr.UpdateCanChange();
    }

    IEnumerator ChangeWaterText()        // меняем воду на лед
    {
        while (Water.GetFloat("_Blend") > 0f)
        {
            Water.SetFloat("_Blend", Water.GetFloat("_Blend") - 0.01f);
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangeIceWaterFall()  // появление исчезание ледяного водопада
    {
        MountainScript MScript = Mountain.GetComponent<MountainScript>();
        MScript.SetMountain(false);
            Color c = MountainMat.GetColor("_Color");
        MountainMat.SetColor("_Color", new Color(c.r, c.g, c.b, 0f));
            c = MountainMat.GetColor("_Color");
        Mountain.SetActive(true);
            while (MountainMat.GetColor("_Color").a < 1f)
            {
            MountainMat.SetColor("_Color", MountainMat.GetColor("_Color") + new Color(0, 0, 0, 0.01f));
                yield return new WaitForSeconds(0.1f);
            }
        if (MountainMat.GetColor("_Color").a >= 1)
        {
            MScript.SetMountain(true);
        }
    }

    IEnumerator ChangePlantTexture()
    {
        while (Plant.GetFloat("_Alpha") < 1f)
        {
            float z = Plant.GetFloat("_Alpha") + 0.05f;
            Plant.SetFloat("_Alpha", z);
            Stone.SetFloat("_Alpha", z);
            Teleport.SetFloat("_Alpha", z);
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator ChangeFoamGradually()
    {
        Foam.ChangeFoam(2);
        yield return new WaitForSeconds(2);
        Foam.ChangeFoam(1);
        yield return new WaitForSeconds(2);
        Foam.ChangeFoam(0);
    }

}
