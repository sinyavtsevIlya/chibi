﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChangeWater : MonoBehaviour
{

    public GameManager GOD;
    public string CurrentState = "Water";
    public string OldState;

    public Material WaterIsland;              // материал земли
    public Material River;              // материал рек
    public Material WaterPlants1, WaterPlants2, WaterObject, Reeds, StoneB;// материалы растений
    public Material Ykazateli, LimitStone;                            // материал указателей и камней на границе и статуй
    public Texture SnowGrass, WaterGrass, YellowGrass;
    Color YellowColor = new Color(0.97f, 0.6f, 0.27f);
    public GameObject River1, River2, River3;             // реки
    BoxCollider MRiver1, MRiver2, MRiver3;  // коллайдеры рек
    public GameObject River1Limit, River2Limit, River3Limit;             // границы рек
    public GameObject IceWaterFall1, IceWaterFall2, LeanWaterfall;   // замерзшие водопады
    public Material IceWaterFallMat, LeanWaterFallMat;
    public GameObject Reeds1, Reeds2; // камыши
    public Transform WaterIsland1Puddle, WaterIsland2Puddle, WaterIsland3Puddle;  // лужи
    public List<Transform> AllWaterPuddles = new List<Transform>();
    public GameObject UnderWaterRes, UnderWaterRes2;      // ресурсы под водой

    bool IstextChange = false, IsPuddleStop = false, IsPlantTextChange = false;
    int ShowStep = 0;
    int TextChange = 0;


    int WaterStep = 0;
    bool WaterChanged = false;
    bool WaterChangedPosition = false;



    public void Init()
    {
        MRiver1 = River1.GetComponent<BoxCollider>();
        MRiver2 = River2.GetComponent<BoxCollider>();
        MRiver3 = River3.GetComponent<BoxCollider>();
        for (int i = 0; i < WaterIsland1Puddle.childCount; i++)
        {
            AllWaterPuddles.Add(WaterIsland1Puddle.GetChild(i));
        }
        for (int i = 0; i < WaterIsland2Puddle.childCount; i++)
        {
            AllWaterPuddles.Add(WaterIsland2Puddle.GetChild(i));
        }
        for (int i = 0; i < WaterIsland3Puddle.childCount; i++)
        {
            AllWaterPuddles.Add(WaterIsland3Puddle.GetChild(i));
        }

        ChangeWaterState();

    }


    public void ChangeWaterState()  //устанавливаем сразу
    {
        switch (CurrentState)
        {
            case "Snow":
                WaterIsland.SetTexture("_Splat0", SnowGrass);
                WaterIsland.SetTexture("_DopSplat", SnowGrass);
                WaterIsland.SetFloat("_Alpha", 1);

                WaterPlants1.SetFloat("_Alpha", 1);
                WaterPlants2.SetFloat("_Alpha", 1);
                WaterPlants1.color = new Color(1,1,1,1);
                WaterPlants2.color = new Color(1, 1, 1, 1);

                WaterObject.SetFloat("_Alpha", 1);
                StoneB.SetFloat("_Alpha", 1);

                Ykazateli.SetFloat("_Alpha", 1);//текстура указателей
                LimitStone.SetFloat("_Alpha", 1);  //текстура камнец у границ
                                                   //   TV_statue.SetFloat("_Alpha", 1);  //текстура статуй
                for (int i = 0; i < GOD.DopEff.AllFoamScriptWater.Length; i++)  // включаем брызги
                    GOD.DopEff.AllFoamScriptWater[i].ChangeFoam(0);

                River.SetFloat("_Blend", 0);
                River1Limit.SetActive(false); // включаем границы реки
                River2Limit.SetActive(false);
                River3Limit.SetActive(false);
                MRiver1.isTrigger = false;
                MRiver2.isTrigger =false;
                MRiver3.isTrigger = false;

                Reeds.SetColor("_Color", new Color(1f, 1f, 1f, 1f)); // цвет камышей
                Reeds1.transform.localPosition = new Vector3(0,-6,0);
                Reeds2.transform.localPosition = new Vector3(0, -6, 0);
                Reeds1.SetActive(false);  // выключаем камыши
                Reeds2.SetActive(false);
                IceWaterFall1.SetActive(true);
                IceWaterFall2.SetActive(true);
                UnderWaterRes.SetActive(false);
                UnderWaterRes2.SetActive(false);

                River1.tag = "Untagged";
                River2.tag = "Untagged";
                River3.tag = "Untagged";

                for (int i = 0; i < AllWaterPuddles.Count; i++)
                    AllWaterPuddles[i].transform.localScale = new Vector3(0, 1, 0);

                GOD.FishingP.RippleOnWaterIsland.gameObject.SetActive(false); // отключаем места рыбалки
                for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //отключаем звук реки
                    GOD.Audio.Rivers[i].enabled = false;

               // Resources.UnloadUnusedAssets();
                break;
            case "Sun":
                WaterIsland.SetTexture("_Splat0", YellowGrass);
                WaterIsland.SetTexture("_DopSplat", YellowGrass);
                WaterIsland.SetFloat("_Alpha", 1);
                WaterPlants1.SetFloat("_Alpha", 0);
                WaterPlants2.SetFloat("_Alpha", 0);
                WaterPlants1.color = YellowColor;
                WaterPlants2.color = YellowColor;

                WaterObject.SetFloat("_Alpha", 0);
                StoneB.SetFloat("_Alpha", 0);

                Ykazateli.SetFloat("_Alpha", 0);//текстура указателей
                LimitStone.SetFloat("_Alpha", 0);  //текстура камнец у границ
                                                   //  TV_statue.SetFloat("_Alpha", 0);  //текстура статуй
                for (int i = 0; i < GOD.DopEff.AllFoamScriptWater.Length; i++)  // включаем брызги
                    GOD.DopEff.AllFoamScriptWater[i].ChangeFoam(0);

                River1.transform.position -= new Vector3(0, 2.7f, 0);
                River2.transform.position -= new Vector3(0, 2.7f, 0);
                River3.transform.position -= new Vector3(0, 2.7f, 0);
                River1.SetActive(false);
                River2.SetActive(false);
                River3.SetActive(false);
                River1Limit.SetActive(false); // включаем границы реки
                River2Limit.SetActive(false);
                River3Limit.SetActive(false);
                MRiver1.isTrigger = false;
                MRiver2.isTrigger = false;
                MRiver3.isTrigger = false;

                UnderWaterRes.SetActive(true);
                UnderWaterRes2.SetActive(true);

                Reeds1.transform.localPosition = new Vector3(0, 0, 0);
                Reeds2.transform.localPosition = new Vector3(0, 0, 0);
                Reeds1.SetActive(true);  // включаем камыши
                Reeds2.SetActive(true);
                Reeds.SetColor("_Color", new Color(0.94f, 0.82f, 0f, 1f)); // цвет камышей

                IceWaterFall1.SetActive(false);
                IceWaterFall2.SetActive(false);
                LeanWaterfall.SetActive(true);

                River1.tag = "Untagged";
                River2.tag = "Untagged";
                River3.tag = "Untagged";

                for (int i = 0; i < AllWaterPuddles.Count; i++)
                    AllWaterPuddles[i].transform.localScale = new Vector3(0, 1, 0);

                GOD.FishingP.RippleOnWaterIsland.gameObject.SetActive(false); // отключаем места рыбалки

                for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //отключаем звук реки
                    GOD.Audio.Rivers[i].enabled = false;

               // Resources.UnloadUnusedAssets();
                break;
            case "Water":
                WaterIsland.SetTexture("_Splat0", WaterGrass);
                WaterIsland.SetTexture("_DopSplat", WaterGrass);
                WaterIsland.SetFloat("_Alpha", 1);
                WaterPlants1.SetFloat("_Alpha", 0);
                WaterPlants2.SetFloat("_Alpha", 0);
                WaterObject.SetFloat("_Alpha", 0);
                WaterPlants1.color = new Color(1, 1, 1, 1);
                WaterPlants2.color = new Color(1, 1, 1, 1);

                Ykazateli.SetFloat("_Alpha", 0);//текстура указателей
                LimitStone.SetFloat("_Alpha", 0);  //текстура камнец у границ
                                                   //  TV_statue.SetFloat("_Alpha", 0);  //текстура статуй

                StoneB.SetFloat("_Alpha", 0);
                River.SetFloat("_Blend", 1);
                River1Limit.SetActive(true); // включаем границы реки
                River2Limit.SetActive(true);
                River3Limit.SetActive(true);
                MRiver1.isTrigger = true;
                MRiver2.isTrigger = true;
                MRiver3.isTrigger = true;
                Reeds1.transform.localPosition = new Vector3(0, 0, 0);
                Reeds2.transform.localPosition = new Vector3(0, 0, 0);
                Reeds.SetColor("_Color", new Color(1f, 1f, 1f, 1f)); // цвет камышей
                Reeds1.SetActive(true);  // включаем камыши
                Reeds2.SetActive(true);

                River1.tag = "Water";
                River2.tag = "Water";
                River3.tag = "Water";

                for (int i = 0; i < GOD.DopEff.AllFoamScriptWater.Length; i++)  // включаем брызги
                    GOD.DopEff.AllFoamScriptWater[i].ChangeFoam(3);

                for (int i = 0; i < AllWaterPuddles.Count; i++)
                    AllWaterPuddles[i].transform.localScale = new Vector3(0.5f, 1f, 0.5f);

                UnderWaterRes.SetActive(false);
                UnderWaterRes2.SetActive(false);

                for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //включаем звук реки
                    GOD.Audio.Rivers[i].enabled = true;

               // Resources.UnloadUnusedAssets();
                break;
        }
    }
    public void ChangeWeather(string state)
    {
        if (GOD.EnviromentContr.CanChange)
        {
            OldState = CurrentState;
            if ((state == "Snow") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;
                WaterIsland.SetTexture("_Splat0", WaterIsland.GetTexture("_DopSplat"));
                WaterIsland.SetTexture("_DopSplat", SnowGrass);
                WaterIsland.SetFloat("_Alpha", 1);
                IstextChange = false;
                IsPuddleStop = false;
                IsPlantTextChange = false;
                ShowStep = 0;
                TextChange = 0;
                StartCoroutine(Porydok1());

                StartCoroutine(ChangeTexture(-1));
                if (OldState != "Water")
                {
                    StartCoroutine(ChangePlantColor(-1));
                    StartCoroutine(ChangePlantTexture(1));
                }
                else
                {
                    ChnagePlantText();
                    GOD.DopEff.ChangeFoam(false);//брызги
                }
                

                StartCoroutine(ShowPuddles(-1));
                StartCoroutine(ChangeWaterOnIsland());
                StartCoroutine(ChangeIceWaterFall(-1));
                if(OldState == "Sun")
                    StartCoroutine(ChangeLeanWaterFall(1));
                River1.tag = "Untagged";
                River2.tag = "Untagged";
                River3.tag = "Untagged";
                StartCoroutine(MoveReeds(false));  // выключаем камыши
                GOD.FishingP.RippleOnWaterIsland.gameObject.SetActive(false); // отключаем места рыбалки

                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);
            }
            else if ((state == "Sun") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;
                WaterIsland.SetTexture("_Splat0", WaterIsland.GetTexture("_DopSplat"));
                WaterIsland.SetTexture("_DopSplat", YellowGrass);
                WaterIsland.SetFloat("_Alpha", 1);
                IstextChange = false;
                IsPuddleStop = false;
                IsPlantTextChange = false;
                ShowStep = 0;
                TextChange = 0;
                StartCoroutine(Porydok1());

                StartCoroutine(ChangeTexture(-1));
                if (OldState != "Water")
                {
                    StartCoroutine(ChangePlantColor(1));
                    StartCoroutine(ChangePlantTexture(-1));
                    StartCoroutine(MoveReeds(true));  // включаем камыши
                }
                else
                {
                    ChnagePlantText();
                    GOD.DopEff.ChangeFoam(false);//брызги
                }

                StartCoroutine(ShowPuddles(-1));
                StartCoroutine(ChangeWaterOnIsland());
                UnderWaterRes.SetActive(true);
                UnderWaterRes2.SetActive(true);

                if (OldState == "Snow")
                    StartCoroutine(ChangeIceWaterFall(1));
                StartCoroutine(ChangeLeanWaterFall(-1));

                River1.tag = "Untagged";
                River2.tag = "Untagged";
                River3.tag = "Untagged";

              
                GOD.DopEff.PlayerOnWater.gameObject.SetActive(false);
                GOD.FishingP.RippleOnWaterIsland.gameObject.SetActive(false); // отключаем места рыбалки

                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);
            }
            else if ((state == "Water") && (state != OldState))
            {
                CurrentState = state;
                GOD.EnviromentContr.CanChange = false;
                WaterIsland.SetTexture("_Splat0", WaterIsland.GetTexture("_DopSplat"));
                WaterIsland.SetTexture("_DopSplat", WaterGrass);
                WaterIsland.SetFloat("_Alpha", 1);
                IstextChange = false;
                IsPuddleStop = false;
                IsPlantTextChange = false;
                ShowStep = 0;
                TextChange = 0;
                ChnagePlantText();
                StartCoroutine(Porydok2());
                StartCoroutine(ShowPuddles(1));
                StartCoroutine(ChangeTexture(1));
                StartCoroutine(ChangeWaterOnIsland());

                River1.tag = "Water";
                River2.tag = "Water";
                River3.tag = "Water";

                if (OldState == "Snow")
                {
                    StartCoroutine(ChangeIceWaterFall(1));
                    StartCoroutine(MoveReeds(true));  // включаем камыши
                }
                if (OldState == "Sun")
                    StartCoroutine(ChangeLeanWaterFall(1));

                GOD.DopEff.ChangeFoam(true);//брызги
                River1Limit.SetActive(true); // включаем границы реки
                River2Limit.SetActive(true);
                River3Limit.SetActive(true);
                MRiver1.isTrigger = true;
                MRiver2.isTrigger = true;
                MRiver3.isTrigger = true;
                River1.SetActive(true);
                River2.SetActive(true);
                River3.SetActive(true);

                GOD.EnviromentContr.PlayerOnIsland(GOD.EnviromentContr.CurrentIslandScript);
            }
            GOD.EnviromentContr.CurrentState = CurrentState;
        }
    }

    IEnumerator MoveReeds(bool b)//двигаем камыши
    {
        if (b) //поднимаем камыши
        {
            Reeds1.SetActive(true);
            Reeds2.SetActive(true);
            while (Reeds1.transform.localPosition.y < 0)
            {
                Reeds1.transform.localPosition += new Vector3(0, 0.1f, 0);
                Reeds2.transform.localPosition += new Vector3(0, 0.1f, 0);
                yield return new WaitForSeconds(0.1f);
            }
            Reeds1.transform.localPosition = new Vector3(0, 0, 0);
            Reeds2.transform.localPosition = new Vector3(0, 0, 0);
        }
        else//опускаем камыши
        {
            while (Reeds1.transform.localPosition.y > -6)
            {
                Reeds1.transform.localPosition -= new Vector3(0, 0.1f, 0);
                Reeds2.transform.localPosition -= new Vector3(0, 0.1f, 0);
                yield return new WaitForSeconds(0.1f);
            }
            Reeds1.transform.localPosition = new Vector3(0, -6, 0);
            Reeds2.transform.localPosition = new Vector3(0, -6, 0);
            Reeds1.SetActive(false);
            Reeds2.SetActive(false);
        }
    }
    IEnumerator Porydok1()
    {
        while (ShowStep < 1)
        {
            if (IstextChange && WaterChangedPosition)
            {
                GOD.EnviromentContr.UpdateCanChange();
                if (CurrentState == "Snow")
                {
                    for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //отключаем звук реки
                        GOD.Audio.Rivers[i].enabled = false;
                }
                if (CurrentState == "Sun")
                {
                    LeanWaterfall.SetActive(true);
                }
                ShowStep++;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator Porydok2()
    {
        while (ShowStep < 1)
        {
            if (IstextChange && IsPuddleStop && WaterChangedPosition)
            {
                GOD.EnviromentContr.UpdateCanChange();
                GOD.DopEff.PlayerOnWater.gameObject.SetActive(true);
                GOD.FishingP.RippleOnWaterIsland.gameObject.SetActive(true); // включаем места рыбалки
                UnderWaterRes.SetActive(false);
                UnderWaterRes2.SetActive(false);
                ShowStep++;
            }
            yield return new WaitForFixedUpdate();
        }
    }
    //ТЕКСТУРЫ_______________________________________________________________________________________________________________
    IEnumerator ChangeTexture(int x)         // меняем текстуры
    {
        while (WaterIsland.GetFloat("_Alpha") > 0f)
        {
            WaterIsland.SetFloat("_Alpha", WaterIsland.GetFloat("_Alpha") - 0.01f);
            if (WaterIsland.GetFloat("_Alpha") <= 0)
                IstextChange = true;
            yield return new WaitForSeconds(0.1f);
        }
    }
    //РАСТЕНИЯ_______________________________________________________________________________________________________________
    void ChnagePlantText()
    {
        if (CurrentState == "Snow")
        {
            StartCoroutine(ChangePlantTexture(1));
        }
        else if (CurrentState == "Sun")
        {
            WaterPlants1.SetFloat("_Alpha", 0);
            WaterPlants2.SetFloat("_Alpha", 0);
            StartCoroutine(ChangePlantColor(1));
        }
        else
        {
            if(OldState == "Sun")
                StartCoroutine(ChangePlantColor(-1));
            else
                StartCoroutine(ChangePlantTexture(-1));
        }

        //Resources.UnloadUnusedAssets();
    }

    IEnumerator ChangePlantColor(int x)
    {
        Color newcolor = WaterPlants1.color;
        if (x < 0)
        {
            while (newcolor.b < 1f)
            {
                if (newcolor.r < 1f)
                    newcolor = new Color(newcolor.r + 0.0005f, newcolor.g, newcolor.b);
                if (newcolor.g < 1)
                    newcolor = new Color(newcolor.r, newcolor.g + 0.02f, newcolor.b);
                if (newcolor.b < 1)
                    newcolor = new Color(newcolor.r, newcolor.g, newcolor.b + 0.03f);
                WaterPlants1.color = newcolor;
                WaterPlants2.color = newcolor;
                yield return new WaitForSeconds(0.1f);
            }
            WaterPlants1.color = new Color(1, 1, 1, 1);
            WaterPlants2.color = new Color(1, 1, 1, 1);
        }
        else
        {
            while (newcolor.b > 0.27f)
            {
                if (newcolor.r > 0.97f)
                    newcolor = new Color(newcolor.r - 0.0005f, newcolor.g, newcolor.b);
                if (newcolor.g > 0.6f)
                    newcolor = new Color(newcolor.r, newcolor.g - 0.02f, newcolor.b);
                if (newcolor.b > 0.27f)
                    newcolor = new Color(newcolor.r, newcolor.g, newcolor.b - 0.03f);
                WaterPlants1.color = newcolor;
                WaterPlants2.color = newcolor;
                yield return new WaitForSeconds(0.1f);
            }
            WaterPlants1.color = YellowColor;
            WaterPlants2.color = YellowColor;
        }
    }
    IEnumerator ChangePlantTexture(int x)
    {
        if (x < 0)
        {
            while (WaterPlants1.GetFloat("_Alpha") > 0f)
            {
                float z = WaterPlants1.GetFloat("_Alpha") - 0.01f;
                WaterPlants1.SetFloat("_Alpha", z);
                WaterPlants2.SetFloat("_Alpha", z);
                if (OldState == "Snow")
                {
                    WaterObject.SetFloat("_Alpha", z);
                    StoneB.SetFloat("_Alpha",z);
                    Ykazateli.SetFloat("_Alpha", z);
                    LimitStone.SetFloat("_Alpha", z);
                 //   TV_statue.SetFloat("_Alpha", z);  //текстура статуй

                }
                yield return new WaitForSeconds(0.1f);
            }
            if (WaterPlants1.GetFloat("_Alpha") <= 0)
                IsPlantTextChange = true;
        }
        else
        {
             while (WaterPlants1.GetFloat("_Alpha") < 0.5f)
             {
                 float z = WaterPlants1.GetFloat("_Alpha") + 0.01f;
                 WaterPlants1.SetFloat("_Alpha", z);
                 WaterPlants2.SetFloat("_Alpha", z);
                 if (CurrentState == "Snow")
                 {
                     WaterObject.SetFloat("_Alpha", z);
                     StoneB.SetFloat("_Alpha", z);
                     Ykazateli.SetFloat("_Alpha", z);
                     LimitStone.SetFloat("_Alpha", z);
                   // TV_statue.SetFloat("_Alpha", z);  //текстура статуй
                }
                 yield return new WaitForSeconds(0.1f);
             }
            if (WaterPlants1.GetFloat("_Alpha") >= 0.5f)
                IsPlantTextChange = true;
        }
    }

 

    //ЛУЖИ______________________________________________________________________________________________________________________
    IEnumerator ShowPuddles(int x)         // постепенно убираем\показываем лужи
    {
        if (AllWaterPuddles.Count > 0)
        {
            if (x < 0)
            {
                while (AllWaterPuddles[0].localScale.x > 0)
                {
                    for (int i = 0; i < AllWaterPuddles.Count; i++)
                    {
                        AllWaterPuddles[i].localScale -= new Vector3(0.01f, 0, 0.01f);
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                if (AllWaterPuddles[0].localScale.x <= 0)
                {
                    IsPuddleStop = true;
                    for (int i = 0; i < AllWaterPuddles.Count; i++)
                    {
                        AllWaterPuddles[i].gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                for (int i = 0; i < AllWaterPuddles.Count; i++)
                {
                    AllWaterPuddles[i].gameObject.SetActive(true);
                }
                while (AllWaterPuddles[0].localScale.x < 0.5f)
                {
                    for (int i = 0; i < AllWaterPuddles.Count; i++)
                    {
                        AllWaterPuddles[i].localScale += new Vector3(0.01f, 0, 0.01f);
                    }
                    yield return new WaitForSeconds(0.1f);
                }
                if (AllWaterPuddles[0].localScale.x >= 0.5f)
                    IsPuddleStop = true;
            }
        }
        else
            IsPuddleStop = true;
    }
    //ВОДА______________________________________________________________________________________________________________________
    IEnumerator ChangeWaterOnIsland()         // меняем воду
    {
        //print(CurrentState + " " + OldState);
        WaterChanged = false;
        WaterStep = 0;
        WaterChangedPosition = true;
        if (CurrentState == "Snow" && OldState == "Water")
        {
            StartCoroutine(ChangeWaterText(-1));
        }
        else if (CurrentState == "Water" && OldState == "Snow")
        {
            StartCoroutine(ChangeWaterText(1));
        }
        else if (CurrentState == "Sun" && OldState == "Water")
        {
            StartCoroutine(ChangeWaterPosition(-1));
        }
        else if (CurrentState == "Water" && OldState == "Sun")
        {
            StartCoroutine(ChangeWaterPosition(1));
        }
        else if (CurrentState == "Snow" && OldState == "Sun")
        {
            River1.SetActive(true);
            River2.SetActive(true);
            River3.SetActive(true);
            UnderWaterRes.SetActive(false);
            UnderWaterRes2.SetActive(false);
            LeanWaterfall.SetActive(false);
            StartCoroutine(ChangeWaterPosition(1));
            while (WaterStep == 0)
            {
                if (WaterChanged)
                {
                    StartCoroutine(ChangeWaterText(-1));
                    WaterStep = 1;
                    WaterChanged = false;
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
        else if (CurrentState == "Sun" && OldState == "Snow")
        {
            WaterChangedPosition = false;
            StartCoroutine(ChangeWaterText(1));
            while (WaterStep == 0)
            {
                if (WaterChanged)
                {
                    StartCoroutine(ChangeWaterPosition(-1));
                    WaterStep = 1;
                    WaterChanged = false;

                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }


    IEnumerator ChangeWaterText(int x)        // меняем воду на лед и обратно
    {
        if (x > 0)
        {
            for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //включаем звук реки
                GOD.Audio.Rivers[i].enabled = true;
            River1Limit.SetActive(true); // включаем границы реки
            River2Limit.SetActive(true);
            River3Limit.SetActive(true);
            MRiver1.isTrigger = true;
            MRiver2.isTrigger = true;
            MRiver3.isTrigger = true;
            while (River.GetFloat("_Blend") < 1f)
            {
                River.SetFloat("_Blend", River.GetFloat("_Blend") + 0.01f);
                for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //включаем звук реки
                    GOD.Audio.Rivers[i].volume += 0.01f;
                yield return new WaitForSeconds(0.1f);
            }
            if (River.GetFloat("_Blend") >= 1)
            {
                WaterChanged = true;
            }
        }
        else
        {
            while (River.GetFloat("_Blend") > 0f)
            {
                if (GOD.Audio.Rivers[0].volume > 0)
                {
                    for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //отключаем звук реки
                        GOD.Audio.Rivers[i].volume -= (GOD.Settings.SoundVolume/100);
                }
                River.SetFloat("_Blend", River.GetFloat("_Blend") - 0.01f);
                if (River.GetFloat("_Blend") <= 0)
                {
                    River1Limit.SetActive(false); // включаем границы реки
                    River2Limit.SetActive(false);
                    River3Limit.SetActive(false);
                    MRiver1.isTrigger = false;
                    MRiver2.isTrigger = false;
                    MRiver3.isTrigger = false;
                }
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    IEnumerator ChangeWaterPosition(int x)        // меняем воду на засуху
    {
        if (x < 0)
        {
            while (River1.transform.localPosition.y > -3)
            {
                River1.transform.localPosition -= new Vector3(0, 0.04f, 0);
                River2.transform.localPosition -= new Vector3(0, 0.04f, 0);
                River3.transform.localPosition -= new Vector3(0, 0.04f, 0);
                if (GOD.Audio.Rivers[0].volume > 0)
                {
                    for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //отключаем звук реки
                        GOD.Audio.Rivers[i].volume -= (GOD.Settings.SoundVolume/100);
                }
                Reeds.SetColor("_Color", Reeds.GetColor("_Color") - new Color(0.001f, 0.003f, 0.02f, 0f)); // цвет камышей
                yield return new WaitForSeconds(0.1f);
            }
            if (River1.transform.localPosition.y <= -3)
            {
                River1.SetActive(false);
                River2.SetActive(false);
                River3.SetActive(false);
                River1Limit.SetActive(false); // выключаем границы реки
                River2Limit.SetActive(false);
                River3Limit.SetActive(false);
                MRiver1.isTrigger = false;
                MRiver2.isTrigger = false;
                MRiver3.isTrigger = false;
                WaterChangedPosition = true;
            }

        }
        else
        {
            River1Limit.SetActive(true); // включаем границы реки
            while (River1.transform.localPosition.y < 1)
            {
                River1.transform.localPosition += new Vector3(0, 0.08f, 0);
                River2.transform.localPosition += new Vector3(0, 0.08f, 0);
                River3.transform.localPosition += new Vector3(0, 0.08f, 0);


                if (GOD.Audio.Rivers[0].volume < GOD.Settings.SoundVolume)
                {
                    for (int i = 0; i < GOD.Audio.Rivers.Length; i++)    //включаем звук реки
                        GOD.Audio.Rivers[i].volume += GOD.Settings.SoundVolume/20;
                }

                Color c = Reeds.GetColor("_Color");
                if (c.r < 1)
                    c.r += 0.01f;
                if (c.g < 1)
                    c.g += 0.01f;
                if (c.b < 1)
                    c.b += 0.06f;
                Reeds.SetColor("_Color", c); // цвет камышей
                yield return new WaitForSeconds(0.1f);
            }
            if (River1.transform.localPosition.y >= 1f)
            {
                UnderWaterRes.SetActive(false);
                UnderWaterRes2.SetActive(false);
                WaterChanged = true;
                WaterChangedPosition = true;
            }
        }
    }

    IEnumerator ChangeIceWaterFall(int x)  // появление исчезание ледяного водопада
    {
        MountainScript MScript = IceWaterFall1.GetComponent<MountainScript>();
        if (x > 0)
        {
            MScript.SetMountain(false);
            Color c = IceWaterFallMat.GetColor("_Color");
            IceWaterFallMat.SetColor("_Color", new Color(c.r, c.g, c.b, 1f));
            while (IceWaterFallMat.GetColor("_Color").a > 0f)
            {
                IceWaterFallMat.SetColor("_Color", IceWaterFallMat.GetColor("_Color") - new Color(0, 0, 0, 0.02f));
                yield return new WaitForSeconds(0.1f);
            }
            if (IceWaterFallMat.GetColor("_Color").a <= 0)
            {
                IceWaterFall1.SetActive(false);
                     IceWaterFall2.SetActive(false);
            }
        }
        else
        {
            MScript.SetMountain(false);
            Color c = IceWaterFallMat.GetColor("_Color");
            IceWaterFallMat.SetColor("_Color", new Color(c.r, c.g, c.b, 0f));
            c = IceWaterFallMat.GetColor("_Color");
            IceWaterFall1.SetActive(true);
            IceWaterFall2.SetActive(true);
            while (IceWaterFallMat.GetColor("_Color").a < 1f)
            {
                IceWaterFallMat.SetColor("_Color", IceWaterFallMat.GetColor("_Color") + new Color(0, 0, 0, 0.01f));
                yield return new WaitForSeconds(0.1f);
            }
            if (IceWaterFallMat.GetColor("_Color").a >= 1)
            {
                MScript.SetMountain(true);
            }

        }
    }

    IEnumerator ChangeLeanWaterFall(int x)  // появление исчезание лиан
    {
        if (x > 0)
        {
            LeanWaterfall.GetComponent<MountainScript>().BotBtn.SetActive(false);
            LeanWaterfall.GetComponent<MountainScript>().TopBtn.SetActive(false);
            Color c = LeanWaterFallMat.GetColor("_Color");
            LeanWaterFallMat.SetColor("_Color", new Color(c.r, c.g, c.b, 1f));
            while (LeanWaterFallMat.GetColor("_Color").a > 0f)
            {
                LeanWaterFallMat.SetColor("_Color", LeanWaterFallMat.GetColor("_Color") - new Color(0, 0, 0, 0.02f));
                yield return new WaitForSeconds(0.1f);
            }
            if (LeanWaterFallMat.GetColor("_Color").a <= 0)
            {
                LeanWaterfall.SetActive(false);
            }
        }
        else
        {
            LeanWaterfall.GetComponent<MountainScript>().BotBtn.SetActive(false);
            LeanWaterfall.GetComponent<MountainScript>().TopBtn.SetActive(false);
            Color c = LeanWaterFallMat.GetColor("_Color");
            LeanWaterFallMat.SetColor("_Color", new Color(c.r, c.g, c.b, 0f));
            c = LeanWaterFallMat.GetColor("_Color");
            LeanWaterfall.SetActive(true);;
            while (LeanWaterFallMat.GetColor("_Color").a < 1f)
            {
                LeanWaterFallMat.SetColor("_Color", LeanWaterFallMat.GetColor("_Color") + new Color(0, 0, 0, 0.01f));
                yield return new WaitForSeconds(0.1f);
            }
            if (LeanWaterFallMat.GetColor("_Color").a >= 1)
            {
                LeanWaterfall.GetComponent<MountainScript>().BotBtn.SetActive(true);
                LeanWaterfall.GetComponent<MountainScript>().TopBtn.SetActive(true);
            }

        }
    }

}
