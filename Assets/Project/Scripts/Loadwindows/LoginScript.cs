﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
#if !NO_FACEBOOK
using Facebook.Unity;
#endif
using UnityEngine.UI;
#if !NO_VK
using com.playGenesis.VkUnityPlugin;
using com.playGenesis.VkUnityPlugin.MiniJSON;
#endif

public class LoginScript : MonoBehaviour
{

    public AudioSource Sound, Music;
    public AudioClip Btn;
    public GameObject VkontakteBtn, FacebookBtn, SkipBtn;
    public Text FacebookTxt, VkontakteTxt, VxodTxt;
    public GameObject Gem1, Gem2;
    private AsyncOperation async;

    void Start()
    {
        SceneManager.LoadScene("Menu");
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        // 

#if (NO_FACEBOOK && NO_VK)
        SkipBtn.SetActive(false);
        //  SceneManager.LoadScene("Menu");
#endif

        if (PlayerPrefs.HasKey("ChibiMusicVolume"))
            Music.volume = PlayerPrefs.GetFloat("ChibiMusicVolume");
        if (PlayerPrefs.HasKey("ChibiSoundVolume"))
            Sound.volume = PlayerPrefs.GetFloat("ChibiSoundVolume");
        if (PlayerPrefsX.GetBool("ChibiFacebook"))
            Gem1.SetActive(false);
        if (PlayerPrefsX.GetBool("ChibiVKontakte"))
            Gem2.SetActive(false);

        if (PlayerPrefs.HasKey("ChibiLanguage"))
            Localization.instance.SetLanguage(PlayerPrefs.GetString("ChibiLanguage"));
        else
        {
            Localization.instance.SetLanguage("");
            PlayerPrefs.SetString("ChibiLanguage", Localization.instance.CurrentLanguage);
        }

        if (PlayerPrefs.HasKey("ChibiMusicVolume"))
        {
            Sound.volume = PlayerPrefs.GetFloat("ChibiMusicVolume");
            Music.volume = Sound.volume;
        }
        else
        {
            Sound.volume = 0.5f;
            Music.volume = 0.5f;
            PlayerPrefs.SetFloat("ChibiMusicVolume", Sound.volume);
        }

#if !NO_VK
        VkontakteTxt.text = Localization.instance.getTextByKey("#Login") + "·" + Localization.instance.getTextByKey("#VKontakte");
        VkontakteBtn.SetActive(true);
         if (VkApi.VkApiInstance.IsUserLoggedIn)
            LoadGame();
#endif
        VxodTxt.text = Localization.instance.getTextByKey("#Skip");
#if !NO_FACEBOOK
        FacebookBtn.SetActive(true);
         FacebookTxt.text = Localization.instance.getTextByKey("#Login") + "·" + Localization.instance.getTextByKey("#Facebook");
        FB.Init();
        if (FB.IsLoggedIn)
            LoadGame();
#endif
    }

    public void FBLogin()
    {
        Sound.PlayOneShot(Btn);
        // List<string> Permisions = new List<string> { "public_profile", "email", "user_friends" };
#if !NO_FACEBOOK
        if (!FB.IsLoggedIn)
            FB.LogInWithPublishPermissions(null, AfterFacebook);
#endif
    }
#if !NO_FACEBOOK
    void AfterFacebook(ILoginResult r)
    {
        if (FB.IsLoggedIn)
        {
            if(!PlayerPrefsX.GetBool("ChibiFacebook"))
                Balance.Gem = Balance.Gem + 15;
            PlayerPrefsX.SetBool("ChibiFacebook", true);
            LoadGame();
        }
    }
#endif
    IEnumerator StartLoad()
    {
        async = SceneManager.LoadSceneAsync("Menu");
        yield return true;
        async.allowSceneActivation = false;
    }
#if !NO_VK
    public void VKLogin()
    {
        Sound.PlayOneShot(Btn);
        VkApi.VkApiInstance.Login();
        StartCoroutine(AfterVKLogin());
    }
    IEnumerator AfterVKLogin()
    {
        while (!VkApi.VkApiInstance.IsUserLoggedIn)
        {
            yield return new WaitForFixedUpdate();
        }
        if (!PlayerPrefsX.GetBool("ChibiVKontakte"))
            Balance.Gem = Balance.Gem + 15;
        PlayerPrefsX.SetBool("ChibiVKontakte", true);
        LoadGame();
    }
#endif
    public void Skip()
    {
        Sound.PlayOneShot(Btn);
        LoadGame();
    }
    public void LoadGame()
    {
        async.allowSceneActivation = true;
    }
}
