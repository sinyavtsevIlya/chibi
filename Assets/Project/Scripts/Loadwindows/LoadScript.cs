﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScript : MonoBehaviour {

    private AsyncOperation async;
    public AudioSource Music;
    public static LoadScript instanse;
    void Start()
    {
        if(instanse==null)
            instanse = this;

        Time.timeScale = 1f;
        if (PlayerPrefs.HasKey("ChibiMusicVolume"))
            Music.volume = PlayerPrefs.GetFloat("ChibiMusicVolume");

 
            IslandLoadScene();

    }
    public void IslandLoadScene()
    {
        switch (PlayerPrefs.GetString("ChibiLevel"))
        {
            case "Game":
                PlayerPrefs.SetString("ChibiLevel", "");
                SceneManager.LoadSceneAsync("Game");
                break;
            case "Menu":
                QualitySettings.SetQualityLevel(2, true);
                PlayerPrefs.SetString("ChibiLevel", "");
                SceneManager.LoadSceneAsync("Menu");
                break;
            case "Tutorial":
                PlayerPrefs.SetString("ChibiLevel", "");
                SceneManager.LoadSceneAsync("Tutorial");
                break;
            default:
                PlayerPrefs.SetString("ChibiLevel", "");
                SceneManager.LoadSceneAsync("Menu");
                break;
        }
    }

}
