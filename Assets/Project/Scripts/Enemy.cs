﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public GameManager GOD;
    AI AiScript;
    public Animator m_Animator;

    public bool IsPlayer = false;                    // сейчас цель игрок? 
    public Transform EnemyBase;                      // точка к которой привязан моб
   // public Transform[] points = new Transform[4];        //  цели между которыми бродит
     public Vector3[] points = new Vector3[4];        //  цели между которыми бродит

    // ПАРАМЕТРЫ ВРАГОВ
    bool Alive = true;
    public string EnemyTag;
    public bool IsAgressive;                          // агрится ли на игрока сразу                    
    public int Damage;                            // урон
    public float maxHP;                               // здоровье максимальное
    public float HP;                               // здоровье текущее
    public float Evasion;                         // уклонения
    public float AgressiveDistance;               // дистанция, с которой агрится
    public float EnemyRun;                  // скорость когда возвращается на базу
    public float EnemyUsialSpeed;                // скорость обычная
    public float EnemySpeed;                    // текущая скорость
    public string[] EnemyLut;                     // что падает с животного

    public enum EnemyState  //состояние животных
    {
        idle,
        walk,
        agr, //сагривается
        pursuit,
        returnHome
    }
    public EnemyState State = EnemyState.idle;
    public float Timer = 0; //таймер для идла
    float PeaceTimer = 0; //таймер для покоя- смена шагов и идлов

    public int RecoveryTime = 0;                            // время восстановления врага
    public bool NowAttack = false;            // сейчас анимация атаки

    public Vector3 OldPositon;

    //Эффекты животных
    public ParticleSystem At1, At2, At3;
    ParticleSystem CurrentEffect;

    //Звуки животных
    public AudioSource EnemySound, EnemyWalk;
    AudioClip CurrentAttack;
    public AudioClip Attack1, Attack2, Attack3, Death, Angry, Idle;
    bool OneAgr = false;

    public void Init()
    {
        AiScript = GetComponent<AI>();
        AiScript.Init();
        SetEnemy();
        tag = "Enemy";
        m_Animator = GetComponent<Animator>();
        EnemySound = GetComponent<AudioSource>();
        EnemyWalk = transform.GetChild(2).GetComponent<AudioSource>();
        GOD.Audio.SetEnemiesSounds(this);
        points[0] = EnemyBase.position + new Vector3(20, 0, 0);
        points[1] = EnemyBase.position + new Vector3(0, 0, 20);
        points[2] = EnemyBase.position + new Vector3(0, 0, -20);
        points[3] = EnemyBase.position + new Vector3(-20, 0, 0);
        AiScript.targetPosition = points[Random.Range(0, 4)];
        //GameObject g = transform.parent.parent.parent.gameObject;
        AiScript.WeGoing = true;
        OldPositon = transform.position;
    }

    public void UpdateEnemy()
    {
        Alive = true;
        tag = "Enemy";
        RecoveryTime = 0;
        transform.position = EnemyBase.position;
        gameObject.SetActive(true);
        AiScript.enabled = true;
        m_Animator.enabled = true;
        m_Animator.Play("Idle 1");
        GetComponent<CapsuleCollider>().isTrigger = false;
        IsPlayer = false;
        // Init();
        AiScript.Init();
        SetEnemy();
        points[0] = EnemyBase.position + new Vector3(20, 0, 0);
        points[1] = EnemyBase.position + new Vector3(0, 0, 20);
        points[2] = EnemyBase.position + new Vector3(0, 0, -20);
        points[3] = EnemyBase.position + new Vector3(-20, 0, 0);
        AiScript.targetPosition = points[Random.Range(0, 4)];
        AiScript.WeGoing = true;
    }

    void SetNewEnemyState()
    {
        switch (State)
        {
            case EnemyState.idle:
                int z = Random.Range(0, 100);
                if (z < 60)
                {
                    m_Animator.Play("Idle 2");
                    EnemySound.PlayOneShot(Idle);
                }
                EnemySpeed = 0;
               m_Animator.SetInteger("Speed", (int)EnemySpeed);
                if (EnemyWalk && EnemyWalk.isPlaying && name != "Ozz")//звук ходьбы
                    EnemyWalk.Stop();
                break;
            case EnemyState.walk:
                int x = Random.Range(0, 4);
                AiScript.targetPosition = points[x];                  // поменяли направление
                AiScript.WeGoing = true;
                EnemySpeed = EnemyUsialSpeed;
                m_Animator.SetInteger("Speed", (int)EnemySpeed);
                if (EnemyWalk && !EnemyWalk.isPlaying && name != "Ozz")//звук ходьбы
                      EnemyWalk.Play();
                    break;
            case EnemyState.returnHome:
                AiScript.targetPosition = EnemyBase.position;
                AiScript.WeGoing = true;
                IsPlayer = false;
                EnemySpeed = EnemyRun;
                m_Animator.SetInteger("Speed", (int)EnemySpeed);
                break;
            case EnemyState.agr:
                AiScript.WeGoing = false;                // сагриваеймся
                EnemySpeed = 0;
                m_Animator.SetInteger("Speed", (int)EnemySpeed);
                m_Animator.Play("Angry");
                EnemySound.Stop();
                EnemySound.PlayOneShot(Angry);
                this.transform.LookAt(GOD.Player.transform);
                AiScript.Path.Clear();
                if (name == "Ozz" && EnemyWalk && EnemyWalk.isPlaying)//звук ходьбы
                    EnemyWalk.Stop();
                break;
            case EnemyState.pursuit:
                if (AiScript.targetPosition != GOD.Player.transform.position)
                {
                    AiScript.targetPosition = GOD.Player.transform.position;
                    AiScript.WeGoing = true;
                    EnemySpeed = EnemyRun;
                    m_Animator.SetInteger("Speed", (int)EnemySpeed);
                }
                break;
        }
    }

    void Update()
    {
        if (Alive)//если животное живо
        {
            if (State != EnemyState.idle && State != EnemyState.agr && State != EnemyState.returnHome && Vector3.SqrMagnitude(transform.position- EnemyBase.position) > (AgressiveDistance*AgressiveDistance) * 25)  // если нпс отдалился от центральное точки, то вернуться домой
            {
                OutPlayer();
            }

            if (!IsPlayer && State != EnemyState.returnHome)
            {
                if (IsAgressive)                 // поведение агрессивныхх мобов
                {
                    if (GOD.PlayerContr.IsAlive && Vector3.SqrMagnitude(GOD.Player.transform.position - this.transform.position) < AgressiveDistance*AgressiveDistance)            // если игрок на расстоянии агра, преследовать его
                        GetDamage();
                }
                if (State != EnemyState.returnHome && State != EnemyState.agr)//если не гоняемся за игроком, то покой
                {
                    if (PeaceTimer == 0)  //меняем цель животного
                    {
                        if (GOD.IsTutorial)
                            State = EnemyState.idle;
                        else
                        {
                            int x = Random.Range(0, 6);
                            if (x < 4)
                                State = EnemyState.walk;
                            else
                                State = EnemyState.idle;
                        }
                        Timer = 0;
                    }
                    PeaceTimer += Time.deltaTime;
                    if (PeaceTimer >= 10)
                        PeaceTimer = 0;
                }
            }
            switch (State)
            {
                case EnemyState.idle://стоим
                    if (Timer == 0)
                        SetNewEnemyState();
                    Timer += Time.deltaTime;
                    if (Timer >= 5f)
                        Timer = 0;
                    break;
                case EnemyState.walk://бродим
                    if (Timer == 0)
                        SetNewEnemyState();
                    Timer += Time.deltaTime;
                    if (Timer >= 5f)
                        Timer = 0;
                    break;
                case EnemyState.returnHome://возвращаемся на респ
                    if (Vector3.SqrMagnitude(transform.position- EnemyBase.position) < 25)             // когда вернулись на точку снова бродим
                    {
                        State = EnemyState.idle;
                        SetNewEnemyState();
                        if (name == "Ozz" && EnemyWalk && EnemyWalk.isPlaying)//звук ходьбы
                            EnemyWalk.Stop();
                    }
                    else
                        State = EnemyState.returnHome;
                    break;
                case EnemyState.pursuit://преследуем
                    SetNewEnemyState();
                    break;
            }
        }
    }

    public void GetDamage()
    {
        if (State!=EnemyState.pursuit && State!=EnemyState.agr)
        {
            State = EnemyState.agr;
            SetNewEnemyState();
        }
    }
    public void StartPursuit()
    {
        if (GOD.PlayerContr.IsAlive)
        {
            State = EnemyState.pursuit;
            IsPlayer = true;
            if (name == "Ozz"&& EnemyWalk && !EnemyWalk.isPlaying)//звук ходьбы
                EnemyWalk.Play();
        }
        else
        {
            State = EnemyState.idle;
            SetNewEnemyState();
            IsPlayer = false;
        }
    }
    public void OutPlayerDead()//отагриваемся после убийства
    {
        IsPlayer = false;
        State = EnemyState.agr;
        SetNewEnemyState();
    }
    public void OutPlayer()//отагриваемся 
    {
        HP = maxHP;
        State = EnemyState.returnHome;
        SetNewEnemyState();
    }
    public void StopSound()
    {
        EnemySound.Stop();
    }
    public void EnemyAttack()
    {
        if (!NowAttack)
        {
                int z = 1;
                if (name == "Bear")
                    z = Random.Range(0, 61);
                else
                    z = Random.Range(0, 91);

                if (z > 0 && z < 30)
                {
                    CurrentEffect = At1;
                    m_Animator.Play("Attack 1");
                    CurrentAttack = Attack1;
                }
                else if (z >= 30 && z < 60)
                {
                    CurrentEffect = At2;
                    m_Animator.Play("Attack 2");
                    CurrentAttack = Attack2;
                }
                else if (z >= 60 && z < 90)
                {
                    CurrentEffect = At3;
                    m_Animator.Play("Attack 3");
                    CurrentAttack = Attack3;
                }
            NowAttack = true;
        }
    }
    public void SetDamage()
    {
        NowAttack = false;
        if (CurrentEffect)
        {
            CurrentEffect.Stop();
            CurrentEffect.Play();
        }
        EnemySound.PlayOneShot(CurrentAttack);
        if (GOD.Player!=null && Vector3.SqrMagnitude(GOD.Player.transform.position- transform.position) < 25f)
        {
            if (!GOD.IsTutorial || GOD.PlayerContr.HP > 50)
            {
                if (GOD.PlayerContr.InBuilding)
                    GOD.PlayerInter.CloseBuildings(); //прерываем строительство
                GOD.PlayerContr.HP = GOD.PlayerContr.HP - (Damage - (Damage * GOD.PlayerContr.Armor) / 100);
                GOD.PlayerInter.ShowIndicators();
                if(!GOD.PlayerAnimation.ImAttack && !GOD.Audio.Sound.isPlaying)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.Pain);
                GOD.PlayerInter.ChangeArmorDurability();
            }
        }
    }


   
    public void AttackStop()
    {
        NowAttack = false;
    }
   /* public void EnemyHit()
    {
        if (Alive && State == EnemyState.pursuit)
        {
            float x = 100;
            x = Random.Range(0, 101);
            if (x > 80)
            {
                // if(GOD.PlayerAnimation.AttackStep==4)
                //m_Animator.Play("Hited 4");
                switch (GOD.PlayerAnimation.AttackStep)
                {
                    case 1:
                        m_Animator.Play("Hited 1");
                        break;
                    case 2:
                        m_Animator.Play("Hited 2");
                        break;
                    case 3:
                        m_Animator.Play("Hited 3");
                        break;
                    case 4:
                        m_Animator.Play("Hited 4");
                        break;
                }
            }
        }
    }*/
    public void EnemyDeath()       // смерть врага
    {
        if (Alive)
        {
            DeadState();
            m_Animator.Play("Death");
            if (GOD.NT.NearTargetObject == gameObject)
                GOD.NT.TargetsAround.Remove(transform);
            EnemySound.PlayOneShot(Death);
            for (int i =0; i< EnemyLut.Length; i++)
            {
                GOD.PlayerInter.Lut(this.gameObject, EnemyLut[i], i*2, i*2, true);
            }
            GOD.DopEff.Soul.transform.SetParent(transform);
            GOD.DopEff.Soul.transform.position = transform.position+new Vector3(0,2,0);
            StopAllCoroutines();
            StartCoroutine(DeadEnemy());
            if (GOD.IsTutorial)
            {
                GOD.Tutor.TutorialStep =19;
                GOD.Tutor.ShowTutorialStepTurn();
                transform.GetChild(4).gameObject.SetActive(false);
            }
        }
    }
    public void DeadState()
    {
        Alive = false;
        tag = "Ignored";
        EnemySound.Stop();
        if (EnemyWalk)
            EnemyWalk.Stop();
        GetComponent<AI>().enabled = false;
    }
    public void Dead()
    {
        if (!GOD.IsTutorial)
        {
            RecoveryTime = GOD.TG.ToSecond() + GOD.PlayerInter.KD;
            GOD.RecoveryEnemy.Add(this);
            m_Animator.Play("Idle 1");
            transform.position = EnemyBase.position;
            AiScript.Path.Clear();
        }
        gameObject.SetActive(false);
    }
    IEnumerator DeadEnemy()
    {
        yield return new WaitForSeconds(3);
        float y = transform.position.y;
        while(transform.position.y>(y-2))
        {
            transform.position -= new Vector3(0, 0.01f, 0);
            yield return new WaitForFixedUpdate();

        }
        Dead();
        
    }
    public void AfterDeath()
    {
        m_Animator.enabled = false;
        GetComponent<CapsuleCollider>().isTrigger = true;
    }
    public void SetEnemy()                    // устанавливаем параметры врагов
    {
        switch (name)
        {
            case "Wolf":
                EnemyTag = "#NameWolf";
                if (GOD.IsTutorial)
                IsAgressive = true;
                else
                    IsAgressive = false;
                Damage = 6;                            // урон
                maxHP = 200;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[1];
                EnemyLut[0] = "Skin";
                break;
            case "Ozz":
                EnemyTag = "#NameOzz";
                IsAgressive = false;
                Damage = 6;                            // урон
                maxHP = 200;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[1];
                EnemyLut[0] = "Honey";
                break;
            case "Bear":
                EnemyTag = "#NameBear";
                IsAgressive = false;
                Damage = 12;                            // урон
                maxHP = 200;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[2];
                EnemyLut[0] = "Skin";
                EnemyLut[1] = "Wool";
                break;
            case "Pig":
                EnemyTag = "#NamePig";
                IsAgressive = false;
                Damage = 15;                            // урон
                maxHP = 200;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[2];
                EnemyLut[0] = "Meat";
                EnemyLut[1] = "Skin";
                break;
            case "Crab":
                EnemyTag = "#NameCrab";
                IsAgressive = false;
                Damage = 18;                            // урон
                maxHP =210;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[1];
                EnemyLut[0] = "CrabShell";
                break;
            case "Elefant":
                EnemyTag = "#NameElefant";
                IsAgressive = false;
                Damage = 10;                            // урон
                maxHP = 400;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[3];
                EnemyLut[0] = "Skin";
                EnemyLut[1] = "Skin";
                EnemyLut[2] = "Meat";
                break;
            case "Spider":
                EnemyTag = "#NameSpider";
                IsAgressive = false;
                Damage = 20;                            // урон
                maxHP = 350;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[1];
                EnemyLut[0] = "Web";
                break;
            case "Scorpion":
                EnemyTag = "#NameScorpion";
                IsAgressive = false;
                Damage = 25;                            // урон
                maxHP = 350;                               // здоровье
                HP = maxHP;
                AgressiveDistance = 10;               // дистанция, с которой агрится
                EnemyRun = 8;                  // скорость когда возвращается на базу
                EnemyUsialSpeed = 2;                // скорость обычная
                EnemySpeed = EnemyUsialSpeed;                   // скорость 
                EnemyLut = new string[3];
                EnemyLut[0] = "Meat";
                EnemyLut[1] = "Meat";
                EnemyLut[2] = "Meat";
                break;
        }
    }


    public void AddEffects()
    {
        Transform eparent = transform.Find("Effects");
        if (eparent)
        {
            switch (name)
            {
                case "Wolf":
                    Transform t = eparent.GetChild(0);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At2 = At1;
                    }
                    t = eparent.GetChild(1);
                    if (t)
                        At3 = t.GetComponent<ParticleSystem>();
                    break;
                case "Ozz":
                    t = eparent.GetChild(0);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At2 = At1;
                        At3 = At1;
                    }
                    break;
                case "Pig":
                    t = eparent.GetChild(0);
                    if (t)
                        At3 = t.GetComponent<ParticleSystem>();
                    t = eparent.GetChild(1);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At2 = At1;
                    }
                    break;
                case "Bear":
                    t = eparent.GetChild(0);
                    if (t)
                        At1 = t.GetComponent<ParticleSystem>();
                    t = eparent.GetChild(1);
                    if (t)
                        At2 = t.GetComponent<ParticleSystem>();
                    break;
                case "Crab":
                    t = eparent.GetChild(0);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At2 = At1;
                        At3 = At1;
                    }
                    break;
                case "Elefant":
                    t = eparent.GetChild(0);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At2 = At1;
                        At3 = At1;
                    }
                    break;
                case "Scorpion":
                    t = eparent.GetChild(0);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At3 = At1;
                    }
                    t = eparent.GetChild(1);
                    if (t)
                        At2 = t.GetComponent<ParticleSystem>();
                    break;
                case "Spider":
                    t = eparent.GetChild(0);
                    if (t)
                    {
                        At1 = t.GetComponent<ParticleSystem>();
                        At2 = At1;
                    }
                    t = eparent.GetChild(1);
                    if (t)
                        At3 = t.GetComponent<ParticleSystem>();
                    break;
            }

        }
    }
}
