﻿using UnityEngine;
using System.Collections;

public class ReturnAnimalEffect : MonoBehaviour {
    ParticleSystem P;

    public void Start()
    {
        P = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (P.isStopped)
        {
            gameObject.SetActive(false);
        }
    }
}
