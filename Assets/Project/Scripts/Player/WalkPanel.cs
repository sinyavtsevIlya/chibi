﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;


public class WalkPanel : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameManager GOD;

    public void OnPointerUp(PointerEventData eventData)
    {
        GOD.CameraScript.OnWalkPanel = false;
        GOD.CameraScript.RotationTouch = 0;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GOD.CameraScript.OnWalkPanel = true;
        if (!GOD.PlayerContr.InPut)
        {
            GOD.CameraScript.OnWalkPanel = true;
            GOD.CameraScript.RotationTouch = Input.touchCount - 1;
            Ray ray = GOD.PlayerCam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
           // Debug.DrawRay(ray.origin, ray.direction * 1000, Color.yellow);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (Vector3.SqrMagnitude(GOD.Player.transform.position- hit.transform.position) < 100)
                {
                    if (hit.collider.tag == "BrokenBridge")
                    {
                        GOD.PlayerInter.OpenBridgeCraft(hit.transform);
                    }
                    if (!GOD.PlayerContr.InBuilding)
                    {
                        // print(hit.collider.name);
                        switch (hit.collider.name)
                        {
                            case "Smithy":  // открываем крафты
                            case "Stove":
                            case "Windmill":
                            case "Mannequie":
                                GOD.CraftContr.OpenCraft(hit.transform.gameObject);
                                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Smithy);
                                break;
                            case "Door":          // открываем дверь
                                hit.collider.transform.GetComponent<DoorScript>().Door();
                                break;
                            case "WoodWallDoor":          // открываем дверь
                                Transform t = hit.collider.transform.Find("Door");
                                if (t)
                                    t.GetComponent<DoorScript>().Door();
                                break;
                            case "Chest":          // сундук
                                GOD.PlayerInter.OpenChestInventory(hit.transform.GetComponent<ChestScript>());
                                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Chest);
                                break;
                            case "IceChest":          // сундук
                                GOD.PlayerInter.OpenChestInventory(hit.transform.GetComponent<ChestScript>());
                                GOD.Audio.Sound.PlayOneShot(GOD.Audio.IceChest);
                                break;
                            case "WaterRipple":          // место рыбалки
                                if (GOD.PlayerInter.ObjectTarget != null && GOD.PlayerInter.ObjectTarget.tag == "Fish")
                                {
                                    if (GOD.PlayerContr.HaveRod)
                                    {
                                        GOD.FishingWindow.FishingSpeed = 1;
                                        GOD.FishingWindow.FirstFishing();
                                    }
                                    else
                                    {
                                        GOD.DialogS.ShowDialog("NeedRod");
                                    }
                                }
                                break;
                            case "Bot":
                                if (hit.transform.GetComponent<MountainBtn>())
                                    hit.transform.GetComponent<MountainBtn>().MountainBtnUp(true);
                                break;
                            case "Top":
                                if (hit.transform.GetComponent<MountainBtn>())
                                    hit.transform.GetComponent<MountainBtn>().MountainBtnUp(true);
                                break;
                            case "Firecamp":
                                GOD.PlayerInter.CurrentFirecamp = hit.transform;
                                GOD.PlayerInter.OpenFirecampWindow();
                                break;
                            case "BuildBridge":
                                hit.transform.GetComponent<BridgeBtn>().BridgeBtnUp();
                                break;
                            /* case "BagAfterDead":
                                 if (GOD.PlayerContr.IsAlive)
                                 {
                                     GOD.PlayerInter.OpenAfterDead(hit.collider.gameObject);
                                     if (GOD.InventoryContr.CanAddSeveral(GOD.InventoryContr.AfterDead.childCount-1)) //если сразу все поднимается, берем все
                                     {
                                         AfterDeadBag a = hit.transform.GetComponent<AfterDeadBag>();
                                         for (int i = 0; i < a.MyIcons.Count; i++)
                                         {
                                             GOD.InventoryContr.AddToInventoryWithParametre(a.MyIcons[i].thisName, a.MyIcons[i].ResourceCount, a.MyIcons[i].currentDurability, a.MyIcons[i].currentValidity, a.MyIcons[i].WaterCount);
                                             GOD.PlayerInter.ShowNewItem(a.MyIcons[i].thisName, a.MyIcons[i].ResourceCount);
                                         }
                                         GameObject g = hit.transform.gameObject;
                                         GOD.InventoryContr.AllBags.Remove(g);
                                         Destroy(g);
                                         GOD.PlayerInter.CloseAfterDead();

                                     }
                                 }
                                 break;*/
                            case "Arrow":
                                GOD.Map.OpenMap();
                                break;
                            case "ArrowBtn":
                                GOD.Map.OpenMap();
                                break;
                            case "StandForArrow":
                                GOD.Map.OpenMap();
                                break;
                        }

                    }
                }
            }
        }
    }

}
