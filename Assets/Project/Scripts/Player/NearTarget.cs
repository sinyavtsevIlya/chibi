﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NearTarget : MonoBehaviour {

    public GameManager GOD;

    public List<Transform> TargetsAround;     // массив ближних целей
    public float MinDistance = 100;           // минимальная дистанция до целей
    public GameObject NearTargetObject;      // ближайшая цель

    public List<Transform> BuildingsAround;     // массив зданий
    public List<Transform> FirecаmpsAround;     // массив костров рядом

    List<string> IgnoredTags = new List<string>() {"Player","Ground","GroundWall","Lut","Ignored","WaterPuddle", "GroundHouse","House","Build"};
    

    void OnTriggerEnter(Collider other)            // заполняем массив целей
    {
        if (!IgnoredTags.Contains(other.transform.tag)) //цели
        {
            if (!TargetsAround.Contains(other.transform))
            {
                if (other.transform.tag == "Heap")
                {
                    if (GOD.PlayerAnimation.ItemOnHand && GOD.PlayerAnimation.ItemOnHand.name.Contains("Shovel"))
                        TargetsAround.Add(other.transform);
                }
                else 
                {
                    TargetsAround.Add(other.transform);
                }
            }
            if (other.gameObject.name == "AutoBot" || other.gameObject.name == "AutoUp")
            {
                if (other.transform.GetComponent<MountainBtn>())
                    other.transform.GetComponent<MountainBtn>().MountainBtnUp();
            }
        }
        else if (other.transform.tag == "GroundHouse" && !BuildingsAround.Contains(other.transform))
        {
            BuildingsAround.Add(other.transform);
            GOD.BuildingS.PlayerIn();
        }
        else if (other.transform.name == "FirecampSphere" && !FirecаmpsAround.Contains(other.transform))
        {
            FirecаmpsAround.Add(other.transform);
        }
        if(GOD.IsTutorial && other.gameObject.name == "TutorialLimit")
        {
            GOD.DialogS.ShowDialog("Tutorial");
        }

        if ((other.gameObject.tag == "Lut")) //подбор лута
        {
            if (other.gameObject.name.Contains("Gem"))
            {
                int x = other.GetComponent<LutScript>().LutCount;
                Balance.Gem += x;
                GOD.PlayerInter.ShowNewItem(other.name, x);
                GOD.Pull.ReturnObject(other.gameObject);
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetLut);
            }
            else
            {
                int x = other.GetComponent<LutScript>().LutCount;
                if (GOD.InventoryContr.CanAdd(other.name, x))
                {
                    GOD.InventoryContr.AddToInventory(other.name, x);
                    GOD.PlayerInter.ShowNewItem(other.name, x);
                    GOD.Pull.ReturnObject(other.gameObject);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetLut);

                    if (GOD.IsTutorial)
                        GOD.Tutor.GetItems++;
                }
                else
                {
                    GOD.DialogS.ShowDialog("CantAdd");
                }
            }
        }
        else if (other.gameObject.tag == "DeadLut") //подбор сумки после смерти
        {
            if (GOD.PlayerContr.IsAlive)
            {
                GOD.PlayerInter.OpenAfterDead(other.gameObject);
                if (GOD.InventoryContr.CanAddSeveral(GOD.InventoryContr.AfterDead.childCount - 1)) //если сразу все поднимается, берем все
                {
                    AfterDeadBag a = other.transform.GetComponent<AfterDeadBag>();
                    for (int i = 0; i < a.MyIcons.Count; i++)
                    {
                        GOD.InventoryContr.AddIcon(a.MyIcons[i]);
                        a.MyIcons[i].ParentSlot = null;
                      //  GOD.InventoryContr.AddToInventoryWithParametre(a.MyIcons[i].thisName, a.MyIcons[i].ResourceCount, a.MyIcons[i].currentDurability, a.MyIcons[i].currentValidity, a.MyIcons[i].WaterCount);
                       // a.MyIcons[i].Delete(GOD);
                      //  i--;
                        GOD.PlayerInter.ShowNewItem(a.MyIcons[i].thisName, a.MyIcons[i].ResourceCount);
                    }
                    GameObject g = other.transform.gameObject;
                    GOD.InventoryContr.AllBags.Remove(g);
                    Destroy(g);
                    if(GOD.PlayerInter.BagAfterDead.activeSelf)
                    GOD.PlayerInter.CloseAfterDead();

                }
            }
        }
        else if ((other.gameObject.tag == "WaterPuddle"))    // в луже
        {
            var c = GOD.DopEff.PlayerOnWater.emission;
            c.rateOverTime = 3f;
        }
        else if (other.gameObject.name.Contains("Island")) // зашли на остров
        {
            if (other.gameObject.name != GOD.EnviromentContr.CurrentIsland)
                GOD.EnviromentContr.PlayerOnIsland(GOD.ReturnIslandByName(other.gameObject.name));
        }
        else if (other.transform.name == "Tornado" || other.transform.name == "TornadoBig")
            other.GetComponent<TornadoScript>().SetDamage();
        else if (GOD.IsTutorial && other.transform.name == "Portal")
            GOD.Tutor.StartTeleport();
    }


    void OnTriggerExit(Collider other)                 // опустошаем массив целей
    {
        if (!IgnoredTags.Contains(other.transform.tag))
        {
            if (TargetsAround.Contains(other.transform))
            {
                TargetsAround.Remove(other.transform);
            }
        }
        else if (other.transform.tag == "GroundHouse" && BuildingsAround.Contains(other.transform))
        {
            BuildingsAround.Remove(other.transform);
            if (BuildingsAround.Count == 0)
                GOD.BuildingS.PlayerOut();
        }
        else if (other.transform.name == "FirecampSphere" && FirecаmpsAround.Contains(other.transform))
        {
                FirecаmpsAround.Remove(other.transform);
        }

        if ((other.gameObject.tag == "WaterPuddle"))    // в луже
        {
            var c = GOD.DopEff.PlayerOnWater.emission;
            c.rateOverTime = 0f;
        }
       // else if (other.transform.name == "Firecamp" || other.transform.name == "WoodBase")
           // GOD.PlayerContr.NearHouse.Remove(other.transform);
        else if (other.transform.name == "Tornado" || other.transform.name == "TornadoBig")
            other.GetComponent<TornadoScript>().StopDamage();
    }

    void FixedUpdate()                             // вычисляем ближающую цель
    {
        MinDistance = 1000;
        NearTargetObject = null;
        for(int i=0; i<TargetsAround.Count; i++)
        {
            if (TargetsAround[i] != null)
            {
                if (TargetsAround[i].name == "WaterRipple")
                {
                    NearTargetObject = TargetsAround[i].gameObject;
                    break;
                }
                else
                {
                    float D = Vector3.SqrMagnitude(TargetsAround[i].position- transform.position);
                    if ((D < MinDistance) && (TargetsAround[i].gameObject.activeSelf))
                    {
                        MinDistance = D;
                        NearTargetObject = TargetsAround[i].gameObject;
                    }
                }
            }
        }
        GOD.PlayerInter.SetTarget(NearTargetObject);

        for (int i = 0; i < TargetsAround.Count; i++)
        {
            if (TargetsAround[i] == null || !TargetsAround[i].gameObject.activeSelf)
            {
                TargetsAround.Remove(TargetsAround[i]);
                i--;
            }
            else if ((Vector3.SqrMagnitude(TargetsAround[i].position - transform.position) > 100f) && TargetsAround[i].CompareTag("Water"))
                TargetsAround.Remove(TargetsAround[i]);
        }

        for (int i = 0; i < BuildingsAround.Count; i++)
        {
            if (BuildingsAround[i] == null)
            {
                BuildingsAround.Remove(BuildingsAround[i]);
                i--;
            }
        }
        for (int i=0; i<FirecаmpsAround.Count; i++)
        {
            if(FirecаmpsAround[i]==null)
            {
                FirecаmpsAround.Remove(FirecаmpsAround[i]);
                i--;
            }
        }

    }
}
