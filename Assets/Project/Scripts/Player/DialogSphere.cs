﻿using UnityEngine;
using System.Collections;

public class DialogSphere : MonoBehaviour {

    public GameManager GOD;
    public string DialogName;

    void OnTriggerEnter(Collider other)  
    {
        if (other.transform.name == "Player")
        {
            string s = DialogName;
            if(DialogName.Contains("WayTo"))
            {
                switch(DialogName)
                {
                    case "WayToRain":
                        if (GOD.ToSnow.IsOpen)
                            s = "Bridge";
                        else
                            s = "DarkBridge";
                        break;
                    case "WayToSand":
                        if (GOD.ToSnow.IsOpen && GOD.ToRain.IsOpen)
                            s = "Bridge";
                        else
                            s = "DarkBridge";
                        break;
                }
            }
            GOD.DialogS.ShowDialog(s);
        }
    }
}

