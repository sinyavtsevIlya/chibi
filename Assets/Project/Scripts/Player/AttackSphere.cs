﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSphere : MonoBehaviour {

    public List<Transform> EnemiesAround;     // массив ближних врагов

    List<string> IgnoredTags = new List<string>() { "Player", "Ground", "GroundWall", "Lut", "Ignored", "WaterPuddle", "GroundHouse", "House", "Build" };


    void OnTriggerEnter(Collider other)            // заполняем массив врагов
    {
        if (!IgnoredTags.Contains(other.transform.tag))
        {
            if (other.transform.tag == "Enemy" && !EnemiesAround.Contains(other.transform)) //сохраняем всех врагов
                EnemiesAround.Add(other.transform);
        }
    }

    void OnTriggerExit(Collider other)                 // опустошаем массив целей
    {
        if (!IgnoredTags.Contains(other.transform.tag))
        {
            if (EnemiesAround.Contains(other.transform)) //удаляем врагов
                EnemiesAround.Remove(other.transform);
        }
    }

}
