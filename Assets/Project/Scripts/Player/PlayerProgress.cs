﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if !NO_GPGS
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class PlayerProgress : MonoBehaviour
{
    public GameManager GOD;
    public MenuScript Menu;

    string[] AllKeys;
    string[] IncrementKeys;
    string[] IslandsKeys;
    string[] WeathersKeys;

    public void Init()
    {
        AllKeys = new string[13];
        AllKeys[0] = GPGSIds.achievement_trailblazer;
        AllKeys[1] = GPGSIds.achievement_welcome_back;
        AllKeys[2] = GPGSIds.achievement_vip_player;
        AllKeys[3] = GPGSIds.achievement_setting_out;
        AllKeys[4] = GPGSIds.achievement_when_it_all_comes_crashing_down;
        AllKeys[5] = GPGSIds.achievement_a_bridge_of_ice;
        AllKeys[6] = GPGSIds.achievement_frozen_water;
        AllKeys[7] = GPGSIds.achievement_climbing_the_vines;
        AllKeys[8] = GPGSIds.achievement_a_whole_lotta_tree;
        AllKeys[9] = GPGSIds.achievement_brilliant_move;
        AllKeys[10] = GPGSIds.achievement_lucky_man;
        AllKeys[11] = GPGSIds.achievement_weather_lord;
        AllKeys[12] = GPGSIds.achievement_great_explorer;

        IncrementKeys = new string[5];
        IncrementKeys[0] = GPGSIds.achievement_fisherman;
        IncrementKeys[1] = GPGSIds.achievement_digger;
        IncrementKeys[2] = GPGSIds.achievement_miner;
        IncrementKeys[3] = GPGSIds.achievement_lumberjack;
        IncrementKeys[4] = GPGSIds.achievement_gatherer;

        IslandsKeys = new string[9];
        IslandsKeys[0] = "AchivmentExecuted_" + GPGSIds.achievement_north_island;
        IslandsKeys[1] = "AchivmentExecuted_" + GPGSIds.achievement_snow_island;
        IslandsKeys[2] = "AchivmentExecuted_" + GPGSIds.achievement_cold_island;
        IslandsKeys[3] = "AchivmentExecuted_" + GPGSIds.achievement_flooded_island;
        IslandsKeys[4] = "AchivmentExecuted_" + GPGSIds.achievement_ancient_island;
        IslandsKeys[5] = "AchivmentExecuted_" + GPGSIds.achievement_marble_island;
        IslandsKeys[6] = "AchivmentExecuted_" + GPGSIds.achievement_deserted_island;
        IslandsKeys[7] = "AchivmentExecuted_" + GPGSIds.achievement_sand_island;
        IslandsKeys[8] = "AchivmentExecuted_" + GPGSIds.achievement_hot_island;

        WeathersKeys = new string[3];
        WeathersKeys[0] = "AchivmentExecuted_" + GPGSIds.achievement_i_need_more_snow;
        WeathersKeys[1] = "AchivmentExecuted_" + GPGSIds.achievement_i_need_more_rain;
        WeathersKeys[2] = "AchivmentExecuted_" + GPGSIds.achievement_i_need_more_sun;
    }


    public void ShowLeaderbords()
    {

        if (GPGSLogin.authenticated)
            GPGSLogin.ShowLeaderboardUI();
        else
        {
            if (GOD)
                GOD.Settings.LoginPlayServises("Leaderbords");
            if (Menu)
                Menu.Settings.LoginPlayServises("Leaderbords");
        }
    }
    public void ShowAchivment()
    {
        if (GPGSLogin.authenticated)
            GPGSLogin.ShowAchivmentUI();
        else
        {
            if (GOD)
                GOD.Settings.LoginPlayServises("Achivments");
            if (Menu)
                Menu.Settings.LoginPlayServises("Achivments");
        } 
    }
   

    //СПИСКИ ДОСТИЖЕНИЙ************************************************************************************************************
    //ПРОЖИТО ДНЕЙ________________________________________________________
    public void SaveMaxDay(int Day)
    {
        GPGSLogin.ReportScore(GPGSIds.leaderboard_days_survived, Day);
    }
    //СМЕРТЕЙ________________________________________________________
    public void SaveMaxDeath()
    {
        GPGSLogin.ReportScore(GPGSIds.leaderboard_death, 1);
    }
    //СМЕРТЕЙ________________________________________________________
    public void SaveMaxKill()
    {
        GPGSLogin.ReportScore(GPGSIds.leaderboard_killers, 1);
    }
    // ВРЕМЕНИ В ИГРЕ________________________________________________________
    public void SaveMaxTime()
    {
        GPGSLogin.ReportScore(GPGSIds.leaderboard_time_in_game, 1);
    }
    // СОЗДАНО ПРЕДМЕТОВ________________________________________________________
    public void SaveMaxCraft(int count)
    {
        GPGSLogin.ReportScore(GPGSIds.leaderboard_items_crafted, count);
    }
    // СОЗДАНО ПОСТРОЕК________________________________________________________
    public void SaveMaxBuild()
    {
        GPGSLogin.ReportScore(GPGSIds.leaderboard_buildings, 1);
    }
    //************************************************************************************************************

    //АЧИВКИ************************************************************************************************************
    public void LoadAchivments()  //загружаем ачивки с гугла
    {
#if !NO_GPGS
        if (Social.localUser.authenticated)
        {
            Social.LoadAchievements(data =>
            {
                foreach (IAchievement achiev in data)
                {
                    switch (achiev.id)
                    {
                        case GPGSIds.achievement_trailblazer: if ((achiev.completed) && (PlayerPrefs.GetInt("ChibiLaunchGame") < 1)) PlayerPrefs.SetInt("ChibiLaunchGame", 1); break;
                        case GPGSIds.achievement_north_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_north_island,true); break;
                        case GPGSIds.achievement_snow_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_snow_island, true); break;
                        case GPGSIds.achievement_cold_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_cold_island, true); break;
                        case GPGSIds.achievement_flooded_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_flooded_island, true); break;
                        case GPGSIds.achievement_ancient_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_ancient_island, true); break;
                        case GPGSIds.achievement_marble_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_marble_island, true); break;
                        case GPGSIds.achievement_deserted_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_deserted_island, true); break;
                        case GPGSIds.achievement_sand_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_sand_island, true); break;
                        case GPGSIds.achievement_hot_island: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_hot_island, true); break;
                        case GPGSIds.achievement_i_need_more_snow: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_i_need_more_snow, true); break;
                        case GPGSIds.achievement_i_need_more_rain: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_i_need_more_rain, true); break;
                        case GPGSIds.achievement_i_need_more_sun: if (achiev.completed) PlayerPrefsX.SetBool("AchivmentExecuted_" + GPGSIds.achievement_i_need_more_sun, true); break;
                        case GPGSIds.achievement_fisherman:
                            int xFisman = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_fisherman, 0);
                            if (xFisman < (int)achiev.percentCompleted)
                                PlayerPrefs.SetInt("AchivmentExecuted_" + GPGSIds.achievement_fisherman, (int)achiev.percentCompleted);
                            break;
                        case GPGSIds.achievement_digger:
                            int xDigger = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_digger, 0);
                            if (xDigger < (int)achiev.percentCompleted)
                                PlayerPrefs.SetInt("AchivmentExecuted_" + GPGSIds.achievement_digger, (int)achiev.percentCompleted);
                            break;
                        case GPGSIds.achievement_miner:
                            int xMiner = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_miner, 0);
                            if (xMiner < (int)achiev.percentCompleted)
                                PlayerPrefs.SetInt("AchivmentExecuted_" + GPGSIds.achievement_miner, (int)achiev.percentCompleted);
                            break;
                        case GPGSIds.achievement_lumberjack:
                            int xLumberjack = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_lumberjack, 0);
                            if (xLumberjack < (int)achiev.percentCompleted)
                                PlayerPrefs.SetInt("AchivmentExecuted_" + GPGSIds.achievement_lumberjack, (int)achiev.percentCompleted);
                            break;
                        case GPGSIds.achievement_gatherer:
                            int xCollector = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_gatherer, 0);
                            if (xCollector < (int)achiev.percentCompleted)
                                PlayerPrefs.SetInt("AchivmentExecuted_" + GPGSIds.achievement_gatherer, (int)achiev.percentCompleted);
                            break;
                    }
                }
            });
        }
        PlayerPrefs.Save();
#endif
        if (AllKeys == null)
            Init();
        for (int i = 0; i < AllKeys.Length; i++)
            GPGSLogin.ReportProgress("AchivmentExecuted_" + AllKeys[i]);
        for (int i = 0; i < IslandsKeys.Length; i++)
            GPGSLogin.ReportProgress(IslandsKeys[i]);
        for (int i = 0; i < WeathersKeys.Length; i++)
            GPGSLogin.ReportProgress(WeathersKeys[i]);
        for (int i=0; i<IncrementKeys.Length; i++)
            GPGSLogin.IncrementAchievement(IncrementKeys[i], PlayerPrefs.GetInt("AchivmentExecuted_" + IncrementKeys[i]), 50);
    }

    public void SetAchivments(int id)
    {
        switch (id)
        {
                case 1:                              //первый и второй вход в игру
                    int x = PlayerPrefs.GetInt("ChibiLaunchGame");
                    if (x < 2)
                        x++;
                    if (x >= 1)
                        GPGSLogin.ReportProgress(GPGSIds.achievement_trailblazer);
                    if (x >= 2)
                        GPGSLogin.ReportProgress(GPGSIds.achievement_welcome_back);
                    PlayerPrefs.SetInt("ChibiLaunchGame", x);
                    break;
                case 2:                        //первая покупка
                    GPGSLogin.ReportProgress(GPGSIds.achievement_vip_player);
                    break;
                case 3:                       //начало пути
                    GPGSLogin.ReportProgress(GPGSIds.achievement_setting_out);
                    break;
                case 4:                      //головоломка с ледяной глыбой
                    GPGSLogin.ReportProgress(GPGSIds.achievement_when_it_all_comes_crashing_down);
                    break;
                case 5:                      //головоломка с ледяным мостом
                    GPGSLogin.ReportProgress(GPGSIds.achievement_a_bridge_of_ice);
                    break;
                case 6:                      //головоломка с водопадом
                    GPGSLogin.ReportProgress(GPGSIds.achievement_frozen_water);
                    break;
                case 7:                      //головоломка с лианами
                    GPGSLogin.ReportProgress(GPGSIds.achievement_climbing_the_vines);
                    break;
                case 8:                      //головоломка с деревом
                    GPGSLogin.ReportProgress(GPGSIds.achievement_a_whole_lotta_tree);
                    break;
                case 9:                      //головоломка с двойной сменой погоды
                    GPGSLogin.ReportProgress(GPGSIds.achievement_brilliant_move);
                    break;
                case 10:                      //выловить самый редкий сундук
                    GPGSLogin.ReportProgress(GPGSIds.achievement_lucky_man);
                    break;
                case 11:                      //ачивка за 50 рыбалок
                    int xFish = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_fisherman);
                    xFish += 2;
                GPGSLogin.IncrementAchievement(GPGSIds.achievement_fisherman, xFish,50);
                    break;
                case 12:                      //ачивка за 50 раскопок
                    int xShovel = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_digger);
                    xShovel += 2;
                GPGSLogin.IncrementAchievement(GPGSIds.achievement_digger, xShovel, 50);
                break;
                case 13:                      //ачивка за 50 руды
                    int xMiner = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_miner);
                xMiner += 2;
                GPGSLogin.IncrementAchievement(GPGSIds.achievement_miner, xMiner, 50);
                break;
                case 14:                      //ачивка за 50 деревьев
                    int xLumberjack = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_lumberjack);
                xLumberjack += 2;
                GPGSLogin.IncrementAchievement(GPGSIds.achievement_lumberjack, xLumberjack, 50);
                break;
                case 15:                      //ачивка за 50 сбора
                    int xCollector = PlayerPrefs.GetInt("AchivmentExecuted_" + GPGSIds.achievement_gatherer);
                xCollector += 2;
                GPGSLogin.IncrementAchievement(GPGSIds.achievement_gatherer, xCollector, 50);
                break;
        }
    }
    public void SetOpenIsland(int id)  //открытие островов
    {
        if (id > 0 && !PlayerPrefsX.GetBool(GPGSIds.achievement_great_explorer, false))
        {
            switch (id)
            {
                case 1: GPGSLogin.ReportProgress(GPGSIds.achievement_north_island); break;
                    case 2: GPGSLogin.ReportProgress(GPGSIds.achievement_snow_island); break;
                    case 3: GPGSLogin.ReportProgress(GPGSIds.achievement_cold_island); break;
                    case 4: GPGSLogin.ReportProgress(GPGSIds.achievement_flooded_island); break;
                    case 5: GPGSLogin.ReportProgress(GPGSIds.achievement_ancient_island); break;
                    case 6: GPGSLogin.ReportProgress(GPGSIds.achievement_marble_island); break;
                    case 7: GPGSLogin.ReportProgress(GPGSIds.achievement_deserted_island); break;
                    case 8: GPGSLogin.ReportProgress(GPGSIds.achievement_sand_island); break;
                    case 9: GPGSLogin.ReportProgress(GPGSIds.achievement_hot_island); break;
            }

                bool x = true;                //проверка на открытие всех островов
                for (int i = 0; i < IslandsKeys.Length; i++)
                {
                    x = x && PlayerPrefsX.GetBool(IslandsKeys[i], false);
                    if (!x)
                        break;
                }
                if (x)
                    GPGSLogin.ReportProgress(GPGSIds.achievement_great_explorer);
        }
    }

    public void SetChangeWeather(int id)  //смена погоды
    {
        if (!PlayerPrefsX.GetBool("AchivmentExecuted_" + GPGSIds.achievement_weather_lord, false))
        {
            switch (id)
            {
                case 1: GPGSLogin.ReportProgress(GPGSIds.achievement_i_need_more_snow); break;
                case 2: GPGSLogin.ReportProgress(GPGSIds.achievement_i_need_more_rain); break;
                case 3: GPGSLogin.ReportProgress(GPGSIds.achievement_i_need_more_sun); break;

            }
            bool x = true;                //проверка на использование всех погод
            for (int i = 0; i < WeathersKeys.Length; i++)
            {
                x = x && PlayerPrefsX.GetBool(WeathersKeys[i], false);
                if (!x)
                    break;
            }
            if (x)
                GPGSLogin.ReportProgress(GPGSIds.achievement_weather_lord);
        }
    }
}
