﻿using UnityEngine;
using System.Collections;

public class FollowPlayerScript : MonoBehaviour {


    public Transform Player;
    public float dopY = 0;

	void Update ()
    {
        transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y+dopY, Player.transform.position.z);
    }
}
