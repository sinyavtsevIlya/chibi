﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogScript : MonoBehaviour {

    GameManager GOD;
    public Text DialogText;
    public GameObject Snow11, Snow12, Snow21, Snow22, Water1, Water2, Sand1, DialogSand2;
    public GameObject BridgeLeft, BridgeRight, BridgeTop;

    public void Init(GameManager g)
    {
        GOD = g;
    }

    public void ShowDialog(string s)
    {
        StopAllCoroutines();
        StartCoroutine(StopDialog());
        if (!GOD.IsTutorial)
        {
            switch (s)
            {
                case "CantAdd":
                    DialogText.text = Localization.instance.getTextByKey("#NoPlace");
                    break;
                case "Bridge":
                    DialogText.text = Localization.instance.getTextByKey("#DialogBridge");
                    break;
                case "DarkBridge":
                    DialogText.text = Localization.instance.getTextByKey("#DialogDarkBridge");
                    break;
                case "WaterFall":
                    DialogText.text = Localization.instance.getTextByKey("#DialogWaterFall");
                    break;
                case "LeanFall":
                    DialogText.text = Localization.instance.getTextByKey("#DialogLeanFall");
                    break;
                case "IceBlock":
                    DialogText.text = Localization.instance.getTextByKey("#DialogIceBlock");
                    if (GOD.QuestContr.IsInit && GOD.QuestContr.Find.Contains("BreakIce"))
                        GOD.QuestContr.CheckAllQuests("find", "BreakIce", 1);
                    break;
                case "IceBridge":
                    DialogText.text = Localization.instance.getTextByKey("#DialogIceBridge");
                    break;
                case "BigTree":
                    DialogText.text = Localization.instance.getTextByKey("#DialogBigTree");
                    break;
                case "SandLake":
                    DialogText.text = Localization.instance.getTextByKey("#DialogLake");
                    break;
                case "NeedShovel":
                    DialogText.text = Localization.instance.getTextByKey("#NeedShovel");
                    break;
                case "NeedRod":
                    DialogText.text = Localization.instance.getTextByKey("#NeedRod");
                    break;
                case "CantChangeWeather":
                    DialogText.text = Localization.instance.getTextByKey("#DialogCantChangeWeather");
                    break;
                case "MyChest":
                    DialogText.text = Localization.instance.getTextByKey("#DialogMyChest");
                    break;
                case "SunMantle":
                    DialogText.text = Localization.instance.getTextByKey("#DialogSunMantle");
                    if(GOD.QuestContr.IsInit && GOD.EnviromentContr.CurrentIsland.Contains("Sand"))
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.WebMantle);
                    break;
                case "RainMantle":
                    DialogText.text = Localization.instance.getTextByKey("#DialogRainMantle");
                    if(GOD.QuestContr.IsInit && GOD.EnviromentContr.CurrentIsland.Contains("Water"))
                        GOD.QuestContr.AddNewQuest(GOD.QuestContr.ChitinMantle);
                    break;
                case "SnowMantle":
                    DialogText.text = Localization.instance.getTextByKey("#DialogSnowMantle");
                    if(GOD.QuestContr.IsInit && GOD.EnviromentContr.CurrentIsland.Contains("Winter"))
                    GOD.QuestContr.AddNewQuest(GOD.QuestContr.WoolMantle);
                    break;
                case "Coldly":
                    DialogText.text = Localization.instance.getTextByKey("#DialogColdly");
                    break;
                case "Hangry":
                    DialogText.text = Localization.instance.getTextByKey("#DialogHangry");
                    break;
                case "UsePick":
                    DialogText.text = Localization.instance.getTextByKey("#DialogUsePick");
                    break;
                case "UseAxe":
                    DialogText.text = Localization.instance.getTextByKey("#DialogUseAxe");
                    break;
                case "Oops":
                    DialogText.text = Localization.instance.getTextByKey("#DialogOops");
                    break;
            }
        }
        else
        {
            switch (s)
            {
                case "Tutorial":
                    DialogText.text = Localization.instance.getTextByKey("#DialogTutorial");
                    break;
            }
        }
    }
    IEnumerator StopDialog()
    {
        yield return new WaitForSeconds(3f);
        DialogText.text = "";
    }

    public string Probel(string s)   // форматирование строки
    {
        char p = ' ';
        string news = "";

        int dopi = 0;
        for(int i=0; i<s.Length; i++)
        {
            dopi++;
            if(dopi >= 12)
            {
                if(s[i]==p)
                {
                    news += "\n";
                    dopi = 0;
                }
                else
                    news += s[i];
            }
            else
                news += s[i];
        }
        return news;
    }

    public void EnableDialogSphere()  // включаем или выключаем диалоговые сферы для погоды
    {
        if(GOD.EnviromentContr.CSnow.CurrentState == "Sun")
        {
            Snow11.SetActive(false);
            Snow12.SetActive(false);
        }
        else
        {
            Snow11.SetActive(true);
            Snow12.SetActive(true);
        }

        if (GOD.EnviromentContr.CSnow.CurrentState == "Water")
        {
            Snow21.SetActive(false);
            Snow22.SetActive(false);
        }
        else
        {
            Snow21.SetActive(true);
            Snow22.SetActive(true);
        }
        //________________________________________________________________________________________________________
        if (GOD.EnviromentContr.CWater.CurrentState == "Snow")
            Water1.SetActive(false);
        else
            Water1.SetActive(true);

        if (GOD.EnviromentContr.CWater.CurrentState == "Sun")
            Water2.SetActive(false);
        else
            Water2.SetActive(true);
        //________________________________________________________________________________________________________
        if (GOD.EnviromentContr.CSand.CurrentState == "Water")
        {
            Sand1.SetActive(false);
            DialogSand2.SetActive(false);
        }
        else
        {
            if(!GOD.EnviromentContr.CSand.IsTreeDown)
                Sand1.SetActive(true);
            if(GOD.EnviromentContr.CSand.OldState == "Water")
                DialogSand2.SetActive(false);
            else
                DialogSand2.SetActive(true);
        }

    }

    public void EnableDialogSphereBridge()  // включаем или выключаем диалоговые сферы для мостов
    {
        if (GOD.ToSnow.gameObject.activeSelf)
            BridgeLeft.SetActive(true);
        else
            BridgeLeft.SetActive(false);

        if (GOD.ToRain.gameObject.activeSelf)
            BridgeTop.SetActive(true);
        else
            BridgeTop.SetActive(false);

        if (GOD.ToSand.gameObject.activeSelf)
            BridgeRight.SetActive(true);
        else
            BridgeRight.SetActive(false);
    }
}
