﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerItems : MonoBehaviour {

    public GameManager GOD;
    //ПОГОДА
    [Header("Погода:")]
    public GameObject WeatherStaff;
    public Material WeatherSnow, WeatherSun, WeatherRain;
    public GameObject Effect1Sun, Effect2Sun, Effect1Water, Effect2Water, Effect1Snow, Effect2Snow;
    //ОДЕЖДА
    [Header("Одежда:")]
    public GameObject[] ArmorItems;   // массив доспехов
    public GameObject DimondShoulderL, DimondShoulderR, DimondHandL, DimondHandR, DimondFeet1, DimondFeet2;
    public GameObject BoneShoulderL, BoneShoulderR, BoneHandL, BoneHandR, BoneFeet1, BoneFeet2;
    public GameObject IronShoulderL, IronShoulderR, IronHandL, IronHandR, IronFeet1, IronFeet2;
    public GameObject LeatherShoulderL, LeatherShoulderR, LeatherHandL, LeatherHandR;
    public Transform PreItemParent;
    public Dictionary<string, GameObject> PreItems = new Dictionary<string, GameObject>();   // массив объектов, которые лежат на панели быстрого доступа
    public Dictionary<string, int> PreItemsCount = new Dictionary<string, int>();   // сколько таких же объектов лежит в панели быстрого доступа

    //ПРИЧЕСКИ
    [Header("Прически:")]
    public Vector3[] GirlHairsPosition = new Vector3[10];
    public Vector3[] BoyHairsPosition = new Vector3[10];
    public Material BoyHairMaterial, BoyCharacterMaterial;
    public Material GirlHairMaterial, GirlCharacterMaterial;

    public void Init()
    {
        BoyHairsPosition[0] = new Vector3(-0.596f,0.003f, -0.009f);
        BoyHairsPosition[1] = new Vector3(-0.555f, -0.051f, -0.03f);
        BoyHairsPosition[2] = new Vector3(-0.559f, -0.05f, -0.019f);
        BoyHairsPosition[3] = new Vector3(-0.55f, -0.002f, -0.011f);
        BoyHairsPosition[4] = new Vector3(-0.541f, -0.075f, -0.017f);
        BoyHairsPosition[5] = new Vector3(-0.519f, -0.109f, -0.054f);
        BoyHairsPosition[6] = new Vector3(-0.529f, -0.027f, -0.064f);
        BoyHairsPosition[7] = new Vector3(-0.579f, -0.059f, -0.016f);
        BoyHairsPosition[8] = new Vector3(-0.588f, -0.059f, 0.011f);
        BoyHairsPosition[9] = new Vector3(-0.627f, -0.044f, -0.018f);

        GirlHairsPosition[0] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[1] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[2] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[3] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[4] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[5] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[6] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[7] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[8] = new Vector3(0.002f, -0.142f, -0.012f);
        GirlHairsPosition[9] = new Vector3(0.002f, -0.142f, -0.012f);
    }

    public void CreatePreItem(string n, InfoScript.ItemType type) //заранее создаем те объекты, которые есть в панели быстрого доступа
    {
        GameObject g=null;
        if((type == InfoScript.ItemType.weapon) || (type == InfoScript.ItemType.instrument) || (type == InfoScript.ItemType.rod))
            g = GetHandItem(n);

        if (g)
        {
            if (!PreItems.ContainsKey(n))
            {
                GameObject newPreItem = Instantiate(g) as GameObject;
                newPreItem.name = g.name;
                PreItems.Add(n, newPreItem);
                PreItemsCount.Add(n, 1);
                newPreItem.transform.SetParent(PreItemParent);
            }
            else
                PreItemsCount[n]+=1;
        }
    }

    public void AddPreItem(GameObject g)
    {
        if (g!=null &&!PreItems.ContainsKey(g.name))
        {
            PreItems.Add(g.name, g);
            PreItemsCount.Add(g.name, 1);
        }
    }

    public void DeletePreItem(string n) 
    {
        GameObject g;
        if (PreItems.ContainsKey(n))
        {
            if (PreItemsCount[n] > 1)
                PreItemsCount[n] -= 1;
            else
            {
                g = PreItems[n];
                if (g == GOD.PlayerAnimation.ItemOnHand)
                {
                    PreItemsCount.Remove(n);
                    PreItems.Remove(n);
                }
                else
                {
                    PreItemsCount.Remove(n);
                    PreItems.Remove(n);
                    Destroy(g);
                }
            }
        }
        Resources.UnloadUnusedAssets();
    }
    public GameObject GetPreItem(string n)
    {
        if (PreItems.ContainsKey(n))
            return PreItems[n];
        else
            return null;
    }
    public void RemovePreItem(GameObject item)
    {
        if (PreItems.ContainsKey(item.name) && PreItems[item.name]!=null)
            PreItems[item.name].transform.SetParent(PreItemParent);
        else
            Destroy(item);
    }
    public GameObject GetHandItem(string n)
    {
        GameObject g = Resources.Load<GameObject>("Weapons/" + n);
        return g;
    }
    public GameObject GetArmorItem(string n)
    {
        for (int i = 0; i < ArmorItems.Length; i++)
        {
            if (("Icon_" + ArmorItems[i].name == n) || ("cloneIcon_" + ArmorItems[i] == n))
                return ArmorItems[i];
        }
        return null;
    }

    public GameObject GetHairPrefab(int x)
    {
        string n;
        if (GOD.OurPlayer == "Girl")
            n = "Hairs/GirlHairs";
        else
            n = "Hairs/BoyHairs";
        GameObject g = Resources.Load<GameObject>(n + "" + x);
        if (g)
            return g;

        g = Instantiate(Resources.Load(n + "" + 0)) as GameObject;
        return g;
    }

    public Vector3 GetHairPosition(int x)
    {
        if (GOD.OurPlayer == "Girl")
            return GirlHairsPosition[x];
        else
            return BoyHairsPosition[x];
    }

    public GameObject GetSkin(int x)
    {
        string n;
        if (GOD.OurPlayer == "Girl")
            n = "Skins/ChibiGirlSkin";
        else
            n = "Skins/ChibiBoySkin";
        GameObject g = Resources.Load<GameObject>(n+""+x);
        if (g)
            return g;

        g = Instantiate(Resources.Load(n+ "" +0)) as GameObject;
        return g;
    }
}
