﻿using UnityEngine;
using System.Collections;

public class ItemsPosOnPlayer : MonoBehaviour {

    [Header("Boy:")]
    public Vector3 BoyPosition;
    public Vector3 BoyRotation;
    public Vector3 BoyScale;
    [Header("Girl:")]
    public Vector3 GirlPosition;
    public Vector3 GirlRotation;
    public Vector3 GirlScale;

    public Vector3 Pos(string s)
    {
        if (s == "Girl")
            return GirlPosition;
        else
            return BoyPosition;
    }

    public Vector3 Rot(string s)
    {
        if (s == "Girl")
            return GirlRotation;
        else
            return BoyRotation;
    }

    public Vector3 Sc(string s)
    {
        if (s == "Girl")
            return GirlScale;
        else
            return BoyScale;
    }
}
