using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        public GameManager GOD;
        public bool PlayerUp = false; // �������� �����������

        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.

        bool IsLoaded = false; //���������� �� ��������

        bool OneSound = true;
       public void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
            m_Character.Init();
            IsLoaded = true;
        }


        private void Update()
        {
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            if (IsLoaded)
            {
                // read inputs
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                bool crouch = Input.GetKey(KeyCode.C);

                // calculate move direction to pass to character
                if (m_Cam != null)
                {
                    // calculate camera relative direction to move:
                    if (!GOD.PlayerContr.IsStop)
                    {
                        m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                        m_Move = v * m_CamForward + h * m_Cam.right;
                    }
                         else
                        m_Move = new Vector3(0, 0, 0);
                }
                else
                {
                    // we use world-relative directions in the case of no main camera
                        m_Move = v * Vector3.forward + h * Vector3.right;
                }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

                if (GOD.PlayerContr.IsAlive)
                {
                    // pass all parameters to the character control script
                    m_Jump = false;
                    if (PlayerUp)
                    {
                        if (m_Character.CurrentUnder != "")
                        {
                            m_Character.CurrentUnder = "";
                            GOD.PlayerInter.ChangeWeatherBtn.color = new Color(1, 1, 1, 0.5f);
                        }
                        if (OneSound)
                            GOD.Audio.SetPlayer(4);
                        OneSound = false;
                        if (v != 0)
                        {
                            GOD.PlayerAnimation.m_Animator.SetFloat("Speed", 1);
                            if (!GOD.Audio.PlayerSound.isPlaying)
                                GOD.Audio.PlayerSound.Play();
                        }
                        else
                        {
                            GOD.PlayerAnimation.m_Animator.SetFloat("Speed", 0);
                            if (GOD.Audio.PlayerSound.isPlaying)
                                GOD.Audio.PlayerSound.Stop();
                        }
                        m_Character.MoveCharacterUp(new Vector3(0, 1 * v, 0));
                    }
                    else
                    {
                        if (!GOD.PlayerAnimation.IsChangeWeather)
                        {
                            OneSound = true;
                            m_Character.Move(m_Move, crouch, m_Jump);
                            m_Character.MoveCharacter(m_Move);
                        }
                    }
                }
            }
        }
    }
}
