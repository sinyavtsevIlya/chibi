﻿using UnityEngine;
using System.Collections;

public class BafController : MonoBehaviour {

    public GameManager GOD;
    // активен ли баф
    [Header("Активность бафов:")]
    public bool IsSpeed = false;  //активен ли баф ускорения
    public bool IsRegen = false;  //активен ли баф увеличения регена
    public bool IsSatiety = false;//активен ли баф сытости
    public bool IsCraft= false;   //активен ли баф ускорения крафта
    // время активности бафов
    [Header("Время активности бафов:")]
    public int SpeedTimer =0;
    public int RegenTimer = 0;
    public int SatietyTimer = 0;
    public int CraftTimer = 0;

    #region FOOD   // бафы от еды
    public void FoodEffects(string foodname)          
    {
        string dopname = "";
        if (foodname.Contains("Icon"))
        {
            for (int i = 5; i < foodname.Length; i++)
                dopname += foodname[i];
        }
        else
            dopname = foodname;
        switch (dopname)
        {
            case "Candy":
                if (!IsSpeed)
                {
                    IsSpeed = true;
                    GOD.PlayerContr.FoodBaf = 1;
                    GOD.PlayerContr.ChangeSpeed();
                }
                if(SpeedTimer>0)
                {
                    SpeedTimer = 0;
                }
                else
                {
                    SpeedTimer = 0;
                    StartCoroutine(StopSpeedBaf());
                }
                break;
            case "BerriePie":
                if (!IsRegen)
                {
                    IsRegen = true;
                    GOD.PlayerContr.RegenHPSpeed += 0.3f;
                    GOD.PlayerInter.ShowPlayerInfo();
                }
                if (RegenTimer > 0)
                {
                    RegenTimer = 0;
                }
                else
                {
                    RegenTimer = 0;
                    StartCoroutine(StopRegenBaf());
                }
                break;
            case "MeatPies":
                if (!IsSatiety)
                {
                    IsSatiety = true;
                    GOD.PlayerContr.MinusFood = 0;
                }
                if (SatietyTimer > 0)
                {
                    SatietyTimer = 0;
                }
                else
                {
                    SatietyTimer = 0;
                    StartCoroutine(StopEatBaf());
                }
                break;
            case "FishPie":
                if (!IsCraft)
                {
                    IsCraft = true;
                    GOD.CraftContr.CraftLimitUsial = Mathf.Round(GOD.CraftContr.CraftLimitUsial - (GOD.CraftContr.CraftLimitUsial * 25) / 100);
                    GOD.CraftContr.CraftLimitBuild = Mathf.Round(GOD.CraftContr.CraftLimitBuild - (GOD.CraftContr.CraftLimitBuild * 25) / 100);
                }
                if (CraftTimer > 0)
                {
                    CraftTimer = 0;
                }
                else
                {
                    CraftTimer = 0;
                    StartCoroutine(StopCraftBaf());
                }
                break;
            case "Vine":
                if (GOD.TG.IHour >= 1 && GOD.TG.IHour <= 5)
                {
                    float y = GOD.PlayerContr.Frost - 0.1f;
                    if (y < 0)
                        y = 0;
                    GOD.PlayerContr.Frost = y;
                    float x = GOD.PlayerInter.FrostWindow.material.GetFloat("_Cutoff");
                    x += 0.1f;
                    if (x > 1)
                        x = 1;
                    GOD.PlayerInter.FrostWindow.material.SetFloat("_Cutoff", x);
                }
                break;
            case "Roll":
                GOD.PlayerContr.HP = GOD.PlayerContr.realmaxHP;
                GOD.PlayerContr.EAT = GOD.PlayerContr.maxEAT;
                GOD.PlayerInter.ShowIndicators();
                break;
        }
    }

    IEnumerator StopSpeedBaf()
    {
        while (SpeedTimer < 300)
        {
            SpeedTimer++;
            yield return new WaitForSeconds(1);
        }
        SpeedTimer = 0;
        IsSpeed = false;
        GOD.PlayerContr.FoodBaf = 0;
        GOD.PlayerContr.ChangeSpeed();
    }

    IEnumerator StopRegenBaf()
    {
        while (RegenTimer < 300)
        {
            RegenTimer++;
            yield return new WaitForSeconds(1);
        }
        IsRegen = false;
        RegenTimer = 0;
        GOD.PlayerContr.RegenHPSpeed = 0.3f;
        GOD.PlayerInter.ShowPlayerInfo();
    }

    IEnumerator StopEatBaf()
    {
        while (SatietyTimer < 300)
        {
            SatietyTimer++;
            yield return new WaitForSeconds(1);
        }
        IsSatiety = false;
        SatietyTimer = 0;
        GOD.PlayerContr.MinusFood = 0.1f;
    }
    IEnumerator StopCraftBaf()
    {
        while (CraftTimer < 300)
        {
            CraftTimer++;
            yield return new WaitForSeconds(1);
        }
        IsCraft = false;
        CraftTimer = 0;
        GOD.CraftContr.CraftLimitUsial = GOD.CraftContr.CraftLimitUsialConst;
        GOD.CraftContr.CraftLimitBuild = GOD.CraftContr.CraftLimitBuildConst;
    }
    #endregion
    #region Poition  // бафы от алхимии
    public void PoitionEffects(string poitionname)
    {
        string dopname = "";
        if (poitionname.Contains("Icon"))
        {
            for (int i = 5; i < poitionname.Length; i++)
                dopname += poitionname[i];
        }
        else
            dopname = poitionname;
        switch (dopname)
        {
            case "HealingSalve":
                GOD.PlayerContr.realmaxHP += ((GOD.PlayerContr.maxHP * 10) / 100);
                if (GOD.PlayerContr.realmaxHP > GOD.PlayerContr.maxHP)
                    GOD.PlayerContr.realmaxHP = GOD.PlayerContr.maxHP;
                break;
            case "StrongHealingSalve":
                GOD.PlayerContr.realmaxHP += ((GOD.PlayerContr.maxHP * 20) / 100);
                if (GOD.PlayerContr.realmaxHP > GOD.PlayerContr.maxHP)
                    GOD.PlayerContr.realmaxHP = GOD.PlayerContr.maxHP;
                break;
        }
    }
                #endregion
        
        }
