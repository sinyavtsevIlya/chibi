﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class PlayerController : MonoBehaviour {

    GameObject NewSkin; 

    public GameManager GOD;

    public bool IsAlive = true;
    public bool IsStop = false;
    [Header("Параметры игрока:")]
    public float maxHP = 100;                      // максимальное здоровье всегда
    public float realmaxHP = 100;                      // максимальное здоровье  со всеми бафами
    public float maxEAT = 100;                      // максимальная сытость
    public float HP, EAT;                 // текущие значения
    public float RegenHPSpeed = 0.3f;                    // количество восстанавливаемого хп
    public float MinusFood = 0.1f;                   // количество голодания
    float minDamage = 8;                     // наносимый персонажем урон с руки
    public float Damage;                     // наносимый персонажем урон
    public float Armor = 0;                 // броня
    public float NormalSpeed = 6;
    public float Speed;
    float HandSpeed = 8;
    public float AxeSpeed;                   // количество ударов топром
    public float PickSpeed;                  // количество ударов киркой
    public float ShovelSpeed;                // количество взмахов лопатой
    public bool HaveBucket;                // есть ли ведро
    public bool HaveRod;                // есть ли удочка
    public string RodType = "";


    bool IsEatDialog = false;//запущен ли диалог голодания

    [Header("Экипировка:")]
    public GameObject[] EqupmentSlots;     // слоты для экипировки
    public List<GameObject> Clothes = new List<GameObject>();       // надетые вещи
    public GameObject OnHand;
  //  public Transform RightHand;

    public GameObject[] DopInventoryInGame;       // доп слоты быстрого доступа

    public bool InBuilding = false;//активен ли режим строительства
    public bool InPut = false;//активен ли режим установки

    public CapsuleCollider PlayerZone;

    GameObject IsSnow=null, IsSun=null, IsWater=null;   // надеты ли плащи
    string HelpState=" ";
    GameObject NowActiveMantle = null;

    public int IslandBaf = -1;   // прибавки к скорости
    public int FoodBaf = 0;

    [Header("Замерзание:")]
    public float Frost = 0; // насколько замерз персонаж
    public bool IsFrost = true; //идет ли замерзание
    public bool IsFrostMaterial = false;  //запущен ли корутин материала
    public bool IsFrostDialog = false;  //запущен ли корутин диалога
   // public List<Transform> NearHouse = new List<Transform>();

    [Header("Дополнения:")]
    public bool[] GirlHairs, BoyHairs;    //какие прически приобретены
    public GameObject CurrentHair;
    public int CurrentGirlHairNumber = 0;
    public int CurrentBoyHairNumber = 0;


    public Color[] HairColors;    // все цвета причесок
    public bool[] Colors;    //какие цвета приобретены
    public int CurrentBoyHairColorNumber = 0;
    public int CurrentGirlHairColorNumber = 0;

    public bool[] GirlSkins, BoySkins;    // какие скины приобретены
    public int CurrentBoySkinNumber = 0;
    public int CurrentGirlSkinNumber = 0;


    public void Init()
    {
        realmaxHP = maxHP;
        HP = realmaxHP;
        EAT = maxEAT;
        Damage = minDamage;
        Speed = NormalSpeed;
        AxeSpeed = HandSpeed;
        PickSpeed = HandSpeed;
        ShovelSpeed = 0;
        HaveBucket = false;
        HaveRod = false;
        StartCoroutine(Indicators());


        GirlHairs = new bool[11];
        BoyHairs = new bool[11];
        GirlSkins = new bool[7];
        BoySkins = new bool[7];
        InitHairColors();
        Colors = new bool[HairColors.Length];

        for (int i = 0; i < Colors.Length; i++)
            Colors[i] = false;
    }

    void FixedUpdate()
    {
        if (transform.position.y < -60)
            PlayerFall();
    }
    void Update()
    {
        if (GOD.NT.BuildingsAround.Count > 0 || GOD.NT.FirecаmpsAround.Count>0)        //есть ли замерзание
            IsFrost = false;
        else
            IsFrost = true;

        if (GOD)
        {
            if (HelpState != GOD.EnviromentContr.CurrentState)      // воздействие островов на скорость
            {
                HelpState = GOD.EnviromentContr.CurrentState;
                switch (GOD.EnviromentContr.CurrentState)
                {
                    case "Water":
                        if (!IsWater)
                        {
                            IslandBaf = -1;
                            ChangeSpeed();
                            GOD.DialogS.ShowDialog("RainMantle");
                        }
                        else
                        {
                            IslandBaf = 0;
                            ChangeSpeed();
                            if (NowActiveMantle != null)
                                GOD.PlayerAnimation.OutAmor(NowActiveMantle);
                            NowActiveMantle = IsWater;
                            GOD.PlayerAnimation.GetArmor(NowActiveMantle);
                        }
                        break;
                    case "Sun":
                        if (!IsSun)
                        {
                            IslandBaf = -1;
                            ChangeSpeed();
                            GOD.DialogS.ShowDialog("SunMantle");
                        }
                        else
                        {
                            IslandBaf = 0;
                            ChangeSpeed();
                            if (NowActiveMantle != null)
                                GOD.PlayerAnimation.OutAmor(NowActiveMantle);
                            NowActiveMantle = IsSun;
                            GOD.PlayerAnimation.GetArmor(NowActiveMantle);
                        }
                        break;
                    case "Snow":
                        if (!IsSnow)
                        {
                            IslandBaf = -1;
                            ChangeSpeed();
                            GOD.DialogS.ShowDialog("SnowMantle");
                        }
                        else
                        {
                            IslandBaf = 0;
                            ChangeSpeed();
                            if (NowActiveMantle != null)
                                GOD.PlayerAnimation.OutAmor(NowActiveMantle);
                            NowActiveMantle = IsSnow;
                            GOD.PlayerAnimation.GetArmor(NowActiveMantle);
                        }
                        break;
                    case "Null":
                        IslandBaf = 0;
                        ChangeSpeed();
                        break;
                }
            }
        }

    }

    IEnumerator Indicators()        
    {
        yield return new WaitForSeconds(1);
        if (EAT > 0)                                 // голод
            EAT -= MinusFood;
        else
        {
            HP -= 1;
            if (!IsEatDialog)
                StartCoroutine(EatDialog());
        }

        if(HP<realmaxHP)                              // восстановление хп
            HP += RegenHPSpeed;
        else
            HP = realmaxHP;

        //замерзание
        if (GOD.TG.IHour >= 1 && GOD.TG.IHour <= 5)
        {
            if (!GOD.PlayerInter.FrostWindow.gameObject.activeSelf)
            {
                GOD.PlayerInter.FrostWindow.gameObject.SetActive(true);
                Texture t = Resources.Load<Texture>("Frost/FrozenScreen");
                 GOD.PlayerInter.FrostMaterial.SetTexture("_MaskTex", t);
            }

            if (!IsFrostMaterial)
                StartCoroutine(FrostAnimation());
            if (!IsFrostDialog)
                StartCoroutine(FrostDialog());
            if (IsFrost)
            {
                if (Frost > 1)
                    HP -= 1.5f;
                else
                {
                    Frost += 0.014f;
                }
            }
            else
            {
                if (Frost > 0)
                {
                    Frost -= 0.02f;
                }
            }
            //GOD.Save.SaveGame();
        }
        GOD.PlayerInter.ShowIndicators();
        StartCoroutine(Indicators());

    }

    IEnumerator FrostAnimation()  // заморозка экрана
    {
        IsFrostMaterial = true;
        while(GOD.TG.IHour >= 1 && GOD.TG.IHour <= 5)
        {
            float x = GOD.PlayerInter.FrostMaterial.GetFloat("_Cutoff");
            if (IsFrost)
            {
                if (x > 0)
                {
                    x -= 0.015f;
                    GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff", x);
                    if(x<=0.85f)
                        GOD.Audio.FreezeSound.gameObject.SetActive(true);
                    else
                        GOD.Audio.FreezeSound.gameObject.SetActive(false);
                }
            }
            else
            {
                if (x < 1)
                {
                    x += 0.02f;
                    GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff", x);
                    GOD.Audio.FreezeSound.gameObject.SetActive(false);
                }
            }
            yield return new WaitForSeconds(1f);
        }
        GOD.Audio.FreezeSound.gameObject.SetActive(false);
        float y = GOD.PlayerInter.FrostMaterial.GetFloat("_Cutoff");
        while (y< 1)
        {
            yield return new WaitForSeconds(0.1f);
            y += 0.01f;
            GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff", y);
        }
        GOD.PlayerInter.FrostWindow.gameObject.SetActive(false);
        GOD.PlayerInter.FrostMaterial.SetTexture("_MaskTex", null);
        GOD.PlayerInter.FrostMaterial.SetFloat("_Cutoff",1);
        //Resources.UnloadUnusedAssets();
        IsFrostMaterial = false;
    }
    IEnumerator FrostDialog()
    {
        IsFrostDialog = true;
        while (GOD.TG.IHour >= 1 && GOD.TG.IHour <= 5)
        {
            if (IsFrost)
            {
                GOD.DialogS.ShowDialog("Coldly");
            }
            yield return new WaitForSeconds(10);
        }
        IsFrostDialog = false;
    }

    IEnumerator EatDialog()
    {
        IsEatDialog = true;
        while (EAT<=0)
        {
            GOD.DialogS.ShowDialog("Hangry");
            yield return new WaitForSeconds(10);
        }
        IsEatDialog = false;
    }

    /*void OnTriggerEnter(Collider other)   // подбираем предметы
    {
        if ((other.gameObject.tag == "Lut"))
        {
            if (other.gameObject.name.Contains("Gem"))
            {
                int x = other.GetComponent<LutScript>().LutCount;
                Balance.Gem += x;
                GOD.PlayerInter.ShowNewItem(other.name, x);
                GOD.Pull.ReturnObject(other.gameObject);
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetLut);
            }
            else
            {
                int x = other.GetComponent<LutScript>().LutCount;
                if (GOD.InventoryContr.CanAdd(other.name, x))
                {
                    GOD.InventoryContr.AddToInventory(other.name, x);
                    GOD.PlayerInter.ShowNewItem(other.name, x);
                    GOD.Pull.ReturnObject(other.gameObject);
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetLut);

                    if (GOD.IsTutorial)
                        GOD.Tutor.GetItems++;
                }
                else
                {
                    GOD.DialogS.ShowDialog("CantAdd");
                }
            }
        }
        else if (other.gameObject.tag == "DeadLut")
        {
            if (IsAlive)
            {
                GOD.PlayerInter.OpenAfterDead(other.gameObject);
                if (GOD.InventoryContr.CanAddSeveral(GOD.InventoryContr.AfterDead.childCount - 1)) //если сразу все поднимается, берем все
                {
                    AfterDeadBag a = other.transform.GetComponent<AfterDeadBag>();
                    for (int i = 0; i < a.MyIcons.Count; i++)
                    {
                        GOD.InventoryContr.AddToInventoryWithParametre(a.MyIcons[i].thisName, a.MyIcons[i].ResourceCount, a.MyIcons[i].currentDurability, a.MyIcons[i].currentValidity, a.MyIcons[i].WaterCount);
                        GOD.PlayerInter.ShowNewItem(a.MyIcons[i].thisName, a.MyIcons[i].ResourceCount);
                    }
                    GameObject g = other.transform.gameObject;
                    GOD.InventoryContr.AllBags.Remove(g);
                    Destroy(g);
                    GOD.PlayerInter.CloseAfterDead();

                }
            }
        }
        else if ((other.gameObject.tag == "WaterPuddle"))    // в луже
        {
            var c = GOD.DopEff.PlayerOnWater.emission;
            c.rateOverTime = 3f;
        }
        else if (other.gameObject.name.Contains("Island")) // зашли на остров
        {
            if (other.gameObject.name != GOD.EnviromentContr.CurrentIsland)
                GOD.EnviromentContr.PlayerOnIsland(GOD.ReturnIslandByName(other.gameObject.name));
        }
        else if (other.transform.name == "WoodBase" || (other.transform.name == "Firecamp" && other.GetComponent<BuildBlock>() == null))
            NearHouse.Add(other.transform);
        else if (other.transform.name == "Tornado" || other.transform.name == "BigTornado")
            other.GetComponent<TornadoScript>().SetDamage();
        else if (GOD.IsTutorial && other.transform.name == "Portal")
            GOD.Tutor.StartTeleport();
        
    }*/

   /* void OnTriggerExit(Collider other)  
    {
        if ((other.gameObject.tag == "WaterPuddle"))    // в луже
        {
            var c = GOD.DopEff.PlayerOnWater.emission;
            c.rateOverTime = 0f;
        }
        else if (other.transform.name == "Firecamp" || other.transform.name == "WoodBase")
            NearHouse.Remove(other.transform);
        else if (other.transform.name == "Tornado" || other.transform.name == "BigTornado")
            other.GetComponent<TornadoScript>().StopDamage();


    }*/


    public void ChangeSpeed()
    {
        Speed = NormalSpeed + IslandBaf + FoodBaf;
        GOD.TPC.m_MoveSpeedMultiplier = Speed;
        float x = 1f;
        switch ((int)Speed)
        {
            case 5:
                x = 0.9f;
                break;
            case 6:
                x = 1f;
                break;
            case 7:
                x = 1.1f;
                break;
            case 8:
                x = 1.2f;
                break;
        }
        GOD.PlayerAnimation.m_Animator.speed = x;
        GOD.PlayerInter.ShowPlayerInfo();
    }
    public void SetPlayerParametre(GameObject G, bool PlaySound)                        // обвноляем параметры при надевании вещей
    {
        InfoScript I = G.GetComponent<InfoScript>();
        I.NowOnPlayer = true;
        I.OnorOutIcon();
        if (GOD.IsTutorial && G.name.Contains("WoodPick") && GOD.Tutor.TutorialStep == 11)
            GOD.Tutor.NextStep = true;
        if (!GOD.IsTutorial && GOD.QuestContr.IsInit && GOD.QuestContr.Clothes.Contains(I.thisName))   //если ожидаем такой предмет, то проверяем все квесты на завершенность
            GOD.QuestContr.CheckAllQuests("use",I.thisName,1);

        switch (I.type)
        {
            case InfoScript.ItemType.clothes:
                if (G.name.Contains("Mantle"))//если это плащ
                {
                    if (G.name.Contains("ChitinMantle"))
                    {
                        IsWater = G;
                        HelpState = " ";
                    }
                    else if (G.name.Contains("WebMantle"))
                    {
                        IsSun = G;
                        HelpState = " ";
                    }
                    else if (G.name.Contains("WoolMantle"))
                    {
                        IsSnow = G;
                        HelpState = " ";
                    }
                    if (NowActiveMantle != null)
                        GOD.PlayerAnimation.OutAmor(NowActiveMantle);
                    NowActiveMantle = G;
                    GOD.PlayerAnimation.GetArmor(NowActiveMantle);
                }
                else if(G.name.Contains("Belt"))
                {
                    switch(G.name)
                    {
                        case "Icon_LeatherBelt":                                           // включаем до слоты быстрого доступа
                            DopInventoryInGame[0].SetActive(true);
                            break;
                        case "Icon_EmeraldBelt":
                            DopInventoryInGame[0].SetActive(true);
                            DopInventoryInGame[1].SetActive(true);
                            break;
                        case "Icon_SapphireBelt":
                            DopInventoryInGame[0].SetActive(true);
                            DopInventoryInGame[1].SetActive(true);
                            DopInventoryInGame[2].SetActive(true);
                            break;
                        case "Icon_AmethystBelt":
                            DopInventoryInGame[0].SetActive(true);
                            DopInventoryInGame[1].SetActive(true);
                            DopInventoryInGame[2].SetActive(true);
                            DopInventoryInGame[3].SetActive(true);
                            break;
                        case "Icon_RubyBelt":
                            DopInventoryInGame[0].SetActive(true);
                            DopInventoryInGame[1].SetActive(true);
                            DopInventoryInGame[2].SetActive(true);
                            DopInventoryInGame[3].SetActive(true);
                            DopInventoryInGame[4].SetActive(true);
                            break;
                    }
                }
                else
                {
                    Armor += I.armor;
                    Clothes.Add(G);
                    GOD.PlayerAnimation.GetArmor(G);
                }
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetItem);
                break;
            case InfoScript.ItemType.weapon:
                GOD.PlayerInter.FightIsDown = false;
                Damage = I.attack;
                GOD.PlayerAnimation.GetItem(I.thisName);     // отрисовываем предмет в руке
                OnHand = G;
                GOD.AttackSphere.SetActive(true);
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWeapon);
                break;
            case InfoScript.ItemType.instrument:
                GOD.PlayerInter.FightIsDown = false;
                if (G.name.Contains("Axe"))
                    AxeSpeed = I.instrumentSpeed;
                else if (G.name.Contains("Pick"))
                    PickSpeed = I.instrumentSpeed;
                else if (G.name.Contains("Shovel"))
                    ShovelSpeed = I.instrumentSpeed;
                else if (G.name.Contains("Bucket"))
                    HaveBucket = true;
                GOD.PlayerAnimation.GetItem(I.thisName);     // отрисовываем предмет в руке
                OnHand = G;
                Damage = I.instrumentDamage;
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWeapon);
                break;
            case InfoScript.ItemType.rod:
                GOD.PlayerAnimation.GetItem(I.thisName);     // отрисовываем предмет в руке
                HaveRod = true;
                string s = "";
                for (int i = 5; i < G.name.Length; i++)
                    s += G.name[i];
                RodType = s;
                OnHand = G;
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWeapon);
                break;
        }
        GOD.PlayerInter.ShowPlayerInfo();
    }

    public void RemovePlayerParametre(GameObject G, bool PlaySound)                        // обноdляем параметры при надевании вещей
    {
        InfoScript I = G.GetComponent<InfoScript>();
        I.NowOnPlayer = false;
        I.OnorOutIcon();
        switch (I.type)
        {
            case InfoScript.ItemType.clothes:
                if (G.name.Contains("Mantle"))//если это плащ
                {
                    if (G.name.Contains("ChitinMantle"))
                    {
                        IsWater = null;
                        HelpState = " ";
                    }
                    else if (G.name.Contains("WebMantle"))
                    {
                        IsSun = null;
                        HelpState = " ";
                    }
                    else if (G.name.Contains("WoolMantle"))
                    {
                        IsSnow = null;
                        HelpState = " ";
                    }
                    if (NowActiveMantle == G)  // если есть активные другие плащи включаем их
                    {
                        GOD.PlayerAnimation.OutAmor(G);
                        if (IsWater)
                            NowActiveMantle = IsWater;
                        else if (IsSun)
                            NowActiveMantle = IsSun;
                        else if (IsSnow)
                            NowActiveMantle = IsSnow;
                        else
                            NowActiveMantle = null;
                        if (NowActiveMantle)
                            GOD.PlayerAnimation.GetArmor(NowActiveMantle);
                    }
                }
                else if (G.name.Contains("Belt"))
                {
                    for (int i = 0; i < DopInventoryInGame.Length; i++)
                        DopInventoryInGame[i].SetActive(false);
                }
                else
                {
                    Armor -= I.armor;
                    Clothes.Remove(G);
                    GOD.PlayerAnimation.OutAmor(G);
                }
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetItem);
                break;
            case InfoScript.ItemType.weapon:
                Damage = minDamage;
                GOD.PlayerAnimation.OutItem();     // снимаем предмет в руке
                OnHand = null;
                GOD.AttackSphere.SetActive(false);
                GOD.AttackS.EnemiesAround.Clear();
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWeapon);
                break;
            case InfoScript.ItemType.instrument:
                if (G.name.Contains("Axe"))
                    AxeSpeed = HandSpeed;
                else if (G.name.Contains("Pick"))
                    PickSpeed = HandSpeed;
                else if (G.name.Contains("Shovel"))
                    ShovelSpeed = HandSpeed;
                else if (G.name.Contains("Bucket"))
                    HaveBucket = false;
                GOD.PlayerAnimation.OutItem();     // снимаем предмет в руке
                OnHand = null;
                Damage = minDamage;
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWeapon);
                break;
            case InfoScript.ItemType.rod:
                HaveRod = false;
                GOD.PlayerAnimation.OutItem();     // снимаем предмет в руке
                RodType = "";
                OnHand = null;
                if (PlaySound)
                    GOD.Audio.Sound.PlayOneShot(GOD.Audio.GetWeapon);
                break;
        }
        GOD.PlayerInter.ShowPlayerInfo();
    }

    public void SetPlayerHairs(int s)   //ставим прическу на игрока
    {
            if (CurrentHair != null)
                Destroy(CurrentHair);
            CurrentHair = Instantiate(GOD.PI.GetHairPrefab(s));
            CurrentHair.name = GOD.PI.GetHairPrefab(s).name;
            CurrentHair.transform.SetParent(GOD.PlayerAnimation.Head.transform);
            CurrentHair.transform.localPosition = GOD.PI.GetHairPosition(s);
            CurrentHair.transform.localRotation = GOD.PI.GetHairPrefab(s).transform.rotation;
        if (GOD.OurPlayer == "Girl")
        {
            CurrentGirlHairNumber = s;
            PlayerPrefs.SetInt("ChibiCurrentGirlHairNumber", GOD.PlayerContr.CurrentGirlHairNumber);//сохраняем облик игрока
        }
        else
        {
            CurrentBoyHairNumber = s;
            PlayerPrefs.SetInt("ChibiCurrentBoyHairNumber", GOD.PlayerContr.CurrentBoyHairNumber);//сохраняем облик игрока
        }
    }

    public void SetPlayerHairColor(int n)
    {
        GOD.Player.transform.GetChild(0).GetChild(1).GetComponent<Renderer>().material.SetColor("_Color", HairColors[n]);
        if (GOD.OurPlayer == "Girl")
        {
            GOD.PI.GirlHairMaterial.SetColor("_Color", HairColors[n]);
            CurrentGirlHairColorNumber = n;
            PlayerPrefs.SetInt("ChibiCurrentGirlHairColorNumber", GOD.PlayerContr.CurrentGirlHairColorNumber); //сохраняем облик игрока
        }
        else
        {
            GOD.PI.BoyHairMaterial.SetColor("_Color", HairColors[n]);
            CurrentBoyHairColorNumber = n;
            PlayerPrefs.SetInt("ChibiCurrentBoyHairColorNumber", GOD.PlayerContr.CurrentBoyHairColorNumber); //сохраняем облик игрока
        }
       
}

    public void SetPlayerSkins(int n)
    {
        if (GOD.OurPlayer == "Girl")
        {
            if (n != CurrentGirlSkinNumber)
            {
                CurrentGirlSkinNumber = n;
                GOD.CreateNewPlayer(n);
                PlayerPrefs.SetInt("ChibiCurrentGirlSkinNumber", GOD.PlayerContr.CurrentGirlSkinNumber); //сохраняем облик игрока
            }
        }
        else
        {
            if (n != CurrentBoySkinNumber)
            {
                CurrentBoySkinNumber = n;
                GOD.CreateNewPlayer(n);
                PlayerPrefs.SetInt("ChibiCurrentBoySkinNumber", GOD.PlayerContr.CurrentBoySkinNumber);//сохраняем облик игрока
            }
        }

    }
    public void InitHairColors()
    {
        HairColors = new Color[28];

        HairColors[0] = new Color(0.15f, 0.15f, 0.16f); // черный
        HairColors[1] = new Color(0.09f, 0.17f, 0.47f); // полуночный черный
        HairColors[2] = new Color(0f, 0.22f, 1f);  // синий
        HairColors[3] = new Color(0.19f, 0.09f, 0.5f);//персидский индиго
        HairColors[4] = new Color(0.32f, 0.11f, 0.76f); //сине-лиловый
        HairColors[5] = new Color(0.54f, 0.2f, 1f); // персидский синий
        HairColors[6] = new Color(0.53f, 0.39f, 1f);  //умеренный аспидно синий
        HairColors[7] = new Color(0.6f, 0.22f, 0.73f);  //темная орхидея 153/155/188
        HairColors[8] = new Color(0.52f, 0.19f, 0.42f);  //яркий красновато-пурпурный 133/5/108
        HairColors[9] = new Color(1f, 0.37f, 0.88f);  //звезды в шоке 255/94/224
        HairColors[10] = new Color(0.9f, 0.64f, 0.87f);  //орхидея крайола 231/163/223
        HairColors[11] = new Color(1f, 0f, 0.8f);  //пурпурная пиццца 255/0/206
        HairColors[12] = new Color(1f, 0f, 0f);  //красный 255/0/0
        HairColors[13] = new Color(0.51f, 0f, 0f);  //коричнево-малиновый 131/0/0
        HairColors[14] = new Color(0.35f, 0.2f, 0.2f);  //каштнаово-коричневый 90/51/51
        HairColors[15] = new Color(1f, 0.35f, 0f);  //оранжевый 255/90/0
        HairColors[16] = new Color(1f, 0.57f, 0f);  //яркий оранжево-желтый 255/146/0
        HairColors[17] = new Color(1f, 0.44f, 0.44f);  //горько сладкий 255/113/113
        HairColors[18] = new Color(0.96f, 0.73f, 0.46f);  //красный песок 244/188/118
        HairColors[19] = new Color(1f, 0.76f, 0f);  //янтарный 255/195/0
        HairColors[20] = new Color(0.98f, 0.97f, 0.44f);  //лимонный 251/247/111
        HairColors[21] = new Color(0.36f, 1f, 0f);  //ярко зеленый 93/255/0
        HairColors[22] = new Color(0.05f, 0.57f, 0.15f);  //индийский зеленый 14/146/37
        HairColors[23] = new Color(0.4f,1f, 0.5f);  //кричащий зеленый  104/255/130
        HairColors[24] = new Color(0f, 0.96f, 1f);  //цвет морской волны 0/244/255
        HairColors[25] = new Color(0.51f, 0.82f, 0.99f);  //небесный 132/208/253
        HairColors[26] = new Color(0f, 0.62f, 1f);  //цвет твиттера 0/160/255
        HairColors[27] = new Color(0f, 0.3f, 0.5f);  //насыщенный синий 0/80/128
    }

    public void StartDeat()           //смерть персонажа
    {
        if (IsAlive)
        {
            IsAlive = false;
            GOD.PlayerInter.FightIsDown = false;
            GOD.Progress.SaveMaxDeath(); // сохраняем смерти в достижения
            MyAppMetrica.LogEvent("Game", "Days", "Days in a row", "" + (GOD.TG.IDay-1));
            MyAppMetrica.LogEvent("Game", "Days", "All Days", ""+GOD.TG.AllDays);
            GOD.PlayerInter.CloseAll();
            GOD.UpdateRecover();
            GOD.PlayerInter.AllCanvas.SetActive(false);
            GOD.PlayerInter.InventoryOnGame.gameObject.SetActive(false);
            GOD.PlayerInter.MobileJoystik.GetComponent<PanelJoystick>().joystick.PointerUp();
            GOD.PlayerInter.MobileJoystik.SetActive(false);
            GOD.Audio.PlayerSound.Stop();
            Time.timeScale = 0.5f;
            int x = Random.Range(-1, 1);
            if (x < 0)
                GOD.PlayerAnimation.m_Animator.Play("Death 1");
            else
                GOD.PlayerAnimation.m_Animator.Play("Death 2");
            GOD.PlayerAnimation.enabled = false;
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Die);
            GetComponent<CapsuleCollider>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            GOD.CameraScript.ToDeathCamera();
            GOD.Save.SaveGame(false);//смерть
            GOD.PlayerCam.GetComponent<Grayscale>().enabled = true;
          /*  for(int i=0; i<GOD.EnviromentContr.AllEnemies.Count; i++)
            {
                if (GOD.EnviromentContr.AllEnemies[i].IsPlayer)
                    GOD.EnviromentContr.AllEnemies[i].OutPlayer();
            }*/
            this.enabled = false;
        }

    }

    public void PlayerFall()//если игрок упал
    {
        GOD.Player.transform.position = new Vector3(0, 0, -86);
        GOD.Player.transform.rotation = Quaternion.Euler(new Vector3(0, -11, 0));
        GOD.DopEff.Ressurection.transform.position = GOD.Player.transform.position + new Vector3(0, 2, 0);
        GOD.DopEff.Ressurection.transform.SetParent(null);
        GOD.PlayerInter.StopRes();
        GOD.DialogS.ShowDialog("Oops");
    }



}
