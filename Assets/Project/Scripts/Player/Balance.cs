﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balance
{
    public static bool noAds
    {
        get
        {
            return PlayerPrefsX.GetBool("ChibiNoAds", false);
        }
        set
        {
            PlayerPrefsX.SetBool("ChibiNoAds", value);
            PlayerPrefs.Save();
        }
    }

    public static int Gem
    {
        get
        {
            return PlayerPrefs.GetInt("ChibiGem", 0);
        }
        set
        {
            PlayerPrefs.SetInt("ChibiGem", value);
            PlayerPrefs.Save();
        }
    }
}
