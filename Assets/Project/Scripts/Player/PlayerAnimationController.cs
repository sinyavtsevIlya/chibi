﻿using UnityEngine;
using System.Collections;

public class PlayerAnimationController : MonoBehaviour {

    public GameManager GOD;

    public Animator m_Animator;


    public GameObject OnHead, OnBody1, OnBody2, OnBody3, OnBody4, OnBody5, OnFeet1, OnFeet2;         // ссылки на надетые доспехи
    [Header("Места на персонаже:")]
    public GameObject Hand;
    public GameObject Spine;
    public GameObject Head, Body2, Body3, Body4, Body5, FeetL, FeetR;           // родители для доспехов
    public GameObject MantleParent;
    public GameObject OnMantle;
    public Transform MantleBone;
    public CapsuleCollider MantleCollider;


    // данные об объекте в руке
    ItemsPositionOnPlayer ItemPos;
   public GameObject ItemOnHand;                   // текущий предмет в руке
    public GameObject ItemOnHandEffect;                   // если есть эффект за ударом то он здесь
    public bool IsUse = false;               // в руке или на спине
    public bool IsWeapon = false;
    public bool IsInstrument = false;
    float useTimer = 0;
    string NextInHand = "";                   //какой предмет сейчас берем в руки
    string NameInHand="";
    public bool IsChangeWeather = false; // в процессе смены погоды

    bool NowOut = false;   // сейчас снимаем предмет

    int state = 1;              // состояния идла
    int oldstate;
    float AFKtimer = 0;        // сколько время игрок афк


    //Бой
    public bool CanAttackNext = true;
    public bool ImAttack = false; // переменная для отключения и включения крика боли
    public int AttackStep = 0;                     // атака в комбо
    float StepTimer = 0;
    bool StartStepTimer = false;

    //Погода
    string NewState = "";

    public void Init(GameManager G)
    {
        GOD = G;
        m_Animator = GetComponent<Animator>();
        Spine = transform.Find("Base Human").Find("Base HumanAss for elements").Find("Base HumanSpine1").Find("Base HumanSpine2").gameObject;
        Body2 = Spine.transform.Find("Base HumanRibcage").Find("Base HumanLCollarbone").gameObject;
        Body3 = Spine.transform.Find("Base HumanRibcage").Find("Base HumanRCollarbone").gameObject;
        Hand = Body3.transform.Find("Base HumanRUpperarm").Find("Base HumanRForearm").Find("Base HumanRPalm").gameObject;
        Body4 = Body2.transform.Find("Base HumanLUpperarm").Find("Base HumanLForearm").Find("Base HumanLPalm").gameObject;
        Head = Spine.transform.Find("Base HumanRibcage").Find("Base HumanNeck").Find("Base HumanHead").gameObject;
        Body5 = Hand;
        FeetL = transform.Find("Base Human").Find("Base HumanAss for elements").Find("Base HumanLThigh").Find("Base HumanLCalf").Find("Base HumanLFoot").gameObject;
        FeetR = transform.Find("Base Human").Find("Base HumanAss for elements").Find("Base HumanRThigh").Find("Base HumanRCalf").Find("Base HumanRFoot").gameObject;
        MantleParent = gameObject;
        MantleBone = Spine.transform.Find("Base HumanRibcage").Find("Mantle");
        MantleCollider = Spine.GetComponent<CapsuleCollider>();
    }


    //ДОСПЕХИ__________________________________________________________________________________________________
    public void GetArmor(GameObject G)
    {

        //for (int i = 0; i < GOD.PI.ArmorItems.Length; i++)
        //{
        // if (("Icon_" + GOD.PI.ArmorItems[i].name == G.name) || ("cloneIcon_" + GOD.PI.ArmorItems[i]==G.name))
        // {
        // GameObject Item = Instantiate(GOD.PI.ArmorItems[i]) as GameObject;
        // Item.name = GOD.PI.ArmorItems[i].name;
        GameObject p = GOD.PI.GetArmorItem(G.name);
        if (p)
        {
            GameObject Item =Instantiate(p) as GameObject;
            Item.name = p.name;
            GameObject parent = null;
            ItemsPosOnPlayer I;
            switch (G.GetComponent<InfoScript>().OnPlayerSlot)
            {
                case "Mantle":
                    parent = MantleParent;
                    OnMantle = Item;
                    Item.transform.SetParent(parent.transform);
                    Item.GetComponent<SkinnedMeshRenderer>().rootBone = MantleBone;
                    CapsuleCollider[] c = new CapsuleCollider[1];
                    c[0] = MantleCollider;
                    Item.GetComponent<Cloth>().capsuleColliders = c;
                    break;
                case "Head":
                    parent = Head;
                    OnHead = Item;
                    Item.transform.SetParent(parent.transform);
                    Item.transform.position = parent.transform.position;
                    Item.transform.localPosition = p.transform.localPosition;
                    Item.transform.localRotation = p.transform.localRotation;
                    break;
                case "Feet":
                    GameObject Feet1 = null, Feet2 = null;
                    if (G.name.Contains("Dimond"))
                    {
                        Feet1 = GOD.PI.DimondFeet1;
                        Feet2 = GOD.PI.DimondFeet2;
                    }
                    else if (G.name.Contains("Bone"))
                    {
                        Feet1 = GOD.PI.BoneFeet1;
                        Feet2 = GOD.PI.BoneFeet2;
                    }
                    else if (G.name.Contains("Iron"))
                    {
                        Feet1 = GOD.PI.IronFeet1;
                        Feet2 = GOD.PI.IronFeet2;
                    }
                    if (Feet1)
                    {

                        OnFeet1 = Item;
                        OnFeet1.transform.SetParent(FeetL.transform);
                        OnFeet1.transform.position = FeetL.transform.position;
                        I = OnFeet1.GetComponent<ItemsPosOnPlayer>();
                        OnFeet1.transform.localPosition = I.Pos(GOD.OurPlayer);
                        OnFeet1.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                        OnFeet1.transform.localScale = I.Sc(GOD.OurPlayer);

                        OnFeet2 = Instantiate(Feet2) as GameObject;
                        OnFeet2.transform.SetParent(FeetR.transform);
                        OnFeet2.transform.position = FeetR.transform.position;
                        I = OnFeet2.GetComponent<ItemsPosOnPlayer>();
                        OnFeet2.transform.localPosition = I.Pos(GOD.OurPlayer);
                        OnFeet2.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                        OnFeet2.transform.localScale = I.Sc(GOD.OurPlayer);
                    }
                    break;
                case "Body":
                    parent = Spine;
                    OnBody1 = Item;
                    Item.transform.SetParent(parent.transform);
                    Item.transform.position = parent.transform.position;
                    I = Item.GetComponent<ItemsPosOnPlayer>();
                    Item.transform.localPosition = I.Pos(GOD.OurPlayer);
                    Item.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                    Item.transform.localScale = I.Sc(GOD.OurPlayer);
                    GameObject ShoulderL = null, ShoulderR = null, HandL = null, HandR = null;
                    if (G.name.Contains("Dimond"))
                    {
                        ShoulderL = GOD.PI.DimondShoulderL;
                        ShoulderR = GOD.PI.DimondShoulderR;
                        HandL = GOD.PI.DimondHandL;
                        HandR = GOD.PI.DimondHandR;

                    }
                    else if (G.name.Contains("Bone"))
                    {
                        ShoulderL = GOD.PI.BoneShoulderL;
                        ShoulderR = GOD.PI.BoneShoulderR;
                        HandL = GOD.PI.BoneHandL;
                        HandR = GOD.PI.BoneHandR;
                    }
                    else if (G.name.Contains("Iron"))
                    {
                        ShoulderL = GOD.PI.IronShoulderL;
                        ShoulderR = GOD.PI.IronShoulderR;
                        HandL = GOD.PI.IronHandL;
                        HandR = GOD.PI.IronHandR;
                    }
                    else if (G.name.Contains("Leather"))
                    {
                        ShoulderL = GOD.PI.LeatherShoulderL;
                        ShoulderR = GOD.PI.LeatherShoulderR;
                        HandL = GOD.PI.LeatherHandL;
                        HandR = GOD.PI.LeatherHandR;
                    }

                    if (ShoulderL)
                    {
                        OnBody2 = Instantiate(ShoulderL);
                        OnBody2.transform.SetParent(Body2.transform);
                        OnBody2.transform.position = Body2.transform.position;
                        I = OnBody2.GetComponent<ItemsPosOnPlayer>();
                        OnBody2.transform.localPosition = I.Pos(GOD.OurPlayer);
                        OnBody2.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                        OnBody2.transform.localScale = I.Sc(GOD.OurPlayer);
                        OnBody3 = Instantiate(ShoulderR);
                        OnBody3.transform.SetParent(Body3.transform);
                        OnBody3.transform.position = Body3.transform.position;
                        I = OnBody3.GetComponent<ItemsPosOnPlayer>();
                        OnBody3.transform.localPosition = I.Pos(GOD.OurPlayer);
                        OnBody3.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                        OnBody3.transform.localScale = I.Sc(GOD.OurPlayer);

                        OnBody4 = Instantiate(HandL);
                        OnBody4.transform.SetParent(Body4.transform);
                        OnBody4.transform.position = Body4.transform.position;
                        I = OnBody4.GetComponent<ItemsPosOnPlayer>();
                        OnBody4.transform.localPosition = I.Pos(GOD.OurPlayer);
                        OnBody4.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                        OnBody4.transform.localScale = I.Sc(GOD.OurPlayer);

                        OnBody5 = Instantiate(HandR);
                        OnBody5.transform.SetParent(Body5.transform);
                        OnBody5.transform.position = Body5.transform.position;
                        I = OnBody5.GetComponent<ItemsPosOnPlayer>();
                        OnBody5.transform.localPosition = I.Pos(GOD.OurPlayer);
                        OnBody5.transform.localRotation = Quaternion.Euler(I.Rot(GOD.OurPlayer));
                        OnBody5.transform.localScale = I.Sc(GOD.OurPlayer);
                    }
                    break;
            }

        }
    }
    public void OutAmor(GameObject G)
    {
        switch (G.GetComponent<InfoScript>().OnPlayerSlot)
        {
            case "Mantle":
                Destroy(OnMantle);
                OnMantle = null;
                //Resources.UnloadUnusedAssets();
                break;
            case "Head":
                Destroy(OnHead);
                OnHead = null;
               // Resources.UnloadUnusedAssets();
                break;
            case "Feet":
                Destroy(OnFeet1);
                Destroy(OnFeet2);
                OnFeet1 = null;
                OnFeet2 = null;
               // Resources.UnloadUnusedAssets();
                break;
            case "Body":
                Destroy(OnBody1);
                OnBody1 = null;
                Destroy(OnBody2);
                Destroy(OnBody3);
                Destroy(OnBody4);
                Destroy(OnBody5);
                OnBody2 = null;
                OnBody3 = null;
                OnBody4 = null;
                OnBody5 = null;
               // Resources.UnloadUnusedAssets();
                break;
        }

    }
    //__________________________________________________________________________________________________________

    void Update()
    {
        if(IsWeapon)                  // отсчет когда оружие уберется за спину
        {
            useTimer += Time.deltaTime;
            if(useTimer>20)
            {
                useTimer = 0;
                IsUse = false;
                NowOut = false;
                if (m_Animator)
                m_Animator.Play("Close");
            }
        }
        if (m_Animator)
        {
            m_Animator.SetBool("IsUse", IsUse);
            m_Animator.SetBool("IsWeapon", IsWeapon);
            m_Animator.SetBool("IsInstrument", IsInstrument);
        }

        if (StartStepTimer)                                    // ожидание повторных нажатий игрока
        {
            StepTimer += Time.deltaTime;
            if(StepTimer>1.5f)
            {
                AttackStep = 0;
                StepTimer = 0;
                StartStepTimer = false;
            }
        }
    }

    void FixedUpdate()
    {
        AFKtimer += Time.deltaTime;
        if ((Input.touchCount == 1 || Input.GetButtonDown("Fire1"))||AFKtimer >=500)
            AFKtimer = 0;
    }

    public void ChoseIdleAnimation()                 // проигрывание идлов
    {
        if (IsUse)
        {
            if (IsWeapon)
            {
                m_Animator.Play("Idle Weapon");
            }
            else if (IsInstrument)
                m_Animator.Play("Idle Tool");
        }
        else
        {
            oldstate = state;
            int x = Random.Range(-1, 3);
            if (x < 0)
                state = 2;
            else
                state = 1;
            if (AFKtimer > 20f)
            {
                x = Random.Range(-1, 3);
                if (x < 0)
                    state = 3;
            }
            if (state > 1 && oldstate > 1)
                state = 1;
            switch (state)
            {
                case 1:
                    m_Animator.Play("Idle 1");
                    break;
                case 2:
                    m_Animator.Play("Idle 2");
                    break;
                case 3:
                    m_Animator.Play("Idle 3");
                    break;
            }
        }
    }

    public void GetItem(string itemName)  // достаем предмет
    {
        NextInHand = itemName;
        NoItem();
        GetAnimation();
    }
    void GetAnimation()
    {
        m_Animator.Play("Open");
    }
    public void NowGetItem()
    {
        if (NextInHand != "")
        {
            GameObject g = GOD.PI.GetPreItem(NextInHand);
            if (g)
                ItemOnHand = g;
            else
            {
                g = GOD.PI.GetHandItem(NextInHand);
                ItemOnHand = Instantiate(g) as GameObject;
            }
            ItemPos =g.GetComponent<ItemsPositionOnPlayer>();
            ItemOnHand.transform.SetParent(Hand.transform);
            ItemOnHand.transform.localPosition = ItemPos.PosOnHand(GOD.OurPlayer);
            ItemOnHand.transform.localRotation = Quaternion.Euler(ItemPos.RotOnHand(GOD.OurPlayer));
            Transform t = ItemOnHand.transform.Find("Effect");
            if (t)
                ItemOnHandEffect = t.gameObject;
            else
                ItemOnHandEffect = null;

            ItemOnHand.name = g.name;
            if (ItemOnHand.name.Contains("Sword"))
            {
                IsWeapon = true;
                IsInstrument = false;
            }
            else
            {
                IsInstrument = true;
                IsWeapon = false;
            }
            if (ItemOnHand.name != "Bucket")
                IsUse = true;
            NextInHand = "";
        }
    }

    public void OutItem()
    {
        NowOut = true;
        m_Animator.Play("Close");
    }

    void SetItemSpine()    // убираем предмет за спину или снимаем
    {
        //Debug.Log(NowOut + " " + IsWeapon + "  " + IsUse);
        if (NowOut)
        {
            NoItem();
            ItemOnHand = null;
            NowOut = false;
        }
        else
        {
            if (IsWeapon && !IsUse)
            {
                ItemOnHand.transform.SetParent(Spine.transform);
                ItemOnHand.transform.localPosition = ItemPos.PosOnPlayer(GOD.OurPlayer);
                ItemOnHand.transform.localRotation = Quaternion.Euler(ItemPos.RotOnPlayer(GOD.OurPlayer));
            }
        }
        
    }

    public void NoItem()
    {
        if (ItemOnHand != null)
        {
            GOD.DopEff.ReturnEffects();
            ItemOnHandEffect = null;
            if (ItemOnHand.transform.childCount > 0)
            {
                for (int i = 0; i < ItemOnHand.transform.childCount; i++)
                    ItemOnHand.transform.GetChild(i).transform.SetParent(GOD.DopEff.EffectsParent);
            }
            GOD.PI.RemovePreItem(ItemOnHand);
            //Destroy(ItemOnHand);
        }
        useTimer = 0;
        IsUse = false;
        CanAttackNext = true;
        IsWeapon = false;
        IsInstrument = false;
        ItemOnHand = null;
       //Resources.UnloadUnusedAssets(); это вот тормозит на устройстве
    }

    public void GetItemSpine()    // достаем предмет из-за спины
    {
        ItemOnHand.transform.SetParent(Hand.transform);
        ItemOnHand.transform.localPosition = ItemPos.PosOnHand(GOD.OurPlayer);
        ItemOnHand.transform.localRotation = Quaternion.Euler(ItemPos.RotOnHand(GOD.OurPlayer));
    }

    public void AttackAnimation()             // анимация атаки
    {
        if (ItemOnHand != null)
        {
            useTimer = 0f;
            if (ItemOnHand.name != "Bucket")
                IsUse = true;
            GetItemSpine();

            if (CanAttackNext) 
            {
                if (m_Animator.GetFloat("Speed") > 0)
                {
                  //  m_Animator.Play("Run Attack");
                }
                else
                {
                    if (AttackStep == 4)
                        AttackStep = 1;
                    else
                        AttackStep++;
                    m_Animator.SetInteger("WeaponState", AttackStep);
                    switch (AttackStep)
                    {
                        case 1:
                            GOD.DopEff.SwordAttack2.transform.SetParent(GOD.DopEff.EffectsParent);
                            m_Animator.Play("Sword attack 1");
                            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Attack1);
                            if (ItemOnHandEffect)
                                ItemOnHandEffect.SetActive(true);
                            break;
                        case 2:
                            m_Animator.Play("Sword attack 2");
                            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Attack2);
                            break;
                        case 3:
                            GOD.DopEff.SwordAttack2.transform.SetParent(GOD.DopEff.EffectsParent);
                            if (ItemOnHandEffect)
                                ItemOnHandEffect.SetActive(true);
                            m_Animator.Play("Sword attack 3");
                            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Attack3);
                            break;
                        case 4:
                            if (ItemOnHandEffect)
                                ItemOnHandEffect.SetActive(true);
                            m_Animator.Play("Sword attack 4");
                            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Attack4);
                            break;
                    }
                    CanAttackNext = false;
                    if(!ImAttack)
                        StartCoroutine(WaitStopAttack());
                }
            }
        }
        else   // если в руках нет оружия
        {
            if (CanAttackNext)
            {
                if (m_Animator.GetFloat("Speed") == 1)
                {
                    m_Animator.Play("Run");
                }
                else
                {
                    m_Animator.Play("Sword attack 1");
                    if (!ImAttack)
                        StartCoroutine(WaitStopAttack());
                }
            }
        }
    }

    IEnumerator WaitStopAttack()
    {
        ImAttack = true;
        yield return new WaitForSeconds(1);
        ImAttack = false;
    }

    public void SwordAttackSound()
    {
        if (ItemOnHand == null)
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Hand);
    }
   
    public void AfterWeaponIdle()
    {
        CanAttackNext = true;
        if (ItemOnHand && ItemOnHand.name.Contains("Bucket"))
            m_Animator.Play("Idle 1");
    }
  /*  public void HitEnemy()              // анимация врага, что его ударили
    {
        GOD.PlayerInter.HitEnemy();
    }*/
    public void AfterAttack()                  // обработка после атаки
    {
        if (ItemOnHand != null)
        {
            if (ItemOnHandEffect)
                ItemOnHandEffect.SetActive(false);
        }
        CanAttackNext = true;
        if (m_Animator.GetFloat("Speed") < 0)
        {
            StepTimer = 0;
            StartStepTimer = true;
            int x = AttackStep * 2;
            if (AttackStep > 0)
                x -= 2;
            if(ItemOnHand && ItemOnHand.name.Contains("Sword"))
                GOD.PlayerInter.DamageEnemy(x, false);
            else
                GOD.PlayerInter.DamageEnemy(x, true);
        }
        if(AttackStep==2)
            GOD.DopEff.SwordAttack2.transform.SetParent(GOD.DopEff.EffectsParent);
        if (AttackStep==4)
        {
            if ((ItemOnHand != null) && (GOD.DopEff.EffectsOn))
            {
                GOD.DopEff.BigHit.transform.SetParent(ItemOnHand.transform);
                    GOD.DopEff.BigHit.transform.localPosition = new Vector3(2, 3, 0);
                    GOD.DopEff.BigHit.Play();
                GOD.DopEff.BigHit.transform.SetParent(null);
            }
        }

    }

    public void Start2SwordAttack()
    {
        GOD.DopEff.SwordAttack2.transform.SetParent(GOD.DopEff.EffectsParent);

        if ((ItemOnHand != null) && (GOD.DopEff.EffectsOn))
        {
            GOD.DopEff.SwordAttack2.transform.SetParent(this.transform);
            GOD.DopEff.SwordAttack2.transform.localPosition = new Vector3(0.3f, 0.7f, 1.9f);
            GOD.DopEff.SwordAttack2.transform.localRotation = Quaternion.Euler(new Vector3(180, 0, 0));
            GOD.DopEff.SwordAttack2.transform.SetParent(null);
            GOD.DopEff.SwordAttack2.Play();
        }
    }

    public void CollectAnimation(bool State, string ObjType)        // анимация сбора
    {
        if (State)
        {
                useTimer = 0f;
                if (ItemOnHand && ItemOnHand.name != "Bucket")
                {
                    IsUse = true;
                    GetItemSpine();
                }
                switch (ObjType)
                {
                    case "Tree":
                        m_Animator.Play("Axe hit");
                        break;
                    case "BigTree":
                        m_Animator.Play("Axe hit");
                        break;
                    case "BreakIce":
                        m_Animator.Play("Circ hit");
                        break;
                    case "Stone":
                        m_Animator.Play("Circ hit");
                        break;
                    case "Heap":
                        m_Animator.Play("Shovel works");
                        break;
                case "Build":
                    m_Animator.Play("Circ hit");
                    break;
                case "Water":
                        m_Animator.Play("Collect Backet");
                        break;
                }
        }
        else
            m_Animator.Play("Idle 1");
    }

    public void FishingAnimation(int state)            // анимация рыбалки
    {
        switch(state)
        {
            case 1:
                m_Animator.Play("Rod");
                break;
            case 2:
                m_Animator.Play("Rod Fail");
                GOD.DopEff.Fail.transform.SetParent(GOD.Player.transform);
                GOD.DopEff.Fail.transform.localPosition = new Vector3(0, 4, 0);
                break;
            case 3:
                m_Animator.Play("Rod Welldone");
                GOD.DopEff.Sucsess.transform.SetParent(GOD.Player.transform);
                GOD.DopEff.Sucsess.transform.localPosition = new Vector3(0, 4, 0);
                break;
        }
    }

    public void StartFishingAgain()
    {
        GOD.FishingWindow.Fishing();
    }


    //ЗВУКИ_________________________________________________________________________________________


    public void AxeSound()
    {
        if (ItemOnHand != null)
        {
            int x = Random.Range(-1, 1);
            if (x < 0)
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Cut1);
            else
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.Cut2);
        }
        else
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Hand);
    }

    public void CirckSound()
    {
        if (ItemOnHand != null)
        {
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Pick);
        }
        else
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.Hand);
    }
    
    public void ShovelSound()
    {
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.Shovel);
    }

    public void AfterCircHit()
    {
        if((ItemOnHand!=null)&& (GOD.DopEff.EffectsOn))
        {
            if(ItemOnHand.name.Contains("Pick"))
            {
                GOD.DopEff.CirckHit.transform.SetParent(ItemOnHand.transform);
                GOD.DopEff.CirckHit.transform.localPosition = new Vector3(2, 3, 0);
                GOD.DopEff.CirckHit.Play();
            }
        }
        GOD.PlayerInter.ExtractObject();
    }

    public void AfterAxeHit()
    {
        if ((ItemOnHand != null)&&(GOD.DopEff.EffectsOn))
        {
            if (ItemOnHand.name.Contains("Axe"))
            {
                GOD.DopEff.AxeHit.transform.SetParent(ItemOnHand.transform);
                GOD.DopEff.AxeHit.transform.localPosition = new Vector3(2, 3, 0);
                GOD.DopEff.AxeHit.Play();
            }
        }
        GOD.PlayerInter.ExtractObject();
    }
    public void AfterShovel()
    {
        GOD.PlayerInter.ExtractObject();
    }
    // Анимация смены погоды
    public void HelpStartChangeWeather(string s)
    {
        NewState = s;
        IsChangeWeather = true;
        if(ItemOnHand)
            NameInHand = ItemOnHand.name;
        m_Animator.Play("OpenStaff");
    }
    void StartChangeWeather()
    {
        NoItem();
        ItemsPositionOnPlayer I = GOD.PI.WeatherStaff.GetComponent<ItemsPositionOnPlayer>();
        GOD.PI.WeatherStaff.transform.SetParent(Hand.transform);
        GOD.PI.WeatherStaff.SetActive(true);
        GOD.PI.WeatherStaff.transform.localPosition = I.PosOnHand(GOD.OurPlayer);
        GOD.PI.WeatherStaff.transform.localRotation = Quaternion.Euler(I.RotOnHand(GOD.OurPlayer));
        m_Animator.Play("Staff attack 4");
    }

    public void InChangeWeather()
    {
        GOD.Audio.Sound.PlayOneShot(GOD.Audio.StartWeather);
        GOD.Audio.WeatherSound.enabled = true;
        GOD.PlayerInter.MinusIngridient(NewState);
        GOD.PI.WeatherStaff.transform.SetParent(null);
        GOD.PI.WeatherStaff.transform.localRotation = Quaternion.Euler(new Vector3(0,90,0));
        IsChangeWeather = false;
        switch (NewState)
        {
            case "Sun":
                GOD.PI.Effect1Sun.SetActive(true);
                break;
            case "Snow":
                GOD.PI.Effect1Snow.SetActive(true);
                break;
            case "Water":
                GOD.PI.Effect1Water.SetActive(true);
                break;
        }
        GOD.EnviromentContr.ChangeWeather(NewState);
        AfterChangeWeather();
    }


    public void AfterChangeWeather()  //возвращаем в руки то что там было
    {
        if (NameInHand != "")
            GetItem(NameInHand);
        WeatherStafAnimated();
    }
    public void WeatherStafAnimated()
    {
        GOD.PI.WeatherStaff.transform.GetChild(0).gameObject.SetActive(true);
        GOD.PI.WeatherStaff.transform.GetChild(1).gameObject.SetActive(true);
    }
    public void StopChangeWeather()
    {
        switch (NewState)
        {
            case "Sun":
                GOD.PI.Effect2Sun.transform.SetParent(null);
                GOD.PI.Effect2Sun.SetActive(true);
                break;
            case "Snow":
                GOD.PI.Effect2Snow.transform.SetParent(null);
                GOD.PI.Effect2Snow.SetActive(true);
                break;
            case "Water":
                GOD.PI.Effect2Water.transform.SetParent(null);
                GOD.PI.Effect2Water.SetActive(true);
                break;
        }
        StartCoroutine(EndStaff());
    }
    IEnumerator EndStaff()
    {
        GOD.PI.WeatherStaff.SetActive(false);
        GOD.PI.WeatherStaff.transform.GetChild(0).gameObject.SetActive(false);
        GOD.PI.WeatherStaff.transform.GetChild(1).gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        GOD.PI.Effect2Sun.transform.SetParent(GOD.PI.WeatherStaff.transform);
        GOD.PI.Effect2Snow.transform.SetParent(GOD.PI.WeatherStaff.transform);
        GOD.PI.Effect2Water.transform.SetParent(GOD.PI.WeatherStaff.transform);
        GOD.PI.Effect1Sun.SetActive(false);
        GOD.PI.Effect2Sun.SetActive(false);
        GOD.PI.Effect1Snow.SetActive(false);
        GOD.PI.Effect2Snow.SetActive(false);
        GOD.PI.Effect1Water.SetActive(false);
        GOD.PI.Effect2Water.SetActive(false);

        if (GOD.IsTutorial && GOD.Tutor.TutorialStep == 15)
            GOD.Tutor.NextStep = true;
    }

    public void FastStopChangeWeather() //прерывание установки посоха
    {
        if (NameInHand != "")
        {
            GetItem(NameInHand);
            NameInHand = "";
        }
        GOD.PI.WeatherStaff.SetActive(false);
        GOD.PI.WeatherStaff.transform.GetChild(0).gameObject.SetActive(false);
        GOD.PI.WeatherStaff.transform.GetChild(1).gameObject.SetActive(false);
        IsChangeWeather = false;
    }

  
}
