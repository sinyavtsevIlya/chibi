﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AfterDeadBag : MonoBehaviour {

    public int MyNumber = -1;
    public List<InfoScript> MyIcons = new List<InfoScript>();

    public void StartStopFly()
    {
        StartCoroutine(StopFly());
    }
    IEnumerator StopFly()
    {
        yield return new WaitForSeconds(2f);
        tag = "DeadLut";
        Transform t = transform.Find("Trail");
        if (t)
            Destroy(t.gameObject);
    }
}
