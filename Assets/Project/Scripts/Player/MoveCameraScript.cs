﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

public class MoveCameraScript : MonoBehaviour
{
    public GameManager GOD;
    public GameObject Player;
    public Camera PlayerCamera;
    public Transform CameraParent;
    public Canvas canvas;
    float CanvasX = 0;
    float StartAngle=0;
    float CurrentAngle = 0;

    bool CameraActive = true;
    bool AfterNear = false;

    public bool UsialCamera = true;
    public bool OnWalkPanel = false;

    Vector3 target;
    float OldMousePosition;
    float dop = 0;

    public int RotationTouch = 0;

    float kx = 1; //коэффицент для сдвига камера при строительстве
    float ky = 1;
    float k = 5; //скорость движения камеры
    Vector3 CameraStandartPosition;
    private void Start()
    {
        CanvasX = canvas.GetComponent<RectTransform>().sizeDelta.x;
        kx = Screen.width / 10;
        ky = Screen.height / 10;
        k = Screen.width / 100;
        CameraStandartPosition = CameraParent.transform.localPosition;
    }
    void Update()
    {
        if (GOD.PlayerContr.InBuilding)//камера режима строительства
        {
            if (GOD.BuildingS.BuildPanelDown)
            {
                if (Input.mousePosition.x >= Screen.width - kx)
                {
                    if (CameraParent.transform.localPosition.x<CameraStandartPosition.x+20)
                       CameraParent.transform.Translate(Vector3.right * k * Time.deltaTime);
                }
                if (Input.mousePosition.x <= kx)
                {
                    if (CameraParent.transform.localPosition.x > CameraStandartPosition.x - 20)
                        CameraParent.transform.Translate(-Vector3.right * k * Time.deltaTime);
                }
                if (Input.mousePosition.y >= Screen.height - ky)
                {
                    if (CameraParent.transform.localPosition.z < CameraStandartPosition.z + 20)
                        CameraParent.transform.Translate(new Vector3(0,0,1) * k * Time.deltaTime);
                }
                if (Input.mousePosition.y <= ky)
                {
                    if (CameraParent.transform.localPosition.z > CameraStandartPosition.z - 20)
                        CameraParent.transform.Translate(-Vector3.forward * k * Time.deltaTime);
                }
            }
        }
        else
        {
            if (UsialCamera)
                transform.position = Player.transform.position;
            else
            {
                //движение камеры
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");

                if (h == 0 && v == 0)
                {
                    target = Vector3.Lerp(target, Vector3.zero, Time.deltaTime * 3f);
                }
                else
                {
                    target = Vector3.Lerp(target, Player.transform.forward * 2f, Time.deltaTime * 2f);
                }
                transform.position = Player.transform.position + target;
            }
        }
        //поворот
        if (CameraActive)
        {
#if (UNITY_EDITOR|| UNITY_STANDALONE_WIN)
            {
                if (OnWalkPanel || (GOD.BuildingS.BuildPanelDown && GOD.BuildingS.block==null))
                {
                    //float y_axis = Input.touches[x].deltaPosition.x * 2;
                    if (Input.GetMouseButtonDown(0))
                        OldMousePosition = Input.mousePosition.x;
                    else
                    {
                        float y_axis = Input.mousePosition.x - OldMousePosition;
                        transform.Rotate(Vector3.up, (y_axis / CanvasX) * 100f, Space.Self);
                        OldMousePosition = Input.mousePosition.x;
                    }
                }
            }
#endif
#if ((UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR)
            {
                if (OnWalkPanel|| (GOD.BuildingS.BuildPanelDown && GOD.BuildingS.block==null))
                {
                    if(Input.touchCount >RotationTouch)
                     {
                          float y_axis = Input.touches[RotationTouch].deltaPosition.x;
                           if(!GOD.BuildingS.BuildPanelDown)
                        transform.Rotate(Vector3.up, (y_axis/CanvasX) *100f, Space.Self);
                        else
                            CameraParent.Rotate(Vector3.up, (y_axis / CanvasX) * 100f, Space.Self);
                     }
                    else
                     RotationTouch=0;
                }
            }
#endif
        }

        if (AfterNear)
            transform.RotateAround(Player.transform.position, Vector3.up, Time.deltaTime * 10);
    }

    public void TeleportCamera()
    {
        transform.position = Player.transform.position;
    }
    public void ToDeathCamera()
    {
        CameraActive = false;
        StartCoroutine(NearCamera());
    }

    IEnumerator NearCamera()
    {
         while (PlayerCamera.transform.localPosition.y > 8)
         {
             PlayerCamera.transform.localPosition += new Vector3(0, -0.1f, 0.1f);
             yield return new WaitForFixedUpdate();
         }
         PlayerCamera.transform.localPosition = new Vector3(0, 8, -8);
        yield return new WaitForFixedUpdate();
        GOD.PlayerInter.OpenDead();
        AfterNear = true;
    }

    public void ToAliveCamera()
    {
        AfterNear = false;
        StartCoroutine(FarCamera());
    }

    IEnumerator FarCamera()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        while (PlayerCamera.transform.localPosition.y <14)
        {
            PlayerCamera.transform.localPosition += new Vector3(0, 0.1f, -0.1f);
            yield return new WaitForFixedUpdate();
        }
        PlayerCamera.transform.localPosition = new Vector3(0, 14, -14);
        CameraActive = true;
    }

   public void AfterBuildingCamera()
    {
        CameraParent.localPosition = CameraStandartPosition;
    }

}
