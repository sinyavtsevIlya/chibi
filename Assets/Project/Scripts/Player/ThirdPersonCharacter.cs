using UnityEngine;


namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class ThirdPersonCharacter : MonoBehaviour
    { 
		[SerializeField] float m_MovingTurnSpeed = 360;
		[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(1f, 10f)][SerializeField] float m_GravityMultiplier = 10f;
		//[SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
		public float m_MoveSpeedMultiplier = 1f;
		//[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float m_GroundCheckDistance = 0.1f;

        public GameManager GOD;
        public TutorialController Tut;
        public string CurrentUnder = ""; // ��� ������ ��� ������ ������
        public string CurrentUnderTag = ""; // ��� ������ ��� ������ ������
        PlayerAnimationController PlayerAnimation;
		Rigidbody m_Rigidbody;
		public bool m_IsGrounded;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;
		float m_ForwardAmount;
		Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;
        public bool UsePlayerMove = true;

        public void Init()
		{
           // m_Animator = GetComponent<Animator>();
            m_Rigidbody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
			m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;

			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;
            if (GOD)
                PlayerAnimation = GOD.PlayerAnimation;

        }


		public void Move(Vector3 move, bool crouch, bool jump)
		{
            if (UsePlayerMove)
            {
                // convert the world relative moveInput vector into a local-relative
                // turn amount and forward amount required to head in the desired
                // direction.
                if (move.magnitude > 1f) move.Normalize();
                move = transform.InverseTransformDirection(move);
                CheckGroundStatus();
                move = Vector3.ProjectOnPlane(move, m_GroundNormal);
                m_TurnAmount = Mathf.Atan2(move.x, move.z);
                m_ForwardAmount = move.z;

                ApplyExtraTurnRotation();

                // control and velocity handling is different when grounded and airborne:
                if (m_IsGrounded)
                {
                    HandleGroundedMovement(crouch, jump);
                }
                else
                {
                    HandleAirborneMovement();
                }

                ScaleCapsuleForCrouching(crouch);
                PreventStandingInLowHeadroom();

                // send input and other state parameters to the animator
                UpdateAnimator(move);
            }
           
		}


		void ScaleCapsuleForCrouching(bool crouch)
		{
			if (m_IsGrounded && crouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, ~0, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Crouching = false;
			}
		}

		void PreventStandingInLowHeadroom()
		{
			// prevent standing up in crouch-only zones
			if (!m_Crouching)
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, ~0, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
				}
			}
		}


		void UpdateAnimator(Vector3 move)
		{
            if (move.x != 0 || move.y != 0 || move.z != 0)
            {
                float x = Mathf.Round(m_Rigidbody.velocity.magnitude);
                x = Mathf.Clamp(x, 0f, 3f);
                PlayerAnimation.m_Animator.SetFloat("Speed", x);
                switch((int)x)
                {
                    case 0:
                        GOD.Audio.PlayerSound.pitch = 0.6f;
                        break;
                    case 2:
                        GOD.Audio.PlayerSound.pitch = 0.8f;
                        break;
                    case 3:
                        GOD.Audio.PlayerSound.pitch = 1f;
                        break;
                }


                    if (!PlayerAnimation.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Run") && !PlayerAnimation.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Run Weapon") && !PlayerAnimation.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Run Tool"))
                    {
                        if (PlayerAnimation.IsInstrument && GOD.PlayerAnimation.IsUse)
                            PlayerAnimation.m_Animator.Play("Run Tool");
                        else if (PlayerAnimation.IsWeapon && GOD.PlayerAnimation.IsUse)
                            PlayerAnimation.m_Animator.Play("Run Weapon");
                        else
                            PlayerAnimation.m_Animator.Play("Run");

                       // if (PlayerAnimation.IsChangeWeather)           //������������� ����� ������
                          //  PlayerAnimation.FastStopChangeWeather();
                    if (!PlayerAnimation.CanAttackNext) //������������� ������
                    {
                        if (PlayerAnimation.ItemOnHand != null)
                        {
                            if (PlayerAnimation.ItemOnHand.transform.childCount > 0)
                                PlayerAnimation.ItemOnHand.transform.GetChild(0).gameObject.SetActive(false);
                        }
                        PlayerAnimation.CanAttackNext = true;
                    }

                    if (GOD)
                    {
                        if (!GOD.Audio.PlayerSound.isPlaying)
                            GOD.Audio.PlayerSound.Play();
                    }
                    }
            }
            else
            {

                    if (PlayerAnimation)
                        PlayerAnimation.m_Animator.SetFloat("Speed", -1);
                if (GOD)
                {
                    if (GOD.Audio.PlayerSound.isPlaying)
                        GOD.Audio.PlayerSound.Stop();
                }
            }

 
        }


		public void HandleAirborneMovement()
		{
			// apply extra gravity from multiplier:
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce(extraGravityForce);

			m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
		}


		void HandleGroundedMovement(bool crouch, bool jump)
		{
			// check whether conditions are right to allow a jump:
			if (jump && !crouch)
			{
				// jump!
				m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_IsGrounded = false;
				m_GroundCheckDistance = 0.1f;
			}
		}

		void ApplyExtraTurnRotation()
		{
			// help the character turn faster (this is in addition to root rotation in the animation)
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}


        public void MoveCharacter(Vector3 direction)
        {
            if (UsePlayerMove)
            {
                m_GravityMultiplier = 6;
                Vector3 v = direction * m_MoveSpeedMultiplier;
                v.y = m_Rigidbody.velocity.y;
                m_Rigidbody.velocity = v;
            }
        }

        public void MoveCharacterUp(Vector3 direction)
        {
            m_GravityMultiplier = 0;
            Vector3 v = direction * m_MoveSpeedMultiplier;
            m_Rigidbody.velocity = v;
        }


        /*public void OnAnimatorMove()
		{
			// we implement this function to override the default root motion.
			// this allows us to modify the positional speed before it's applied.
			if (m_IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (transform.parent.position* m_MoveSpeedMultiplier) / Time.deltaTime;
                print("!!1!" + m_Animator.deltaPosition);
                // we preserve the existing y part of the current velocity.
                v.y = m_Rigidbody.velocity.y;
				m_Rigidbody.velocity = v;
               
			}
		}*/


        void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
			// 0.1f is a small offset to start the ray from inside the character
			// it is also good to note that the transform position in the sample assets is at the base of the character
			if (Physics.Raycast(transform.position + (Vector3.up * 3f), Vector3.down, out hitInfo))
			{
                CurrentUnder = hitInfo.collider.name;
                if (CurrentUnder=="IceBridge1")                  //������ �������� �����
                    GOD.Progress.SetAchivments(5);
                CurrentUnderTag = hitInfo.collider.tag;
                if (CurrentUnder != "Ground" && CurrentUnderTag != "WaterPuddle")
                {
                    if (GOD.PlayerInter.ChangeWeatherBtn.color.a != 0.5f)
                        GOD.PlayerInter.ChangeWeatherBtn.color = new Color(1, 1, 1, 0.5f);
                }
                else
                {
                    if (GOD.PlayerInter.ChangeWeatherBtn.color.a != 1)
                        GOD.PlayerInter.ChangeWeatherBtn.color = new Color(1, 1, 1, 1);
                }

                m_GroundNormal = hitInfo.normal;
                if(Mathf.Abs(transform.position.y - hitInfo.transform.position.y) < m_GroundCheckDistance)
                    m_IsGrounded = true;
                else
                {
                    m_IsGrounded = false;
                    m_GroundNormal = Vector3.up;
                }
                    if (hitInfo.collider.name == "Bridge" || hitInfo.collider.tag == "BigTree") //���� ��� ������ ���� ��� ������
                    {
                        GOD.Audio.SetPlayer(2);
                    }
                    else
                    {
                        if (hitInfo.collider.tag == "Ground" && hitInfo.collider.name=="Ground") //���� ��� ������ �����
                        {
                            Renderer rend = hitInfo.transform.GetComponent<Renderer>();
                            Material mat = rend.sharedMaterial;
                            Texture2D tex = mat.GetTexture("_Control") as Texture2D;
                            int texX = (int)(tex.width * hitInfo.textureCoord.x);
                            int texY = (int)(tex.height * hitInfo.textureCoord.y);
                            Color color = tex.GetPixel(texX, texY);
                            float x = color.r;  //���������� ����� ���� �����������
                            if (x < color.g)
                                x = color.g;
                            if (x < color.b)
                                x = color.b;

                        if (GOD)
                        {
                            if (x == color.r)
                                GOD.Audio.SetPlayer(1);
                            else
                                GOD.Audio.SetPlayer(0);
                        }
                        }
                        else
                        {
                        if (GOD)
                        {
                            if (hitInfo.collider.tag == "WaterPuddle")
                                GOD.Audio.SetPlayer(3);
                            else if(hitInfo.collider.tag == "GroundHouse")
                                GOD.Audio.SetPlayer(2);
                            else
                                GOD.Audio.SetPlayer(0);
                        }   
                        }
                    }
                //	m_Animator.applyRootMotion = true;
            }
			else
			{
				m_IsGrounded = false;
				m_GroundNormal = Vector3.up;
			//	m_Animator.applyRootMotion = false;
			}
		}
	}
}
