﻿using UnityEngine;
using UnityEngine.UI;

public class FishingWindowScript : MonoBehaviour {

    public GameManager GOD;
    // Рыбалка
    [Header("Рыбалка:")]
    public Text FishingText;
    public GameObject FishingWindow, FishingTutor;
    bool IsFishing = false;
    bool IsFishingDown = false; //нажата ли кнопка рыбалки
    public RectTransform FishingCircle, EndFishing;
    public Image FishingSprite;
    public Text FishingStage, FishingStageNumber, FishingXLut;
    public Animator FishingStageAnimator;
    public int FishingSpeed = 1;
    int[] ChestProbability = new int[] { 0, 2, 5, 10, 15, 20, 25, 30, 35, 40 };   // вероятность выпадения сундука
    int[] FishProbability = new int[] { 0, 20, 30, 40, 50, 60, 70, 80, 90, 100 };   // вероятность выпадения рыбы
    float FishingTimer = 0;



    void Update()
    {
        if (GOD.PlayerContr.IsAlive)
        {
            if (IsFishing)
            {
                FishingTimer += Time.deltaTime;
                if (FishingTimer >= 0.01f)
                {
                    FishingTimer = 0;
                    if (!GOD.Audio.FishingSound.activeSelf)
                        GOD.Audio.FishingSound.SetActive(true);

                    FishingCircle.sizeDelta = new Vector2(FishingCircle.sizeDelta.x - FishingSpeed, FishingCircle.sizeDelta.y - FishingSpeed);
                    if (FishingCircle.sizeDelta.x >= EndFishing.sizeDelta.x - 20 && FishingCircle.sizeDelta.x <= EndFishing.sizeDelta.x + 20)
                    {
                        if (FishingSprite.color != new Color(1, 0.72f, 0, 1))
                        {
                            FishingSprite.color = new Color(1, 0.72f, 0, 1);
                            if (FishingTutor.activeSelf)
                                FishingText.text = Localization.instance.getTextByKey("#FishingTutorial2");
                        }
                    }
                    else
                    {
                        if (FishingSprite.color != new Color(1, 1, 1, 1))
                            FishingSprite.color = new Color(1, 1, 1, 1);
                        if (FishingTutor.activeSelf)
                            FishingText.text = Localization.instance.getTextByKey("#FishingTutorial1");
                    }
                    if (FishingCircle.sizeDelta.x <= 0)
                        OnFish();
                }
            }
        }
    }

    public void FirstFishing()
    {
        FishingSpeed = 1;
        FishingTutor.SetActive(true);
        IsFishingDown = false;
        Fishing();
    }
    public void Fishing()
    {
        IsFishingDown = false;
        if (GOD.InventoryContr.CanAdd(" ", 1))
        {
            if (GOD.PlayerContr.OnHand)
            {
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.FishingStart);
                GOD.PlayerAnimation.FishingAnimation(1);
                FishingCircle.sizeDelta = new Vector2(300, 300);
                FishingText.text = Localization.instance.getTextByKey("#FishingTutorial1");
                FishingWindow.SetActive(true);
                IsFishing = true;
                FishingStageNumber.text = "" + FishingSpeed;
                FishingStageAnimator.Play("FishingStage");
                if (FishingSpeed >= 8)
                    FishingXLut.text = Localization.instance.getTextByKey("#LutIncreased") + "x2";
                else if (FishingSpeed == 10)
                    FishingXLut.text = Localization.instance.getTextByKey("#LutIncreased") + "x3";
                else
                    FishingXLut.text = "";
            }
            else
                Closefish();
        }
        else
        {
            Closefish();
            GOD.DialogS.ShowDialog("CantAdd");
        }
    }

    public void OnFish()       // нажатие на кнопку ловли
    {
        if (!IsFishingDown)//если еще не нажата
        {
            IsFishingDown = true;
            GOD.Audio.FishingSound.SetActive(false);
            Vector2 v1 = new Vector2(EndFishing.sizeDelta.x - 20, EndFishing.sizeDelta.y - 20);
            Vector2 v2 = new Vector2(EndFishing.sizeDelta.x + 20, EndFishing.sizeDelta.y + 20);
            if (FishingCircle.sizeDelta.x <= v2.x && FishingCircle.sizeDelta.x >= v1.x)
            {
                FishingTutor.SetActive(false);
                IsFishing = false;
                GOD.Progress.SetAchivments(11); //ачивка рыбака
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.FishingSucsess);
                GOD.PlayerAnimation.FishingAnimation(3);
                int x = Random.Range(0, 101);
                if (x <= ChestProbability[FishingSpeed - 1])
                {
                    switch (GOD.PlayerContr.RodType)
                    {
                        case "FragileRod":
                            GOD.InventoryContr.AddToInventory("FishChestGreen", 1);
                            GOD.PlayerInter. GOD.PlayerInter.ShowNewItem("FishChestGreen", 1);
                            break;
                        case "NormalRod":
                            x = Random.Range(0, 101);
                            if (x <= 25)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestBlue", 1);
                                GOD.PlayerInter. GOD.PlayerInter.ShowNewItem("FishChestBlue", 1);
                            }
                            else
                            {
                                GOD.InventoryContr.AddToInventory("FishChestGreen", 1);
                                GOD.PlayerInter. GOD.PlayerInter. GOD.PlayerInter.ShowNewItem("FishChestGreen", 1);
                            }
                            break;
                        case "StrongRod":
                            x = Random.Range(0, 101);
                            if (x <= 20)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestPurpule", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestPurpule", 1);
                            }
                            else if (x > 0 && x <= 45)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestBlue", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestBlue", 1);
                            }
                            else
                            {
                                GOD.InventoryContr.AddToInventory("FishChestGreen", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestGreen", 1);
                            }
                            break;
                        case "DurableRod":
                            x = Random.Range(0, 101);
                            if (x <= 15)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestYellow", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestYellow", 1);
                            }
                            else if (x > 15 && x <= 35)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestPurpule", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestPurpule", 1);
                            }
                            else if (x > 35 && x <= 60)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestBlue", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestBlue", 1);
                            }
                            else
                            {
                                GOD.InventoryContr.AddToInventory("FishChestGreen", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestGreen", 1);
                            }
                            break;
                        case "DimondRod":
                            x = Random.Range(0, 101);
                            if (x <= 10)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestRed", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestRed", 1);
                                GOD.Progress.SetAchivments(10); //ачивка Везунчик
                            }
                            else if (x > 10 && x <= 25)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestYellow", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestYellow", 1);
                            }
                            else if (x > 25 && x <= 45)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestPurpule", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestPurpule", 1);
                            }
                            else if (x > 45 && x <= 70)
                            {
                                GOD.InventoryContr.AddToInventory("FishChestBlue", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestBlue", 1);
                            }
                            else
                            {
                                GOD.InventoryContr.AddToInventory("FishChestGreen", 1);
                                 GOD.PlayerInter.ShowNewItem("FishChestGreen", 1);
                            }
                            break;
                    }
                }
                else
                {
                    x = Random.Range(0, 101);                                                 // вероятность выпадения рыбы
                    if (x <= FishProbability[FishingSpeed - 1])
                    {
                        int FishCount = 1;
                        if (FishingSpeed >= 8)
                            FishCount = 2;
                        if (FishingSpeed == 10)
                            FishCount = 3;
                        switch (GOD.PlayerContr.RodType)
                        {
                            case "FragileRod":
                                GOD.InventoryContr.AddToInventory("Sprat", FishCount);
                                 GOD.PlayerInter.ShowNewItem("Sprat", FishCount);
                                break;
                            case "NormalRod":
                                x = Random.Range(0, 101);
                                if (x < 50)
                                {
                                    GOD.InventoryContr.AddToInventory("Carp", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Carp", FishCount);
                                }
                                else
                                {
                                    GOD.InventoryContr.AddToInventory("Sprat", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Sprat", FishCount);
                                }
                                break;
                            case "StrongRod":
                                x = Random.Range(0, 101);
                                if (x < 33)
                                {
                                    GOD.InventoryContr.AddToInventory("Som", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Som", FishCount);
                                }
                                else if (x >= 33 && x <= 66)
                                {
                                    GOD.InventoryContr.AddToInventory("Carp", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Carp", FishCount);
                                }
                                else
                                {
                                    GOD.InventoryContr.AddToInventory("Sprat", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Sprat", FishCount);
                                }
                                break;
                            case "DurableRod":
                                x = Random.Range(0, 101);
                                if (x <= 15)
                                {
                                    GOD.InventoryContr.AddToInventory("Trout", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Trout", FishCount);
                                }
                                else if (x > 15 && x <= 35)
                                {
                                    GOD.InventoryContr.AddToInventory("Som", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Som", FishCount);
                                }
                                else if (x > 35 && x <= 60)
                                {
                                    GOD.InventoryContr.AddToInventory("Carp", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Carp", FishCount);
                                }
                                else
                                {
                                    GOD.InventoryContr.AddToInventory("Sprat", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Sprat", FishCount);
                                }
                                break;
                            case "DimondRod":
                                x = Random.Range(0, 101);
                                if (x <= 10)
                                {
                                    GOD.InventoryContr.AddToInventory("Salmon", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Salmon", FishCount);
                                }
                                else if (x > 10 && x <= 25)
                                {
                                    GOD.InventoryContr.AddToInventory("Trout", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Trout", FishCount);
                                }
                                else if (x > 25 && x <= 45)
                                {
                                    GOD.InventoryContr.AddToInventory("Som", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Som", FishCount);
                                }
                                else if (x > 45 && x <= 70)
                                {
                                    GOD.InventoryContr.AddToInventory("Carp", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Carp", FishCount);
                                }
                                else
                                {
                                    GOD.InventoryContr.AddToInventory("Sprat", FishCount);
                                     GOD.PlayerInter.ShowNewItem("Sprat", FishCount);
                                }
                                break;
                        }
                    }
                    else
                    {
                        GOD.InventoryContr.AddToInventory("Alga", 1);
                         GOD.PlayerInter.ShowNewItem("Alga", 1);
                    }
                }
                if (FishingSpeed < 10)
                    FishingSpeed++;
                GOD.PlayerInter.OpenInventory();
                GOD.PlayerInter.CloseInventoryImmediatly();
            }
            else
            {
                IsFishing = false;
                GOD.Audio.Sound.PlayOneShot(GOD.Audio.FishingFail);
                GOD.PlayerAnimation.FishingAnimation(2);
                GOD.PlayerInter.ChangeDurability(GOD.PlayerContr.OnHand);
                FishingSpeed = 1;
            }
            if (GOD.PlayerInter.ObjectTarget.GetComponent<FishPlaceScript>().ResourseCount > 0)             // иссякание источника рыбы
                GOD.PlayerInter.ObjectTarget.GetComponent<FishPlaceScript>().ResourseCount--;
            else
            {
                Closefish();
                GOD.FishingP.RemoveFishingPlace(GOD.PlayerInter.ObjectTarget);
            }
        }
    }

    public void Closefish()
    {
        IsFishing = false;
        GOD.Audio.FishingSound.SetActive(false);
        FishingWindow.SetActive(false);
        GOD.PlayerAnimation.CollectAnimation(false, "w");
    }
}
