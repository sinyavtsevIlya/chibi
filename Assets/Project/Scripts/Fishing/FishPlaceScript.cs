﻿using UnityEngine;
using System.Collections;

public class FishPlaceScript : MonoBehaviour {

    public GameManager GOD;
    public Vector3[] Massiv;
    public bool[] CheckMassiv;
    public int PlaceInMassiv;
    public int ResourseCount = 5;

    GameObject FishSprite;

    void Start()
    {
        FishSprite = transform.GetChild(1).gameObject;
    }

    public void Update()
    {
        if(FishSprite)
            FishSprite.transform.LookAt(GOD.PlayerCam.transform);
    }
}
