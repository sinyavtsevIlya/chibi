﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FishingPlace : MonoBehaviour {

    public GameManager GOD;

    public Transform RippleOnGreenIsland;  //местонахождение ряби для зеленого острова
    Vector3[] LakePositions = new Vector3[15];
    bool[] OnLakeActive = new bool[15];        // запоминаем какие точк активны

    public Transform RippleOnWaterIsland;  //местонахождение ряби для всех водных островов

    Vector3[] RiverPositions = new Vector3[24];
    Vector3[] RiverPositions2 = new Vector3[31];
    Vector3[] RiverPositions3 = new Vector3[13];
    bool[] OnRiverActive = new bool[26];        // запоминаем какие точк активны
    bool[] OnRiverActive2 = new bool[31];        // запоминаем какие точк активны
    bool[] OnRiverActive3 = new bool[13];        // запоминаем какие точк активны


    public void Init()
    {
        SetPositions();
       FindFishingPlace(RiverPositions, 6, OnRiverActive, RippleOnWaterIsland);
        FindFishingPlace(RiverPositions2, 6, OnRiverActive2, RippleOnWaterIsland);
        FindFishingPlace(RiverPositions3, 3, OnRiverActive3, RippleOnWaterIsland);
        FindFishingPlace(LakePositions, 3, OnLakeActive, RippleOnGreenIsland);
    }
    void SetPositions()
    {
        RiverPositions[0] = new Vector3(44, -1.5f, 197);  // река первого отсрова
        RiverPositions[1] = new Vector3(34, -1.5f, 197);
        RiverPositions[2] = new Vector3(23, -1.5f, 200);
        RiverPositions[3] = new Vector3(23, -1.5f, 220);
        RiverPositions[4] = new Vector3(22, -1.5f, 240);
        RiverPositions[5] = new Vector3(22, -1.5f, 260);
        RiverPositions[6] = new Vector3(23, -1.5f, 277);
        RiverPositions[7] = new Vector3(-19, -1.5f, 283);
        RiverPositions[8] = new Vector3(-15, -1.5f, 283);
        RiverPositions[9] = new Vector3(-34, -1.5f, 283);
        RiverPositions[10] = new Vector3(-48, -1.5f, 283);
        RiverPositions[11] = new Vector3(-48, -1.5f, 263);
        RiverPositions[12] = new Vector3(-31, -1.5f, 263);
        RiverPositions[13] = new Vector3(-26, -1.5f, 263);
        RiverPositions[14] = new Vector3(-7, -1.5f, 263);
        RiverPositions[15] = new Vector3(5, -1.5f, 243);
        RiverPositions[16] = new Vector3(5, -1.5f, 231);
        RiverPositions[17] = new Vector3(5, -1.5f, 221);
        RiverPositions[18] = new Vector3(5, -1.5f, 203);
        RiverPositions[19] = new Vector3(5, -1.5f, 191);
        RiverPositions[20] = new Vector3(17, -1.5f, 182);
        RiverPositions[21] = new Vector3(31, -1.5f, 182);
        RiverPositions[22] = new Vector3(39, -1.5f, 182);
        RiverPositions[23] = new Vector3(48, -1.5f, 182);

        RiverPositions2[0] = new Vector3(22, 48f, 303);  // река второго отсрова
        RiverPositions2[1] = new Vector3(22, 48f, 310);
        RiverPositions2[2] = new Vector3(38, 48f, 327);
        RiverPositions2[3] = new Vector3(50, 48f, 327);
        RiverPositions2[4] = new Vector3(60, 48f, 327);
        RiverPositions2[5] = new Vector3(70, 48f, 325);
        RiverPositions2[6] = new Vector3(80, 48f, 325);
        RiverPositions2[7] = new Vector3(90, 48f, 325);
        RiverPositions2[8] = new Vector3(80, 48f, 325);
        RiverPositions2[9] = new Vector3(90, 48f, 325);
        RiverPositions2[10] = new Vector3(100, 48f, 325);
        RiverPositions2[11] = new Vector3(105, 48f, 328);
        RiverPositions2[12] = new Vector3(105, 48f, 340);
        RiverPositions2[13] = new Vector3(105, 48f, 350);
        RiverPositions2[14] = new Vector3(105, 48f, 360);
        RiverPositions2[15] = new Vector3(105, 48f, 370);
        RiverPositions2[16] = new Vector3(105, 48f, 380);
        RiverPositions2[17] = new Vector3(105, 48f, 390);
        RiverPositions2[18] = new Vector3(105, 48f, 400);
        RiverPositions2[19] = new Vector3(83, 48f, 400);
        RiverPositions2[20] = new Vector3(83, 48f, 390);
        RiverPositions2[21] = new Vector3(83, 48f, 380);
        RiverPositions2[22] = new Vector3(83, 48f, 370);
        RiverPositions2[23] = new Vector3(83, 48f, 360);
        RiverPositions2[24] = new Vector3(83, 48f, 350);
        RiverPositions2[25] = new Vector3(70, 48f, 349);
        RiverPositions2[26] = new Vector3(60, 48f, 349);
        RiverPositions2[27] = new Vector3(50, 48f, 349);
        RiverPositions2[28] = new Vector3(40, 48f, 349);
        RiverPositions2[29] = new Vector3(14, 48f, 349);
        RiverPositions2[30] = new Vector3(2, 48f, 307);

        RiverPositions3[0] = new Vector3(-10, 92f, 348);  // река третьего отсрова
        RiverPositions3[1] = new Vector3(-20, 92f, 348);
        RiverPositions3[2] = new Vector3(-30, 92f, 348);
        RiverPositions3[3] = new Vector3(-40, 92f, 348);
        RiverPositions3[4] = new Vector3(-50, 92f, 348);
        RiverPositions3[5] = new Vector3(-58, 92f, 340);
        RiverPositions3[6] = new Vector3(-60, 92f, 330);
        RiverPositions3[7] = new Vector3(-56, 92f, 325);
        RiverPositions3[8] = new Vector3(-50, 92f, 323);
        RiverPositions3[9] = new Vector3(-40, 92f, 323);
        RiverPositions3[10] = new Vector3(-30, 92f, 323);
        RiverPositions3[11] = new Vector3(-20, 92f, 323);
        RiverPositions3[12] = new Vector3(-10, 92f, 323);

        LakePositions[0] = new Vector3(22, 0f, -22);  // озеро
        LakePositions[1] = new Vector3(4, 0f, -31);
        LakePositions[2] = new Vector3(27, 0f, -10);
        LakePositions[3] = new Vector3(29, -0f, 0);
        LakePositions[4] = new Vector3(27, 0f, 10);
        LakePositions[5] = new Vector3(13, 0f, 24);
        LakePositions[6] = new Vector3(0, 0f, 25);
        LakePositions[7] = new Vector3(7.7f, 0f, 24);
        LakePositions[8] = new Vector3(-12, 0f, 9);
        LakePositions[9] = new Vector3(-7, 0f, 20);
        LakePositions[10] = new Vector3(21, 0f, 20);
        LakePositions[11] = new Vector3(-14, 0f, -2);
        LakePositions[12] = new Vector3(14, 0f, -28);
        LakePositions[13] = new Vector3(-12, 0f, -14);
        LakePositions[14] = new Vector3(-8, 0f, -25);


    }

    public void FindFishingPlace(Vector3[] mas, int count, bool[]Check, Transform FishParent)   // определение места для рыбалки
    {
        for(int i=0; i<count; i++)
        {
            bool GetIt = false;
            int operationCount = 0;
            while(!GetIt)
            {
                int x = Random.Range(0, mas.Length);
                operationCount++;
                if (!Check[x])
                {
                    GameObject g = Instantiate(GOD.DopEff.FishPlace, mas[x], GOD.DopEff.FishPlace.transform.rotation) as GameObject;
                    g.name = GOD.DopEff.FishPlace.name;
                    g.SetActive(true);
                    g.transform.SetParent(FishParent);
                    g.transform.position = mas[x];
                    FishPlaceScript f = g.GetComponent<FishPlaceScript>();
                   f.Massiv = mas;
                    f.CheckMassiv = Check;
                    f.PlaceInMassiv = x;
                    Check[x] = true;
                    GetIt = true;

                }
                if (operationCount >= mas.Length)
                {
                    /*for(int y=0; y<mas.Length;y++)
                    {
                        if(!OnRiverActive.Contains(y))
                        {
                            GameObject g = Instantiate(GOD.DopEff.FishPlace, mas[x], GOD.DopEff.FishPlace.transform.rotation) as GameObject;
                            g.SetActive(true);
                            g.transform.SetParent(RippleOnWaterIsland);
                            g.GetComponent<FishPlaceScript>().PlaceInMassiv = x;
                            OnRiverActive.Add(x);
                            GetIt = true;
                            break;
                        }
                    }*/
                    GetIt = true;
                }
            }
        }
    }

    public void RemoveFishingPlace(GameObject place)
    {
        Transform p = place.transform.parent;
        FishPlaceScript f = place.GetComponent<FishPlaceScript>();
        f.CheckMassiv[f.PlaceInMassiv] = false;
        Destroy(place);
        StartCoroutine(WaitNewPlace(f.Massiv,f.CheckMassiv,p));
    }

    IEnumerator WaitNewPlace(Vector3[] m, bool[] b, Transform p)
    {
        yield return new WaitForSeconds(10f);
        FindFishingPlace(m, 1, b, p);
    }
}
