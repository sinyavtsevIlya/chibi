﻿using UnityEngine;
using System.Collections;

public class FishChestScript : MonoBehaviour {

    public GameManager GOD;
    string ItemName = " ";
    public DragHandler thisDrag;
    int ItemCount = 0;

    int PressCount = 0;

    public void OnChestPress()
    {
        if (GOD.PlayerInter.Inventory.activeSelf)
        {
            if (PressCount > 0)
            {
                WhatWeGet();
                PressCount = 0;
                StopAllCoroutines();
            }
            else
            {
                PressCount++;
                StopAllCoroutines();
                StartCoroutine(PressKd());
            }
        }
    }

    IEnumerator PressKd()
    {
        yield return new WaitForSeconds(0.2f);
        PressCount = 0;
        if (GOD.PlayerInter.Inventory.activeSelf && !thisDrag.IsDrag)
        {
            if (!GOD.PlayerInter.InfoWindow.activeSelf)
                GOD.PlayerInter.OpenVkladky2();
            GOD.PlayerInter.ShowInfo(this.gameObject);
        }
    }

    void WhatWeGet()   // что получаем из сундука
    {
        if (ItemName == " ")
        {
            int x = 0;
            switch (name)
            {
                case "Icon_FishChestRed":
                    x = Random.Range(0, 101);
                    if (x < 20)
                    {
                        ItemName = "Dimond";
                        ItemCount = 1;
                    }
                    else if (x >= 20 && x < 40)
                    {
                        ItemName = "EssenceIce";
                        ItemCount = 1;
                    }
                    else if (x >= 40 && x < 60)
                    {
                        ItemName = "EssenceSun";
                        ItemCount = 1;
                    }
                    else if (x >= 60 && x < 80)
                    {
                        ItemName = "EssenceWater";
                        ItemCount = 1;
                    }
                    else
                    {
                        ItemName = "Roll";
                        ItemCount = 1;
                    }
                    break;
                case "Icon_FishChestYellow":
                    x = Random.Range(0, 101);
                    if (x < 25)
                    {
                        ItemName = "Sapphire";
                        ItemCount = 1;
                    }
                    else if (x >= 25 && x < 50)
                    {
                        ItemName = "Ruby";
                        ItemCount = 1;
                    }
                    else if (x >= 50 && x < 75)
                    {
                        ItemName = "Amethyst";
                        ItemCount = 1;
                    }
                    else
                    {
                        ItemName = "Emerald";
                        ItemCount = 1;
                    }
                    break;
                case "Icon_FishChestPurpule":
                    x = Random.Range(0, 101);
                    if (x < 25)
                    {
                        ItemName = "CocoaBeans";
                        ItemCount = 1;
                    }
                    else if (x >= 25 && x < 50)
                    {
                        ItemName = "Condiment";
                        ItemCount = 1;
                    }
                    else if (x >= 50 && x < 75)
                    {
                        ItemName = "Candy";
                        ItemCount = 1;
                    }
                    else
                    {
                        ItemName = "FishPie";
                        ItemCount = 1;
                    }
                    break;
                case "Icon_FishChestBlue":
                    x = Random.Range(0, 101);
                    if (x < 25)
                    {
                        ItemName = "Thread";
                        ItemCount = 1;
                    }
                    else if (x >= 25 && x < 50)
                    {
                        ItemName = "Rope";
                        ItemCount = 1;
                    }
                    else if (x >= 50 && x < 75)
                    {
                        ItemName = "Cord";
                        ItemCount = 1;
                    }
                    else
                    {
                        ItemName = "IronIngot";
                        ItemCount = 1;
                    }
                    break;
                case "Icon_FishChestGreen":
                    x = Random.Range(0, 101);
                    if (x < 50)
                    {
                        ItemName = "Wood";
                        ItemCount = 5;
                    }
                    else
                    {
                        ItemName = "Stone";
                        ItemCount = 5;
                    }
                    break;
            }
        }
        Debug.Log("FISH " + ItemName);
        if (GOD.InventoryContr.CanAdd(ItemName, ItemCount))
        {
            GOD.InventoryContr.AddToInventory(ItemName, ItemCount);
            GOD.PlayerInter.ShowNewItem(ItemName, ItemCount);
            GOD.InventoryContr.RemoveFromInventory(GetComponent<InfoScript>(), 1);
            ItemName = " ";
            GOD.PlayerInter.OpenInventory();
            GOD.Audio.Sound.PlayOneShot(GOD.Audio.OpenChest);
        }
        else
            GOD.PlayerInter.OpenCantOpen();

        
    }
}
