using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DynamicMapUpdate : MonoBehaviour 
{
    public float UpdateTimer = 0.5F;
    private Vector3 lastPosition;
    Bounds lastBounds;
    ObjectScript OS;
    

    public void Init (bool IsBuild=false) 
    {
        if (IsBuild)
        {
            lastPosition = transform.position;
            lastBounds = OS.mapUpdateRenderer.bounds;
            UpdateMapOnce(OS.mapUpdateRenderer);
        }
        else
        {
            OS = GetComponent<ObjectScript>();
            if (OS)
            {
                lastPosition = transform.position;
                lastBounds = OS.mapUpdateRenderer.bounds;
                UpdateMapOnce(OS.mapUpdateRenderer);
            }
        }
      //  StartCoroutine(UpdateMap());
	}	

    IEnumerator UpdateMap()
    {
        if (transform.position != lastPosition)
        {
            Bounds bR = GetComponent<Renderer>().bounds;
            Pathfinder.Instance.DynamicRaycastUpdate(lastBounds);
            Pathfinder.Instance.DynamicRaycastUpdate(bR);
            lastPosition = transform.position;
            lastBounds = bR;
        }

        yield return new WaitForSeconds(UpdateTimer);
        StartCoroutine(UpdateMap());
    }

   public void UpdateMapOnce(Renderer r)
    {
        // Debug.Log("UP");
        //  Bounds bR = t.GetComponent<Renderer>().bounds;
        Bounds bR = r.bounds;
        Pathfinder.Instance.DynamicRaycastUpdate(lastBounds);
        Pathfinder.Instance.DynamicRaycastUpdate(bR);
        lastPosition = transform.position;
        lastBounds = bR;
    }

    void OnDestroy()
    {
       // UpdateMapOnce();
    }
}
