using UnityEngine;
using System.Collections;

public class AI : Pathfinding
{

    Enemy EnemyScript;

    //public Transform target;
    public Vector3 targetPosition;
    public bool WeGoing = false;

    private CharacterController controller;
    public bool newPath = true;
    bool moving = true; 
    private GameObject[] AIList;

    public void Init()
    {
        AIList = GameObject.FindGameObjectsWithTag("Enemy");
        EnemyScript = GetComponent<Enemy>();
    }

    int NullCount = 0;
    void FixedUpdate()
    {
        bool IsAttack = false;
        if (EnemyScript.IsPlayer)//���� ������� �� �������
        {
            if (!EnemyScript.GOD.PlayerContr.IsAlive)
            {
                EnemyScript.OutPlayerDead();
                return;
            }
            IsAttack = Vector3.SqrMagnitude(targetPosition - transform.position)<12f;
            if (IsAttack)  //���� ����� � ����� - ��������������� � �������
            {
                EnemyScript.EnemyAttack();
                transform.LookAt(targetPosition);
                if (EnemyScript.EnemyWalk && EnemyScript.EnemyWalk.isPlaying && EnemyScript.name != "Ozz")
                    EnemyScript.EnemyWalk.Stop();
            }
        }

            if (WeGoing)
        {
            if (EnemyScript.IsPlayer)//���� ������� �� �������
            {
                if (!IsAttack &&EnemyScript.m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Run"))//���������� ������
                {
                    if (EnemyScript.EnemyWalk && !EnemyScript.EnemyWalk.isPlaying&& EnemyScript.name != "Ozz")
                        EnemyScript.EnemyWalk.Play();
                    if (Path.Count == 0)
                        NewPath();
                    else if(Vector3.SqrMagnitude(targetPosition- Path[Path.Count - 1]) > 25F)//���� ��������� ����� ����� ������� ������ �� ������
                    {
                        NewPath();
                    }
                    if (Path.Count == 0)
                    {
                        NullCount++;
                        if (NullCount > 50)
                        {
                            EnemyScript.OutPlayer();
                            NullCount = 0;
                        }
                    }
                    else
                    {
                        NullCount = 0;
                        MoveMethod();
                    }
                }
            }
            else
            {
                if (Path.Count == 0) //������ ����
                    NewPath();
                if (hasPath)
                    MoveMethod();
                else if(EnemyScript.State != Enemy.EnemyState.returnHome)
                {
                    if(EnemyScript.State!=Enemy.EnemyState.idle)
                        EnemyScript.Timer = 0;
                    EnemyScript.State = Enemy.EnemyState.idle;
                }
            }
        }
     
    }

    void NewPath()
    {
        if (hasPath)
            FindPath(transform.position, targetPosition);

        hasPath = false;
    }


    private void MoveMethod()
    {
        if (Path.Count > 0)
        {
            Vector3 direction = (Path[0] - transform.position).normalized;
            transform.LookAt(direction + transform.position);

            foreach (GameObject g in AIList)
            {
                if (Vector3.SqrMagnitude(g.transform.position - transform.position) < 1F)
                {
                    Vector3 dir = (transform.position - g.transform.position).normalized;
                    dir.Set(dir.x, 0, dir.z);
                    direction += 0.2F * dir;
                }
            }

            direction.Normalize();


            transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, Time.deltaTime * EnemyScript.EnemySpeed);
            if (transform.position.x < Path[0].x + 0.4F && transform.position.x > Path[0].x - 0.4F && transform.position.z > Path[0].z - 0.4F && transform.position.z < Path[0].z + 0.4F)
            {
                Path.RemoveAt(0);
            }

            RaycastHit[] hit = Physics.RaycastAll(transform.position + (Vector3.up * 20F), Vector3.down, 100);
            float maxY = -Mathf.Infinity;
            foreach (RaycastHit h in hit)
            {
                if ((h.transform.tag == "Untagged") || (h.transform.tag == "Ground"))
                {
                    if (maxY < h.point.y)
                    {
                        maxY = h.point.y;
                    }
                }
            }
            if (maxY > -100)
            {
                transform.position = new Vector3(transform.position.x, maxY + 0F, transform.position.z);
            }
        }
    }
}
