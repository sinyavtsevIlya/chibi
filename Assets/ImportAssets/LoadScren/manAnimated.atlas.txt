
manAnimated.png
size: 1623,1623
format: RGBA8888
filter: MipMap,MipMap
repeat: none
axe
  rotate: false
  xy: 1, 971
  size: 1008, 651
  orig: 1010, 653
  offset: 1, 1
  index: -1
back
  rotate: true
  xy: 951, 1
  size: 967, 622
  orig: 969, 624
  offset: 1, 1
  index: -1
elephant
  rotate: false
  xy: 207, 217
  size: 84, 72
  orig: 86, 74
  offset: 1, 1
  index: -1
eyelids
  rotate: false
  xy: 1, 49
  size: 180, 69
  orig: 182, 71
  offset: 1, 1
  index: -1
ground
  rotate: false
  xy: 1, 292
  size: 947, 676
  orig: 949, 678
  offset: 1, 1
  index: -1
hair
  rotate: false
  xy: 1, 121
  size: 203, 168
  orig: 205, 170
  offset: 1, 1
  index: -1
hand
  rotate: true
  xy: 1472, 1461
  size: 161, 94
  orig: 163, 96
  offset: 1, 1
  index: -1
hand2
  rotate: false
  xy: 1012, 994
  size: 194, 109
  orig: 196, 111
  offset: 1, 1
  index: -1
man
  rotate: false
  xy: 1012, 1106
  size: 457, 516
  orig: 459, 518
  offset: 1, 1
  index: -1
