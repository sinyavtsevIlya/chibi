﻿using Balaso;
using System;
using UnityEngine;

public class AppTracking
{
    private Action<bool> _callback;

    public void Initialize(Action<bool> callback )
    {
        _callback = callback;

#if UNITY_IOS
        AppTrackingTransparency.OnAuthorizationRequestDone += OnAuthorizationRequestDone;

        var currentStatus = AppTrackingTransparency.TrackingAuthorizationStatus;
        Debug.Log(string.Format("Current authorization status: {0}", currentStatus.ToString()));
        if (currentStatus != AppTrackingTransparency.AuthorizationStatus.AUTHORIZED)
        {
            Debug.Log("Requesting authorization...");
            AppTrackingTransparency.RequestTrackingAuthorization();
        }
        else
        {
            UpdateAppTracking();
        }
#else
        _callback?.Invoke(true);
#endif

    }


#if UNITY_IOS
    private void OnAuthorizationRequestDone(AppTrackingTransparency.AuthorizationStatus status)
    {
        switch (status)
        {
            case AppTrackingTransparency.AuthorizationStatus.NOT_DETERMINED:
                Debug.Log("AuthorizationStatus: NOT_DETERMINED");
                break;
            case AppTrackingTransparency.AuthorizationStatus.RESTRICTED:
                Debug.Log("AuthorizationStatus: RESTRICTED");
                break;
            case AppTrackingTransparency.AuthorizationStatus.DENIED:
                Debug.Log("AuthorizationStatus: DENIED");
                break;
            case AppTrackingTransparency.AuthorizationStatus.AUTHORIZED:
                Debug.Log("AuthorizationStatus: AUTHORIZED");
                break;
        }

        UpdateAppTracking();
    }

    private void UpdateAppTracking()
    {
        var IDFA = AppTrackingTransparency.IdentifierForAdvertising();
        var trackingEnabled = !string.IsNullOrEmpty(IDFA);
        
        _callback?.Invoke(trackingEnabled);
    }
#endif

}
